declare module 'react-images-viewer' {
    import * as React from 'react';
  
    interface ReactImagesViewerProps {
      // visible: boolean;
      onClose: () => void;
      imgs: Array<{
        src: string;
        alt?: string;
      }>;
      activeIndex?: number;
      isOpen : boolean;
      onClickPrev: Function;
      onClickNext: Function;
      currImg:number;
      width: number;
      zIndex?: number;
      drag?: boolean;
      zoom?: boolean;
      rotatable?: boolean;
      scalable?: boolean;
      maxScale?: number;
      defaultScale?: number;
      zIndexStart?: number;
      disableMouseZoom?: boolean;
      spinnerColor?:string;
      onMaskClick?: () => void;
    }
  
    export default class ReactImagesViewer extends React.Component<ReactImagesViewerProps> {}
  }