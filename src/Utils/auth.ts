const TOKEN = 'token'
const REFRESH_TOKEN = 'refresh_token'
const ADMIN_USER_DETAILS = 'admin-details'

const isUserAuthenticated = () => {
    const token = localStorage.getItem(TOKEN)
    if (token) {
        return true
    }
    return false
}

const clearAll = () => {
    localStorage.clear()
}

const getToken = () => {
    return localStorage.getItem(TOKEN)
}

const getRefreshToken = () => {
    return localStorage.getItem(REFRESH_TOKEN)
}

const setToken = (token: string) => {
    localStorage.setItem(TOKEN, token)
}

const setRefreshToken = (token: string) => {
    localStorage.setItem(REFRESH_TOKEN, token)
}

const getAdminUser = () => {
    return localStorage.getItem(ADMIN_USER_DETAILS)
}

export {
    isUserAuthenticated,
    clearAll,
    getToken,
    setToken,
    setRefreshToken,
    getRefreshToken,
    getAdminUser
}