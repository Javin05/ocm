export const ORDERSTATUS: any = {
  ACCEPTED: 'Kommande festligheter',
  PENDING: 'Pågående förfrågningar',
};

export const IMGAEVALIDATION: any = [
  "image/jpeg",
  "image/jpg",
  "image/png",
  "image/gif",
];
