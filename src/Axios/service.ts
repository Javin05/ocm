import axios from "axios";
import { clearAll, getRefreshToken, getToken, setRefreshToken, setToken } from "../Utils/auth";
import { failureNotify } from "../Components/Toast/Toast";

// Axios instance for API requiring authorization token
// const commonAxiosInstance = axios.create({
//     baseURL: import.meta.env.VITE_CORE_BASE_URL
// })

const commonAxiosInstance = axios.create({
    baseURL: import.meta.env.VITE_CORE_BASE_URL,
    headers: {
        "Access-Control-Allow-Origin": "*",
        'content-type': 'application/json;charset=UTF-8',
        'accept': 'application/json, text/plain, */*',
    }
});


// Axios instance for Login/Signup API
const authAxiosInstance = axios.create({
    // baseURL: import.meta.env.VITE_CORE_BASE_URL
});
// Axios instance for API requiring formdata in body
const formAxiosInstance = axios.create({
    baseURL: import.meta.env.VITE_CORE_BASE_URL,
    headers: {
        "Access-Control-Allow-Origin": "*",
        'content-type': 'application/json;charset=UTF-8',
        'accept': 'application/json, text/plain, */*',
    }
})

const handleResponse = (response: any) => {
    // Handle successful responses
    return response;
};

// const HandleError = (error: {
//   response: { data: any; status: any; headers: any };
//   request: any;
//   message: any;
//   config: any;
// }) => {
//   let token = getToken();
//   // Handle errors
//   console.log('ifblocktoken', token);
//   if (token && error.response && error.response.status) {
//     // Token exists and error response is valid
//     console.log('ifblock', error);
//     if (error.response.status === 401 || error.response.status === 403) {
//       // Handle 401 or 403 errors
//       if (error.response.status === 403) {
//         clearAll();
//         window.location.href = `${window.location.origin}/login`;
//       } else {
//         const postBody = {
//           token: getRefreshToken(),
//         };
//         const headers = {
//           'content-type': 'application/json;charset=UTF-8',
//           Accept: 'application/json',
//         };
//         // Hitting refresh token API and updating the Tokens in local storage
//         authAxiosInstance
//           .post('/cemo/auth/new/access', postBody, { headers })
//           .then((res) => {
//             setToken(res.data?.accessToken);
//             setRefreshToken(res.data?.refreshToken);
//             token = res.data?.accessToken;
//             // Retry the original request after refreshing tokens
//             commonAxiosInstance
//               .request({
//                 url: error.config.url,
//                 method: error.config.method,
//                 data: error.config.data,
//                 headers: error.config.headers,
//               })
//               .catch((err) => {
//                 console.log(err);
//               });
//           })
//           .catch((err) => {
//             console.log(err);
//           });
//       }
//     }
//   } else {
//     if (error.response?.data?.errorErrorMessage) {
//       failureNotify(error.response.data.errorErrorMessage);
//     } else {
//       window.location.href = `${window.location.origin}/login`;
//     }
//     clearAll();
//   }
// };


const HandleError = (error: { response: { data: any; status: any; headers: any; }; request: any; message: any; config: any; }) => {
    let token = getToken();
    // Handle errors
    console.log('ifblocktoken',token)
    if (token !== 'undefined' && token) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log('ifblock',error)
        if (error.response.status === 403) {

            if (error.response.status === 403) {
                // failureNotify('Sesion expired, login to continue');
                clearAll()
                window.location.href = `${window.location.origin}/login`
            } else {
                const postBody = {
                    "token": getRefreshToken()
                }
                const headers = {
                    // 'Content-Type': 'application/json',
                    'content-type': 'application/json;charset=UTF-8',
                    'Accept': 'application/json',
                };
                // Hitting refresh token API and updating the Tokens in localstorage
                authAxiosInstance.post('/cemo/auth/new/access', postBody, { headers }).then((res) => {
                    setToken(res.data?.accessToken);
                    setRefreshToken(res.data?.refreshToken);
                    token = res.data?.accessToken;
                }).catch((err) => {
                    console.log(err)
                })
            }

            commonAxiosInstance.request({
                url: error.config.url,
                method: error.config.method,
                data: error.config.data,
                headers: error.config.headers
            })
        }

    } else {
        if(error.response.data?.errorErrorMessage){
            failureNotify(error.response.data?.errorErrorMessage);
        }else {
           // failureNotify("session expired,  login to continue");
           window.location.href = `${window.location.origin}/login`
        }
         
        clearAll()
       
    }

};

formAxiosInstance.interceptors.request.use((config: any) => {
    let token = getToken() as string;

    if (token) {
        config.headers = {
            "Authorization": `Bearer ${token}`,
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "multipart/form-data",
            'accept': 'application/json, text/plain, */*',
        }
    } else {
        config.headers = {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "multipart/form-data",
            'accept': 'application/json, text/plain, */*',
        }
    }
    return config;
})

commonAxiosInstance.interceptors.request.use((config: any) => {
    let token = getToken() as string;

    if (token) {
        config.headers = {
            "Authorization": `Bearer ${token}`,
            "Access-Control-Allow-Origin": "*",
            'content-type': 'application/json;charset=UTF-8',
            'accept': 'application/json, text/plain, */*',
        }
    } else {
        config.headers = {
            "Access-Control-Allow-Origin": "*",
            'content-type': 'application/json;charset=UTF-8',
            'accept': 'application/json, text/plain, */*',
        }
    }

    return config
})

formAxiosInstance.interceptors.request.use((config: any) => {
    const token = getToken();
    if (config.headers && token) {
        config.headers = {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'multipart/form-data',
            // "Access-Control-Allow-Origin": "*",
        }
    } else {
        config.headers = {
            "Access-Control-Allow-Origin": "*",
        }
    }
    return config
})

commonAxiosInstance.interceptors.response.use(handleResponse, HandleError);
formAxiosInstance.interceptors.response.use(handleResponse, HandleError);
authAxiosInstance.interceptors.response.use(handleResponse, HandleError);

export {
    commonAxiosInstance as commonAxios,
    formAxiosInstance as formAxios
}