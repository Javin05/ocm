import { combineReducers, configureStore } from '@reduxjs/toolkit'
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist'
import storage from 'redux-persist/lib/storage';
// import sessionStorage from 'redux-persist/lib/storage/session';

//Reducers
import userReducer from "./features/userSlice"
import eventReducer from "./features/eventsSlice"
import productCartReducer from "./features/productCartSlice"
import notificationReducer from './features/notificationSlice'
import wishlistSlice from './features/wishlistSlice';
import counterReducer from "./features/cartCountSlice";

const persistConfig = {
  key: 'root',
  storage: storage,
  debug: true,
  // blacklist: ['productsCart'] // exclude productsCartReducer from being persisted
};

const rootReducer = combineReducers({
  user: userReducer,
  events: eventReducer,
  productsCart: productCartReducer,
  notification:notificationReducer,
  cartCount:counterReducer,
  wishlist:wishlistSlice
});

const persistedReducer = persistReducer(persistConfig, rootReducer);


export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
})

export const persistor = persistStore(store)

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch