import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface Event {
    eventDetails: {
        id: string,
        name: string,
        city: string,
        image: string,
        description: string,
        appUserId: string,
        pincode: string,
        operatingHours: [
            {
                id: string,
                startTime: string,
                endTime: string,
                eventId: string
            }
        ],
        category: {
            id: string,
            categoryName: string
        }
    }[]
}

const initialState: Event = {
    eventDetails: []
}

export const eventsSlice = createSlice({
    name: 'event',
    initialState,
    reducers: {
        setEventDetails: (state, action: PayloadAction<any>) => {
            state.eventDetails = action.payload
        }
    },
})

// Action creators are generated for each case reducer function
export const { setEventDetails } = eventsSlice.actions

export default eventsSlice.reducer