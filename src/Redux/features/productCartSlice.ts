import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface Product {
    quantity: number,
}

export interface ProductCart {
    productDetails: Product
}

const initialState: ProductCart = {
    productDetails: {
        quantity: 0
    }
}

export const productsCartSlice = createSlice({
    name: 'event',
    initialState,
    reducers: {
        addProductToCart: (state) => {
            state.productDetails.quantity += 1;
        },
        clearCart: (state) => {
            state.productDetails.quantity = 0;
        }
    },
})

// Action creators are generated for each case reducer function
export const { addProductToCart, clearCart } = productsCartSlice.actions

export default productsCartSlice.reducer