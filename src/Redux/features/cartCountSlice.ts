import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { commonAxios } from '../../Axios/service';


export interface Product {
  quantity: number,
}

export interface ProductCart {
  productDetails: Product
}

const initialState: ProductCart = {
  productDetails: {
      quantity: 0
  }
}

export const getCartListCount:any = createAsyncThunk('cartCount/getCartListCount', async (userId:any) => {
  try {
    const response = await commonAxios.get(`/cemo/bookingitems/get/cart/${userId}`);
    return response.data?.bookingItemList?.length
  } catch (error) {
    throw error;
  }
});

export const counterSlice:any = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    getCartListCount: (state:any,payload:any) => {
      state.productDetails.quantity = payload
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getCartListCount.fulfilled, (state, action) => {
        state.productDetails.quantity = action.payload;
      })
      .addCase(getCartListCount.rejected, (state, action) => {
        // Handle the error or update state as needed
      });
  },
})

// Action creators are generated for each case reducer function
export const { } = counterSlice.actions

export default counterSlice.reducer