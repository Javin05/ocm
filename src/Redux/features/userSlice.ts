import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface User {
    userDetails: {
        email: string,
        firstName: string,
        id: string,
        lastName: string,
        mobile: string,
        password: string,
        telephone: string,
        profileImage:string,
        businessAddress:string,
        businessCity:string,
        role: string
    }
}

const initialState: User = {
    userDetails: {
        email: "",
        firstName: "",
        id: "",
        lastName: "",
        mobile: "",
        password: "",
        telephone: "",
        profileImage:"",
        businessAddress:"",
        businessCity:"",
        role:""
    }
}

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setUserDetails: (state, action: PayloadAction<any>) => {
            state.userDetails = action.payload
        }
    },
})

// Action creators are generated for each case reducer function
export const { setUserDetails } = userSlice.actions

export default userSlice.reducer