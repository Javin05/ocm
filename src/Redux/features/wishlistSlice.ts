import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { commonAxios } from '../../Axios/service';

export interface Product {
    wishlist: any,
}

export interface ProductCart {
  wishlistDetails: Product
}

const initialState: ProductCart = {
    wishlistDetails: {
      wishlist: []
    }
}

export const getWishlistCount:any = createAsyncThunk('wishlist/getWishlistCount', async (userId:any) => {
  try {
    const response = await commonAxios.get(`/cemo/favourite-items/${userId}`);
    return response.data;
  } catch (error) {
    throw error;
  }
});

export const wishlistSlice = createSlice({
    name: 'event',
    initialState,
    reducers: {
      getWishlistCount: (state:any,payload:any) => {
        state.wishlistDetails.wishlist = payload
      }
    },
    extraReducers: (builder) => {
      builder
        .addCase(getWishlistCount.fulfilled, (state, action) => {
          state.wishlistDetails.wishlist = action.payload;
        })
        .addCase(getWishlistCount.rejected, (state, action) => {
          // Handle the error or update state as needed
        });
    },
})

// Action creators are generated for each case reducer function
export const {  } = wishlistSlice.actions

export default wishlistSlice.reducer