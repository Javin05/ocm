import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface Root {
    notificationDetails: {
        messageCount: number
        commentCount: number
        messageList: MessageList[]
        commentList: CommentList[]
        adminMessageCount:number
        supplierMessageCount:number
        customerMessageCount:number
    }
}

export interface MessageList {
    id: string
    booking: Booking
    customer: Customer
    supplier: Supplier
    message: string
    isCustomerRead: boolean
    isSupplierRead: boolean
    createdDate: string
    updatedDate: string
    isCustomerMessage: boolean
}

export interface Booking {
    id: string
    bookingDate: string
    bookingTime: string
    memberCount: number
    firstName: string
    lastName: string
    address: string
    postalCode: any
    category: string
    orderNumber: string
    bookingStatus: string
    payment: any
    overAllVATAmount?: number
    overAllTotalAmount?: number
    paymentType: string
    appUserId: any
}

export interface Customer {
    id: string
    firstName: string
    lastName: string
    mobile: string
    telephone: string
    email: string
    businessName: string
    businessNumber: string
    businessAddress: string
    businessPinCode: string
    businessCity: string
    password: string
    accountStatus: string
    createdAt: string
    updatedAt: string
    categories: string
    profileImage: string
    resetToken: any
}

export interface Supplier {
    id: string
    firstName: string
    lastName: string
    mobile: string
    telephone: string
    email: string
    businessName: string
    businessNumber: string
    businessAddress: string
    businessPinCode: string
    businessCity: string
    password: string
    accountStatus: string
    createdAt: string
    updatedAt: string
    categories: string
    profileImage: string
    resetToken: any
}

export interface CommentList {
    id: string
    customer: Customer
    supplier: Supplier
    event: any
    product: Product
    comments: string
    rating: number
    isRead: boolean
    createdDate: string
    updatedDate: string
    status: string
}
export interface Product {
    id: string
    name: string
    description: string
    image: string
    quantity: number
    price: number
    status: string
    productNumber: string
    createdAt: string
    updatedAt: string
    appUser: AppUser
    category: Category
    taxType: TaxType
    isFavourite: boolean
}

export interface AppUser {
    id: string
    firstName: string
    lastName: string
    mobile: string
    telephone: string
    email: string
    businessName: string
    businessNumber: string
    businessAddress: string
    businessPinCode: string
    businessCity: string
    password: string
    accountStatus: string
    createdAt: string
    updatedAt: string
    categories: string
    profileImage: string
    resetToken: any
}

export interface Category {
    id: string
    categoryName: string
    isSystemFlag: boolean
    image: string
    createdAt: string
    status: string
    supplier: Supplier
}
export interface TaxType {
    id: string
    taxPercentage: number
}


const initialState: Root = {
    notificationDetails: {
        messageCount: 0,
        commentCount: 0,
        supplierMessageCount:0,
        adminMessageCount:0,
        customerMessageCount:0,
        messageList: [],
        commentList: []
    }
}

export const notificationSlice = createSlice({
    name: 'notification',
    initialState,
    reducers: {
        setNotification: (state, action: PayloadAction<any>) => {
            state.notificationDetails = action.payload
        },
    },
})

// Action creators are generated for each case reducer function
export const { setNotification } = notificationSlice.actions

export default notificationSlice.reducer