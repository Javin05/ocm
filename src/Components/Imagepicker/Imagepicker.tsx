import React, { useEffect, useState } from "react";
import { RxCross2 } from "react-icons/rx";
import { failureNotify, successNotify } from "../Toast/Toast";

function Imagepicker({
  max,
  value,
  setImageValue,
  onImageUpload,
  updatedValue,
  base64,
  isDisabled,
  product,
  imagevalue,
  updatedValues,
  base64Image,
}: any) {
  const [selectedFile, setSelectedFile] = useState([]);
  const [error, setError] = useState("");
  const [limit, setLimit] = useState(5);
  const [imagelist, setImagelist] = useState<any>([]);

  const getBase64 = (file: any) => {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      imagelist.push(reader.result?.toString());
      //   this.setState({});
    };
    reader.onerror = function (error) { };
  };

  const setFiles = (files: any) => {
    setImageValue(files);
  };

  const handleFileChange = async (event: any) => {
    event.preventDefault();
    const files = event.target.files;
    const fileArray: any = Array.from(files);
    const allowedExtensions = [".jpg", ".jpeg", ".png", ".gif"];

    const allValid = fileArray.every((file: any) => {
      const extension = file.name.toLowerCase().split('.').pop();
      return allowedExtensions.includes(`.${extension}`);
    });

    if (allValid) {
      if (files.length <= max) {
        setSelectedFile(files[0]); // Store the selected image

        const promises = fileArray?.map((file: any, i: any) => {
          return new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.onload = (event: any) => {
              const base64String = event.target.result;
              resolve(base64String);
            };

            reader.onerror = (error) => {
              reject(error);
            };

            reader.readAsDataURL(file);
          });
        });
        Promise.all(promises)
          .then((base64Array: any) => {
            const need_image = base64Array?.map((value: any, i: any) => {
              return {
                values: value,
                id: i + 1,
              };
            });
            if (value === "product" || "user-profile") {
              updatedValue(need_image);
            } else {
              updatedValues(need_image);
            }
          })
          .catch((error) => { })
          .finally(() => {
            onImageUpload(files);
          });
      } else {
        setError(`You can select up to ${max} files.`);
      }
    } else {
      failureNotify("Please upload valid image files (jpg, jpeg, png, gif).");
    }
  };

  return (
    <>
      <div>
        <input
          type="file"
          id="file-input" 
          name="file-input"
          onChange={handleFileChange}
          disabled={isDisabled}
          multiple
          className="w-[100%]"
        />
         <label id="file-input-label" htmlFor="file-input">Select a File</label>
          {error && <p className="text-red-500">{error}</p>}
      </div>
    </>
  );
}

export default Imagepicker;
