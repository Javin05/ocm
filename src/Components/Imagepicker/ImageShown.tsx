import React from "react";
import { RxCross2 } from "react-icons/rx";

function ImageShown({
  base64Image,
  handleUpload,
  updatedValue,
  product,
  imagevalue,
  updatedValues,
  imagevalues,
  updatedValuesEventUpdate,
  imageBinding,
  bindedImage,
  deleteImages,
  isDisabled,
}: any) {
  const deleteImage = (id: any, values: any) => {
    let value;
    if (values === "createproduct(or)event") {
      value = base64Image.filter((getId: any) => {
        return getId.id !== id;
      });
      deleteImages(value);
    } else if (values === "updateproduct(or)updateevent") {
      value = bindedImage.filter((getId: any) => {
        return getId.id !== id;
      });

      imageBinding(value);
    }
  };
  console.log(bindedImage,"bindedImage")
  return (
    <div className="flex ">
      {base64Image && base64Image.length > 0
        ? base64Image?.map((img: any) => {
          return (
            <>
              <div className="px-3 py-2 relative">
                <img
                  src={img.values}
                  alt="image"
                  width={200}
                  height={200}
                  className="img"
                />

                <RxCross2
                  className="cursor-pointer m-2 absolute top-0 right-1 bg-red-400"
                  onClick={(id) => {
                    deleteImage(img.id, "createproduct(or)event");
                  }}
                />
              </div>
            </>
          );
        })
        : bindedImage
          ? bindedImage?.map((img: any) => {
            return (
              <>
                <div className="px-3 py-2 relative">
                  <img
                    src={img.image}
                    alt="image"
                    width={100}
                    height={100}
                    className="img"
                  />
                  {
                    !isDisabled &&
                    <RxCross2
                      className="cursor-pointer m-2 absolute top-0 right-1 bg-red-400"
                      onClick={(id) => {
                        deleteImage(img.id, "updateproduct(or)updateevent");
                      }}
                    />

                  }

                </div>
              </>
            );
          })
          : null}
    </div>
  );
}

export default ImageShown;
