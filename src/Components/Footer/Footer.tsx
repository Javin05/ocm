import { useContext } from "react";
import creativeImg from "../../assets/blixa-black.png";
import instaImg from "../../assets/instagram.svg";
import facebookImg from "../../assets/facebook.svg";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { AuthContext } from "../../Pages/RootLayout/RootLayout";
import { useNavigate } from "react-router-dom";

const Footer = () => {
  const userDetails = useSelector((state: RootState) => state.user.userDetails);
  const { setIsDeleteEventModalOpen, setMenuClicked } = useContext(AuthContext);
  const navigateToAdminLogin = () => {
    setIsDeleteEventModalOpen(true);
    setMenuClicked("adminlogin");
    window.scrollTo(0, 0);
  };

  const navigateToSupplierLogin = () => {
    setMenuClicked("supplierlogin");
    setIsDeleteEventModalOpen(true);
    window.scrollTo(0, 0);
  };

  const navigateToSupplierSignup = () => {
    setMenuClicked("suppliersignup");
    setIsDeleteEventModalOpen(true);
    window.scrollTo(0, 0);
  };

  return (
    <div className="bg-[#FFE5D8]">
      <div className="grid grid-cols-1 lg:grid-cols-4 gap-2 p-5 sm:mx-8 lg:mx-20 text-center lg:text-left">
        <div className="flex lg:justify-start justify-center" style={{ marginTop: '-19px', maxWidth: '261px' }}>
          <a href="/">
            <img src={creativeImg} className=" ml-4 pl-0 w-52 cursor-pointer" />
          </a>
        </div>
        <div className="grid-item mt-5 md:pl-5 flex flex-col">
          <h1 className="text-[18px] md:text-[24px] font-semibold  font-[sofiapro] text-secondary">
            Snabblänkar
          </h1>
          <span className="text-[20px] text-secondary mt-2 font-extralight cursor-pointer">
            <Link to="/aboutus">Om oss</Link>
          </span>
          <span className="text-[20px] text-secondary mt-0.5 font-extralight cursor-pointer">
            <Link to={`/contactus/${true}`}>Kontakta oss</Link>
          </span>
        </div>
        <div className="grid-item mt-5 md:pl-5 flex flex-col">
          <h1 className="text-[18px] md:text-[24px] font-semibold font-[sofiapro] text-secondary ">
            Företag
          </h1>
          {userDetails.id ? (
            <span className="text-[20px] text-secondary mt-2 font-extralight cursor-pointer">
              <a onClick={() => navigateToSupplierLogin()}>Leverantörsinlogg</a>
            </span>
          ) : (
            <span className="text-[20px] text-secondary cursor-pointer mt-2 font-extralight">
              <Link to="/supplierlogin">Leverantörsinlogg</Link>
            </span>
          )}
          {userDetails.id ? (
            <span className="text-[20px] text-secondary mt-0.5 font-extralight cursor-pointer">
              <a onClick={() => navigateToAdminLogin()}>Admininlogg</a>
            </span>
          ) : (
            <span className="text-[20px] text-secondary mt-0.5 font-extralight cursor-pointer">
              <Link to="/adminlogin">Admininlogg</Link>
            </span>
          )}
          {userDetails.id ? (
            <span className="text-[20px] text-secondary mt-0.5 font-extralight cursor-pointer">
              <a onClick={() => navigateToSupplierSignup()}>Bli en av oss!</a>
            </span>
          ) : (
            <span className="text-[20px] text-secondary mt-0.5 font-extralight cursor-pointer">
              <Link to="/suppliersignup">Bli en av oss!</Link>
            </span>
          )}
        </div>
        <div className="grid-item mt-5 md:pl-5 flex flex-col">
          <h1 className="text-[18px] md:text-[24px] font-semibold font-[sofiapro] text-secondary">
            Frågor?
          </h1>
          <span className="text-[20px] text-secondary mt-0.5 font-extralight cursor-pointer">
            <Link to={`/contactus/${false}`}>FAQ</Link>
          </span>
        </div>
      </div>
      <div className="border-b border-[#2B292D] opacity-10"></div>
      <div className="sm:relative sm:py-7 lg:py-4">
        <div className="flex justify-center items-center py-3 flex-col sm:flex-row">
          <span className="text-[14px] sm:mx-3 text-secondary mt-2 font-extralight cursor-pointer">
            <Link to="/termsAndConditions">Allmänna kundvillkor</Link>
          </span>
          <span className="text-[14px] sm:mx-3 text-secondary mt-2 font-extralight cursor-pointer">
            <Link to="/cooperationpatners">
              Allmänna villkor samarbetspartners
            </Link>
          </span>
          <span className="text-[14px] text-secondary mx-1 sm:mx-5 cursor-pointer mt-2">
            <Link to="/Retur">Retur</Link>
          </span>
          <span className="text-[14px] text-secondary mx-1 sm:mx-5 cursor-pointer mt-2">
            <Link to="/Integritetspolicy">Integritetspolicy</Link>{" "}
          </span>
        </div>

        <div className="p-4 flex justify-center sm:absolute lg:top-4 sm:top-[86px] right-6">
          <div className="flex sm:-mt-10 lg:mt-0 justify-center">
            <img
              src={instaImg}
              onClick={() => {
                window.open("https://www.instagram.com/ourcreativemoment/");
              }}
              className="mx-4 cursor-pointer"
            />
            <img
              src={facebookImg}
              className="mx-4 cursor-pointer"
              onClick={() => {
                window.open(
                  "https://www.facebook.com/profile.php?id=100087915611006"
                );
              }}
            />
          </div>
        </div>

        <div className="flex items-center justify-center text-secondary">
          <span className="text-[14px] mt-2">
            &copy; 2024 Blixa
          </span>
        </div>
      </div>
    </div>
  );
};

export default Footer;
