import React, { useEffect, useState } from 'react'
import Footer from './Footer'
import Header from '../Header/Header'
import { useSelector } from 'react-redux';
import { RootState } from '../../Redux/store';
import { commonAxios } from '../../Axios/service';

const Retur = () => {
  const [listOfProductsSize, setListOfProducts] = useState(0);
  const userDetails = useSelector((state: RootState) => state.user.userDetails);
  const getCartDetails = async () => {
    if (userDetails.id) {
      if (userDetails.role === "ROLE_CUSTOMER") {
        try {
          const response = await commonAxios.get(
            `/cemo/bookingitems/get/cart/${userDetails.id}`
          );
          const listOfProducts = response.data.bookingItemList.filter(
            (item: any) => item.bookingItemType.toLowerCase() === "product"
          );

          if (listOfProducts) {
            setListOfProducts(listOfProducts.length);
          } else {
            setListOfProducts(0)
          }
        } catch (error) {
        }
      }
    } else {
      const listOfProducts = JSON.parse(localStorage.getItem("cartListDetails") as string);
      if (listOfProducts) {
        setListOfProducts(listOfProducts.quantity || 0);
      }
    }
  }

  useEffect(() => {
    getCartDetails();
    window.scrollTo(0, 0);
  }, []);

  return (
    <div>
      <Header productCount={listOfProductsSize} />
      <div className='sm:py-5 py-2 px-4 sm:px-10 sm:mx-28'>

        <h1 className="font-bold text-xl text-secondary mt-4 sm:mt-0">OM DU ÅNGRAR DITT KÖP</h1>
        <p className='text-lg mt-2 text-secondary'>Om du ångrar ditt köp kan du lämna tillbaka Varan genom att skicka den till oss. Du måste returnera
          Varan utan onödigt dröjsmål och senast inom 14 dagar efter den dag då du meddelat oss om ditt
          beslut att utnyttja din ångerrätt</p>

        <p className='text-lg mt-4 text-secondary'>Vid retur måste Varan paketeras väl och lämpligen i samma emballage som den levererades i för att
          minimera risken för transportskador. Varan som returneras ska vara komplett, vilket innebär att alla
          delar, tillbehör, eventuella bruksanvisningar m.m. ska återsändas till oss. Om du hanterat Varan utöver
          vad som är tillåtet eller om någon del av Varan saknas är du ansvarig för varans minskade värde och
          vi förbehåller oss rätten att vid återbetalningen göra avdrag för värdeminskningen.</p>

        <p className='text-lg mt-4 text-secondary'>För att göra en retur behöver du göra följande:</p>
        <div className='sm:mx-20'>
          <p className='text-lg mt-4 text-secondary'>1. Kontakta info@ourcreativemoments.se för att boka en retur</p>
          <p className='text-lg text-secondary'>2. BLIXA mejlar dig en fraktsedel för att märka upp godset</p>
          <p className='text-lg text-secondary'>3. Instruktioner mejlas till dig för hur returen ska hanteras</p>
          <p className='text-lg text-secondary'>4. Så snart Varan tagits emot av BLIXA återbetalas du för Varan enligt våra Villkor</p>
        </div>
        <p className='text-lg mt-4 text-secondary'>Vid returen behöver du ange följande uppgifter:</p>
        <p className='text-lg mt-4 text-secondary'>Namn:</p>
        <p className='text-lg mt-4 text-secondary'>Din adress:</p>
        <p className='text-lg mt-4 text-secondary'>Ordernummer:</p>
        <p className='text-lg mt-4 text-secondary'>Artikelnummer:</p>
        <p className='text-lg mt-4 text-secondary'>Orsak:</p>
        <p className='text-lg mt-4 text-secondary'>Eventuell kommentar:</p>

      </div>
      <Footer />

    </div>
  )
}

export default Retur