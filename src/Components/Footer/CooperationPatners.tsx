import React, { useEffect, useRef, useState } from "react";
import landingImg from "../../assets/hero_image.jpeg";
import Footer from "../../Components/Footer/Footer";
import Header from "../../Components/Header/Header";
import omImage from "../../assets/sweden-marriage.jpg";
import StaderCard from "../../Components/StaderCard/StaderCard";
import staderOne from "../../assets/staderOne.png";
import staderTwo from "../../assets/staderTwo.png";
import staderThree from "../../assets/staderThree.png";
import staderFour from "../../assets/staderFour.png";
import { createSearchParams, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { commonAxios } from "../../Axios/service";

const TermsAndCondition = () => {
    const [listOfProductsSize, setListOfProducts] = useState(0);
    const userDetails = useSelector((state: RootState) => state.user.userDetails);
    const getCartDetails = async () => {
        if (userDetails.id) {
            if (userDetails.role === "ROLE_CUSTOMER") {
                try {
                    const response = await commonAxios.get(
                        `/cemo/bookingitems/get/cart/${userDetails.id}`
                    );
                    const listOfProducts = response.data.bookingItemList.filter(
                        (item: any) => item.bookingItemType.toLowerCase() === "product"
                    );

                    if (listOfProducts) {
                        setListOfProducts(listOfProducts.length);
                    } else {
                        setListOfProducts(0)
                    }
                } catch (error) {
                }
            }
        } else {
            const listOfProducts = JSON.parse(localStorage.getItem("cartListDetails") as string);
            if (listOfProducts) {
                setListOfProducts(listOfProducts.quantity || 0);
            }
        }
    }

    useEffect(() => {
        getCartDetails();
        window.scrollTo(0, 0);
    }, []);

    return (
        <>
            <Header productCount={listOfProductsSize} />
            {/* <div className="md:grid md:grid-cols-2 grid-cols-1 p-2"> */}
            <div className="flex md:flex-row flex-col">
                <div className="w-full px-5 flex sm:mx-28">
                    <p className="text-secondary">

                        <h1 className="order-first font-[sofiapro] font-bold text-[24px] text-secondary py-4">ALLMÄNNA VILLKOR FÖR SAMARBETSPARTERS TILL Blixa</h1>
                        <span>2023-10-03</span><br />
                        <span>Villkoren</span>

                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                1.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA AB, org. nr. 559393–0356, (<span className="font-bold">”BLIXA”</span>) bedriver plattformen ”Blixa”
                                (<span className="font-bold">”Plattformen”</span>) som finns tillgänglig på OurCreativeMoments.se. Plattformen gör det möjligt
                                för företag och privatpersoner (<span className="font-bold">”Kund”</span>) att beställa mat, dryck, livsmedel, tjänster och andra
                                varor (<span className="font-bold">”Produkter”</span>) direkt från de butiker, restauranger och övriga tjänster som är anslutna
                                till Plattformen.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                1.2
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ansluter sig till Plattformen genom att godkänna dessa allmänna villkor
                                (<span className="font-bold">”Villkoren”</span>).
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                1.3
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA och Samarbetspartnern benämns i Villkoren var för sig <span className="font-bold">”Part”</span> och gemensamt
                                <span className="font-bold">”Parterna”</span>.

                                <br />
                            </p>
                        </div>

                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold">
                                1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                SAMARBETET

                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                1.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Parterna ska under samarbetet gemensamt agera lojalt gentemot varandra och inte genom
                                handling eller underlåtenhet agera på ett sätt som kan skada den andre parten eller partens
                                samarbetspartners.

                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                1.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Genom att Samarbetspartnern ansluter sig till Plattformen och gör Produkter tillgängliga på
                                Plattformen godkänner Samarbetspartnern att sälja Produkter, och eventuell leverans som
                                utförs av Samarbetspartnern enligt de beställningar som görs av Kund via Plattformen
                                (<span className="font-bold">”Beställningar”</span>), till BLIXA. BLIXA säljer i sin tur Produkterna och leveransen av desamma till
                                Kund. BLIXA får betalt av Kunden och BLIXA betalar sedan Samarbetspartnern i enlighet med
                                punkt 5 nedan (<span className="font-bold">”Samarbetet”</span>).

                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold">
                                2
                            </p>

                            <br />
                            <p className="mt-2 mr-1 text-secondary font-bold ml-2">
                                BLIXA:S ÅTAGANDEN
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                2.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                Plattformen
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.1.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA ger Samarbetspartnern tillgång till Plattformen, på sådant sätt som krävs för att
                                Samarbetspartnern ska kunna uppfylla sina skyldigheter enligt dessa Villkor, när
                                Samarbetspartnern ansluter sig till Plattformen.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.1.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA informerar Samarbetspartnern om Plattformen och hur Plattformen används, på sådant
                                sätt som krävs för att Samarbetspartnern ska kunna uppfylla sina skyldigheter enligt dessa
                                Villkor, när Samarbetspartnern ansluter sig till Plattformen.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.1.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA garanterar inte tillgängligheten eller funktionaliteten av Plattformen eller eventuell
                                teknisk utrustning som tillhandahålls och ansvarar inte för avbrott av densamma under
                                Samarbetet. BLIXA är inte ersättningsskyldig gentemot Samarbetspartnern för eventuella
                                driftavbrott eller brister/fel i Plattformen.

                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.1.4
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4 ">
                                BLIXA har rätt att begränsa antalet Produkter som görs tillgängliga på Plattformen av
                                Samarbetspartnern. BLIXA kan också begränsa eller avvisa Produkter på Plattformen som är
                                olagliga eller som enligt BLIXAs uppfattning inte anses stämma överens med BLIXAs
                                värdegrunder.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.1.5
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA har rätt att ta bort eller redigera material som enligt BLIXA är kränkande eller stötande,
                                bryter mot lag, marknadsför annan leverantörs beställningsplattform eller som i övrigt saknar
                                koppling till Plattformens användningsområden.

                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                2.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-0.5">
                                Beställning
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.2.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA ansvarar inte för att vissa Beställningar kanvara felaktiga eller falska.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.2.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                I samband med en Beställning ställer BLIXA ut kvitto eller faktura till Kunden i egenskap av
                                säljare av Produkterna. Samarbetspartnern ska därmed inte tillhandahålla Kunden kvitto
                                eller faktura i samband med en Beställning.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold">
                                3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                SAMARBETSPARTNERNS ÅTAGANDEN
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                3.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                Beställning
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.1.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ansvarar för att ange korrekt och uppdaterad information om vilka dagar
                                och tider som Samarbetspartnern kan ta emot Beställningar.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.1.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ansvarar för att leverera den eller de Produkter som framgår av Kundens
                                Beställning och som BLIXA har godkänt. Genom att acceptera en Beställning ingår
                                Samarbetspartnern ett bindande avtal med BLIXA avseende köpet av Produkten eller
                                Produkterna samt eventuella beställda leveranstjänster. Bindande avtal för BLIXA i
                                förhållande till Samarbetspartnern uppkommer först när BLIXA godkänt Beställningen.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.1.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ska inom 24 timmar från det att Beställningen mottagits av
                                Samarbetspartnern bekräfta densamma. Om en Produkt inte finns tillgänglig måste
                                Samarbetspartnern omedelbart avböja Beställningen och markera Produkten som
                                slutsåld/otillgänglig på Plattformen.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.1.4
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                För det fall Samarbetspartnern tillhandahåller leverans för beställningar ska
                                Samarbetspartnern efter mottagen Beställning förbereda, paketera och leverera avtalad
                                Beställning direkt till Kund i enlighet med den tid som framgår av Beställningen.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.1.5
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                För det fall Kunden via Plattformen valt att hämta upp Beställningen direkt hos
                                Samarbetspartnern ska Samarbetspartnern paketera och tillgängliggöra Produkterna för
                                avhämtning av Kund i enlighet med den tid som framgår av Beställningen.
                                Samarbetspartnern ansvarar för att Produkterna är förpackade på ett lämpligt sätt.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                3.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-1">
                                Marknadsföring av produkter
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.2.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern åtar sig att under Samarbetet tillgängliggöra aktuellt sortiment via
                                Plattformen.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20 ">
                                3.2.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ska på dagen för godkännandet av Villkoren tillhandahålla den
                                information om Samarbetspartnern som ligger till grund för Samarbetet på Plattformen.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.2.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ansvarar vid var tid för informationens fullständighet och korrekthet och
                                åtar sig att meddela BLIXA om eventuella förändringar av sådan information utan dröjsmål.
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                3.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                Sortimentet
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.3.1
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ska förse BLIXA med vid var tid uppdaterad och korrekt information om
                                Samarbetsparterns aktuella utbud av produkter (<span className="font-bold">”Sortiment”</span>) inklusive rekommenderade
                                försäljnings- och jämförelsepriser (<span className="font-bold">”Prislista”</span>).
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.3.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Vid eventuella uppdateringar av Samarbetspartnerns Sortiment och Prislista ska
                                Samarbetspartnerns omedelbart meddela BLIXA. BLIXA ansvarar inte för riktigheten avseende
                                information om Samarbetspartnerns Sortiment som visas på Plattformen.
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.3.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern åtar sig att vid var tid hålla tillräcklig mängd av Produkterna som erbjuds
                                enligt Sortiment tillgängligt för Beställningar på Plattformen, samt att Produkterna är i gott
                                skick, inklusive avseende bäst-före-datum i den mån tillämpligt.
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                3.4
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                Utrustning
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.4.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ska under Avtalstiden inneha den utrustning som krävs för att hantera
                                Plattformen samt tillse att samtlig personal är införstådd med hur BLIXAs tekniska system och
                                programvaror fungerar.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.4.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Om det uppstått tekniska problem eller annat problem med Plattformen som medför att
                                Samarbetspartnern inte har möjlighet att fullgöra sina åtaganden enligt Avtalet ska
                                Samarbetspartnern omedelbart meddela BLIXA om det.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.4.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern åtar sig att vara tillgänglig per telefon eller chatt så att BLIXA alltid kan
                                kontakta Samarbetspartnern om frågor eller problem uppstår.
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                3.5
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2" >
                                Tillstånd m.m.
                                <br />
                            </p>
                        </div>


                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.5.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4 ">
                                Samarbetspartnern ansvarar för att Samarbetspartnerns verksamhet bedrivs i enlighet med
                                vid var tid gällande och tillämpliga lagar, förordningar och andra regelverk, inklusive
                                myndighetsföreskrifter- och råd (<span className="font-bold">“Gällande Regelverk”</span>). Samarbetspartnern ska under
                                Samarbetet inneha samtliga tillstånd, licenser och registreringar (<span className="font-bold">”Tillstånd”</span>) som är
                                tillämpliga för den verksamhet som Samarbetspartnern bedriver. Vidare ska
                                Samarbetspartnern under Samarbetet vara godkänd för F-skatt och momsregistrerad.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.5.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ansvarar för att Samarbetspartnerns Produkter och marknadsföringen av
                                dessa uppfyller Gällande Regelverk.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.5.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ansvarar för att vid var tid hålla BLIXA informerad om gällande lagar,
                                förordningar och andra tillämpliga regleringar avseende försäljning och leverans av de
                                Produkter som görs tillgängliga av Samarbetspartnern på Plattformen, i den mån dessa
                                förpliktigar BLIXA inom ramen för Villkoren.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                3.6
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                Särskilt om vissa produkter
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                1.3.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern åtar sig att löpande informera BLIXA om sina rutiner avseende försäljning
                                av Produkter för vilka åldersgränser gäller enligt lag eller för vilka Samarbetspartnern i övrigt
                                tillämpar åldersgränser.
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                1.3.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern åtar sig att informera BLIXA om särskilda skyldigheter föreligger vid
                                BLIXAs leverans av vissa Produkter till Kund i enlighet med gällande lagar, förordningar och
                                andra tillämpliga regleringar. BLIXA äger rätt att neka leverans av Produkt om uppfyllande av
                                sådan skyldighet medför oskäligt betungande eller kostsamma anpassningar för BLIXA och
                                har rätt att returnera varor som inte levereras i enlighet med denna punkt 4.6.2 eller som
                                annars avtalats mellan Parterna.
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                3.7
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                Reklamation och returer
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                1.3.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ansvarar för att Produkten är fri från fel och i övrigt i avtalat skick, samt
                                att eventuell leverans av Produkten genomförs i enlighet med avtalad Beställning.
                                Samarbetspartnern ansvarar för att ta emot och hantera reklamationer och returer i tre (3) år
                                från datumet för Beställning och/eller separat överenskommelse mellan Parterna.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                1.3.4
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ska kompensera BLIXA för alla kostnader, inklusive leveranskostnader, till
                                följd av felaktiga Produkter tillhandahållna av Samarbetspartnern. Samarbetspartnern är
                                medveten om att BLIXA i förhållande till Kunden kan ha ett ansvar enligt konsumenträttsliga
                                regler och att Samarbetspartnerns ersättningsskyldighet därför även omfattar BLIXA:s ansvar
                                enligt nyss nämnda regler.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                3.8
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                Brist i åtagande
                                <br />
                            </p>
                        </div>
                        <div className="flex ">

                            <p className="mt-2 mr-4 text-secondary ml-14">
                                Om Samarbetspartnern brister mot sina åtaganden i denna punkt 4 har BLIXA rätt att ge
                                Samarbetspartnern en varning, och vid upprepande gånger, stänga av Samarbetspartnern
                                från Plattformen eller om BLIXA så väljer, säga upp Avtalet enligt punkt 9.2 nedan.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold">
                                4
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                PRISER OCH ERSÄTTNING
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                4.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                Pris och prissättning
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                4.1.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ska tillhandahålla BLIXA en prislista med rekommenderade priser mot
                                slutkund (<span className="font-bold">”Prislista”</span>). BLIXA ska av Samarbetspartnern köpa in Produkterna till ett pris som
                                motsvarar 80 procent av priserna i Prislistan.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                4.1.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ska se till att deras rekommenderade priser i Prislistan motsvarar de
                                priser som Samarbetspartnern själv erbjuder slutkunden vid beställningar direkt från
                                Samarbetspartnern via Samarbetspartnerns webbplats eller mobilapp, eller annan plattform
                                som drivs av Samarbetspartnern.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                4.1.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Eventuella kostnader för frakt ingår i priserna som anges i Prislistan.
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                4.1.4
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA har alltid rätten att själv bestämma priserna för Produkterna mot Kund.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                4.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                Avgift
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                4.2.1
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Vid av Samarbetspartnern upprepade misstag, bristfällig leverans eller felaktig hantering har
                                BLIXA rätt att applicera Sanktionsavgifter i enlighet med följande.
                                <br />
                                <div className="flex ">
                                    <p className="mt-2 mr-4 text-secondary">
                                        (a)
                                    </p>

                                    <br />
                                    <p className="mt-2 mr-4 text-secondary ">
                                        Vid två (2) eller fler Beställningar för vilka fel eller misstag förelegat kan en
                                        Sanktionsavgift om 10 000 kronor tas ut:
                                        <br />
                                    </p>

                                </div>
                                <div className="ml-12">
                                    <div className="flex ">
                                        <p className="mt-2 mr-4 text-secondary ">
                                            (i)
                                        </p>

                                        <br />
                                        <p className="mt-2 mr-4 text-secondary ">
                                            Felaktig Beställning inklusive fel eller felaktig Produkt;
                                            <br />
                                        </p>

                                    </div>
                                    <div className="flex ">
                                        <p className="mt-2 mr-4 text-secondary ">
                                            (ii)
                                        </p>

                                        <br />
                                        <p className="mt-2 mr-4 text-secondary ">
                                            Produkt med kort eller passerat bäst-före-datum;
                                            <br />
                                        </p>

                                    </div>
                                    <div className="flex ">
                                        <p className="mt-2 mr-4 text-secondary">
                                            (iii)
                                        </p>

                                        <br />
                                        <p className="mt-2 mr-4 text-secondary ">
                                            Bortglömda eller saknade Produkter;
                                            <br />
                                        </p>

                                    </div>
                                    <div className="flex ">
                                        <p className="mt-2 mr-4 text-secondary">
                                            (iv)
                                        </p>

                                        <br />
                                        <p className="mt-2 mr-4 text-secondary ">
                                            Otillräckliga förpackningar.
                                            <br />
                                        </p>

                                    </div>
                                </div>
                                <div className="flex ">
                                    <p className="mt-2 mr-4 text-secondary">
                                        (b)
                                    </p>

                                    <br />
                                    <p className="mt-2 mr-4 text-secondary ">
                                        Vid försening av leverans orsakad av Samarbetspartnern vid två (2) eller fler
                                        beställningar kan en sanktionsavgift om 10 000 kronor tas ut.
                                        <br />
                                    </p>

                                </div>
                                <div className="flex ">
                                    <p className="mt-2 mr-4 text-secondary ">
                                        (c)
                                    </p>

                                    <br />
                                    <p className="mt-2 mr-4 text-secondary ">
                                        Vid två (2) eller fler avböjda beställningar på grund av otillgänglig produkt kan en
                                        Sanktionsavgift om 10 000 kronor tas ut.
                                        <br />
                                    </p>

                                </div>
                                <div className="flex ">
                                    <p className="mt-2 mr-4 text-secondary">
                                        (d)
                                    </p>

                                    <br />
                                    <p className="mt-2 mr-4 text-secondary ">
                                        Vid två (2) eller fler avböjda beställningar på grund av Samarbetspartnerns
                                        otillgänglighet kan en Sanktionsavgift om 10 000 kronor tas ut.
                                        <br />
                                    </p>

                                </div>
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                4.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                Betalning
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                1.3.5
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Betalning från BLIXA till Samarbetspartnern sker den sista dagen nästkommande månad efter
                                att BLIXA erhållit betalning från Kund avseende den aktuella Beställningen.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                1.3.6
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Om angivet utbetalningsdatum inte är en bankdag sker utbetalningen närmaste bankdag
                                efter ovan angivet utbetalningsdatum.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold">
                                5
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                SEKRETESS
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary sm:ml-20">
                                5.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Parterna förbinder sig gentemot den andra Parten att under och efter Samarbetet och ett (1)
                                år därefter iaktta strikt sekretess beträffande innehållet i detta Avtal och att inte avslöja
                                konfidentiell information rörande all information om den andra Parten eller dennes
                                närstående företag, deras ekonomi, verksamhet, riktlinjer eller planer och andra
                                företagshemligheter (<span className="font-bold">”Konfidentiell information”</span>) samt att inte själv använda eller för tredje
                                part avslöja sådan Konfidentiell information om Part. Parterna ska tillse att anställda, anlitade
                                konsulter och styrelseledamöter hos respektive Part iakttar ovanstående
                                sekretessbestämmelse.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                5.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Ovanstående begränsningar gäller inte information som:
                                <br />
                                <div className="flex ">
                                    <p className="mt-2 mr-4 text-secondary">
                                        (a)
                                    </p>

                                    <br />
                                    <p className="mt-2 mr-4 text-secondary ">
                                        är eller blir allmänt känd på annat sätt än genom brott mot Villkoren;
                                        <br />
                                    </p>

                                </div>
                                <div className="flex ">
                                    <p className="mt-2 mr-4 text-secondary">
                                        (b)
                                    </p>

                                    <br />
                                    <p className="mt-2 mr-4 text-secondary ">
                                        den mottagande Parten bevisligen har kännedom om redan innan man får den av den
                                        andra Parten;
                                        <br />
                                    </p>

                                </div>
                                <div className="flex ">
                                    <p className="mt-2 mr-4 text-secondary">
                                        (c)
                                    </p>

                                    <br />
                                    <p className="mt-2 mr-4 text-secondary ">
                                        den mottagande Parten bevisligen erhållit från tredje man utan sekretessåtagande;
                                        eller
                                        <br />
                                    </p>

                                </div>
                                <div className="flex ">
                                    <p className="mt-2 mr-4 text-secondary">
                                        (d)
                                    </p>

                                    <br />
                                    <p className="mt-2 mr-4 text-secondary ">
                                        Part är skyldig att lämna ut enligt börs- eller noteringregler som Parten är bunden av.
                                        <br />
                                    </p>

                                </div>
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold">
                                2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                IMMATERIELLA RÄTTIGHETER
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                2.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                Rättigheter
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.1.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary">
                                BLIXA innehar samtliga rättigheter inklusive immateriella rättigheter till varumärket Our
                                Creative Moments, Plattformen, dess tekniska utrustning och däri ingående programvaror.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.1.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Villkoren ska inte tolkas så att det innebär att äganderätt, titel, upphovsrätt eller andra
                                immateriella rättigheter överlåts till Samarbetspartnern. Samarbetspartnern har inte rätt att,
                                utöver vad som skriftligen medges av BLIXA, använda, kopiera, ändra eller på annat sätt
                                hantera immateriella rättigheter som tillhör BLIXA, inte heller överföra, överlåta eller upplåta
                                rätt till sådana immateriella rättigheter.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold ml-6">
                                2.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                Marknadsföring
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.2.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA har rätt, men inte skyldighet, att vid marknadsföring av Plattformen använda
                                Samarbetspartnerns namn och varumärke.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.2.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA har rätt att nyttja texter, logotyper, bildmaterial och annat material som tillgängliggörs
                                av Samarbetspartnern för publicering på Plattformen eller annars för marknadsföring i
                                enlighet med Villkoren. Äganderätten till materialet som tillhandahålls av Samarbetspartnern
                                kvarstannar dock hos Samarbetspartnern. Samarbetspartnern ansvarar för att säkerställa att
                                Samarbetspartnerns material inte gör intrång i tredje mans rätt eller på annat sätt står i strid
                                med gällande lagstiftning och ska ersätta och hålla BLIXA skadeslöst i förhållande till alla
                                kostnader eller anspråk, inklusive legala kostnader, till följd av intrång i tredje mans rätt till
                                patent eller immateriella äganderättigheter eller vid strid mot gällande lagstiftning.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.2.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA ska i förväg godkänna allt marknadsförings- och skyltningsmaterial relaterat till
                                Samarbetet som framtagits av Samarbetspartnern innefattande, men inte begränsat till,
                                bilder, texter, banners, annonser, skyltar, tidningsartiklar, reklamfilmer etc., oavsett om
                                sådant material är skriftligt eller digitalt. BLIXAs godkännande befriar inte Partner från sin
                                skyldighet att följa vid var tid gällande lagar och förordningar, inklusive men inte begränsat till
                                marknadsföringslagstiftning, lagar avseende namn och bild i reklam och
                                upphovsrättslagstiftning.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold">
                                3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                PERSONUPPGIFTER OCH TILLGÅNG TILL DATA
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Utöver den information som framgår på Plattformen kan Kunden informeras om
                                Samarbetspartnerns namn, adress, momsnummer och kontaktuppgifter i kvitto,
                                orderbekräftelse eller på annat sätt.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA får i samband med Samarbetspartnerns användning av Plattformen tillgång till
                                personuppgifter som hör till Samarbetspartnerns anställda samt övriga data hänförlig till
                                Samarbetspartnern och dess användning av Plattformen såsom orderinformation och
                                Sortiment. BLIXA behandlar personuppgifter i enlighet med BLIXAs vid var tid gällande
                                integritetspolicy.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                5.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Vid fullgörandet av Villkoren och i syfte att utföra Beställningar får Samarbetspartnern
                                tillgång till Kunders personuppgifter, såsom exempelvis Kundernas namn, telefonnummer
                                och adress eller annan information som tillhandahålls av Kunder och som anses nödvändiga
                                för att leverera Beställningen. Samarbetspartnern får endast använda personuppgifterna i
                                syfte att utföra Beställningarna. En förutsättning för BLIXA att dela sådana personuppgifter
                                med Samarbetspartnern är att Samarbetspartnern endast använder personuppgifterna i
                                syfte att utföra Beställningarna. Samarbetspartnern är införstådd med att den är
                                personuppgiftsansvarig för sin behandling av personuppgifter inom ramen för Samarbetet
                                och ansvarar för att agera i enlighet tillämplig dataskyddslagstiftning. BLIXA är
                                personuppgiftsansvarig för sin behandling av personuppgifter inom ramen för Samarbetet.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                5.4
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern åtar sig härmed att ersätta och hålla BLIXA skadeslös mot alla krav och
                                anspråk från myndigheter eller tredje part, inklusive legala kostnader och verkställande av
                                denna punkt 11 , till följd av att Samarbetspartnern eller någon anställd, ombud eller
                                underleverantör till Samarbetspartnern bryter mot tillämplig dataskyddslagstiftning.
                                <br />
                            </p>
                        </div>

                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold">
                                6
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                VILLKORENS GILTIGHETSTID OCH VILLKORENS UPPHÖRANDE
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                6.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Villkoren gäller från det att Samarbetspartnern godkänt dessa Villkor och löper tills vidare
                                med en uppsägningstid om en (1) månad.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                6.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Utan hinder för vad som framgår av punkt 9.1 har BLIXA rätt att säga upp Villkoren med
                                omedelbar verkan om:
                                <br />
                                <div className="flex ">
                                    <p className="mt-2 mr-4 text-secondary ">
                                        (a)
                                    </p>

                                    <br />
                                    <p className="mt-2 mr-4 text-secondary ">
                                        Samarbetspartnern brutit mot Villkoren, till exempel genom att återkommande brustit i
                                        beställning, kvalitet, leverans eller service, och inte åtgärdat det inom tre (3) dagar
                                        efter att BLIXA påpekat det;
                                        <br />
                                    </p>

                                </div>
                                <div className="flex ">
                                    <p className="mt-2 mr-4 text-secondary ">
                                        (b)
                                    </p>

                                    <br />
                                    <p className="mt-2 mr-4 text-secondary ">
                                        Samarbetspartnern börjar samarbeta med annan likande leverantör av
                                        beställningsplattformen online;
                                        <br />
                                    </p>

                                </div>
                                <div className="flex ">
                                    <p className="mt-2 mr-4 text-secondary">
                                        (c)
                                    </p>

                                    <br />
                                    <p className="mt-2 mr-4 text-secondary ">
                                        Samarbetspartnern försätts i konkurs, ingår ackord, träder i likvidation eller på annat
                                        sätt befinns vara insolvent eller inte längre innehar nödvändiga tillstånd för sin
                                        verksamhet; eller
                                        <br />
                                    </p>

                                </div>
                                <div className="flex ">
                                    <p className="mt-2 mr-4 text-secondary">
                                        (d)
                                    </p>

                                    <br />
                                    <p className="mt-2 mr-4 text-secondary ">
                                        Samarbetspartnern har överlåtit verksamheten eller försäljningstillfällen.
                                        <br />
                                    </p>

                                </div>

                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                6.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Uppsägning ska ske skriftligen.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold">
                                7
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                ANSVAR
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                7.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern är säljare av Produkterna (till BLIXA) och ansvarig för Produkternas
                                kvalitet, beskaffenhet och egenskaper i övrigt till dess att Produkterna levererats till Kund.
                                Samarbetspartnern är ansvarig för fel i Produkten och skada som BLIXA, Kund och/eller
                                annan tredje part lider till följd av försålda Produkter via Plattformen, i enlighet med dessa
                                Villkor och gällande produktansvarslagstiftning.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                7.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern ska försvara och hålla BLIXA skadeslöst för alla krav, skadestånd,
                                sanktionsavgifter, kostnader och utgifter (inklusive skäliga kostnader för anlitande av juridiskt
                                ombud) som BLIXA åsamkas till följd av att Samarbetspartnern brutit mot bestämmelse i
                                Villkoren eller gällande lag eller förordning.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold">
                                8
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                FORCE MAJEURE
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-14">
                                Part är befriad från påföljd för underlåtenhet att fullgöra viss förpliktelse enligt dessa Villkor
                                om underlåtenheten har sin grund i omständighet som ligger utanför Parts kontroll och som
                                avsevärt försvårar eller försenar fullgörandet därav. Sådan omständighet kan vara ändrad
                                lagstiftning, strejk, blockad, brand, översvämning, pandemi, annan allvarlig smittspridning
                                eller liknande händelse. Part som påkallar befrielse på grund av sådan händelse ska utan
                                dröjsmål underrätta andra Parten därom.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold">
                                9
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-2">
                                ÖVRIGT
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                9.1
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern har inte rätt att överlåta sina rättigheter och skyldigheter enligt dessa
                                Villkor till annan part utan BLIXAs skriftliga samtycke. Sker överlåtelse utan BLIXAs samtycke
                                har BLIXA rätt att säga upp Samarbetet med omedelbar verkan.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                9.2
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Villkoren utgör parternas fullständiga reglering av de frågor som villkoren berör.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                9.3
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA har rätt att göra ändringar av och/eller tillägg till Villkoren genom att informera Parten
                                om sådana ändringar. BLIXA har även rätt att när som helst justera de Avgifter som anges i
                                punkt 5.2 ovan.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold">
                                10
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-0.5">
                                TILLÄMPLIG LAG OCH TVIST
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                10.1
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Villkoren ska tolkas i enlighet med svensk rätt.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                10.2
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary  ml-4">
                                Tvist som uppstår mellan Parterna med anledning av Villkoren ska slutligt avgöras genom
                                skiljedomsförfarande administrerat vid Stockholm Handelskammares Skiljedomsinstitut.
                                Skiljeförfarandets säte ska vara Örebro.
                                <br />
                                <br />
                            </p>
                        </div>
                    </p>
                </div>
            </div>


            <Footer />
        </>
    );
};
export default TermsAndCondition;
