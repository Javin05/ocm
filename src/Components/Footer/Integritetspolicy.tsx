import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import integritetspolicy from "../../assets/Integritetspolicy .jpg";
import { commonAxios } from '../../Axios/service';
import { RootState } from '../../Redux/store';
import Header from '../Header/Header';
import Footer from './Footer';


const Integritetspolicy = () => {
    const [listOfProductsSize, setListOfProducts] = useState(0);
    const userDetails = useSelector((state: RootState) => state.user.userDetails);
    const getCartDetails = async () => {
        if(userDetails.id){
        if (userDetails.role === "ROLE_CUSTOMER") {
          try {
            const response = await commonAxios.get(
              `/cemo/bookingitems/get/cart/${userDetails.id}`
            );
            const listOfProducts = response.data.bookingItemList.filter(
              (item: any) => item.bookingItemType.toLowerCase() === "product"
            );
    
            if (listOfProducts){
              setListOfProducts(listOfProducts.length);
            }else{
              setListOfProducts(0)
            }
          } catch (error) {
          }
        }
      } else {
        const listOfProducts = JSON.parse(localStorage.getItem("cartListDetails") as string);
        if (listOfProducts){
          setListOfProducts(listOfProducts.quantity || 0);
        }
      }
    }

    useEffect(() => {
        getCartDetails();
        window.scrollTo(0, 0);
    }, []);

    return (
        <>
        <Header productCount={listOfProductsSize}/>
            <div className='flex justify-center'>
                <img src={integritetspolicy} className="md:w-[80%]" />
            </div>
        <Footer />
        </>


    )
}

export default Integritetspolicy