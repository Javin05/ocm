import React, { useEffect, useRef, useState } from "react";
import landingImg from "../../assets/hero_image.jpeg";
import Footer from "../../Components/Footer/Footer";
import Header from "../../Components/Header/Header";
import omImage from "../../assets/sweden-marriage.jpg";
import StaderCard from "../../Components/StaderCard/StaderCard";
import staderOne from "../../assets/staderOne.png";
import staderTwo from "../../assets/staderTwo.png";
import staderThree from "../../assets/staderThree.png";
import staderFour from "../../assets/staderFour.png";
import { createSearchParams, useNavigate } from "react-router-dom";
import { commonAxios } from "../../Axios/service";
import { RootState } from "../../Redux/store";
import { useSelector } from "react-redux";

const TermsAndCondition = () => {
    const [listOfProductsSize, setListOfProducts] = useState(0);
    const userDetails = useSelector((state: RootState) => state.user.userDetails);
    const getCartDetails = async () => {
        if (userDetails.id) {
            if (userDetails.role === "ROLE_CUSTOMER") {
                try {
                    const response = await commonAxios.get(
                        `/cemo/bookingitems/get/cart/${userDetails.id}`
                    );
                    const listOfProducts = response.data.bookingItemList.filter(
                        (item: any) => item.bookingItemType.toLowerCase() === "product"
                    );

                    if (listOfProducts) {
                        setListOfProducts(listOfProducts.length);
                    } else {
                        setListOfProducts(0)
                    }
                } catch (error) {
                }
            }
        } else {
            const listOfProducts = JSON.parse(localStorage.getItem("cartListDetails") as string);
            if (listOfProducts) {
                setListOfProducts(listOfProducts.quantity || 0);
            }
        }
    }

    useEffect(() => {
        getCartDetails();
        window.scrollTo(0, 0);
    }, []);

    return (
        <>
            <Header productCount={listOfProductsSize} />
            <div className="flex md:flex-row flex-col">
                <div className="w-full px-5 flex sm:mx-28">
                    <p className="text-secondary">

                        <h1 className="order-first font-[sofiapro] font-bold text-[24px] text-secondary  mb-2 mt-10">ALLMÄNNA VILLKOR FÖR Blixa</h1>

                        Villkoren uppdaterades den 2023-10-03
                        <br />
                        Villkoren
                        <br />
                        <div className="flex">
                            <p className="mt-2 sm:mr-4 text-secondary sm:ml-20">
                                1.1
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Dessa allmänna villkor (<span className="font-bold">"Villkoren”</span>) gäller mellan BLIXA AB, org.nr. 559393–0356, (”BLIXA”)
                                och dig som kund (<span className="font-bold">”Kund”</span>) vid köp av produkter (<span className="font-bold">”Varor”</span>) och tjänster (<span className="font-bold">”Tjänster”</span>) via
                                OurCreativeMoments.se (”Plattformen”).
                                <br />
                            </p>
                        </div>
                        <div className="flex">
                            <p className="mt-2 sm:mr-4 text-secondary sm:ml-20">
                                1.2
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Genom att registrera ett användarkonto hos BLIXA, använda Plattformen och/eller köpa Varor
                                och/eller Tjänster via Plattformen, bekräftar du att du har läst och godkänner Villkoren. Du
                                intygar också att du kan godkänna Villkoren med bindande verkan. Villkoren ska,
                                tillsammans med Kundens order och orderbekräftelse, utgöra avtalet mellan BLIXA och
                                Kunden (<span className="font-bold">”Avtalet”</span>).
                                <br />
                            </p>
                        </div>
                        <div className="flex">
                            <p className="mt-2 sm:mr-4 text-secondary sm:ml-20">
                                1.3
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA förbehåller sig rätten att uppdatera Villkoren från tid till annan, och genom att lägga en
                                beställning godkänner du de vid var tid gällande Villkoren.

                                <br />
                            </p>
                        </div>
                        <div className="flex">
                            <p className="mt-2 sm:mr-4 text-secondary sm:ml-20">
                                1.4
                            </p>
                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA förbehåller sig rätten att överlåta sina rättigheter och skyldigheter enligt Villkoren till
                                tredje part, förutsatt att den övertagande tredje parten kan förväntas fullgöra sina
                                skyldigheter enligt Villkoren på ett för kunden tillfredsställande sätt.

                                <br />
                            </p>
                        </div>
                        <br />
                        <h1 className="order-first font-[sofiapro] font-bold text-[16px] text-secondary sm:p-4 py-4 sm:ml-12">KONTAKTUPPGIFTER</h1>
                        <h1 className="order-first font-[sofiapro] font-bold text-[16px] text-secondary sm:p-2 py-2 sm:ml-14">FÖR ATT FÅ SVAR PÅ FRÅGOR KRING VILLKOREN, PLATTFORMEN, VAROR,
                            TJÄNSTER, LEVERANS- OCH FRAKTVILLKOR, FÖR ATT ÅNGRA ELLER
                            REKLAMERA ETT KÖP ELLER FRAMFÖRA ETT KLAGOMÅL ELLER ANDRA
                            SYNPUNKTER SKA DU KONTAKTA BLIXA PÅ FÖLJANDE KONTAKTUPPGIFTER:</h1>

                        <p className="mt-2 mr-4 text-secondary sm:ml-16">
                            BLIXA AB, org.nr. 559393–0356
                        </p>
                        <p className="mt-2 mr-4 text-secondary sm:ml-16">
                            Attersta 315
                        </p>
                        <p className="mt-2 mr-4 text-secondary sm:ml-16">
                            705 94 Örebro
                        </p>
                        <p className="mt-2 mr-4 text-secondary sm:ml-16">
                            Telefon: 070-5200260
                        </p>
                        <p className="mt-2 mr-4 text-secondary sm:ml-16">
                            E-post: info@ourcreativemoments.se
                        </p>
                        <br />
                        <div className="flex">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                PLATTFORMEN OCH PARTERS

                                <br />
                            </p>
                        </div>
                        <div className="flex">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                2.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA driver Plattformen och administrerar beställningar som görs via Plattformen. Via
                                Plattformen kan du som Kund beställda de Varor och Tjänster som BLIXA, via sina
                                samarbetspartnerns, vid var tid håller tillgängliga på Plattformen. BLIXA:s samarbetspartnerns
                                består av restauranger, butiker och entreprenörer som säljer sina Varor och Tjänster genom
                                BLIXA och BLIXA:s Plattform (<span className="font-bold">”Samarbetspartners”</span>).

                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                BESTÄLLNING
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                För att genomföra en beställning lägger du den eller de Varor och/eller Tjänster som du vill
                                köpa i din varukorg. När du trycker på ”lägg beställning” lämnar du ett erbjudande till BLIXA
                                att köpa de Varor och/eller Tjänster som är i din varukorg. Efter genomförd beställning får du
                                ett mejl som bekräftar att BLIXA mottagit din beställning. BLIXA har 24 timmar på sig att
                                antingen acceptera eller avböja din beställning. När BLIXA har accepterat din beställning
                                skickas du till kassan där du betalar för din beställning. När du har betalt för din beställning,
                                eller valt att betala din beställning genom faktura, får du en orderbekräftelse och
                                samarbetspartnern påbörjar utförandet av din beställning. Du betalar alltså för din beställning
                                direkt via Plattformen. Se vidare om hur du betalar för din beställning i punkt 6 6 nedan.

                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                Du ansvarar för att de uppgifter som du lämnar vid registrering av ditt användarkonto hos
                                BLIXA, eller vid din beställning av Varor och/eller Tjänster, är korrekta.

                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                Du har möjlighet att återkalla din beställning till dess att du fått en orderbekräftelse. Efter att
                                beställningen har accepterats av BLIXA hänvisar vi till reglerna för ångerrätt enligt punkt 9
                                nedan.

                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                3.4
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                Information om Varorna, Tjänsterna och priser lämnas med reservation för tryck- och
                                skrivfel, prishöjningar och slutförsäljning, i den utsträckning som det är tillåtet enligt gällande
                                lagstiftning.

                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                4
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                PRISER
                                <br />
                            </p>
                        </div>
                        <div className="flex ">

                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                Information om Varorna, Tjänsterna och priser lämnas med reservation för tryck- och
                                skrivfel, prishöjningar och slutförsäljning, i den utsträckning som det är tillåtet enligt gällande
                                lagstiftning.

                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                5
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                BETALNINGSVILLKOR
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                5.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA erbjuder Kunden att betala med bankkort, kreditkort eller de andra betalningsmetoder
                                som erbjuds på Plattformen vid beställningstillfället. BLIXA förbehåller sig rätten att inte
                                erbjuda samtliga betalningsmetoder för alla beställningar samt att genomföra en
                                kreditkontroll innan fakturaköp medges.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                5.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                BLIXA tillhandahåller en extern betaltjänstleverantör genom vilken alla betalningar processas
                                genom. Genom köp via Plattformen godkänner du, utöver dessa Villkor, den externa
                                betaltjänstleverantörs villkor. Besök den externa betaltjänsteleverantörens hemsida för mer
                                information.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                6
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                LEVERANS
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                6.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                BLIXA tillhandhåller olika leveransalternativ och det är BLIXA:s Samarbetspartner som utför
                                leveransen. Leveranslaternativ för den aktuella ordern, leveranskostand och datum för
                                leverans framgår för varje enskild beställning. BLIXA reserverar sig för att beräknad
                                leveranstid och datum utgör uppskattade tider och att uppskattade leveranstider kan komma
                                att påverkas av orsaker som är utanför BLIXA:s eller Samarbetspartnerns kontroll.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                6.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                BLIXA reserverar sig för att en beställning av flera Varor kan komma att levereras vid olika
                                tillfällen.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                6.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                När du gör en beställning med leverans bekräftar och godkänner du att de beställda Varorna
                                kommer att levereras till den plats eller gatuadress som du har angett för beställningen.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                6.4
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                För beställningar som inkluderar Varor med legitimationskrav kan det krävas att
                                beställningen tas emot av dig som gör beställningen.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                6.5
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                Om du beställt en Tjänst kommer den Samarbetspartnern som ska utföra Tjänsten att
                                kontakta dig via de kontaktuppgifter som du angett vid beställningen för att schemalägga när
                                och var Tjänsterna ska tillhandahållas (om det inte anges vid beställningstillfället).
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                6.6
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                Risken för förlust av, eller skada på, Varan övergår på dig när Varan har levererats på
                                överenskommen plats. BLIXA är inte ansvariga för eventuell förlust eller skada på Varor som
                                uppstår efter leverans.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                7
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                HÄMTA DIN BESTÄLLNING
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                7.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Du kan vid beställningstillfället välja om du vill hämta upp din Vara/Varor direkt hos den
                                Samarbetspartnern som Varan/Varorna kommer ifrån. Varan/Varorna ska i sådant fall
                                hämtas vid den plats och tidpunkt som anges vid beställningstillfället.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                7.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Samarbetspartnern kan kräva att du identifierar dig med giltig legitimation när du hämtar upp
                                dina Varor. De gäller alla Varor som kräver ID-kontroll eller för vilka åldersbegränsningar
                                eller andra krav gäller.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                7.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-4">
                                Om du inte hämtar din beställning inom angiven tidsperiod eller på angiven plats kan du
                                debiteras fullt pris för Varorna i den mån de inte kan säljas på nytt och/eller för andra
                                kostnader som BLIXA haft i samband med beställningen.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                8
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                TRANSPORTSKADOR
                                <br />
                            </p>
                        </div>
                        <div className="flex ">

                            <br />
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                Du ska kontrollera Varan vid mottagandet av densamma och därvid kontrollera om
                                emballage och/eller Varan är behäftad med synliga fel (<span className="font-bold">”Transportskador”</span>). Eventuella
                                Transportskador ska noteras och anmälas till kundtjänst omedelbart.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                9
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                ÅNGERRÄTT
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-20">
                                9.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                Ångerrätten
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-24">
                                9.1.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary  ml-2">
                                Du har rätt att utnyttja din ångerrätt inom 14 dagar från den dag du tog emot Varan eller den
                                dag avtalet för köpt Tjänst ingicks (<span className="font-bold">"Tidsfristen”</span>).
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-24">
                                9.1.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary  ml-2">
                                Om du vill utnyttja din ångerrätt ska du inom Tidsfristen skicka ett klart och tydligt
                                meddelande till BLIXA där det framgår att du avser att nyttja din ångerrätt. Ett
                                standardformulär för utövandet av ångerrätten finns här.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-24">
                                9.1.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary  ml-2">
                                Ångerrätten gäller endast under förutsättning att ingen försegling brutits (om tillämpligt).
                                BLIXA har rätt att göra avdrag för eventuell värdeminskning till följd av att Varan har hanterats
                                i större omfattning än som varit nödvändigt för att fastställa dess egenskaper eller funktion.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-24">
                                9.1.4
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary  ml-2">
                                Du står för samtliga kostnader i samband med återköp, inklusive men inte begränsat till
                                kostnaden för returfrakt.
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-24">
                                9.1.5
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary  ml-2">
                                I det fall du utnyttjar din ångerrätt för en köpt Tjänst innan den har slutförts, har BLIXA rätt till
                                ersättning för den del av Tjänsten som redan har utförts och för det arbete som måste
                                utföras trots att du utövat din ångerrätt. BLIXA har även rätt till ersättning för utebliven vinst till
                                följd av de uppdrag som BLIXA avböjt att åta sig med hänsyn till den avbeställda tjänsten.
                                <br />
                            </p>
                        </div>

                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-20">
                                9.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-3">
                                Undantag från ångerrätt
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-24">
                                9.2.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary  ml-2">
                                Ångerrätten enligt denna punkt 10 gäller inte vid köp av Varor som av hälso- eller hygienskäl
                                inte lämpligen kan återlämnas med bruten försegling. Ångerrätten gäller inte heller för köp av
                                måltider, livsmedel eller andra varor med kort hållbarhet eller som på grund av sin natur inte
                                kan returneras eller som snabbt kan försämras eller bli för gamla, vid kontamineringsrisk
                                eller vid obruten kylkedja, samt i andra situationer där uttryckligt undantag finns enligt
                                gällande lagstiftning.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-24">
                                9.2.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary  ml-2">
                                Ångerrätten gäller inte heller för en Vara som tillverkats enligt Kundens anvisningar eller som
                                annars har fått en tydlig personlig prägel.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-24">
                                9.2.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-2">
                                Ångerrätten gäller inte heller för en tjänst som har fullgjorts mot betalning.
                                <br />
                            </p>
                        </div>

                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary font-bold sm:ml-20">
                                9.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-3">
                                Återbetalning
                                <br />
                            </p>
                        </div>

                        <div className="flex">
                            <br />
                            <p className="mt-2 mr-4 text-secondary  sm:ml-24">
                                Vid utövandet av ångerrätten kommer BLIXA, beroende på betalningsmetod, kreditera
                                fakturan eller återbetala pengarna till det kort/bankkonto från vilket betalningen drogs vid
                                köpet.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                10
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                REKLAMATION
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                10.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                Du har rätt att reklamera en Vara i tre (3) år efter att Varan mottagits om felet funnits vid
                                avlämnandet av Varan.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                10.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-2">
                                Du ska inom skälig tid efter att du fick kännedom om felet, reklamera felet till BLIXA via
                                BLIXA:s kundtjänst/de kontaktuppgifter som framgår i punkt 2 ovan. Du ska i din reklamation
                                ange med en beskrivning, och bild på, vad som är fel.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                10.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-2">
                                BLIXA står kostanden för returfrakten vid godkänd reklamation. Vid icke-godkänd reklamation
                                förbehåller sig BLIXA rätt att debitera Kund kostanden för returfrakt.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                11
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                GARANTIER
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                11.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                BLIXA lämnar inga garantier beträffande innehåll, information, råvaror, ingredienser, eller
                                annan information som rör Varorna, Tjänsterna eller övrigt som tillhandahålls av, eller
                                genom, BLIXA på Plattformen. BLIXA lämnar inte heller några garantier beträffande
                                Plattformens funktion, tillgänglighet, användbarhet eller säkerhet.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                11.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                BLIXA ansvarar inte för Varornas lämplighet för det specifika syfte du avser använda Varorna
                                till, utöver vad som framgår av Varornas beskrivning på Plattformen.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                12
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                BEHANDLING AV PERSONUPPGIFTER
                                <br />
                            </p>
                        </div>
                        <div className="flex ">

                            <br />
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                BLIXA kommer, i samband med Kundens köp och användning av Plattformen, att behandla
                                personuppgifter. Läs mer om hur BLIXA behandlar personuppgifter i BLIXA:s integritetspolicy.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                13
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                DIN ANVÄNDNING AV PLATTFORMEN
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                13.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                Du har inte rätt att använda BLIXA:s varumärken utöver vad som krävs för användandet av
                                Plattformen eller BLIXA:s tjänster.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                13.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-2">
                                BLIXA förbehåller sig rätten att stänga av en kund från Plattformen och neka en beställning
                                om kunden bryter mot Villkoren, eller använder Plattformen på ett otillbörligt sätt. BLIXA
                                förbehåller sig även rätten att debitera kunden för kostnader samt skada som BLIXA lider till
                                följd därav.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                14
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                FORCE MAJEURE
                                <br />
                            </p>
                        </div>
                        <div className="flex ">

                            <br />
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                BLIXA förbehåller sig rätten att stänga av, ställa in eller avboka beställningar om BLIXA eller
                                BLIXA:s Samarbetspartnern inte har möjlighet att fullborda en beställning till följd av en
                                händelse som är utom BLIXA:s eller BLIXA:s Samarbetspartners kontroll och som BLIXA eller
                                BLIXA:s Samarbetspartner inte skäligen kunnat förutse. Exempel på sådana händelser, men
                                inte begränsat till, är krig, naturkatastrofer, pandemi, terroristaktioner, politiska oroligheter,
                                strejk, lockout, blockad eller annat avtalshinder, eldsvåda, eller annan olyckshändelse.
                                <br />
                            </p>
                        </div>
                        <br />
                        <div className="flex ">
                            <p className="mt-2 mr-1 text-secondary font-bold sm:ml-6">
                                15
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary font-bold ml-4">
                                TVISTER
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                15.1
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-3">
                                Svensk lag ska tillämpas vid tolkning av Villkoren.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                15.2
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-2">
                                Om det föreligger en tvist mellan BLIXA och Kunden ska parterna i första hand försöka lösa
                                tvisten genom överenskommelse och genom Allmänna reklamationsnämnden, Box 174, 101
                                23 Stockholm,<a className="text-blue-500 underline" href="https://www.arn.se/" target="_blank">www.arn.se</a>
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                15.3
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-2">
                                Tvister kan även prövas av allmän domstol.
                                <br />
                            </p>
                        </div>
                        <div className="flex ">
                            <p className="mt-2 mr-4 text-secondary sm:ml-20">
                                15.4
                            </p>

                            <br />
                            <p className="mt-2 mr-4 text-secondary ml-2">
                                Om du som Kund är bosatt inom EU har du även rätt att använda dig av Europeiska
                                kommissions onlineplattform för att avgöra en tvist Europeiska kommissions onlineplattform
                                för att avgöra en tvist ec.europa.eu
                                <br />
                                <br />
                            </p>
                        </div>

                    </p>
                </div>
            </div>


            <Footer />
        </>
    );
};
export default TermsAndCondition;
