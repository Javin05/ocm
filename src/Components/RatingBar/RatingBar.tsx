import { useState } from "react";
import Rating from "@mui/material/Rating";

export default function RatingBar({ readonly, handleChange, ratings }: any) {
  const [rating, setRating] = useState(ratings);

  const handleRatingsChange = (event: any, value: any) => {
    setRating(value);
    handleChange(value);
  };

  return (
    <>
      <div className="flex items-center gap-4">
        <Rating
          name="half-rating-read"
          value={rating}
          onChange={(event, newValue) => {
            handleRatingsChange(event, newValue);
          }}
          className="text-[2px]"
          precision={0.5}
          readOnly={readonly}
        />
      </div>
    </>
  );
}
