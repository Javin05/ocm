const SearchCard = ({ img, title, activeChange, isActive, onClick }: any) => {
  const handlecardClick = () => {
    activeChange(title);
    onClick(title);
  };

  return (
    <div
      className="bg-white rounded-lg cursor-pointer"
      onClick={handlecardClick}
      style={{ backgroundColor: `${isActive ? "#D6B27D" : "#FFF5F4"}` }}
    >
      <div className="py-3 px-3 text-center">
        <img src={img} className="mx-auto mb-1" height={35} width={35} />
        <h1
          style={{ color: 'black' }}
          className={`text-[9px] uppercase font-black ${isActive ? "text-white" : "text-secondary"
            }`}
        >
          {title}
        </h1>
      </div>
    </div>
  );
};

export default SearchCard;
