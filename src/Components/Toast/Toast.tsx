import { ToastPosition, toast } from "react-toastify";


export function successNotify (message:string, toastLocation? :string) {
    if(!toastLocation) {
      return toast.success(message,{
        position: 'top-right',
        autoClose: 3500,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: false,
        progress: undefined,
      });
    } else {
      return toast.success(message,{
        position: 'bottom-right',
        autoClose: 3500,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: false,
        progress: undefined,
      });
    }
   
}

  export function  failureNotify(message:string) { 
    return  toast.error(message,{
    position: 'top-right',
    autoClose: 3500,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: false,
    draggable: false,
    progress: undefined,
  })};