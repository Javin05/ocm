import { CSVLink } from 'react-csv'
import { MouseEvent, ReactNode } from 'react';
import { SiMicrosoftexcel } from "react-icons/si";

interface CSVLinkProps {
  asyncOnClick?: boolean;
  data: any[] | (() => any[]);
  enclosingCharacter?: string;
  filename?: string;
  headers?: Headers;
  onClick?: (event: MouseEvent<HTMLAnchorElement, MouseEvent>, done: () => void) => void;
  separator?: string;
  target?: string;
  uFEFF?: boolean;
}

type SyncClickHandler = (event: MouseEvent<HTMLAnchorElement, MouseEvent>) => void;
type AsyncClickHandler = (event: MouseEvent<HTMLAnchorElement, MouseEvent>) => Promise<void>;

interface AsyncCSVLinkProps extends Omit<CSVLinkProps, 'data' | 'onClick'> {
  data: () => Promise<any[]>;
  children: ReactNode;
  asyncOnClick?: boolean;
  onClick?: SyncClickHandler | AsyncClickHandler | undefined;
}

function prepareCSVData(data: any[]): string {
  data.map(e => { delete e.projectId; delete e.timeEntryId, delete e.userId })
  const headers = Object.keys(data[0]);
  const csvRows = [
    headers.join(','),
    ...data.map((row) => headers.map((header) => JSON.stringify(row[header])).join(',')),
  ];
  return csvRows.join('\n');
}



export function AsyncDownloadCSV(props: AsyncCSVLinkProps): JSX.Element {
  const { data, asyncOnClick, onClick, children, ...rest } = props;

  const handleClick = async (event: any, done: any) => {
    event.preventDefault();
    console.log("file name ---> ", props.filename)
    console.log("props", props)
    if (asyncOnClick) {
      await onClick?.(event);
      const dataArray = await data();
      const csvData = prepareCSVData(dataArray);
      const csvBlob = new Blob([csvData], { type: 'text/csv' }); // Use 'text/csv' for CSV files
      const csvUrl = URL.createObjectURL(csvBlob);
      const link = document.createElement('a');
      link.href = csvUrl;
      link.download = props.filename || 'data.csv'; // Default filename to data.csv if not provided
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    } else {
      const dataArray = await data();
      const csvData = prepareCSVData(dataArray);
      const csvBlob = new Blob([csvData], { type: 'text/csv' }); // Use 'text/csv' for CSV files
      const csvUrl = URL.createObjectURL(csvBlob);
      const link = document.createElement('a');
      link.href = csvUrl;
      link.download = props.filename || 'data.csv'; // Default filename to data.csv if not provided
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  };

  return (
    <CSVLink asyncOnClick={true} data={[]} onClick={handleClick}>
      {children}
    </CSVLink>
  );
}


interface IDownloadCSVProps {
  data: any[],
  filename: string
}

const DownloadCSV = ({ data, filename }: IDownloadCSVProps) => {
  const newData = data.map((row) => {
    console.log("----------->", row);
    const { '-v': omitV, _id, isDeleted, __v, password, verificationToken, userId, timeEntryId, projectId, id, timeSheetId, updatedAt,
      ...newRow } = row;

    newRow.projectCompleted = newRow.projectCompleted ? 'Yes' : 'No';
    newRow.active && (newRow.active = newRow.active ? 'Yes' : 'No');
    return newRow;
  });

  return (
    <CSVLink data={newData} filename={filename} className='rounded-lg px-2 py-2'>
      <SiMicrosoftexcel className="w-6 h-6 text-red-400 hover:text-red-600 hover:cursor-pointer" />
    </CSVLink>
  )
}

export default DownloadCSV;
