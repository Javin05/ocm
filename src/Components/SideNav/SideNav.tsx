import { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import LogoImg from "../../assets/blixa-black.png";
import Category from "../../assets/icons8-category-48.png"
import resetpassword from '../../assets/icons8-reset-30.png'
import goldresetpassword from '../../assets/icons8-reset-50.png'
import Categorygrey from '../../assets/icons8-category-50.png'
import orderMenuIcon from "../../assets/icons8-products-50 (1).png";
import merchantsIcon from "../../assets/icons8-merchant-account-64 (1).png";
import schduleIcon from "../../assets/schdule.svg";
import event from "../../assets/icons8-events-64.png"
import eventColor from "../../assets/icons8-events-64 (1).png"
import messageIcon from "../../assets/greymessageIcon.png";
import settingsIcon from "../../assets/settings.svg";
import supportIcon from "../../assets/call.svg";
import callIcon from "../../assets/goldcall.png";
import orderListIcon from "../../assets/icons8-products-50.png";
import merchantsActive from "../../assets/icons8-merchant-account-64.png";
import schduleActive from "../../assets/schduleActive.svg";
import categoryIcon from "../../assets/orderList.svg";
import categoryActive from "../../assets/categoryActive.svg";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../Redux/store";
import msgIcon from "../../assets/goldmsgIcon.png";
import quotation from "../../assets/greyQuotation.png";
import goldQuotation from "../../assets/goldQuotation.png";
import commentsgrey from "../../assets/greycomments.png";
import commentsgold from "../../assets/goldcomments.png";

import { HiShoppingBag, HiUsers } from "react-icons/hi";
import { HiUserPlus } from "react-icons/hi2";
import { commonAxios } from "../../Axios/service";
import { setNotification } from "../../Redux/features/notificationSlice";
import { GrContact } from "react-icons/gr";
import { IoNotificationsSharp } from 'react-icons/io5';
import { MdContactSupport } from "react-icons/md";
import productIconMenu from "../../assets/icons8-product-100.svg"
import eventIconMenu from "../../assets/icons8-event-64.svg"

const SideNav = () => {
  const pathName = window.location.pathname;
  console.log(pathName, "pathName")
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  console.log(userInfo, "userinfo");
  const messageNotification = useSelector(
    (state: RootState) => state.notification.notificationDetails.messageCount
  );
  const commentNotification = useSelector(
    (state: RootState) => state.notification.notificationDetails.commentCount
  );

  const supplierMessageCount = useSelector(
    (state: RootState) =>
      state.notification.notificationDetails.supplierMessageCount
  );
  const adminMessageCount = useSelector(
    (state: RootState) =>
      state.notification.notificationDetails.adminMessageCount
  );
  useEffect(() => {
    if (userInfo.id) {
      getMessagesCount();
    }
  }, []);

  const getMessagesCount = async () => {
    if (userInfo.role === "ROLE_ADMIN") {
      try {
        const messageCount = await commonAxios.get(
          `cemo/appuser/notification/count?adminId=${userInfo.id}`
        );

        if (messageCount.data) {
          dispatch(setNotification(messageCount.data));
        }
      } catch (e) { }
    } else {
      try {
        const messageCount = await commonAxios.get(
          `cemo/appuser/notification/count?supplierId=${userInfo.id}&customerId=`
        );

        if (messageCount.data) {
          dispatch(setNotification(messageCount.data));
        }
      } catch (e) { }
    }
  };
  return (
    <div className="space-y-3 h-[100vh] overflow-y-scroll w-[15%] p-3 bg-[#FFE5D8] shadow">
      <div
        onClick={() => navigate("/")}
        className="flex items-center justify-center cursor-pointer"
      >
        <img src={LogoImg} className="w-36" />
      </div>
      <div className="mt-4">
        <h1 className="uppercase text-[#92929D] font-black ml-2 text-[18px] mt-8 tracking-[2px] text-center">
          Inspireras här
        </h1>
      </div>
      <div className="flex-1">
        <ul className="pt-2 pb-4 space-y-1 text-sm">
          <Link to={"/orderList"}>
            <li
              className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/orderList" ? " text-[#c48c39]" : "text-secondary"
                }`}
            >
              <HiShoppingBag
                className={`text-[#D6B27D] h-[24px] w-[24px] ${pathName === "/orderList" ? "text-[#D6B27D]" : "text-secondary"
                  }`}
              />
              <span
                className={`mx-3 text-[16px] font-medium ${pathName === "/orderList" ? "text-golden" : "text-secondary"
                  }`}
              >
                Orderlista
              </span>
            </li>
          </Link>
          <Link to={"/eventslist"}>
            <li
              className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/eventslist"
                ? " text-[#c48c39]"
                : "text-secondary"
                }`}
            >
              <img
                src={pathName === "/eventslist" ? eventColor : event}
                className="h-[22px] w-[22px]"
              />
              <span
                className={`mx-4 text-[16px] font-medium ${pathName === "/eventslist" ? "text-[#D6B27D]" : "text-secondary"
                  }`}
              >
                Event/tjänst
              </span>
            </li>
          </Link>
          {/* {userInfo.role === "ROLE_SUPPLIER" ? null : ( */}
          <Link to={"/eventscreen"}>
            <li
              className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/eventscreen"
                ? " text-[#c48c39]"
                : "text-secondary"
                }`}
            >
              <img
                src={pathName === "/eventscreen" ? eventIconMenu : eventIconMenu}
                className="h-[22px] w-[22px]"
              />
              <span
                className={`mx-4 text-[16px] font-medium ${pathName === "/eventscreen" ? "text-[#D6B27D]" : "text-secondary"
                  }`}
              >
                Event feedback
              </span>
            </li>
          </Link>
          {/* )} */}

          <Link to={"/products"}>
            <li
              className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/products" ? " text-[#c48c39]" : "text-secondary"
                }`}
            >
              <img
                src={pathName === "/products" ? orderMenuIcon : orderListIcon}
                className="h-[22px] w-[22px]"
              />
              <span
                className={`mx-4 text-[16px] font-medium ${pathName === "/products" ? "text-[#D6B27D]" : "text-secondary"
                  }`}
              >
                Produkter
              </span>
            </li>
          </Link>
          {/* {userInfo.role === "ROLE_SUPPLIER" ? null : ( */}
          <Link to={"/productscreen"}>
            <li
              className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/products" ? " text-[#c48c39]" : "text-secondary"
                }`}
            >
              <img
                src={pathName === "/productscreen" ? productIconMenu : productIconMenu}
                className="h-[22px] w-[22px]"
              />
              <span
                className={`mx-4 text-[16px] font-medium ${pathName === "/productscreen" ? "text-[#D6B27D]" : "text-secondary"
                  }`}
              >
                Produkt feedback
              </span>
            </li>
          </Link>
          {/* )} */}
          {userInfo.role === "ROLE_SUPPLIER" ? null : (
            <Link to={"/merchant"}>
              <li
                className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/merchant" ? "text-[#c48c39]" : "text-secondary"
                  }`}
              >
                <img
                  src={
                    pathName === "/merchant" ? merchantsActive : merchantsIcon
                  }
                  className="w-5 h-5"
                />
                <span
                  className={`mx-4 text-[16px] font-medium ${pathName === "/merchant" ? "text-[#D6B27D]" : "text-secondary"
                    }`}
                >
                  Leverantörer
                </span>
              </li>
            </Link>
          )}
          {userInfo.role === "ROLE_SUPPLIER" ? null : (
            <Link to={"/customerlist"}>
              <li
                className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/customerlist"
                  ? " text-[#c48c39]"
                  : "text-secondary"
                  }`}
              >
                {/* <img
                src={
                  pathName === "/customerlist"
                    ? merchantsActive
                    : merchantsIcon
                }
                className=""
              /> */}
                <HiUsers
                  className={`h-[24px] w-[24px] ${pathName === "/customerlist"
                    ? "text-[#D6B27D]"
                    : "text-gray-400"
                    }`}
                />
                <span
                  className={`mx-3 text-[16px] font-medium ${pathName === "/customerlist" ? "text-[#D6B27D]" : "text-secondary"
                    }`}
                >
                  Kundlista
                </span>
              </li>
            </Link>
          )}

          {userInfo.role === "ROLE_SUPPLIER" ? null : (
            <Link to={"/activateSupplier"}>
              <li
                className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/activateSupplier"
                  ? "bg-[#fffafa] text-[#c48c39]"
                  : "text-secondary"
                  }`}
              >
                <HiUserPlus
                  className={`h-[24px] w-[24px] ${pathName === "/activateSupplier"
                    ? "text-[#D6B27D]"
                    : "text-gray-400"
                    }`}
                />
                <span
                  className={`mx-4 text-[16px] text-secondary font-medium ${pathName === "/activateSupplier" ? "text-[#D6B27D]" : "text-secondary"
                    }`}
                >
                  Väntande konton
                </span>
              </li>
            </Link>
          )}

          <Link to={"/schedule"}>
            <li
              className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/schedule" ? " text-[#c48c39]" : "text-secondary"
                }`}
            >
              <img
                src={pathName === "/schedule" ? schduleActive : schduleIcon}
                className="h-[22px] w-[22px]"
              />
              <span
                className={`mx-3 text-[16px] ${pathName === "/schedule" ? "text-[#D6B27D]" : "text-secondary"
                  } font-medium`}
              >
                Schema
              </span>
            </li>
          </Link>
          {userInfo.role === "ROLE_ADMIN" && (
            <Link to={"/category"}>
              <li
                className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/category"
                  ? " text-[#c48c39]"
                  : "text-secondary"
                  }`}
              >
                <img
                  src={pathName === "/category" ? Category : Categorygrey}
                  className="h-[22px] w-[22px]"
                />
                <span
                  className={`mx-3 text-[16px] ${pathName === "/category" ? "text-[#D6B27D]" : "text-secondary"
                    } font-medium`}
                >
                  Kategori
                </span>
              </li>
            </Link>
          )}

          {userInfo.role === "ROLE_ADMIN" && (
            <Link to={"/notifyme"}>
              <li
                className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/notifyme"
                  ? " text-[#c48c39]"
                  : "text-secondary"
                  }`}
              >

                <IoNotificationsSharp className="h-[22px] w-[22px]" color={`${pathName === "/notifyme" ? "" : "#8C7E79"} `} />
                <span
                  className={`mx-3 text-[16px] ${pathName === "/notifyme" ? "text-[#D6B27D]" : "text-secondary"
                    } font-medium`}
                >
                  Meddela mig
                </span>
              </li>
            </Link>
          )}
          <Link to={"/profile-password-reset"}>
            <li
              className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/profile-password-reset"
                ? " text-[#c48c39]"
                : "text-secondary"
                }`}
            >
              <img
                src={
                  pathName === "/profile-password-reset"
                    ? goldresetpassword
                    : resetpassword
                }
                className="h-[22px] w-[22px]"
              />
              <span
                className={`mx-3 text-[16px] ${pathName === "/profile-password-reset" ? "text-[#D6B27D]" : "text-secondary"
                  } font-medium`}
              >
                Återställ lösenord
              </span>
            </li>
          </Link>

          {userInfo.role === "ROLE_SUPPLIER" ? null : (
            <Link to={"/contactFeedback"}>
              <li
                className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/contactFeedback"
                  ? " text-[#c48c39]"
                  : "text-secondary"
                  }`}
              >
                <MdContactSupport
                  className={`h-[24px] w-[24px] ${pathName === "/contactFeedback"
                    ? "text-[#D6B27D]"
                    : "text-gray-400"
                    }`}
                />
                <span
                  className={`mx-3 text-[16px] font-medium ${pathName === "/contactFeedback" ? "text-[#D6B27D]" : "text-secondary"
                    }`}
                >
                  Kontakta oss
                </span>
              </li>
            </Link>
          )}
          {userInfo.role === "ROLE_SUPPLIER" && (
            <>
              <Link to={"/quotations-list"}>
                <li
                  className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/quotations-list"
                    ? " text-[#c48c39]"
                    : "text-secondary"
                    }`}
                >
                  <img
                    src={
                      pathName === "/quotations-list"
                        ? goldQuotation
                        : quotation
                    }
                    className="h-[26px] w-[26px]"
                  />
                  <span
                    className={`mx-4 text-[16px] ${pathName === "/quotations-list" ? "text-[#D6B27D]" : "text-secondary"
                      } font-medium`}
                  >
                    Offerter
                  </span>

                </li>
              </Link>

              <Link to={"/Reviewandcomments"}>
                <li
                  className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/Reviewandcomments"
                    ? "text-[#c48c39]"
                    : "text-secondary"
                    }`}
                >
                  <img
                    src={
                      pathName === "/Reviewandcomments"
                        ? commentsgold
                        : commentsgrey
                    }
                    className="h-[26px] w-[26px]"
                  />
                  <span
                    className={`mx-4 text-[16px] ${pathName === "/Reviewandcomments" ? "text-[#D6B27D]" : "text-secondary"
                      } font-medium`}
                  >
                    Omdömen och kommentarer
                  </span>
                </li>
              </Link>
            </>
          )}
          <Link to={"/backpanel-support"}>
            <li
              className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/backpanel-support"
                ? " text-[#c48c39]"
                : "text-secondary"
                }`}
            >
              <img
                src={pathName === "/backpanel-support" ? callIcon : supportIcon}
                className="h-[26px] w-[26px]"
              />
              <span
                className={`mx-4 text-[16px] ${pathName === "/backpanel-support" ? "text-[#D6B27D]" : "text-secondary"
                  } font-medium`}
              >
                <div className="flex">
                  {userInfo.role === "ROLE_ADMIN"
                    ? "Kontakta leverantör"
                    : "Kontakta admin"}
                </div>
              </span>
            </li>
          </Link>
          <Link to={"/customer-messages"}>
            <li
              className={`cursor-pointer font-bold px-2 flex items-center py-3 rounded-md transition-all my-2 ${pathName === "/backpanel-support"
                ? " text-[#c48c39]"
                : "text-secondary"
                }`}
            >
              <img
                src={pathName === "/customer-messages" ? callIcon : supportIcon}
                className="h-[26px] w-[26px]"
              />
              <span
                className={`mx-4 text-[16px] flex ${pathName === "/customer-messages" ? "text-[#D6B27D]" : "text-secondary"
                  } font-medium`}
              >
                Contact Kund
              </span>
            </li>
          </Link>
        </ul>
      </div>
    </div>
  );
};

export default SideNav;
