import React from "react";
import arrowImg from "../../assets/arrow.svg";

const Button = ({ btnName, style }: any) => {
  return (
    <div className="flex p-2 w-auto font-semibold rounded-lg bg-[#D6B37D] text-white px-3">
      <button
        className={`rounded-lg text-white mx-4 font-semibold uppercase ${style}`}
      >
        {btnName}
      </button>
      {/* <img src={arrowImg} className="cursor-pointer mr-3" /> */}
    </div>
  );
};

export default Button;
