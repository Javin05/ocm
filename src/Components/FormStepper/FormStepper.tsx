import React from 'react'
import { Stepper } from 'react-form-stepper'

const FormStepper = (props: any) => {
    return (
        <div>
            <Stepper
                {...props}
                connectorStateColors={true}
                connectorStyleConfig={{
                    completedColor: '#D6B27D',
                    activeColor: '#D6B27D',
                    disabledColor: '#eee'
                }}
                styleConfig={{
                    activeBgColor: '#D6B27D',
                    completedBgColor: '#D6B27D',
                    inactiveBgColor: '#eee',
                    activeTextColor: '#F7FAFC',
                    completedTextColor: '#F7FAFC',
                    inactiveTextColor: '#1A202C'
                }}
            />
        </div>
    )
}

export default FormStepper