import React, { useState } from "react";
import bookNowKitchen from "../../assets/bookNowKitchen.png";
import Button from "../Button/Button";
import { useNavigate } from "react-router-dom";
import { HiHeart } from "react-icons/hi";
import { AiFillHeart } from "react-icons/ai";
import { IoIosHeartEmpty } from "react-icons/io";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { createSearchParams } from "react-router-dom";
import { MdOutlineIosShare } from 'react-icons/md'
import { failureNotify, successNotify } from "../Toast/Toast";
import notFavHeart from "../../assets/latestNotfav.png";
import { Tooltip } from "@material-tailwind/react";
import share from "../../assets/share.png";
import { commonAxios } from "../../Axios/service";
import { getWishlistCount } from "../../Redux/features/wishlistSlice";

const BookingCard = ({
  cardTitle,
  cardImg,
  businessName,
  name,
  isFavourite,
  isShare,
  id,
  value,
  isFirstRow,
  actualEvent,
  setIsPopupDetailModalOpen,
  setIsLoading,
  setDefaultRatings,
  setRatingCount,
  setQuickViewDetail,
  setIsFavourite
}: any) => {
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [isFav, setIsFav] = useState(isFavourite);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  console.log(actualEvent, "actualEvent")

  const handleNavigate = async (data: any) => {
    getFavouriteItems(data.id);
    setIsLoading(true);
    let response;
    if (data.isProduct) {
      response = await commonAxios.get(
        `cemo/ratings/average/rating?productId=${data.id}`
      );
      setDefaultRatings(response.data);
      setRatingCount(response.data)
    } else if (!data.isProduct) {
      response = await commonAxios.get(
        `cemo/ratings/average/rating?eventId=${data.id}`
      );
      setDefaultRatings(response.data);
      setRatingCount(response.data)
    }
    setQuickViewDetail(data);
    setIsPopupDetailModalOpen(true);
    setIsLoading(false);
    // setIsPopupDetailModalOpen(true)

  }
  const navigatePage = () => {
    if (actualEvent.isProduct) {
      navigate(`${`/cart?id=${actualEvent.appUser.id}&productId=${actualEvent.id}`}`)
    }
    else {
      navigate(`${`/booking?id=${actualEvent.id}`}`)
    }
  }

  const getFavouriteItems = async (id: any) => {
    if (userInfo.id) {
      try {
        const response = await commonAxios.get(
          `cemo/favourite-items/favourite/${userInfo.id}/${id}`
        );
        setIsFavourite(response.data.isFavourite);
        dispatch(getWishlistCount(userInfo.id));
      } catch (e) {
      } finally {
      }
    }
  };
  const handleFavouriteClick = (id: any) => {
    value(actualEvent);
    setIsFav(!isFav);
    dispatch(getWishlistCount(userInfo.id));
  };
  const handleShareClick = async (id: any) => {

    try {
      let text = `${window.location.href}` + 'booking?id=' + id
      await navigator.clipboard.writeText(text);
      successNotify('Link Copied')
    } catch (error) {
      failureNotify('The link is not copied')
    }
  }
  return (
    <div className="relative w-[89%] h-[400px] md:w-full group"  >
      <div className="" onClick={() => handleNavigate(actualEvent)}>
        <div className="absolute w-[100%] h-[400px] rounded-[0.5rem] group-hover:z-20 group-hover:bg-[#00000090] flex justify-center cursor-pointer">
          <div
            className="flex-col items-center justify-center hidden group-hover:flex"
          >
            <h1 className="font-extrabold text-white text-[20px] text-center">
              {name}
            </h1>
            <h1 className="text-white font-bold text-center">{businessName}</h1>
          </div>
        </div>
        {cardTitle && (
          <h1 className="absolute tracking-widest text-white lg:text-4xl font-bold uppercase right-10 mt-10">
            {cardTitle}
          </h1>
        )}
        <div
          className="md:h-full"
          onClick={handleNavigate}
        >
          <img
            style={{objectFit:"cover"}}
            src={cardImg}
            className={`cursor-pointer h-[400px] ${isFirstRow ? 'w-full' : 'w-[400px]'} object-fit rounded-lg`}
          />
        </div>
      </div>
      <div className="absolute right-5 bottom-2 group-hover:z-40 flex">


        <div>
          <button
            className={`rounded-lg text-white uppercase mx-3  px-4 py-2 mt-1 text-[16px] lg:mx-3 md:text-[12px] xl:text-[16px] font-semibold bg-[#D6B27D] `}
            onClick={navigatePage}
          >
            {actualEvent.isProduct ? 'Köp' : 'Besök'}
          </button>

        </div>

        {userInfo.id && (
          // <Tooltip className='bg-[#D6B27D]' content={isFavourite ? "Remove from wishlist" : 'Add to wishlist'} placement="right-start">
          <div
            onClick={() => handleFavouriteClick(id)}
            className="cursor-pointer"
          >
            {isFav ? (
              <div className="rounded-full p-2 hover:bg-secondary">
                <AiFillHeart className="h-6 w-6 text-[#D6B27D]" />
              </div>
            ) : (
              <div className="rounded-full p-2 hover:bg-secondary">
                <img style={{objectFit:"cover"}} src={notFavHeart} className="h-7 w-7 z-40 text-white hover:text-[#D6B27D] object-fit" />
              </div>
            )}
          </div>
          // </Tooltip>
        )}
        {
          !userInfo.id &&
          // <Tooltip className='bg-[#D6B27D]' content={"Share"} placement="right-start">
          <div className="rounded-full p-2 mb-2  hover:bg-[#777070ef] cursor-pointer mx-1" onClick={() => handleShareClick(id)}>
            <img src={share}
              style={{objectFit:"cover"}} className={`${userInfo.id ? "h-6 w-6 text-[#D6B27D] z-40 object-fit" : "h-6 w-6 text-[#D6B27D] z-40 object-fit"}`} />
          </div>
          // </Tooltip>

        }

      </div>
    </div>
  );
};

export default BookingCard;
