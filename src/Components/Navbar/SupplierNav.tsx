import { useNavigate } from "react-router-dom";
import { isUserAuthenticated } from "../../Utils/auth";
import { failureNotify, successNotify } from "../Toast/Toast";

const SupplierNav = (props: any) => {

    const navigate = useNavigate();

    const handleUserFlow = () => {
        if (!isUserAuthenticated()) {
            failureNotify('Login as user to continue')
            navigate('/login')
        } else {
            successNotify('Switched to user profile')
            navigate('/')
        }
    }

    // return (
    //   <nav className="flex md:justify-center items-center bg-[#FFFFFF] p-6">
    //     {/* <img src={profile} alt="profile" className="absolute right-8 top-5 h-10 w-10" /> */}

    //     <div className="flex">
    //       <div className=" text-[#D6B37D] text-2xl flex justify-center items-center">
    //         <span className="mx-10 ">
    //           {props.role}
    //         </span>
    //       </div>
    //       {/* <div onClick={handleUserFlow} className=" text-[#171725] text-2xl flex justify-center items-center cursor-pointer">
    //         <span className="mx-10">
    //           User
    //         </span>
    //       </div> */}

    //     </div>
    //   </nav>
    // );
};

export default SupplierNav;
