import { useState } from "react";
import schduleIcon from "../../assets/schdule.svg";
const BookedCard = ({ events, getEventDetails, resetSlotsValues }: any) => {
  console.log(events, "in bookcard");
  const [active, setActive] = useState<any>(null);
  const getBookingDetailsData = (event: any, index: Number) => {
    getEventDetails(event);
    setActive(index)
    resetSlotsValues();
  }


  return (
    <>
      {events.map((e: any, index: Number) => (
        <div className={`p-3 border-b-2 ${index === active ? 'bg-[#FFE5D8]' : null}`} onClick={() => getBookingDetailsData(e, index)}>
          <div className="flex cursor-pointer">
            <img src={schduleIcon} className="h-5 w-5 mt-2" />
            <p className="ml-2 text-golden text-[16px] mt-1">  <span className="text-golden">Startdatum:</span>{" "}
              <span className="text-gray-500">{e.startDate}</span> <br />
              <span className="text-golden">Slutdatum:</span>{" "}
              <span className="text-gray-500 ml-2">{e.endDate}</span></p>
          </div>
          <div className="mx-4 cursor-pointer">
            <div className="flex">
              <span className="text-secondary text-[16px] font-medium ml-3 mt-2">
                {e.name}
              </span>
            </div>
            <div className="flex flex-col gap-2">
              <p className="text-secondary text-[14px] font-medium ml-3">Organisatör: {e.appUser.firstName} {e.appUser.lastName} </p>
              <p className="text-secondary text-[14px] font-medium ml-3">Fördröjning innan nästa bokning: {e.delayTime.split(':')[0]} : {e.delayTime.split(':')[1]} Timmar</p>
              {/* <img src={e.appUser.profileImage} className="h-[32px] w-[32px] mx-4 my-auto rounded-2xl" /> */}
            </div>

          </div>
        </div>
      ))}
    </>
  );
};

export default BookedCard;
