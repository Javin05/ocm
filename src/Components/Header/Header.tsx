import React, { useEffect, useRef, useState } from "react";
import mainLogo from "../../assets/OCM-logo-resized (1).png";
import searchIcon from "../../assets/SearchIcon.svg";
import cartIcon from "../../assets/cartIcon.png";
import profileIcon from "../../assets/Profile.svg";
import wishlistIcon from "../../assets/favourite.png";
import "./Header.css";
import notificationIcon from "../../assets/notification.svg";
import { Link, createSearchParams, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { setUserDetails } from "../../Redux/features/userSlice";
import LogoImg from "../../assets/blixa-black.png";
import { failureNotify, successNotify } from "../Toast/Toast";
import { clearAll } from "../../Utils/auth";
import { commonAxios, formAxios } from "../../Axios/service";
import deleteIcon from "../../assets/Delete.png";
import Imagepicker from "../Imagepicker/Imagepicker";
import ImageShown from "../Imagepicker/ImageShown";
import Loader from "../Loader/Loader";
import { setNotification } from "../../Redux/features/notificationSlice";
import Notifications from "../Notifications/Notifications";
import { Badge, Tooltip } from "@material-tailwind/react";
import CountLoader from "../CountLoader/CountLoader";
import { getWishlistCount } from "../../Redux/features/wishlistSlice";
import { getCartListCount } from "../../Redux/features/cartCountSlice";

const Header = ({ productCount }: any) => {
  const userDetails = useSelector((state: RootState) => state.user.userDetails);
  const [dropdownVisible, setDropdownVisible] = useState(false);
  const [listOfProductsSize, setListOfProducts] = useState(0);
  const [isSearchOpen, setIsSearchOpen] = useState<boolean>(true);
  const [isloading, setIsLoading] = useState(false);
  const [browserWidth, setBrowserWidth] = useState(window.innerWidth)
  const [wishList, setWishList] = useState<any>(null);
  const dropdownRef = useRef<HTMLDivElement>(null);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [globalSearch, setGlobalSearch] = useState("");

  const cartInfo = useSelector(
    (state: RootState) => state.productsCart.productDetails.quantity
  );
  const messageCount = useSelector(
    (state: RootState) => state.notification.notificationDetails.messageCount
  );
  const commentCount = useSelector(
    (state: RootState) => state.notification.notificationDetails.commentCount
  );
  const userInfo = useSelector((state: RootState) => state.user.userDetails);

  const wishListInfo: any = useSelector((state: RootState) => state.wishlist.wishlistDetails);
  const cartItemsInfo = useSelector((state: any) => state.cartCount.productDetails);
  console.log(cartItemsInfo, "cartItemsInfo")
  const getCartDetails = async () => {
    if (userDetails.id) {
      if (userDetails.role === "ROLE_CUSTOMER") {
        try {
          setIsLoading(true);
          const response = await commonAxios.get(
            `/cemo/bookingitems/get/cart/${userDetails.id}`
          );
          const listOfProducts = response.data.bookingItemList.filter(
            (item: any) => item.bookingItemType.toLowerCase() === "product"
          );

          if (listOfProducts) {
            setListOfProducts(listOfProducts.length);
          } else {
            setListOfProducts(0)
          }
        } catch (error) {
        } finally {
          setIsLoading(false);
        }
      }
    } else {
      const listOfProducts = JSON.parse(localStorage.getItem("cartListDetails") as string);
      if (listOfProducts) {
        setListOfProducts(listOfProducts.quantity || 0);
      }
    }
  };



  useEffect(() => {
    if (userDetails?.id !== "") {
      getCartDetails();
      dispatch(getWishlistCount(userInfo.id));
    }
    if (userDetails.role === "ROLE_CUSTOMER") {
      dispatch(getCartListCount(userInfo.id))
    }
  }, [cartInfo]);

  useEffect(() => {
    if (userDetails?.id) {
      getMessageCount();
    }
  }, []);

  const getMessageCount = async () => {
    try {
      const messageCount = await commonAxios.get(
        `cemo/appuser/notification/count?supplierId=&customerId=${userDetails.id}`
      );
      if (messageCount.data) {
        dispatch(setNotification(messageCount.data))
          ;
      }
    } catch (e) { }
  };

  const handleLogout = () => {
    if (userDetails.role === "ROLE_ADMIN") {
      navigate("/adminlogin");
      dispatch(setUserDetails({}));
      successNotify("Logout successful");
      clearAll();
    } else if (userDetails.role === "ROLE_SUPPLIER") {
      navigate("/supplierlogin");
      dispatch(setUserDetails({}));
      successNotify("Logout successful");
      clearAll();
    } else {
      navigate("/login");
      dispatch(setUserDetails({}));
      successNotify("Logout successful");
      clearAll();
    }
  };
  const toggleDropdown = () => {
    setDropdownVisible(!dropdownVisible);
  };

  const getEventsBySearch = async (text: string) => {
    const globalSearchObj = JSON.parse(
      localStorage.getItem("globalSearchObj")!
    );

    const updatedGlobalSearchObj = {
      ...globalSearchObj,
      title: "",
      filterList: [],
    };
    const params = {
      searchText: text,
      searchTitle: "",
      location: globalSearchObj.location,
    };
    localStorage.setItem(
      "globalSearchObj",
      JSON.stringify(updatedGlobalSearchObj)
    );
    navigate({
      pathname: "/popular",
      search: `?${createSearchParams(params)}`,
    });
  };

  const handleKeyDown = (event: any) => {
    if (event.key === "Enter" && globalSearch !== "") {
      getEventsBySearch(globalSearch);
    }
  };

  const mediaQuery = window.matchMedia('(max-width: 568px)');

  useEffect(() => {
    // getCartDetails();
    const handleClickOutside = (event: MouseEvent) => {
      if (
        dropdownRef.current &&
        !dropdownRef.current.contains(event.target as Node)
      ) {
        setDropdownVisible(false);
      }
    };

    if (mediaQuery.matches) {
      // Media query matches (viewport is smaller than or equal to 568px)
      setIsSearchOpen(true)
    }

    document.addEventListener("click", handleClickOutside);
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, []);

  let currentUrl = window.location.href;

  const pathname = window.location.pathname;


  useEffect(() => {
    const handleResize = () => {
      setBrowserWidth(window.innerWidth);
    };

    window.addEventListener('resize', handleResize);


    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div
      className={`${pathname === "/ordershistory" ? "bg-[#FFE5D8]" : ""
        }`}
    >
      {/* {isloading ? <Loader /> : null} */}
      {userDetails.id ? (
        <div className="flex flex-col sm:block">
          <div className="flex sm:flex-row sm:justify-between flex-col xl:py-4 py-1 lg:pr-10 order-2">
            <div>
              {window.location.href.includes("ordershistory") &&
                userDetails.role === "ROLE_CUSTOMER" ? (
                <div
                  onClick={() => navigate("/")}
                  className="flex items-center justify-center cursor-pointer sm:px-6 px-2"
                  style={{ marginBottom: '-12px', height: '82px' }}
                >
                  <img src={LogoImg} className="w-36 mx-4" />
                </div>
              ) : null}
            </div>
            <div className="flex flex-col sm:flex-row items-center w-[100%] justify-end">
              {userDetails.role === "ROLE_CUSTOMER" ? (
                <div className={`relative order-2 sm:order-1 sm:right-0 ${isSearchOpen ? 'right-0' : 'right-[-70px]'}`}>
                  <input
                    type="text"
                    placeholder={isSearchOpen && browserWidth > 700 ? "Sök efter evenemangsplats, aktiviteter, partyshop" : isSearchOpen && browserWidth < 700 ? "Search..." : ''}
                    className={`border-none py-2 sm:mr-2 sm:px-4 rounded-lg border-2 border-gray-300 ${isSearchOpen ? 'lg:w-[440px] input-with-padding' : 'w-10 bg-transparent'} text-secondary placeholder:text-secondary placeholder:text-[17px] sm:placeholder:text-[17px] focus:outline-none mb-2 focus:border-transparent transition-all duration-300 input-with-padding`}
                    value={globalSearch}
                    onChange={(event: any) =>
                      setGlobalSearch(event.target.value)
                    }
                    onKeyDown={handleKeyDown}
                  />
                  <img
                    src={searchIcon}
                    onClick={() => {
                      getEventsBySearch(globalSearch);
                      // setIsSearchOpen(!isSearchOpen)
                      // setGlobalSearch('')
                    }}
                    className="absolute right-0 sm:right-3 top-[7px] cursor-pointer"
                  />
                </div>
              ) : null}
              <div className="flex flex-col justify-end sm:mt-0 order-1 w-[70%] sm:w-[62%] md:w-[50%] lg:w-[40%] xl:w-[12%]">
                <div className="flex flex-row justify-between items-center">
                  {userDetails.role === "ROLE_CUSTOMER" ? (
                    // <Tooltip className='bg-[#D6B27D]' content="Notifications" placement="bottom">
                    <div
                      className="items-center"
                    >
                      <Notifications scrollDown={true} />
                    </div>
                    // </Tooltip >
                  ) : null}

                  {userDetails.role === "ROLE_CUSTOMER" ? (
                    // <Tooltip className='bg-[#D6B27D]' content="Wishlist" placement="bottom">
                    <div
                      className="cart-item grid items-center"
                      onClick={() =>
                        navigate({
                          pathname: "/ordershistory",
                          search: `?${createSearchParams({
                            activeMenu: "Favourites",
                          })}`,
                        })
                      }
                    >
                      <Badge
                        content={wishListInfo ? <span>{wishListInfo.wishlist.length}</span> : <CountLoader />}
                        className="bg-[#D6B27D] border border-white min-w-[12px] min-h-[12px] w-[20px] h-[20px]"
                      >
                        <img
                          src={wishlistIcon}
                          className="sm:mt-[1px] cursor-pointer w-[30px]"
                        />
                      </Badge>
                    </div>
                    // </Tooltip>
                  ) : null}
                  {userDetails.role === "ROLE_CUSTOMER" ? (
                    // <Tooltip className='bg-[#D6B27D]' content="Cart Items" placement="bottom">
                    <div
                      className="cart-item grid items-center"
                      onClick={() => {
                        navigate(
                          {
                            pathname: "/shoppingCart",
                          },
                          {
                            state: { currentUrl },
                          }
                        );
                      }}
                    >
                      {productCount === undefined ? <Badge
                        content={listOfProductsSize}
                        className="bg-[#D6B27D] border border-white min-w-[12px] min-h-[12px] w-[20px] h-[20px]"
                      >
                        <img src={cartIcon} className="cursor-pointer w-[25px] mt-1" />
                      </Badge> :
                        <Badge
                          content={cartItemsInfo != null ? <span>{cartItemsInfo.quantity}</span> : <CountLoader />}
                          className="bg-[#D6B27D] border border-white min-w-[12px] min-h-[12px] w-[20px] h-[20px]"
                        >
                          <img src={cartIcon} className="cursor-pointer w-[25px] mt-1" />
                        </Badge>}
                    </div>
                    // </Tooltip>
                  ) : null}

                  <div ref={dropdownRef}>
                    {userDetails.role === "ROLE_CUSTOMER" ? (
                      <img
                        src={profileIcon}
                        className="cursor-pointer w-[30px] h-[29px]"
                        onClick={() =>
                          navigate("/ordershistory?activeMenu=Orders")
                        }
                      />
                    ) : null}
                    {userDetails.role === "ROLE_ADMIN" ||
                      userDetails.role === "ROLE_SUPPLIER" ? (
                      <img
                        src={profileIcon}
                        className="w-[30px] h-[30px] cursor-pointer"
                        onClick={() => navigate("/orderList")}
                      />
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
          </div>
          {window.location.pathname === "/ordershistory" ? null : (
            <div className="flex justify-center py-2 cursor-pointer order-1">
              <img
                // src={mainLogo}
                className={` ${userDetails.role === "ROLE_CUSTOMER"
                  ? "xl:-mt-[34px]"
                  : "-mt-[15px]"
                  } `}
                onClick={() => navigate("/")}
              />
            </div>
          )}
        </div>
      ) : (
        <div className="">
          <img
            // src={mainLogo}
            onClick={() => navigate("/")}
          />
        </div>
      )}
    </div>
  );
};

export default Header;
