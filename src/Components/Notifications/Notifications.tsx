import {
  Menu,
  MenuHandler,
  MenuList,
  MenuItem,
  Avatar,
  Typography,
  Badge,
} from "@material-tailwind/react";
import type { MenuHandlerProps } from "@material-tailwind/react";
import { ClockIcon } from "@heroicons/react/24/outline";
import notificationIcon from "../../assets/solid-notification-bell-icon.png";
import blackNotificationIcon from "../../assets/solid-notification-bell-icon-black.png";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { useContext, useEffect, useState } from "react";
import moment from "moment";
import { commonAxios } from "../../Axios/service";
import { setNotification } from "../../Redux/features/notificationSlice";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../Pages/RootLayout/RootLayout";

const Notifications = (props: any) => {
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const adminMessageCount = useSelector(
    (state: RootState) =>
      state.notification.notificationDetails.adminMessageCount
  );
  const supplierMessageCount = useSelector(
    (state: RootState) =>
      state.notification.notificationDetails.supplierMessageCount
  );
  const customerMessageCount = useSelector(
    (state: RootState) =>
      state.notification.notificationDetails.customerMessageCount
  );

  const messageCount = useSelector(
    (state: RootState) => state.notification.notificationDetails.messageCount
  );
  const commentCount = useSelector(
    (state: RootState) => state.notification.notificationDetails.commentCount
  );
  const messageList = useSelector(
    (state: RootState) => state.notification.notificationDetails.messageList
  );
  const commentList = useSelector(
    (state: RootState) => state.notification.notificationDetails.commentList
  )?.filter((comment) => comment.status === "ACCEPTED");
  const navigate = useNavigate();
  const [notificationList, setNotificationList] = useState<any>([]);
  const [allNotification, setAllNotification] = useState<any>([]);
  const [isModalVisible, setIsModalVisible] = useState(true);
  const dispatch = useDispatch();
  const [isRead, setIsRead] = useState<any>(false);
  const { setNotifyReRender, notifyRender } = useContext(AuthContext);
  const getMessageCount = async () => {
    let URL = "";
    if (userInfo.role === "ROLE_CUSTOMER") {
      URL = `cemo/appuser/notification/count?supplierId=&customerId=${userInfo.id}`;
    } else if (userInfo.role === "ROLE_SUPPLIER") {
      URL = `cemo/appuser/notification/count?supplierId=${userInfo.id}&customerId=`;
    } else {
      URL = `cemo/appuser/notification/count?adminId=${userInfo.id}`;
    }

    try {
      const messageCount = await commonAxios.get(URL);
      if (messageCount?.data) {
        dispatch(setNotification(messageCount?.data));
        setNotificationList(messageCount?.data);
        getAllNotifications();
      }
    } catch (e) { }
  };

  useEffect(() => {
    getMessageCount();
  }, [notifyRender])

  const getAllNotifications = async () => {
    let URL = "";
    try {
      if (userInfo.role === "ROLE_CUSTOMER") {
        URL = `/cemo/messages/users?customerId=${userInfo.id}`;
      } else if (userInfo.role === "ROLE_SUPPLIER") {
        URL = `/cemo/messages/users?supplierId=${userInfo.id}`;
      } else {
        URL = `/cemo/messages/users?adminId=${userInfo.id}`;
      }
      const response = await commonAxios.get(URL);
      const notificationData: any = response.data.slice(0, 20);
      setAllNotification(notificationData);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getAllNotifications();
  }, [notifyRender]);

  const updateNotification = () => {
    const notifications = messageList?.concat(commentList as any);

    notifications?.sort((a, b) => {
      const dateA = moment(a.updatedDate, "YYYY-MM-DD HH:mm:ss");
      const dateB: any = moment(b.updatedDate, "YYYY-MM-DD HH:mm:ss");
      return dateB.diff(dateA);
    });

    setNotificationList(notifications);
  };

  const clearAllNotifications = async () => {
    const commentIdList = commentList?.map((comment: any) => comment.id);
    const messageIdList = messageList?.map((message: any) => message.id);
    const commentBody = {
      id: commentIdList,
    };
    const messageBody = {
      id: messageIdList,
      customerId: userInfo.role === "ROLE_CUSTOMER" ? userInfo.id : null,
      supplierId: userInfo.role === "ROLE_SUPPLIER" ? userInfo.id : null,
      adminId: userInfo.role === "ROLE_ADMIN" ? userInfo.id : null,
    };
    if (commentIdList?.length > 0 && messageIdList?.length > 0) {
      const responseOne = await commonAxios.put(
        "/cemo/comments/update/status",
        commentBody
      );
      const responseTwo = await commonAxios.put(
        "/cemo/messages/update/status",
        messageBody
      );
      if (responseOne.data && responseTwo.data) {
        getMessageCount();
      }
    } else if (commentIdList?.length > 0) {
      const responseOne = await commonAxios.put(
        "/cemo/comments/update/status",
        commentBody
      );
      if (responseOne.data) {
        getMessageCount();
      }
    } else {
      const responseTwo = await commonAxios.put(
        "/cemo/messages/update/status",
        messageBody
      );
      if (responseTwo.data) {
        getMessageCount();
      }
    }
  };

  const handleNavigateClick = async (notification: any) => {
    console.log(notification, "notification")
    if (userInfo.role === "ROLE_SUPPLIER") {

      // Supplier Message handleShare flow 

      const updateVal = {
        id: [notification.id],
        customerId: notification?.customer?.id,
        supplierId: userInfo.id,
        admin: "admin",
      };

      try {
        const response = await commonAxios.put(
          "/cemo/messages/update/status",
          updateVal
        );
        console.log(notification?.booking, "notification?.booking")
        // if(notification?.booking){
        //   return 
        // }
        if (notification.isCustomerMessage) {
          navigate("/customer-messages", { state: { key: notification } });
        } else {
          navigate("/backpanel-support", { state: { key: notification } });
        }
      } catch (error) {
        console.log(error);
      }
    } else {

      // Admin Message handleShare flow 

      if (notification.isCustomerMessage) {

        const updateVal = {
          id: [notification.id],
          // customerId: notification?.customer?.id,
          // supplierId: null,
          adminId: "admin",
        };

        try {
          const response = await commonAxios.put(
            "/cemo/messages/update/status",
            updateVal
          );
          navigate("/customer-messages", { state: { key: notification } });
        } catch (error) {
          console.log(error);
        }
      } else {
        const updateVal = {
          id: [notification.id],
          // customerId: null,
          // supplierId: notification?.supplier?.id,
          adminId: "admin",
        };

        try {
          const response = await commonAxios.put(
            "/cemo/messages/update/status",
            updateVal
          );
          navigate("/backpanel-support", { state: { key: notification } });
        } catch (error) {
          console.log(error);
        }
      }
    }
    if (userInfo.role === "ROLE_CUSTOMER") {

      const updateVal = {
        id: [notification.id],
        customerId: notification.customer.id,
        supplierId: null,
        adminId: "admin",
      };
      try {
        const response = await commonAxios.put(
          "/cemo/messages/update/status",
          updateVal
        );
        if (location === "/ordershistory") {
          getNotificationsCount();
        }
        navigate("/ordershistory", { state: { key: notification } })
      } catch (error) {
      }
    }
  };

  const getNotificationsCount = () => {
    switch (userInfo.role) {
      case "ROLE_ADMIN":
        let valueAdmin: any = adminMessageCount + messageCount + commentCount;
        if (valueAdmin > 100) {
          return valueAdmin = '99+';
        } else {
          return valueAdmin
        }
      case "ROLE_SUPPLIER":
        let valueSupplier: any = supplierMessageCount + messageCount + commentCount;
        if (valueSupplier > 100) {
          return valueSupplier = '99+';
        } else {
          return valueSupplier
        }
      case "ROLE_CUSTOMER":
        let valueCustomer: any = customerMessageCount + messageCount + commentCount;
        if (valueCustomer > 100) {
          return valueCustomer = '99+';
        } else {
          return valueCustomer
        }
      default:
        let value: any = messageCount + commentCount;
        if (value > 100) {
          return value = '99+';
        } else {
          return value
        }
    }
    // return messageCount + commentCount;
  };

  useEffect(() => {
    updateNotification();
  }, [adminMessageCount, supplierMessageCount, customerMessageCount]);

  useEffect(() => {
    const handleScroll = () => {
      if (!props.scrollDown) {
        const scrollThreshold = 50;
        if (window.scrollY > scrollThreshold) {
          setIsModalVisible(false);
        } else {
          setIsModalVisible(true);
        }
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const location = window.location.pathname;

  useEffect(() => {
    getNotificationsCount();
  }, [])

  return (
    <>
      {isModalVisible && (
        <Menu placement="left-end">
          <MenuHandler>
            <section className="grid items-center">
              <Badge
                content={
                  <span>
                    {getNotificationsCount() ? getNotificationsCount() : 0}
                  </span>
                }
                className="bg-[#D6B27D] border border-white min-w-[12px] min-h-[12px] w-[20px] h-[20px]"
              >
                <img
                  src={
                    location === "/" ? notificationIcon : blackNotificationIcon
                  }
                  className="cursor-pointer w-[34px] h-[29px] inline-block"
                  style={{ objectPosition: '0', objectFit: 'cover' }}
                />
              </Badge>
            </section>
          </MenuHandler>
          <MenuList className="flex flex-col gap-2 max-h-[550px] mt-10 ml-10 h-auto overflow-y-scroll modal-popup w-[500px]">
            {allNotification?.length > 0 ? (
              allNotification?.map((notification: any, index: any) => {
                return index < 20 ? (
                  <MenuItem
                    key={index}
                    className={`${userInfo.role === "ROLE_SUPPLIER"
                      ? notification.isSupplierRead
                        ? "flex items-center gap-4 py-2 pr-8 pl-2 border-b bg-white "
                        : "flex items-center gap-4 py-2 pr-8 pl-2 border-b bg-[#FFE5D8]"
                      : userInfo.role === "ROLE_CUSTOMER"
                        ? notification.isCustomerRead
                          ? "flex items-center gap-4 py-2 pr-8 pl-2 border-b bg-white "
                          : "flex items-center gap-4 py-2 pr-8 pl-2 border-b bg-[#FFE5D8]"
                        : userInfo.role === "ROLE_ADMIN" && notification.isAdminRead
                          ? "flex items-center gap-4 py-2 pr-8 pl-2 border-b bg-white "
                          : "flex items-center gap-4 py-2 pr-8 pl-2 border-b bg-[#FFE5D8]"
                      } `}
                    onClick={() => handleNavigateClick(notification)}
                  >
                    <div className="flex justify-start">
                      {notification.isCustomerMessage ? (
                        notification.customer?.profileImage ? (
                          <img
                            className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                            src={notification.customer?.profileImage}
                            alt="Customer Profile"
                          />
                        ) : (
                          <span className="h-9 w-9 p-2 rounded-full flex justify-center items-center font-semibold bg-[#D6B27D] text-white mr-2 mb-2">
                            {`${notification.customer?.firstName.charAt(0).toUpperCase()}${notification.customer?.lastName.charAt(0).toUpperCase()}`}
                          </span>
                        )
                      ) : notification.isSupplierMessage ? (
                        notification.supplier?.profileImage ? (
                          <img
                            className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                            src={notification.supplier?.profileImage}
                            alt="Supplier Profile"
                          />
                        ) : (
                          <span className="h-9 w-9 p-2 ml-3 rounded-full flex justify-center items-center font-semibold bg-[#D6B27D] text-white mr-2 mb-2">
                            {`${notification.supplier?.firstName.charAt(0).toUpperCase()}${notification.supplier?.lastName.charAt(0).toUpperCase()}`}
                          </span>
                        )
                      ) : (
                        notification.admin?.profileImage ? (
                          <img
                            className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                            src={notification.admin?.profileImage}
                            alt="Admin Profile"
                          />
                        ) : (
                          <span className="h-9 w-9 p-2 rounded-full flex justify-center items-center font-semibold bg-[#D6B27D] text-white mr-2 mb-2">
                            {`${notification.admin?.firstName.charAt(0).toUpperCase()}${notification.admin?.lastName.charAt(0).toUpperCase()}`}
                          </span>
                        )
                      )}
                    </div>
                    <div className="flex flex-col gap-1">
                      <Typography
                        variant="small"
                        color="gray"
                        className="font-normal"
                      >
                        {notification.message}
                      </Typography>
                      <Typography
                        variant="small"
                        className="flex items-center gap-1 text-xs text-gray-600"
                      >
                        <ClockIcon className="h-3 w-3" />
                        {moment(
                          notification.updatedDate,
                          "YYYY-MM-DD HH:mm:ss"
                        ).format("DD/MM/YYYY HH:mm:ss")}
                      </Typography>
                    </div>
                  </MenuItem>
                ) : null;
              })
            ) : (
              <p className="font-semibold outline-none text-md flex justify-center items-center">
                No more Notifications
              </p>
            )}

          </MenuList>
        </Menu>
      )}
    </>
  );
};

export default Notifications;
