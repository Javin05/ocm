import React, { useEffect, useState } from 'react';
import { GoogleMap, InfoWindow, useJsApiLoader } from '@react-google-maps/api';
import { Marker } from '@react-google-maps/api';

interface Position {
  lat: number;
  lng: number
}
const containerStyle = {
  width: '100%',
  height: '375px',
  borderWidth: '2px',
  borderColor: '#FFE5D8',
  borderRadius: '14px'
};

const apiKey = 'AIzaSyDYsIF0ap_NrOL3xyjf1yB-3IkhN0O4lSI';

const MapView = ({ latitude, longitude, address }: any) => {
  console.log("locationData-->", latitude, longitude)
  const [city, setCity] = useState('');
  const [infoOpen, setInfoOpen] = useState(false);
  const [position, setPosition] = useState<any>(null);
  const [map, setMap] = React.useState<any>(null)
  const [zoom, setZoom] = useState(12);


  const onLoad = React.useCallback(function callback(map: google.maps.Map) {
    setPosition(new window.google.maps.LatLng(longitude, longitude));
    const bounds = new window.google.maps.LatLngBounds({ lat: latitude, lng: longitude });
    map.fitBounds(bounds);
    map.setZoom(18)
    setMap(map)
  }, [zoom])

  const onUnmount = React.useCallback(function callback(map: any) {
    setMap(null)
  }, [])

  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: apiKey
  })

  const handleMarkerClick = () => {
    setInfoOpen(!infoOpen);
  };

  return isLoaded ? (
    <div className='flex py-4 justify-center border-cyan-900'>
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={position}
        zoom={18}
        options={{
          zoomControlOptions: {
            position: window.google.maps.ControlPosition.RIGHT_CENTER,
          },
          zoom: 18,
          maxZoom: 18,
        }}
        onLoad={onLoad}
        onUnmount={onUnmount}
      >
        { /* Child components, such as markers, info windows, etc. */}
        <Marker position={{ lat: latitude, lng: longitude }} onClick={handleMarkerClick} >
          {infoOpen && (
            <InfoWindow onCloseClick={handleMarkerClick}>
              <div>
                <h2 className="">{address}</h2>
              </div>
            </InfoWindow>
          )}
        </Marker>
      </GoogleMap>
    </div>
  ) : <></>
}

export default MapView;