import { useNavigate } from "react-router-dom";
import breadcrumbArrow from "../../assets/breadcrumb-via.svg";
import { useState } from "react";

const BreadCrumb = ({ breadsList }: any) => {
  const navigate = useNavigate();

  const [sanitisedBreads, setSanitisedBreads] = useState(breadsList?.filter((bread: any) => bread.name !== 'undefined'))
  // onClick={() => navigate(`${bread.naviagteTo}`)}
  return (
    <section className="flex flex-row justify-start items-center gap-2 md:gap-3 mx-5 mb-3 md:mx-20">
      {sanitisedBreads?.map((bread: any, index: number) => {
        if (bread.name) {
          if (index === sanitisedBreads.length - 1) {
            return (
              <p className="font-medium text-[10px] md:text-[12px] lg:text-[15px] text-[#D6B27D] ">
                {bread.name}
              </p>
            );
          } else {
            return (
              <>
                {bread.naviagteTo ? (
                  bread.naviagteTo === "dynamicRoute" ? (
                    <p
                      className="font-medium text-[10px] md:text-[12px] lg:text-[15px] cursor-pointer text-secondary"
                      onClick={() => navigate(-1)}
                    >
                      {bread.name}
                    </p>
                  ) : (
                    <p
                      className="font-medium text-[10px] md:text-[12px] lg:text-[15px] cursor-pointer text-secondary"
                      onClick={() => navigate(`${bread.naviagteTo}`)}
                    >
                      {bread.name}
                    </p>
                  )
                ) : (
                  <p className="font-medium text-xl cursor-pointer text-secondary">
                    {bread.name}
                  </p>
                )}
                <img
                  src={breadcrumbArrow}
                  alt="breadcrumbArrow"
                  width={4}
                  height={10}
                />
              </>
            );
          }
        }
      })}
    </section>
  );
};

export default BreadCrumb;
