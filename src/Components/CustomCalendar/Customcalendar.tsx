import Calendar from 'react-calendar';
import "./Customcalendar.css";
import { Value, TileArgs } from 'react-calendar/dist/cjs/shared/types';
import moment from 'moment';
import { useEffect } from 'react';
import { failureNotify } from '../Toast/Toast';

const Customcalendar = ({ date, datesAvailableBookingStatus, setDate, datesBookingStatus, startDate, endDate }: any) => {
    const dayMap: any = {
        'Mon': 'Monday',
        'Tue': 'Tuesday',
        'Wed': 'Wednesday',
        'Thu': 'Thursday',
        'Fri': 'Friday',
        'Sat': 'Saturday',
        'Sun': 'Sunday'
    }

    const hasBookedEvent = (EventdDateToCheck: any) => {
        return datesBookingStatus.includes(moment(EventdDateToCheck).format('DD/MM/YYYY').toString());
    };

    const hasAvailableEvent = (EventdDateToCheck: any) => {
        return datesAvailableBookingStatus.includes(moment(EventdDateToCheck).format('DD/MM/YYYY').toString());
    }
    const getEventTile = (props: TileArgs) => {
        const { view, date } = props
        if (view === 'month' && hasAvailableEvent(date)) {
            return <div style={{ backgroundColor: 'green', width: '12px', height: '12px', borderRadius: '50%', margin: '4px auto 0px auto' }} />;
        } else if (view === 'month' && hasBookedEvent(date)) {
            return <div style={{ backgroundColor: 'red', width: '12px', height: '12px', borderRadius: '50%', margin: '4px auto 0px auto' }} />;
        }
        // else {
        //     return <div style={{ backgroundColor: 'transparent', width: '12px', height: '12px', borderRadius: '50%', margin: '4px auto 0px auto' }} />;
        // }
    };


    const currentDate: any = moment(new Date()).format('DD-MM-YYYY')

    return (
        <div className='custom-calendar'>
            <Calendar
                minDate={new Date()}
                maxDate={new Date(endDate)}
                // value={[new Date(), date]}
                formatShortWeekday={(locale, value) =>
                    ['S', 'M', 'T', 'W', 'T', 'F', 'S'][value.getDay()]
                }
                onChange={(value: any, event: React.MouseEvent<HTMLButtonElement>) => {
                    const today = new Date(value);
                    let monthFormatted =
                        today.getMonth() < 10 ? `0${today.getMonth() + 1}` : today.getMonth() + 1;
                    let dateFormatted =
                        today.getDate() < 10 ? `0${today.getDate()}` : today.getDate();
                    const weekDay = `${today.getFullYear()}-${monthFormatted}-${dateFormatted}`;

                    const dateToCheck = moment(weekDay);
                    const todayDate = moment().startOf('day');

                    if (dateToCheck.isBefore(todayDate)) {
                        failureNotify('Past dates cannot be booked')
                    } else {
                        // setDate((prev: Date) => {
                            // var abbrTags = document.querySelectorAll('abbr');
                            // abbrTags?.forEach((abbr: HTMLElement) => {
                            //     // if (abbr.ariaLabel === dayMap[prev?.toString()?.split(' ')[0]]) {
                            //     //     abbr.parentElement!.classList.remove('react-calendar__month-view__weekdays__weekday--current')
                            //     // }

                            //     // if (abbr.ariaLabel === dayMap[value!.toString()?.split(' ')[0]]) {
                            //     //     abbr.parentElement!.classList.add('react-calendar__month-view__weekdays__weekday--current')
                            //     // }
                            // })
                            setDate(value);
                            return value;
                        // })
                    }

                }}
                // onActiveStartDateChange={({ action, activeStartDate, value, view }) => }
                // allowPartialRange={true}
                tileContent={getEventTile}
                tileClassName={({ date, view }) => {
                    if (currentDate === moment(date).format("DD-MM-YYYY")) {
                        return 'highlight'
                    }
                }}
            // selectRange={true}
            // activeStartDate={new Date()}
            // defaultActiveStartDate={new Date()}
            // minDate={new Date()}
            />
        </div>
    )
}

export default Customcalendar