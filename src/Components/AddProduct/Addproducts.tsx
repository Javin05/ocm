import React, { useEffect, useState, useRef } from "react";
import { commonAxios, formAxios } from "../../Axios/service";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { failureNotify, successNotify } from "../Toast/Toast";
import deleteIcon from "../../assets/Delete.png";
import Select from "react-select";
import { RxCross2 } from "react-icons/rx";
import "./addproduct.css";
type initialProductValues = {
  name: string;
  description: string;
  price: string;
  quantity: string;
  category: string;
  taxId: string;
  files: string[];
  // companyName: string;
};
interface TaxOption {
  id: string;
  taxPercentage: number;
}
const AddProducts = ({
  setIsAddProductModalOpen,
  setIsLoading,
  setCustomerData,
  seteventData,
  isLoading,
  productsData,
  addEventModalHeader,
  getProductsList,
  getProductsListBySupplier,
  setIsModalOpen,
}: any) => {
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [momsCalculation, setMomCalculation] = useState<any>("");
  const [base64Image, setBase64Image] = useState<any>("");
  const [error, setError] = useState<any>("");
  const [errors, setErrors] = useState<any>([
    {
      name: "",
      quantity: "",
      // companyName: "",
      description: "",
      price: "",
      category: "",
      taxId: "",
      files: [],
    },
  ]);

  const [formData, setFormData] = useState<any>([
    {
      name: "",
      description: "",
      price: "",
      category: "",
      taxId: "",
      files: [],
      // companyName: "",
    },
  ]);
  const [categoryerror, setCategoryerror] = useState<any>("");
  const [options, setOptions] = useState<any>([]);
  const [productCategory, setProductCategory] = useState<any>(null);
  const [moms, setMoms] = useState<any>([]);
  const [fileError, setfileError] = useState<any>("");
  const [fileProduct, setfileProduct] = useState<any>([]);
  const [videoUrl, setVideoUrl] = useState("");
  const [uploadProgress, setUploadProgress] = useState<any>("");
  const [video, setVideo] = useState<any>("");
  const [showControls, setShowControls] = useState<boolean>(false);
  const [videoPlay, setVideoPlay] = useState<boolean>(false);
  const videoRef = useRef<any>(null);
  useEffect(() => {
    if (productsData) {
      const categories = productsData?.categories?.map((item: any) => item.id)
      console.log('productsData---->', productsData)
      setFormData(productsData);
      setProductCategory(categories);
      setMomCalculation(productsData.taxType?.id);
      let multipleImage;
      if (productsData.image) {
        let splitedImage = productsData.image.split(",");
        if (splitedImage) {
          multipleImage = splitedImage.map((val: any, i: any) => {
            return {
              id: i + 1,
              values: val,
            };
          });
        }
      }
      setBase64Image(multipleImage);
      setfileProduct(multipleImage)
    }
  }, []);


  const handleFileChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    max: number,
    min: number
  ) => {
    event.preventDefault();
    const files: FileList | null = event.target.files;
    const updatedFiles: File[] = [];
    if (!files) {
      return;
    }
    const allowedExtensions = ['.jpg', '.jpeg', '.png', '.gif'];
    let totalUploadedImages = base64Image ? base64Image.length : 0;
    let errorOccurred = false;
    Array.from(files).forEach((file: File) => {
      const fileName = file.name.toLowerCase();
      const isImageFile = allowedExtensions.some(ext => fileName.endsWith(ext));
      if (isImageFile) {
        if (totalUploadedImages < max) {
          const reader = new FileReader();
          reader.onload = (event: any) => {
            const base64String = event.target.result;
            setBase64Image((prevImages: any[]) => {
              prevImages = prevImages || [];
              return [...prevImages, { id: Date.now(), values: base64String, }];
            });

            setError("");
          };
          reader.onerror = (error) => {
            failureNotify('Error reading the file.');
            errorOccurred = true;
          };
          reader.readAsDataURL(file);
          updatedFiles.push(file);
          totalUploadedImages++;
        } else if (!errorOccurred) {
          failureNotify(`You can upload a maximum of ${max} files.`);
          errorOccurred = true;
        }
      } else {
        failureNotify(`File '${file.name}' is not a valid image file (jpg, jpeg, png, gif).`);
        errorOccurred = true;
      }
    })
    setfileProduct((prevFiles: any) => [...(prevFiles || []), ...updatedFiles]);
  }

  const handleVideoFileChange = (e: any) => {
    const video = e.target.files[0];
    setVideo(video);

    const reader = new FileReader();

    reader.onloadend = () => {
      const videoData: any = reader.result;

      setVideoUrl(videoData);
    };
    reader.onprogress = (progressEvent) => {
      if (progressEvent.lengthComputable) {
        const progressPercent: any = Math.round(
          (progressEvent.loaded / progressEvent.total) * 100
        );
        setUploadProgress(progressPercent);
      }
    };

    reader.readAsDataURL(video);
  };

  const handleCategoryChange = (selectedOptions: any) => {
    const selectedValues = selectedOptions.map((option: any) => option.value);
    setProductCategory(selectedValues)
    setCategoryerror("");
  };

  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setFormData((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    if (!formData.name) {
      setErrors({ name: "Obligatorisk" });
    } else if (!formData.description) {
      setErrors({ description: "Obligatorisk" });
    }
    // else if (!formData.companyName) {
    //   setErrors({ companyName: "Obligatorisk" });
    // }
    else if (!momsCalculation) {
      setErrors({ taxId: "Obligatorisk" });
    } else if (!formData.quantity) {
      setErrors({ quantity: "Obligatorisk" });
    } else if (!Math.max(0, formData.quantity)) {
      setErrors({ quantity: "Quantity cannot be negative" });
    }

    else if (!formData.price) {
      setErrors({ price: "Obligatorisk" });
    }
    else if (!Math.max(0, formData.price)) {
      setErrors({ price: "Price cannot be negative" });
    }
    if (!productCategory || productCategory.length === 0) {
      setCategoryerror("Obligatorisk");
    } else if (!fileProduct) {
      setfileError("Obligatorisk");
    } else {
      setIsLoading(true);

      let value: any;
      let productBody = {}
      if (fileProduct.length > 0) {
        let fileArray: string[] = [];
        for (let i = 0; i < fileProduct.length; i++) {
          fileArray.push(fileProduct[i]);
        }
        const formDatas = new FormData();
        fileArray.forEach((file: any, i: any) => {
          formDatas.append(`file`, file);
        });
        formDatas.append("folder", "products");

        console.log("formDatas", formDatas)

        value = await formAxios.post("/cemo/upload/files", formDatas);

        // const formVideo = new FormData();
        // formVideo.append("file", video);
        // formVideo.append("folder", "video");

        // let videovalue = await formAxios.post("/cemo/upload/files", formVideo);

        if (value?.data) {
          productBody = {
            price: Number(formData.price),
            image: value.data.join(","),
            appUserId: userInfo.id,
            categoryId: productCategory,
            name: formData.name,
            // companyName: formData.companyName,
            description: formData.description,
            quantity: Number(formData.quantity),
            taxId: momsCalculation,
          };
        }
      } else if (base64Image?.length > 0) {
        productBody = {
          price: Number(formData.price),
          image: base64Image && base64Image.map((item: any) => item.values).join(', '),
          appUserId: userInfo.id,
          categoryId: productCategory,
          name: formData.name,
          // companyName: formData.companyName,
          description: formData.description,
          quantity: Number(formData.quantity),
          taxId: momsCalculation,
        };
      } else {
        failureNotify('Produktbild är obligatorisk')
        setIsLoading(false)
      }

      if (fileProduct?.length > 0 || base64Image?.length > 0) {
        if (addEventModalHeader === "Save Product") {
          try {
            const response = await commonAxios.put(
              `/cemo/products/${productsData.id}`,
              productBody
            );
            console.log("response", response)

            successNotify("Produkten har ändrats");
            setIsAddProductModalOpen(false);
            if (userInfo.role === "ROLE_ADMIN") {
              getProductsList();
            } else {
              getProductsListBySupplier();
            }
          } catch (error) {
          } finally {
            setIsModalOpen(false);
            setIsLoading(false);
          }
        } else {
          try {
            const response = await commonAxios.post(
              "/cemo/products/with/event",
              productBody
            );
            console.log("response2", response)

            successNotify("Produkten har lagts till");
            setIsAddProductModalOpen(false);
            if (userInfo.role === "ROLE_ADMIN") {
              getProductsList();
            } else {
              getProductsListBySupplier();
            }
          } catch (error) {
          } finally {
            setIsLoading(false);
          }
        }
      }
    }
  };


  const getEventCategories = async () => {
    const response = await commonAxios.get("/cemo/categories");
    if (response.status === 200) {
      // Filter categories with status 'ACTIVE'
      const activeCategories = response.data.filter((category: any) => category.status === "ACTIVE");

      const categoriesList = activeCategories.map((category: any) => ({
        value: category.id,
        label: category.categoryName,
        id: category.id,
      }));

      setOptions(categoriesList);
    }
  };

  const getMoms = async () => {
    const response = await commonAxios.get("/cemo/taxtypes/all");
    if (response.data) {
      setMoms(response.data);
    }
  };

  const handleMomsChange = (e: any) => {
    setMomCalculation(e.target.name);
  };

  useEffect(() => {
    getEventCategories();
    getMoms();
  }, []);

  const deleteImage = (index: number) => {
    const updatedImage = base64Image.filter((img: any, idx: number) => {
      return idx !== index;
    });
    setBase64Image(updatedImage);
  };


  const handleMouseEnter = () => {
    setShowControls(true);
    videoRef.current!.play();
  };

  const handleMouseLeave = () => {
    setShowControls(false);
    setVideoPlay(true);
    videoRef.current.pause();
  };

  console.log('base64Image', base64Image)

  return (
    <div className="w-full h-full bg-[#00000080] absolute z-40 right-0">
      <section className="w-full h-full flex flex-col items-center justify-center text-secondary">
        <form className="bg-[#ffffff] w-[40rem] h-4/5 max-h-4/5 overflow-y-scroll px-8 py-6 relative">
          <button
            onClick={(e) => {
              e.preventDefault();
              setIsAddProductModalOpen(false);
              setIsModalOpen(false);
              setCustomerData([]);
              // seteventData([]);
            }}
          >
            <img src={deleteIcon} className="absolute top-5 right-5 h-6 w-6" />
          </button>
          <h2 className="mb-4 text-lg font-semibold text-center font-[sofiapro]">
            {addEventModalHeader !== 'Save Product' ? 'Lägg till ny produkt' : 'Edit Product'}
          </h2>
          <label className="flex font-semibold">Produktnamn*</label>
          <input
            type="text"
            name="name"
            id="name"
            placeholder="Namn"
            className="outline-none border-[1px] border-gray-300 my-2 w-full h-10 bg-transparent rounded-[10px]
            px-4 py-2"
            onChange={handleChange}
            value={formData.name}
            onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
          />
          {
            errors.name && <p className="text-red-500">{errors.name}</p>
          }

          <label className="flex mt-3 font-semibold">Produktbeskrivning*</label>
          <textarea
            rows={6}
            name="description"
            id="description"
            placeholder="Beskrivning"
            className="outline-none border-[1px] rounded-lg border-gray-300 my-1 px-2 pt-2 w-full bg-transparent"
            onChange={handleChange}
            value={formData.description}
          />
          {errors.description && (
            <p className="text-red-500">{errors.description}</p>
          )}
          {/* <label className="flex mt-3 font-semibold">Företagsnamn*</label> */}
          {/* <input
            type="text"
            name="companyName"
            id="companyname"
            placeholder="Företagsnamn"
            className="outline-none border-[1px] border-gray-300 my-2 w-full h-10 bg-transparent rounded-[10px]
            px-4 py-2"
            onChange={handleChange}
            value={formData.companyName}
          />
          {errors.companyName && (
            <p className="text-red-500">{errors.companyName}</p>
          )} */}
          <label className="flex font-semibold mt-3">Välj kategorier*</label>
          <div onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}>
            <Select
              className="py-3 text-secondary placeholder-secondary  md:w-full  outline-none leading-tight mb-2 text-sm"
              placeholder="Välj kategorier"
              name="category"
              onChange={handleCategoryChange}
              // value={productCategory}
              value={options.filter((option: any) =>
                productCategory?.includes(option.value)
              )}
              options={options}
              isMulti={true}
            />
          </div>

          {categoryerror && <p className="text-red-500">{categoryerror}</p>}
          <label className="flex font-semibold">Välj Momssats*</label>
          <div className="my-4 flex flex-row w-1/3 justify-between">

            {moms?.map((option: TaxOption) => (
              <label key={option.id}>
                <input
                  type="radio"
                  name={option.id}
                  className="mr-2 flex gap-4"
                  onChange={handleMomsChange}
                  checked={momsCalculation === option.id}
                  onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                />
                {option.taxPercentage}%
              </label>
            ))}
          </div>
          {errors.taxId && <p className="text-red-500">{errors.taxId}</p>}
          <div className="w-full">
            <label
              className="border-[1px] block w-full h-15 rounded-lg text-center my-5 p-3"
              htmlFor="videoInput"
              id="customLabel"
            >
              Ladda Upp bild
            </label>
          </div>
          <div className="my-2">
            <input
              id="videoInput"
              type="file"
              multiple
              accept=".jpg, .jpeg, .png, .gif"
              onChange={(e) => handleFileChange(e, 4, 1)}
            />
            {error && <p className="text-red-500">{error}</p>}
            {fileError && <p className="text-red-500">{fileError}</p>}
            <div className="flex my-5">
              {base64Image &&
                base64Image.map((imageurl: any, i: any) => {
                  return (
                    <>
                      <div className="relative ">
                        <img
                          height={200}
                          width={200}
                          className="px-3"
                          src={imageurl.values}
                          style={{ height: '200px', width: '200px' }}
                        />
                        <RxCross2
                          className="cursor-pointer m-2 absolute top-2 right-2 bg-red-300"
                          onClick={(id) => {
                            deleteImage(i);
                          }}
                        />
                      </div>
                    </>
                  );
                })}
            </div>
          </div>
          {/* {!videoUrl && (
            <>
              <div className="w-full">
                <label
                  className="border-[1px] block w-full h-15 rounded-lg text-center my-5 p-3"
                  htmlFor="fileInput"
                  id="customLabel"
                >
                  Ladda upp video
                </label>
              </div>
              <div className="my-2">
                <input
                  id="fileInput"
                  type="file"
                  accept="video/*"
                  onChange={(e) => handleVideoFileChange(e)}
                />
              </div>
            </>
          )} */}
          {/* {uploadProgress > 0 && uploadProgress < 100 && (
            <div className="flex justify-center">
              <progress value={uploadProgress} max="100" />
              <span className="mx-5">{`${uploadProgress}%`}</span>
            </div>
          )} */}
          {/* {videoUrl && (
            <div className="relative flex justify-center">
              <video
                className=""
                src={videoUrl}
                width={300}
                controls={showControls}
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
                ref={videoRef}
              />
              <RxCross2
                className="absolute top-2 right-36 bg-[white] cursor-pointer "
                onClick={() => setVideoUrl("")}
              />
            </div>
          )} */}
          <label className="flex mt-3 font-semibold">Product Antal*</label>
          <input
            type="number"
            name="quantity"
            placeholder="Antal"
            className="outline-none border-[1px] rounded-lg border-gray-300 my-2 px-2 w-full h-10 bg-transparent"
            onChange={handleChange}

            value={formData.quantity}
            onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
          />
          {errors.quantity && <p className="text-red-500">{errors.quantity}</p>}
          <label className="flex mt-3 font-semibold">Produktpris*</label>
          <input
            type="number"
            name="price"
            placeholder="Pris"
            className="outline-none border-[1px] rounded-[10px] border-gray-300  my-2 px-4 py-2 w-full h-10 bg-transparent"
            onChange={handleChange}
            value={formData.price}
            onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
          />
          {errors.price && <p className="text-red-500">{errors.price}</p>}
          <div className="flex justify-center">
            <button
              type="submit"
              onClick={handleSubmit}
              disabled={isLoading}
              className="px-4 py-2 md:mr-4 bg-[#D6B27D] rounded-lg  mt-6 text-md font-semibold cursor-pointer text-white uppercase"
            >
              {addEventModalHeader === "Save Product"
                ? "Spara"
                : "Lägg till produkt"}
            </button>
          </div>
        </form>
      </section>
    </div>
  );
};
export default AddProducts;
