import React from 'react'
import { useSelector } from 'react-redux';
import { Navigate, Route } from 'react-router';
import { RootState } from '../../Redux/store';

const ProtectedRoute = ({ component }: any) => {
    // const userDetails = useSelector((state: RootState) => state.user.userDetails)
    // const isUserAuthenticated = userDetails.id !== '';
    const isUserAuthenticated = true;
    return isUserAuthenticated ? component : <Navigate to='/login' replace={true} />
}

export default ProtectedRoute