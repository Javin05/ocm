import {
  useNavigate,
  useSearchParams,
  createSearchParams,
} from "react-router-dom";

const RelatedProductCard = ({
  image,
  category,
  price,
  cardName,
  product,
  onClick,
  loggedInUser,
}: any) => {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();

  return (
    <div
      className={`bg-[#FBF3F3] rounded-xl md:ml-16 w-full h-[500px] md:h-[370px] lg:h-[420px] border mb-10 shadow-2xl`}
    >
      <div
        className="w-full h-[306px] lg:h-[300px] md:h-[200px] mb-4 p-2"
        onClick={() => {
          const params = {
            id: product?.appUser?.id,
            productId: product?.id,
            searchText: searchParams.get("searchText") as string,
          };
          navigate({
            pathname: "/cart",
            search: `?${createSearchParams(params)}`,
          });
        }}
      >
        <img
          className="w-full h-full object-cover rounded-xl"
          src={image}
          alt={image}
        />
      </div>
      <div className="px-2 sm:-mt-6">
        <div className="flex flex-row justify-between items-center">
          <span className="text-[#D6B37D] rounded-xl font-semibold px-4 md:py-2 bg-[#FFC12320] my-4">
            {category}
          </span>
          <p className="text-[20px] my-3 text-center text-secondary">{cardName}</p>
          <span className="font-bold text-secondary">{price}</span>
        </div>

        <div className="w-full text-center">
          <button
            onClick={() => onClick(product)}
            className="bg-[#D6B27D] font-bold w-2/3 inline-block py-3 md:text-[20px] uppercase text-sm rounded-xl text-white"
          >
            Lägg till i varukorg
          </button>
        </div>
      </div>
    </div>
  );
};

export default RelatedProductCard;
