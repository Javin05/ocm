import moment from "moment";
import { useEffect, useState } from "react";

const OperatingHoursCard = ({
  operatingHours,
  setSlotsForActiveOperatingHrs,
}: any) => {
  console.log(operatingHours, "in operatingHours");
  const [active, setActive] = useState<any>(null);
  useEffect(() => {
    setActive(null)
  }, [operatingHours])
  return (
    <>
      {operatingHours.map((e: any, index: any) => (
        <div
          className={`cursor-pointer py-8 border-b-2 ${index === active ? 'bg-[#FFE5D8]' : null}`}
          onClick={() => {
            setSlotsForActiveOperatingHrs(e)
            setActive(index)
          }}
        >
          <div className="flex items-center gap-2">
            <div className="flex justify-center items-center">
              <p className="ml-2 text-golden text-[16px] cursor-pointer mr-2">
                {e.operatingDate}
              </p>-
              <p className="ml-2 mr-2">{e.weekDays.toUpperCase()}</p>
            </div>

          </div>
          <div className="ml-2">
            <p className="text-sm">
              {moment(e.startTime, "hh:mm A").format("HH:mm")} -{" "}
              {moment(e.endTime, "hh:mm A").format("HH:mm")}
            </p>
          </div>
        </div>
      ))}
    </>
  );
};

export default OperatingHoursCard;
