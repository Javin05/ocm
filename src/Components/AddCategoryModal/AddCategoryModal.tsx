import react from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import deleteIcon from "../../assets/Delete.png";
import { commonAxios } from "../../Axios/service";
import moment from "moment";
import { failureNotify, successNotify } from "../Toast/Toast";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";


interface initialValues {
  categoryName: string;
  image: string;
  categoryPrice: string;
  createdAt: string;
}

const AddCategoryModal = ({
  setIsModalOpen,
  getCategoryList,
  content,
  categoryData,
  editPopup,
}: any) => {
  let base64Image: string | undefined;
  const loggedInUser = useSelector((state: RootState) => state.user.userDetails);
  const addNewCategory = async (values: initialValues) => {
    console.log(values, "specialvalues");
    if (content === "Edit category") {
      const eventBody: any = {
        id: categoryData.id,
        categoryName: values.categoryName,
        // categoryPrice: Number(values.categoryPrice),
        createdAt: values.createdAt,
        // image: values.image,
      };

      try {
        const response = await commonAxios.put(
          `cemo/categories/${categoryData.categoryName}`,
          eventBody
        );
        if (response.data) {
          successNotify("Category updated successfully");
          getCategoryList();
        }
      } catch (err) {
        failureNotify("Oops! something went wrong!");
      }
    } else {
      const eventBody: any = {
        categoryName: values.categoryName,
        // categoryPrice: Number(values.categoryPrice),
        supplierId: loggedInUser.id,
        // image: "base64",
      };

      console.log(eventBody, "eventbodypost");
      try {
        const response = await commonAxios.post("/cemo/categories", eventBody);
        if (response.data.errorErrorMessage){
          failureNotify("Category already exists."); 
        } else {
          successNotify("Category added successfully");
          getCategoryList();
        }
      } catch (error) {
        failureNotify("Oops! something went wrong!");
      }
    }
    setIsModalOpen(false);
  };

  const renderEditData = () => {
    console.log(categoryData, "categorydata");
    const initialValues = {
      categoryName: categoryData.categoryName,
      image: categoryData.image,
      categoryPrice: categoryData.categoryPrice,
      createdAt: categoryData.createdAt,
    };
    return initialValues;
  };

  const formik = useFormik<initialValues>({
    initialValues:
      content === "Edit category"
        ? renderEditData()
        : {
          categoryName: "",
          image: "",
          categoryPrice: "",
          createdAt: "",
        },

    validationSchema: Yup.object({
      categoryName: Yup.string().required("Obligatorisk"),
      // image: Yup.string().required("Required"),
      // categoryPrice: Yup.string().required("Required"),
      // createdAt: Yup.string().required("Required"),
    }),
    onSubmit: (values) => addNewCategory(values),
  });

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        event.preventDefault();
        const imageUrl = reader.result?.toString();
        base64Image = imageUrl;
      };
    }
  };
  return (
    <>
      <div className="w-full h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <form
            onSubmit={formik.handleSubmit}
            className="bg-[#ffffff] w-[40rem] h-auto px-8 py-6 relative"
          >
            <button onClick={() => setIsModalOpen(false)}>
              <img src={deleteIcon} className="absolute right-5 h-6 w-6 -mt-6" />
            </button>
            <h2 className="mb-4 text-lg font-semibold text-secondary">Lägg till kategori</h2>

            <div className="mt-4 flex text-secondary">
              <div>
                <label className="mx-4 font-semibold" htmlFor="categoryName">
                Kategorinamn 
                </label>
                <input
                  type="text"
                  name="categoryName"
                  id="categoryName"
                  placeholder="Kategorinamn"
                  className="mt-3 outline-[#97A0C3] px-4 border-2 py-2 h-10 rounded-[10px]"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.categoryName}
                />
                {formik.touched.categoryName && formik.errors.categoryName ? (
                  <div className="text-red-500 ml-[160px] mt-3">{formik.errors.categoryName}</div>
                ) : null}
              </div>
              {/* <div>
                <label className="mt-1" htmlFor="categoryPrice">
                  Category Price
                </label>
                <input
                  type="text"
                  name="categoryPrice"
                  id="categoryPrice"
                  placeholder="price"
                  className="mt-3 rounded-lg outline-[#97A0C3] px-3 w-full border-2 h-10"
                  onChange={formik.handleChange}
                  value={formik.values.categoryPrice}
                />
              </div> */}
            </div>
            <div className="mt-4">
              {/* <label className="mt-4" htmlFor="eventImage">
                Upload a Image
              </label> */}
              <div className="flex flex-col items-center">
                {" "}
                {/* <label className="relative rounded-md py-2 px-4 cursor-pointer">
                  <span className="absolute inset-0 flex items-center">
                    <svg
                      className="h-6 w-6 text-gray-500"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M12 6v6m0 0v6m0-6h6m-6 0H6"
                      ></path>
                    </svg>
                  </span>
                  <span className="pl-7">Ladda upp bild</span>
                  <input
                    type="file"
                    className="absolute rounded-lg outline-[#97A0C3] inset-0 opacity-0 cursor-pointer"
                  />
                </label> */}
                {/* <input
                  type="file"
                  name="image"
                  id="eventImage"
                  placeholder="Event Image"
                  classNameName="mt-5 rounded-lg outline-[#97A0C3] px-3 w-full h-20 border-2"
                  onChange={handleFileChange}
                /> */}
              </div>
            </div>
            <div className="flex justify-center">
              <button
                type="submit"
                // disabled={isLoading}
                className="px-6 p-2 w-auto uppercase md:mr-4 bg-[#D6B27D] text-white rounded-lg mt-6 text-md font-semibold cursor-pointer"
              >
              Lägg till
              </button>
            </div>
          </form>
        </section>
      </div>
    </>
  );
};

export default AddCategoryModal;
