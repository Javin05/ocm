import React, { useEffect, useState } from "react";
import Calendar from "react-calendar";
import "./ScheduleCalendar.css";
import "react-calendar/dist/Calendar.css";
import moment from "moment";

const SchduleCalendar = ({ selectedDates, setSelectedDates }: any) => {
  const [currentMonth, setCurrentMonth] = useState(new Date());
  const [nextMonth, setNextMonth] = useState(
    new Date(currentMonth.getFullYear(), currentMonth.getMonth() + 1, 1)
  );

  const handleDateChange = (date: any) => {
    console.log("selectedDates --->", date);
    if (selectedDates.length === 0) {
      setSelectedDates([...date]); // Set the start date
    } else if (selectedDates.length === 1) {
      // Check if the selected date is after the start date
      if (date.length > selectedDates[0]) {
        setSelectedDates([...selectedDates, date]); // Set the end date
      } else {
        setSelectedDates([...date]); // Reset and set new start date
      }
    } else {
      setSelectedDates([...date]); // Reset and set new start date
    }
  };

  const getFormattedDate = (date: string) => {
    const formattedDate = new Date(date);
    return moment(formattedDate).format("YYYY-MM-DD");
  };

  console.log(currentMonth, "currentMonth");
  console.log(nextMonth, "nextMonth");
  return (
    <div className="schedule-calendar">
      <div className="">
        <div className="calendar mb-10 mt-5">
          <h2 className="text-[16px] font-semibold py-2 text-secondary">
            {currentMonth.toLocaleString("default", {
              month: "long",
              year: "numeric",
            })}
          </h2>
          <Calendar
            value={selectedDates}
            selectRange={true}
            onChange={handleDateChange}

          // minDate={selectedDates.length > 0 ? selectedDates[0] : currentMonth}
          // maxDate={nextMonth}
          />
        </div>
        {selectedDates.length === 2 && (
          <div className="my-6">
            <small className="text-sm text-secondary font-semibold">
              <span className="text-golden">Startdatum:</span>{getFormattedDate(selectedDates[0])} </small> <br />
            <small className="text-sm text-secondary font-semibold">
              <span className="text-golden">Slutdatum:</span>{getFormattedDate(selectedDates[1])}</small>
          </div>
        )}
      </div>
    </div>
  );
};

export default SchduleCalendar;
