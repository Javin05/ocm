import React from 'react'
import closeImg from "../../assets/Close.svg";
const CartCard = ({ cartImg, cartTitle, cartRs }: any) => {
    return (
        <div className='flex w-[100%]'>
            <div className='w-[40%]'>
                <img src={cartImg} className="md:h-36 md:w-36 mt-10" />
            </div>
            <div className='flex flex-col justify-center w-[40%]'>
                <h2 className='font-semibold text-[10px] mt-10 md:mt-0 ml-3 md:ml-0  md:text-[20px]'>{cartTitle}</h2>
                <p className='mt-3 font-semibold text-[10px] md:text-[16px] md:mt-0 ml-3 md:ml-0 '>{cartRs}</p>
            </div>
            <div className='w-[20%] flex items-center mt-5 md:mt-0 ml-2 md:ml-0'>
                <button className='border p-3 rounded-full'><img src={closeImg} className=" " /></button>
            </div>
        </div>
    )
}

export default CartCard