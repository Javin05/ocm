import React from "react";

const NewsLetter = () => {
  return (
    <div className="px-5 sm:px-32 md:px-20">
      <div className="bg-[#FFE5D8] h-[40vh] rounded-lg md:rounded-[50px] flex flex-col justify-center items-center mt-20 lg:mt-0">
        <div className="">
          <h1 className="text-[#D6B37D] text-[16px] mt-10 italic font-semibold text-center">
            {" "}
            -Vårat nyhetsbrev
          </h1>
          <h1 className="md:text-[30px] font-semibold md:mt-0 text-secondary">
            Prenumerera på vårat nyhetsbrev
          </h1>
        </div>
        <div className="">
          <input
            type="email"
            className="py-3 focus:outline-none rounded-full mx-10 lg:mx-6 w-100px md:w-[500px] lg:w-[400px] mt-2 px-4 lowercase"
            placeholder="Din e-post"
          />
          <button className="bg-[#D6B27D] uppercase py-1.5 px-6 text-lg rounded-lg font-bold font-[sofiapro] text-white text-center ml-[110px] md:ml-[250px] lg:ml-[0px] mt-3 sm:mt-2 mb-2">
            Skapa konto
          </button>
        </div>
      </div>
    </div>
  );
};

export default NewsLetter;
