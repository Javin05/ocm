import React, { ChangeEvent, useEffect, useState } from "react";
import { commonAxios } from "../../Axios/service";
import { useFormik } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import { successNotify } from "../Toast/Toast";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import deleteIcon from "../../assets/Delete.png";
import fileupload from "../../assets/file-upload.svg";

type initialproductValues = {
  name: string;
  description: string;
  businessCity: string;
  businessPinCode: string;
  eventStart: string;
  eventEnd: string;
  categories: string;
  slothours: string;
  slotRegister: slotregister;
  price: string;
  quantity: string;
  maxcount: string;
};

type slotregister = slotRegisters[];
interface slotRegisters {
  intime: string;
  outtime: string;
  day: string;
  days: string;
}

const AddProductsModal = ({
  isLoading,
  options,
  setIsModalOpen,
  getProductsList,
  getProductsListBySupplier,
  addProductsModalHeader,
  productData,
  productsHeader,
  productsData,
}: any) => {
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [file, setFile] = useState<File | null>(null);
  const [base64Image, setBase64Image] = useState(productData.image);
  const weekSlots = [
    {
      days: "Mondag",
      day: "",
      intime: "",
      outtime: "",
    },
    { days: "Tisdag", day: "", intime: "", outtime: "" },
    {
      days: "Onsdag",
      day: "",
      intime: "",
      outtime: "",
    },
    { days: "Torsdag", day: "", intime: "", outtime: "" },
    {
      days: "Fredag",
      day: "",
      intime: "",
      outtime: "",
    },
    {
      days: "Lördag",
      day: "",
      intime: "",
      outtime: "",
    },
    {
      days: "Söndag",
      day: "",
      intime: "",
      outtime: "",
    },
  ];
  console.log("AddEventsModal", productData);

  const addNewEvent = async (productValue: initialproductValues) => {
    let choosed_array = productValue.slotRegister.filter(
      (value: any) => value.day === true
    );

    if (
      productValue.slothours === "1" ||
      productValue.slothours === "ONE_HOUR"
    ) {
      productValue.slothours = "ONE_HOUR";
    } else if (
      productValue.slothours === "2" ||
      productValue.slothours === "TWO_HOUR"
    ) {
      productValue.slothours = "TWO_HOUR";
    } else if (
      productValue.slothours === "3" ||
      productValue.slothours === "THREE_HOUR"
    ) {
      productValue.slothours = "THREE_HOUR";
    } else {
      productValue.slothours = "FOUR_HOUR";
    }
    let operatingHours = choosed_array.map((val) => {
      const main = {
        hourSlot: "ONE_HOUR",
        weekDay: val.days,
        startTime: val.intime,
        endTime: val.outtime,
      };
      return main;
    });

    const businessCityName = productValue.businessCity;

    fetch(
      `https://nominatim.openstreetmap.org/search?q=${businessCityName}&format=json&limit=1`
    )
      .then((response) => response.json())
      .then((data) => {
        // const latitude = data[0].lat;
        // const longitude = data[0].lon;
        // console.log(`Latitude: ${latitude}, Longitude: ${longitude}`);
      })
      .catch((error) => console.error(error));

    const productBody: any = {
      name: productValue.name,
      description: productValue.description,
      businessCity: productValue.businessCity,
      businessPinCode: productValue.businessPinCode,
      supplierId: userInfo.id,
      image: base64Image,
      operatingHours: operatingHours,
      categories: {
        categoriesName: productValue.categories,
      },
      price: productValue.price,
      maxCount: productValue.maxcount,
      startDate: productValue.eventStart,
      endDate: productValue.eventEnd,
    };
    console.log(productBody, "productBody");
    if (addProductsModalHeader === "Save Product") {
      productBody.id = productData.id;
    }

    const URL =
      addProductsModalHeader === "Save Product"
        ? `/cemo/products/all/update/${productData.id}`
        : "/cemo/products/all/add";
    console.log(productData.id, "ProductId");
    if (addProductsModalHeader === "Save Product") {
      const response = await commonAxios.put(URL, JSON.stringify(productBody));
      if (response.data) {
        successNotify("Product edited successfully");

        if (userInfo.role === "ROLE_ADMIN") {
          console.log("Product List shown here");
          getProductsList();
        } else {
          getProductsListBySupplier();
        }
      }
    } else {
      const response = await commonAxios.post(URL, JSON.stringify(productBody));
      const params = {
        message: productBody && productBody.name,
        customerId: null,
        supplierId: userInfo && userInfo.id == 'admin' ? '' : userInfo.id,
        adminId: "admin",
        isCustomerMessage: false,
        isSupplierMessage: userInfo && userInfo.id == 'admin' ? false : true,
        isAdminMessage: userInfo && userInfo.id == 'admin' ? true : false
      }
      const messageURL = "/cemo/products"
      const messageData = commonAxios.post(messageURL, JSON.stringify(params));
      console.log(messageData)

      if (response.data) {
        successNotify("Product added successfully");
        if (userInfo.role === "ROLE_ADMIN") {
          getProductsList();
        } else {
          getProductsListBySupplier();
        }
      }
    }
    setIsModalOpen(false);
  };

  const loadProductsEditValues = () => {
    const oldEventsWeekSlots = productData?.operatingHours?.map(
      (item: any) => item.weekDays
    );
    const editWeekSlots = weekSlots.map((slot: any) => {
      if (oldEventsWeekSlots?.includes(slot.days)) {
        const currentOperatingHour = productData?.operatingHours
          ?.filter((item: any) => item.weekDays === slot.days)
          ?.at(0);
        return {
          days: currentOperatingHour.weekDays,
          day: true,
          intime: currentOperatingHour.startTime,
          outtime: currentOperatingHour.endTime,
        };
      } else {
        return slot;
      }
    });
    console.log(
      "loadProductsEditValues",
      productData,
      oldEventsWeekSlots,
      editWeekSlots
    );
    const initialEditproductValues = {
      name: productData.name,
      description: productData.description,
      businessCity: productData.businessCity,
      businessPinCode: productData.businessPinCode,
      eventStart: productData.startDate,
      eventEnd: productData.endDate,
      categories: productData?.categories?.categoriesName,
      quantity: productData.quantity,
      slothours: productData.operatingHours[0].hourSlot,
      price: `${productData.price}`,
      maxcount: `${productData.operatingHours[0]?.slot[0]?.maxAvailableCount}`,
      slotRegister: [...editWeekSlots],
    };

    return initialEditproductValues;
  };

  const Time = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
    22, 23, 24,
  ];

  const formik = useFormik<initialproductValues>({
    initialValues:
      addProductsModalHeader === "Edit Event"
        ? loadProductsEditValues()
        : {
          name: "",
          description: "",
          businessCity: "",
          businessPinCode: "",
          eventStart: "",
          eventEnd: "",
          categories: "",
          slothours: "",
          price: "",
          quantity: 0,
          maxcount: "",
          slotRegister: [...weekSlots],
        },
    // validationSchema: Yup.object({
    //   name: Yup.string().required("Required"),
    //   description: Yup.string().required("Required"),
    //   businessCity: Yup.string().required("Required"),
    //   businessPinCode: Yup.string().required("Required"),
    //   eventStart: Yup.string().required("Required"),
    //   eventEnd: Yup.string().required("Required"),
    //   slothours: Yup.string().required("Required"),
    //   price: Yup.string().required("Required"),
    //   maxcount: Yup.string().required("Required"),
    // }),
    onSubmit: (values) => addNewEvent(values),
  });

  const handleFileChange = (event: ChangeEvent<HTMLInputElement>) => {
    const selectedFile = event.target.files?.[0];
    if (selectedFile) {
      setFile(selectedFile);
      if (event.target.files && event.target.files.length > 0) {
        const file = event.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          event.preventDefault();
          const imageUrl = reader.result?.toString();
          setBase64Image(imageUrl);
        };
      }
    }
  };

  const handleDragOver = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
  };

  const handleDrop = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    const selectedFile = event.dataTransfer.files?.[0];
    if (selectedFile) {
      setFile(selectedFile);
    }
  };

  return (
    <div className="w-[85%] h-full bg-[#00000080] absolute z-40 right-0">
      <section className="w-full h-full flex flex-col items-center justify-center">
        <form
          onSubmit={formik.handleSubmit}
          className="bg-[#ffffff] w-[40rem] h-[50rem] overflow-y-scroll px-8 py-6 rounded-lg relative"
        >
          <button onClick={() => setIsModalOpen(false)}>
            <img src={deleteIcon} className="absolute top-5 right-5 h-6 w-6" />
          </button>
          <h2 className="mb-4 text-lg font-semibold text-center">
            {addProductsModalHeader}
          </h2>
          <input
            type="text"
            name="name"
            placeholder="Produktnamn"
            className="outline-none border-[1px] rounded-lg border-gray-300 my-2 px-2 w-full h-10 bg-transparent"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.name}
          />
          <textarea
            rows={6}
            name="description"
            id="eventDescription"
            placeholder="Produktbeskrivning"
            className="outline-none border-[1px] rounded-lg border-gray-300 my-1 px-2 pt-2 w-full bg-transparent"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.description}
          />
          <input
            type="text"
            name="businessCity"
            id="eventbusinessCity"
            placeholder="City"
            className="outline-none border-[1px] rounded-lg border-gray-300 my-1 px-2 w-full h-10 bg-transparent"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.businessCity}
          />
          <input
            type="text"
            name="PinCode"
            id="eventName"
            placeholder="businessPinCode"
            className="outline-none border-[1px] rounded-lg border-gray-300 my-1 px-2 w-full h-10 bg-transparent"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.businessPinCode}
          />
          <Select
            className="py-3 text-[#000000] placeholder-[#B5B5BE]  md:w-full  outline-none leading-tight mb-2 text-lg"
            placeholder="Välj kategorier"
            name="categories"
            onBlur={formik.handleBlur}
            onChange={(option: any) =>
              formik.setFieldValue("categories", option.value)
            }
            value={options.find((option: any) =>
              formik.values.categories?.includes(option.value)
            )}
            options={options}
          />
          <section
            className="w-full py-10 px-10 border-[1px] rounded-lg flex flex-col items-center justify-center my-5"
            onDragOver={handleDragOver}
            onDrop={handleDrop}
          >
            <input
              id="file-upload"
              type="file"
              accept=".jpg,.jpeg,.png,.gif"
              className="hidden"
              onChange={handleFileChange}
            />
            {file ? (
              <div onClick={() => setFile(null)}>
                <p>{file.name}</p>
                <p>{file.size} bytes</p>
              </div>
            ) : (
              <label htmlFor="file-upload" className="cursor-pointer">
                <img className="ml-11 mb-2" src={fileupload} alt="fileupload" />
                <span className="text-[#97A0C3] text-xs font-bold">
                  Ladda upp bild
                </span>
              </label>
            )}
          </section>
          <div className="flex flex-row items-center justify-between mt-4">
            <section className="flex flex-col">
              <label className="mt-1 font-semibold" htmlFor="eventStart">
                Event startdatum:
              </label>
              <input
                type="date"
                name="eventStart"
                id="eventStart"
                className="outline-none px-2 w-full h-10 bg-transparent"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.eventStart}
              />
            </section>
            {/* <section className="flex flex-col">
              <label className="mt-1 font-semibold" htmlFor="eventEnd">
                Event slutdatum:
              </label>
              <input
                type="date"
                name="eventEnd"
                id="eventEnd"
                className="outline-none px-2 w-full h-10 bg-transparent"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.eventEnd}
              />
            </section> */}
          </div>
          <div>
            <div className="flex items-center mt-4">
              <table className="table-auto">
                <thead>
                  <tr>
                    <th className="">Dagar</th>
                    <th>Öppnar</th>
                    <th className="">Stänger</th>
                  </tr>
                </thead>
                <tbody>
                  {formik.values.slotRegister.map((day: any, i) => {
                    return (
                      <>
                        <tr>
                          <td className="flex flex-row">
                            <input
                              type="checkbox"
                              name={`slotRegister[${i}].day`}
                              value={formik.values.slotRegister[i].day}
                              checked={formik.values.slotRegister[i].day as any}
                              onChange={formik.handleChange}
                              className="mt-1"
                            />
                            <label className="mx-5" id="day">
                              {day.days}
                            </label>
                          </td>

                          <td>
                            <input
                              type="time"
                              id="intime"
                              className="outline-none bg-transparent"
                              onChange={formik.handleChange}
                              onBlur={formik.handleBlur}
                              name={`slotRegister[${i}].intime`}
                              value={formik.values.slotRegister[i].intime}
                            />
                          </td>
                          <td>
                            <input
                              type="time"
                              name={`slotRegister[${i}].outtime`}
                              id="outime"
                              onChange={formik.handleChange}
                              onBlur={formik.handleBlur}
                              value={formik.values.slotRegister[i].outtime}
                              className="outline-none w-full bg-transparent"
                            />
                          </td>
                        </tr>
                      </>
                    );
                  })}
                </tbody>
              </table>
              <div className="mx-5">
                <label>Event längd</label>
                <select
                  className="w-20 ml-3"
                  name="slothours"
                  id="time"
                  onChange={formik.handleChange}
                  value={formik.values.slothours}
                >
                  {Time.map((time, i) => {
                    if (i < 4) {
                      return (
                        <>
                          <option>{time}</option>
                        </>
                      );
                    }
                  })}
                </select>
                <div className="mt-5">
                  <label>Pris</label>
                  <input
                    type="number"
                    name="price"
                    className="border outline-none rounded-lg px-1 py-2"
                    onChange={formik.handleChange}
                    value={formik.values.price}
                  />
                </div>
                <div className="mt-5">
                  <label>Max antal personer</label>
                  <input
                    type="number"
                    name="maxcount"
                    className="border outline-none rounded-lg px-1 py-2"
                    onChange={formik.handleChange}
                    value={formik.values.maxcount}
                  />
                </div>
              </div>
            </div>
          </div>
          <button
            type="submit"
            disabled={isLoading}
            className="px-4 py-2 md:mr-4 bg-[#D6B27D] rounded-lg w-full mt-6 text-md font-semibold cursor-pointer uppercase"
          >
            {addProductsModalHeader}
          </button>
        </form>
      </section>
    </div>
  );
};

export default AddProductsModal;
