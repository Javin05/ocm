import React, { ChangeEvent, useEffect, useState } from "react";
import { commonAxios, formAxios } from "../../Axios/service";
import { useFormik } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import { failureNotify, successNotify } from "../Toast/Toast";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import deleteIcon from "../../assets/Delete.png";
import fileupload from "../../assets/file-upload.svg";
import Loader from "../Loader/Loader";
import { RxCross2 } from "react-icons/rx";
import { IMGAEVALIDATION } from "../../Utils/constants"
import _ from 'lodash'

type initialEventValues = {
  name: string;
  description: string;
  city: string;
  pincode: string;
  eventStart: string;
  eventEnd: string;
  category: string[];
  slothours: string;
  slotRegister: slotregister;
  price: string;
  mincount: string;
  maxcount: string;
  delayHours: string;
  delayMinutes: string;
  companyName: string;
  fromPrice: string;
  units: string

};

type slotregister = slotRegisters[];
interface slotRegisters {
  intime: string;
  outtime: string;
  day: string;
  days: string;
}

const validateDateField = (values: any) => {
  const hasSelectedDate = values.slotRegister.some((day: any) => day.day);
  const timeErrors = values.slotRegister.reduce(
    (errors: any, day: any, i: any) => {
      if (day.day) {
        if (!day.intime || !day.outtime) {
          errors[`slotRegister`] = {
            intime: "Intime and outtime is required when the day is selected",
            outtime: "Outtime is required when the day is selected",
          };
        } else {
          const intime = new Date(`2000-01-01T${day.intime}`);
          const outtime = new Date(`2000-01-01T${day.outtime}`);
          // if (intime <= outtime) {
          //   errors[`slotRegister`] = {
          //     outtime: "Outtime must be after Intime",
          //   };
          // }
        }
      }
      return errors;
    },
    {}
  );

  if (!hasSelectedDate || Object.keys(timeErrors).length > 0) {
    return { date: "Please select at least one date", ...timeErrors };
  }

  return {};
};


const AddEventsModal = ({
  isLoading,
  options,
  choose,
  setIsModalOpen,
  getEventsList,
  getEventsListBySupplier,
  addEventModalHeader,
  eventData,
  seteventData,
}: any) => {


  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [file, setFile] = useState<File | null>(null);
  const [enableLoader, setEnableLoader] = useState(false);
  const [base64Image, setBase64Image] = useState<any>([]);
  const [formStep, setFormStep] = useState(1);
  const [stepOneFormData, setStepOneFormData] = useState<any>(null);
  const [momsCalculation, setMomCalculation] = useState<any>("");
  const [getaquote, setgetaquote] = useState<any>(false)
  const [getPayment, setgetPayment] = useState<any>(false)
  const [moms, setMoms] = useState<any>([])
  const [errors, setErrors] = useState<any>(null);
  const [currentPrice, setCurrentPrice] = useState<number>(0);
  const [currentPayment, setCurrentPayment] = useState<number>(0)
  const weekSlots = [
    {
      days: "MONDAY",
      day: "",
      intime: "",
      outtime: "",
    },
    { days: "TUESDAY", day: "", intime: "", outtime: "" },
    {
      days: "WEDNESDAY",
      day: "",
      intime: "",
      outtime: "",
    },
    { days: "THURSDAY", day: "", intime: "", outtime: "" },
    {
      days: "FRIDAY",
      day: "",
      intime: "",
      outtime: "",
    },
    {
      days: "SATURDAY",
      day: "",
      intime: "",
      outtime: "",
    },
    {
      days: "SUNDAY",
      day: "",
      intime: "",
      outtime: "",
    },
  ];
  const delayHours = [
    "00",
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10",
    "11",
    "12",
  ].map((hour: string) => {
    return {
      label: hour,
      value: hour,
    };
  });
  const delayMinutes = ["00", "30"].map((hour: string) => {
    return {
      label: hour,
      value: hour,
    };
  });


  const setFormDataAndNavigateToFileUpload = (
    eventValue: initialEventValues
  ) => {
    setStepOneFormData(eventValue);
    setFormStep(2);
  };

  const addNewEvent = async (event: any) => {
    event.preventDefault();
    setEnableLoader(true);
    // setMomCalculation("")
    let choosed_array = stepOneFormData.slotRegister.filter(
      (value: any) => value.day === true
    );

    if (
      stepOneFormData.slothours === "1" ||
      stepOneFormData.slothours === "ONE_HOUR"
    ) {
      stepOneFormData.slothours = "ONE_HOUR";
    } else if (
      stepOneFormData.slothours === "2" ||
      stepOneFormData.slothours === "TWO_HOUR"
    ) {
      stepOneFormData.slothours = "TWO_HOUR";
    } else if (
      stepOneFormData.slothours === "3" ||
      stepOneFormData.slothours === "THREE_HOUR"
    ) {
      stepOneFormData.slothours = "THREE_HOUR";
    } else {
      stepOneFormData.slothours = "FOUR_HOUR";
    }
    let operatingHours = choosed_array.map((val: any) => {
      const main = {
        hourSlot: "ONE_HOUR",
        weekDay: val.days,
        startTime: val.intime,
        endTime: val.outtime,
      };
      return main;
    });

    let categoryObj: any = [];

    stepOneFormData?.category.map((item: any) => {
      categoryObj.push({ categoryName: item })
    })

    const eventBody: any = {
      name: stepOneFormData.name,
      description: stepOneFormData.description,
      city: stepOneFormData.city,
      pincode: stepOneFormData.pincode,
      companyName: stepOneFormData.companyName,
      supplierId: userInfo.id,
      image: base64Image && base64Image.map((item: any) => item.values).join(', '),
      slotHours: stepOneFormData.slothours,
      operatingHours: operatingHours,
      category: categoryObj,
      price: stepOneFormData.price,
      delayTime: `${stepOneFormData.delayHours ?? "00"}:${stepOneFormData.delayMinutes ?? "00"
        }`,
      minCount: stepOneFormData.mincount,
      maxCount: stepOneFormData.maxcount,
      startDate: stepOneFormData.eventStart,
      endDate: stepOneFormData.eventEnd,
      taxId: momsCalculation,
      fromPrice: stepOneFormData.fromPrice,
      units: stepOneFormData.units
    };

    if (addEventModalHeader === "Edit Event") {
      eventBody.id = eventData.id;
    }

    const URL =
      addEventModalHeader === "Edit Event"
        ? `/cemo/events/update/${eventData.id}`
        : "/cemo/events/add";
    if (base64Image) {
      if (addEventModalHeader === "Edit Event") {
        const response = await commonAxios.put(URL, JSON.stringify(eventBody));
        if (response.data) {
          successNotify("Eventet har ändrats");
          setMomCalculation("")
          setEnableLoader(false);
          if (userInfo.role === "ROLE_ADMIN") {
            getEventsList();
          } else {
            getEventsListBySupplier();
            setEnableLoader(false);
          }
        }
      } else {
        const response = await commonAxios.post(URL, JSON.stringify(eventBody));
        const params = {
          message: eventBody && eventBody.name,
          customerId: null,
          supplierId: userInfo && userInfo.id == 'admin' ? '' : userInfo.id,
          adminId: "admin",
          isCustomerMessage: false,
          isSupplierMessage: userInfo && userInfo.id == 'admin' ? false : true,
          isAdminMessage: userInfo && userInfo.id == 'admin' ? true : false,
        }
        const messageURL = "/cemo/messages"
        const messageData = await commonAxios.post(messageURL, JSON.stringify(params));
        console.log(messageData)

        if (response.data) {
          successNotify("Eventet har lagts till");
          setEnableLoader(false);
          if (userInfo.role === "ROLE_ADMIN") {
            getEventsList();
          } else {
            getEventsListBySupplier();
            setEnableLoader(false);
          }
        }
      }
    } else {
      failureNotify('Image is mandatory for event')
    }

    setIsModalOpen(false);
  };

  const loadEventEditValues = () => {
    const oldEventsWeekSlots = eventData?.operatingHours?.map(
      (item: any) => item.weekDays
    );
    const editWeekSlots = weekSlots.map((slot: any) => {
      if (oldEventsWeekSlots?.includes(slot.days)) {
        const currentOperatingHour = eventData?.operatingHours
          ?.filter((item: any) => item.weekDays === slot.days)
          ?.at(0);
        return {
          days: currentOperatingHour.weekDays,
          day: true,
          intime: currentOperatingHour.startTime,
          outtime: currentOperatingHour.endTime,
        };
      } else {
        return slot;
      }
    });

    const getSlotValue = (value: string) => {
      if (
        value === "ONE_HOUR"
      ) {
        return 1
      } else if (
        value === "TWO_HOUR"
      ) {
        return 2
      } else if (
        value === "THREE_HOUR"
      ) {
        return 3
      } else {
        return 4
      }
    }
    const category = eventData?.categories?.map((item: any) => item.categoryName);

    const initialEditEventValues = {
      name: eventData.name,
      description: eventData.description,
      city: eventData.city,
      pincode: eventData.pincode,
      eventStart: eventData.startDate,
      companyName: eventData.companyName,
      eventEnd: eventData.endDate,
      category: category,
      slothours: getSlotValue(eventData?.slotHours),
      price: `${eventData.price}`,
      delayHours: eventData.delayTime ? eventData.delayTime.split(":")[0] : undefined,
      delayMinutes: eventData.delayTime ? eventData.delayTime.split(":")[1] : undefined,
      mincount: `${eventData.operatingHours?.at(0)?.slot?.at(0)?.minAvailableCount
        }`,
      maxcount: `${eventData.operatingHours?.at(0)?.slot?.at(0)?.maxAvailableCount - eventData.operatingHours?.at(0)?.slot?.at(0)?.bookedCount
        }`,
      slotRegister: [...editWeekSlots],
      // fromPrice: `${(eventData.fromPrice === "" && eventData.fromPrice === "getaquote") ? '' : eventData.fromPrice}`
      fromPrice: `${eventData.fromPrice}`,
      units: eventData.units
    };

    return initialEditEventValues;
  };
  const Time = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
    22, 23, 24,
  ];

  const formik: any = useFormik<any>({
    initialValues:
      addEventModalHeader === "Edit Event"
        ? loadEventEditValues()
        : {
          name: "",
          description: "",
          city: "",
          pincode: "",
          // companyName: "",
          eventStart: "",
          eventEnd: "",
          category: [""],
          slothours: "",
          price: "",
          // maxcount: "",
          // mincount: "",
          delayHours: "00",
          delayMinutes: "00",
          slotRegister: [...weekSlots],
          fromPrice: "",
          units: ""

        },
    // validationSchema: Yup.object({
    //   name: Yup.string().required("Obligatorisk"),
    //   description: Yup.string().required("Obligatorisk"),
    //   city: Yup.string().required("Obligatorisk"),
    //   pincode: Yup.string().required("Obligatorisk"),
    //   eventStart: Yup.string().required("Obligatorisk"),
    //   eventEnd: Yup.string().required("Obligatorisk"),
    //   price: Yup.string().required("Obligatorisk"),
    //   fromPrice: Yup.string().required("Obligatorisk"),
    //   mincount: Yup.number().min(1, 'mincount must be at least 1').required("Obligatorisk"),
    //   maxcount: Yup.number().required("Required").min(Yup.ref('mincount'), 'Maximum count must be greater than minimum count')
    //     .test('is-greater', 'Maximum count must be greater than minimum count', function (value) {
    //       const { mincount } = this.parent;
    //       return value >= mincount;
    //     }),
    //   category: Yup.array()
    //     .min(1, "Please select at least one option")
    //     .required("Atleast One catgory is required"),
    // }),

    validate: values => {
      const errors: Partial<typeof values> = {};
      // Basic required validations
      if (!values.name) {
        errors.name = 'Obligatorisk';
      }
      if (!values.description) {
        errors.description = 'Obligatorisk';
      }
      if (!values.city) {
        errors.city = 'Obligatorisk';
      }
      if (!values.pincode) {
        errors.pincode = 'Obligatorisk';
      }
      if (!values.eventStart) {
        errors.eventStart = 'Obligatorisk';
      }
      if (!values.eventEnd) {
        errors.eventEnd = 'Obligatorisk';
      }
      if (!getaquote && !values.price) {
        errors.price = 'Obligatorisk';
      }
      // Conditional validation for fromPrice
      if (getPayment && !values.fromPrice) {
        errors.fromPrice = 'Obligatorisk';
      }
      // Category array validation
      if (values.category.length === 0) {
        errors.category = 'At least one category is required';
      }
      return errors;
    },
    onSubmit: (values, { setSubmitting }) => {
      const dateErrors = validateDateField(values);
      if (Object.keys(dateErrors).length > 0) {
        setSubmitting(false);
        setErrors(dateErrors);
      } else {
        setFormDataAndNavigateToFileUpload(values);
      }
    },
  });

  const deleteImage = (index: number) => {
    const updatedImage = base64Image.filter((img: any, idx: number) => {
      return idx !== index;
    });
    setBase64Image(updatedImage);
  };

  const handleFileChange = async (
    event: React.ChangeEvent<HTMLInputElement>,
    max: number,
    min: number) => {
    const selectedFiles = event.target.files;
    let errorOccurred = false;
    let totalUploadedImages = base64Image ? base64Image.length : 0;
    if (selectedFiles && selectedFiles.length > 0) {
      for (let i = 0; i < selectedFiles.length; i++) {
        const selectedFile = selectedFiles[i];
        let isValidFileFormat = _.includes(IMGAEVALIDATION, selectedFile.type);
        if (isValidFileFormat) {
          if (totalUploadedImages < max) {
            try {
              setEnableLoader(true);
              const formData = new FormData();
              formData.append('file', selectedFile);
              formData.append('folder', 'events');
              const response = await formAxios.post('/cemo/upload/files', formData);
              totalUploadedImages++;
              if (response.data && response.data.length > 0) {
                const imageURL = response.data[0];
                setBase64Image((prevBase64Images: string[]) => [...prevBase64Images, { values: imageURL }]);
              } else {
                failureNotify('Oops! Something went wrong, try to upload again');
                errorOccurred = true;
              }
            } catch (error) {
              failureNotify('Oops! Something went wrong, try to upload again');
            } finally {
              setEnableLoader(false);
            }
          } else if (!errorOccurred) {
            failureNotify(`You can upload a maximum of ${max} files.`);
            errorOccurred = true;
          }
        } else {
          failureNotify(`File Please upload a valid image file (jpg, jpeg, png, gif).`);
        }
      }
    } else {
      failureNotify('Please select at least one file.');
    }
  }

  const handleDragOver = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
  };

  const getMoms = async () => {
    const response = await commonAxios.get("/cemo/taxtypes/all");
    if (response.data) {
      setMoms(response.data);
    }
    if (eventData?.price === 0) {
      setgetaquote(!getaquote)
    }
    if (eventData?.fromPrice === 0) {
      setgetPayment(!getPayment)
    }
  };

  const handleDrop = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    const selectedFile = event.dataTransfer.files?.[0];
    if (selectedFile) {
      setFile(selectedFile);
    }
  };
  const sortedCities = choose.slice().sort((a: any, b: any) =>
    a.value.localeCompare(b.value)
  );

  const sortedCategories = options.slice().sort((a: any, b: any) =>
    a.value.localeCompare(b.value)
  );

  useEffect(() => {
    getMoms()
    setBase64Image([])
  }, [])

  console.log('eventData', eventData)

  useEffect(() => {
    if (_.isArray(eventData.image)) {
      const image = _.filter(eventData && eventData.image)
      setBase64Image((prevBase64Images: string[]) => [...prevBase64Images, { values: image }])
    } else if (!_.isEmpty(eventData && eventData.image)) {
      setBase64Image((prevBase64Images: string[]) => [...prevBase64Images, { values: eventData.image }])
    }
    return () => {
      setBase64Image([]);
    };
  }, [addEventModalHeader]);

  useEffect(() => {
    if (addEventModalHeader === "Edit Event") {
      if (eventData.taxType?.id) {
        setMomCalculation(eventData.taxType?.id)
      }
    }
  }, [])

  const handleMomsChange = (e: any) => {
    setMomCalculation(e.target.name);
  };

  const handleSelectChange = (selectedOptions: any) => {

    const selectedValues = selectedOptions.map((option: any) => option.value);
    formik.setFieldValue("category", selectedValues);
  };

  const translateSweden = (day: any) => {

    const actualDates: any = {
      MONDAY: 'Mondag',
      TUESDAY: 'Tisdag',
      WEDNESDAY: 'Onsdag',
      THURSDAY: 'Torsdag',
      FRIDAY: 'Fredag',
      SATURDAY: 'Lördag',
      SUNDAY: 'Söndag'
    }

    if (day in actualDates) {
      return actualDates[day];
    }

  }

  return (
    <div className="w-[85%] h-full bg-[#00000080] absolute z-40 right-0">
      {enableLoader && <Loader />}
      <section className="w-full h-full flex flex-col items-center justify-center text-secondary">
        {formStep === 1 ? (
          <form
            onSubmit={formik.handleSubmit}

            className="bg-[#ffffff] w-[54rem] h-[90%] max-h-4/5 overflow-y-scroll px-8 py-6 relative">
            <button
              onClick={() => {
                setIsModalOpen(false);
                seteventData([]);
              }}
            >
              <img
                src={deleteIcon}
                className="absolute top-5 right-5 h-6 w-6"
              />
            </button>
            <h2 className="mb-4 text-lg font-semibold text-center">
              {addEventModalHeader === "Edit Event" ? "Redigera event" : addEventModalHeader}
            </h2>
            <label className="flex mt-3 font-semibold">Eventnamn*</label>
            <input
              type="text"
              name="name"
              placeholder="Eventnamn"
              className="outline-none border-[1px] rounded-[10px]
              py-2
              px-4  border-gray-300 my-2 w-full h-10 bg-transparent"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.name}
              onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
            />
            <div className="my-1">
              {formik.errors.name ? (
                <span className="text-red-500 font-medium">
                  {formik.errors.name}
                </span>
              ) : null}
            </div>
            <label className="flex mt-3 font-semibold">Event beskrivning*</label>
            <textarea
              rows={6}
              name="description"
              id="eventDescription"
              placeholder="Event beskrivning"
              className="outline-none border-[1px] rounded-lg border-gray-300 my-1 px-2 pt-2 w-full bg-transparent"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.description}
            />
            <div className="my-1">
              {formik.errors.description ? (
                <span className="text-red-500 font-medium">
                  {formik.errors.description}
                </span>
              ) : null}
            </div>
            <label className="flex mt-3 font-semibold">Välj Momssats</label>
            <div className="my-4 flex flex-row w-1/3 justify-between">
              {moms?.map((option: any) => (
                <label key={option.id}>
                  <input
                    type="radio"
                    name={option.id}
                    className="mr-2 flex gap-4"
                    onChange={handleMomsChange}
                    checked={momsCalculation === option.id}
                    onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                  />
                  {option.taxPercentage}%
                </label>
              ))}
            </div>
            {/* {errors.taxId && <p className="text-red-500">{errors.taxId}</p>} */}
            {/* <label className="flex mt-3 font-semibold">Företagsnamn</label> */}
            {/* <input
              type="text"
              name="companyName"
              placeholder="Företagsnamn"
              className="outline-none border-[1px] rounded-[10px]
              py-2
              px-4  border-gray-300 my-2 w-full h-10 bg-transparent"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.companyName}
            /> */}
            {/* <div className="my-1">
              {formik.errors.companyName ? (
                <span className="text-red-500 font-medium">
                  {formik.errors.companyName}
                </span>
              ) : null}
            </div> */}
            <label className="flex mt-3 font-semibold">Stad</label>
            <div onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}>
              <Select
                className="py-3 md:w-full  outline-none leading-tight mb-2"
                placeholder="Choose Stad"
                name="city"
                onBlur={formik.handleBlur}
                onChange={(option: any) =>
                  formik.setFieldValue("city", option.value.toUpperCase())
                }
                value={choose.filter((option: any) =>
                  formik.values.city?.includes(option.value.toUpperCase())
                )}
                options={sortedCities}

              />
            </div>
            <div className="my-1">
              {formik.errors.city ? (
                <span className="text-red-500 font-medium">
                  {formik.errors.city}
                </span>
              ) : null}
            </div>
            <label className="flex mt-3 font-semibold">Postkod*</label>
            <input
              type="text"
              name="pincode"
              id="eventName"
              placeholder="Postkod"
              className="outline-none border-[1px]  border-gray-300 my-1 w-full h-10 bg-transparent rounded-[10px]
              py-2
              px-4"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.pincode}
              onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
            />
            <div className="my-1">
              {formik.errors.pincode ? (
                <span className="text-red-500 font-medium">
                  {formik.errors.pincode}
                </span>
              ) : null}
            </div>
            <label className="flex mt-3 font-semibold">Välj kategorier*</label>
            <div onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}>
              <Select
                options={options}
                className="py-3  placeholder-[#B5B5BE] text-[16px] md:w-full  outline-none leading-tight "
                placeholder={"Välj kategorier"}
                onChange={handleSelectChange
                }

                value={options.filter((option: any) =>
                  formik.values.category?.includes(option.value)
                )}
                isMulti={true}
              />
            </div>

            <div className="my-1">
              {formik.errors.category ? (
                <span className="text-red-500 font-medium">
                  {formik.errors.category}
                </span>
              ) : null}
            </div>
            <div className="flex flex-row items-center justify-between my-2">
              <section className="flex flex-col">
                <label className="mt-1 font-semibold" htmlFor="eventStart">
                  Event startdatum:
                </label>
                <input
                  type="date"
                  name="eventStart"
                  id="dateInput"
                  className="custom-date-input"
                  // id="eventStart"
                  // className="outline-none w-full h-10 bg-transparent"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.eventStart}
                />
                <div className="my-1">
                  {formik.errors.eventStart ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.eventStart}
                    </span>
                  ) : null}
                </div>
              </section>
              <section className="flex flex-col">
                <label className="mt-1 font-semibold" htmlFor="eventEnd">
                  Event slutdatum:
                </label>
                <input
                  type="date"
                  name="eventEnd"
                  id="eventEnd"
                  className="outline-none w-full h-10 bg-transparent"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.eventEnd}
                />
                <div className="my-1">
                  {formik.errors.eventEnd ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.eventEnd}
                    </span>
                  ) : null}
                </div>
              </section>
            </div>
            <div className="flex">
              <div className="border p-5">
                <table className="table-auto">
                  <thead>
                    <tr>
                      <th className="">Dagar</th>
                      <th>Öppnar</th>
                      <th className="">Stänger</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formik.values.slotRegister.map((day: any, i: any) => {
                      return (
                        <>
                          <tr className="">
                            <td className="flex flex-row">
                              <input
                                type="checkbox"
                                name={`slotRegister[${i}].day`}
                                value={formik.values.slotRegister[i].day}
                                checked={
                                  formik.values.slotRegister[i].day as any
                                }
                                onChange={(e: any) => {
                                  formik.handleChange(e);
                                  setErrors(null);
                                }}
                                className="mt-4"
                                onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                              />
                              <label className="mx-5 mt-3 uppercase" id="day">
                                {translateSweden(day.days)}
                              </label>
                            </td>

                            <td>
                              <input
                                type="time"
                                id="intime"
                                className="outline-none bg-transparent mt-3"
                                onChange={(e: any) => {
                                  formik.handleChange(e);
                                  setErrors(null);
                                }}
                                onBlur={formik.handleBlur}
                                name={`slotRegister[${i}].intime`}
                                value={formik.values.slotRegister[i].intime}
                              />
                            </td>
                            <td>
                              <input
                                type="time"
                                name={`slotRegister[${i}].outtime`}
                                id="outime"

                                onChange={(e: any) => {
                                  formik.handleChange(e);
                                  setErrors(null);
                                }}
                                onBlur={formik.handleBlur}
                                value={formik.values.slotRegister[i].outtime}
                                className="outline-none w-full bg-transparent mt-3"
                              />
                            </td>
                          </tr>
                        </>
                      );
                    })}
                  </tbody>
                </table>
                {errors && errors.slotRegister ? (
                  <p className="text-red-500">{errors.slotRegister.intime}</p>
                ) : (
                  <p className="text-red-500">{errors?.date}</p>
                )}
              </div>
              <div className="ms-5 w-full border p-5 border-inherit">
                <label className="font-bold">Event längd:</label>
                <select
                  className="w-20 ml-3 p-1"
                  name="slothours"
                  id="time"
                  onChange={formik.handleChange}
                  value={formik.values.slothours}
                >
                  {Time.map((time, i) => {
                    if (i < 4) {
                      return (
                        <>
                          <option>{time}</option>
                        </>
                      );
                    }
                  })}
                </select>
                <div className="my-2">
                  {formik.errors.slothours ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.slothours}
                    </span>
                  ) : null}
                </div>
                <div onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                  className="mt-5 flex flex-row justify-between items-center">
                  <p className="w-[40%] font-bold">Fördröjning innan nästa bokning:</p>
                  <Select
                    className="py-3 px-1 text-[#000000] placeholder-[#B5B5BE]  md:w-1/2  outline-none leading-tight mb-2 text-lg mr-2"
                    placeholder="hours"
                    name="delayHours"
                    onBlur={formik.handleBlur}
                    onChange={(option: any) =>
                      formik.setFieldValue("delayHours", option.value)
                    }
                    value={delayHours.filter((option: any) =>
                      formik.values.delayHours?.includes(option.value)
                    )}
                    options={delayHours}
                  />
                  <Select
                    className="py-3 text-[#000000] placeholder-[#B5B5BE]  md:w-1/2  outline-none leading-tight mb-2 text-lg"
                    placeholder="minutes"
                    name="delayMinutes"
                    onBlur={formik.handleBlur}
                    onChange={(option: any) =>
                      formik.setFieldValue("delayMinutes", option.value)
                    }
                    value={delayMinutes.filter((option: any) =>
                      formik.values.delayMinutes?.includes(option.value)
                    )}
                    options={delayMinutes}
                  />
                </div>
                <div className="flex items-center justify-between">
                  <div className="mt-5">
                    <label className="font-bold mt-2">Pris:</label>
                    <input
                      type="number"
                      name="price"
                      className="border outline-none rounded-lg ml-2 py-2 w-24 px-2"
                      onChange={(e) => {
                        // const newValue = parseFloat(e.target.value);
                        // if (!isNaN(newValue) && newValue >= 0) {
                        // Allow non-negative values or empty input (deletion)
                        formik.handleChange(e);
                        if (!getaquote) {
                          setCurrentPrice(parseFloat(e.target.value));
                        }
                        // }
                      }}
                      value={getaquote ? 0 : formik.values.price}
                      disabled={getaquote}
                      min={0}
                      onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                    />
                    <div className="my-1">
                      {formik.errors.price ? (
                        <span className="text-red-500 font-medium">
                          {formik.errors.price}
                        </span>
                      ) : null}
                    </div>
                  </div>

                  {/* Dropdown Field */}
                  <div className="mt-5 ms-3">
                    <label className="font-bold mt-2">Välj unit:</label>
                    <select
                      name="units"
                      className="border outline-none rounded-lg ml-2 py-2 w-24 px-2"
                      onChange={(e) => {
                        formik.handleChange(e);
                      }}
                      value={formik.values.units}
                    >
                      <option value="Person">Person</option>
                      <option value="Timmer">Timmer</option>
                      <option value="Tillfälle">Tillfälle</option>
                    </select>
                  </div>
                  <div className="mt-5">
                    <input
                      type="checkbox"
                      name="getaquote"
                      className="outline-none border-[1px] rounded-[10px] mx-1 border-gray-300 bg-transparent"
                      onChange={() => {
                        setgetaquote(!getaquote)

                        if (!getaquote) {
                          // If getaquote was false and is now true, set fromPrice to currentPayment
                          setCurrentPayment(formik.values.fromPrice);
                          formik.setFieldValue("fromPrice", currentPayment);
                        } else {
                          // If getaquote was true and is now false, clear fromPrice
                          formik.setFieldValue("fromPrice", "");
                        }


                      }}
                      checked={getaquote}
                      value={getaquote ? 0 : formik.values.fromPrice}
                      onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                    />
                    <label className="mt-5 font-bold">Fåoffert</label>
                  </div>

                </div>
                {
                  getaquote ? (
                    <div className="mt-5">
                      <label className="font-bold mt-2">Pris från:</label>
                      <input
                        type="number"
                        name="fromPrice"
                        className="border outline-none ml-3 rounded-[10px] py-2 px-4"
                        onChange={(e) => {

                          formik.handleChange(e);
                          if (!getPayment) {
                            setCurrentPayment(parseFloat(e.target.value));
                          }

                        }}
                        value={getPayment ? 0 : formik.values.fromPrice}
                        disabled={getPayment}
                        min={0}
                        onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                      />
                      <div className="my-1">
                        {formik.errors.fromPrice ? (
                          <span className="text-red-500 font-medium">
                            {formik.errors.fromPrice}
                          </span>
                        ) : null}
                      </div>
                    </div>
                  ) : null
                }
                <div className="mt-5">
                  <label className="font-bold">Minsta antal:</label>
                  <input
                    type="number"
                    name="mincount"
                    className="border outline-none ml-3 rounded-[10px] py-2 px-4"
                    onChange={(e) => {
                      const newValue = parseInt(e.target.value, 10);
                      if (!isNaN(newValue) || e.target.value === "") {
                        // Allow non-negative values or empty input (deletion)
                        formik.handleChange(e);
                      }
                    }}
                    value={formik.values.mincount}
                    min={1}
                    onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                  />
                  <div className="my-1">
                    {formik.errors.mincount ? (
                      <span className="text-red-500 font-medium">
                        {formik.errors.mincount}
                      </span>
                    ) : null}
                  </div>
                </div>
                <div className="mt-5">
                  <label className="font-bold">Max antal personer:</label>
                  <input
                    type="number"
                    name="maxcount"
                    className="border outline-none ml-3 rounded-[10px] py-2 px-4"
                    onChange={(e) => {
                      const newValue = parseInt(e.target.value, 10);
                      if (!isNaN(newValue) || e.target.value === "") {
                        // Allow non-negative values or empty input (deletion)
                        formik.handleChange(e);
                      }
                    }}
                    value={formik.values.maxcount}
                    min={0}
                    onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                  />
                  <div className="my-1">
                    {formik.errors.maxcount ? (
                      <span className="text-red-500 font-medium">
                        {formik.errors.maxcount}
                      </span>
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
            <div className="flex justify-center">
              <button
                type="submit"
                disabled={isLoading}
                className="px-4 py-2 md:mr-4 uppercase bg-[#D6B27D] rounded-lg mt-6 text-md font-semibold cursor-pointer text-white"
              >
                Fortsätt
              </button>
            </div>
          </form>
        ) : (
          <form
            onSubmit={addNewEvent}
            className="bg-[#ffffff] w-[50rem] h-[50rem] overflow-y-scroll px-8 py-6 rounded-lg relative"
          >
            <button
              onClick={() => {
                setIsModalOpen(false);
                seteventData([]);
              }}
            >
              <img
                src={deleteIcon}
                className="absolute top-5 right-5 h-6 w-6"
              />
            </button>
            <section
              className="w-full py-10 px-10 border-[1px] rounded-lg flex flex-col items-center justify-center my-5"
              onDragOver={handleDragOver}
              onDrop={handleDrop}
            >
              <input
                id="file-upload"
                type="file"
                multiple
                accept=".jpg,.jpeg,.png,.gif"
                className="hidden"
                onChange={(e) => handleFileChange(e, 4, 1)}
              />
              <label htmlFor="file-upload" className="cursor-pointer">
                <img
                  className="ml-11 mb-2"
                  src={fileupload}
                  alt="fileupload"
                />
                <span className="text-[#97A0C3] text-xs ml-4 font-bold">
                  Ladda upp bild
                </span>
              </label>
              <div className="flex my-5">
                {base64Image &&
                  base64Image.map((imageurl: any, i: any) => {
                    return (
                      <>
                        <div className="relative ">
                          <img
                            height={200}
                            width={200}
                            className="px-3"
                            src={imageurl.values}
                          />
                          <RxCross2
                            className="cursor-pointer m-2 absolute top-2 right-2 bg-red-300"
                            onClick={(id) => {
                              deleteImage(i);
                            }}
                          />
                        </div>
                      </>
                    );
                  })}
              </div>
            </section>
            <div className="flex justify-center">
              <button
                onClick={() => setFormStep(1)}
                disabled={isLoading}
                className="px-4 py-2 md:mr-4 bg-[#D6B27D] rounded-lg mt-6 text-md font-semibold cursor-pointer uppercase text-white"
              >
                Tillbaka
              </button>
              {base64Image ? (
                <button
                  type="submit"
                  disabled={isLoading}
                  className="px-4 py-2 bg-[#D6B27D] rounded-lg mt-6 text-md font-semibold cursor-pointer text-white uppercase"
                >
                  {addEventModalHeader === "Edit Event" ? 'Spara' : addEventModalHeader}
                </button>
              ) : null}
            </div>
          </form>
        )}
      </section>
    </div>
  );
};

export default AddEventsModal;
