import React, { useState, useRef, useEffect, useContext } from "react";
import notificationIcon from "../../assets/notification.svg";
import { getAdminUser, setRefreshToken, setToken } from "../../Utils/auth";
import RectangleIcon from "../../assets/downIcon.svg";
import { useNavigate, createSearchParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../Redux/store";
import { failureNotify, successNotify } from "../Toast/Toast";
import { setUserDetails } from "../../Redux/features/userSlice";
import { clearAll } from "../../Utils/auth";
import deleteIcon from "../../assets/Delete.png";
import { commonAxios, formAxios } from "../../Axios/service";
import Loader from "../Loader/Loader";
import Imagepicker from "../Imagepicker/Imagepicker";
import ImageShown from "../Imagepicker/ImageShown";
import { GrLogout } from "react-icons/gr";
import { AuthContext } from "../../Pages/RootLayout/RootLayout";

import {
  setNotification,
} from "../../Redux/features/notificationSlice";
import Notifications from "../Notifications/Notifications";
import { RxCross2 } from "react-icons/rx";

interface UserDetails {
  accountStatus: string;
  businessAddress: string;
  businessCity: string;
  businessName: string;
  businessNumber: string;
  businessPinCode: string;
  categories: string;
  createdAt: string;
  email: string;
  firstName: string;
  lastName: string;
  telephone: string;
  id: string;
  mobile: string;
  resetToken: string;
  updatedAt: string;
  profileImage: string;
}

const AdminHeader = () => {
  const userDetails = useSelector((state: RootState) => state.user.userDetails);

  console.log("userDetail--------->", userDetails.profileImage)

  const notificationCount = useSelector(
    (state: RootState) => state.notification.notificationDetails.commentCount
  );
  const notificationCountforMessage = useSelector(
    (state: RootState) => state.notification.notificationDetails.messageCount
  );

  const [dropdownVisible, setDropdownVisible] = useState(false);
  const [editModalVisible, setEditModalVisible] = useState(false);
  const [isloading, setIsLoading] = useState(false);
  const [bindedImage, setBindedImage] = useState<any>([]);
  const [imagevalue, setImageValue] = useState<any>([]);
  const [base64Image, setBase64Image] = useState<any>([]);
  const [messageCount, setMessageCount] = useState<number>(0);
  const naviagte = useNavigate();
  const { setIsDeleteEventModalOpen, setMenuClicked } = useContext(AuthContext);
  const [editedUserDetails, setEditedUserDetails] = useState<any>({
    accountStatus: "",
    businessAddress: "",
    businessCity: "",
    businessName: "",
    businessNumber: "",
    businessPinCode: "",
    categories: "",
    createdAt: "",
    email: "",
    firstName: "",
    lastName: "",
    telephone: "",
    id: "",
    mobile: "",
    resetToken: "",
    updatedAt: "",
    profileImage: "",
  });

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleLogout = () => {

    if (userDetails.role === "ROLE_ADMIN") {
      setIsDeleteEventModalOpen(true);
      setMenuClicked("");
      window.scrollTo(0, 0);
      // dispatch(setUserDetails({}));
      // successNotify("Logout successful");
      // clearAll();

    }
    else {
      setMenuClicked("");
      setIsDeleteEventModalOpen(true);
      window.scrollTo(0, 0);
      // dispatch(setUserDetails({}));
      // successNotify("Logout successful");
      // clearAll();

    }

    // else if (userDetails.role === "ROLE_SUPPLIER") {
    //   setMenuClicked("");
    //   setIsDeleteEventModalOpen(true);
    //   window.scrollTo(0, 0);
    //   dispatch(setUserDetails({}));
    //   successNotify("Logout successful");
    //   clearAll();
    //   navigate("/supplierlogin");
    // }
    // else {
    //   dispatch(setUserDetails({}));
    //   successNotify("Logout successful");
    //   clearAll();
    //   navigate("/login");
    // }
  };

  const toggleDropdown = () => {
    setDropdownVisible(!dropdownVisible);
    setBindedImage([])
    setImageValue([]);
    setBase64Image([]);
  };

  const updateProfile = async () => {
    setIsLoading(true);
    await formAxios.get("/cemo/appuser/" + userDetails.id).then(
      async (response) => {
        try {
          const userData = response.data;
          setEditedUserDetails({
            accountStatus: userData.accountStatus,
            businessAddress: userData.businessAddress,
            businessCity: userData.businessCity,
            businessName: userData.businessName,
            businessNumber: userData.businessNumber,
            businessPinCode: userData.businessPinCode,
            categories: userData.categories,
            createdAt: userData.createdAt,
            email: userData.email,
            firstName: userData.firstName,
            lastName: userData.lastName,
            telephone: userData.telephone,
            id: userData.id,
            mobile: userData.mobile,
            resetToken: userData.resetToken,
            updatedAt: userData.updatedAt,
            profileImage: userData.profileImage,
          });
          toggleDropdown();
          toggleEditModal();
          setBindedImage([{ id: 1, image: userData.profileImage }]);
        } catch (error) {
        } finally {
          setIsLoading(false);
        }
      },
      (error) => {
        failureNotify("Error Occured while Upload Profile image");
      }
    );
    setIsLoading(false);
  };

  const getUserDetails = async () => {
    await formAxios.get("/cemo/appuser/" + userDetails.id).then(
      async (response) => {
        try {
          const userData = response.data;
          setEditedUserDetails({
            accountStatus: userData.accountStatus,
            businessAddress: userData.businessAddress,
            businessCity: userData.businessCity,
            businessName: userData.businessName,
            businessNumber: userData.businessNumber,
            businessPinCode: userData.businessPinCode,
            categories: userData.categories,
            createdAt: userData.createdAt,
            email: userData.email,
            firstName: userData.firstName,
            lastName: userData.lastName,
            telephone: userData.telephone,
            id: userData.id,
            mobile: userData.mobile,
            resetToken: userData.resetToken,
            updatedAt: userData.updatedAt,
            profileImage: userData.profileImage,
          });
          setBindedImage([{ id: 1, image: userData.profileImage }]);
        } catch (error) {
        } finally {
          setIsLoading(false);
        }
      },
      (error) => {
        failureNotify("Error Occured while fetching Profile details");
      }
    );
  }

  useEffect(() => {
    getUserDetails()
  }, [])

  const toggleEditModal = () => {
    setEditModalVisible(!editModalVisible);
  };

  const saveProfileChanges = async () => {
    const formData = new FormData();
    let fileArray: string[] = [];
    for (let i = 0; i < imagevalue.length; i++) {
      fileArray.push(imagevalue[i]);
    }
    fileArray.forEach((file: any, i: any) => {
      formData.append(`file`, file);
    });
    formData.append("folder", "user-profile");
    if (fileArray.length != 0) {
      await formAxios.post("/cemo/upload/files", formData).then(
        async (data) => {
          try {
            editedUserDetails.profileImage = data.data[0];
            editedUserDetails.mobile = editedUserDetails.mobile;
            const response = await commonAxios.put(
              `/cemo/appuser/update/` + userDetails.id,
              editedUserDetails
            );
            if (response.data) {
              successNotify("Profile update successfully!!!");
              setBase64Image([]);
              setBindedImage([])
              setImageValue([])
              dispatch(setUserDetails(response.data));
              toggleEditModal();
            }
          } catch (error) {

          } finally {
          }
        },
        (error) => {

          failureNotify("Error Occured while Upload Profile image");
        }
      );
    } else {
      editedUserDetails.mobile = editedUserDetails.mobile;
      if (bindedImage.length === 0 && imagevalue.length === 0 && base64Image.length === 0) {
        delete editedUserDetails?.profileImage;
      }
      await commonAxios
        .put(`/cemo/appuser/update/` + userDetails.id, editedUserDetails)
        .then(
          (res) => {
            if (res.data) {
              successNotify("Profile update successfully!!!");
              setBindedImage([])
              setImageValue([]);
              setBase64Image([]);
              dispatch(setUserDetails(res.data));
              // setFormEnable(true)
              toggleEditModal();
            }
          },
          (err) => {
            failureNotify("Profile update failed !!!");
          }
        );
    }
  };

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setEditedUserDetails((prevUserDetails: any) => ({
      ...prevUserDetails,
      [name]: value,
    }));
  };

  const processUserRole = (role: string): string => {
    if (role === "ROLE_ADMIN") {
      return "Super Admin användare";
    } else if (role === "ROLE_SUPPLIER") {
      return "Leverantörsanvändare";
    } else {
      return "Kundanvändare";
    }
  };

  const dropdownRef = useRef<HTMLDivElement>(null);
  const processedUserRole = processUserRole(userDetails.role);

  useEffect(() => {
    if (userDetails.id) {
      getMessagesCount();
    }
  }, []);

  const getMessagesCount = async () => {
    if (userDetails.role === "ROLE_ADMIN") {
      try {
        const messageCount = await commonAxios.get(
          `cemo/appuser/notification/count?adminId=${userDetails.id}`
        );

        if (messageCount.data) {
          dispatch(setNotification(messageCount.data));
        }
      } catch (e) { }
    } else {
      try {
        const messageCount = await commonAxios.get(
          `cemo/appuser/notification/count?supplierId=${userDetails.id}&customerId=`
        );

        if (messageCount.data) {
          dispatch(setNotification(messageCount.data));
        }
      } catch (e) { }
    }
  };

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (
        dropdownRef.current &&
        !dropdownRef.current.contains(event.target as Node)
      ) {
        setDropdownVisible(false);
      }
    };

    document.addEventListener("click", handleClickOutside);
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, []);

  const conversionImage = (value: any) => {
    setBase64Image(value);
  };

  const handleImageUpload = (image: any) => {
    setImageValue(image);
  };

  const updatedPic = (image: any) => {
    setBindedImage(image);
  };

  const logOutSupplier = async () => {
    const adminUser = await getAdminUser();
    if (adminUser) {
      const parsedDetails = await JSON.parse(adminUser);
      await dispatch(setUserDetails(parsedDetails));
      setToken(parsedDetails?.accessToken);
      setRefreshToken(parsedDetails?.refreshToken);
      localStorage.removeItem("shadowToken");
      naviagte("/merchant", { replace: true });
      successNotify("Fortsätt som super admin!");
    } else {
      failureNotify("Supplier logout failed.");
    }
  };

  const handleFileChange = async (event: any) => {
    event.preventDefault();
    const files = event.target.files;
    const fileArray: any = Array.from(files);
    const allowedExtensions = [".jpg", ".jpeg", ".png", ".gif"];

    const allValid = fileArray.every((file: any) => {
      const extension = file.name.toLowerCase().split('.').pop();
      return allowedExtensions.includes(`.${extension}`);
    });

    if (allValid) {
      if (files.length <= 1) {
        // setSelectedFile(files[0]); // Store the selected image
        const promises = fileArray?.map((file: any, i: any) => {
          return new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.onload = (event: any) => {
              const base64String = event.target.result;
              resolve(base64String);
            };

            reader.onerror = (error) => {
              reject(error);
            };

            reader.readAsDataURL(file);
          });
        });
        Promise.all(promises)
          .then((base64Array: any) => {
            const need_image = base64Array?.map((value: any, i: any) => {
              return {
                values: value,
                id: i + 1,
              };
            });
            setBase64Image(need_image);
          })
          .catch((error) => { })
          .finally(() => {
            // onImageUpload(files);
            handleImageUpload(files)
          });
      }
    } else {
      failureNotify("Please upload valid image files (jpg, jpeg, png, gif).");
    }
  };
  return (
    <div className="h-[70px] flex items-center justify-end pr-5 border-b border-[#E2E2EA]">
      {/* <input type='text' placeholder='text here'/> */}
      {/* {isloading ? <Loader /> : null} */}
      <div className="flex justify-end items-center cursor-pointer">
        {localStorage.getItem("shadowToken") && (
          <div
            className="relative py-1 px-2 hover:bg-[#f6f2ee] rounded-md mr-4"
            onClick={() => logOutSupplier()}
          >
            {/* <GrLogout className="text-gray-800 w-[20px] h-[26px] " /> */}
            <p className="text-[#0a0909]">
              Tillbaka till admin
            </p>
          </div>
        )}

        <div
          className="cart-item py-2"
        // onClick={() =>
        //   navigate({
        //     pathname: "/messages",
        //     search: `?${createSearchParams({
        //       activeMenu: "messages",
        //     })}`,
        //   })
        // }
        >
          {userDetails.role !== "ROLE_CUSTOMER" ? (
            <div
              className="cart-item py-2"
            >
              <Notifications />
            </div>
          ) : null}
          {/* <img src={notificationIcon} className="mx-4 cursor-pointer w-6" />
          {notificationCount + notificationCountforMessage !== 0 && (
            <div className="badge">
              {notificationCount + notificationCountforMessage}
            </div>
          )} */}
        </div>
        <>
          <div ref={dropdownRef}>
            <div className="flex flex-row" onClick={toggleDropdown}>
              {userDetails.profileImage ? (
                <img
                  src={userDetails.profileImage}
                  className="h-[32px] w-[32px] mx-4 my-auto rounded-lg"
                />
              ) : (
                <div className="h-[36px] w-[36px] mx-4 my-auto rounded-full bg-[#D6B37D]">
                  <div className="flex justify-center items-center mt-2 text-[#ffffff] font-bold">
                    {userDetails?.firstName[0]?.toUpperCase() +
                      userDetails?.lastName[0]?.toUpperCase()}
                  </div>
                </div>
              )}

              <div className="text-secondary text-[14px] font-bold">
                {/* {`${userDetails.firstName} ${userDetails.lastName}`} */}
                {editedUserDetails.businessName}
                <br />
                <span className="flex justify-center text-[#92929D] text-[12px]">
                  {processedUserRole}
                </span>
              </div>
              <img src={RectangleIcon} className="mx-4" />
            </div>
          </div>
          {dropdownVisible && (
            <div className="dropdown-menu absolute bg-white w-40 shadow-md z-10 ml-5">
              <a
                href="#"
                onClick={updateProfile}
                className="block px-4 py-2 text-secondary font-semibold text-[12px] hover:bg-gray-100"
              >
                Uppdatera profil
              </a>
              <a
                href="#"
                onClick={handleLogout}
                className="block px-4 py-2 text-secondary font-semibold text-[12px] hover:bg-gray-100"
              >
                Logga ut
              </a>
            </div>
          )}
          {editModalVisible && (
            <div className="z-10 overflow-y-scroll modal-overlay fixed top-0 right-0 w-[100%] h-full bg-black bg-opacity-50 flex justify-center items-center">
              <div className="modal bg-white w-[600px] p-4 rounded-lg relative top-20">
                <h2 className="text-lg font-semibold mb-4 text-secondary text-center p-3">Redigera produkt</h2>
                <form className="flex flex-col">
                  <button onClick={toggleEditModal}>
                    <img
                      src={deleteIcon}
                      className="absolute top-5 right-5 h-6 w-6"
                    />
                  </button>
                  <div className="mb-4">
                    <label htmlFor="firstName" className="block mb-1 font-extrabold text-secondary">
                      Förnamn:
                    </label>
                    <input
                      type="text"
                      id="firstName"
                      name="firstName"
                      value={editedUserDetails.firstName}
                      onChange={handleInputChange}
                      className="border border-gray-300 rounded px-2 py-1 w-full text-secondary"
                    />
                  </div>
                  <div className="mb-4">
                    <label htmlFor="lastName" className="block mb-1 font-extrabold text-secondary">
                      Efternamn:
                    </label>
                    <input
                      type="text"
                      id="lastName"
                      name="lastName"
                      value={editedUserDetails.lastName}
                      onChange={handleInputChange}
                      className="border border-gray-300 rounded px-2 py-1 w-full text-secondary"
                    />
                  </div>
                  <div className="mb-4">
                    <label htmlFor="email" className="block mb-1 font-extrabold text-secondary">
                      E-post
                    </label>
                    <input
                      type="text"
                      id="email"
                      name="email"
                      value={userDetails.email}
                      disabled
                      className="border border-gray-300 rounded px-2 py-1 w-full text-secondary"
                    />
                  </div>
                  <div className="mb-4">
                    <label htmlFor="mobile" className="block mb-1 font-extrabold text-secondary">
                      Telefon:
                    </label>
                    <input
                      type="text"
                      id="mobile"
                      name="mobile"
                      value={editedUserDetails.mobile}
                      onChange={handleInputChange}
                      className="border border-gray-300 rounded px-2 py-1 w-full text-secondary"
                    />
                  </div>
                  <div className="mb-4">
                    <label htmlFor="profilePic" className="block mb-1 font-extrabold text-secondary">
                      Profilbild:
                    </label>
                    {/* <Imagepicker
                      max={1}
                      value="user-profile"
                      onImageUpload={handleImageUpload}
                      updatedValue={conversionImage}
                    />
                   {userDetails.profileImage && <ImageShown
                      bindedImage={bindedImage}
                      base64Image={base64Image}
                      updatedValue={conversionImage}
                      imageBinding={updatedPic}
                      deleteImages={conversionImage}
                    />} */}

                    <div>
                      <input
                        type="file"
                        id="file-input"
                        name="file-input"
                        onChange={handleFileChange}
                        // disabled={isDisabled}
                        multiple
                        className="w-[100%]"
                      />
                      <label id="file-input-label" htmlFor="file-input">Select a File</label>
                      {/* {error && <p className="text-red-500">{error}</p>} */}
                    </div>
                    <div className="flex ">
                      {base64Image && base64Image.length > 0
                        ? base64Image?.map((img: any) => {
                          return (
                            <>
                              <div className="px-3 py-2 relative">
                                <img
                                  src={img.values}
                                  alt="image"
                                  width={200}
                                  height={200}
                                  className="img"
                                />
                                <RxCross2
                                  className="cursor-pointer m-2 absolute top-0 right-1 bg-red-400"
                                  onClick={(id) => {
                                    // deleteImage(img.id, "createproduct(or)event");
                                    setBase64Image([]);
                                    setBindedImage([])
                                    setImageValue([]);
                                  }}
                                />
                              </div>
                            </>
                          );
                        })
                        : bindedImage
                          ? bindedImage?.map((img: any, index: any) => {
                            return (
                              <>
                                {/* { index === 1 ?  */}
                                <div className="px-3 py-2 relative">
                                  {img.image !== null ?
                                    <>
                                      <img
                                        src={img.image}
                                        alt="image"
                                        width={100}
                                        height={100}
                                        className="img"
                                      />
                                      {
                                        <RxCross2
                                          className="cursor-pointer m-2 absolute top-0 right-1 bg-red-400"
                                          onClick={(id) => {
                                            // deleteImage(img.id, "updateproduct(or)updateevent");
                                            setBindedImage([])
                                            setImageValue([])
                                          }}
                                        />
                                      }
                                    </>
                                    : null}
                                </div>
                                {/* : null}  */}
                              </>
                            );
                          })
                          : null}
                    </div>
                  </div>
                  <div className="mb-4">
                    <label htmlFor="telephone" className="block mb-1 font-extrabold text-secondary">
                      organisationnumber:
                    </label>
                    <input
                      type="text"
                      id="telephone"
                      name="businessNumber"
                      value={editedUserDetails.businessNumber}
                      onChange={handleInputChange}
                      className="border border-gray-300 rounded px-2 py-1 w-full text-secondary"
                    />
                  </div>
                  <div className="flex justify-end">
                    <button
                      type="button"
                      onClick={saveProfileChanges}
                      className="px-4 py-1 rounded-md bg-[#D6B37D] cursor-pointer font-semibold text-white hover:bg-[#FFE5D8] hover:text-black transition-all mr-2 uppercase"
                    >
                      Spara
                    </button>
                    <button
                      type="button"
                      onClick={toggleEditModal}
                      className="px-4 py-1 rounded-md bg-golden text-white cursor-pointer font-semibold text-white hover:bg-[#FFE5D8] hover:text-black transition-all uppercase"
                    >
                      Avbryt
                    </button>
                  </div>
                </form>
              </div>
            </div>
          )}
        </>
      </div>
    </div>
  );
};

export default AdminHeader;
