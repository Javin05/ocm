const Spinner = () => {
    return (
        <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center opacity-90 z-50">
            <div className="border-t-8 border-[#F7CB73] rounded-full animate-spin h-12 w-12"></div>
        </div>
    );
};

//background:
export default Spinner;