import React from 'react';
import arrowOne from "../../assets/arrow-21.svg"

const StaderCard = ({ staderImg, staderTitle, onCardClick }: any) => {
    return (
        <div className='relative'>
            <img src={staderImg} className='' />
            <h1 className='text-[32px] text-[#D6B27D] absolute bottom-2 mx-4 font-semibold'>{staderTitle}</h1>
            <button onClick={() => onCardClick(staderTitle)} className='bg-[#D6B27D] py-4 px-4  absolute bottom-0 right-0 rounded-[4px]'><img src={arrowOne} /></button>
            <div className='staderCard text-[200px] md:text-[180px]'> {staderTitle.charAt(0)} </div>
        </div>
    )
}

export default StaderCard