import React from "react";

const BookingDetails = ({ userName, date, time, category }: any) => {
  return (
    <div className="border border-[#F6F7FB] md:p-20 md:rounded-[48px]">
      <h1 className="text-[#1A202C] font-semibold text-[32px]">
      Bokningsdetaljer
      </h1>
      <div className="">
        <h3 className="text-[#1A202C] text-[24px] mt-5">Namn: {userName}</h3>
        <h3 className="text-[#1A202C] text-[24px] mt-5">Datum: {date}</h3>
        <h3 className="text-[#1A202C] text-[24px] mt-5">Tid: {time}</h3>
        <h3 className="text-[#1A202C] text-[24px] font-bold mt-5">
          kategori: {category}
        </h3>
        <button className="w-full mt-8 bg-[#D6B27D] font-semibold text-[#F7FAFC] text-[20px] py-2 rounded-[10px] uppercase">
          Redigera
        </button>
      </div>
    </div>
  );
};

export default BookingDetails;
