import moment from "moment";
import { useState } from "react";

const AvailableSlots = ({ slots }: any) => {
  console.log(slots, "in operatingHours");

  const slotClass = (e: any) => {
    return e.bookedCount >= e.maxAvailableCount
      ? 'bg-golden text-white'
      : 'bg-[#FFE5D8] text-[#353231]';
  };

  // Function to convert time to railway time format
  const convertTime = (time: any) => {
    return moment(time, 'hh:mm A').format('HH:mm');
  };

  return (
    <>
      {slots.map((e: any, index: any) => (
        <div className={`w-full py-4 px-5 border-b-2`} key={index}>
          <div className="flex">
            <p className={`border p-4 font-semibold border-[#EAEBEF] mx-2 my-2 rounded-lg ${slotClass(e)}`}>
              <div className="flex mx-2 text-secondary">
                {convertTime(e.inTime)} - {convertTime(e.outTime)}
              </div>
              <div className="flex">
                <section className="w-3 h-3 mt-1 mx-2 rounded-full bg-[#D6B37D]" />
                <span className={`${slotClass(e)} text-secondary`}> Available: {e.maxAvailableCount - e.bookedCount}</span>
              </div>
              <div className="flex">
                <section className="w-3 h-3 mt-1 mx-2 rounded-full bg-green-700" />
                <span className={'slotClass(e) text-secondary'}>Booked: {e.bookedCount}</span>
              </div>
            </p>
          </div>
        </div>
      ))}
    </>
  );
};

export default AvailableSlots;
