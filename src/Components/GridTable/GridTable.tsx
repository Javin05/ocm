import React, { useEffect, useMemo, useState } from "react";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-alpine.css";
import { AgGridReact } from "ag-grid-react";
import "./AgGrid.css";
const GridTable = ({ columnDefs, gridOptions, rowData, onGridReady }: any) => {
  const defaultColDef = useMemo(
    () => ({
      sortable: true,
      resizable: true,
    }),
    []
  );

  useEffect(() => {
    const ag4to = document.getElementById('ag-4-to');
    const ag4rowcount = document.getElementById('ag-4-row-count');
    if (ag4rowcount) {
      ag4rowcount.innerHTML = ag4rowcount.innerText + ' ' + 'items'
    }
    if (ag4to) {
      ag4to.innerHTML = '-';
    }
  }, [])

  return (
    <div className="h-[85vh] max-h-[85vh]  ag-theme-alpine custom-ag-grid">
      <AgGridReact
        rowData={rowData}
        columnDefs={columnDefs}
        gridOptions={gridOptions}
        headerHeight={38}
        defaultColDef={defaultColDef}
        pagination={true}
        paginationPageSize={10}
        onGridReady={onGridReady}
      />
    </div>
  );
};

export default GridTable;
