import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
} from "react-router-dom";
import BookingScreen from "../Pages/BookingScreen/BookingScreen";
import Checkout from "../Pages/Checkout/Checkout";
import DetailsPage from "../Pages/DetailsPage/DetailsPage";
import LandingPage from "../Pages/LandingPage/LandingPage";
import Login from "../Pages/LoginPage/LoginPage";
import Signup from "../Pages/SignUpPage/SignUp";
import SupplierLogin from "../Pages/SupplierAuth/Login/SupplierLogin";
import SupplierSignup from "../Pages/SupplierAuth/SignUp/SupplierSignup";
import SupplierSuccessfulRegistration from "../Pages/SupplierAuth/SignUp/SupplierSuccessfulRegistration";
import QuotationRefectedSuccessfully from "../Pages/QuotationList/QuotationRefectedSuccessfully";
import ProductDetail from "../Pages/ProductDetail/ProductDetail";
import RootLayout from "../Pages/RootLayout/RootLayout";
import ProtectedRoute from "../Components/ProtectedRoute/ProtectedRoute";
import AdminOrderList from "../Pages/SuperAdmin/OrderList/AdminOrderList";
import Shopping from "../Pages/Shopping/Shopping";
import MerchantScreen from "../Pages/SuperAdmin/MerchantScreen/MerchantScreen";
import ScheduleScreen from "../Pages/SuperAdmin/ScheduleScreen/ScheduleScreen";
import CategoryScreen from "../Pages/SuperAdmin/CategoryScreen/CategoryScreen";
import EventsList from "../Pages/EventsList/EventsList";
import Payment from "../Pages/SupplierAuth/Payment/Payment";
import Customerlist from "../Pages/SuperAdmin/Customerlist/Customerlist";
import Products from "../Pages/SuperAdmin/Productlist/Products";
import AdminLogin from "../Pages/SuperAdmin/AdminLogin/AdminLogin";
import OrderHistory from "../Pages/OrderHistory/OrderHistory";
import Messages from "../Pages/Messages/Messages";
import ForgotPassword from "../Pages/ForgotPassword/ForgotPassword";
import ResetPassword from "../Pages/ResetPassword/ResetPassword";
import ContactUs from "../Pages/ContactUs/ContactUs";
import ContactUsSuccess from "../Pages/ContactUs/ContactUsSuccess";
import Butiker from "../Pages/Butiker/Butiker";
import FAQ from "../Pages/FAQ/FAQ";
import Aboutus from "../Pages/Aboutus/Aboutus";
import ActivateSupplier from "../Pages/SuperAdmin/ActivateSupplier/ActivateSupplier";
import Reviewandcomments from "../Pages/Reviewandcomments/Reviewandcomments";
import OrderDetails from "../Pages/OrderDetails/OrderDetails";
import PendingOrderDetails from "../Pages/PendingOrderDetails/PendingOrderDetails";
import PaymentSuccess from "../Pages/SupplierAuth/Payment/PaymentSuccessScreen";
import ProfilePasswordReset from "../Pages/ResetPassword/ProfilePasswordReset";
import BackpanelSupport from "../Pages/BackpanelSupport/BackpanelSupport";
import ContactAdmin from "../Pages/CustomerChat/ContactAdmin";
import ContactSupplier from "../Pages/CustomerChat/ContactSupplier";
import QuotationList from "../Pages/QuotationList/QuotationList";
import Getquote from "../Pages/getquote/Getquote";
import Contactsupplier from "../Pages/Contact-supplier/Contactsupplier";
import ContactUsFeedBack from "../Pages/SuperAdmin/ContactUsFeedBack/ContactUsFeedBack";
import Integritetspolicy from "../Components/Footer/Integritetspolicy";
import Retur from "../Components/Footer/Retur";
import AllmännaVillkorSamarbetspartners from "../Components/Footer/CooperationPatners";
import TermsAndCondition from "../Components/Footer/TermsAndCondition";
import PopularActivity from "../Pages/PopularActivity/PopularActivity";
import ContactCustomer from "../Pages/BackpanelSupport/ContactCustomer";
import NotifyMe from "../Pages/SuperAdmin/NotifyMe/NotifyMe";
import CustomerQuotationList from "../Pages/QuotationList/CustomerQuotationList";
import ProductScreen from "../Pages/ProductScreen/ProductScreen";
import EventListScreen from "../Pages/EventListScreen/EventListScreen";

const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      {/* Public routes */}
      <Route path="/signup" element={<Signup />} />
      <Route path="/login" element={<Login />} />
      <Route path="/supplierlogin" element={<SupplierLogin />} />
      <Route path="/adminlogin" element={<AdminLogin />} />
      <Route path="/suppliersignup" element={<SupplierSignup />} />
      <Route path="/forgotpassword" element={<ForgotPassword />} />
      <Route path="/reset-password" element={<ResetPassword />} />
      <Route path="/contactus/:isScroll" element={<ContactUs />} />
      <Route path="butiker" element={<Butiker />} />
      <Route path="/faq" element={<FAQ />} />
      <Route path="/aboutus" element={<Aboutus />} />
      <Route path="/termsandconditions" element={<TermsAndCondition />} />
      <Route
        path="/cooperationpatners"
        element={<AllmännaVillkorSamarbetspartners />}
      />
      <Route path="/Integritetspolicy" element={<Integritetspolicy />} />
      <Route path="/Retur" element={<Retur />} />
      <Route
        path="/supplierSuccessfulRegistration"
        element={<SupplierSuccessfulRegistration />}
      />
      <Route
        path="/quotationRejected"
        element={<QuotationRefectedSuccessfully />}
      />
      <Route path="/contactUsSuccess" element={<ContactUsSuccess />} />
      {/* Protected routes */}
      <Route path="/" element={<RootLayout />}>
        <Route index element={<LandingPage />} />
        <Route path="/popular" element={<PopularActivity />} />
        <Route path="/payment" element={<Payment />} />
        <Route
          path="/PaymentSuccess"
          element={<ProtectedRoute component={<PaymentSuccess />} />}
        />
        <Route path="/details" element={<DetailsPage />} />
        <Route
          path="/booking"
          element={<ProtectedRoute component={<BookingScreen />} />}
        />

        <Route
          path="/orderDetails"
          element={<ProtectedRoute component={<OrderDetails />} />}
        />
        <Route
          path="/pendingOrderDetails"
          element={<ProtectedRoute component={<PendingOrderDetails />} />}
        />
        <Route
          path="/checkout"
          element={<ProtectedRoute component={<Checkout />} />}
        />
        <Route
          path="/Reviewandcomments"
          element={<ProtectedRoute component={<Reviewandcomments />} />}
        />
        <Route
          path="/cart"
          element={<ProtectedRoute component={<ProductDetail />} />}
        />
        <Route
          path="/orderList"
          element={<ProtectedRoute component={<AdminOrderList />} />}
        />
        <Route
          path="/messages"
          element={<ProtectedRoute component={<Messages />} />}
        />
        <Route
          path="/eventslist"
          element={<ProtectedRoute component={<EventsList />} />}
        />

        <Route
          path="/eventscreen"
          element={<ProtectedRoute component={<EventListScreen />} />}
        />
        <Route
          path="/products"
          element={<ProtectedRoute component={<Products />} />}
        />

        <Route
          path="/productscreen"
          element={<ProtectedRoute component={<ProductScreen />} />}
        />

        <Route
          path="/ordershistory"
          element={<ProtectedRoute component={<OrderHistory />} />}
        />
        <Route
          path="/shoppingCart"
          element={<ProtectedRoute component={<Shopping />} />}
        />
        <Route
          path="/merchant"
          element={<ProtectedRoute component={<MerchantScreen />} />}
        />
        <Route
          path="/getquote"
          element={<ProtectedRoute component={<Getquote />} />}
        />

        <Route
          path="/customerlist"
          element={<ProtectedRoute component={<Customerlist />} />}
        />

        <Route
          path="/activateSupplier"
          element={<ProtectedRoute component={<ActivateSupplier />} />}
        />

        <Route
          path="/schedule"
          element={<ProtectedRoute component={<ScheduleScreen />} />}
        />
        <Route
          path="/category"
          element={<ProtectedRoute component={<CategoryScreen />} />}
        />
        <Route
          path="/profile-password-reset"
          element={<ProtectedRoute component={<ProfilePasswordReset />} />}
        />
        <Route
          path="/contactFeedback"
          element={<ProtectedRoute component={<ContactUsFeedBack />} />}
        />
        <Route
          path="/backpanel-support"
          element={<ProtectedRoute component={<BackpanelSupport />} />}
        />
        <Route
          path="/contact-admin"
          element={<ProtectedRoute component={<ContactAdmin />} />}
        />
        <Route
          path="/contact-suppliers"
          element={<ProtectedRoute component={<ContactSupplier />} />}
        />
        <Route
          path="/quotations-list"
          element={<ProtectedRoute component={<QuotationList />} />}
        />
        <Route
          path="/quotations-customer"
          element={<ProtectedRoute component={<CustomerQuotationList />} />}
        />
        <Route
          path="/customer-messages"
          element={<ProtectedRoute component={<ContactCustomer />} />}
        />
        <Route
          path="/notifyme"
          element={<ProtectedRoute component={<NotifyMe />} />}
        />
      </Route>
    </>
  )
);

export default router;
