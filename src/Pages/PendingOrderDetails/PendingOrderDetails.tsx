import React from "react";
import { useLocation } from "react-router-dom";

const PendingOrderDetails = () => {
  const location = useLocation();
  const state = location.state;
  return (
    <>
      <div className="grid grid-cols-2 p-10">
        <div className="p-5">
          <img
            src={state.image}
            className="rounded-lg object-cover"
            height={100}
            width={600}
          />
        </div>
        <div className="p-5">
          <div className="mb-6 border-b-[1px] border-black flex justify-between">
            <div className="">
              <h1 className="font-bold text-[30px] text-[#c48c39]">
                {state?.orderDetails[0]?.itemName}
              </h1>
              <p className="text-gray-600 mb-5">
                Ordernummer #{" "}
                <span className="font-bold">{state.orderNumber}</span>
              </p>
            </div>
            <div className="">
            <h1 className="font-bold text-[30px] text-[#c48c39]">
                Payment Type
              </h1>
              <p className="text-gray-600 mb-5">
                <span className="font-bold">ONLINE</span>
              </p>
            </div>
          </div>
          <div>
            <p className="text-gray-600">{state.orderDetails.length} Items</p>
          </div>
          <section className="pb-6 mb-8 border-dotted border-b-2 border-[#e3e3e3]">
            {state?.orderDetails.map((item: any) => (
              <div className="flex flex-row justify-between items-center w-full mb-2">
                <span className="font-semibold text-gray-600">
                  {item.itemName} {item.quantity}
                </span>
                <span className="text-gray-600 text-lg font-medium">
                  moms/item -{" "}
                  {item.itemCategory === "EVENT"
                    ? 25
                    : item.momsPercentagePerItem}{" "}
                  %
                </span>
                <span className="text-gray-600 text-lg font-medium">
                  {item.price} SEK
                </span>
              </div>
            ))}
          </section>
          <section className="pb-8 mb-6 border-b-[1px] border-black">
            <div className="flex flex-row justify-between items-center w-full mb-2">
              <span className="font-medium text-gray-600">Item Total</span>
              <span className="text-gray-600 text-lg font-medium">
              {state?.totalAmount} SEK
              </span>
            </div>
            <div className="flex flex-row justify-between items-center w-full mb-2">
              <span className="text-gray-600 font-medium">MOMS</span>
              <span className="text-gray-600 text-lg font-medium">
                {state?.taxes} SEK
              </span>
            </div>
          </section>
          <div className="flex flex-row justify-between items-center w-full mb-2">
            <span className="text-gray-600 font-bold">BILL TOTAL</span>
            <span className="text-gray-600 font-bold">
              {state?.totalItemPrice} SEK
            </span>
          </div>
        </div>
      </div>
    </>
  );
};

export default PendingOrderDetails;
