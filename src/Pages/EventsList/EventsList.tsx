import React, { useEffect, useState } from "react";
import AdminHeader from "../../Components/AdminHeader/AdminHeader";
import GridTable from "../../Components/GridTable/GridTable";
import SideNav from "../../Components/SideNav/SideNav";
import searchIcon from "../../assets/adminSearch.svg";
import eyeIcon from "../../assets/viewIcon.svg";
import deleteIcon from "../../assets/Delete.png";
import eventdeleteicon from "../../assets/deleteIcon.svg";
import editIcon from "../../assets/editIcon.svg";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { commonAxios, formAxios } from "../../Axios/service";
import { successNotify, failureNotify } from "../../Components/Toast/Toast";
import { setUserDetails } from "../../Redux/features/userSlice";
import { RootState } from "../../Redux/store";
import { setToken } from "../../Utils/auth";
import { FormikContext, useFormik } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import Loader from "../../Components/Loader/Loader";
import AddEventsModal from "../../Components/AddEventsModal/AddEventsModal";
import Imagepicker from "../../Components/Imagepicker/Imagepicker";
import ImageShown from "../../Components/Imagepicker/ImageShown";
import AddProductsModal from "../../Components/AddProductsModal/AddProductsModal";
import AddProducts from "../../Components/AddProduct/Addproducts";
import Spinner from "../../Components/Loader/Spinner";
import moment from "moment";

type initialProductValues = {
  name: string;
  description: string;
  price: string;
  quantity: string;
  category: string;
  taxId: string;
};

interface TaxOption {
  id: string;
  taxPercentage: number;
}

const EventsList = () => {
  const [rowData, setRowData] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isAddProductModalOpen, setIsAddProductModalOpen] = useState(false);
  const [isActionModalOpen, setIsActionModalOpen] = useState(false);
  const [isDeleteEventModalOpen, setIsDeleteEventModalOpen] = useState(false);
  const [customerData, setCustomerData] = useState<any>([]);
  const [eventData, seteventData] = useState<any>([]);
  const [eventsList, setEventsList] = useState([]);
  const [filterData, setFilterData] = useState("");
  const [imagevalue, setImageValue] = useState<any>([]);
  const [base64Image, setBase64Image] = useState<any>([]);
  const [moms, setMoms] = useState<any>([]);
  const [formData, setFormData] = useState({
    name: "",
    description: "",
    price: "",
    quantity: "",
    category: "",
    taxId: "",
    files: [],
  });
  const [productCategory, setProductCategory] = useState<any>(null);
  const [productFiles, setProductFiles] = useState<any>([]);
  const [momCalculation, setMomCalculation] = useState<any>("");
  const [addEventModalHeader, setAddEventModalHeader] = useState("Skapa nytt event");
  const [rowHeight, setRowHeight] = useState(50);

  const userInfo = useSelector((state: RootState) => state.user.userDetails);

  const [columnDefs]: any = useState([
    { headerName: "SNo", field: "Sno", width: 60 },
    { headerName: "Event Id", field: "eventNumber", width: 110 },
    { headerName: "Eventnamn", field: "name", width: 130 },

    { headerName: "Startdatum", field: "startDate", width: 150 },
    { headerName: "Slutdatum", field: "endDate", width: 150 },
    { headerName: "Pris (SEK)", field: "price", width: 100 },
    { headerName: "Moms %", field: "taxType.taxPercentage", width: 90 },
    {
      headerName: "Skapad den", field: "dateTime", width: 150,
      cellRendererFramework: (params: any) => {
        const parsedDate = moment(params.value, "DD/MM/YYYY HH:mm");
        const formattedDate = parsedDate.format("YYYY-MM-DD HH:mm");
        return formattedDate; // Assuming you want to display the formatted date in the cell
      }
    },

    {
      headerName: "Åtgärder",
      cellRendererFramework: (params: any) => {
        const handleButtonClick = (data: any) => {
          setCustomerData(data.data);
          setIsActionModalOpen(true);
        };
        const handleAddProduct = () => {
          setIsLoading(true);
          getEventbyid(params.data);
          setAddEventModalHeader("Edit Event");
        };
        const handleEventDelete = () => {
          seteventData(params.data);
          setIsDeleteEventModalOpen(true);
        };
        return (
          <>
            <div className="flex flex-row justify-between items-center">
              <button
                onClick={() => {
                  handleButtonClick(params);
                }}
                className="mr-2"
              >
                <img
                  src={eyeIcon}
                  className="mx-1 border-[#E2E2EA] p-1 "
                />
              </button>
              <button onClick={handleAddProduct}>
                <img
                  src={editIcon}
                  className=" mx-1 border-[#E2E2EA]  p-1 "
                />
              </button>
              <button onClick={handleEventDelete}>
                <img
                  src={eventdeleteicon}
                  className=" mx-2 border-[#E2E2EA]  p-1"
                  width={30}
                  height={30}
                />
              </button>
            </div>
          </>
        );
      },
      width: 240,
    },
  ]);



  const onGridReady = (params: any) => {
    const columnApi = params.companyName;
    columnApi.setColumnVisible('companyName', userInfo.role === 'ROLE_ADMIN'); // Set visibility dynamically

  };



  const getEventbyid = async (eventid: any) => {
    try {
      const response = await commonAxios.get(`/cemo/events/${eventid.id}`);

      if (response.data) {
        seteventData(response.data);
        setIsModalOpen(true);
        setIsLoading(false);
      }
    } catch (e) { }
  };

  const [isLoading, setIsLoading] = useState(false);
  const [options, setOptions] = useState<any>([{}]);
  const [choose, setChoose] = useState<any>([{}]);
  let base64ProductImage: string | undefined;

  const gridOptions = {
    getRowHeight: function (params: any) {
      return 80;
    },
    defaultColDef: {
      cellStyle: { fontFamily: "Montserrat" },
    },
  };

  const getEventCategories = async () => {
    const response = await commonAxios.get("/cemo/categories");

    if (response.status === 200) {
      const activeCategories = response.data.filter((category: any) => category.status === "ACTIVE");
      const categoriesList = activeCategories.map((category: any) => ({
        value: category.categoryName,
        label: category.categoryName,
        id: category.id,
      }));

      setOptions(categoriesList);
    }
  };

  const getEventCities = async () => {
    const response = await commonAxios.get("/cemo/cities");
    if (response.status === 200) {
      const cityList = response.data.map((city: any) => {
        return {
          value: city.city,
          label: city.city,
          id: city.id,
        };
      });
      setChoose(cityList);
    }

  }

  const getFormattedDate = (timeStamp: string) => {
    const date = new Date(timeStamp);

    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString();
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");

    const formattedDate = `${day}/${month}/${year} ${hours}:${minutes}`;

    return formattedDate;
  };

  const getEventsList = async () => {
    setIsLoading(true);
    try {
      const response: any = await commonAxios.get("/cemo/events/all");

      let value = response.data.filter((val: any) => {
        return val.status !== 'INACTIVE'
      })


      // setEventsList(response.data);
      setIsLoading(false);
      const gridData = value.map((item: any, index: any) => {
        const idSplit = item.id.split("-");
        return {
          Sno: index + 1,
          merchantName: item.appUser?.firstName + " " + item.appUser?.lastName,
          customerMobile: item.appUser?.mobile,
          dateTime: getFormattedDate(item.createdAt),
          ...item,
          uid: idSplit.at(idSplit.length - 1),
        };
      });
      setRowData(gridData);
    } catch (error) {
      setIsLoading(false);
      failureNotify("oops!something went wrong");
    }
  };

  const getEventsListBySupplier = async () => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get(
        `/cemo/events/supplier/${userInfo.id}`
      );
      let value = response.data.filter((val: any) => {
        return val.status !== 'INACTIVE'
      })
      setEventsList(value);
      setIsLoading(false);
      const gridData = value?.map((item: any, index: any) => {
        const idSplit = item.id.split("-");
        return {
          Sno: index + 1,
          merchantName: item.appUser?.firstName + " " + item.appUser?.lastName,
          customerMobile: item.appUser?.mobile,
          dateTime: getFormattedDate(item.createdAt),
          ...item,
          uid: idSplit.at(idSplit.length - 1),
          name: item.name.toUpperCase(),
        };
      });
      setRowData(gridData);
    } catch (error) {
      setIsLoading(false);
      failureNotify("oops!something went wrong");
    }
  };

  const getMoms = async () => {
    const response = await commonAxios.get("/cemo/taxtypes/all");
    if (response.data) {
      setMoms(response.data);
    }
  };

  useEffect(() => {
    const handleEscPress = (event: KeyboardEvent) => {
      if (event.key === "Escape") {
        setIsModalOpen(false);
        setIsActionModalOpen(false);
        setIsAddProductModalOpen(false);
        setIsDeleteEventModalOpen(false);
        setCustomerData([]);
        seteventData([]);
      }
    };

    document.addEventListener("keydown", handleEscPress);

    return () => {
      document.removeEventListener("keydown", handleEscPress);
    };
  }, []);

  useEffect(() => {
    if (userInfo.role === "ROLE_ADMIN") {
      getEventsList();
    } else {
      getEventsListBySupplier();
    }
    getEventCategories();
    getEventCities();
    getMoms();
  }, []);

  const formattedDate = (date: any) => {
    const formattedDate = moment(date).format('YYYY-MM-DD HH:mm');
    return formattedDate;
  };
  const CustomerDetails = () => {
    return (
      <div className="w-full h-full fixed top-0 left-0 flex items-center justify-center bg-[#00000080] z-40">
        <div className="bg-white w-[80%] md:w-[80%] lg:w-[60%] xl:w-[60%] max-w-[900px] max-h-[90%] overflow-y-auto p-10 relative">

          <button onClick={() => setIsActionModalOpen(false)}>
            <img
              src={deleteIcon}
              className="absolute top-5 right-5 h-6 w-6"
            />
          </button>
          <h2 className="text-xl font-bold font-[sofiapro] text-center text-secondary">
            Eventuppgifter
          </h2>
          <div className="grid grid-cols-1 gap-4">
            <div className="p-4">
              <h1 className="-mt-3 font-bold text-secondary">Leverantörsuppgifter</h1>
              <div className="border p-3 rounded-lg mt-1">
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Förnamn</h1>
                      <h1 className="text-secondary">{customerData?.appUser.firstName}</h1>
                    </div>
                  </div>
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Efternamn</h1>
                      <h1 className="text-secondary">{customerData?.appUser.lastName}</h1>
                    </div>
                  </div>
                </div>
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Telefonnummer</h1>
                      <h1 className="text-secondary">{customerData?.appUser.mobile}</h1>
                    </div>
                  </div>
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">E-post</h1>
                      <h1 className="text-secondary">{customerData?.appUser.email}</h1>
                    </div>
                  </div>
                </div>
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Telefon</h1>
                      <h1 className="text-secondary">{customerData?.appUser?.telephone || "-"}</h1>
                    </div>
                  </div>
                  <div className="p-4">
                    <h1 className="font-semibold text-secondary">Skapad den</h1>
                    <h1 className="text-secondary">{formattedDate(customerData?.createdAt)}</h1>
                  </div>
                </div>
              </div>
            </div>
            <div className="p-4 h-[30rem]">
              <h1 className="-mt-3 font-bold text-secondary">Eventuppgifter</h1>
              <div className="border p-3 rounded-lg mt-1  h-full">
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Eventnamn</h1>
                      <h1 className="text-secondary">{customerData?.name}</h1>
                    </div>
                  </div>
                  <div className="grid grid-cols-2 gap-4">
                    <div className="p-4">
                      <div className="">
                        <h1 className="font-semibold text-secondary">Pris</h1>
                        <h1 className="text-secondary">{customerData?.price}</h1>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Startdatum</h1>
                      <h1 className="text-secondary">{customerData?.startDate}</h1>
                    </div>
                  </div>
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Slutdatum</h1>
                      <h1 className="text-secondary">{customerData?.endDate}</h1>
                    </div>
                  </div>
                </div>
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Stad</h1>
                      <h1 className="text-secondary">{customerData?.city}</h1>
                    </div>
                  </div>
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Postkod</h1>
                      <h1 className="text-secondary">{customerData?.pincode}</h1>
                    </div>
                  </div>
                </div>
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="h-16 overflow-y-auto whitespace-normal">
                      <h1 className="font-semibold text-secondary">Beskrivning</h1>
                      <h1 className="text-secondary">{customerData?.description}</h1>
                    </div>
                  </div>
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Moms Percentage</h1>
                      <h1 className="text-secondary">{customerData?.taxType?.taxPercentage}</h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    );
  };

  const filterDatas = (filterData: any) => {
    const value = rowData?.filter(
      (val: any) =>
        val?.name?.toLowerCase()?.includes(filterData?.toLowerCase()) ||
        val?.price
          ?.toString()
          ?.toLowerCase()
          ?.includes(filterData?.toLowerCase()) ||
        val?.updatedAt?.toLowerCase()?.includes(filterData?.toLowerCase())
    );
    return value;
  };

  const DeleteEventModal = () => {
    const handleclose = () => {
      setIsDeleteEventModalOpen(false);
      setCustomerData([]);
      seteventData([]);
    };

    const deleteEvent = async () => {
      setIsLoading(true);
      try {
        const response = await commonAxios.delete(
          `/cemo/events/${eventData.id}`
        );
        if (response.data) {
          if (userInfo.role === "ROLE_ADMIN") {
            getEventsList();
          } else {
            getEventsListBySupplier();
          }
          successNotify(`${eventData.name} Eventet har ändrats`);
          handleclose();
        }
      } catch (error) {
        failureNotify("oops! something went wrong");
      } finally {
        setIsLoading(false);
      }
    };

    return (
      <div className="w-[85%] h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <div className="bg-white rounded-lg flex flex-col justify-center items-center md:w-[450px] w-full p-4 md:h-56 h-46 relative">
            <button onClick={handleclose}>
              <img
                src={deleteIcon}
                className="absolute top-5 right-5 h-5 w-5"
              />
            </button>
            <p className="font-semibold text-lg text-secondary">
              Är du säker att du vill ta bort eventet? "{eventData.name}"
            </p>
            <section className="flex flex-row justify-between items-center w-[60%] mr-30 mt-10">
              <button
                onClick={deleteEvent}
                className="px-6 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer mr-6 uppercase"
              >
                Ta bort
              </button>
              <button
                onClick={handleclose}
                className="px-6 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer uppercase"
              >
                Avbryt
              </button>
            </section>
          </div>
        </section>
      </div>
    );
  };

  return (
    <div className="flex">
      <SideNav />

      {isModalOpen ? (
        <AddEventsModal
          getEventsList={getEventsList}
          eventData={eventData}
          seteventData={seteventData}
          addEventModalHeader={addEventModalHeader}
          getEventsListBySupplier={getEventsListBySupplier}
          isLoading={isLoading}
          setIsModalOpen={setIsModalOpen}
          options={options}
          choose={choose}
        />
      ) : null}
      {isAddProductModalOpen ? (
        <AddProducts
          setIsAddProductModalOpen={setIsAddProductModalOpen}
          setIsLoading={setIsLoading}
          seteventData={seteventData}
          setCustomerData={setCustomerData}
          isLoading={isLoading}
        />
      ) : null}
      {isActionModalOpen ? <CustomerDetails /> : null}
      {isDeleteEventModalOpen ? <DeleteEventModal /> : null}
      {isLoading && <Spinner />}
      <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
        <AdminHeader />
        <div className="mx-0 my-6">
          <div className="flex flex-row items-center justify-between">
            <h1 className=" text-secondary text-[24px] font-semibold p-4">
              Event
            </h1>
            <section>
              <button
                onClick={() => {
                  setIsModalOpen(true);
                  setAddEventModalHeader("Skapa nytt event");
                }}
                className="px-6 p-2 md:mr-4 bg-[#D6B27D] text-white rounded-xl text-lg font-semibold cursor-pointer outline-none w-auto uppercase"
              >
                Lägg till event
              </button>
            </section>
          </div>
          <div className=" border border-[#FFFFFF] bg-[#ffff] rounded-[20px] p-4 mt-2">
            <div className="relative w-full">
              <input
                type="text"
                className="w-full h-[38px] pl-10 pr-5 border rounded-md border-[#E2E2EA] focus:outline outline-[#E2E2EA] focus:border-transparent"
                placeholder="Sök efter namn, leverantör, mobil etc..."
                onChange={(e) => setFilterData(e.target.value)}
                value={filterData}
              />
              <div className="absolute inset-y-0 left-0 flex items-center pl-3">
                <img src={searchIcon} />
              </div>
            </div>
            <div className="mt-6">
              {rowData.length >= 1 ?

                <GridTable
                  columnDefs={columnDefs}
                  gridOptions={{
                    ...gridOptions,
                    getRowHeight: () => rowHeight,
                  }}
                  rowData={filterDatas(filterData)}
                  onGridReady={onGridReady}
                /> : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EventsList;
