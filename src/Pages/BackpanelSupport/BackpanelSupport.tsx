import React, { useState } from 'react'
import AdminHeader from '../../Components/AdminHeader/AdminHeader'
import Loader from '../../Components/Loader/Loader'
import SideNav from '../../Components/SideNav/SideNav'
import ContactAdmin from './ContactAdmin'
import { useSelector } from 'react-redux'
import { RootState } from '../../Redux/store'
import ContactSupplier from './ContactSupplier'

const BackpanelSupport = () => {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const userDetails = useSelector((state: RootState) => state.user.userDetails);
    return (
        <div className="flex">
            {/* {isLoading ? <Loader /> : null} */}
            <SideNav />
            <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
                <AdminHeader />
                {
                    userDetails.role === 'ROLE_ADMIN' ? <ContactAdmin setIsLoading={setIsLoading} /> : <ContactSupplier setIsLoading={setIsLoading} />
                }
            </div>
        </div>
    )
}

export default BackpanelSupport