import moment from "moment";
import { useEffect, useRef, useState } from "react";
import { commonAxios } from "../../Axios/service";
import { failureNotify } from "../../Components/Toast/Toast";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { setNotification } from "../../Redux/features/notificationSlice";
import { useLocation } from "react-router-dom";

const ContactSupplier = ({ setIsLoading }: any) => {
  const [messages, setMessages] = useState<any[]>([]);
  const [newMessage, setNewMessage] = useState("");
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const dispatch = useDispatch();
  const handlenewcomment = (e: any) => {
    setNewMessage(e.target.value);
  };
  const messageContainerRef = useRef<any>(null);


  const addComment = async () => {
    try {
      const payload = {
        message: newMessage,
        customerId: null,
        supplierId: userInfo.id,
        adminId: "admin",
        isCustomerMessage: false,
        isSupplierMessage: true,
        isAdminMessage: false,
      };
      const response = await commonAxios.post("/cemo/messages", payload);
      if (response.data) {
        getMessages();
        setNewMessage("");
      }
    } catch (error) {
      failureNotify("oops! something went wrong");
    }
  };

  const sortAndSetMessages = async (messagesArray: any[]) => {
    let unreadMessages = messagesArray.filter((readMessage: any) => {
      return readMessage.isSupplierRead === false;
    });

    let unreadtIds = unreadMessages.map((val: any) => {
      return val.id;
    });
    console.log(unreadtIds, "ids");
    let payload = {
      id: unreadtIds,
      customerId: null,
      supplierId: userInfo.id,
      adminId: null,
    };

    let updatedMessageResponse = await commonAxios.put(
      `/cemo/messages/update/status`,
      payload
    );

    if (updatedMessageResponse?.data) {
      if (userInfo.role === "ROLE_SUPPLIER") {
        try {
          const messageCount = await commonAxios.get(
            `cemo/appuser/notification/count?supplierId=${userInfo.id}`
          );
          if (messageCount.data) {
            dispatch(setNotification(messageCount.data));
          }
        } catch (e) { }
      }
    }
    messagesArray.sort((a, b) => {
      const dateA = moment(a.updatedDate, "YYYY-MM-DD HH:mm:ss");
      const dateB = moment(b.updatedDate, "YYYY-MM-DD HH:mm:ss");
      return dateA.diff(dateB);
    });
    setMessages(messagesArray);
    setIsLoading(false);
  };

  const getMessages = async () => {
    try {
      const response = await commonAxios.get(
        `/cemo/messages/supplier?supplierId=${userInfo.id}&contactAdmin=true`
      );
      if (response.data) {
        sortAndSetMessages(response.data);
      }
    } catch (error) {
    } finally {
    }
  };

  useEffect(() => {
    setIsLoading(true);
    getMessages();
  }, []);

  useEffect(() => {
    // Scroll to the bottom when messages change
    if (messageContainerRef.current) {
      messageContainerRef.current.scrollTop = messageContainerRef.current.scrollHeight;
    }
  }, [messages]);

  return (
    <div className="mx-10 my-6">
      <div className="bg-[#fff] p-4 m-6 h-[85vh] max-h-[90vh] flex flex-row">
        <section className="w-1/3 py-4 px-10 h-full border-[#e3e3e3] border rounded-[10px] overflow-y-scroll">
          <h2 className="font-bold font-[sofiapro] my-6 text-2xl text-secondary">
            Admin
          </h2>
        </section>
        <section className="w-2/3 h-full">
          <div className="w-4/5 h-[80%] overflow-y-scroll flex flex-col mx-auto border border-[#e3e3e3] rounded-[10px]" ref={messageContainerRef}>
            {messages.length > 0 ? (
              messages.map((message: any) =>
                !message.isSupplierMessage ? (
                  <div className="flex flex-row items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                    <section className="w-3/4">
                      <div className="flex justify-start">
                        {message.admin?.profileImage ? (
                          <img
                            className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                            src={message.admin?.profileImage}
                          ></img>
                        ) : (
                          //   <span className="h-9 w-9 p-2 rounded-full flex justify-center items-center font-semibold bg-[#D6B27D] text-white mr-2 mb-2">
                          <div className="h-[36px] w-[36px] my-auto  rounded-full bg-[#D6B37D]">
                            <div className="flex justify-center items-center mt-2  text-[#ffffff] font-bold"></div>
                            <span className="ml-2 text-white font-bold">
                              {message.admin?.firstName
                                .split("")
                                .at(0)
                                ?.toUpperCase() +
                                "" +
                                message.admin?.lastName
                                  .split("")
                                  .at(0)
                                  ?.toUpperCase()}
                            </span>
                          </div>
                        )}
                        <p className="text-[#D6B27D] mx-2 font-medium mt-1.5">
                          {message?.admin?.firstName} {message?.admin?.lastName}
                        </p>
                      </div>
                      <h4 className="text-sm text-left text-secondary">
                        {message?.message}
                      </h4>
                    </section>
                    <span className="w-1/4 text-sm text-secondary">
                      {moment(message?.createdDate).format("ddd, MMM DD YYYY")}
                    </span>
                  </div>
                ) : (
                  <div className="flex flex-row justify-between items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                    <span className="text-sm text-right text-secondary">
                      {moment(message?.createdDate).format("ddd, MMM DD YYYY")}
                    </span>
                    <section className="w-3/4">
                      <div className="flex flex-row justify-end">
                        {message.supplier?.profileImage ? (
                          <img
                            className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                            src={message.supplier?.profileImage}
                          ></img>
                        ) : (
                          <div className="h-[36px] w-[36px] my-auto  rounded-full bg-[#D6B37D]">
                            <div className="flex justify-center items-center mt-2  text-[#ffffff] font-bold"></div>
                            <span className="ml-2 text-white font-bold">
                              {message.supplier?.firstName
                                .split("")
                                .at(0)
                                ?.toUpperCase() +
                                "" +
                                message.supplier?.lastName
                                  .split("")
                                  .at(0)
                                  ?.toUpperCase()}
                            </span>
                          </div>
                        )}
                        <p className="text-[#D6B27D] font-medium mt-1.5">
                          {message?.supplier?.firstName}{" "}
                          {message?.supplier?.lastName}
                        </p>
                      </div>
                      <h4 className="text-sm text-right text-secondary">
                        {message?.message}
                      </h4>
                    </section>
                  </div>
                )
              )
            ) : (
              <div className="justify-center py-4 px-44 text-[#D6B37D]">
                Inga meddelanden
              </div>
            )}
          </div>
          <div className="w-4/5 mx-auto mt-2">
            <textarea
              placeholder="Dina kommentarer..."
              value={newMessage}
              onChange={handlenewcomment}
              className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md"
              name="comment"
              id="comment"
              rows={3}
            />
          </div>
          <div className="w-4/5 mx-auto">
            <div className="flex justify-center">
              <button
                onClick={addComment}
                className="bg-[#D6B27D] outline-none w-auto uppercase cursor-pointer text-white mb-3 font-semibold rounded-md px-6 text-lg py-2"
              >
                SKICKA
              </button>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default ContactSupplier;
