import moment from "moment";
import React, { useContext, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import AdminHeader from "../../Components/AdminHeader/AdminHeader";
import SideNav from "../../Components/SideNav/SideNav";
import { RootState } from "../../Redux/store";
import Select from "react-select";
import { RxCross1 } from "react-icons/rx";
import { commonAxios } from "../../Axios/service";
import { failureNotify } from "../../Components/Toast/Toast";
import { useLocation, useNavigate } from 'react-router-dom';
import { AuthContext } from "../RootLayout/RootLayout";

const ContactCustomer = () => {
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [messages, setMessages] = useState<any>([]);
  const [newMessage, setNewMessage] = useState("");
  const [selectedCustomer, setSelectedCustomer] = useState<any>();
  const [customerData, setCustomerData] = useState();

  const messageContainerRef = useRef<any>(null);
  const { setNotifyReRender, notifyRender } = useContext(AuthContext);
  let location = useLocation();
  useEffect(() => {
    if (location.state) {
      const defaultValue = {
        label: location?.state?.key?.customer?.firstName,
        value: location?.state?.key?.customer?.id
      }
      setSelectedCustomer(defaultValue);
      getMessages(defaultValue.value)
    }
  }, [location]);


  useEffect(() => {
    // Scroll to the bottom when messages change
    if (messageContainerRef.current) {
      messageContainerRef.current.scrollTop = messageContainerRef.current.scrollHeight;
    }
  }, [messages]);

  const existMessageRead = async (messages: any) => {
    const updateVal = {
      id: [messages[0]?.id],
      customerId: messages?.customer?.id,
      supplierId: messages.data?.supplier.id,
      admin: "admin",
    };
    const responseUpdate = await commonAxios.put(
      "/cemo/messages/update/status",
      updateVal
    );
    setNotifyReRender(!notifyRender)
  };


  const getMessages = async (customerId: string) => {
    try {
      if (userInfo.role === "ROLE_ADMIN") {
        const response = await commonAxios.get(
          `/cemo/messages/users?customerId=${customerId}&adminId=${userInfo.id}`
        );
        setMessages(response.data);
        existMessageRead(response.data)
      } else {
        const response = await commonAxios.get(
          `/cemo/messages/users?customerId=${customerId}&supplierId=${userInfo.id}`
        );
        setMessages(response.data);
        existMessageRead(response.data)
      }
    } catch (error) { }
  }

  useEffect(() => {
    getCustomers();
  }, []);

  const getCustomers = async () => {
    try {
      const response = await commonAxios.get(`/cemo/appuser/all/customers/supplier?appUser=${userInfo.id}`);
      const activeCustomers = response.data.filter(
        (supplier: any) => supplier.accountStatus === "ACTIVE"
      );
      console.log("activeCustomers", activeCustomers)
      if (activeCustomers.length > 0) {
        setCustomerData(
          activeCustomers.map((supplier: any) => ({
            label: `${supplier.firstName} ${supplier.lastName} (${supplier.businessCity})`,
            value: supplier.id,
          }))
        );
      }
    } catch (error) {
      // Handle the error
    }
  };

  const addComment = async () => {
    try {
      if (userInfo.role === "ROLE_SUPPLIER") {
        const payload = {
          message: newMessage,
          customerId: selectedCustomer?.value,
          supplierId: userInfo.id,
          adminId: null,
          isCustomerMessage: false,
          isSupplierMessage: true,
          isAdminMessage: false,
        };
        const response = await commonAxios.post("/cemo/messages", payload);
        if (response.data) {
          setNewMessage("");
          getMessages(selectedCustomer?.value);
        }
      } else {
        const payload = {
          message: newMessage,
          customerId: selectedCustomer?.value,
          supplierId: null,
          adminId: "admin",
          isCustomerMessage: false,
          isSupplierMessage: false,
          isAdminMessage: true,
        };
        const response = await commonAxios.post("/cemo/messages", payload);
        if (response.data) {
          setNewMessage("");
          getMessages(selectedCustomer?.value);
        }
      }
    } catch (error) {
      failureNotify("oops! something went wrong");
    }
  };

  const handleCustomerChange = (selectedOption: any) => {
    setSelectedCustomer(selectedOption);
    if (selectedOption) {
      const selectedCustomer = selectedOption.value;
      getMessages(selectedCustomer);
    }
  };

  const handlenewcomment = (e: any) => {
    setNewMessage(e.target.value);
  };
  const CustomDropdownIndicator = () => (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        style={{ width: "24px", height: "24px", fill: '#A09792' }}
      >
        <path fill="none" d="M0 0h24v24H0z" />
        <path d="M7 10l5 5 5-5z" />
      </svg>
    </div>
  );

  const customComponents: any = {
    DropdownIndicator: CustomDropdownIndicator,
  };

  const customStyles = {
    control: (provided: any, state: any) => ({
      ...provided,
      border: "none",
      outline: "none",
      fontSize: "17px",
      width: "300px",
      boxShadow: "none",
    }),
    option: (provided: any, state: any) => ({
      ...provided,
      backgroundColor: "transparent",
      fontSize: "17px",
      width: "300px",
      boxShadow: "none",
      color: state.isSelected ? "#D6B27D" : "gray",
    }),
    singleValue: (provided: any) => ({
      ...provided,
      color: "gray", // Set the initial text color to gray
    }),
    valueContainer: (provided: any) => ({
      ...provided,
      color: "gray", // Set the initial text color to gray
    }),
  };

  const handleClick = () => {
    setSelectedCustomer("");
  };

  return (
    <div className="flex">
      <SideNav />
      <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
        <AdminHeader />
        <div className="mx-10 my-6">
          <div className="bg-[#fff] p-4 m-6 h-[85vh] max-h-[90vh] flex flex-row">
            <section className="w-1/3 py-4 px-10 h-full border-[#e3e3e3] border rounded-[10px] overflow-y-scroll">
              <h2 className="font-bold font-[sofiapro] my-6 text-2xl text-secondary">
                Kund
              </h2>
              <div className="flex items-center -mt-[7px] mr-5 border border-1 border-slate-300 h-10 bg-[white] rounded-lg">
                <Select
                  components={customComponents}
                  value={selectedCustomer}
                  onChange={handleCustomerChange}
                  options={customerData || []}
                  placeholder={"Choose Kund"}
                  className={""}
                  styles={customStyles}
                />
                <RxCross1
                  className="mx-2 cursor-pointer"
                  onClick={handleClick}
                  style={{ color: '#A09792' }}
                />
              </div>
            </section>
            <section className="w-2/3 h-full">
              <div className="w-4/5 h-[80%] overflow-y-scroll flex flex-col mx-auto border border-[#e3e3e3] rounded-[10px]" ref={messageContainerRef}>
                {messages.length > 0 ? (
                  messages.map((message: any) =>
                    message.isCustomerMessage ? (
                      <div className="flex flex-row items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                        <section className="w-3/4">
                          <div className="flex justify-start">
                            {message.customer?.profileImage ? (
                              <img
                                className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                src={message.customer?.profileImage}
                              ></img>
                            ) : (
                              <div className="h-[36px] w-[36px] my-auto  rounded-full bg-[#D6B37D]">
                                <div className="flex justify-center items-center mt-2  text-[#ffffff] font-bold"></div>
                                <span className="ml-2 text-white font-bold">
                                  {message.customer?.firstName
                                    .split("")
                                    .at(0)
                                    ?.toUpperCase() +
                                    "" +
                                    message.customer?.lastName
                                      .split("")
                                      .at(0)
                                      ?.toUpperCase()}
                                </span>
                              </div>
                            )}
                            <p className="text-[#D6B27D] font-medium mt-1.5 mx-2">
                              {message?.customer?.firstName}{" "}
                              {message?.customer?.lastName}
                            </p>
                          </div>
                          <h4 className="text-sm text-left text-secondary">
                            {message?.message}
                          </h4>
                        </section>
                        <span className="w-1/4 text-sm text-secondary">
                          {moment(message?.createdDate).format(
                            "ddd, MMM DD YYYY"
                          )}
                        </span>
                      </div>
                    ) : (
                      <div className="flex flex-row justify-between items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                        <span className="text-sm text-right text-secondary">
                          {moment(message?.createdDate).format(
                            "ddd, MMM DD YYYY"
                          )}
                        </span>
                        <section className="w-3/4">
                          <div className="flex flex-row justify-end">
                            <>
                              {userInfo.role === "ROLE_SUPPLIER" ? (
                                <>
                                  <>
                                    {message.supplier?.profileImage ? (
                                      <>
                                        <img
                                          className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                          src={message.supplier?.profileImage}
                                        ></img>
                                      </>
                                    ) : (
                                      <div className="h-[36px] w-[36px] my-auto  rounded-full bg-[#D6B37D]">
                                        <div className="flex justify-center items-center mt-2  text-[#ffffff] font-bold"></div>
                                        <span className="ml-2 text-white font-bold">
                                          {message.supplier?.firstName
                                            .split("")
                                            .at(0)
                                            ?.toUpperCase() +
                                            "" +
                                            message.supplier?.lastName
                                              .split("")
                                              .at(0)
                                              ?.toUpperCase()}
                                        </span>
                                      </div>
                                    )}
                                  </>
                                </>
                              ) : (
                                <>
                                  {message.admin?.profileImage ? (
                                    <>
                                      <img
                                        className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                        src={message.admin?.profileImage}
                                      ></img>
                                    </>
                                  ) : (
                                    <div className="h-[36px] w-[36px] my-auto  rounded-full bg-[#D6B37D]">
                                      <div className="flex justify-center items-center mt-2  text-[#ffffff] font-bold"></div>
                                      <span className="ml-2 text-white font-bold">
                                        {message.admin?.firstName
                                          .split("")
                                          .at(0)
                                          ?.toUpperCase() +
                                          "" +
                                          message.admin?.lastName
                                            .split("")
                                            .at(0)
                                            ?.toUpperCase()}
                                      </span>
                                    </div>
                                  )}
                                </>
                              )}
                            </>

                            {userInfo.role === "ROLE_SUPPLIER" ? (
                              <p className="text-[#D6B27D] font-medium mt-1.5">
                                {message?.admin?.firstName}{" "}
                                {message?.admin?.lastName}
                              </p>
                            ) : (
                              <p className="text-[#D6B27D] font-medium mt-1.5">
                                {message?.supplier?.firstName}{" "}
                                {message?.supplier?.lastName}
                              </p>
                            )}
                          </div>
                          <h4 className="text-sm text-right text-secondary">
                            {message?.message}
                          </h4>
                        </section>
                      </div>
                    )
                  )
                ) : (
                  <div className="justify-center py-4 px-44 text-[#D6B37D]">
                    Inga meddelanden
                  </div>
                )}
              </div>
              <div className="w-4/5 mx-auto mt-2">
                <textarea
                  placeholder="Dina kommentarer..."
                  value={newMessage}
                  onChange={handlenewcomment}
                  className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md"
                  name="comment"
                  id="comment"
                  rows={3}
                />
              </div>
              <div className="w-4/5 mx-auto">
                <div className="flex justify-center">
                  <button
                    onClick={addComment}
                    className="bg-[#D6B27D] outline-none w-auto uppercase cursor-pointer text-white mb-3 font-semibold rounded-md px-6 text-lg py-2"
                  >
                    SKICKA
                  </button>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactCustomer;
