import moment from "moment";
import { commonAxios } from "../../Axios/service";
import { useContext, useEffect, useRef, useState } from "react";
import { failureNotify } from "../../Components/Toast/Toast";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { setNotification } from "../../Redux/features/notificationSlice";
import Select from "react-select";
import { RxCross1 } from "react-icons/rx";
import { useLocation } from "react-router-dom";
import { AuthContext } from "../RootLayout/RootLayout";

export interface Message {
  id: string;
  booking: any;
  customer: any;
  supplier: Supplier;
  admin: Admin;
  message: string;
  isCustomerRead: boolean;
  isSupplierRead: boolean;
  isAdminRead: boolean;
  createdDate: string;
  updatedDate: string;
  isCustomerMessage: boolean;
  isSupplierMessage?: boolean;
}

export interface Supplier {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
  profileImage: string;
  resetToken: any;
}

export interface Admin {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
  profileImage: string;
  resetToken: string;
}

const ContactAdmin = ({ setIsLoading }: any) => {
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [messages, setMessages] = useState<Message[]>([]);
  const [messagedSuppliers, setMessagedSuppliers] = useState<any>([]);
  const [activeSupplier, setActiveSupplier] = useState<string>("");
  const [newMessage, setNewMessage] = useState("");
  const [selectedCustomer, setSelectedCustomer] = useState<any>();
  const [selectedSupplier, setSelectedSupplier] = useState<any>(null);
  const [supplierData, setSupplierData] = useState<any>(null);
  const { setNotifyReRender, notifyRender } = useContext(AuthContext);
  const messageContainerRef = useRef<any>(null);

  const dispatch = useDispatch();
  const handlenewcomment = (e: any) => {
    setNewMessage(e.target.value);
  };

  let location = useLocation();

  useEffect(() => {
    if (location.state) {
      const defaultValue = {
        label: location?.state?.key?.supplier?.businessName,
        value: location?.state?.key?.supplier?.id
      }
      setSelectedSupplier(defaultValue);
      getMessages(defaultValue.value)
    }
  }, []);

  const addComment = async () => {
    try {
      const payload = {
        message: newMessage,
        customerId: null,
        supplierId: selectedSupplier?.value,
        adminId: "admin",
        isCustomerMessage: false,
        isSupplierMessage: false,
        isAdminMessage: true,
      };
      const response = await commonAxios.post("/cemo/messages", payload);
      if (response.data) {
        setNewMessage("");
        getMessages(selectedSupplier?.value);
        setActiveSupplier(activeSupplier);
      }
    } catch (error) {
      failureNotify("oops! something went wrong");
    }
  };

  const sortAndSetMessages = (
    messagesArray: Message[],
    currSupplier: string
  ) => {
    messagesArray.sort((a, b) => {
      const dateA = moment(a.updatedDate, "YYYY-MM-DD HH:mm:ss");
      const dateB = moment(b.updatedDate, "YYYY-MM-DD HH:mm:ss");
      return dateA.diff(dateB);
    });
    setMessages(
      messagesArray.filter(
        (message: Message) => message.supplier.id === currSupplier
      )
    );
    setIsLoading(false);
  };

  const existMessageRead = async (messages: any) => {
    const updateVal = {
      id: [messages[0]?.id],
      customerId: messages?.customer?.id,
      supplierId: messages.data?.supplier.id,
      admin: "admin",
    };
    console.log(updateVal, "updateVal");
    const responseUpdate = await commonAxios.put(
      "/cemo/messages/update/status",
      updateVal
    );
    setNotifyReRender(!notifyRender)
  };


  useEffect(() => {
    setIsLoading(true);
  }, [activeSupplier]);

  const getMessages = async (supplierId: string) => {
    try {
      const response = await commonAxios.get(
        `/cemo/messages/users?supplierId=${supplierId}&adminId=${userInfo.id}`
      );
      existMessageRead(response.data)
      setMessages(response.data);
    } catch (error) { }
  };

  const getSuppliers = async () => {
    try {
      const response = await commonAxios.get(`/cemo/appuser/all/suppliers`);
      const activeSuppliers = response.data.filter(
        (supplier: any) => supplier.accountStatus === "ACTIVE"
      );
      if (activeSuppliers.length > 0) {
        setSupplierData(
          activeSuppliers.map((supplier: any) => ({
            label: supplier.businessName,
            value: supplier.id,
          }))
        );
      }
    } catch (error) {
      // Handle the errorgetMessages
    }
  };

  const CustomDropdownIndicator = () => (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        style={{ width: "24px", height: "24px", fill: '#A09792' }}
      >
        <path fill="none" d="M0 0h24v24H0z" />
        <path d="M7 10l5 5 5-5z" />
      </svg>
    </div>
  );

  const customComponents: any = {
    DropdownIndicator: CustomDropdownIndicator,
  };

  useEffect(() => {
    getSuppliers();
  }, []);

  const handleSupplierChange = (selectedOption: any) => {
    setSelectedSupplier(selectedOption);
    if (selectedOption) {
      const selectedSupplierId = selectedOption.value;
      getMessages(selectedSupplierId);
    }
  };

  const handleClick = () => {
    setSelectedSupplier("");
  };

  const customStyles = {
    control: (provided: any, state: any) => ({
      ...provided,
      border: "none",
      outline: "none",
      fontSize: "17px",
      width: "220px",
      boxShadow: "none",
    }),
    option: (provided: any, state: any) => ({
      ...provided,
      backgroundColor: "transparent",
      fontSize: "17px",
      width: "300px",
      boxShadow: "none",
      color: state.isSelected ? "#D6B27D" : "gray",
    }),
    singleValue: (provided: any) => ({
      ...provided,
      color: "gray", // Set the initial text color to gray
    }),
    valueContainer: (provided: any) => ({
      ...provided,
      color: "gray", // Set the initial text color to gray
    }),
  };

  useEffect(() => {
    // Scroll to the bottom when messages change
    if (messageContainerRef.current) {
      messageContainerRef.current.scrollTop = messageContainerRef.current.scrollHeight;
    }
  }, [messages]);

  return (
    <>
      <div className="mx-10 my-6">
        <div className="bg-[#fff] p-4 m-6 h-[85vh] max-h-[90vh] flex flex-row">
          <section className="w-1/3 py-4 px-10 h-full border-[#e3e3e3] border rounded-[10px] overflow-y-scroll">
            <h2 className="font-bold font-[sofiapro] my-6 text-2xl text-secondary">
              Suppliers
            </h2>
            <div className="flex items-center -mt-[7px] mr-5 border border-1 border-slate-300 h-10 bg-[white] rounded-lg">
              <Select
                components={customComponents}
                value={selectedSupplier}
                onChange={handleSupplierChange}
                options={supplierData || []}
                placeholder={"Välj leverantörer"}
                className={""}
                styles={customStyles}
              />
              <RxCross1 className="mx-2 cursor-pointer"
                onClick={handleClick} style={{ color: '#A09792' }}
              />
            </div>
          </section>
          <section className="w-2/3 h-full">
            <div className="w-4/5 h-[80%] overflow-y-scroll flex flex-col mx-auto border border-[#e3e3e3] rounded-[10px]" ref={messageContainerRef}>
              {messages.length > 0 ? (
                messages.map((message: Message) =>
                  message.isSupplierMessage ? (
                    <div className="flex flex-row items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                      <section className="w-3/4">
                        <div className="flex justify-start">
                          {message.supplier?.profileImage ? (
                            <img
                              className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                              src={message.supplier?.profileImage}
                            ></img>
                          ) : (
                            <div className="h-[36px] w-[36px] my-auto  rounded-full bg-[#D6B37D]">
                              <div className="flex justify-center items-center mt-2  text-[#ffffff] font-bold"></div>
                              <span className="ml-2 text-white font-bold">
                                {message.supplier?.firstName
                                  .split("")
                                  .at(0)
                                  ?.toUpperCase() +
                                  "" +
                                  message.supplier?.lastName
                                    .split("")
                                    .at(0)
                                    ?.toUpperCase()}
                              </span>
                            </div>
                          )}
                          <p className="text-[#D6B27D] font-medium mt-1.5">
                            {message?.supplier?.businessName}{" "}
                            {/* {message?.supplier?.lastName} */}
                          </p>
                        </div>
                        <h4 className="text-sm text-left text-secondary">
                          {message?.message}
                        </h4>
                      </section>
                      <span className="w-1/4 text-sm text-secondary">
                        {moment(message?.createdDate).format(
                          "ddd, MMM DD YYYY"
                        )}
                      </span>
                    </div>
                  ) : (
                    <div className="flex flex-row justify-between items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                      <span className="text-sm text-right text-secondary">
                        {moment(message?.createdDate).format(
                          "ddd, MMM DD YYYY"
                        )}
                      </span>
                      <section className="w-3/4">
                        <div className="flex flex-row justify-end">
                          {message.admin?.profileImage ? (
                            <img
                              className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                              src={message.admin?.profileImage}
                            ></img>
                          ) : (
                            <div className="h-[36px] w-[36px] my-auto  rounded-full bg-[#D6B37D]">
                              <div className="flex justify-center items-center mt-2  text-[#ffffff] font-bold"></div>
                              <span className="ml-2 text-white font-bold">
                                {message.admin?.firstName
                                  .split("")
                                  .at(0)
                                  ?.toUpperCase() +
                                  "" +
                                  message.admin?.lastName
                                    .split("")
                                    .at(0)
                                    ?.toUpperCase()}
                              </span>
                            </div>
                          )}
                          <p className="text-[#D6B27D] font-medium mt-1.5">
                            {message?.admin?.firstName}{" "}
                            {message?.admin?.lastName}
                          </p>
                        </div>
                        <h4 className="text-sm text-right text-secondary">
                          {message?.message}
                        </h4>
                      </section>
                    </div>
                  )
                )
              ) : (
                <div className="justify-center py-4 px-44 text-[#D6B37D]">
                  Inga meddelanden
                </div>
              )}
            </div>
            <div className="w-4/5 mx-auto mt-2">
              <textarea
                placeholder="Dina kommentarer..."
                value={newMessage}
                onChange={handlenewcomment}
                className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md"
                name="comment"
                id="comment"
                rows={3}
              />
            </div>
            <div className="w-4/5 mx-auto">
              <div className="flex justify-center">
                <button
                  onClick={addComment}
                  className="bg-[#D6B27D] outline-none w-auto uppercase cursor-pointer text-white mb-3 font-semibold rounded-md px-6 text-lg py-2"
                >
                  SKICKA
                </button>
              </div>
            </div>
          </section>
        </div>
      </div>
    </>
  );
};

export default ContactAdmin;
