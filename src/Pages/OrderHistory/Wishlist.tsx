import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { commonAxios } from "../../Axios/service";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import { HiChevronLeft, HiChevronRight, HiHeart } from "react-icons/hi";
import { useNavigate } from "react-router-dom";
import { AiFillHeart } from "react-icons/ai";
import { Button, Tooltip } from "@material-tailwind/react";
import { MdOutlineIosShare } from "react-icons/md";
import share from "../../assets/share.png";
import { getWishlistCount } from "../../Redux/features/wishlistSlice";
import MinLoader from "../../Components/MinLoader/MinLoader";
export interface Wishlist {
  id: string;
  appUser: AppUser;
  product: Product;
  event: Event;
}

export interface AppUser {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
  profileImage: any;
}

export interface Product {
  id: string;
  name: string;
  description: string;
  image: string;
  quantity: number;
  price: number;
  status: string;
  productNumber: string;
  appUser: AppUser2;
  category: Category;
  taxType: TaxType;
}

export interface AppUser2 {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
  profileImage: string;
}

export interface Category {
  id: string;
  categoryName: string;
  categoryPrice: number;
  image: string;
  createdAt: string;
  status: string;
}

export interface TaxType {
  id: string;
  taxPercentage: number;
}

const Wishlist = ({ setIsLoading }: any) => {
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [wishList, setWishList] = useState<any>([]);
  const [active, setActive] = useState(1);
  const [initialPageData, setInitialPageData] = useState<any>(null);
  const [isMinLoader, setMinLoader] = useState(false);
  const perPage = 10
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const getWishlist = async (startIndex: any, endIndex: any) => {
    try {
      setMinLoader(true);
      const response = await commonAxios.get(
        `/cemo/favourite-items/${userInfo.id}`
      );

      setWishList(response.data);
      setInitialPageData(response.data.slice(startIndex, endIndex))
      if (startIndex === response.data.length && startIndex !== 0) {
        setActive((prevCount) => prevCount - 1)
        setInitialPageData(response.data.slice(startIndex - 10, endIndex))
      }
    } catch (error) {
    } finally {
      setMinLoader(false);
    }
  };

  const togglefavourite = async (details: any, productId: string) => {
    const startIndex = (active - 1) * perPage;
    const endIndex = startIndex + perPage;
    if (details.product !== null) {
      try {
        // setIsLoading(true);
        const payload = {
          appUserId: userInfo.id,
          productId: productId,
        };
        await commonAxios.post("/cemo/favourite-items", payload).then(
          (response) => {
            successNotify("favourite updated successfully");
          },
          (error) => {
            failureNotify("favourite Update failed.");
          }
        );
      } catch (error) {
      } finally {
        // setIsLoading(false);
        getWishlist(startIndex, endIndex);
        dispatch(getWishlistCount(userInfo.id));
      }
    } else {
      setMinLoader(true);
      try {
        const payload = {
          appUserId: userInfo.id,
          eventId: productId,
        };
        await commonAxios.post("/cemo/favourite-items", payload).then(
          (response) => {
            successNotify("favourite updated successfully");
          },
          (error) => {
            failureNotify("favourite Update failed.");
          }
        );
      } catch (error) {
      } finally {
        // setIsLoading(false);
        getWishlist(startIndex, endIndex);
        dispatch(getWishlistCount(userInfo.id));
      }
    }
  };

  const getDescription = (wish: any) => {
    if (wish.product !== null) {
      const shortDescription = wish.product?.description?.substring(
        0,
        wish.product?.description?.indexOf(".")
      );
      if (shortDescription === "") {
        return wish.product?.description;
      } else {
        return shortDescription;
      }
    } else {
      const shortDescription = wish.event?.description?.substring(
        0,
        wish.event?.description?.indexOf(".")
      );
      if (shortDescription === "") {
        return wish.event?.description;
      } else {
        return shortDescription;
      }
    }
  };
  useEffect(() => {
    getWishlist(0, perPage);
  }, []);

  const orderByDate = (wishList: any) => {
    console.log(wishList, 'favouriteitems')
    let value = wishList.sort((a: any, b: any) => {
      return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime();
    });
    return value;
  };

  const handleFavClick = (details: any, id: any) => {
    if (details.product === null) {
      navigate(`/booking?id=${details.event?.id}`);
    } else {
      navigate(`/cart?id=${details.product.appUser.id}&productId=${details.product?.id}`);
    }
  }

  const handleShareClick: any = async (orderDetails: any) => {
    if (orderDetails.event === null) {
      try {
        let text = `${window.location.href.split('/')[2]}` + '/cart?id=' + orderDetails.product?.appUser?.id + '&productId=' + orderDetails.product?.id
        await navigator.clipboard.writeText(text);
        successNotify('Link Copied')
      } catch (error) {
        failureNotify('the Link is Not Copied')
      }
    } else {
      try {
        let text = `${window.location.href.split('/')[2]}` + '/booking?id=' + orderDetails.event?.id
        await navigator.clipboard.writeText(text);
        successNotify('Link Copied')
      } catch (error) {
        failureNotify('the Link is Not Copied')
      }
    }
    // return pendingOrders.map((val: any) => {
    //   if (val.orderNumber === orderId) {
    //     return val.showToolTip = true
    //   }
    // })
  }

  const totalPages: any = Math.ceil(wishList.length / perPage);

  const prev = (value: any) => {
    setActive((prevCount) => {
      const updatedCount = prevCount - 1;
      const startIndex = (updatedCount - 1) * perPage;
      const endIndex = startIndex + perPage;
      setInitialPageData(wishList.slice(startIndex, endIndex))
      return updatedCount; // Return the new state value
    });


  };


  const next = (value: any) => {
    setActive((prevCount) => {
      const updatedCount = prevCount + 1;
      const startIndex = (updatedCount - 1) * perPage;
      const endIndex = startIndex + perPage;
      setInitialPageData(wishList.slice(startIndex, endIndex))
      return updatedCount; // Return the new state value
    });
  };

  return (
    <>
      <div className="md:px-10 px-4 w-full h-[85vh] max-h-[85vh] my-10">
        <div className="md:px-10 px-4 w-full h-[85vh] max-h-[85vh] overflow-y-auto sm:w-full my-2">
          <p className="font-[sofiapro] font-bold text-3xl text-secondary">
            Din önskelista
          </p>
          {isMinLoader && <MinLoader />}
          {initialPageData && initialPageData.length > 0 ? (
            orderByDate(initialPageData)?.map((wish: any) => (
              <div className="border-[1px] rounded-md shadow-xl md:p-4 p-2 my-10 flex flex-col sm:flex-row gap-10 justify-between items-center w-full z-50">
                <section className="sm:w-1/4 w-full">
                  <img
                    className="w-full h-40 object-cover rounded-md cursor-pointer"
                    onClick={() => {
                      if (wish.product === null) {
                        navigate(`/booking?id=${wish.event?.id}`);
                      } else {
                        navigate(
                          `/cart?id=${wish.product.appUser.id}&productId=${wish.product?.id}`
                        );
                      }
                    }}
                    src={
                      wish.product?.image?.split(",")?.at(2) ||
                      wish.product?.image ||
                      wish.event?.image
                    }
                    alt={wish.product?.name || wish.event?.name}
                  />
                </section>
                <section className="sm:w-3/4 w-full">
                  <p className="font-medium text-xl mb-6 text-secondary">
                    {(wish.product?.name || wish.event?.name)?.toUpperCase()}
                  </p>

                  <p className="font-light text-sm mb-6 text-secondary">
                    {getDescription(wish)}...
                  </p>
                  <div className="flex justify-end items-center">
                    {/* <span className="md:bg-[#FFE5D8] text-secondary md:font-medium rounded-full px-3 py-2 text-sm">
                  {" "}
                  Category :{" "}
                  {wish.product?.category?.categoryName ||
                    wish.event?.category?.categoryName}
                </span>
                <span className="md:bg-[#FFE5D8] sm:mx-4 text-secondary font-medium rounded-full px-3 py-2 text-sm">
                  Price: {wish.product?.price || wish.event?.price} Kr
                </span> */}
                    {/* <Tooltip className='bg-[#D6B27D]' content="Remove from wishlist" placement="bottom"> */}
                    <span className="">
                      {" "}
                      <div className="rounded-full p-2 hover:bg-[#f1ebebef] cursor-pointer">
                        <AiFillHeart
                          className="h-6 w-6 text-[#D6B27D] z-40 "
                          onClick={() =>
                            togglefavourite(
                              wish,
                              wish.product?.id || wish.event?.id
                            )
                          }
                        />
                      </div>
                    </span>
                    {/* </Tooltip> */}
                    {/* <Tooltip className='bg-[#D6B27D]' content="Share" placement="bottom"> */}
                    <div
                      className=" w-10 h-10 rounded-full p-2 hover:bg-[#f1ebebef] mx-2 cursor-pointer"
                      onClick={() => handleShareClick(wish)}
                    >
                      <img
                        src={share}
                        className="h-6 w-6 text-[#D6B27D] z-40"
                      />
                    </div>
                    {/* </Tooltip> */}
                    <span
                      className="hover:bg-[#f1ebebef] sm:mx-4 text-secondary font-medium rounded-full px-3 py-2 text-sm cursor-pointer"
                      onClick={() =>
                        handleFavClick(wish, wish.product?.id || wish.event?.id)
                      }
                    >
                      Visa mer
                    </span>
                  </div>
                </section>
              </div>
            ))
          ) : (
            <>
              {/* {isMinLoader ? (
                <MinLoader />
              ) : ( */}
              {!isMinLoader ?
                <p className="text-center justify-center m-[100px] text-secondary text-xl">
                  No items in your whishlist
                </p> : null}
              {/* )} */}
            </>
          )}
          <div className="sm:flex sm:flex-row flex-col sm:justify-between justify-center py-4">
            <div className="flex items-center justify-center mx-2">
              <span className="text-[#D6B27D] text-md ">
                <span className="font-bold">{active * perPage + 1 - 10} </span>
                <span className="">to</span>{" "}
                <span className="font-bold">{active * perPage}</span>
                <span> of </span>{" "}
                <span className="font-bold">{totalPages * perPage}</span>
              </span>
            </div>
            <div className="flex items-center justify-center sm:gap-4">
              <Button
                variant="text"
                // color="gray"
                className="flex items-center sm:gap-2 p-1 "
                onClick={() => prev(active - 1)}
                disabled={active === 1}
              >
                <div className="border border-gray-300 rounded-md p-2">
                  <HiChevronLeft className="text-secondary text-[20px]" />
                </div>
                {/* <ArrowLeftIcon strokeWidth={2} className="h-4 w-4" /> Previous */}
              </Button>

              {/* {
                    [...Array(totalPages)].map((val: any, i: any) => {
                      return (
                        <IconButton className={`${active === i + 1 ? 'bg-[#D6B27D]' : 'bg-white text-secondary'}`} {...getItemProps(i + 1)}>{i + 1}</IconButton>)
                    })
                  } */}
              <span className="text-[#D6B27D] text-md">
                Sida <span className="font-semibold">{active}</span> of
                <span className="font-semibold"> {totalPages}</span>
              </span>

              <Button
                variant="text"
                // color="gray"
                className="flex items-center sm:gap-2 p-1 "
                onClick={() => next(active + 1)}
                disabled={active === totalPages || totalPages === 0}
              >
                <div className="border border-gray-300 rounded-md p-2">
                  <HiChevronRight className="text-secondary text-[20px]" />
                </div>
              </Button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Wishlist;
