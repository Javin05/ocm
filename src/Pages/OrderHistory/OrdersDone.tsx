import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../../Redux/store"
import { commonAxios } from "../../Axios/service"
import moment from "moment"
import { Drawer, Tooltip, Button, IconButton } from "@material-tailwind/react"
import { XMarkIcon } from "@heroicons/react/24/outline"
import { failureNotify, successNotify } from "../../Components/Toast/Toast"
import { addProductToCart } from "../../Redux/features/productCartSlice"
import RatingPopup from "./RatingPopup"
import { messageDatewise } from "../../Utils/Helperfunction"
import { useLocation, useSearchParams } from "react-router-dom"
import { IoIosHeartEmpty, IoIosSend } from "react-icons/io"
import { ArrowRightIcon, ArrowLeftIcon } from "@heroicons/react/24/outline"
import { setNotification } from "../../Redux/features/notificationSlice"
import "./OrderDone.css"
import { useNavigate, useParams } from "react-router-dom"
import OrderHistory from "./OrderHistory"
import { FiShare2 } from "react-icons/fi"
import { BiCopyAlt } from "react-icons/bi"
import Loader from "../../Components/Loader/Loader"
import shareIcon from "../../assets/grey.png"
import shareic from "../../assets/share.png"
import { ORDERSTATUS } from "../../Utils/constants"
import Select from "react-select"
import { HiChevronLeft, HiChevronRight } from "react-icons/hi"
import { RxCross1 } from "react-icons/rx"
import FeedBack from './FeedBack'

export interface OrderHistory {
  itemCategory: any
  orderTitle: string
  orderNumber: string
  companyName: string
  image: string
  orderDate: string
  orderDetails: OrderDetail[]
  totalItemPrice: number
  taxes: number
  totalAmount: number
  supplierId: string
  id: string
}

export interface OrderDetail {
  itemName: string
  quantity: number
  price: number
  itemId: string
  itemCategory: "PRODUCT" | "EVENT"
  eventDate: string
  slotTime: string
  momsPercentagePerItem: number
}
export interface OrderMessages {
  id: string
  booking: Booking
  customer: AppUser
  message: string
  isRead: boolean
  createdDate: string
  updatedDate: string
}

export interface Booking {
  id: string
  bookingDate: string
  bookingTime: string
  memberCount: any
  firstName: string
  lastName: string
  address: string
  postalCode: number
  category: string
  orderNumber: string
  bookingStatus: string
  payment: Payment
  overAllVATAmount: number
  overAllTotalAmount: number
  paymentType: string
  appUserId: any
}

export interface Payment {
  id: string
  paymentType: string
  cardNumber: any
  expiryDate: any
  country: any
  cw: any
  zipCode: any
  amount: number
  paymentMethod: string
  paymentStatus: string
  transactionDate: string
  curreny: string
  paymentId: string
}

export interface AppUser {
  id: string
  firstName: string
  lastName: string
  mobile: string
  telephone: string
  email: string
  businessName: string
  businessNumber: string
  businessAddress: string
  businessPinCode: string
  businessCity: string
  password: string
  accountStatus: string
  createdAt: string
  updatedAt: string
  categories: string
  profileImage: string
}

const OrdersDone = ({ setIsLoading }: any) => {
  const userInfo = useSelector((state: RootState) => state.user.userDetails)

  const [orderHistory, setOrderHistory] = useState<any>([])
  const [orderEventHistory, setOrderEventHistory] = useState<any>([])
  const [orderActive, setOrderActive] = useState<OrderHistory>()
  const [openRight, setOpenRight] = React.useState(false)
  const [ratingValue, setRatingValue] = useState<any>([])
  const [openChat, setOpenChat] = React.useState(false)
  const [orderMessages, setOrderMessages] = useState<OrderMessages[]>([])
  const [orderValue, setOrderValue] = useState<any>({})
  const [ratings, setRatings] = useState()
  const [openedItemsMap, setOpenedItemsMap] = useState(new Map())
  const [ratingModal, setRatingModal] = useState<boolean>(false)
  const [openedItems, setOpenedItems] = useState([])
  const [share, setShare] = useState("")
  const [check, setCheck] = useState(false)
  const [loader, setLoader] = useState(false)
  const [pendingOrders, setPendingOrders] = useState([])
  const [previousOrders, setPreviousOrders] = useState([])
  const [Booking, setOrderBooking] = useState<Booking[]>([])
  const [orderPayment, setOrderPayment] = useState<Booking>()
  const [tabMenu, setTabMenu] = useState("pending")
  const [selectedOption, setSelectedOption] = useState<any>(null)
  const [click, setClick] = useState(false)
  const [initialPageData, setInitialPageData] = useState<any>([])
  const [active, setActive] = useState(1)
  const [title, setTitle] = useState("Kommande festligheter")
  const [initialPageDataforPrevious, setInitialPageDataforPrevious] = useState(
    []
  )
  const [successPageData, setsuccessPageData] = useState(
    []
  )
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [feedBackData, setFeedBackData] = useState({})

  const dispatch = useDispatch()
  const navigate = useNavigate()
  const perPage = 10
  const searchParams = useParams()
  const value = window.location

  const openDrawerRight = (order: OrderHistory) => {
    setClick(false)
    setOrderActive(order)
    setOpenRight(true)
  }
  const closeDrawerRight = () => {
    setOrderActive(undefined)
    setOpenRight(false)
  }

  const handlePopupClose = () => {
    setRatingModal(!ratingModal)
    setOpenedItems([])
  }

  const navigateToOrderDetails = (order: any) => {
    navigate(`/orderDetails?orderNumber=${order.orderNumber}`, {
      state: order,
    })
  }

  const getOrderMessages = async (order: OrderHistory) => {
    console.log(order, "order: OrderHistory")
    try {
      const response = await commonAxios.get(
        `/cemo/messages/users?supplierId=${order.supplierId}&customerId=${userInfo.id}`
      )
      setOrderMessages(response.data)
    } catch (error) {
      // Handle the error
    }
  }
  useEffect(() => {
    const overlayElements: any = document.querySelectorAll('.bg-black')
    overlayElements.forEach((element: any) => {
      element.style.display = openChat && 'none' // hide when openChat is true, show when openChat is false
    })
  }, [openChat])

  const openDrawerChat = (order: OrderHistory) => {
    setOrderMessages([])
    setOrderActive(order)
    getOrderMessages(order)
    setOpenChat(!openChat)
  }
  const closeDrawerChat = () => {
    setOrderActive(undefined)
    setOpenChat(!openChat)
    setNewComment("")
  }

  function transformData(responseData: any) {
    const array: any = []
    responseData.map((item: any) => {

      if (item.bookingItemType === "PRODUCT") {
        const obj = {
          id: item.id,
          itemCategory: item.bookingItemType,
          orderTitle: item.product.name,
          orderNumber: item.orderNumber,
          companyName: item.product.companyName,
          image: item.product.image,
          orderDate: item.booking.bookingDate + "T" + item.booking.bookingTime,
          supplierId: item.product.id,
          orderDetails: [
            {
              itemId: item.product.id,
              itemName: item.product.name,
              quantity: item.quantity,
              price: item.totalPrice,
              itemCategory: item.bookingItemType,
              eventDate: item.booking.bookingDate,
              slotTime: null,
              momsPercentagePerItem: null,
            },
          ],
          totalItemPrice: item.totalPrice,
          taxes: item.vatAmount,
          totalAmount: item.withoutVATAmount,
        };
        array.push(obj);
      } else {
        const obj = {
          id: item.id,
          itemCategory: item.bookingItemType,
          orderTitle: item.event.name,
          orderNumber: item.orderNumber,
          companyName: item.event.appUser.businessName,
          image: item.event.image,
          orderDate: item.booking.bookingDate,
          // + "T" + item.booking.bookingTime,
          supplierId: item.event.appUser.id,
          orderDetails: [
            {
              itemId: item.event.id,
              itemName: item.event.name,
              quantity: item.booking.memberCount,
              price: item.totalPrice,
              itemCategory: item.bookingItemType,
              eventDate: item.event.startDate, // You may need to modify this field based on your requirements
              slotTime: item.schedule.startTime + "-" + item.schedule.endTime, // You may need to modify this field based on your requirements
              momsPercentagePerItem: null, // You may need to fill this based on your requirements
            },
          ],
          totalItemPrice: item.totalPrice,
          taxes: item.vatAmount,
          totalAmount: item.withoutVATAmount,
        }
        array.push(obj)
      }
    })
    return array
  }

  const getSucessOrders = async () => {
    try {
      const response = await commonAxios.get(
        `/cemo/bookingitems/get/event/product/${userInfo.id}`
      )
      if (response.data) {
        const SucessData = response.data.bookingItemList.filter((item: any) => {
          return (
            item.status === "SUCCESS" &&
            (item.bookingItemType === "EVENT" ||
              item.bookingItemType === "PRODUCT")
          )
        })
        console.log('SucessData', SucessData)
        const allEvents = SucessData.filter(
          (item: any) => item.bookingItemType === "EVENT"
        )
        const AllEventData = transformData(SucessData)
        if (AllEventData) {
          setsuccessPageData(
            AllEventData
              ?.slice((active - 1) * perPage, active * perPage)
          );
          setOrderEventHistory(AllEventData)

        }
      }
    } catch (error) {
    } finally {
    }
  }
  console.log("orderEventHistory", orderEventHistory)

  const getHistoryOfOrders = async () => {
    try {
      const response = await commonAxios.get(
        `/cemo/bookingitems/order/history/${userInfo.id}`
      )

      if (response.data) {
        setOrderHistory(
          response?.data?.sort((a: any, b: any) =>
            moment(b.orderDate).diff(moment(a.orderDate))
          )
        )
        setInitialPageDataforPrevious(
          response?.data
            ?.sort((a: any, b: any) =>
              moment(b.orderDate).diff(moment(a.orderDate))
            )
            .slice(0, perPage)
        )
      }
    } catch (error) {
    } finally {
    }
  }

  const [newComment, setNewComment] = useState("")

  const handlenewcomment = (e: any) => {
    setNewComment(e.target.value)
  }


  const addComment = async () => {
    try {
      const payload = {
        orderNumber: orderActive?.orderNumber,
        message: newComment,
        supplierId: orderActive?.supplierId,
        customerId: userInfo.id,
        isCustomerMessage: true,
        isSupplierMessage: false,
        isAdminMessage: false
      }
      const response = await commonAxios.post("/cemo/messages", payload)
      if (response.data) {
        getOrderMessages(orderActive!)
        setNewComment("")
      }
    } catch (error) {
      failureNotify("oops! something went wrong")
    }
  }
  let currentUrl = window.location.href
  const handlerepeatorder = async (order: OrderDetail[]) => {
    for await (const singleOrder of order) {
      const updatedProduct = {
        appUserId: userInfo.id,
        productId: singleOrder.itemId,
        bookingItemType: "PRODUCT",
        bookingDate: new Date()
          .toLocaleDateString()
          .split("/")
          .reverse()
          .join("-"),
        quantity: 1,
      }
      try {
        const response = await commonAxios.post(
          "/cemo/bookingitems/new",
          updatedProduct
        )

        dispatch(addProductToCart())

        if (order[0]?.itemCategory === "PRODUCT") {
          navigate(
            `${`/cart?id=${userInfo.id}&productId=${singleOrder.itemId}`}`
          )
        }
      } catch (error) {
        failureNotify("oops! something went wrong, Try again")
      }
    }
  }

  useEffect(() => {
    getPendingOrders()
    getSucessOrders()
  }, [orderHistory])


  useEffect(() => {
    getHistoryOfOrders()
  }, [])

  const openModal = async (order: any) => {
    let id = order.orderDetails.map((val: any) => {
      return val.itemId
    })
    let payload = {
      customerId: userInfo.id,
      productOrEventId: id,
    }

    try {
      const response = await commonAxios.post(
        `
        /cemo/ratings/customer`,
        payload
      )
      setRatingValue(response.data)
      if (response.data) {
        setRatingModal(true)
        setOrderValue(order)
      }
    } catch (e) { }
  }

  const checkShare = (order: any) => {
    const filteredShare = orderHistory.filter((value: any) => {
      return value.orderNumber === order.orderNumber
    })
  }

  //Pending Orders

  const navigateToPeningOrderDetails = (order: any) => {
    navigate(`/pendingOrderDetails?orderNumber=${order.orderNumber}`, {
      state: order,
    })
  }

  const getPendingOrders = async () => {
    try {
      const response = await commonAxios.get(
        `/cemo/bookingitems/pending/order/${userInfo.id}`
      )

      const upcomingFestives = response?.data?.filter((data: any) => {
        return data
      })
      setPendingOrders(
        upcomingFestives.sort((a: any, b: any) =>
          moment(b.orderDate).diff(moment(a.orderDate))
        )
      )
      setPreviousOrders(response?.data?.sort((a: any, b: any) =>
        moment(b.orderDate).diff(moment(a.orderDate))
      ))
      // setInitialPageData(
      //   upcomingFestives
      //     ?.sort((a: any, b: any) =>
      //       moment(b.orderDate).diff(moment(a.orderDate))
      //     )
      //     .slice(0, perPage)
      // )

      // Sort pending orders first
      const sortedPendingOrders = pendingOrders
        ?.filter((order: any) => { return order.status === 'PENDING' })
        .sort((a: any, b: any) => moment(b.orderDate).diff(moment(a.orderDate)))
      const sortedUpcomingFestives = upcomingFestives
        ?.filter((order: any) => { return order.status === 'ACCEPTED' })
        .sort((a: any, b: any) => moment(b.orderDate).diff(moment(a.orderDate)))
      const sortedData = sortedPendingOrders.concat(sortedUpcomingFestives)
      setInitialPageData(sortedData.slice(0, perPage))
    } catch (error) {
    } finally {
    }
  }

  const handleOrderAgain = (orderDetails: any) => {
    if (orderDetails.orderDetails[0].itemCategory === "PRODUCT") {
      navigate(
        "/cart?id=" +
        orderDetails.supplierId +
        "&productId=" +
        orderDetails.orderDetails[0].itemId
      )
    } else {
      navigate(`/booking?id=${orderDetails.orderDetails[0].itemId}`)
    }
  }

  const handleShareClick = async (orderDetails: any) => {
    setClick(true)
    setShare(orderDetails.orderNumber)
    if (orderDetails.orderDetails[0].itemCategory === "PRODUCT") {
      try {
        let text =
          `${window.location.href.split("/")[2]}` +
          "/cart?id=" +
          orderDetails.supplierId +
          "&productId=" +
          orderDetails.orderDetails[0].itemId
        await navigator.clipboard.writeText(text)
        successNotify("Link Copied")
      } catch (error) {
        failureNotify("the Link is Not Copied")
      }
    } else {
      try {
        let text =
          `${window.location.href.split("/")[2]}` +
          "/booking?id=" +
          orderDetails.orderDetails[0].itemId
        await navigator.clipboard.writeText(text)
        successNotify("Link Copied")
      } catch (error) {
        failureNotify("the Link is Not Copied")
      }
    }
    // return pendingOrders.map((val: any) => {
    //   if (val.orderNumber === orderId) {
    //     return val.showToolTip = true
    //   }
    // })
  }

  const handleShareClickForDrawer = async (
    orderDetails: any,
    supplierId: any
  ) => {
    setClick(true)
    setShare(orderDetails.itemId)
    if (orderDetails.itemCategory === "PRODUCT") {
      try {
        let text =
          `${window.location.href.split("/")[2]}` +
          "/cart?id=" +
          supplierId +
          "&productId=" +
          orderDetails.itemId
        await navigator.clipboard.writeText(text)
        successNotify("Link Copied")
      } catch (error) {
        failureNotify("the Link is Not Copied")
      }
    } else {
      try {
        let text =
          `${window.location.href.split("/")[2]}` +
          "/booking?id=" +
          orderDetails.itemId
        await navigator.clipboard.writeText(text)
        successNotify("Link Copied")
      } catch (error) {
        failureNotify("the Link is Not Copied")
      }
    }
    // return pendingOrders.map((val: any) => {
    //   if (val.orderNumber === orderId) {
    //     return val.showToolTip = true
    //   }
    // })
  }

  const getItemProps = (index: any) =>
  ({
    variant: active === index ? "filled" : "text",
    color: active === index ? "blue" : "blue-gray",
    onClick: () =>
      setActive((prevCount) => {
        const updatedCount = prevCount + 1
        const startIndex = (updatedCount - 1) * perPage
        const endIndex = startIndex + perPage
        setInitialPageData(pendingOrders.slice(startIndex, endIndex))

        return updatedCount // Return the new state value
      }),
  } as any)

  const next = (value: any) => {
    // if (active === 5) return

    // setActive(active + 1)

    setActive((prevCount) => {
      const updatedCount = prevCount + 1
      const startIndex = (updatedCount - 1) * perPage
      const endIndex = startIndex + perPage

      console.log("startIndex", startIndex)
      console.log("endIndex", endIndex)

      if (tabMenu === "pending") {
        // if(selectedOption?.value === "All Pending Orders"){
        //   const allOrders = [...pendingOrders,...orderEventHistory]
        //   setInitialPageData(allOrders.slice(startIndex, endIndex))
        // } else 
        if (selectedOption?.value === "Kommande festligheter") {
          setInitialPageData(orderEventHistory.slice(startIndex, endIndex))
        } else {
          setInitialPageData(pendingOrders.slice(startIndex, endIndex))
        }
      } else if (tabMenu === "previous") {
        setInitialPageDataforPrevious(orderHistory.slice(startIndex, endIndex))
      }
      else {
        console.log("jfhgjdfhgdf")
        setsuccessPageData(orderEventHistory.slice(startIndex, endIndex))
      }
      return updatedCount // Return the new state value
    })
  }


  const totalPages = Math.ceil(
    tabMenu === "pending"
      ? pendingOrders?.length / perPage
      : tabMenu === "previous"
        ? orderHistory.length / perPage
        : orderEventHistory.length / perPage
  );

  const totalPagesforPrevious = Math.ceil(orderHistory.length / perPage)
  const prev = (value: any) => {
    setActive((prevCount) => {
      const updatedCount = prevCount - 1
      const startIndex = (updatedCount - 1) * perPage
      const endIndex = startIndex + perPage
      if (tabMenu === "pending") {
        setInitialPageData(pendingOrders.slice(startIndex, endIndex))
      } else if (tabMenu === "previous") {
        setInitialPageDataforPrevious(orderHistory.slice(startIndex, endIndex))
      }
      else {
        console.log("jfhgjdfhgdf")
        setOrderEventHistory(orderEventHistory.slice(startIndex, endIndex))
      }

      return updatedCount // Return the new state value
    })
  }

  const handleClick = () => {
    setSelectedOption("")
  }

  const handleMerchantChange = (selectedOption: any) => {
    if (selectedOption.value === "Pågående förfrågningar") {
      setTitle("Pågående förfrågningar")
      console.log(previousOrders, "previousOrders")
      const ongoingRequest = previousOrders.filter((data: any) => {
        return data.status === "PENDING"
      })
      console.log(ongoingRequest, "ongoingRequest")
      setInitialPageData(
        ongoingRequest
          ?.sort((a: any, b: any) =>
            moment(b.orderDate).diff(moment(a.orderDate))
          )
          .slice(0, perPage)
      )
      setPendingOrders(ongoingRequest)
    }
    // else if (selectedOption.value === "All Pending Orders"){
    //   setTitle("All Pending Orders")
    //   const allOrders = [...pendingOrders,...orderEventHistory]
    //   setInitialPageData(
    //     allOrders
    //       ?.sort((a: any, b: any) =>
    //         moment(b.orderDate).diff(moment(a.orderDate))
    //       )
    //       .slice(0, perPage)
    //   )
    // }
    else {
      setTitle("Kommande festligheter")
      const upcomingFestives = previousOrders.filter((data: any) => {
        return data.status === "ACCEPTED"
      })
      setInitialPageData(
        upcomingFestives
          ?.sort((a: any, b: any) =>
            moment(b.orderDate).diff(moment(a.orderDate))
          )
          .slice(0, perPage)
      )
      setPendingOrders(
        upcomingFestives.sort((a: any, b: any) =>
          moment(b.orderDate).diff(moment(a.orderDate))
        )
      )
    }
    setSelectedOption(selectedOption)
    setActive(1)
  }

  const CustomDropdownIndicator = () => (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        style={{ width: "24px", height: "24px", fill: '#A09792' }}
      >
        <path fill="none" d="M0 0h24v24H0z" />
        <path d="M7 10l5 5 5-5z" />
      </svg>
    </div>
  )

  const customComponents: any = {
    DropdownIndicator: CustomDropdownIndicator,
  }

  const customStyles = {
    control: (provided: any, state: any) => ({
      ...provided,
      border: "none",
      outline: "none",
      fontSize: "17px",
      width: "200px",
      boxShadow: "none",
    }),
    option: (provided: any, state: any) => ({
      ...provided,
      backgroundColor: "transparent",
      fontSize: "17px",
      width: "300px",
      boxShadow: "none",
      color: state.isSelected ? "#D6B27D" : "gray",
    }),
    singleValue: (provided: any) => ({
      ...provided,
      color: "gray", // Set the initial text color to gray
    }),
    valueContainer: (provided: any) => ({
      ...provided,
      color: "gray", // Set the initial text color to gray
    }),
  }

  useEffect(() => {
    if (openChat) {
      document.body.classList.add("overflow-hidden")
    } else {
      document.body.classList.remove("overflow-hidden")
    }
  }, [openChat])

  const options = [
    // { value:'All Pending Orders',label:'All Pending Orders' },
    { value: 'Kommande festligheter', label: 'Kommande festligheter' },
    { value: 'Pågående förfrågningar', label: 'Pågående förfrågningar' },
  ]
  const defaultValue = options[0]

  console.log('initialPageDataforPrevious', initialPageDataforPrevious)

  return (

    <>
      {isModalOpen ? (
        <FeedBack
          setIsModalOpen={setIsModalOpen}
          feedBackData={feedBackData}
        />
      ) : null}
      {tabMenu === "pending" && (
        <div className="mt-5">
          <div className="mx-5 flex">
            <button
              className={`text-xl px-5 rounded-tl-lg text-secondary rounded-tr-lg ${tabMenu === "pending" &&
                "border-t-2 border-l-2 border-r-2 text-golden border-[#FFE5D8]"
                }`}
              onClick={() => setTabMenu("pending")}
            >
              Förfrågningar
            </button>
            <button
              className={`ml-2 text-xl  text-secondary`}
              onClick={() => {
                setTabMenu("previous")
                setSelectedOption(null)
              }}
            >
              Tidigare beställningar
            </button>
            <button
              className={`ml-2 text-xl  text-secondary`}
              onClick={() => {
                setTabMenu("success")
                setSelectedOption(null)
              }}
            >
              Godkända
            </button>
          </div>

          <Drawer
            placement="right"
            size={699}
            open={openRight}
            onClose={closeDrawerRight}
            className="md:p-4 h-screen"
          >
            <div className="mb-6 md:ml-[600px] ml-[300px] overflow-y-scroll flex items-center justify-between">
              <IconButton
                variant="text"
                color="blue-gray"
                onClick={closeDrawerRight}
              >
                <XMarkIcon strokeWidth={2} className="h-5 w-5" />
              </IconButton>
            </div>
            <div className="md:px-12 px-4">
              <section className="pb-2 mb-6 border-b-[1px] border-black">
                <div className="flex flex-row justify-between items-center ">
                  {/* <h2 className='font-bold text-2xl text-[#c48c39]'>{orderPayment?.paymentType}</h2> */}
                  <span className="font-bold md:text-xl text-md text-[#c48c39]">
                    {orderActive?.orderTitle}
                  </span>
                  <span className="font-bold md:text-xl text-md text-[#c48c39]">
                    Payment type
                  </span>
                </div>
                <div className="flex flex-row justify-between items-center w-full mb-2">
                  <span className="font-bold text-sm text-secondary  pb-2 mb-1 border-black">
                    Order #{orderActive?.orderNumber}
                  </span>
                  <span className="font-bold text-sm text-secondary  pb-2 mb-1 pr-[75px] border-black">
                    ONLINE
                  </span>
                </div>
              </section>
              {/* <p className='font-bold text-sm text-[#000000] pb-8 mb-6 border-b-[1px] border-black'>Order #{orderActive?.orderNumber}</p> */}
              <p className="mb-4">{orderActive?.orderDetails.length} items</p>
              <section className="pb-6 mb-8 border-dotted border-b-2 border-[#e3e3e3] text-secondary ">
                {orderActive?.orderDetails.map((item: any) => (
                  <div className="flex flex-row justify-between items-center w-full mb-2">
                    <span className="font-semibold">
                      {item.itemName} X {item.quantity}
                    </span>
                    {/* {click && share === item.itemId && successNotify('Link Copied')} */}

                    {/* <Tooltip className='bg-[#D6B27D] z-[9999]' content="Share" placement="bottom"> */}
                    <div
                      onClick={() =>
                        handleShareClickForDrawer(item, orderActive.supplierId)
                      }
                      className="hidden sm:flex rounded-full p-3 hover:bg-[#dbd4d4ef] "
                    >
                      <img
                        src={shareIcon}
                        className="h-5 w-5 text-[#D6B27D] cursor-pointer"
                      />
                    </div>
                    {/* </Tooltip> */}

                    <span className="text-gray-600 text-lg font-medium">
                      moms/item -{" "}
                      {item.itemCategory === "EVENT"
                        ? Math.round(
                          (orderActive?.taxes / orderActive?.totalItemPrice) *
                          100
                        )
                        : item.momsPercentagePerItem}{" "}
                      %
                    </span>
                    <span className="text-gray-600 text-lg font-medium">
                      {item.price} SEK
                    </span>
                  </div>
                ))}
              </section>
              <section className="pb-8 mb-6 border-b-[1px] border-black">
                <div className="flex flex-row justify-between items-center w-full mb-2">
                  <span className="font-medium">Item Total</span>
                  <span className="text-gray-600 text-lg font-medium">
                    {orderActive?.totalAmount} SEK
                  </span>
                </div>
                <div className="flex flex-row justify-between items-center w-full mb-2">
                  <span className="text-gray-600 font-medium">MOMS</span>
                  <span className="text-gray-600 text-lg font-medium">
                    {orderActive?.taxes} SEK
                  </span>
                </div>
              </section>
              <div className="flex flex-row justify-between items-center w-full mb-2">
                <span className=" font-bold text-secondary ">BILL TOTAL</span>
                <span className="text-secondary  font-bold">
                  {orderActive?.totalItemPrice} SEK
                </span>
              </div>
            </div>
          </Drawer>
          <Drawer
            placement="right"
            size={470}
            open={openChat}
            onClose={closeDrawerChat}
            className="p-4 h-screen"
          >
            <div className="flex items-center justify-between md:mb-3 ml-[270px] md:ml-[410px]">
              <IconButton
                variant="text"
                color="blue-gray"
                onClick={closeDrawerChat}
              >
                <XMarkIcon strokeWidth={2} className="h-5 w-5" />
              </IconButton>
            </div>
            <div className="px-4 h-[60vh] max-h-[60vh] -mt-1 overflow-y-scroll border-[1px] border-[#e3e3e3] rounded-md">
              {orderMessages.length > 0 ? (
                messageDatewise(orderMessages).map((message: any) =>
                  !message.isCustomerMessage ? (
                    <div className="flex flex-row justify-between items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md">
                      <section className="w-3/4">
                        <div className="flex flex-row">
                          {message.supplier?.profileImage ? (
                            <img
                              className="h-[25px] w-[25px] mx-4 mb-4 rounded-full"
                              src={message.supplier?.profileImage}
                            ></img>
                          ) : (
                            <span className="rounded-full p-2 px-4 font-semibold mx-2 bg-[#D6B27D] text-white">
                              {message.supplier?.firstName
                                .split("")
                                .at(0)
                                ?.toUpperCase() +
                                "" +
                                message.supplier?.lastName
                                  .split("")
                                  .at(0)
                                  ?.toUpperCase()}
                            </span>
                          )}
                          <p className="text-[#D6B27D] font-medium mt-1.5 text-[14px]">
                            {message?.supplier?.businessName}{" "}
                            {/* {message?.supplier?.lastName} */}
                          </p>
                        </div>
                        <h4 className="text-sm  text-left text-[40px] text-secondary">
                          {message?.message}
                        </h4>
                      </section>
                      <span className="ml-2 text-[10px] text-secondary">
                        {moment(message?.createdDate).format(
                          "ddd, MMM DD YYYY"
                        )}
                      </span>
                    </div>
                  ) : (
                    <div className="flex flex-row items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md">
                      <span className="w-1/4 text-[10px] text-secondary">
                        {moment(message?.createdDate).format(
                          "ddd, MMM DD YYYY"
                        )}
                      </span>
                      <section className="w-3/4">
                        <div className="flex justify-end">
                          <p className="text-[#D6B27D] font-medium  mt-1.5 text-[14px]">
                            {message?.customer?.firstName}{" "}
                            {message?.customer?.lastName}
                          </p>
                          {message.customer?.profileImage ? (
                            <img
                              className="h-[25px] w-[25px] mx-4 mb-4 rounded-full"
                              src={message.customer?.profileImage}
                            ></img>
                          ) : (
                            <span className="p-2 px-4 rounded-full font-semibold bg-[#D6B27D] text-white">
                              {message.booking?.firstName
                                .split("")
                                .at(0)
                                ?.toUpperCase() +
                                "" +
                                message.booking?.lastName
                                  .split("")
                                  .at(0)
                                  ?.toUpperCase()}
                            </span>
                          )}
                        </div>
                        <h4 className="text-sm text-right text-[20px] text-secondary">
                          {message?.message}
                        </h4>
                      </section>
                    </div>
                  )
                )
              ) : (
                <div className="justify-center w-full py-4 px-10 text-[#D6B37D]">
                  Send message to the supplier
                </div>
              )}
            </div>
            <div className="relative mt-3">
              <div className="">
                <textarea
                  placeholder="Dina kommentarer..."
                  value={newComment}
                  onChange={handlenewcomment}
                  className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md"
                  name="comment"
                  id="comment"
                  rows={3}
                />
              </div>
              <IoIosSend
                onClick={addComment}
                className="absolute rotate-45 cursor-pointer bottom-[5px] right-[15px] h-[60px] w-[40px]  text-[#D6B27D] send-icon"
              />
            </div>
          </Drawer>
          <div className="mx-2">
            <div className="md:px-10 px-4 w-full h-[85vh] max-h-[85vh] overflow-y-auto sm:w-full my-2">
              {loader && <Loader />}

              {/* <p className="font-[kammerlander] font-bold text-3xl">Pending Orders</p> */}
              {initialPageData.length > 0 ? (
                initialPageData?.map((order: any) => (

                  <div className="border-[1px] rounded-md shadow-xl p-4 my-10 flex flex-col sm:flex-row justify-between items-center w-full">
                    <section className="sm:w-auto w-full">
                      <img
                        className="w-[152px] h-[152px] object-cover rounded-md cursor-pointer"
                        src={order.image}
                        onClick={() => {
                          navigate(
                            `/booking?id=${order.orderDetails[0]?.itemId}`
                          )
                        }}
                      />
                    </section>
                    <section className="sm:w-3/4 w-full">
                      <div className="">
                        <div className="grid grid-cols-1 sm:grid-cols-2 gap-4">
                          <p className="font-bold text-xl mb-3 font-[sofiapro] text-secondary  xl:-ml-20 md:ml-2 lg:ml-2">
                            {order.orderTitle.toUpperCase()}
                          </p>
                          <p className=
                            {`sm:mx-4 font-bold text-golden font-medium text-[20px] mt-4 sm:mt-0`}
                          >
                            {ORDERSTATUS[order.status]}

                          </p>
                        </div>
                        <span className="  text-secondary font-medium  items-center rounded-md  xl:-ml-20 md:ml-2 lg:ml-2">
                          {`Order# ${order.orderNumber}`}
                        </span>

                        {/* <button className="bg-[#FFC12320] rounded-xl md:rounded-full ml-3 font-medium w-auto h-8 md:w-30 md:h-10 lg:w-40 lg:h-14 p-2 text-[10px] md:text-[16px] text-[#D6B37D]">{`Order Number # ${order.orderNumber}`}</button> */}
                      </div>

                      <span className=" text-secondary text-sm rounded-md py-2  xl:-ml-20 md:ml-2 lg:ml-2">
                        {moment(order.orderDate).format("ddd, MMM DD YYYY")}
                      </span>
                      <br />

                      {/* {share === order.orderNumber && <p className="text-white mx-10 bg-black h-8 w-32 rounded-md flex items-center justify-center"> Link Copied!</p>} */}

                      <div className="flex justify-start items-center xl:-ml-24 md:-ml-2 lg:-ml-2">
                        {/* <span className="bg-[#FFE5D8] sm:mx-4 text-black font-medium rounded-md px-3 py-2">
                       {order.orderDate}
                       </span> */}
                        <span
                          onClick={() => openDrawerChat(order)}
                          className="sm:mx-4 text-golden font-medium text-[20px] cursor-pointer mt-4 sm:mt-0"
                        >
                          Chatt
                        </span>
                        {/* <span
                        onClick={() => handlerepeatorder(order.orderDetails)}
                        className="sm:mx-4 text-[#c48c39] font-medium text-lg cursor-pointer"
                      >
                        Beställ igen
                      </span> */}
                        <button
                          type="button"
                          // onClick={() => openDrawerRight(order)}
                          onClick={() => navigateToPeningOrderDetails(order)}
                          className="sm:mx-4 text-golden font-medium text-[20px] cursor-pointer"
                        >
                          Visa detaljer
                        </button>

                        {order && order.orderDetails.length < 2 ? (
                          <div className="relative group mb-2">
                            <button
                              type="button"
                              onClick={() => handleShareClick(order)}
                              className="sm:mx-4 text-golden font-medium text-lg cursor-pointer flex"
                            >
                              {/* <Tooltip className='bg-[#D6B27D] z-[9999]' content="Share" placement="right"> */}
                              <div className="flex">
                                <div className="relative">
                                  <div className="hover:bg-[#ebe2e2ef] rounded-full p-2 mt-1 transition-all duration-200">
                                    <img
                                      src={shareic}
                                      className="h-5 w-5 text-golden flex items-center z-40"
                                      alt="Share Icon"
                                    />
                                  </div>
                                </div>
                                {/* <p className="flex items-center ml-1 mt-2.5">Share</p> */}
                              </div>
                              {/* </Tooltip> */}
                            </button>
                          </div>
                        ) : null}
                      </div>
                    </section>
                  </div>
                ))
              ) : (
                <p className="text-center justify-center m-60 text-gray-600 text-xl">
                  No pending orders to show
                </p>
              )}
              <div className="sm:flex sm:flex-row flex-col sm:justify-between justify-center py-4">
                <div className="flex items-center justify-center mx-2">
                  <span className="text-golden text-md ">
                    <span className="font-bold">
                      {active * perPage + 1 - 10}{" "}
                    </span>
                    <span className="">to</span>{" "}
                    <span className="font-bold">{active * perPage}</span>
                    <span> of </span>{" "}
                    <span className="font-bold">{totalPages * perPage}</span>
                  </span>
                </div>
                <div className="flex items-center justify-center sm:gap-4">
                  <Button
                    variant="text"
                    // color="gray"
                    className="flex items-center sm:gap-2 p-1 "
                    onClick={() => prev(active - 1)}
                    disabled={active === 1}
                  >
                    <div className="border border-gray-300 rounded-md p-2">
                      <HiChevronLeft className="text-secondary text-[20px]" />
                    </div>
                    {/* <ArrowLeftIcon strokeWidth={2} className="h-4 w-4" /> Previous */}
                  </Button>

                  {/* {
                    [...Array(totalPages)].map((val: any, i: any) => {
                      return (
                        <IconButton className={`${active === i + 1 ? 'bg-[#D6B27D]' : 'bg-white text-secondary'}`} {...getItemProps(i + 1)}>{i + 1}</IconButton>)
                    })
                  } */}
                  <span className="text-golden text-md">
                    Sida <span className="font-semibold">{active}</span> of
                    <span className="font-semibold"> {totalPages}</span>
                  </span>

                  <Button
                    variant="text"
                    // color="gray"
                    className="flex items-center sm:gap-2 p-1 "
                    onClick={() => next(active + 1)}
                    disabled={active === totalPages}
                  >
                    <div className="border border-gray-300 rounded-md p-2">
                      <HiChevronRight className="text-secondary text-[20px]" />
                    </div>
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
      {tabMenu === "previous" && (
        <div className="mt-5">
          <div className="mx-5 flex">
            <button
              className={`text-xl rounded-tl-lg text-secondary rounded-tr-lg `}
              onClick={() => {
                setTabMenu("pending")
                setSelectedOption(null)
              }}
            >
              Förfrågningar
            </button>
            <button
              className={`text-xl px-3 rounded-tl-lg text-secondary rounded-tr-lg ${tabMenu === "previous" &&
                "m-2 border-t-2 border-l-2 border-r-2 text-golden border-[#FFE5D8]"
                }`}
              onClick={() => setTabMenu("previous")}
            >
              Tidigare beställningar
            </button>
            <button
              className={`ml-2 text-xl  text-secondary`}
              onClick={() => {
                setTabMenu("success")
                setSelectedOption(null)
              }}
            >
              Godkända
            </button>
          </div>
          <Drawer
            placement="right"
            size={699}
            open={openRight}
            onClose={closeDrawerRight}
            className="px-4 h-screen"
          >
            <div className="mb-6 md:ml-[600px] ml-[300px] overflow-y-scroll flex items-center justify-between mt-5">
              <IconButton
                variant="text"
                color="blue-gray"
                onClick={closeDrawerRight}
              >
                <XMarkIcon strokeWidth={2} className="h-5 w-5" />
              </IconButton>
            </div>
            <div className="md:px-12 px-2">
              <h2 className="font-bold text-2xl text-golden text-center">
                {orderActive?.companyName}
              </h2>
              <p className="font-bold text-sm text-secondary pb-8 mb-6 border-b-[1px] border-black text-center">
                Order #{orderActive?.orderNumber}
              </p>
              <p className="mb-4 text-secondary">
                {orderActive?.orderDetails.length} items
              </p>
              <section className="pb-6 mb-8 border-dotted border-b-2 border-[#e3e3e3]">
                {orderActive?.orderDetails.map((item: any) => (
                  <div className="flex flex-row justify-between items-center w-full mb- text-secondary">
                    <span className="font-semibold">
                      {item.itemName} {item.quantity}
                    </span>
                    {/* {click && share === item.itemId && successNotify('Link Copied')} */}
                    {/* <button onClick={() => handleShareClickForDrawer(item, orderActive.supplierId)} className="hidden sm:flex rounded-full p-3 hover:bg-[#dbd4d4ef]">
                      <img src={shareIcon} className="h-5 w-5 text-[#D6B27D] z-40" />                   
                       </button> */}

                    {/* <Tooltip className='bg-[#D6B27D] z-[9999]' content="Share" placement="bottom"> */}
                    <div
                      onClick={() =>
                        handleShareClickForDrawer(item, orderActive.supplierId)
                      }
                      className="hidden sm:flex rounded-full p-3 hover:bg-[#dbd4d4ef] "
                    >
                      <img
                        src={shareIcon}
                        className="h-5 w-5 text-golden cursor-pointer"
                      />
                    </div>
                    {/* </Tooltip> */}

                    <span className="text-gray-600 text-lg font-medium">
                      moms/item -{" "}
                      {item.itemCategory === "EVENT"
                        ? Math.round(
                          (orderActive?.taxes / orderActive?.totalItemPrice) *
                          100
                        )
                        : item.momsPercentagePerItem}{" "}
                      %
                    </span>
                    <span className="text-gray-600 text-lg font-medium ">
                      {item.price} SEK
                    </span>
                  </div>
                ))}
              </section>
              <section className="pb-8 mb-6 border-b-[1px] border-black">
                <div className="flex flex-row justify-between items-center w-full mb-2">
                  <span className="font-medium text-secondary">Item Total</span>
                  <span className="text-gray-600 text-lg font-medium">
                    {orderActive?.totalAmount} SEK
                  </span>
                </div>
                <div className="flex flex-row justify-between items-center w-full mb-2">
                  <span className="text-gray-600 font-medium">MOMS</span>
                  <span className="text-gray-600 text-lg font-medium">
                    {orderActive?.taxes} SEK
                  </span>
                </div>
              </section>
              <div className="flex flex-row justify-between items-center w-full mb-2">
                <span className="text-secondary font-bold">BILL TOTAL</span>
                <span className="text-secondary font-bold">
                  {orderActive?.totalItemPrice} SEK
                </span>
              </div>
            </div>
          </Drawer>
          {ratingModal && (
            <RatingPopup
              orderValue={orderValue}
              ratingModal={ratingModal}
              // setIsLoading={setIsLoading}
              setRatingModal={handlePopupClose}
              ratingValue={ratingValue}
            // ratings={ratings}
            />
          )}

          <Drawer
            placement="right"
            size={699}
            open={openChat}
            onClose={closeDrawerChat}
            className="px-4 h-screen"
          >
            <div className="mb-6 flex justify-start items-center">
              <IconButton
                variant="text"
                color="blue-gray"
                onClick={closeDrawerChat}
              >
                <XMarkIcon strokeWidth={2} className="h-5 w-5" />
              </IconButton>
              <div className="flex">
                <h1 className="font-bold md:ml-56 ml-28 text-lg text-[#D6B27D]">
                  {orderActive?.companyName}
                </h1>
              </div>
            </div>
            <div>
              <h2 className="font-bold text-lg py-4 text-secondary">
                Order No : {orderActive?.orderNumber}
              </h2>
            </div>
            <div className="px-4 h-[55vh] max-h-[55vh] w-full overflow-y-scroll border-[1px] border-[#e3e3e3] rounded-md">
              {orderMessages.length > 0 ? (
                messageDatewise(orderMessages).map((message: any) =>
                  !message.isCustomerMessage ? (
                    <div className="flex flex-row justify-between items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md">
                      <section className="w-3/4">
                        <div className="flex flex-row items-center">
                          {message.supplier?.profileImage ? (
                            <img
                              className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                              src={message.supplier?.profileImage}
                            ></img>
                          ) : (
                            <span className="rounded-full p-2 px-4 font-semibold mx-2 bg-[#D6B27D] text-white">
                              {message.supplier?.firstName
                                .split("")
                                .at(0)
                                ?.toUpperCase() +
                                "" +
                                message.supplier?.lastName
                                  .split("")
                                  .at(0)
                                  ?.toUpperCase()}
                            </span>
                          )}
                          <p className="text-[#D6B27D] font-medium mt-1.5">
                            {message?.supplier?.businessName}{" "}
                            {/* {message?.supplier?.lastName} */}
                          </p>
                        </div>
                        <h4 className="text-sm text-left text-secondary">
                          {message?.message}
                        </h4>
                      </section>
                      <span className="w-1/4 ml-8 text-sm text-secondary">
                        {moment(message?.createdDate).format(
                          "ddd, MMM DD YYYY"
                        )}
                      </span>
                    </div>
                  ) : (
                    <div className="flex flex-row items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md">
                      <span className="w-1/4 text-sm text-secondary">
                        {moment(message?.createdDate).format(
                          "ddd, MMM DD YYYY"
                        )}
                      </span>
                      <section className="w-3/4">
                        <div className="flex justify-end items-center py-3">
                          <p className="text-[#D6B27D] font-medium mt-1.5">
                            {message?.customer?.firstName}{" "}
                            {message?.customer?.lastName}
                          </p>

                          {message.customer?.profileImage ? (
                            <img
                              className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                              src={message.customer?.profileImage}
                            ></img>
                          ) : (
                            <span className="rounded-full p-2 font-semibold mx-2 bg-[#D6B27D] text-white">
                              {message.customer?.firstName
                                .split("")
                                .at(0)
                                ?.toUpperCase() +
                                "" +
                                message.customer?.lastName
                                  .split("")
                                  .at(0)
                                  ?.toUpperCase()}
                            </span>
                          )}
                        </div>
                        <h4 className="text-sm text-right text-secondary">
                          {message?.message}
                        </h4>
                      </section>
                    </div>
                  )
                )
              ) : (
                <div className="justify-center py-4 px-44 text-[#D6B37D]">
                  Inga meddelanden
                </div>
              )}
            </div>
            {/* <div className="pt-10 w-full">
              <textarea
                placeholder="Dina kommentarer..."
                value={newComment}
                onChange={handlenewcomment}
                className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md"
                name="comment"
                id="comment"
                rows={3}
              />
            </div>
            <div className="w-full">
              <button
                onClick={addComment}
                className="bg-[#D6B27D] outline-none w-full mt-6 cursor-pointer text-white font-semibold rounded-md px-3 text-lg py-2"
              >
                SKICKA
              </button>
            </div> */}

            <div className="relative mt-3">
              <div className="">
                <textarea
                  placeholder="Dina kommentarer..."
                  value={newComment}
                  onChange={handlenewcomment}
                  className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md"
                  name="comment"
                  id="comment"
                  rows={3}
                />
              </div>
              <IoIosSend
                onClick={addComment}
                className="absolute rotate-45 cursor-pointer bottom-[5px] right-[15px] h-[60px] w-[40px]  text-[#D6B27D] send-icon"
              />
            </div>
          </Drawer>
          <div className="mx-2">
            <div className="md:px-10 px-4 w-full h-[85vh] max-h-[85vh] overflow-y-auto sm:w-full my-2">
              {/* <p className="font-[kammerlander] font-bold text-3xl">Previous Orders</p> */}
              {initialPageDataforPrevious.length > 0 ? (
                initialPageDataforPrevious?.map((order: any) => (
                  <div className="border-[1px] rounded-md shadow-xl p-4 my-10 flex flex-col sm:flex-row justify-between items-center w-full">
                    <section className="sm:w-auto w-full">
                      <img
                        className="w-[152px] h-[152px] object-cover rounded-md cursor-pointer group"
                        src={order.image?.split(",")?.at(0)}
                        alt="product-image"
                        // onClick={() => navigateToOrderDetails(order)}
                        onClick={() => {
                          if (
                            order?.orderDetails[0]?.itemCategory === "EVENT"
                          ) {
                            navigate(
                              `/booking?id=${order?.orderDetails[0]?.itemId}`
                            )
                          } else {
                            navigate(
                              `/cart?id=${order.supplierId}&productId=${order?.orderDetails[0]?.itemId}`
                            )
                          }
                        }}
                      />
                    </section>

                    <section className="sm:w-3/4 w-full ">
                      <div className="">
                        <p className="font-bold text-xl font-[sofiapro] text-secondary xl:-ml-6 md:ml-4 lg:ml-4">
                          {order && order.orderTitle ? order.orderTitle.toUpperCase() : ''}
                        </p>
                        <span className="  text-secondary font-medium items-center rounded-md xl:-ml-6 md:ml-4 lg:ml-4">
                          {`Order# ${order.orderNumber}`}
                        </span>

                        {/* <button className="bg-[#FFC12320] rounded-xl md:rounded-full ml-3 font-medium w-auto h-8 md:w-30 md:h-10 lg:w-40 lg:h-14 p-2 text-[10px] md:text-[16px] text-[#D6B37D]">{`Order Number # ${order.orderNumber}`}</button> */}
                      </div>

                      <span className=" text-secondary text-sm rounded-md py-2 xl:-ml-6 md:ml-4 lg:ml-4">
                        {moment(order.orderDate).format("ddd, MMM DD YYYY")}
                      </span>
                      <br />

                      {/* <p className='font-medium text-lg mb-6'><span className='font-bold'>Price</span> : {order.totalAmount} kr</p> */}
                      {/* {click && share === order.orderNumber && successNotify('Link Copied')} */}
                      {/* <div> {share === order.orderNumber && <p className="text-white mx-10 bg-black h-8 w-32 rounded-md flex items-center justify-center mb-6"> Link Copied!</p>}</div> */}

                      <div className="flex justify-start items-center xl:-ml-10 md:ml-1 lg:ml-1">
                        <span
                          onClick={() => openDrawerChat(order)}
                          className="sm:mx-4 text-golden font-medium text-[20px] cursor-pointer mt-4 sm:mt-0"
                        >
                          Chatt
                        </span>
                        <span
                          // onClick={() => handlerepeatorder(order.orderDetails)}
                          onClick={() => handleOrderAgain(order)}
                          className="sm:mx-4 text-golden text-[20px] font-medium cursor-pointer mt-4 sm:mt-0"
                        >
                          Beställ igen
                        </span>
                        <button
                          type="button"
                          // onClick={
                          //   () => openDrawerRight(order)
                          // }
                          // onClick={() => handleOrderAgain(order)}
                          onClick={() => navigateToOrderDetails(order)}
                          className="sm:mx-4 text-golden font-medium cursor-pointer mt-4 sm:mt-0 text-[20px]"
                        >
                          Visa detaljer
                        </button>
                        {order?.orderDetails[0]?.itemCategory === "PRODUCT" && (
                          <button
                            type="button"
                            onClick={() => openModal(order)}
                            className="sm:mx-4 text-golden font-medium cursor-pointer mt-4 sm:mt-0  text-[20px]"
                          >
                            Betyg
                          </button>
                        )}
                        {order.orderDetails.length < 2 ? (
                          <div className="relative group mb-2">
                            <div className="">
                              <button
                                type="button"
                                onClick={() => handleShareClick(order)}
                                className="sm:mx-4 text-golden font-medium text-[20px] cursor-pointer flex"
                              >
                                <div className="flex">
                                  <div className="relative">
                                    <div className="hover:bg-[#ebe2e2ef] rounded-full  p-2 mt-1 transition-all duration-200">
                                      <img
                                        src={shareic}
                                        className="h-5 w-5 text-[#D6B27D] flex items-center z-40"
                                        alt="Share Icon"
                                      />
                                    </div>
                                  </div>
                                  {/* <p className="flex items-center ml-1 mt-2">Share</p> */}
                                </div>
                                {/* </Tooltip> */}
                              </button>
                            </div>
                          </div>
                        ) : null}
                      </div>
                    </section>

                    <div className="flex flex-col">
                      {/* <div>
                {share && (
                  <>
                    {" "}
                    <div className="bg-white text-[#c48c39]">Link Copied!</div>
                  </>
                )}
              </div> */}
                      {/* <div className="flex"> */}
                      {/* <FiShare2
                  className="text-[20px] cursor-pointer mx-5"
                  onClick={() => {
                    checkShare(order)
                  }}
                /> */}
                      {/* <section className="bg-gray-300 font-semibold text-white rounded-xl text-xs py-1 px-2 top-0 right-0">
                      {order.orderDetails[0]?.itemCategory}
                    </section>
                  </div> */}
                    </div>
                  </div>
                ))
              ) : (
                <p className="text-gray-600 text-xl text-center justify-center m-60">
                  No prevoius orders to show
                </p>
              )}
              <div className="sm:flex sm:flex-row flex-col sm:justify-between justify-center py-4">
                <div className="flex justify-center items-center sm:mx-2">
                  <span className="text-golden text-md ">
                    <span className="font-bold">
                      {active * perPage + 1 - 10}{" "}
                    </span>
                    <span className="">to</span>{" "}
                    <span className="font-bold">{active * perPage}</span>
                    <span> of </span>{" "}
                    <span className={`font-bold`}>
                      {totalPagesforPrevious * perPage}
                    </span>
                  </span>
                </div>
                <div className="flex justify-center items-center sm:gap-4">
                  <Button
                    variant="text"
                    color="gray"
                    className="flex items-center sm:gap-2 p-1 sm:p-4"
                    onClick={() => prev(active - 1)}
                    disabled={active === 1}
                  >
                    <div className="border border-gray-300 rounded-md p-2">
                      <HiChevronLeft className="text-secondary text-[20px]" />
                    </div>
                  </Button>

                  {/* {
                    [...Array(totalPages)].map((val: any, i: any) => {
                      return (
                        <IconButton className={`${active === i + 1 ? 'bg-[#D6B27D]' : 'bg-white text-secondary'}`} {...getItemProps(i + 1)}>{i + 1}</IconButton>)
                    })
                  } */}
                  <span className="text-golden text-md">
                    Sida <span className="font-semibold">{active}</span> of
                    <span className="font-semibold">
                      {" "}
                      {totalPagesforPrevious}
                    </span>
                  </span>

                  <Button
                    variant="text"
                    color="gray"
                    className="flex items-center sm:gap-2 p-1 sm:p-4"
                    onClick={() => next(active + 1)}
                    disabled={active === totalPagesforPrevious}
                  >
                    <div className="border border-gray-300 rounded-md p-2">
                      <HiChevronRight className="text-secondary text-[20px]" />
                    </div>
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
      {tabMenu === "success" && (
        <div className="mt-5">
          <div className="mx-5 flex">
            <button
              className={`text-xl rounded-tl-lg text-secondary rounded-tr-lg `}
              onClick={() => {
                setTabMenu("pending")
                setSelectedOption(null)
              }}
            >
              Förfrågningar
            </button>
            <button
              className={`text-xl ml-2 rounded-tl-lg text-secondary rounded-tr-lg`}
              onClick={() => {
                setTabMenu("previous")
                setSelectedOption(null)
              }}
            >
              Tidigare beställningar
            </button>
            <button
              className={`text-xl px-5 rounded-tl-lg text-secondary rounded-tr-lg ${tabMenu === "success" &&
                " m-2 border-t-2 border-l-2 border-r-2 text-[#D6B27D] border-[#FFE5D8]"
                }`}
              onClick={() => {
                setTabMenu("success")
              }}
            >
              Godkända
            </button>
          </div>
          <Drawer
            placement="right"
            size={699}
            open={openRight}
            onClose={closeDrawerRight}
            className="px-4 h-screen"
          >
            <div className="mb-6 md:ml-[600px] ml-[300px] overflow-y-scroll flex items-center justify-between mt-5">
              <IconButton
                variant="text"
                color="blue-gray"
                onClick={closeDrawerRight}
              >
                <XMarkIcon strokeWidth={2} className="h-5 w-5" />
              </IconButton>
            </div>
            <div className="md:px-12 px-2">
              <h2 className="font-bold text-2xl text-golden text-center">
                {orderActive?.companyName}
              </h2>
              <p className="font-bold text-sm text-secondary pb-8 mb-6 border-b-[1px] border-black text-center">
                Order #{orderActive?.orderNumber}
              </p>
              <p className="mb-4 text-secondary">
                {orderActive?.orderDetails.length} items
              </p>
              <section className="pb-6 mb-8 border-dotted border-b-2 border-[#e3e3e3]">
                {orderActive?.orderDetails.map((item: any) => (
                  <div className="flex flex-row justify-between items-center w-full mb- text-secondary">
                    <span className="font-semibold">
                      {item.itemName} {item.quantity}
                    </span>
                    {/* {click && share === item.itemId && successNotify('Link Copied')} */}
                    {/* <button onClick={() => handleShareClickForDrawer(item, orderActive.supplierId)} className="hidden sm:flex rounded-full p-3 hover:bg-[#dbd4d4ef]">
                      <img src={shareIcon} className="h-5 w-5 text-[#D6B27D] z-40" />                   
                       </button> */}

                    {/* <Tooltip className='bg-[#D6B27D] z-[9999]' content="Share" placement="bottom"> */}
                    <div
                      onClick={() =>
                        handleShareClickForDrawer(item, orderActive.supplierId)
                      }
                      className="hidden sm:flex rounded-full p-3 hover:bg-[#dbd4d4ef] "
                    >
                      <img
                        src={shareIcon}
                        className="h-5 w-5 text-golden cursor-pointer"
                      />
                    </div>
                    {/* </Tooltip> */}

                    <span className="text-gray-600 text-lg font-medium">
                      moms/item -{" "}
                      {item.itemCategory === "EVENT"
                        ? Math.round(
                          (orderActive?.taxes / orderActive?.totalItemPrice) *
                          100
                        )
                        : item.momsPercentagePerItem}{" "}
                      %
                    </span>
                    <span className="text-gray-600 text-lg font-medium ">
                      {item.price} SEK
                    </span>
                  </div>
                ))}
              </section>
              <section className="pb-8 mb-6 border-b-[1px] border-black">
                <div className="flex flex-row justify-between items-center w-full mb-2">
                  <span className="font-medium text-secondary">Item Total</span>
                  <span className="text-gray-600 text-lg font-medium">
                    {orderActive?.totalAmount} SEK
                  </span>
                </div>
                <div className="flex flex-row justify-between items-center w-full mb-2">
                  <span className="text-gray-600 font-medium">MOMS</span>
                  <span className="text-gray-600 text-lg font-medium">
                    {orderActive?.taxes} SEK
                  </span>
                </div>
              </section>
              <div className="flex flex-row justify-between items-center w-full mb-2">
                <span className="text-secondary font-bold">BILL TOTAL</span>
                <span className="text-secondary font-bold">
                  {orderActive?.totalItemPrice} SEK
                </span>
              </div>
            </div>
          </Drawer>
          {ratingModal && (
            <RatingPopup
              orderValue={orderValue}
              // setIsLoading={setIsLoading}
              setRatingModal={handlePopupClose}
              ratingValue={ratingValue}
            // ratings={ratings}
            />
          )}

          <Drawer
            placement="right"
            size={699}
            open={openChat}
            onClose={closeDrawerChat}
            className="px-4 h-screen"
          >
            <div className="mb-6 flex justify-start items-center">
              <IconButton
                variant="text"
                color="blue-gray"
                onClick={closeDrawerChat}
              >
                <XMarkIcon strokeWidth={2} className="h-5 w-5" />
              </IconButton>
              <div className="flex">
                <h1 className="font-bold md:ml-56 ml-28 text-lg text-[#D6B27D]">
                  {orderActive?.companyName}
                </h1>
              </div>
            </div>
            <div>
              <h2 className="font-bold text-lg py-4 text-secondary">
                Order No : {orderActive?.orderNumber}
              </h2>
            </div>
            <div className="px-4 h-[55vh] max-h-[55vh] w-full overflow-y-scroll border-[1px] border-[#e3e3e3] rounded-md">
              {orderMessages.length > 0 ? (
                messageDatewise(orderMessages).map((message: any) =>
                  !message.isCustomerMessage ? (
                    <div className="flex flex-row justify-between items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md">
                      <section className="w-3/4">
                        <div className="flex flex-row items-center">
                          {message.supplier?.profileImage ? (
                            <img
                              className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                              src={message.supplier?.profileImage}
                            ></img>
                          ) : (
                            <span className="rounded-full p-2 px-4 font-semibold mx-2 bg-[#D6B27D] text-white">
                              {message.supplier?.firstName
                                .split("")
                                .at(0)
                                ?.toUpperCase() +
                                "" +
                                message.supplier?.lastName
                                  .split("")
                                  .at(0)
                                  ?.toUpperCase()}
                            </span>
                          )}
                          <p className="text-golden font-medium mt-1.5">
                            {message?.supplier?.firstName}{" "}
                            {message?.supplier?.lastName}
                          </p>
                        </div>
                        <h4 className="text-sm text-left">
                          {message?.message}
                        </h4>
                      </section>
                      <span className="w-1/4 ml-8 text-sm">
                        {moment(message?.createdDate).format(
                          "ddd, MMM DD YYYY"
                        )}
                      </span>
                    </div>
                  ) : (
                    <div className="flex flex-row items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md w-full">
                      <span className="w-1/4 text-sm text-secondary">
                        {moment(message?.createdDate).format(
                          "ddd, MMM DD YYYY"
                        )}
                      </span>
                      <section className="w-3/4">
                        <div className="flex justify-end">
                          <p className="text-golden font-medium mt-1.5">
                            {message?.customer?.firstName}{" "}
                            {message?.customer?.lastName}
                          </p>

                          {message.customer?.profileImage ? (
                            <img
                              className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                              src={message.customer?.profileImage}
                            ></img>
                          ) : (
                            <span className="h-12 w-12 p-2 rounded-md font-semibold bg-[#D6B27D] text-white">
                              {message.customer?.firstName
                                .split("")
                                .at(0)
                                ?.toUpperCase() +
                                "" +
                                message.customer?.lastName
                                  .split("")
                                  .at(0)
                                  ?.toUpperCase()}
                            </span>
                          )}
                        </div>
                        <h4 className="text-sm text-right">
                          {message?.message}
                        </h4>
                      </section>
                    </div>
                  )
                )
              ) : (
                <div className="justify-center py-4 px-44 text-golden">
                  Inga meddelanden
                </div>
              )}
            </div>
            <div className="pt-10 w-full">
              <textarea
                placeholder="Dina kommentarer..."
                value={newComment}
                onChange={handlenewcomment}
                className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md"
                name="comment"
                id="comment"
                rows={3}
              />
            </div>
            <div className="w-full">
              <button
                onClick={addComment}
                className="bg-golden outline-none w-full mt-6 cursor-pointer text-white font-semibold rounded-md px-3 text-lg py-2"
              >
                SKICKA
              </button>
            </div>
          </Drawer>
          <div className="mx-2">
            <div className="md:px-10 px-4 w-full h-[85vh] max-h-[85vh] overflow-y-auto sm:w-full my-2">
              {/* <p className="font-[kammerlander] font-bold text-3xl">Previous Orders</p> */}
              {/* <p>Kommande festligheter</p> */}
              {successPageData.length > 0 ? (
                successPageData?.map((order: any) => (
                  <div className="border-[1px] rounded-md shadow-xl p-4 my-10 flex flex-col sm:flex-row justify-between items-center w-full">
                    <section className="sm:w-auto w-full">
                      <img
                        className="w-[152px] h-[152px] object-cover rounded-md cursor-pointer group "
                        src={order.image}
                        onClick={() => navigateToOrderDetails(order)}
                      />
                    </section>

                    <section className="sm:w-3/4 w-full ">
                      <div className="">
                        <p className="font-bold text-xl font-[sofiapro] text-secondary xl:-ml-6 md:ml-4 lg:ml-4">
                          {order.orderTitle.toUpperCase()}
                        </p>
                        <span className="  text-secondary font-medium items-center rounded-md xl:-ml-6 md:ml-4 lg:ml-4">
                          {`Order# ${order.orderNumber}`}
                        </span>

                        {/* <button className="bg-[#FFC12320] rounded-xl md:rounded-full ml-3 font-medium w-auto h-8 md:w-30 md:h-10 lg:w-40 lg:h-14 p-2 text-[10px] md:text-[16px] text-[#D6B37D]">{`Order Number # ${order.orderNumber}`}</button> */}
                      </div>

                      <span className=" text-secondary text-sm rounded-md py-2 xl:-ml-6 md:ml-4 lg:ml-4">
                        {moment(order.orderDate).format("ddd, MMM DD YYYY")}
                      </span>
                      <br />

                      {/* <p className='font-medium text-lg mb-6'><span className='font-bold'>Price</span> : {order.totalAmount} kr</p> */}
                      {/* {click && share === order.orderNumber && successNotify('Link Copied')} */}
                      {/* <div> {share === order.orderNumber && <p className="text-white mx-10 bg-black h-8 w-32 rounded-md flex items-center justify-center mb-6"> Link Copied!</p>}</div> */}

                      <div className="flex justify-start items-center xl:-ml-10 md:ml-1 lg:ml-1">
                        <span
                          onClick={() => openDrawerChat(order)}
                          className="sm:mx-4 text-golden font-medium text-[20px] cursor-pointer mt-4 sm:mt-0"
                        >
                          Chatt
                        </span>

                        <span
                          onClick={() => {
                            setIsModalOpen(true)
                            setFeedBackData({
                              bookingItem: order.id
                            })
                          }}
                          className="sm:mx-4 text-golden font-medium text-[20px] cursor-pointer mt-4 sm:mt-0"
                        >
                          Respons
                        </span>
                        {/* <span
                          onClick={() => handlerepeatorder(order.orderDetails)}
                          className="sm:mx-4 text-[#c48c39] text-[20px] font-medium cursor-pointer mt-4 sm:mt-0"
                        >
                          Beställ igen
                        </span> */}
                        <button
                          type="button"
                          // onClick={() => openDrawerRight(order)}
                          onClick={() => navigateToPeningOrderDetails(order)}
                          className="sm:mx-4 text-golden font-medium cursor-pointer mt-4 sm:mt-0 text-[20px]"
                        >
                          Visa detaljer
                        </button>
                        {/* <button
                          type="button"
                          onClick={() => openModal(order)}
                          className="sm:mx-4 text-[#c48c39] font-medium cursor-pointer mt-4 sm:mt-0  text-[20px]"
                        >
                          Betyg
                        </button> */}
                        {order.orderDetails && order.orderDetails.length < 2 ? (
                          <div className="relative group mb-2">
                            <div className="">
                              <button
                                type="button"
                                onClick={() => handleShareClick(order)}
                                className="sm:mx-4 text-golden font-medium text-[20px] cursor-pointer flex"
                              >
                                {/* <Tooltip className='bg-[#D6B27D] z-[9999]' content="Share" placement="right"> */}

                                <div className="flex">
                                  <div className="relative">
                                    <div className="hover:bg-[#ebe2e2ef] rounded-full  p-2 mt-1 transition-all duration-200">
                                      <img
                                        src={shareic}
                                        className="h-5 w-5 text-golden flex items-center z-40"
                                        alt="Share Icon"
                                      />
                                    </div>
                                  </div>
                                  {/* <p className="flex items-center ml-1 mt-2">Share</p> */}
                                </div>
                                {/* </Tooltip> */}
                              </button>
                            </div>
                          </div>
                        ) : null}
                      </div>
                    </section>

                    <div className="flex flex-col">
                      {/* <div>
                {share && (
                  <>
                    {" "}
                    <div className="bg-white text-[#c48c39]">Link Copied!</div>
                  </>
                )}
              </div> */}
                      {/* <div className="flex"> */}
                      {/* <FiShare2
                  className="text-[20px] cursor-pointer mx-5"
                  onClick={() => {
                    checkShare(order)
                  }}
                /> */}
                      {/* <section className="bg-gray-300 font-semibold text-white rounded-xl text-xs py-1 px-2 top-0 right-0">
                      {order.orderDetails[0]?.itemCategory}
                    </section>
                  </div> */}
                    </div>
                  </div>
                ))
              ) : (
                <p className="text-gray-600 text-xl text-center justify-center m-60">
                  No prevoius orders to show
                </p>
              )}
              <div className="sm:flex sm:flex-row flex-col sm:justify-between justify-center py-4">
                <div className="flex justify-center items-center sm:mx-2">
                  <span className="text-golden text-md ">
                    <span className="font-bold">
                      {active * perPage + 1 - 10}{" "}
                    </span>
                    <span className="">to</span>{" "}
                    <span className="font-bold">{active * perPage}</span>
                    <span> of </span>{" "}
                    <span className="font-bold">
                      {totalPagesforPrevious * perPage}
                    </span>
                  </span>
                </div>
                <div className="flex justify-center items-center sm:gap-4">
                  <Button
                    variant="text"
                    color="gray"
                    className="flex items-center sm:gap-2 p-1 sm:p-4"
                    onClick={() => prev(active - 1)}
                    disabled={active === 1}
                  >
                    <div className="border border-gray-300 rounded-md p-2">
                      <HiChevronLeft className="text-secondary text-[20px]" />
                    </div>
                  </Button>
                  <span className="text-golden text-md">
                    Sida <span className="font-semibold">{active}</span> of
                    <span className="font-semibold">
                      {" "}
                      {totalPagesforPrevious}
                    </span>
                  </span>
                  <Button
                    variant="text"
                    color="gray"
                    className="flex items-center sm:gap-2 p-1 sm:p-4"
                    onClick={() => next(active + 1)}
                    disabled={active === totalPagesforPrevious}
                  >
                    <div className="border border-gray-300 rounded-md p-2">
                      <HiChevronRight className="text-secondary text-[20px]" />
                    </div>
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  )
}

export default OrdersDone
