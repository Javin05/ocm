import deleteIcon from "../../assets/Delete.png";
import RatingBar from "../../Components/RatingBar/RatingBar";
import "react-accessible-accordion/dist/fancy-example.css";
import {
  Accordion,
  AccordionHeader,
  AccordionBody,
} from "@material-tailwind/react";
import { useEffect, useState } from "react";
import { commonAxios } from "../../Axios/service";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import { RootState } from "../../Redux/store";
import { useSelector } from "react-redux";

const RatingPopup = ({
  orderValue,
  setIsLoading,
  setRatingModal,
  onAccordionChange,
  ratingValue,
  ratingModal
}: any) => {
  const [selected, setSelected] = useState<any>();
  const [description, setDescription] = useState<string>("");
  const [activeIndex, setActiveIndex] = useState<any>();
  const [rating, setRating] = useState<any>();
  const [ratingsStar, setRatingsStar] = useState<any>();
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [open, setOpen] = useState(0);
  const [isFocused, setIsFocused] = useState(false);
  // setIsLoading(false);
  const handleclose = () => {
    setRatingModal(false);
    document.body.classList.remove('no-scroll');
  };


  useEffect(() => {
    if (ratingValue) {
      setDescription(ratingValue.description)
    }
  }, [])

  useEffect(() => {
    if (ratingModal) {
      document.body.classList.add('no-scroll');
    } else {
      document.body.classList.remove('no-scroll');
    }
  }, [ratingModal]);


  const handleSubmit = async (
    val: any,
    bindedRatings: any,
    bindedDescription: any
  ) => {
    // setIsLoading(true);
    let payload;
    if (val.itemCategory === "PRODUCT") {
      payload = {
        productId: val.itemId,
        rating: rating ? rating : bindedRatings,
        customerId: userInfo.id,
        descriptions: description,
        eventId: null,
      };
    } else {
      payload = {
        productId: null,
        rating: rating,
        customerId: userInfo.id,
        descriptions: description,
        eventId: val.itemId,
      };
    }

    const response = await commonAxios.post(`cemo/comments/add`, payload);

    if (response.data) {
      setRatingModal(false);
      successNotify("Thankyou for your reviews");
    } else {
      setRatingModal(false);
      failureNotify("Oops! something went wrong!");
    }
  };

  const changeStarsValue = (value: any) => {
    setRating(value);
  };

  const handleOpen = (value: any) => {
    setOpen(open === value ? 0 : value);
    orderValue.orderDetails.map(async (val: any, i: any) => {
      if (open === i + 1) {
        let response;
        if (val.itemCategory === "PRODUCT") {
          response = await commonAxios.get(
            `/cemo/ratings/${userInfo?.id}?productId=${val.itemId}`
          );

          setRatingsStar(response?.data[0]?.ratings);
        } else {
          response = await commonAxios.get(
            `/cemo/ratings/${userInfo?.id}?eventId=${val.itemId}`
          );

          setRatingsStar(response?.data[0]?.ratings);
        }
      }
    });
  };

  const handleFocus = () => {
    setIsFocused(true);
  };

  const handleBlur = () => {
    setIsFocused(false);
  };
  const getUpdatedReviews = (values: any) => {
    setRating(values);
  };

  useEffect(() => {
    orderValue.orderDetails.map((val: any, index: any) => {
      let ratingValues = ratingValue.find(
        (productid: any) => productid.product.id === val.itemId
      );
      setDescription(ratingValues?.descriptions);
    });
  }, []);

  return (
    <div className="w-full h-screen bg-[#00000080] fixed top-0 z-40">
      <section className="w-full h-full flex items-center justify-center -ml-52">
        <div className="bg-white rounded-lg border-2 border-[#D7B480] flex flex-col sm:flex-row md:w-[570px] w-100px relative px-4">
          <button onClick={handleclose}>
            <img src={deleteIcon} className="absolute top-2 right-2 h-6 w-6" />
          </button>
          <div className="w-full py-5">
            {orderValue.orderDetails.map((val: any, index: any) => {
              let ratingValues = ratingValue.find(
                (productid: any) => productid.product.id === val.itemId
              );
              return (
                <>
                  {/* <Accordion
                    open={open === index + 1}
                    className=""
                  >
                    <AccordionHeader onClick={() => handleOpen(index + 1)}>
                      {val.itemName}
                    </AccordionHeader>
                    <AccordionBody> */}
                  <div className="flex flex-col">
                    <p className="font-semibold mb-2 ">Betyg</p>
                    {ratingValues?.descriptions || ratingValues?.ratings ?
                      <RatingBar
                        ratings={ratingValues && ratingValues.ratings}
                        handleChange={getUpdatedReviews}
                        readonly={true}
                      /> :
                      <RatingBar
                        ratings={ratingValues && ratingValues.ratings}
                        handleChange={getUpdatedReviews}
                      />}
                    <div className="p-2"> {/* Add padding to create a gap */}
                      <textarea
                        placeholder="Description (optional)"
                        onChange={(e) => setDescription(e.target.value)}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        value={description}
                        className={
                          isFocused
                            ? "border-2 border-[#D6B27D] rounded-lg w-full mt-4 p-2"
                            : "border-2 border-[#D6B27D] mt-4 rounded-lg w-full p-2"
                        }
                        disabled={ratingValues?.descriptions || ratingValues?.ratings}
                      />
                    </div>
                  </div>
                  <div className="flex justify-center mt-4">
                    <button
                      onClick={() =>
                        handleSubmit(
                          val,
                          ratingValues?.ratings,
                          ratingValues?.descriptions
                        )
                      }
                      className="bg-[#D6B27D] text-white  p-2 uppercase font-semibold rounded-lg mt-5"
                      disabled={ratingValues?.descriptions || ratingValues?.ratings}
                    >
                      Lägg till
                    </button>
                  </div>

                  {/* </AccordionBody>
                  </Accordion> */}
                </>
              );
            })}
          </div>
        </div>
      </section>
    </div>
  );
};
export default RatingPopup;
