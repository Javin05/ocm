import React, { useContext, useEffect, useState } from "react";
import { Link, useLocation, useNavigate, useSearchParams } from "react-router-dom";
import OrdersDone from "./OrdersDone";
import callIcon from "../../assets/goldcall.png";
import supportIcon from "../../assets/call.svg";
import Wishlist from "./Wishlist";
import goldQuotation from "../../assets/goldQuotation.png";
import quotation from "../../assets/greyQuotation.png";
import Loader from "../../Components/Loader/Loader";
import MyProfile from "./MyProfile";
import PendingOrders from "./PendingOrders";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { setUserDetails } from "../../Redux/features/userSlice";
import { successNotify } from "../../Components/Toast/Toast";
import { clearAll } from "../../Utils/auth";
import { ImProfile } from "react-icons/im"
import cartIcon from "../../assets/cartIcon.png";
import wishlistIcon from "../../assets/favourite.png";
import { AiOutlineLogout } from "react-icons/ai"
import { AuthContext } from "../RootLayout/RootLayout";
import orderMenuIcon from "../../assets/orderListActive.svg";
import orderListIcon from "../../assets/orderMenu.svg";
import greyperson from "../../assets/empcontact.png";
import contact from "../../assets/contact.png";
import heartIcon from "../../assets/icons8-heart-32.png";
import heartIcons from "../../assets/greyheart.png";
import logout from "../../assets/icons8-logout-64.png";
import logouts from "../../assets/icons8-logout-96.png"
import ContactAdmin from "../CustomerChat/ContactAdmin";
import Contactsupplier from "../CustomerChat/ContactSupplier";
import QuotationList from "../QuotationList/QuotationList";
import CustomerQuotationList from "../QuotationList/CustomerQuotationList";

const OrderHistory = () => {
  const [searchParams] = useSearchParams();
  const [activeMenu, setActiveMenu] = useState("Orders");
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const location = useLocation();
  const dispatch = useDispatch();
  const { setIsDeleteEventModalOpen, setMenuClicked, menuClicked } = useContext(AuthContext);
  const pathName = window.location.pathname;

  const locations = useLocation();
  const { state } = locations;

  useEffect(() => {
    if (state && state.key.isSupplierMessage) {
      setActiveMenu("contactsupplier");
    } else {
      setActiveMenu("contactadmin")
    }
  }, [locations])

  console.log(menuClicked, "menuClicked");

  useEffect(() => {
    if (searchParams.get("activeMenu")) {
      setActiveMenu(searchParams.get("activeMenu") as string);
    }
  }, [location, locations]);

  const handleLogout = () => {
    setMenuClicked("");
    setIsDeleteEventModalOpen(true);
    window.scrollTo(0, 0);

  }

  const navigateAdminNotofication = () => {
    setActiveMenu("contactadmin")

  }
  const navigatesupplierNotofication = () => {
    setActiveMenu("contactsupplier")
  }

  return (
    <div className="w-full h-screen flex flex-row">
      <section className="w-[18%] h-full p-3 bg-[#FFE5D8] shadow">
        <div className="mt-5">
          <h1 className="uppercase text-[#92929D] font-black sm:ml-4 mt-8 tracking-[2px] pb-4 text-[10px] md:text-[15px]">
            Kul att du är här!
          </h1>
        </div>
        <ul className="flex flex-col">
          <li
            className={`flex cursor-pointer font-bold sm:px-4 py-3 text-[20px] rounded-md transition-all my-2 ${activeMenu === "Orders"
              ? " text-[#c48c39]"
              : "text-secondary"
              }`}
            onClick={() => setActiveMenu("Orders")}
          >
            <img
              src={activeMenu === "Orders" && !menuClicked ? orderMenuIcon : orderListIcon}
              className="h-[18px] w-[18px]"
            />
            <span
              className={`hidden sm:flex text-[18px] text-secondary font-medium ml-3 -mt-1 ${activeMenu === "Orders" && !menuClicked ? "text-[#D6B27D] font-semibold" : null
                }`}
            >
              Beställningar
            </span>
          </li>
          <li
            className={`flex cursor-pointer font-bold sm:px-3 py-3 text-[20px] rounded-md transition-all my-2 ${activeMenu === "Favourites"
              ? " text-[#c48c39]"
              : "text-secondary"
              }`}
            onClick={() => setActiveMenu("Favourites")}
          >
            <img
              src={activeMenu === "Favourites" && !menuClicked ? heartIcon : heartIcons}
              className="h-[22px] w-[22px]"
            />
            <span
              className={`hidden sm:flex mx-4 text-[18px] text-secondary ml-3 font-medium ${activeMenu === "Favourites" && !menuClicked ? "text-[#D6B27D] font-semibold" : null
                }`}
            >
              Favoriter
            </span>
          </li>
          <li
            className={`flex cursor-pointer font-bold sm:px-3 py-3 text-[20px] rounded-md transition-all my-2 ${activeMenu === "minProfil"
              ? " text-[#c48c39]"
              : "text-secondary"
              }`}
            onClick={() => setActiveMenu("minProfil")}
          >
            <img
              src={activeMenu === "minProfil" && !menuClicked ? contact : greyperson}
              className="h-[24px] w-[24px]"
            />
            <span
              className={`hidden sm:flex mx-3 text-[18px] text-secondary font-medium  ${activeMenu === "minProfil" && !menuClicked ? "text-[#D6B27D] font-semibold" : null
                }`}
            >
              MinProfil
            </span>
          </li>
          <li
            className={`flex cursor-pointer font-bold sm:px-3 py-3 text-[20px] rounded-md transition-all my-2 ${activeMenu === "minProfil"
              ? " text-[#c48c39]"
              : "text-secondary"
              }`}
            onClick={() => setActiveMenu("offerter")}
          >
            <img
              src={activeMenu === "offerter" && !menuClicked ? goldQuotation : quotation}
              className="h-[24px] w-[24px]"
            />
            <span
              className={`hidden sm:flex mx-3 text-[18px] text-secondary font-medium  ${activeMenu === "offerter" && !menuClicked ? "text-[#D6B27D] font-semibold" : null
                }`}
            >
              Offerter
            </span>
          </li>
         
          <li
            className={`flex cursor-pointer font-bold sm:px-3 py-3 text-[20px] rounded-md transition-all my-2 ${activeMenu === "minProfil"
              ? " text-[#c48c39]"
              : "text-secondary"
              }`}
            onClick={() => setActiveMenu("contactadmin")}
          >
            <img src={activeMenu === "contactadmin" ? callIcon : supportIcon}
              className="h-[26px] w-[26px]" />
            <span
              className={`hidden sm:flex mx-3 text-[18px] text-secondary font-medium  ${activeMenu === "contactadmin" && !menuClicked ? "text-[#D6B27D] font-semibold" : null
                }`}
            >
              Kontakta Admin
            </span>
          </li>


          <li
            className={`flex cursor-pointer font-bold sm:px-3 py-3 text-[20px] rounded-md transition-all my-2 ${activeMenu === "contactsupplier"
              ? " text-[#c48c39]"
              : "text-secondary"
              }`}
            onClick={() => setActiveMenu("contactsupplier")}
          >
            <img src={activeMenu === "contactsupplier" ? callIcon : supportIcon}
              className="h-[26px] w-[26px]" />
            <span
              className={`hidden sm:flex mx-3 text-[18px] text-secondary font-medium  ${activeMenu === "contactsupplier" && !menuClicked ? "text-[#D6B27D] font-semibold" : null
                }`}
            >
              Kontakta Supplier
            </span>
          </li>

          <li
            className={`flex cursor-pointer font-bold sm:px-3 py-3 text-[20px] rounded-md transition-all my-2 text-secondary${pathName === "/logout"
              ? " text-[#c48c39]"
              : "text-secondary"
              }
            `}
            onClick={() => handleLogout()}
          >
            <img
              src={menuClicked ? logout : logouts}
              className="h-[24px] w-[24px] -ml-1"
            />
            <span
              className={`hidden sm:flex mx-4 text-[18px] text-secondary font-medium cursor-pointer ml-3 ${menuClicked && "text-[#D6B27D] font-semibold "
                }`}
            >
              Logga ut
            </span>
          </li>
        </ul>
      </section>
      <section className="w-[85%]">
        {activeMenu === "Favourites" && (
          <Wishlist />
        )}
        {activeMenu === "Orders" && <OrdersDone />}
        {activeMenu === "minProfil" && (
          <MyProfile />
        )}
        {activeMenu === "offerter" && <CustomerQuotationList/>} 
        {activeMenu === "contactadmin" && <ContactAdmin notification={navigateAdminNotofication} />}
        {activeMenu === "contactsupplier" && <Contactsupplier notification={navigatesupplierNotofication} />}

      </section>
    </div>
  );
};

export default OrderHistory;
