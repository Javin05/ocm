import React, { useEffect, useState } from "react";
import { commonAxios } from "../../Axios/service";
import { RootState } from "../../Redux/store";
import Loader from "../../Components/Loader/Loader";
import { useDispatch, useSelector } from "react-redux";
import { failureNotify } from "../../Components/Toast/Toast";
import { addProductToCart } from "../../Redux/features/productCartSlice";
import { Drawer, IconButton } from "@material-tailwind/react";
import { XMarkIcon } from "@heroicons/react/24/outline";
import moment from "moment";
import { useNavigate } from "react-router-dom";
import { setNotification } from "../../Redux/features/notificationSlice";

export interface OrderHistory {
  orderTitle: string;
  orderNumber: string;
  companyName: string;
  image: string;
  orderDate: string;
  orderDetails: OrderDetail[];
  totalItemPrice: number;
  taxes: number;
  totalAmount: number;
  supplierId: string;
}

export interface OrderDetail {
  itemName: string;
  quantity: number;
  price: number;
  itemId: string;
  itemCategory: "PRODUCT" | "EVENT";
  eventDate: string;
  slotTime: string;
  momsPercentagePerItem: number;
}
export interface OrderMessages {
  id: string;
  booking: Booking;
  customer: AppUser;
  message: string;
  isRead: boolean;
  createdDate: string;
  updatedDate: string;
}

export interface Booking {
  id: string;
  bookingDate: string;
  bookingTime: string;
  memberCount: any;
  firstName: string;
  lastName: string;
  address: string;
  postalCode: number;
  category: string;
  orderNumber: string;
  bookingStatus: string;
  payment: Payment;
  overAllVATAmount: number;
  overAllTotalAmount: number;
  paymentType: string;
  appUserId: any;
}

export interface Payment {
  id: string;
  paymentType: string;
  cardNumber: any;
  expiryDate: any;
  country: any;
  cw: any;
  zipCode: any;
  amount: number;
  paymentMethod: string;
  paymentStatus: string;
  transactionDate: string;
  curreny: string;
  paymentId: string;
}

export interface AppUser {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
  profileImage: string;
}

const PendingOrders = ({ setIsLoading }: any) => {
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [loader, setLoader] = useState(false);
  const [pendingOrders, setPendingOrders] = useState([]);
  const [orderHistory, setOrderHistory] = useState<OrderHistory[]>([]);
  const [orderActive, setOrderActive] = useState<OrderHistory>();
  const [Booking, setOrderBooking] = useState<Booking[]>([]);
  const [orderPayment, setOrderPayment] = useState<Booking>();
  const [openRight, setOpenRight] = React.useState(false);
  const [openChat, setOpenChat] = React.useState(false);
  const [orderMessages, setOrderMessages] = useState<OrderMessages[]>([]);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const getOrderMessages = async (order: OrderHistory) => {
    try {
      const response = await commonAxios.get(
        `/cemo/messages?orderNumber=${order?.orderNumber}`
      );

      if (response.data) {
        let updatedCustomerMessages = response.data.filter((val: any) => {
          return val.isCustomerRead === false;
        });
        let updatedMessagesId = updatedCustomerMessages.map((val: any) => {
          return val.id;
        });

        let payload = {
          id: updatedMessagesId,
          customerId: userInfo.id,
          supplierId: null,
        };

        let updatedMessageResponse = await commonAxios.put(
          `/cemo/messages/update/status`,
          payload
        );

        if (updatedMessageResponse.data) {
          try {
            const messageCount = await commonAxios.get(
              `cemo/appuser/notification/count?supplierId=&customerId=${userInfo.id}`
            );
            if (messageCount.data) {
              dispatch(setNotification(messageCount.data));
            }
          } catch (e) { }
        }
      }
      setOrderMessages(response.data);
    } catch (error) { }
  };

  const navigateToPeningOrderDetails = (order: any) => {
    navigate(`/pendingOrderDetails?orderNumber=${order.orderNumber}`, {
      state: order,
    });
  };

  const openDrawerRight = (order: OrderHistory) => {
    setOrderActive(order);
    setOpenRight(true);
  };
  const closeDrawerRight = () => {
    setOrderActive(undefined);
    setOpenRight(false);
  };

  const openDrawerChat = (order: OrderHistory) => {
    setOrderActive(order);
    getOrderMessages(order);
    setOpenChat(true);
  };
  const closeDrawerChat = () => {
    setOrderActive(undefined);
    setOpenChat(false);
  };
  const handlerepeatorder = async (order: OrderDetail[]) => {
    for await (const singleOrder of order) {
      const updatedProduct = {
        appUserId: userInfo.id,
        productId: singleOrder.itemId,
        bookingItemType: "PRODUCT",
        bookingDate: new Date()
          .toLocaleDateString()
          .split("/")
          .reverse()
          .join("-"),
        quantity: 1,
      };
      try {
        const response = await commonAxios.post(
          "/cemo/bookingitems/new",
          updatedProduct
        );

        dispatch(addProductToCart());
      } catch (error) {
        failureNotify("oops! something went wrong, Try again");
      }
    }
  };
  const [newComment, setNewComment] = useState("");

  const handlenewcomment = (e: any) => {
    setNewComment(e.target.value);
  };

  const addComment = async () => {
    try {
      const payload = {
        orderNumber: orderActive?.orderNumber,
        message: newComment,
        customerId: userInfo.id,
        supplierId: orderActive?.supplierId,
      };
      const response = await commonAxios.post("/cemo/messages", payload);
      if (response.data) {
        getOrderMessages(orderActive!);
        setNewComment("");
      }
    } catch (error) {
      failureNotify("oops! something went wrong");
    }
  };

  const getPendingOrders = async () => {
    try {
      setLoader(true);
      const response = await commonAxios.get(
        `/cemo/bookingitems/pending/order/${userInfo.id}`
      );
      setPendingOrders(response.data);
    } catch (error) {
    } finally {
      setLoader(false);
    }
  };

  useEffect(() => {
    getPendingOrders();
  }, []);

  return (
    <>
      <Drawer
        placement="right"
        size={699}
        open={openRight}
        onClose={closeDrawerRight}
        className="p-4"
      >
        <div className="mb-6 flex items-center justify-between">
          <IconButton
            variant="text"
            color="blue-gray"
            onClick={closeDrawerRight}
          >
            <XMarkIcon strokeWidth={2} className="h-5 w-5" />
          </IconButton>
        </div>
        <div className="px-12">
          <section className="pb-2 mb-6 border-b-[1px] border-black">
            <div className="flex flex-row justify-between items-center ">
              {/* <h2 className='font-bold text-2xl text-[#c48c39]'>{orderPayment?.paymentType}</h2> */}
              <span className="font-bold text-2xl text-[#c48c39]">
                {orderActive?.companyName}
              </span>
              <span className="font-bold text-2xl text-[#c48c39]">
                PaymentType
              </span>
            </div>
            <div className="flex flex-row justify-between items-center w-full mb-2">
              <span className="font-bold text-sm text-[#000000] pb-2 mb-1 border-black">
                Order #{orderActive?.orderNumber}
              </span>
              <span className="font-bold text-sm text-[#000000] pb-2 mb-1 px-16 border-black">
                ONLINE
              </span>
            </div>
          </section>
          {/* <p className='font-bold text-sm text-[#000000] pb-8 mb-6 border-b-[1px] border-black'>Order #{orderActive?.orderNumber}</p> */}
          <p className="mb-4">{orderActive?.orderDetails.length} items</p>
          <section className="pb-6 mb-8 border-dotted border-b-2 border-[#e3e3e3]">
            {orderActive?.orderDetails.map((item: OrderDetail) => (
              <div className="flex flex-row justify-between items-center w-full mb-2">
                <span className="font-semibold">
                  {item.itemName} X {item.quantity}
                </span>
                <span className="text-gray-600 text-lg font-medium">
                  moms/item -{" "}
                  {item.itemCategory === "EVENT"
                    ? (Math.round((orderActive?.taxes / orderActive?.totalItemPrice) * 100))
                    : item.momsPercentagePerItem}{" "}
                  %
                </span>
                <span className="text-gray-600 text-lg font-medium">
                  {item.price} SEK
                </span>
              </div>
            ))}
          </section>
          <section className="pb-8 mb-6 border-b-[1px] border-black">
            <div className="flex flex-row justify-between items-center w-full mb-2">
              <span className="font-medium">Item Total</span>
              <span className="text-gray-600 text-lg font-medium">
                {orderActive?.totalItemPrice} SEK
              </span>
            </div>
            <div className="flex flex-row justify-between items-center w-full mb-2">
              <span className="text-gray-600 font-medium">MOMS</span>
              <span className="text-gray-600 text-lg font-medium">
                {orderActive?.taxes} SEK
              </span>
            </div>
          </section>
          <div className="flex flex-row justify-between items-center w-full mb-2">
            <span className="text-black font-bold">BILL TOTAL</span>
            <span className="text-black font-bold">
              {orderActive?.totalAmount} SEK
            </span>
          </div>
        </div>
      </Drawer>
      <Drawer
        placement="right"
        size={699}
        open={openChat}
        onClose={closeDrawerChat}
        className="p-4"
      >
        <div className="mb-6 flex items-center justify-between">
          <IconButton
            variant="text"
            color="blue-gray"
            onClick={closeDrawerChat}
          >
            <XMarkIcon strokeWidth={2} className="h-5 w-5" />
          </IconButton>
        </div>
        <div className="px-12 h-[65vh] max-h-[65vh] w-full overflow-y-scroll border-[1px] border-[#e3e3e3] rounded-md">
          {orderMessages.map((message: OrderMessages) => (
            <div className="flex flex-row justify-between items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
              <section>
                <div className="flex flex-row">
                  {message.customer?.profileImage ? (
                    <img
                      className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                      src={message.customer?.profileImage}
                    ></img>
                  ) : (
                    <span className="h-12 w-12 p-2 rounded-md font-semibold bg-[#D6B27D] text-white">
                      {message.customer?.firstName
                        .split("")
                        .at(0)
                        ?.toUpperCase() +
                        "" +
                        message.customer?.lastName
                          .split("")
                          .at(0)
                          ?.toUpperCase()}
                    </span>
                  )}
                  <p className="text-[#D6B27D] font-medium mt-1.5">
                    {message.customer?.firstName}
                  </p>
                </div>
                <h4 className="text-lg">{message.message}</h4>
              </section>
              <span className="text-xs">
                {moment(message.createdDate).format("ddd, MMM DD YYYY")}
              </span>
            </div>
          ))}
        </div>
        <div className="pt-10 w-full">
          <textarea
            placeholder="Dina kommentarer..."
            value={newComment}
            onChange={handlenewcomment}
            className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md"
            name="comment"
            id="comment"
            rows={3}
          />
        </div>
        <div className="w-full">
          <button
            onClick={addComment}
            className="bg-[#FFE5D8] outline-none w-full mt-6 cursor-pointer text-black font-semibold rounded-md px-3 text-lg py-2"
          >
            send
          </button>
        </div>
      </Drawer>

      <div className="px-10 sm:px-0 w-full h-[85vh] max-h-[85vh] overflow-y-scroll sm:w-4/5 sm:mr-auto sm:ms-10 mx-auto my-10">
        {loader && <Loader />}
        <p className="font-medium text-3xl">Väntande konton</p>
        {pendingOrders.length > 0 ? (
          pendingOrders?.map((order: any) => (
            <div className="border-[1px] rounded-md shadow-xl p-4 my-10 flex flex-col sm:flex-row gap-10 justify-between items-center w-full">
              <section className="sm:w-1/4 w-full">
                <img
                  className="w-full h-40 object-cover rounded-md cursor-pointer"
                  src={order.image}
                  onClick={() => navigateToPeningOrderDetails(order)}
                />
              </section>
              <section className="sm:w-3/4 w-full">
                <p className="font-bold text-xl mb-6 font-[sofiapro]">
                  {order.orderTitle}
                </p>
                <div className="flex flex-row justify-start items-center my-4">
                  {/* <span className="bg-[#FFE5D8] sm:mx-4 text-black font-medium rounded-md px-3 py-2">
                  {order.orderDate}
                </span> */}
                  <span className="bg-[#FFE5D8] sm:mx-4 text-black font-medium rounded-md px-3 py-2">
                    {moment(order.orderDate).format("ddd, MMM DD YYYY")}
                  </span>

                  <span
                    onClick={() => openDrawerChat(order)}
                    className="sm:mx-4 text-[#c48c39] font-medium text-lg cursor-pointer"
                  >
                    Chatt
                  </span>
                  <span
                    onClick={() => handlerepeatorder(order.orderDetails)}
                    className="sm:mx-4 text-[#c48c39] font-medium text-lg cursor-pointer"
                  >
                    Beställ igen
                  </span>
                  <button
                    type="button"
                    onClick={() => openDrawerRight(order)}
                    className="sm:mx-4 text-[#c48c39] font-medium text-lg cursor-pointer"
                  >
                    view details
                  </button>
                </div>
              </section>
            </div>
          ))
        ) : (
          <p>No pending orders</p>
        )}
      </div>
    </>
  );
};

export default PendingOrders;
