import { useFormik } from "formik";
import * as Yup from "yup";
import deleteIcon from "../../assets/Delete.png";
import { commonAxios } from "../../Axios/service";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";

const FeedBack = ({ setIsModalOpen, feedBackData }: any) => {
  console.log("feedBackData", feedBackData)
  const formik = useFormik({
    initialValues: {
      FeedBackName: "",
    },
    validationSchema: Yup.object({
      FeedBackName: Yup.string().required("Feedback är obligatoriskt"),
    }),
    onSubmit: async (values, { resetForm }) => {
      const payload = {
        bookingItem : feedBackData?.bookingItem,
        feedBack: values.FeedBackName
      }
      const response = await commonAxios.post("/cemo/feedback/create", payload)
      if (response.data) {
        successNotify(response.data)
        resetForm()
        setIsModalOpen(false)
      }else{
        failureNotify(response.data)
      }
    },
  });

  return (
    <>
      <div className="w-full h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <form
            onSubmit={formik.handleSubmit}
            className="bg-[#ffffff] w-[40rem] h-auto px-8 py-6 relative"
          >
            <button onClick={() => setIsModalOpen(false)}>
              <img src={deleteIcon} className="absolute right-5 h-6 w-6 -mt-6" />
            </button>
            <h2 className="mb-4 text-lg font-semibold text-secondary">Lägg till feedback</h2>

            <div className="mt-4 flex text-secondary">
              <div>
                {/* <label className="mx-4 font-semibold" htmlFor="FeedBackName">
                  Respons
                </label> */}
                <textarea
                  name="FeedBackName"
                  id="FeedBackName"
                  placeholder="Respons"
                  className="mt-3 outline-[#97A0C3] px-4 border-2 py-2 h-32 w-[30rem] rounded-[10px]" // Adjusted height and width
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.FeedBackName}
                />
                {formik.touched.FeedBackName && formik.errors.FeedBackName ? (
                  <div className="text-red-500 mt-3">{formik.errors.FeedBackName}</div>
                ) : null}
              </div>
            </div>
            <div className="flex justify-center">
              <button
                type="submit"
                className="px-6 p-2 w-auto uppercase md:mr-4 bg-[#D6B27D] text-white rounded-lg mt-6 text-md font-semibold cursor-pointer"
              >
                Lägg till
              </button>
            </div>
          </form>
        </section>
      </div>
    </>
  );
};

export default FeedBack;
