import React, { useEffect, useRef, useState } from "react";
import Imagepicker from "../../Components/Imagepicker/Imagepicker";
import ImageShown from "../../Components/Imagepicker/ImageShown";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { useNavigate } from "react-router-dom";
import deleteIcon from "../../assets/Delete.png";
import { formAxios, commonAxios } from "../../Axios/service";
import { successNotify, failureNotify } from "../../Components/Toast/Toast";
import { setUserDetails } from "../../Redux/features/userSlice";
import Loader from "../../Components/Loader/Loader";
import { BiEdit } from "react-icons/bi";
import { Drawer, IconButton, Tooltip } from "@material-tailwind/react";
import { RxCross2 } from "react-icons/rx";


interface UserDetails {
  accountStatus: string;
  businessAddress: string;
  businessCity: string;
  businessName: string;
  businessNumber: string;
  businessPinCode: string;
  categories: string;
  createdAt: string;
  email: string;
  firstName: string;
  lastName: string;
  telephone: string;
  id: string;
  mobile: string;
  resetToken: string;
  updatedAt: string;
  profileImage: string;
}

const MyProfile = (props: any) => {
  const userDetails = useSelector((state: RootState) => state.user.userDetails);
  const [bindedImage, setBindedImage] = useState<any>("");
  const [imagevalue, setImageValue] = useState<any>([]);
  const [loader, setLoader] = useState<Boolean>(false);
  const [base64Image, setBase64Image] = useState<any>([]);
  const dispatch = useDispatch();
  const [isValidEmail, setIsValidEmail] = useState(true);
  const [formEnable, setFormEnable] = useState(true);
  const [editedUserDetails, setEditedUserDetails] = useState<any>({
    accountStatus: "",
    businessAddress: "",
    businessCity: "",
    businessName: "",
    businessNumber: "",
    businessPinCode: "",
    categories: "",
    createdAt: "",
    email: "",
    firstName: "",
    lastName: "",
    telephone: "",
    id: "",
    mobile: "",
    resetToken: "",
    updatedAt: "",
    profileImage: "",
  });

  const handleFileChange = async (event: any) => {
    event.preventDefault();
    const files = event.target.files;
    const fileArray: any = Array.from(files);
    const allowedExtensions = [".jpg", ".jpeg", ".png", ".gif"];

    const allValid = fileArray.every((file: any) => {
      const extension = file.name.toLowerCase().split('.').pop();
      return allowedExtensions.includes(`.${extension}`);
    });

    if (allValid) {
      if (files.length <= 1) {
        // setSelectedFile(files[0]); // Store the selected image
        const promises = fileArray?.map((file: any, i: any) => {
          return new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.onload = (event: any) => {
              const base64String = event.target.result;
              resolve(base64String);
            };

            reader.onerror = (error) => {
              reject(error);
            };

            reader.readAsDataURL(file);
          });
        });
        Promise.all(promises)
          .then((base64Array: any) => {
            const need_image = base64Array?.map((value: any, i: any) => {
              return {
                values: value,
                id: i + 1,
              };
            });
            setBase64Image(need_image);
          })
          .catch((error) => { })
          .finally(() => {
            // onImageUpload(files);
            handleImageUpload(files)
          });
      }
    } else {
      failureNotify("Please upload valid image files (jpg, jpeg, png, gif).");
    }
  };
  function capitalizeFirstLetter(value: any) {
    if (!value) {
      return ''; // Handle empty values
    }
    return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
  }

  const updateProfile = async () => {
    setLoader(true);
    await formAxios.get("/cemo/appuser/" + userDetails.id).then(
      async (response) => {
        try {

          const userData = response.data;
          setEditedUserDetails({
            accountStatus: userData.accountStatus,
            businessAddress: userData.businessAddress,
            businessCity: capitalizeFirstLetter(userData.businessCity),
            businessName: userData.businessName,
            businessNumber: userData.businessNumber,
            businessPinCode: userData.businessPinCode,
            categories: userData.categories,
            createdAt: userData.createdAt,
            email: userData.email,
            firstName: userData.firstName,
            lastName: userData.lastName,
            telephone: userData.telephone,
            id: userData.id,
            mobile: userData.mobile,
            resetToken: userData.resetToken,
            updatedAt: userData.updatedAt,
            profileImage: userData.profileImage,
          });
          setBindedImage([{ id: 1, image: userData.profileImage }]);
        } catch (error) {

        } finally {
          // setLoader(false);
        }
      },
      (error) => {

        failureNotify("Error Occured while Upload Profile image");
      }
    );
    // setLoader(false);
  };

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    if (name === "email") {
      // Email pattern validation using regex
      const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      setIsValidEmail(emailPattern.test(value));
    }
    setEditedUserDetails((prevUserDetails: any) => ({
      ...prevUserDetails,
      [name]: value,
    }));
  };

  const updatedPic = (image: any) => {
    setBindedImage(image);
  };

  const handleImageUpload = (image: any) => {
    setImageValue(image);
  };

  const conversionImage = (value: any) => {
    setBase64Image(value);
  };

  const removeImages = () => {
    setImageValue([]);
  }

  const saveProfileChanges = async () => {
    const formData = new FormData();
    let fileArray: string[] = [];
    for (let i = 0; i < imagevalue.length; i++) {
      fileArray.push(imagevalue[i]);
    }
    fileArray.forEach((file: any, i: any) => {
      formData.append(`file`, file);
    });
    formData.append("folder", "user-profile");
    if (fileArray.length != 0) {
      await formAxios.post("/cemo/upload/files", formData).then(
        async (data) => {
          try {
            editedUserDetails.profileImage = data.data[0];
            editedUserDetails.mobile = editedUserDetails.mobile;
            const response = await commonAxios.put(
              `/cemo/appuser/update/` + userDetails.id,
              editedUserDetails
            );

            if (response.data) {

              if (response.data.role === "ROLE_CUSTOMER") {
                successNotify("Profile update successfully!!!");
                setBase64Image([]);
                setBindedImage([])
                setImageValue([])
                dispatch(setUserDetails(response.data));
                setFormEnable(true)

              }
            }
          } catch (error) {

          } finally {
          }
        },
        (error) => {

          failureNotify("Error Occured while Upload Profile image");
        }
      );
    } else {
      editedUserDetails.mobile = editedUserDetails.mobile;
      if (bindedImage.length === 0 && imagevalue.length === 0 && base64Image.length === 0) {
        delete editedUserDetails?.profileImage;
      }
      await commonAxios
        .put(`/cemo/appuser/update/` + userDetails.id, editedUserDetails)
        .then(
          (res) => {
            if (res.data) {

              successNotify("Profile update successfully!!!");
              setBindedImage([])
              setImageValue([]);
              setBase64Image([]);
              dispatch(setUserDetails(res.data));
              setFormEnable(true)
            }
          },
          (err) => {
            failureNotify("Profile update failed !!!");
          }
        );
    }
  };

  useEffect(() => {
    updateProfile();
  }, [formEnable]);

  const editProfileChanges = () => {
    setFormEnable(false);
  }

  return (
    <div className="md:px-5 px-4">
      {loader && <>
        <svg className="animate-spin h-5 w-5 mr-3 ..." viewBox="0 0 24 24">
        </svg>
      </>}

      <div className="flex justify-between items-center">
        {/* <h2 className="text-3xl font-bold uppercase font-[sofiapro] mb-2 mt-4 text-secondary">Redigera produkt</h2> */}
        <p className="font-[sofiapro] font-bold text-3xl text-secondary  pt-2">Redigera profile</p>

        <span className="bg-[#D6B37D] h-[28px] w-[28px] cursor-pointer font-bold flex justify-center items-center rounded-md">
          <Tooltip className="bg-[#D6B37D] " content="Edit" placement="bottom">
            <div>
              <BiEdit
                onClick={editProfileChanges}
                className="w-[22px] h-[22px] text-white transition-all"
              />
            </div>
          </Tooltip>
        </span>
      </div>
      {!formEnable ?
        <form className="flex flex-col h-full w-full border-2 px-4 md:px-8 py-2 rounded-lg">
          <div className="mb-4 w-full mt-5">
            <label htmlFor="firstName" className="font-extrabold block mb-1 text-secondary">
              Förnamn:
            </label>
            <input
              type="text"
              id="firstName"
              name="firstName"
              disabled={formEnable}
              value={editedUserDetails.firstName}
              onChange={handleInputChange}
              className="border border-gray-300 rounded px-2 py-1 w-full text-secondary"
            />
            {!isValidEmail && <p>Please enter a valid email address.</p>}
          </div>
          <div className="mb-4 w-full">
            <label htmlFor="lastName" className="block mb-1 text-secondary font-extrabold ">
              Efternamn:
            </label>
            <input
              type="text"
              id="lastName"
              name="lastName"
              disabled={formEnable}
              value={editedUserDetails.lastName}
              onChange={handleInputChange}
              className="border border-gray-300 rounded px-2 py-1 w-full text-secondary"
            />
          </div>
          <div className="mb-4 w-full">
            <label htmlFor="email" className="block mb-1 text-secondary font-extrabold">
              E-Post:
            </label>
            <input
              type="text"
              id="email"
              name="email"
              value={userDetails.email}
              disabled
              className="border border-gray-300 rounded px-2 py-1 w-full text-secondary"
            />
          </div>
          <div className="mb-4 w-full">
            <label htmlFor="mobile" className="block mb-1 text-secondary font-extrabold">
              Telefonnummer:
            </label>

            <input
              type="text"
              id="mobile"
              name="mobile"
              value={editedUserDetails.mobile}
              disabled={formEnable}
              onChange={handleInputChange}
              className="border border-gray-300 rounded px-2 py-1 w-full text-secondary"
            />
          </div>
          <div className="mb-4 w-full">
            <label htmlFor="businessAddress" className="block mb-1 text-secondary font-extrabold ">
              Adress:
            </label>
            <input
              type="text"
              id="businessAddress"
              name="businessAddress"
              value={editedUserDetails.businessAddress}
              disabled={formEnable}
              onChange={handleInputChange}
              className="border border-gray-300 rounded px-2 py-1 w-full text-secondary"
            />
          </div>
          <div className="mb-4 w-full">
            <label htmlFor="businessCity" className="block mb-1 text-secondary font-extrabold">
              Stad:
            </label>

            <input
              type="text"
              id="businessCity"
              name="businessCity"
              value={editedUserDetails.businessCity}
              disabled={formEnable}
              onChange={handleInputChange}
              className="border border-gray-300 rounded px-2 py-1 w-full text-secondary"
            />
          </div>
          <div className="mb-4 w-full">
            <label htmlFor="profilePic" className="block mb-1 text-secondary font-extrabold">
              Profilbild:
            </label>


            {/* <Imagepicker
              max={1}
              value="user-profile"
              isDisabled={formEnable}
              onImageUpload={handleImageUpload}
              updatedValue={conversionImage}
              base64={base64Image}
              bindedImage={bindedImage}
            /> */}
            <div>
              <input
                type="file"
                id="file-input"
                name="file-input"
                onChange={handleFileChange}
                // disabled={isDisabled}
                multiple
                className="w-[100%]"
              />
              <label id="file-input-label" htmlFor="file-input">Select a File</label>
              {/* {error && <p className="text-red-500">{error}</p>} */}
            </div>
            <div className="flex ">
              {base64Image && base64Image.length > 0
                ? base64Image?.map((img: any) => {
                  return (
                    <>
                      <div className="px-3 py-2 relative">
                        <img
                          src={img.values}
                          alt="image"
                          width={200}
                          height={200}
                          className="img"
                        />
                        <RxCross2
                          className="cursor-pointer m-2 absolute top-0 right-1 bg-red-400"
                          onClick={(id) => {
                            // deleteImage(img.id, "createproduct(or)event");
                            setBase64Image([]);
                            setBindedImage([])
                            setImageValue([]);
                          }}
                        />
                      </div>
                    </>
                  );
                })
                : bindedImage
                  ? bindedImage?.map((img: any, index: any) => {
                    return (
                      <>
                        {/* { index === 1 ?  */}
                        <div className="px-3 py-2 relative">
                          {img.image !== null ?
                            <>
                              <img
                                src={img.image}
                                alt="image"
                                width={100}
                                height={100}
                                className="img"
                              />
                              {
                                <RxCross2
                                  className="cursor-pointer m-2 absolute top-0 right-1 bg-red-400"
                                  onClick={(id) => {
                                    // deleteImage(img.id, "updateproduct(or)updateevent");
                                    setBindedImage([])
                                    setImageValue([])
                                  }}
                                />
                              }
                            </>
                            : null}
                        </div>
                        {/* : null}  */}
                      </>
                    );
                  })
                  : null}
            </div>




            {/* <div>
              <ImageShown
                bindedImage={bindedImage}
                isDisabled={formEnable}
                base64Image={base64Image}
                updatedValue={conversionImage}
                imageBinding={updatedPic}
                deleteImages={conversionImage}
                removeImages={removeImages}
              />
            </div> */}
          </div>
          <div className="flex justify-center">
            <button
              type="button"
              onClick={saveProfileChanges}
              className="px-4 py-1 rounded-md bg-[#D6B37D] cursor-pointer font-semibold text-white transition-all mr-2 uppercase"
            >
              Spara
            </button>
          </div>
        </form> :
        <div className="border border-1 px-4 rounded-lg grid sm:grid-cols-2 grid-cols-1 text-secondary lg:text-2xl md:text-xl sm:text-lg text-sm ">
          <div className="w-full sm:w-1/2 ">
            <div className="flex flex-col p-4">
              {/* <span >FirstName</span>
                <span className="font-bold">{userDetails.firstName}</span> */}
              <h1 className="font-extrabold text-[20px]">Förnamn</h1>
              <h1 className="text-secondary text-[20px]">{userDetails.firstName}</h1>
            </div>
            {/* <div className="flex flex-col py-3">
                <span>Telefonnummer</span>
                <span className="font-bold">{userDetails.mobile}</span>
              </div> */}
            <div className="flex flex-col p-4">
              <div className="">
                <h1 className="font-extrabold text-[20px]">Telefonnummer</h1>
                <h1 className="text-secondary text-[20px]">{userDetails.mobile}</h1>
              </div>
            </div>
            <div className="flex flex-col p-4">
              <div className="">
                <h1 className="font-extrabold text-[20px]">Adress</h1>
                <h1 className="text-secondary text-[20px]">{userDetails.businessAddress}</h1>
              </div>
            </div>
            <div className="flex flex-col p-4 font-extrabold text-[20px]">
              <span>Profilbild</span>

              <div className="opacity-60">
                {(bindedImage[0]?.image === null && base64Image.length === 0) ? null :
                  <ImageShown
                    bindedImage={bindedImage}
                    base64Image={base64Image}
                    isDisabled={formEnable}
                    updatedValue={() => { }}
                    imageBinding={() => { }}
                    deleteImages={() => { }}
                  />}
              </div>
            </div>
          </div >
          <div className="w-full sm:w-1/2">
            <div className="flex flex-col py-3">
              {/* <span>LastName</span>
                <span className="font-bold">{userDetails.lastName}</span> */}
              <h1 className="font-extrabold text-[20px] mt-1 text-secondary">Efternamn</h1>
              <h1 className="text-secondary text-[20px]">{userDetails.lastName}</h1>
            </div >
            <div className="flex flex-col py-3">
              {/* <span>Email</span>
                <span className="font-bold">{userDetails.email}</span> */}
              <h1 className="font-extrabold text-[20px] mt-2 text-secondary">E-Post</h1>
              <h1 className="text-secondary text-[20px] -mt-1 ">{userDetails.email}</h1>
            </div>
            <div className="flex flex-col py-3">
              <h1 className="font-extrabold text-[20px]  mt-2 text-secondary">Stad</h1>
              <h1 className="text-secondary capitalize text-[20px]">{capitalizeFirstLetter(userDetails.businessCity)}</h1>
            </div>
          </div >
        </div >
      }

    </div>
  );
};

export default MyProfile;
