import React, { useEffect, useState } from "react";
import CheckoutOne from "./CheckoutOne";
import CheckoutTwo from "./CheckoutTwo";
import CheckoutThree from "./CheckoutThree";
import { createSearchParams, useLocation } from "react-router-dom";
import BreadCrumb from "../../Components/BreadCrumb/BreadCrumb";
const Checkout = () => {
  const [activeStep, setActiveStep] = useState(0);
  const locationData = useLocation();
  console.log('locationdata', locationData.state)
  const detailsParams = {
    id: locationData.state.eventId,
    searchText: locationData.state.searchText,
  };

  const popularParams = {
    searchTitle: "",
    searchText: locationData.state.searchText,
  };

  const steps = [
    { label: "", onClick: () => setActiveStep(0) },
    { label: "", onClick: () => setActiveStep(1) },
    { label: "", onClick: () => setActiveStep(2) },
  ];

  const breadsList = [
    {
      name: "Startsida",
      naviagteTo: "/",
    },
    {
      name: "Populära aktiviteter",
      naviagteTo: `/popular?${createSearchParams(popularParams)}`,
    },
    {
      name: locationData?.state?.eventName,
      naviagteTo: `/booking?${createSearchParams(detailsParams)}`
    },
    {
      name: "Checka ut",
    },
  ];

  const changeActiveState = (value: any) => {
    setActiveStep(value);
  };

  function getSectionComponent() {
    switch (activeStep) {
      case 0:
        return (
          <CheckoutOne
            changeActiveState={changeActiveState}
            locationData={locationData}
          />
        );
      case 1:
        return (
          <CheckoutTwo
            changeActiveState={changeActiveState}
            locationData={locationData}
          />
        );
      case 2:
        return <CheckoutThree />;
      default:
        return null;
    }
  }

  useEffect(() => {
    window.scrollTo(0, 230)
  }, [])

  return (
    <div className="overflow-x-hidden">
      <section className="mt-10 -ml-4 text-secondary">
        <BreadCrumb breadsList={breadsList} />
      </section>
      <div>{getSectionComponent()}</div>
    </div>
  );
};

export default Checkout;
