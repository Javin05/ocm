import React, { useContext, useEffect, useState } from "react";
import BookingDetails from "../../Components/BookingDetails/BookingDetails";
import { commonAxios } from "../../Axios/service";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import { useNavigate } from "react-router-dom";
import Loader from "../../Components/Loader/Loader";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { Button, Tooltip } from "@material-tailwind/react";
import { AuthContext } from "../RootLayout/RootLayout";
import _ from 'lodash'

const CheckoutOne = ({ changeActiveState, locationData }: any) => {
  const navigate = useNavigate();

  const [eventData, setEventData] = useState<any>({});
  const [isLoading, setIsLoading] = useState(false);
  const [newComment, setNewComment] = useState("");
  const [isBookingDone, setIsBookingDone] = useState(false);
  const [bookingData, setBookingData] = useState(null);
  const [enableOrder, setEnableOrder] = useState(false);
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  // const [isDeleteEventModalOpen, setIsDeleteEventModalOpen] = useState(false);
  const { setIsDeleteEventModalOpen, setIsBookingWithOutLogIn } =
    useContext(AuthContext);

  const handleRequestSubmit = async (event: any) => {
    event.preventDefault();
    if (!userInfo.id) {
      window.scrollTo(0, 0);
      let StateDate = locationData.state
      localStorage.setItem(
        "redirectBackTo",
        JSON.stringify({
          pathname: window.location.pathname,
          search: window.location.search,
          state: StateDate,
        })
      );
      return setIsBookingWithOutLogIn(true)
      // setIsDeleteEventModalOpen(true);
      window.scrollTo(0, 0);
    } else {
      setIsLoading(true);
      const dateString = locationData.state.bookingDate;
      const date = new Date(dateString);
      const year = date.getFullYear();
      const month = ("0" + (date.getMonth() + 1)).slice(-2);
      const day = ("0" + date.getDate()).slice(-2);
      const formattedDate = `${year}-${month}-${day}`;

      const body = {
        ...formData,
        ...locationData.state,
        message: newComment,
        bookingDate: formattedDate,
        appUserId: userInfo.id,
        productId: null,
        bookingItemType: "EVENT",
        quantity: 0,
      };
      commonAxios.post("/cemo/bookingitems/new", body).then((response: any) => {
        if (response.data) {
          setBookingData(response.data);
          setIsBookingDone(true);
          setIsLoading(false);
          // changeActiveState(1);
          setEnableOrder(true);
          successNotify("Your booking request has been submitted")
        } else {
          failureNotify("oops! something went wrong, Try again");
          setIsLoading(false);
        }
      });
    }
  };

  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    address: "",
    postalcode: "",
  });

  const handlechange = (event: any) => {
    setFormData((prev: any) => {
      return {
        ...prev,
        [event.target.name]: event.target.value,
      };
    });
  };

  const getEventDetails = async () => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get(
        `/cemo/events/${locationData.state.eventId}`
      );
      if (response.data) {
        setEventData(response.data);
      }
    } catch (error) {

    } finally {
      setIsLoading(false);
    }
  };

  const sendRequest = () => {
    navigate("/");
  };

  useEffect(() => {
    const handleEscPress = (event: KeyboardEvent) => {
      if (event.key === "Escape") {
        setIsDeleteEventModalOpen(false);
      }
    };

    document.addEventListener("keydown", handleEscPress);

    return () => {
      document.removeEventListener("keydown", handleEscPress);
    };
  }, []);

  const handlepayment = (paymentData: any, event: any) => {
    event.preventDefault();
    if (!userInfo.id) {
      window.scrollTo(0, 0);
      let StateDate = locationData.state
      localStorage.setItem(
        "redirectBackTo",
        JSON.stringify({
          pathname: window.location.pathname,
          search: window.location.search,
          state: StateDate,
        })
      );
      return setIsBookingWithOutLogIn(true)
    }
    if (isBookingDone) {
      navigate("/payment", {
        state: {
          paymentData: paymentData,
          bookingData: bookingData,
        },
      });
    } else {
      if (!userInfo.id) {
        setIsDeleteEventModalOpen(true);
        window.scrollTo(0, 0);
      } else if (eventData.price === 0) {
        setIsLoading(true);
        const dateString = locationData.state.bookingDate;
        const date = new Date(dateString);
        const year = date.getFullYear();
        const month = ("0" + (date.getMonth() + 1)).slice(-2);
        const day = ("0" + date.getDate()).slice(-2);
        const formattedDate = `${year}-${month}-${day}`;

        const body = {
          ...formData,
          memberCount: locationData.state.memberCount,
          bookingSlotId: locationData.state.bookingSlotId,
          bookingDate: formattedDate,
          eventId: locationData.state.eventId,
          message: newComment,
          appUserId: userInfo.id,
        };

        commonAxios
          .post("/cemo/quotation", body)
          .then((response: any) => {
            if (response.data) {
              successNotify("Quotation sent.");
              setIsLoading(false);
              setEnableOrder(true);
              navigate("/");
            } else {
              failureNotify("oops! something went wrong, Try again");
              setIsLoading(false);
            }
            console.log(response.data, "response.data");
          })

          .catch((error: any) => {
            failureNotify("oops! something went wrong, Try again");
            setIsLoading(false);
          });
        // console.log(response.data,"response.data");
      } else {
        setIsLoading(true);
        // console.log(response.data,"response.data");
        const dateString = locationData.state.bookingDate;
        const date = new Date(dateString);
        const year = date.getFullYear();
        const month = ("0" + (date.getMonth() + 1)).slice(-2);
        const day = ("0" + date.getDate()).slice(-2);
        const formattedDate = `${year}-${month}-${day}`;

        const body = {
          ...formData,
          ...locationData.state,
          bookingDate: formattedDate,
          appUserId: userInfo.id,
          productId: null,
          bookingItemType: "EVENT",
          quantity: 0,
        };
        commonAxios
          .post("/cemo/bookingitems/new", body)
          .then((response: any) => {
            if (response.data) {
              setBookingData(response.data);
              setIsBookingDone(true);
              setIsLoading(false);
              setEnableOrder(true);
              navigate("/payment", {
                state: {
                  paymentData: paymentData,
                  bookingData: response.data,
                  memberCount: response.data.booking?.memberCount
                },
              });
            } else {
              failureNotify("oops! something went wrong, Try again");
              setIsLoading(false);
            }
          })
          .catch((error: any) => {
            failureNotify("oops! something went wrong, Try again");
            setIsLoading(false);
          });
      }
    }
  };
  const handlenewcomment = (e: any) => {
    setNewComment(e.target.value);
  };

  useEffect(() => {
    getEventDetails();
  }, []);

  console.log('eventData====>', eventData)

  return (
    <>
      {isLoading && <Loader />}

      <div className="grid grid-cols-1 lg:grid-cols-2">
        {!isBookingDone ? (
          <div className="grid-cols-1 border-2 mt-5 py-4 mx-3 md:ml-24 rounded-lg mb-5">
            <form className="mx-4 mt-2">
              <div className="mb-4">
                <label className="block font-bold mb-2 text-secondary text-[16px]">
                  Förnamn
                </label>
                <input
                  className="appearance-none border rounded-[10px] md:w-full py-2 px-4 leading-tight focus:outline-none focus:shadow-outline"
                  id="first-name"
                  type="text"
                  placeholder="Ange ditt förnamn"
                  name="firstName"
                  value={formData.firstName}
                  onChange={handlechange}
                />
              </div>
              <div className="mb-4">
                <label className="block  font-bold mb-2 text-secondary text-[16px]">
                  Efternamn
                </label>
                <input
                  className="appearance-none border rounded-[10px]
                  py-2
                  px-4  md:w-full leading-tight focus:outline-none focus:shadow-outline"
                  id="last-name"
                  type="text"
                  placeholder="Ange ditt efternamn"
                  name="lastName"
                  value={formData.lastName}
                  onChange={handlechange}
                />
              </div>
              <div className="mb-4">
                <label className="block font-bold mb-2 text-secondary text-[16px]">
                  Adress
                </label>
                <input
                  className="appearance-none border md:w-full rounded-[10px]
                  py-2
                  px-4 leading-tight focus:outline-none focus:shadow-outline"
                  id="address"
                  type="text"
                  placeholder="Ange din adress"
                  name="address"
                  value={formData.address}
                  onChange={handlechange}
                />
              </div>
              <div className="mb-4">
                <label className="block font-bold mb-1 text-secondary text-[16px]">
                  Postkod
                </label>
                <input
                  className="appearance-none border rounded-[10px] py-2 px-4 md:w-full leading-tight focus:outline-none focus:shadow-outline"
                  id="postalcode"
                  type="text"
                  placeholder="124 786"
                  name="postalcode"
                  value={formData.postalcode}
                  onChange={handlechange}
                />
              </div>
              <div>
                <h1 className="ml-1 py-2 font-bold text-secondary text-[16px]">
                  Message
                </h1>
                <textarea
                  name="message"
                  value={newComment}
                  onChange={handlenewcomment}
                  rows={3}
                  placeholder="Dina kommentarer..."
                  className="p-3 w-full border outline-none rounded-lg"
                />
              </div>
              <div className="w-full text-center">
                <button
                  type="submit"
                  disabled={
                    !formData.firstName ||
                    !formData.lastName ||
                    !formData.address ||
                    !formData.postalcode
                  }
                  onClick={(event) => {
                    if (_.isNumber(eventData.fromPrice)) {
                      handleRequestSubmit(event)
                    } else {
                      handlepayment(eventData, event)
                    }
                  }}
                  className="text-white bg-[#D6B27D] disabled:bg-[#e3e3e3] font-bold p-2 px-6 text-[16px] rounded-lg w-auto focus:outline-none focus:shadow-outline mt-2 uppercase"
                >
                  {
                    _.isNumber(eventData.fromPrice) ? "Skicka förfrågan" : "BOKA EVENEMANG"
                  }
                </button>
              </div>
              {/* {
                eventData.price !== 0 && <p className="my-2 text-center text-secondary font-semibold font-[sofiapro]">
                  Or
                </p>
              } */}

              {/* {eventData.price !== 0 ? (
                <>
                  <div className="w-full flex justify-center items-center gap-3">
                    <button
                      type="submit"
                      disabled={
                        !formData.firstName ||
                        !formData.lastName ||
                        !formData.address ||
                        !formData.postalcode ||
                        !newComment
                      }
                      onClick={handleRequestSubmit}
                      className="text-white bg-[#D6B27D] ml-10 inline-block disabled:bg-[#e3e3e3] font-bold p-2 px-6 rounded-lg focus:outline-none focus:shadow-outline w-auto uppercase"
                    >
                      Skicka förfrågan
                    </button>
                    <Tooltip
                      className='bg-[#D6B27D]'
                      content="You can submit your queries as request"
                      animate={{
                        mount: { scale: 1, y: 0 },
                        unmount: { scale: 0, y: 25 },
                      }}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        strokeWidth={2}
                        className="h-7 w-7 mt-3 cursor-pointer text-[#D6B27D]"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M11.25 11.25l.041-.02a.75.75 0 011.063.852l-.708 2.836a.75.75 0 001.063.853l.041-.021M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9-3.75h.008v.008H12V8.25z"
                        />
                      </svg>
                    </Tooltip>
                  </div>

                </>
              ) : null} */}

            </form>
          </div>
        ) : (
          <div className="grid grid-cols-1 mt-5 px-4">
            <div className="grid-cols-1">
              <div className="flex justify-center items-center min-h-[55vh]">
                <div className="border-2 p-5 rounded-lg md:p-10  md:w-[70%] shadow-md">
                  <h1 className="text-[#1A202C] font-semibold md:text-[32px] font-[sofiapro] text-center">
                    Din förfrågan är skickad
                  </h1>
                  <div className="flex items-center mb-4 mt-8">
                    {/* <input
                      type="radio"
                      className="h-6 w-6"
                      name="radio"
                      checked
                    /> */}
                    <label className="ml-2 text-[#1A202C] text-[20px] block text-center w-full">
                      Vi svarar normalt sett inom 48h
                    </label>
                  </div>
                  <button
                    onClick={sendRequest}
                    className="w-full mt-8 uppercase bg-[#D6B27D] py-3 font-bold rounded-[10px] text-[#F7FAFC] p-2 px-6"
                  >
                    Fortsätt utforska
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}
        <div>
          <div className="grid grid-cols-1 p-5 sm:mt-0 w-full">
            <div className="grid-cols-1">
              <div className="flex justify-center items-center">
                <div className="border-2 rounded-lg p-6 w-full md:mr-20">
                  <h1 className="text-secondary font-semibold text-[20px] font-[sofiapro]">
                    Bokningsdetaljer
                  </h1>
                  <label className="mt-3 block font-bold text-secondary text-[18px]">
                    Event Namn :
                  </label>
                  <h3 className="text-secondary text-[18px]">
                    {eventData.name}
                  </h3>
                  <label className=" mt-3 block font-bold text-secondary text-[18px]">
                    Event Startdatum :
                  </label>
                  <h3 className="text-secondary text-[18px]">
                    {eventData.startDate}
                  </h3>
                  <label className="mt-3 block font-bold text-secondary text-[18px]">
                    Event Slutdatum :
                  </label>
                  <h3 className="text-secondary text-[18px]">
                    {eventData.endDate}
                  </h3>

                  <label className="mt-3 block font-bold text-secondary text-[18px]">
                    Event Pris :
                  </label>
                  {eventData.price !== 0 ? (
                    <h3 className="text-secondary text-[18px]">
                      {eventData.price} SEK  (incl.{eventData.taxType?.taxPercentage ?? 'N/A'} % tax)
                    </h3>
                  ) : (
                    <h3 className="text-secondary text-[18px]">N/A</h3>
                  )}
                  <label className="mt-3 block font-bold text-secondary text-[18px]">
                    Kategori :
                  </label>
                  <div>
                    {eventData?.categories?.map((category: any, index: any) => (
                      <span key={category.id}>
                        <h3 className="text-secondary text-[18px]" style={{ display: 'inline' }}>
                          {category.categoryName[0].toUpperCase() + category.categoryName.slice(1).toLowerCase()}
                        </h3>
                        {index < eventData.categories.length - 1 && ', '}
                      </span>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CheckoutOne;