import React from "react";
import BookingDetails from "../../Components/BookingDetails/BookingDetails";
import NewsLetter from "../../Components/NewsLetter/NewsLetter";
import { useNavigate } from "react-router-dom";

const CheckoutTwo = ({ changeActiveState }: any) => {
  const navigate = useNavigate();

  const sendRequest = () => {
    navigate("/");
  };

  return (
    <div className="md:w-screen md:mx-10 overflow-hidden ">
      <div className="grid grid-cols-1 lg:grid-cols-2">
        <div className="grid-cols-1">
          <div className="flex justify-center items-center min-h-[70vh]">
            <div className="border p-5 rounded-lg md:rounded-[48px] md:p-20  md:w-[70%]">
              <h1 className="text-[#1A202C] font-semibold md:text-[32px]">
                Request
              </h1>
              <div className="flex items-center mb-4 mt-8">
                <input type="radio" className="h-6 w-6" name="radio" checked />
                <label className="ml-2 text-[#1A202C] text-[20px]">
                  Booking successful, We have sent the request
                </label>
              </div>
              <button
                onClick={sendRequest}
                className="w-full mt-8 bg-[#D6B27D] py-3 uppercase font-semibold rounded-[10px] text-[#F7FAFC]"
              >
                Lägg order
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="mt-5">
        <NewsLetter />
      </div>
    </div>
  );
};

export default CheckoutTwo;
