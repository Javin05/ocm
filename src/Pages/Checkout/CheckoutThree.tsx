import React from 'react'
import BookingDetails from '../../Components/BookingDetails/BookingDetails'

const CheckoutThree = () => {
    return (
        <div className='md:w-screen'>
            <div className='grid md:grid-cols-2 grid-cols-1'>
                <div className='grid-cols-1 '>
                    <div className="flex justify-center items-center min-h-[70vh]">
                        <BookingDetails
                            userName={"John"}
                            date={"05-05-2023"}
                            time={"13.00-15.00"}
                            category={"PaintBall"}
                        />
                    </div>
                </div>
                <div className='grid-cols-1'>

                </div>
            </div>
        </div>
    )
}

export default CheckoutThree