import moment from 'moment'
import { commonAxios } from '../../Axios/service';
import { useContext, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../Redux/store';
import { setNotification } from '../../Redux/features/notificationSlice';
import { AuthContext } from '../RootLayout/RootLayout';


export interface Admin {
    id: string
    firstName: string
    lastName: string
    mobile: string
    telephone: string
    email: string
    businessName: string
    businessNumber: string
    businessAddress: string
    businessPinCode: string
    businessCity: string
    password: string
    accountStatus: string
    createdAt: string
    updatedAt: string
    categories: string
    profileImage: string
    resetToken: string
}

export interface Supplier {
    id: string
    firstName: string
    lastName: string
    mobile: string
    telephone: string
    email: string
    businessName: string
    businessNumber: string
    businessAddress: string
    businessPinCode: string
    businessCity: string
    password: string
    accountStatus: string
    createdAt: string
    updatedAt: string
    categories: string
    profileImage: string
    resetToken: any
}

export interface Message {
    id: string
    booking: any
    customer: any
    supplier: Supplier
    admin: Admin
    message: string
    isCustomerRead: boolean
    isSupplierRead: boolean
    isAdminRead: boolean
    createdDate: string
    updatedDate: string
    isCustomerMessage: boolean
    isSupplierMessage?: boolean
}

const ContactAdmin = (props: any) => {

    const userInfo = useSelector((state: RootState) => state.user.userDetails)
    const [adminData, setAdminData] = useState<any>(null);
    const [responseData, setResponseData] = useState<any>([]);
    const [newMessage, setNewMessage] = useState(""); // State to store the new message
    const dispatch = useDispatch();
    const [messages, setMessages] = useState<any[]>([]);
    const { setNotifyReRender, notifyRender } = useContext(AuthContext);
    const messageContainerRef = useRef<any>(null);

    const addComment = async () => {
        try {
            const payload = {
                message: newMessage,
                customerId: userInfo.id,
                adminId: "admin",
                isCustomerMessage: true,
                isAdminMessage: false,
                isSupplierMessage: false,
            };

            const response = await commonAxios.post("/cemo/messages", payload);

            if (response.data) {
                setNewMessage("");
                getMessages();
            }
        } catch (error) {
            // Handle the error
        }
    };

    const sortAndSetMessages = async (messagesArray: any[]) => {
        let unreadMessages = messagesArray.filter((readMessage: any) => {
            return readMessage.isCustomerRead === false;
        });

        let unreadtIds = unreadMessages.map((val: any) => {
            return val.id;
        });
        console.log(unreadtIds, "ids");
        let payload = {
            id: unreadtIds,
            customerId: userInfo.id,
            supplierId: null,
            adminId: null,
        };

        let updatedMessageResponse = await commonAxios.put(
            `/cemo/messages/update/status`,
            payload
        );
        if (updatedMessageResponse?.data) {
            if (userInfo.role === "ROLE_CUSTOMER") {
                try {
                    const messageCount = await commonAxios.get(
                        `cemo/appuser/notification/count?AdminId=admin`
                    );
                    if (messageCount.data) {
                        dispatch(setNotification(messageCount.data));
                    }
                } catch (e) { }
            }
        }
        messagesArray.sort((a, b) => {
            const dateA = moment(a.updatedDate, "YYYY-MM-DD HH:mm:ss");
            const dateB = moment(b.updatedDate, "YYYY-MM-DD HH:mm:ss");
            return dateA.diff(dateB);
        });
        setMessages(messagesArray);
    };

    const existMessageRead = async (messages: any) => {
        const updateVal = {
            id: [messages[0]?.id],
            customerId: messages?.customer?.id,
            supplierId: messages.data?.supplier.id,
            admin: "admin",
        };
        const responseUpdate = await commonAxios.put(
            "/cemo/messages/update/status",
            updateVal
        );
        setNotifyReRender(!notifyRender)
    };


    const getMessages = async () => {
        try {
            const response = await commonAxios.get(
                `/cemo/messages/users?adminId=admin&customerId=${userInfo.id}`
            );
            setResponseData(response.data)
            setAdminData(response.data[0]?.admin);
            if (response.data) {
                sortAndSetMessages(response.data);
                // existMessageRead(response.data)
                setNotifyReRender(!notifyRender)
            }
        }
        catch (error) {
        }
    };
    useEffect(() => {
        getMessages()
    }, [])

    useEffect(() => {
        // Scroll to the bottom when messages change
        if (messageContainerRef.current) {
            messageContainerRef.current.scrollTop = messageContainerRef.current.scrollHeight;
        }
    }, [messages]);

    return (
        <div className="mx-10 my-6">
            <div className="bg-[#fff] p-4 m-6 h-[85vh] max-h-[90vh] flex flex-row">
                <section className="w-1/3 py-4 px-10 h-full border-[#e3e3e3] border rounded-[10px] overflow-y-scroll">
                    <h2 className="font-bold font-[sofiapro] my-6 text-2xl text-secondary">
                        Admin
                    </h2>

                    <div className="flex">
                        <p className="font-bold text-lg text-[#D6B27D]">
                            {adminData ? (
                                <p>{adminData?.firstName}</p>
                            ) : (
                                <p>Inga meddelanden</p>
                            )}
                        </p>
                    </div>
                </section>

                <section className="w-2/3 h-full">
                    <div className="w-4/5 h-[80%] overflow-y-scroll flex flex-col mx-auto border border-[#e3e3e3] rounded-[10px]" ref={messageContainerRef}>
                        {responseData.length > 0 ? (
                            responseData.map((message: Message) =>
                                !message.isCustomerMessage ? (
                                    <div className="flex flex-row items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                                        <section className="w-3/4">
                                            <div className="flex justify-start">
                                                {message.admin?.profileImage ? (
                                                    <img
                                                        className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                                        src={message.admin?.profileImage}
                                                    ></img>
                                                ) : (
                                                    <span className="h-9 w-9 p-2 rounded-full flex justify-center items-center font-semibold bg-[#D6B27D] text-white mr-2 mb-2">
                                                        {message.admin?.firstName
                                                            .split("")
                                                            .at(0)
                                                            ?.toUpperCase() +
                                                            "" +
                                                            message.admin?.lastName
                                                                .split("")
                                                                .at(0)
                                                                ?.toUpperCase()}
                                                    </span>
                                                )}
                                                <p className="text-[#D6B27D] font-medium mt-1.5">
                                                    {message?.admin?.firstName}{" "}
                                                    {message?.admin?.lastName}
                                                </p>
                                            </div>
                                            <h4 className="text-sm text-left text-secondary">
                                                {message?.message}
                                            </h4>
                                        </section>
                                        <span className="w-1/4 text-sm text-secondary">
                                            {moment(message?.createdDate).format(
                                                "ddd, MMM DD YYYY"
                                            )}
                                        </span>
                                    </div>
                                ) : (
                                    <div className="flex flex-row justify-between items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                                        <span className="text-sm text-right text-secondary">
                                            {moment(message?.createdDate).format(
                                                "ddd, MMM DD YYYY"
                                            )}
                                        </span>
                                        <section className="w-3/4">
                                            <div className="flex flex-row justify-end">
                                                {message.customer?.profileImage ? (
                                                    <img
                                                        className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                                        src={message.customer?.profileImage}
                                                    ></img>
                                                ) : (
                                                    <span className="h-9 w-9 p-2 rounded-md flex justify-center items-center font-semibold bg-[#D6B27D] text-white mr-2 mb-2">
                                                        {message.customer?.firstName
                                                            .split("")
                                                            .at(0)
                                                            ?.toUpperCase() +
                                                            "" +
                                                            message.customer?.lastName
                                                                .split("")
                                                                .at(0)
                                                                ?.toUpperCase()}
                                                    </span>
                                                )}
                                                <p className="text-[#D6B27D] font-medium mt-1.5">
                                                    {message?.customer?.firstName}{" "}
                                                    {message?.customer?.lastName}
                                                </p>
                                            </div>
                                            <h4 className="text-sm text-right text-secondary">
                                                {message?.message}
                                            </h4>
                                        </section>
                                    </div>
                                )
                            )
                        ) : (
                            <div className="justify-center py-4 px-44 text-[#D6B37D]">
                                Inga meddelanden
                            </div>
                        )}
                    </div>
                    <div className="w-4/5 mx-auto mt-2">
                        <textarea
                            placeholder="Dina kommentarer..."
                            value={newMessage}
                            onChange={(e) => setNewMessage(e.target.value)} // Update the new message state
                            className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md"
                            name="comment"
                            id="comment"
                            rows={3}
                        />
                    </div>
                    <div className="w-4/5 mx-auto">
                        <div className='flex justify-center'>
                            <button
                                onClick={addComment}
                                className="bg-[#D6B27D] outline-none w-auto uppercase cursor-pointer text-white mb-3 font-semibold rounded-md px-6 text-lg py-2"
                            >
                                SKICKA
                            </button>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    )
}

export default ContactAdmin