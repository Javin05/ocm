import moment from 'moment'
import { commonAxios } from '../../Axios/service';
import { useContext, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../Redux/store';
import Select from "react-select";
import { RxCross1 } from "react-icons/rx";
import { failureNotify } from '../../Components/Toast/Toast';
import { useLocation } from 'react-router-dom';
import { setNotification } from '../../Redux/features/notificationSlice';
import { AuthContext } from '../RootLayout/RootLayout';

export interface Customer {
    id: string
    firstName: string
    lastName: string
    mobile: string
    telephone: string
    email: string
    businessName: string
    businessNumber: string
    businessAddress: string
    businessPinCode: string
    businessCity: string
    password: string
    accountStatus: string
    createdAt: string
    updatedAt: string
    categories: string
    profileImage: string
    resetToken: any
}
export interface Supplier {
    id: string
    firstName: string
    lastName: string
    mobile: string
    telephone: string
    email: string
    businessName: string
    businessNumber: string
    businessAddress: string
    businessPinCode: string
    businessCity: string
    password: string
    accountStatus: string
    createdAt: string
    updatedAt: string
    categories: string
    profileImage: string
    resetToken: any
}

export interface Message {
    id: string
    booking: any
    customer: Customer
    supplier: Supplier
    message: string
    isCustomerRead: boolean
    isSupplierRead: boolean
    isAdminRead: boolean
    createdDate: string
    updatedDate: string
    isCustomerMessage: boolean
    isSupplierMessage?: boolean
}

const ContactSupplier = (props: any) => {
    const userInfo = useSelector((state: RootState) => state.user.userDetails)
    const [supplierData, setSupplierData] = useState<any>(null);
    const [responseData, setResponseData] = useState<any>([])
    const [selectedSupplier, setSelectedSupplier] = useState<any>(null);
    const [newComment, setNewComment] = useState("");
    const dispatch = useDispatch();
    const [messages, setMessages] = useState<any[]>([]);
    const { setNotifyReRender, notifyRender } = useContext(AuthContext);
    const messageContainerRef = useRef<any>(null);


    const locations = useLocation();
    const { state } = locations;
    console.log(state, "state from contact")

    useEffect(() => {
        if (locations.state) {
            const defaultValue = {
                label: locations?.state?.key?.supplier?.businessName,
                value: locations?.state?.key?.supplier?.id
            }
            setSelectedSupplier(defaultValue);
            getMessages(defaultValue.value)
        }
    }, [locations]);


    const CustomDropdownIndicator = () => (
        <div
            style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
            }}
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                style={{ width: "24px", height: "24px", fill: '#A09792' }}
            >
                <path fill="none" d="M0 0h24v24H0z" />
                <path d="M7 10l5 5 5-5z" />
            </svg>
        </div>
    );

    const customComponents: any = {
        DropdownIndicator: CustomDropdownIndicator,
    };

    const customStyles = {
        control: (provided: any, state: any) => ({
            ...provided,
            border: "none",
            outline: "none",
            fontSize: "17px",
            width: "220px",
            boxShadow: "none",
        }),
        option: (provided: any, state: any) => ({
            ...provided,
            backgroundColor: "transparent",
            fontSize: "17px",
            width: "300px",
            boxShadow: "none",
            color: state.isSelected ? "#D6B27D" : "gray",
        }),
        singleValue: (provided: any) => ({
            ...provided,
            color: "gray", // Set the initial text color to gray
        }),
        valueContainer: (provided: any) => ({
            ...provided,
            color: "gray", // Set the initial text color to gray
        }),
    };

    const addComment = async () => {
        try {
            if (!selectedSupplier) {
                // Handle the case where no supplier is selected
                failureNotify("Please select a supplier before sending a message");
                return;
            }

            const payload = {
                message: newComment,
                supplierId: selectedSupplier.value, // Use selectedSupplier to get the supplierId
                customerId: userInfo.id,
                isCustomerMessage: true,
                isSupplierMessage: false,
                isAdminMessage: false,
            };

            const response = await commonAxios.post("/cemo/messages", payload);
            if (response.data) {
                setNewComment("");
                getMessages(selectedSupplier.value); // Refresh messages after posting a new one
            }
        } catch (error) {
            failureNotify("Oops! Something went wrong");
        }
    };

    const getSuppliers = async () => {
        try {
            const response = await commonAxios.get(
                `/cemo/appuser/all/supplier/customers?appUser=${userInfo.id}`
            );
            const activeSuppliers = response.data.filter((supplier: any) => supplier.accountStatus === "ACTIVE");
            if (activeSuppliers.length > 0) {
                setSupplierData(activeSuppliers.map((supplier: any) => ({
                    label: supplier.businessName,
                    value: supplier.id,
                })));
            }
        } catch (error) {
            // Handle the error
        }
    };

    useEffect(() => {
        getSuppliers()
    }, [])

    const handleSupplierChange = (selectedOption: any) => {
        setResponseData([]);
        setSelectedSupplier(selectedOption);
        if (selectedOption) {
            const selectedSupplierId = selectedOption.value;
            getMessages(selectedSupplierId);
            // setNotifyReRender(!notifyRender)
        }
    };

    const sortAndSetMessages = async (messagesArray: any[]) => {
        let unreadMessages = messagesArray.filter((readMessage: any) => {
            return readMessage.isCustomerRead === false;
        });

        let unreadtIds = unreadMessages.map((val: any) => {
            return val.id;
        });
        console.log(unreadtIds, "ids");
        let payload = {
            id: unreadtIds,
            customerId: userInfo.id,
            supplierId: null,
            adminId: null,
        };

        let updatedMessageResponse = await commonAxios.put(
            `/cemo/messages/update/status`,
            payload
        );
        if (updatedMessageResponse?.data) {
            if (userInfo.role === "ROLE_CUSTOMER") {
                try {
                    const messageCount = await commonAxios.get(
                        `cemo/appuser/notification/count?supplierId=${userInfo.id}`
                    );
                    if (messageCount.data) {
                        dispatch(setNotification(messageCount.data));
                        setNotifyReRender(!notifyRender)
                    }
                } catch (e) { }
            }
        }
        messagesArray.sort((a, b) => {
            const dateA = moment(a.updatedDate, "YYYY-MM-DD HH:mm:ss");
            const dateB = moment(b.updatedDate, "YYYY-MM-DD HH:mm:ss");
            return dateA.diff(dateB);
        });
        setMessages(messagesArray);
    };

    const existMessageRead = async (messages: any) => {
        const updateVal = {
            id: [messages[0]?.id],
            customerId: messages?.customer?.id,
            supplierId: messages.data?.supplier.id,
            admin: "admin",
        };
        const responseUpdate = await commonAxios.put(
            "/cemo/messages/update/status",
            updateVal
        );
    };

    const getMessages = async (supplierId: string) => {
        try {
            const response = await commonAxios.get(
                `/cemo/messages/users?supplierId=${supplierId}&customerId=${userInfo.id}`
            );
            setResponseData(response.data);
            if (response.data) {
                sortAndSetMessages(response.data);
                // existMessageRead(response.data)
            }
        } catch (error) {
            // Handle the error
        }
    };
    const handleClick = () => {
        setSelectedSupplier("");
    };

    useEffect(() => {
        // Scroll to the bottom when messages change
        if (messageContainerRef.current) {
            messageContainerRef.current.scrollTop = messageContainerRef.current.scrollHeight;
        }
    }, [messages]);


    return (

        <div className="mx-10 my-6">
            <div className="bg-[#fff] p-4 m-6 h-[85vh] max-h-[90vh] flex flex-row">
                <section className="w-1/3 py-4 px-10 h-full border-[#e3e3e3] border rounded-[10px] overflow-y-scroll">
                    <h2 className="font-bold font-[sofiapro] my-6 text-2xl text-secondary">
                        Suppliers
                    </h2>
                    <div className="flex items-center -mt-[7px] mr-5 border border-1 border-slate-300 h-10 bg-[white] rounded-lg">
                        <Select
                            components={customComponents}
                            value={selectedSupplier}
                            onChange={handleSupplierChange}
                            options={supplierData || []}
                            placeholder={"Välj leverantörer"}
                            className={""}
                            styles={customStyles}
                        />
                        <RxCross1
                            className="mx-2 cursor-pointer"
                            onClick={handleClick}
                            style={{ color: '#A09792' }}

                        />
                    </div>

                </section>
                <section className="w-2/3 h-full">
                    <div className="w-4/5 h-[80%] overflow-y-scroll flex flex-col mx-auto border border-[#e3e3e3] rounded-[10px]" ref={messageContainerRef}>
                        {responseData.length > 0 ? (
                            responseData.map((message: Message) =>
                                !message.isCustomerMessage ? (
                                    <div className="flex flex-row items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                                        <section className="w-3/4">
                                            <div className="flex justify-start">
                                                {message.supplier?.profileImage ? (
                                                    <img
                                                        className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                                        src={message.supplier?.profileImage}
                                                    ></img>
                                                ) : (
                                                    <span className="h-9 w-9 p-2 rounded-full flex justify-center items-center font-semibold bg-[#D6B27D] text-white mr-2 mb-2">
                                                        {message.supplier?.firstName
                                                            .split("")
                                                            .at(0)
                                                            ?.toUpperCase() +
                                                            "" +
                                                            message.supplier?.lastName
                                                                .split("")
                                                                .at(0)
                                                                ?.toUpperCase()}
                                                    </span>
                                                )}
                                                <p className="text-[#D6B27D] font-medium mt-1.5">
                                                    {message?.supplier?.businessName}{" "}
                                                    {/* {message?.supplier?.lastName} */}
                                                </p>
                                            </div>
                                            <h4 className="text-sm text-left text-secondary">
                                                {message?.message}
                                            </h4>
                                        </section>
                                        <span className="w-1/4 text-sm text-secondary">
                                            {moment(message?.createdDate).format(
                                                "ddd, MMM DD YYYY"
                                            )}
                                        </span>
                                    </div>
                                ) : (
                                    <div className="flex flex-row justify-between items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                                        <span className="text-sm text-right text-secondary">
                                            {moment(message?.createdDate).format(
                                                "ddd, MMM DD YYYY"
                                            )}
                                        </span>
                                        <section className="w-3/4">
                                            <div className="flex flex-row justify-end">
                                                {message.customer?.profileImage ? (
                                                    <img
                                                        className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                                        src={message.customer?.profileImage}
                                                    ></img>
                                                ) : (
                                                    <span className="h-9 w-9 p-2 rounded-md flex justify-center items-center font-semibold bg-[#D6B27D] text-white mr-2 mb-2">
                                                        {message.customer?.firstName
                                                            .split("")
                                                            .at(0)
                                                            ?.toUpperCase() +
                                                            "" +
                                                            message.customer?.lastName
                                                                .split("")
                                                                .at(0)
                                                                ?.toUpperCase()}
                                                    </span>
                                                )}
                                                <p className="text-[#D6B27D] font-medium mt-1.5">
                                                    {message?.customer?.firstName}{" "}
                                                    {message?.customer?.lastName}
                                                </p>
                                            </div>
                                            <h4 className="text-sm text-right text-secondary">
                                                {message?.message}
                                            </h4>
                                        </section>
                                    </div>
                                )
                            )
                        ) : (
                            <div className="justify-center py-4 px-44 text-[#D6B37D]">
                                Inga meddelanden
                            </div>
                        )}
                    </div>
                    <div className="w-4/5 mx-auto mt-2">
                        <textarea
                            placeholder="Dina kommentarer..."
                            value={newComment}
                            onChange={(e) => setNewComment(e.target.value)}
                            className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md"
                            name="comment"
                            id="comment"
                            rows={3}
                        />
                    </div>
                    <div className="w-4/5 mx-auto">
                        <div className='flex justify-center'>
                            <button
                                onClick={addComment}
                                className="bg-[#D6B27D] outline-none w-auto uppercase cursor-pointer text-white mb-3 font-semibold rounded-md px-6 text-lg py-2"
                            >
                                SKICKA
                            </button>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    )
}

export default ContactSupplier

