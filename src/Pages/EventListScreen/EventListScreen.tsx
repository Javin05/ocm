import React, { useState, useEffect } from 'react';
import AdminHeader from "../../Components/AdminHeader/AdminHeader";
import GridTable from "../../Components/GridTable/GridTable";
import SideNav from "../../Components/SideNav/SideNav";
import { commonAxios } from "../../Axios/service";
import searchIcon from "../../assets/adminSearch.svg";
import { RootState } from "../../Redux/store";
import { useSelector } from "react-redux";
import moment from 'moment';
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import Loader from '../../Components/Loader/Loader';

const EventListScreen = () => {
  const [filterData, setFilterData] = useState("");
  const [rowData, setRowData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [countLoading, setcountLoading] = useState(false);
  const userInfo = useSelector((state: RootState) => state.user.userDetails);

  useEffect(() => {
    if (userInfo.role === "ROLE_ADMIN") {
      getEventsListAdmin();
      setIsLoading(true);
    } else if (userInfo.role === "ROLE_SUPPLIER") {
      getEventsListAdmin();
      setIsLoading(true);
    }
  }, [userInfo.role]);

  const getEventsListAdmin = async () => {
    try {
      const response = await commonAxios.get(`/cemo/feedback/list?type=Event${userInfo.role === "ROLE_SUPPLIER" ? `&supplierId=${userInfo.id}` : ''}`);
      setIsLoading(false)
      setcountLoading(false)
      const activeEvents = response.data.filter((event: any) => event.status === 'ACTIVE');
      const gridData = activeEvents.map((event: any, index: number) => ({
        Sno: index + 1,
        customer: {
          name: `${event.customer.firstName} ${event.customer.lastName}`,
          image: event.customer.profileImage,
          dateTime: event.customer.createdAt,
        },
        supplier: event.supplier.businessName,
        event: event.event ? event.event.name : 'N/A',
        eventId: event.id
      }));
      setRowData(gridData);
    } catch (error) {
      console.error('Error fetching events:', error);
    }
  };

  const columnDefs = ([
    { headerName: "SNo", field: "Sno", width: 100 },
    {
      headerName: "Kund",
      field: "customer",
      width: 480,
      cellRendererFramework: (params: any) => (
        <div className="flex items-center">
          <img src={params.value.image} alt="Customer" className="w-8 h-8 rounded-full mr-2" />
          <span>{params.value.name}</span>
          <div style={{ marginTop: '5px', marginLeft: '49px' }}> {moment(params.value.dateTime).format('DD/MM/YYYY - HH:mm')}</div>
        </div>
      )
    },
    { headerName: "Supplier", field: "supplier", width: 290 },
    { headerName: "Event", field: "event", width: 190 },

    userInfo.role === "ROLE_SUPPLIER" ?
      {
        headerName: "Åtgärder",
        cellRendererFramework: (params: any) => {
          return (
            <>
              <div className="flex flex-row justify-between items-center">
                {
                  params.data.isSupplier ? (
                    <div
                      className='mr-2 bg-green-500 bg-opacity-70 w-[70px] rounded-lg text-[#FFFFFF] p-1 flex items-center justify-center'
                    >
                      Skickat
                    </div>
                  ) : null
                }
              </div>
            </>
          );
        },
      } :
      {
        headerName: "Åtgärder",
        cellRendererFramework: (params: any) => {
          const handleRejectClick = () => {
            // Handle rejection action
            console.log("Reject clicked");
          };

          return (
            <>
              <div className="flex flex-row justify-between items-center">
                {
                  params.data.isSupplier ? (
                    <div
                      className='mr-2 bg-green-500 bg-opacity-70 w-[70px] rounded-lg text-[#FFFFFF] p-1 flex items-center justify-center'
                    >
                      Skickat
                    </div>
                  ) : (
                    <>
                      {countLoading && (

                        <button
                          className="p-2 w-auto m-5 font-semibold rounded-lg bg-[#D6B37D] text-white px-6 uppercase"
                          type="submit"
                        >
                          <svg
                            aria-hidden="true"
                            className="inline w-5 h-5 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-[#D6B37D]"
                            viewBox="0 0 100 101"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                              fill="currentColor"
                            />
                            <path
                              d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                              fill="currentFill"
                            />
                          </svg>
                        </button>
                      )}
                      {
                        !countLoading ? (
                          <>
                            <button
                              onClick={() => handleSendClick(params.data.eventId)}
                              className='mr-2 bg-[#D6B27D] w-[70px] rounded-lg text-[#FFFFFF] p-1'
                              disabled={countLoading}
                            >
                              Skicka
                            </button>
                            <button
                              onClick={handleRejectClick}
                              className='bg-[#D9512C] w-[70px] rounded-lg text-[#FFFFFF] p-1'
                            >
                              Avvisa
                            </button>
                          </>
                        ) : ''
                      }
                    </>
                  )
                }
              </div>
            </>
          );
        },
      }
  ]);

  const handleSendClick = async (id: string) => {
    setcountLoading(true)
    try {
      const response = await commonAxios.put(`/cemo/feedback/update/${id}`);
      if (response.status === 200) {
        successNotify(response.data)
        getEventsListAdmin();
      } else {
        failureNotify(response.data)
        setcountLoading(false)
      }
    } catch (error) {
      console.error('Error updating feedback:', error);
    }
  };

  const filterDatas = (filterData: string) => {
    return rowData.filter((val: any) =>
      val.customer && val.customer.name.toLowerCase().includes(filterData.toLowerCase())
    );
  };

  return (
    <div className='flex'>
      <SideNav />
      <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
        <AdminHeader />
        <div className="mx-0 my-6">
          {
            isLoading ? <Loader /> : (
              <>
                <div className="flex flex-row items-center justify-between">
                  <h1 className="text-secondary  text-[24px] font-semibold p-4">
                    Event feedback
                  </h1>
                </div>
                <div className=" border border-[#FFFFFF] bg-[#ffff] rounded-[20px] p-4 mt-2">
                  <div className="relative w-full">
                    <input
                      type="text"
                      className="w-full h-[38px] pl-10 pr-5 border rounded-md border-[#E2E2EA] focus:outline outline-[#E2E2EA] focus:border-transparent"
                      placeholder="Sök på namn, leverantör, mobil osv..."
                      onChange={(e) => setFilterData(e.target.value)}
                      value={filterData}
                    />
                    <div className="absolute inset-y-0 left-0 flex items-center pl-3">
                      <img src={searchIcon} alt="Search icon" />
                    </div>
                  </div>
                  <div className="mt-6">
                    <GridTable
                      columnDefs={columnDefs}
                      rowData={filterDatas(filterData)}
                    />
                  </div>
                </div>
              </>
            )}
        </div>
      </div>
    </div>
  );
};

export default EventListScreen;
