import React, { useContext, useEffect, useState } from "react";
import fillbulet from "../../assets/fillbulet.png";
import Increase from "../../assets/Increase.png";
import Decrease from "../../assets/decrease.png";
import {
  createSearchParams,
  useLocation,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import Loader from "../../Components/Loader/Loader";
import { commonAxios } from "../../Axios/service";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import RelatedProductCard from "../../Components/RelatedProductCard/RelatedProductCard";
import RatingBar from "../../Components/RatingBar/RatingBar";
// import Swiper core and required modules
import { Navigation, Pagination, Scrollbar, A11y } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { HiHeart, HiOutlineHeart } from "react-icons/hi";
import { AiFillHeart } from "react-icons/ai";
import notFavHeart from "../../assets/latestNotfav.png";
import share from "../../assets/share.png";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import { addProductToCart } from "../../Redux/features/productCartSlice";
import { IoIosHeartEmpty, IoIosSend } from "react-icons/io";
import moment from "moment";
import { setNotification } from "../../Redux/features/notificationSlice";
import { AuthContext } from "../RootLayout/RootLayout";
import { Tooltip } from "@material-tailwind/react";
import BreadCrumb from "../../Components/BreadCrumb/BreadCrumb";
import { MdOutlineIosShare } from "react-icons/md";
import { getWishlistCount } from "../../Redux/features/wishlistSlice";
import { getCartListCount } from "../../Redux/features/cartCountSlice";

const ProductDetail = () => {
  const [count, setCount] = useState(1);
  const [searchParams] = useSearchParams();
  const [isLoading, setIsLoading] = useState(false);
  const [activeImage, setActiveImage] = useState(0);
  const [product, setProduct] = useState<any>({});
  const [productsList, setProductsList] = useState<any>([]);
  const [swiperObj, setSwiperObj] = useState<any>(null);
  const [isFavourite, setIsFavourite] = useState<Boolean>(false);
  const [newComment, setNewComment] = useState("");
  const [ratings, setRatings] = useState();
  const [browserWidth, setBrowserWidth] = useState(window.innerWidth);
  const [showFullDescription, setShowFullDescription] = useState(false);
  const [feedBackList, setFeedBackList] = useState<any>([]);
  const userInfo = useSelector((state: RootState) => state.user.userDetails);

  const dispatch = useDispatch();
  const { setBookingLogin } = useContext(AuthContext);

  const navigate = useNavigate();
  const popularParams = {
    searchTitle: "",
    searchText: searchParams.get("searchText") as string,
  };
  const breadsList = [
    {
      name: "Home",
      naviagteTo: "/",
    },
    {
      name: "Popular Activities",
      naviagteTo: popularParams.searchText
        ? `/popular?${createSearchParams(popularParams)}`
        : `/popular?searchTitle=&searchText=${product?.appUserResponseDTO?.businessCity}`,
    },

    // {
    //   name:
    //     product?.categoryResponseDTO?.categoryName[0] +
    //     product?.categoryResponseDTO?.categoryName.slice(1).toLowerCase(),
    //   naviagteTo: `/popular?searchTitle=&searchText=${product?.appUserResponseDTO?.businessCity}&category=${product?.categoryResponseDTO?.categoryName}`,
    // },
    {
      name: product?.name,
    },
  ];

  const handleShareClick = async (id: any) => {
    try {
      let text = `${window.location.href}`;
      await navigator.clipboard.writeText(text);
      successNotify("Link Copied");
    } catch (error) {
      failureNotify("the Link is Not Copied");
    }
  };

  const increment = () => {
    setCount(count + 1);
  };
  const decrement = () => {
    if (count > 0) {
      setCount(count - 1);
    }
  };

  useEffect(() => {
    const handleResize = () => {
      setBrowserWidth(window.innerWidth);
    };

    window.addEventListener("resize", handleResize);

    // Cleanup the event listener when the component unmounts
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const getProductList = async () => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get(
        `/cemo/products/get/${searchParams.get("id")}`
      );

      if (response.data) {
        setProductsList(response.data);
      }
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  const getProductDetail = async () => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get(
        `/cemo/products/${searchParams.get("productId")}`
      );

      if (response.data) {
        updateCommentCount(response.data);
        setProduct(response.data);
        setIsFavourite(response.data.isFavourite);
      }
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  const getfeedBackDetail = async () => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get(
        `/cemo/feedback/list?id=${searchParams.get("productId")}`
      );

      if (response.data) {
        // updateCommentCount(response.data);
        // setProduct(response.data);
        // setIsFavourite(response.data.isFavourite);
        setFeedBackList(response.data)

      }
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  console.log("feedback", feedBackList)

  const updateCommentCount = async (data: any) => {
    let customerId: any = [];
    let isCustomerCommentNotification: any = [];
    data.comments.map((customer: any) => {
      if (!customerId.includes(customer.customer.id)) {
        customerId.push(customer.customer.id);
      }
      isCustomerCommentNotification = customerId.filter(
        (loginCustomer: any) => {
          return loginCustomer === userInfo.id;
        }
      );
    });



    if (isCustomerCommentNotification.length > 0) {
      let updatedCustomerCommentCount = data.comments.filter((val: any) => {
        return val.isRead === false;
      });

      let updatedMessagesId = updatedCustomerCommentCount.map((val: any) => {
        return val.id;
      });

      let payload = {
        id: updatedMessagesId,
        customerId: userInfo.id,
        supplierId: null,
      };
      let updatedMessageResponse = await commonAxios.put(
        `/cemo/comments/update/status`,
        payload
      );

      if (updatedMessageResponse.data) {
        try {
          const messageCount = await commonAxios.get(
            `cemo/appuser/notification/count?supplierId=&customerId=${userInfo.id}`
          );
          if (messageCount.data) {
            dispatch(setNotification(messageCount.data));
          }
        } catch (e) { }
      }
    }
  };

  const handleAddToCart = async () => {
    if (!localStorage.getItem("cartSupplier")) {
      localStorage.setItem(
        "cartSupplier",
        JSON.stringify(product?.appUserResponseDTO?.id)
      );

      setIsLoading(true);
      const date = new Date();
      const year = date.getFullYear();
      const month = ("0" + (date.getMonth() + 1)).slice(-2);
      const day = ("0" + date.getDate()).slice(-2);
      const formattedDate = `${year}-${month}-${day}`;
      const updatedProduct = {
        appUserId: userInfo.id,
        productId: searchParams.get("productId"),
        bookingItemType: "PRODUCT",
        bookingDate: formattedDate,
        quantity: count,
      };
      try {
        const response = await commonAxios.post(
          "/cemo/bookingitems/new",
          updatedProduct
        );

        dispatch(addProductToCart());
        successNotify("Produkt lagd i varukorg");
        dispatch(getCartListCount(userInfo.id))
        navigate(
          {
            pathname: "/shoppingCart",
          },
          {
            state: {
              currentUrls: product?.name,
              previousUrl: window.location.href,
            },
          }
        );
      } catch (error) {
        failureNotify("oops! something went wrong, Try again");
      } finally {
        setIsLoading(false);
      }
    } else {
      const cartSupplier = JSON.parse(
        localStorage.getItem("cartSupplier") as string
      );
      if (cartSupplier !== product.appUserResponseDTO?.id) {
        failureNotify(
          "Products from multiple suppliers cannot be added to cart"
        );
      } else {
        setIsLoading(true);
        const date = new Date();
        const year = date.getFullYear();
        const month = ("0" + (date.getMonth() + 1)).slice(-2);
        const day = ("0" + date.getDate()).slice(-2);
        const formattedDate = `${year}-${month}-${day}`;
        const updatedProduct = {
          appUserId: userInfo.id,
          productId: searchParams.get("productId"),
          bookingItemType: "PRODUCT",
          bookingDate: formattedDate,
          quantity: count,
        };
        try {
          const response = await commonAxios.post(
            "/cemo/bookingitems/new",
            updatedProduct
          );

          dispatch(addProductToCart());
          successNotify("Produkt lagd i varukorg");
          dispatch(getCartListCount(userInfo.id))
          navigate(
            {
              pathname: "/shoppingCart",
            },
            {
              state: {
                currentUrls: product?.name,
                previousUrl: window.location.href,
              },
            }
          );
        } catch (error) {
          failureNotify("oops! something went wrong, Try again");
        } finally {
          setIsLoading(false);
        }
      }
    }
  };

  const handleAddToCartWithoutLogin = async () => {
    if (!localStorage.getItem("cartSupplier")) {
      localStorage.setItem(
        "cartSupplier",
        JSON.stringify(product?.appUserResponseDTO?.id)
      );
      const date = new Date();
      const year = date.getFullYear();
      const month = ("0" + (date.getMonth() + 1)).slice(-2);
      const day = ("0" + date.getDate()).slice(-2);
      const formattedDate = `${year}-${month}-${day}`;
      const cartItemList = {
        product: product,
        bookingItemType: "PRODUCT",
        totalPrice: product?.price * count,
        price: product?.price,
        vatAmount: product?.Tax,
        quantity: count,
        // totalVATDetailsDTO:{overAllVATAmount: product?.Tax}
      };
      localStorage.setItem("cartListDetails", JSON.stringify([cartItemList]));
      // dispatch(addProductToCart());
      successNotify("Produkt lagd i varukorg");
      // dispatch(getCartListCount(userInfo.id))
      navigate(
        {
          pathname: "/shoppingCart",
        },
        {
          state: {
            currentUrls: product?.name,
            previousUrl: window.location.href,
          },
        }
      );

    } else {
      const cartSupplier = JSON.parse(
        localStorage.getItem("cartSupplier") as string
      );
      if (cartSupplier !== product.appUserResponseDTO?.id) {
        failureNotify(
          "Products from multiple suppliers cannot be added to cart"
        );
      } else {
        const existingCartItems = localStorage.getItem("cartListDetails");
        let arr =
          existingCartItems && existingCartItems.length
            ? JSON.parse(existingCartItems)
            : [];
        const isExisProduct = arr.filter(
          (item: any) => item.product.id === product.id
        );
        arr = arr.filter((item: any) => item.product.id !== product.id);
        if (isExisProduct.length) {
          const finalQuantity = count + isExisProduct[0]?.quantity;
          const cartItemList = {
            product: product,
            bookingItemType: "PRODUCT",
            totalPrice: product?.price * finalQuantity,
            price: product?.price,
            vatAmount: product?.Tax,
            quantity: finalQuantity,
          };
          arr.push(cartItemList);
        } else {
          const cartItemList = {
            product: product,
            bookingItemType: "PRODUCT",
            totalPrice: product?.price * count,
            price: product?.price,
            vatAmount: product?.Tax,
            quantity: count,
          };
          arr.push(cartItemList);
        }
        try {
          localStorage.setItem("cartListDetails", JSON.stringify(arr));
          successNotify("Produkt lagd i varukorg");
          navigate(
            {
              pathname: "/shoppingCart",
            },
            {
              state: {
                currentUrls: product?.name,
                previousUrl: window.location.href,
              },
            }
          );
        } catch (error) {
          failureNotify("oops! something went wrong, Try again");
        } finally {
          setIsLoading(false);
        }
      }
    }
  };

  const redirectToChecking = () => {
    if (userInfo.id) {
      if (userInfo.role && userInfo.role !== "ROLE_CUSTOMER") {
        failureNotify('Sorry! You have logged in as a Supplier and you cannot checkout in this account. Please create a Customer account and proceed.')
      } else {
        handleAddToCart()
      }
    } else {
      handleAddToCartWithoutLogin();
      //window.scrollTo(0, 0);
      //localStorage.setItem('redirectBackTo', JSON.stringify(window.location.pathname + window.location.search))
      // setBookingLogin(true);
    }
  };

  const handleRelatedProductAddToCart = async (productItem: any) => {
    const cartSupplier = JSON.parse(
      localStorage.getItem("cartSupplier") as string
    );
    if (
      !localStorage.getItem("cartSupplier") &&
      cartSupplier === product.appUserResponseDTO?.id
    ) {
      setIsLoading(true);

      const updatedProduct = {
        appUserId: userInfo.id,
        productId: productItem?.id,
        bookingItemType: "PRODUCT",
        bookingDate: new Date()
          .toLocaleDateString()
          .split("/")
          .reverse()
          .join("-"),
        quantity: 1,
      };
      try {
        const response = await commonAxios.post(
          "/cemo/bookingitems/new",
          updatedProduct
        );

        dispatch(addProductToCart());
        dispatch(getCartListCount(userInfo.id))
        successNotify("Produkt lagd i varukorg");
        dispatch(getCartListCount(userInfo.id))
      } catch (error) {
        failureNotify("oops! something went wrong, Try again");
      } finally {
        setIsLoading(false);
      }
    } else if (
      !localStorage.getItem("cartSupplier") &&
      cartSupplier !== product.appUserResponseDTO?.id
    ) {
      failureNotify("Products from multiple suppliers cannot be added to cart");
    } else {
      localStorage.setItem(
        "cartSupplier",
        JSON.stringify(product.appUserResponseDTO?.id)
      );

      setIsLoading(true);

      const updatedProduct = {
        appUserId: userInfo.id,
        productId: productItem?.id,
        bookingItemType: "PRODUCT",
        bookingDate: new Date()
          .toLocaleDateString()
          .split("/")
          .reverse()
          .join("-"),
        quantity: 1,
      };
      try {
        const response = await commonAxios.post(
          "/cemo/bookingitems/new",
          updatedProduct
        );

        dispatch(addProductToCart());
        successNotify("Produkt lagd i varukorg");
        dispatch(getCartListCount(userInfo.id))
      } catch (error) {
        failureNotify("oops! something went wrong, Try again");
      } finally {
        setIsLoading(false);
      }
    }
  };

  const redirectToCheckingRelatedProduct = (productItem: any) => {
    if (userInfo.id) {
      if (userInfo.role && userInfo.role !== "ROLE_CUSTOMER") {
        failureNotify(
          "Sorry! You have logged in as a Supplier and you cannot checkout in this account. Please create a Customer account and proceed."
        );
      } else {
        handleRelatedProductAddToCart(productItem);
      }
    } else {
      window.scrollTo(0, 0);
      localStorage.setItem(
        "redirectBackTo",
        JSON.stringify(window.location.pathname + window.location.search)
      );
      setBookingLogin(true);
    }
  };

  const togglefavourite = async () => {
    try {
      // setIsLoading(true);
      const payload = {
        appUserId: userInfo.id,
        productId: searchParams.get("productId"),
      };
      await commonAxios.post("/cemo/favourite-items", payload).then(
        (response) => {
          setIsFavourite(!response.data.deleted);
        },
        (error) => { }
      );
    } catch (error) {
    } finally {
      // setIsLoading(false);
      dispatch(getWishlistCount(userInfo.id));
    }
  };

  const handlenewcomment = (e: any) => {
    setNewComment(e.target.value);
  };

  const addComment = async () => {
    setIsLoading(true);
    const payload = {
      customerId: userInfo.id, //Not null
      supplierId: product?.appUserResponseDTO?.id, //Not null
      eventId: null, //It must be null if productId have a value
      productId: searchParams.get("productId"), //It must be null if eventId have a value
      comments: newComment, //Not null
      rating: 2, //It can be null and value must be in 1 to 5
    };
    try {
      const response = await commonAxios.post("/cemo/comments/add", payload);
      if (response.data) {
        successNotify(`Comment added succesfully`);
        getProductDetail();
        setNewComment("");
      }
    } catch (error) {
      failureNotify("oops! something went wrong");
    } finally {
      setIsLoading(false);
    }
  };

  const location = useLocation();

  useEffect(() => {
    const scrollToTop = () => {
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    };
    scrollToTop();
    // if(userInfo && userInfo?.id != ''){
    getProductDetail();
    getProductList();
    getRatingDetails();
    getfeedBackDetail()
    // }
  }, [location]);

  useEffect(() => {
    if (userInfo.id) {
      getFavouriteItems();
    }
  }, [isFavourite]);

  const getFavouriteItems = async () => {
    try {
      const response = await commonAxios.get(
        `cemo/favourite-items/favourite/${userInfo.id}/${searchParams.get(
          "productId"
        )}`
      );
      setIsFavourite(response.data.isFavourite);
    } catch (e) {
    } finally {
    }
  };

  const getRatingDetails = async () => {
    const response = await commonAxios.get(
      `cemo/ratings/average/rating?productId=${searchParams.get("productId")}`
    );

    if (response.data) {
      setRatings(response.data);
    }
  };

  {
  }

  const toggleDescription = () => {
    setShowFullDescription(!showFullDescription);
  };
  const categoryNames = product?.categoryResponseDTO?.map((category: any) => category.categoryName);
  const formattedCategoryNames = categoryNames?.join(', ');

  return (
    <>
      {isLoading ? <Loader /> : null}

      <section className="mt-6 -ml-6 text-secondary px-2">
        {product?.categoryResponseDTO?.categoryName && product?.name && (
          <BreadCrumb breadsList={breadsList} />
        )}
      </section>
      <div className="bg-[#FFFFFF]">
        <div className="grid md:grid-cols-2 grid-cols-1">
          <div className="flex">
            <div className="sm:ml-4 md:ml-14 flex flex-col justify-between mr-4">
              <img
                onClick={() => setActiveImage(0)}
                src={product.image?.split(",")?.at(0) || product.image}
                alt="Image 1"
                className={`mt-4 shadow-2xl h-[66px] md:h-[100px] lg:h-[150px] xl:w-[250px] object-cover rounded-xl cursor-pointer ${activeImage === 0 ? "border-[#D6B27D] border-[3px]" : null
                  }`}
              />
              <img
                onClick={() => setActiveImage(1)}
                src={product.image?.split(",")?.at(1) || product.image}
                alt="Image 2"
                className={` shadow-2xl h-[66px] md:h-[100px] lg:h-[150px] md:w-[250px] object-cover rounded-xl cursor-pointer ${activeImage === 1 ? "border-[#D6B27D] border-[3px]" : null
                  }`}
              />
              <img
                onClick={() => setActiveImage(2)}
                src={product.image?.split(",")?.at(2) || product.image}
                alt="Image 3"
                className={` shadow-2xl h-[66px] md:h-[100px] lg:h-[150px]  md:w-[250px] object-cover rounded-xl cursor-pointer ${activeImage === 2 ? "border-[#D6B27D] border-[3px]" : null
                  }`}
              />
            </div>
            <div className="w-2/3 flex flex-col">
              <img
                src={
                  product.image?.split(",")?.at(activeImage) || product.image
                }
                alt="Image 1"
                className="mt-4 mr-4  w-full object-cover rounded-xl border-[#D6B27D] border-[2px] shadow-2xl
                sm:w-[30rem] md:w-[42rem] lg:w-[40rem]  lg:h-[30rem] h-[14rem] sm:h-[22rem] "
              />
            </div>
          </div>
          <div className="p-4 ml-0.5 mt-4 md:-mt-7 flex flex-col">
            <div className="flex items-center">
              <h1 className="text-[24px] md:text-[32px] lg:text-[44px] font-semibold sm:mt-5 font-[sofiapro] overflow-hidden text-secondary whitespace-break-spaces">{`${product.name}`}</h1>
              <div className="flex mx-4 sm:mt-4">
                {/* <Tooltip className='bg-[#D6B27D]' content={isFavourite ? "Remove from wishlist" : 'Add to wishlist'} placement="bottom"> */}
                <div onClick={togglefavourite} className="">
                  {isFavourite ? (
                    <div className="rounded-full p-3 hover:bg-[#f4eeeeef] cursor-pointer">
                      <AiFillHeart className="h-6 w-6 text-[#D6B27D]" />
                    </div>
                  ) : (
                    <div className="rounded-full p-3 hover:bg-[#f4eeeeef] cursor-pointer">
                      <img
                        src={notFavHeart}
                        className="h-7 w-7 z-40  hover:text-[#D6B27D]"
                      />
                    </div>
                  )}
                </div>
                {/* </Tooltip> */}
                {/* <Tooltip className='bg-[#D6B27D]' content={"Share"} placement="bottom"> */}
                <div
                  className="rounded-full p-3 hover:bg-[#f4eeeeef] cursor-pointer"
                  onClick={() => handleShareClick(searchParams.get("id"))}
                >
                  {" "}
                  <img src={share} className="h-6 w-6 text-[#D6B27D] z-40 " />
                </div>
                {/* </Tooltip> */}
              </div>
            </div>
            <div className="flex mt-3 md:mt-6">
              <button className="bg-[#FFC12320] rounded-xl md:rounded-full font-medium w-auto h-8 md:w-30 md:h-10 lg:w-40 lg:h-14 p-2 text-[10px] md:text-[16px] text-[#D6B37D]">{`Item # ${product?.productNumber}`}</button>
              <h1 className="flex items-center font-semibold text-[18px] md:text-[24px] lg:text-[36px] mx-3 md:mx-10 text-secondary">
                {" "}
                {product.price ? `${product.price.toFixed(2)} SEK` : ""}
              </h1>
            </div>
            <div className="flex mt-3 md:mt-8 mr-6">
              <h6 className="font-semibold text-[10px] md:text-[16px] mr-2 text-secondary">
                Kategorier:
              </h6>
              <h6 className="font-bold underline text-[10px] md:text-[16px] text-secondary">
                {/* {product?.categoryResponseDTO?.categoryName} */}
                {formattedCategoryNames}
              </h6>
              <div className="flex flex-row items-center md:mt-0 mt-1 mx-3 lg:mx-5 xl:mx-10">
                <h6 className="font-semibold text-[10px] md:text-[16px] text-secondary">
                  I lager:
                </h6>
                <h6 className="font-bold text-[10px] md:text-[16px] text-secondary ml-2">
                  {product?.quantity}
                </h6>
              </div>
            </div>

            <div className="flex mt-4 md:mt-8 md:flex-row gap-8">
              <div className="flex items-center justify-between border border-[#D6B27D] rounded-full w-auto">
                <button
                  className={`font-bold rounded px-3 md:px-3 xl:px-5 ${product?.quantity === 0 || count === 1
                    ? "cursor-not-allowed opacity-50"
                    : ""
                    }`}
                  onClick={decrement}
                  disabled={product?.quantity === 0 || count === 1}
                >
                  <img src={Decrease} alt="Decrease" />
                </button>
                <span className="text-xl px-1 md:px-3 xl:px-5 font-bold rounded mt-1">
                  {count}
                </span>
                <button
                  className={`font-bold px-3 md:px-3 xl:px-5 rounded ${product?.quantity === 0 || count === product.quantity
                    ? "cursor-not-allowed opacity-50"
                    : ""
                    }`}
                  onClick={increment}
                  disabled={
                    product?.quantity === 0 || count === product.quantity
                  }
                >
                  <img src={Increase} alt="Increase" />
                </button>
              </div>

              <div className="flex flex-row justify-between">
                <div className="md:mt-0 mt-1 mx-3 lg:mx-5 xl:mx-10">
                  {product?.quantity === 0 ? (
                    <button
                      className="rounded-lg  px-5 py-2.5 text-secondary bg-blue-gray-50 flex text-[9px] md:text-[12px] lg:text-[14px] xl:text-lg text-center font-bold cursor-not-allowed"
                      disabled
                    >
                      Slut i lager
                    </button>
                  ) : (
                    <div>
                      {userInfo?.role !== "ROLE_SUPPLIER" &&
                        userInfo?.role !== "ROLE_ADMIN" ? (
                        <div>
                          <button
                            onClick={redirectToChecking}
                            className="rounded-lg bg-[#D6B27D] uppercase px-5 py-2.5 text-white flex text-[9px] md:text-[12px] lg:text-[14px] xl:text-lg text-center font-bold"
                          >
                            Lägg till i varukorg
                          </button>
                        </div>
                      ) : (
                        <div></div>
                      )}
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="mt-4 md:mt-8 font-bold flex gap-3 text-secondary">
              <p className="text-[12px] md:text-[16px] font-bold"> Betyg:</p>
              <div>
                {product?.comments?.length > 0 ?
                  product?.comments?.map((comment: any, index: number) => {
                    if (comment.status === "ACCEPTED" && comment.rating) {
                      if (index === 0) {
                        return (
                          <>
                            <RatingBar ratings={ratings} readonly={true} />
                          </>
                        )
                      }
                    } else {
                      return (
                        <p className="text-[12px] md:text-[16px] font-bold">
                          {"No Betyg"}
                        </p>
                      );
                    }
                  }) : <p className="text-[12px] md:text-[16px] font-bold">
                    {"No Betyg"}
                  </p>}
                {/* {product?.comments?.status === "" ? (
                  <RatingBar ratings={ratings} readonly={true} />
                ) : (
                  <p className="text-[12px] md:text-[16px] font-bold">
                    {"No Betyg"}
                  </p>
                )} */}
              </div>
            </div>
          </div>
        </div>
        <div className="px-5 mt-14 ml-1">
          <div className="sm:mx-10">
            <h1 className="text-secondary text-[16px] font-semibold">
              Produktdetaljer
            </h1>
            <h1 className="md:text-[32px] text-secondary font-bold font-[kammerlander]">
              Beskrivning
            </h1>
            <div className="flex md:flex-row flex-col">
              {/* <p className="mr-4 text-secondary sm:ml-4"> */}
              <img
                src={
                  product.image?.split(",")?.at(activeImage) || product.image
                }
                className="object-cover rounded-lg sm:w-[10rem] lg:w-[12rem] sm:mr-7 mb-2 float-left 
                   md:w-[10rem] h-[50%] lg-h-[100%]"
              />
              {/* <p className="mr-4 text-secondary">{product.description}</p> */}
              {product?.description?.length > 200 ?
                <div>
                  {showFullDescription ? (
                    <>
                      {product?.description}
                      <span style={{ color: 'grey', cursor: 'pointer' }} onClick={toggleDescription}>
                        {' '}
                        ... Read less
                      </span>
                    </>
                  ) : (
                    <>
                      {product?.description.slice(0, 200)} {/* Display the first 200 characters */}
                      <span style={{ color: 'grey', cursor: 'pointer' }} onClick={toggleDescription}>
                        {' '}
                        ... Read more
                      </span>
                    </>
                  )}
                </div>
                : <>
                  <p className="mb-0 mt-0 text-secondary text-lg font-normal">
                    {product?.description
                      ?.split(".")
                      ?.slice(0, 2) // Adjust the number of sentences to include as needed
                      ?.join(".")}
                  </p>
                </>}

            </div>
          </div>
        </div>
        {userInfo.id && (
          <div className="px-10 sm:px-32 md:px-20  md:-ml-4 mt-10 h-[30rem]">
            <div className="bg-[#FFE5D8] rounded-lg md:rounded-[50px] flex flex-col justify-center items-center mt-20 lg:mt-0">
              <h1 className="text-secondary font-[kammerlander] text-[16px] font-semibold mt-6 ml-1 md:text-[26px]">
                Reviews
              </h1>
              <div className="max-h-[25rem] w-full h-[20rem] overflow-y-scroll my-2  rounded-xl md:0 sm:0 lg:0">
                {feedBackList.length > 0 &&
                  feedBackList.map((comment: any, index: number) => {
                    return (
                      <div
                        className={`flex flex-row justify-between items-baseline my-3 mx-1 ${index === product.comments.length - 1
                          ? "border-0"
                          : "border-b-[1px]"
                          } border-[#ddd0cc]`}
                      >
                        <section>
                          <div className="flex flex-row">
                            {comment.customer !== null ? (
                              comment.customer?.profileImage ? (
                                <img
                                  className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                  src={comment.customer.profileImage}
                                  alt="Profile"
                                />
                              ) : (
                                <span className="h-8 w-9 p-1 ml-4 rounded-lg font-semibold bg-[#D6B27D] text-white">
                                  {comment.customer.firstName.charAt(0).toUpperCase() +
                                    comment.customer.lastName.charAt(0).toUpperCase()}
                                </span>
                              )
                            ) : (
                              comment.supplier?.profileImage ? (
                                <img
                                  className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                  src={comment.supplier.profileImage}
                                  alt="Profile"
                                />
                              ) : (
                                <span className="h-8 w-9 p-1 ml-4 rounded-lg font-semibold bg-[#D6B27D] text-white">
                                  {comment.supplier.firstName.charAt(0).toUpperCase() +
                                    comment.supplier.lastName.charAt(0).toUpperCase()}
                                </span>
                              )
                            )}
                            {comment.customer !== null ? (
                              <p className="text-[#D6B27D] font-medium mt-1.5 ml-2">
                                {comment.customer.firstName}
                              </p>
                            ) : (
                              <p className="text-[#D6B27D] font-medium mt-1.5 ml-2">
                                {comment.supplier.firstName}
                              </p>
                            )}
                            <p className="text-[#D6B27D] font-medium mt-1.5 ml-20">
                              {comment.feedBack}
                            </p>
                          </div>
                          <div className="ml-4 -mt-0">
                            <RatingBar
                              ratings={comment.rating?.ratings}
                              readonly={true}
                            />
                          </div>
                          <h4 className="text-lg mx-4 mb-3 text-secondary">
                            {comment.rating?.descriptions}
                          </h4>
                        </section>
                        <span className="text-xs mr-4 text-secondary">
                          {moment(comment.createdDate).format(
                            "ddd, MMM DD YYYY"
                          )}
                        </span>
                      </div>
                    );
                  })}
                {feedBackList && feedBackList.length === 0 && (
                  <div className="text-secondary font-[kammerlander] text-center text-2xl mt-6 mb-4 ml-1">
                    No Reviews Added Yet
                  </div>
                )}
              </div>
              {/* <textarea
              value={newComment}
              onChange={handlenewcomment}
              rows={2}
              placeholder="Add Your Reviews..."
              className="p-4 w-3/4 border-2 outline-none rounded-lg border-[#D6B27D] mb-8"
            ></textarea> */}
              {/* <IoIosSend
              onClick={addComment}
              className="absolute rotate-45 cursor-pointer right-[16%] mr-5 lg:mr-4 xl:mr-0 bottom-9 h-[40px] w-[40px]  text-[#D6B27D] send-icon"
            /> */}
            </div>
          </div>
        )}
        <div className="md:mx-0 sm:mx-16 mt-3">
          <h1 className="text-secondary text-[16px] font-semibold mt-10 md:px-16 px-4">
            Utforska mer
          </h1>
          <h1 className="md:text-[26px] text-secondary font-bold font-[kammerlander] md:px-16 px-4">
            Relaterade produkter
          </h1>
          <div className="w-full mt-6 relative">
            {productsList?.length > 3 ? (
              <section className="flex flex-row gap-10 items-center absolute md:right-14 right-10 md:-top-20 -top-20">
                <button
                  onClick={() => swiperObj.slidePrev()}
                  className="w-12 h-12 rounded-full bg-[#D6B27D] flex justify-center items-center"
                >
                  <img src={Decrease} alt="Decrease" />
                </button>
                <button
                  onClick={() => swiperObj.slideNext()}
                  className="w-12 h-12 rounded-full bg-[#D6B27D] flex justify-center items-center"
                >
                  <img src={Increase} alt="Increase" />
                </button>
              </section>
            ) : null}
            {productsList?.length > 0 ? (
              <Swiper
                modules={[Navigation, Pagination, Scrollbar, A11y]}
                spaceBetween={30}
                // slidesPerView={window.innerWidth > 630px ? 3 : 1}
                slidesPerView={browserWidth > 700 ? 3 : 1}
                onSwiper={(swiper) => setSwiperObj(swiper)}
              >
                {productsList?.map((product: any) => {
                  return (
                    <SwiperSlide>
                      <RelatedProductCard
                        product={product}
                        onClick={redirectToCheckingRelatedProduct}
                        image={
                          product.image?.split(",")?.at(0) || product.image
                        }
                        category="Shop"
                        price={`${product.price} SEK`}
                        cardName={product.name}
                        loggedInUser={userInfo}
                      />
                    </SwiperSlide>
                  );
                })}
              </Swiper>
            ) : (
              <section className="mx-auto">
                <p className="font-bold text-lg text-secondary">
                  Related Products Not Available
                </p>
              </section>
            )}
          </div>
        </div>
      </div>
    </>
  );
};
export default ProductDetail;
