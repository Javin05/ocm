import React, { useEffect, useState } from "react";
import SideNav from "../../Components/SideNav/SideNav";
import AdminHeader from "../../Components/AdminHeader/AdminHeader";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { commonAxios } from "../../Axios/service";
import Loader from "../../Components/Loader/Loader";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import deleteIcon from "../../assets/Delete.png";
import moment from "moment";
import { useDispatch } from "react-redux";
import { useParams, useSearchParams } from "react-router-dom";
import {
  HiCheckCircle,
  HiOutlineCheckCircle,
  HiOutlineXCircle,
  HiXCircle,
} from "react-icons/hi";
import { setNotification } from "../../Redux/features/notificationSlice";
import { messageDatewise } from "../../Utils/Helperfunction";

export interface Message {
  id: string;
  customer: Customer;
  supplier: Supplier;
  event: Event | any;
  product: Product | any;
  message: string;
  rating: number;
  isRead: boolean;
  status: string;
  createdDate: string;
  updatedDate: string;
}

export interface Customer {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
  profileImage: string;
}

export interface Supplier {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
  profileImage: any;
}

export interface Event {
  id: string;
  name: string;
  description: string;
  city: string;
  image: string;
  appUser: AppUser;
}

export interface Product {
  id: string;
  name: string;
  description: string;
  image: string;
  quantity: number;
  price: number;
  status: string;
  productNumber: string;
  appUser: AppUser;
  category: Category;
  taxType: TaxType;
}

export interface AppUser {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName?: string;
  businessNumber?: string;
  businessAddress?: string;
  businessPinCode?: string;
  businessCity?: string;
  password: string;
  accountStatus: string;
  createdAt?: string;
  updatedAt: string;
  categories?: string;
  profileImage: string;
}

export interface Category {
  id: string;
  categoryName: string;
  categoryPrice: number;
  image?: string;
  createdAt: string;
  status: string;
}

export interface TaxType {
  id: string;
  taxPercentage: number;
}

export interface ProdOrEventList {
  id: string;
  orderNumber: string;
  supplierId: string;
}

const Messages = () => {
  const userInfo = useSelector((state: RootState) => state.user.userDetails);

  const [comments, setcomments] = useState<Message[]>([]);
  const [productsAndEventsList, setProductsAndEventsList] = useState<any>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [activeMenu, setActiveMenu] = useState<any>("");
  const [itemToReply, setItemToReply] = useState<Message>();
  const [orderNumber, setOrderNumber] = useState<any>();
  const [commentModalOpen, setAddCommentModalOpen] = useState(false);
  const [newMessage, setNewMessage] = useState("");
  const dispatch = useDispatch();
  const [searchParams] = useSearchParams()


  const handleclick = async (item: any) => {
    if (item.orderNumber) {
      setActiveMenu(item.orderNumber);
      try {
        setIsLoading(true);
        const response = await commonAxios.get(
          `/cemo/messages?orderNumber=${item?.orderNumber}`
        );

        if (response.data) {
          let updatedMessage = response.data.filter((val: any) => {
            return val.isSupplierRead === false;
          });

          let updatedMessagesId = updatedMessage.map((val: any) => {
            return val.id;
          });
          let payload = {
            id: updatedMessagesId,
            customerId: null,
            supplierId: userInfo.id,
          };

          let updatedMessageResponse = await commonAxios.put(
            `/cemo/messages/update/status`,
            payload
          );

          if (updatedMessageResponse.data) {
            if (userInfo.role === "ROLE_ADMIN") {
              try {
                const messageCount = await commonAxios.get(
                  `cemo/appuser/notification/count?adminId=${userInfo.id}`
                );

                if (messageCount.data) {
                  dispatch(setNotification(messageCount.data));
                }
              } catch (e) { }
            } else {
              try {
                const messageCount = await commonAxios.get(
                  `cemo/appuser/notification/count?supplierId=${userInfo.id}&customerId=`
                );

                if (messageCount.data) {
                  dispatch(setNotification(messageCount.data));
                }
              } catch (e) { }
            };
          }
        }
        setOrderNumber(item.orderNumber);
        setcomments(response.data);
      } catch (error) {
      } finally {
        setIsLoading(false);
      }
    } else {
      setActiveMenu(item);
      try {
        setIsLoading(true);
        const response = await commonAxios.get(
          `/cemo/messages?orderNumber=${item}`
        );

        if (response.data) {
          let updatedMessage = response.data.filter((val: any) => {
            return val.isSupplierRead === false;
          });

          let updatedMessagesId = updatedMessage.map((val: any) => {
            return val.id;
          });
          let payload = {
            id: updatedMessagesId,
            customerId: null,
            supplierId: userInfo.id,
          };

          let updatedMessageResponse = await commonAxios.put(
            `/cemo/messages/update/status`,
            payload
          );

          if (updatedMessageResponse.data) {
            if (userInfo.role === "ROLE_ADMIN") {
              try {
                const messageCount = await commonAxios.get(
                  `cemo/appuser/notification/count?adminId=${userInfo.id}`
                );

                if (messageCount.data) {
                  dispatch(setNotification(messageCount.data));
                }
              } catch (e) { }
            } else {
              try {
                const messageCount = await commonAxios.get(
                  `cemo/appuser/notification/count?supplierId=${userInfo.id}&customerId=`
                );

                if (messageCount.data) {
                  dispatch(setNotification(messageCount.data));
                }
              } catch (e) { }
            };
          }
        }
        setOrderNumber(item);
        setcomments(response.data);
      } catch (error) {
      } finally {
        setIsLoading(false);
      }
    }
  };

  const getMessages = async () => {
    try {
      setIsLoading(true);
      const response = await commonAxios.get(
        `/cemo/messages/supplier?supplierId=${userInfo.id}`
      );

      const orderNumberId: any = [];
      const orderNumber: any = [];
      response.data?.forEach((item: any) => {
        if (!orderNumberId.includes(item.booking.orderNumber)) {
          orderNumberId.push(item.booking.orderNumber);
          orderNumber.push({
            id: item.id,
            orderNumber: item.booking.orderNumber,
            supplierId: item.supplier.id,
            customerId: item.customer.id,
          });
        }
      });
      setProductsAndEventsList(orderNumber);

      const value = searchParams.get('orderNumber')
      if (value) {
        setActiveMenu(value)
        setOrderNumber(value);
        handleclick(value);
      }
      else {
        setOrderNumber(orderNumber?.at(0)?.orderNumber);
        setActiveMenu(orderNumber?.at(0)?.id as string);
        if (orderNumber?.at(0)) {
          handleclick(orderNumber?.at(0)!);
        }
      }
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getMessages();
  }, []);

  // const toggleMesageStatus = async (comment: any, status: boolean) => {
  //   try {
  //     setIsLoading(true);
  //     if (status) {
  //       comment.status = "ACCEPTED";
  //     } else {
  //       comment.status = "REJECTED";
  //     }
  //     await commonAxios
  //       .put(`/cemo/comments/update/${comment.id}`, comment)
  //       .then(
  //         (response) => {
  //           successNotify("Comment updated successfully");
  //         },
  //         (error) => {
  //           failureNotify("Comment Update failed.");
  //         }
  //       );
  //   } catch (error) {
  //   } finally {
  //     setIsLoading(false);
  //     getMessages();
  //   }
  // };

  const handlenewcomment = (e: any) => {
    setNewMessage(e.target.value);
  };

  const addComment = async () => {

    const customerID = productsAndEventsList.filter((item: any) => activeMenu === item.orderNumber);
    try {
      const payload = {
        orderNumber: orderNumber,
        message: newMessage,
        supplierId: userInfo.id,
        customerId: customerID?.at(0).customerId,
        isCustomerMessage: userInfo.role === 'ROLE_CUSTOMER' ? true : false,
        isSupplierMessage: userInfo.role === 'ROLE_SUPPLIER' ? true : false
      };
      const response = await commonAxios.post("/cemo/messages", payload);

      if (response.data) {
        handleclick(response.data.booking!);
        setNewMessage("");
      }
    } catch (error) {
      failureNotify("oops! something went wrong");
    }
  };

  return (
    <div className="flex">
      <SideNav />
      <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
        {isLoading ? <Loader /> : null}
        {/* {commentModalOpen ? <AddCommentModal /> : null} */}
        <AdminHeader />
        <div className="mx-10 my-6">
          <div className="flex flex-row justify-start items-center">
            <h1 className="text-secondary text-[20px] font-semibold">
              Messages
            </h1>
            {/* <button
              onClick={() => handlereply()}
              className="px-4 py-2 mb-2 ml-auto md:mr-4 bg-[#D6B27D] rounded-xl text-white text-lg font-semibold cursor-pointer outline-none"
            >
              Add Comments
            </button> */}
          </div>
          <div className="bg-[#fff] p-4 m-6 h-[80vh] max-h-[80vh] flex flex-row">
            <section className="w-1/3 py-4 px-10 h-full border-[#e3e3e3] border rounded-[10px] overflow-y-scroll">
              <h2 className="font-bold font-[sofiapro] my-6 text-2xl text-secondary">
                Orders
              </h2>

              {productsAndEventsList.length === 0 ? (
                <div className="flex justify-center items-center">
                  <p className="font-bold text-lg text-gray-600">
                    No Orders to show
                  </p>
                </div>
              ) : (
                productsAndEventsList?.map((item: ProdOrEventList) => (
                  <p
                    className={`cursor-pointer font-bold px-6 py-3 rounded-md transition-all my-2 text-secondary  ${activeMenu === item.orderNumber
                      ? "bg-[#D6B27D] text-white"
                      : "bg-transparent text-[#171725]"
                      }`}
                    onClick={() => handleclick(item)}
                  >
                    {item.orderNumber}
                  </p>
                ))
              )}
            </section>
            <section className="w-2/3 h-full">
              <div className="w-4/5 h-full overflow-y-scroll flex flex-col mx-auto border border-[#e3e3e3] rounded-[10px]">
                {comments.length > 0 ? (
                  messageDatewise(comments).map((message: any) =>
                    message.isCustomerMessage ? (
                      <div className="flex flex-row items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                        <section className="w-3/4">
                          <div className="flex justify-start">
                            {message.customer?.profileImage ? (
                              <img
                                className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                src={message.customer?.profileImage}
                              ></img>
                            ) : (
                              <span className="h-12 w-12 p-2 rounded-md font-semibold bg-[#5b62d1] text-white">
                                {message.customer?.firstName
                                  .split("")
                                  .at(0)
                                  ?.toUpperCase() +
                                  "" +
                                  message.supplier?.lastName
                                    .split("")
                                    .at(0)
                                    ?.toUpperCase()}
                              </span>
                            )}
                            <p className="text-[#D6B27D] font-medium mt-1.5">
                              {message?.customer?.firstName}{" "}
                              {message?.customer?.lastName}
                            </p>
                          </div>
                          <h4 className="text-sm text-left text-secondary">
                            {message?.message}
                          </h4>
                        </section>
                        <span className="w-1/4 text-sm text-secondary">
                          {moment(message?.createdDate).format(
                            "ddd, MMM DD YYYY"
                          )}
                        </span>
                      </div>
                    ) : (
                      <div className="flex flex-row justify-between items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                        <span className="text-sm text-right text-secondary">
                          {moment(message?.createdDate).format(
                            "ddd, MMM DD YYYY"
                          )}
                        </span>
                        <section className="w-3/4">
                          <div className="flex flex-row justify-end">
                            {message.supplier?.profileImage ? (
                              <img
                                className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                src={message.supplier?.profileImage}
                              ></img>
                            ) : (
                              <span className="h-12 w-12 p-2 rounded-md font-semibold bg-[#D6B27D] text-white">
                                {message.supplier?.firstName
                                  .split("")
                                  .at(0)
                                  ?.toUpperCase() +
                                  "" +
                                  message.supplier?.lastName
                                    .split("")
                                    .at(0)
                                    ?.toUpperCase()}
                              </span>
                            )}
                            <p className="text-[#D6B27D] font-medium mt-1.5">
                              {message?.supplier?.firstName}{" "}
                              {message?.supplier?.lastName}
                            </p>
                          </div>
                          <h4 className="text-sm text-right text-secondary">
                            {message?.message}
                          </h4>
                        </section>
                      </div>
                    )
                  )
                ) : (
                  <div className="justify-center py-4 px-44 text-[#D6B37D]">
                    Inga meddelanden
                  </div>
                )}
                <div className="px-5 pt-10 w-full">
                  <textarea
                    placeholder="Dina kommentarer..."
                    value={newMessage}
                    onChange={handlenewcomment}
                    className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md text-secondary"
                    name="comment"
                    id="comment"
                    rows={3}
                  />
                </div>
                <div className="w-full px-5">
                  <div className="flex justify-center">
                    <button
                      onClick={addComment}
                      className="bg-[#D6B27D] outline-none w-auto mt-6 cursor-pointer uppercase text-white font-semibold rounded-md px-6 text-lg py-2"
                    >
                      SKICKA
                    </button>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Messages;
