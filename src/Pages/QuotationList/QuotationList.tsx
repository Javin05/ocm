import React, { useEffect, useState } from "react";
import { commonAxios } from "../../Axios/service";
import SideNav from "../../Components/SideNav/SideNav";
import Loader from "../../Components/Loader/Loader";
import GridTable from "../../Components/GridTable/GridTable";
import Select from "react-select";
import AdminHeader from "../../Components/AdminHeader/AdminHeader";
import {
  Button,
  Dialog,
  DialogBody,
  DialogFooter,
  DialogHeader,
  Input,
  Typography,
} from "@material-tailwind/react";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import viewEye from "../../assets/greyeye.png";
import mail from "../../assets/icons8-mail-20.png";
import eyeIcon from "../../assets/viewIcon.svg";
import deleteIcon from "../../assets/Delete.png";
import eventdeleteicon from "../../assets/closeIcon.png";
import editIcon from "../../assets/editIcon.svg";
import moment from "moment";
import { IoMdCheckboxOutline } from "react-icons/io";
import { useNavigate } from "react-router-dom";

export interface Quotation {
  id: string;
  sno: number;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  message: string;
  quotationStatus: string;
  bookingDate: string;
  slot: Slot;
  numberOfAttendees: number;
  price: number;
  createdAt: string;
  updatedAt: string;
  supplier: Supplier;
  customer: Customer;
  event: Event;
}

export interface Slot {
  id: string;
  inTime: string;
  outTime: string;
  minAvailableCount: number;
  maxAvailableCount: number;
  bookedCount: number;
  slotTime: string;
}

export interface Supplier {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
  profileImage: string;
  resetToken: any;
}

export interface Customer {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
  profileImage: string;
  resetToken: string;
}

export interface Event {
  id: string;
  name: string;
  description: string;
  city: string;
  image: string;
  appUser: AppUser;
  category: Category;
  pincode: string;
  createdAt: string;
  updatedAt: string;
  price: number;
  status: string;
  eventNumber: string;
  taxType: TaxType;
  operatingHours: any;
  startDate: string;
  endDate: string;
  latitude: any;
  longitude: any;
  delayTime: string;
  comments: any;
  isFavourite: boolean;
}

export interface AppUser {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
  profileImage: string;
  resetToken: any;
}

export interface Category {
  id: string;
  categoryName: string;
  isSystemFlag: boolean;
  image: string;
  createdAt: string;
  status: string;
  supplier: Supplier;
}
export interface TaxType {
  id: string;
  taxPercentage: number;
}

const QuotationList = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [quotationList, setQuotationList] = useState<Quotation[]>([]);
  const [quotationListFiltered, setQuotationListFiltered] = useState<
    Quotation[]
  >([]);
  const [open, setOpen] = React.useState(false);
  const [openReject, setOpenReject] = React.useState(false);
  const handleOpen = () => setOpen(!open);
  const [openUpdate, setOpenUpdate] = React.useState(false);
  const handleOpenUpdate = () => setOpenUpdate(!openUpdate);
  const handleOpenReject = () => setOpenReject(!openReject);
  const [price, setPrice] = useState(0);
  const navigate = useNavigate();
  const statusOptions = [
    {
      value: "PENDING",
      label: "Inväntar",
    },
    {
      value: "REJECTED",
      label: "Nekad",
    },
    {
      value: "ACCEPTED",
      label: "ACCEPTED",
    },
  ];

  const [columnDefs] = useState([
    { headerName: "Sno", field: "sNo", width: 80 },
    { headerName: "Förnamn", field: "firstName", width: 150 },
    { headerName: "Event", field: "event.name", width: 170 },
    {
      headerName: "Datum",
      field: "createdAt",
      width: 150,
      cellRendererFramework: (props: any) => (
        <div>{moment(props.data.createdAt).format("YYYY-MM-DD")}</div>
      ),
    },
    { headerName: "Antal", field: "numberOfAttendees", width: 80 },
    { headerName: "Status", field: "quotationStatus", width: 180 },
    { headerName: "Pris", field: "price", width: 120 },
    {
      headerName: "Åtgärder",
      cellRendererFramework: (props: any) => (
        <section className="flex items-center justify-between w-full">
          <div className="flex flex-row justify-between items-center">
            <button
              onClick={() => {
                setActiveRowValues(props.data);
                handleOpen();
              }}
              className="mr-4"
            >
              <img
                src={eyeIcon}
                className="border border-[#E2E2EA] p-2 rounded-[10px]"
              />
            </button>
            <button
              onClick={() => {
                setActiveRowValues(props.data);
                navigate("/customer-messages", { state: { key: props.data } });
              }}
              className="mr-4"
            >
              {/* <a
                href={`mailto:${
                  props.data.email
                }?subject=${`Sub : ${props.data.event.name} event quotation price regards`}&body=${
                  props.data.message
                }`}
                target="_blank"
                rel="noopener noreferrer"
                className="flex items-center"
              > */}
              <img
                src={mail}
                className="border border-[#E2E2EA] p-1.5 rounded-[10px]"
              />
              {/* </a> */}
            </button>
            {props?.data?.quotationStatus === "PENDING" ? (
              <button
                onClick={() => {
                  setActiveRowValues(props.data);
                  handleOpenUpdate();
                }}
                className="border border-[#E2E2EA] p-[5px] rounded-[10px]"
              >
                <IoMdCheckboxOutline className="h-[20px] w-[20px]  rounded-xl text-[#42d49c] hover:text-[#277c5b] cursor-pointer" />
              </button>
            ) : null}
            <button
              onClick={() => {
                setActiveRowValues(props.data);
                handleOpenReject();
              }}
            >
              <img
                src={eventdeleteicon}
                className="border mx-4 border-[#E2E2EA] rounded-[10px] p-1"
                width={32}
                height={32}
              />
            </button>
          </div>
        </section>
      ),
      width: 420,
    },
  ]);
  const [activeStatus, setActiveStatus] = useState(statusOptions[0]);
  const [activeRowValues, setActiveRowValues] = useState<Quotation>();
  console.log(activeRowValues, "activeRowValues");
  const CustomDropdownIndicator = () => (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        style={{ width: "24px", height: "24px" }}
      >
        <path fill="none" d="M0 0h24v24H0z" />
        <path fill="#92929D" d="M7 10l5 5 5-5z" />
      </svg>
    </div>
  );

  const customStyles = {
    control: (provided: any, state: any) => ({
      ...provided,
      border: "1px solid #ccc",
      borderRadius: "4px",
      fontSize: "13px",
      width: "180px",
    }),
    option: (provided: any, state: any) => ({
      ...provided,
      backgroundColor: "transparent",
      fontSize: "13px",
      width: "180px",
      color: state.isSelected ? "#D6B27D" : "#8C7E79",
    }),
    singleValue: (provided: any) => ({
      ...provided,
      color: "#8C7E79", // Set the initial text color to gray
    }),
    valueContainer: (provided: any) => ({
      ...provided,
      color: "#8C7E79", // Set the initial text color to gray
    }),
  };
  const customComponents: any = {
    DropdownIndicator: CustomDropdownIndicator,
  };
  const gridOptions = {
    getRowHeight: function (params: any) {
      return 50;
    },
    defaultColDef: {
      cellStyle: { fontFamily: "Montserrat" },
    },
  };

  const setGridRowData = (data?: Quotation[]) => {
    const rowData = data || quotationList;
    if (rowData?.length > 0) {
      const filteredValue = rowData.filter(
        (row: Quotation) => row.quotationStatus === activeStatus.value
      );
      const newArr = filteredValue.map((item, index) => ({
        ...item,
        sNo: index + 1,
      }));

      setQuotationListFiltered(newArr);
    }
  };
  const getQuotationList = () => {
    // setIsLoading(true);
    commonAxios.get("/cemo/quotation?SUPPLIER=supplier").then(
      (data) => {
        const indexedData = data.data?.map((row: any, index: number) => ({
          ...row,
          sno: index + 1,
        }));
        setQuotationList(indexedData);
        setGridRowData(indexedData);
        setIsLoading(false);
      },
      (error) => {
        setIsLoading(false);
      }
    );
  };

  const handleStatuschange = (event: any) => {
    setActiveStatus(event);
  };

  const handleupdateprice = async () => {
    const URL = `/cemo/quotation/update?quotationId=${activeRowValues?.id}&price=${price}&status=PENDING`;
    try {
      const response = await commonAxios.put(URL);
      if (response.data) {
        handleOpenUpdate();
        successNotify("Price updated");
        getQuotationList();
      }
    } catch (error) {
      failureNotify("Price not updated, try again");
    }
  };

  const handlereject = async () => {
    const URL = `/cemo/quotation/update?quotationId=${activeRowValues?.id}&status=REJECTED`;
    try {
      const response = await commonAxios.put(URL);
      if (response.data) {
        handleOpenReject();
        successNotify("Quotation updated");
        getQuotationList();
      }
    } catch (error) {
      failureNotify("Quotation not updated, try again");
      handleOpenReject();
    }
  };

  useEffect(() => {
    getQuotationList();
  }, []);

  useEffect(() => {
    setGridRowData();
  }, [activeStatus]);
  return (
    <div className="flex">
      <SideNav />
      <div className="w-[85%] h-screen overflow-scroll bg-[#fafafb]">
        <AdminHeader />
        <div className="mx-0">
          {/* {isLoading && <Loader />} */}
          <Dialog open={open} handler={handleOpen} size="lg" className="p-5">
            <DialogHeader className="text-[#898888] justify-center">
              Kunduppgifter
            </DialogHeader>
            <DialogBody className="text-[#898888] overflow-scroll h-[30rem]">
              <Typography variant="h5" className="mb-2">
                Quote Details
              </Typography>

              <section className="flex flex-col gap-2 rounded">
                <div className="border p-5">
                  <div className="flex flex-row w-full items-center">
                    <section className="w-1/2 gap-4">
                      <Typography variant="h6">Eventnamn</Typography>
                      <Typography variant="paragraph" className="">
                        {activeRowValues?.event.name}
                      </Typography>
                    </section>
                    <section className="w-1/2 gap-4">
                      <Typography variant="h6">Event city name</Typography>
                      <Typography variant="paragraph" className="mt-2">
                        {
                          activeRowValues
                            ? activeRowValues?.event.city
                              ?.charAt(0)
                              .toUpperCase() +
                            activeRowValues?.event.city
                              ?.slice(1)
                              .toLowerCase()
                            : null /* or some other value, like "City Not Available" */
                        }{" "}
                      </Typography>
                    </section>
                  </div>
                  <div className="flex flex-row w-full items-center">
                    <section className="w-1/2 gap-4 mt-2">
                      <Typography variant="h6">Booking date</Typography>
                      <Typography variant="paragraph" className="mt-2">
                        {activeRowValues?.bookingDate}
                      </Typography>
                    </section>

                    <section>
                      <Typography variant="h6">Number of attendees</Typography>
                      <Typography variant="paragraph" className="mt-2">
                        {activeRowValues?.numberOfAttendees}
                      </Typography>
                    </section>
                  </div>
                  <div className="flex flex-row w-full items-center">
                    <section className="gap-4 mt-2">
                      <Typography variant="h6">Quotation message</Typography>
                      <Typography variant="paragraph" className="mt-2">
                        {activeRowValues?.message &&
                          activeRowValues.message.length > 0
                          ? activeRowValues.message
                          : "NA"}
                      </Typography>
                    </section>
                  </div>
                </div>
                <Typography variant="h5" className="mb-2">
                  Kunduppgifter
                </Typography>
                <div className="border p-5">
                  <div className="flex flex-row w-full items-center">
                    <section className="w-1/2 mt-2 gap-4">
                      <Typography variant="h6">Förnamn</Typography>
                      <Typography variant="paragraph" className="mt-2">
                        {activeRowValues?.firstName!}
                      </Typography>
                    </section>
                    <section className="w-1/2 gap-4 mt-2">
                      <Typography variant="h6">Efternamn</Typography>
                      <Typography variant="paragraph" className="mt-2">
                        {activeRowValues?.lastName}
                      </Typography>
                    </section>
                  </div>
                  <div className="flex flex-row w-full items-center">
                    <section className="w-1/2 gap-4 mt-2">
                      <Typography variant="h6">E-post</Typography>
                      <Typography variant="paragraph" className="mt-2">
                        {activeRowValues?.email}
                      </Typography>
                    </section>
                    <section className="w-1/2  gap-4 mt-2">
                      <Typography variant="h6">Phone number</Typography>
                      <Typography variant="paragraph" className="mt-2">
                        {activeRowValues?.phoneNumber}
                      </Typography>
                    </section>
                  </div>
                </div>
              </section>
            </DialogBody>
          </Dialog>
          <Dialog open={openUpdate} handler={handleOpenUpdate}>
            <DialogHeader className="text-secondary">
              Godkänn förfrågan
            </DialogHeader>
            <DialogBody>
              <input
                placeholder="Pris"
                type="number"
                onChange={(event: any) => setPrice(Number(event.target.value))}
                className="border focus:border-golden w-full focus:outline-none p-2 rounded-md"
              />
            </DialogBody>
            <DialogFooter>
              {/* <Button btnName={"Besök"} /> */}
              <div className="flex p-2 text-center font-semibold rounded-lg bg-[#D6B37D] text-white px-3">
                <button
                  onClick={handleupdateprice}
                  className="rounded-lg text-white mx-4 uppercase font-bold"
                >
                  Lämna pris
                </button>
              </div>
            </DialogFooter>
          </Dialog>
          <Dialog
            className="py-8 px-2"
            open={openReject}
            handler={handleOpenReject}
          >
            <DialogHeader className="text-sm  justify-center ">
              Are you sure you want to reject the quote?
            </DialogHeader>
            <DialogFooter className="flex flex-row justify-center gap-10 w-full">
              <Button
                onClick={() => {
                  handlereject();
                }}
                variant="outlined"
                style={{
                  color: "red",
                  paddingBlock: "0.5rem",
                  border: "1px solid red",
                }}
              >
                Ja
              </Button>
              <Button
                onClick={() => {
                  handleOpenReject();
                }}
                variant="outlined"
                style={{
                  color: "#D6B27D",
                  paddingBlock: "0.5rem",
                  border: "1px solid #D6B27D",
                }}
              >
                No
              </Button>
            </DialogFooter>
          </Dialog>
          <div className="border border-[#FFFFFF] bg-[#ffff] rounded-[20px] p-4 mt-1">
            <div className="flex justify-between items-center">
              <h1 className="text-secondary text-[24px] font-semibold">
                Offerter
              </h1>
              <div>
                <Select
                  options={statusOptions}
                  placeholder={"Quotation status"}
                  styles={customStyles}
                  defaultValue={activeStatus}
                  onChange={handleStatuschange}
                  components={customComponents}
                />
              </div>
            </div>
            <div className="pt-6">
              <GridTable
                columnDefs={columnDefs}
                gridOptions={gridOptions}
                rowData={quotationListFiltered}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default QuotationList;
