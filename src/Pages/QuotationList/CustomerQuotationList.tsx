import React, { useEffect, useState } from "react";
import searchIcon from "../../assets/adminSearch.svg";
import GridTable from "../../Components/GridTable/GridTable";
import Select from "react-select";
import moment from "moment";
import { commonAxios } from "../../Axios/service";
import eyeIcon from "../../assets/viewIcon.svg";
import { useNavigate } from "react-router-dom";
import { Dialog, DialogBody, DialogHeader, Typography } from "@material-tailwind/react";

const statusOptions:any = [
  { value: "PENDING", label: "Inväntar" },
  { value: "ACCEPTED", label: "Accepted" },
  { value: "REJECTED", label: "Nekad" },
];

const CustomerQuotationList = () => {
  const [rowHeight, setRowHeight] = useState(50);
  const [rowData,setRowData] = useState<any>(null);
  const [filterData, setFilterData] = useState("");
  const [activeStatus, setActiveStatus] = useState<any>(statusOptions[0]);
  const [activeRowValues, setActiveRowValues] = useState<any>();
  const [quotationListFiltered, setQuotationListFiltered] = useState<any>([]);
  const [open,setOpen] = useState(false)
  const handleOpen = () => setOpen(!open);
  const navigate = useNavigate();

  const columnDefs = [
    { headerName: "sNo", field: "sNo", width: 80 },
    { headerName: "Förnamn", field: "firstName", width: 150 },
    { headerName: "Event", field: "event.name", width: 170 },
    { headerName: "Datum", field: "date", width: 150 ,cellRendererFramework: (props: any) => (
        <div>{moment(props.data.createdAt).format('YYYY-MM-DD')}</div>
      ),},
    { headerName: "Antal", field: "numberOfAttendees", width: 80 },
    { headerName: "Status", field: "quotationStatus", width: 180 },
    { headerName: "Pris", field: "price", width: 120 },
    {
      headerName: "Åtgärder",
      cellRendererFramework: (props: any) => (
          <section className="flex items-center justify-between w-full">
              <div className="flex flex-row justify-between items-center">
                  <button
                      onClick={() => {
                          setActiveRowValues(props.data);
                          handleOpen();
                      }}
                      className="mr-4"
                  >
                      <img
                          src={eyeIcon}
                          className="border border-[#E2E2EA] p-2 rounded-[10px]"
                      />
                  </button>
                  
                  
              </div>
          </section>
      ),
      width: 420,
  }
  ];

  const gridOptions = {
    getRowHeight: function (params: any) {
      return 80;
    },
    defaultColDef: {
      cellStyle: { fontFamily: "Montserrat" },
    },
  };


  const customStyles = {
    control: (provided: any, state: any) => ({
      ...provided,
      border: "1px solid #ccc",
      borderRadius: "4px",
      fontSize: "13px",
      width: "180px",
    }),
    option: (provided: any, state: any) => ({
      ...provided,
      backgroundColor: "transparent",
      fontSize: "13px",
      width: "180px",
      color: state.isSelected ? "#D6B27D" : "#8C7E79",
    }),
    singleValue: (provided: any) => ({
      ...provided,
      color: "#8C7E79", // Set the initial text color to gray
    }),
    valueContainer: (provided: any) => ({
      ...provided,
      color: "#8C7E79", // Set the initial text color to gray
    }),
  };

  const CustomDropdownIndicator = () => (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        style={{ width: "24px", height: "24px",fill: '#A09792' }}
      >
        <path fill="none" d="M0 0h24v24H0z" />
        <path d="M7 10l5 5 5-5z" />
      </svg>
    </div>
  );

  const customComponents: any = {
    DropdownIndicator: CustomDropdownIndicator,
  };

  const handleStatuschange = (event: any) => {
    setActiveStatus(event);
  };

  const functionIdentifier = 'myFunction';
  console.log(activeStatus,"activeStatus")

  const setGridRowData = (data?: any) => {
    const rowDatas = data || rowData;
    if (rowDatas?.length > 0) {
        const filteredValue = rowDatas.filter((row: any) => row.quotationStatus === activeStatus.value)
        const newArr = filteredValue.map((item:any, index:any) => ({
            ...item,
            sNo: index + 1,
          }));
        setQuotationListFiltered(newArr)
    }
}

  useEffect(() => {
    setGridRowData();
}, [activeStatus,rowData])


  const getQuotationDetails = async () => {
    try {
      const response = await commonAxios.get('/cemo/quotation');
      if(response.data){

        const indexedData = response.data?.map((row: any, index: number) => ({
          ...row,
          sno: index + 1
      }));
        setRowData(indexedData)
      }
    } catch (error) {
      console.log(error)
    }
  }

  const handlePayment = () => {
    const bookingData = {
      event:activeRowValues.event,
      totalPrice:activeRowValues.numberOfAttendees * activeRowValues.price,
    }
    navigate('/payment',{
      state:{
        bookingData:bookingData,
        paymentData:activeRowValues,
        memberCount:activeRowValues.numberOfAttendees,
        functionIdentifier
      }
    });
  }

  const handleRejectOrder = async () => {
    try {
      const response = await commonAxios.put(`/cemo/quotation/update?quotationId=${activeRowValues?.id}&status=REJECTED`);
      if(response.status === 200){
        getQuotationDetails();
        handleOpen();
      }
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getQuotationDetails();
  },[])

  return (
    <div className="mx-0 my-6">
      <div className="flex justify-between items-center mx-4">
        <h1 className="text-secondary text-[24px] font-semibold">Offerter</h1>
        <div>
          <Select
            options={statusOptions}
            placeholder={"Quotation status"}
            styles={customStyles}
            defaultValue={activeStatus}
            onChange={handleStatuschange}
            components={customComponents}
          />
        </div>
      </div>
      <div className=" border border-[#FFFFFF] bg-[#ffff] rounded-[20px] p-4 mt-2">
        <div className="mt-6">
          <GridTable
            columnDefs={columnDefs}
            gridOptions={{
              ...gridOptions,
              getRowHeight: () => rowHeight,
            }}
            rowData={quotationListFiltered}
          />
        </div>
      </div>

      <Dialog open={open} handler={handleOpen} size="lg" className="p-5">
        <DialogHeader className="text-[#898888] justify-center">
          Kunduppgifter
        </DialogHeader>
        <DialogBody className="text-[#898888] overflow-scroll h-[30rem]">
          <Typography variant="h5" className="mb-2">
            Quote Details
          </Typography>
          <section className="flex flex-col gap-3 rounded">
          <div className="border p-5">
            <div className="flex flex-row w-full items-center">
              <section className="w-1/2 gap-4">
                <Typography variant="h6">Eventnamn</Typography>
                <Typography variant="paragraph" className="">
                  {activeRowValues?.event.name}
                </Typography>
              </section>
              <section className="w-1/2 gap-4">
                <Typography variant="h6">Event city name</Typography>
                <Typography variant="paragraph" className="mt-2">
                  {
                    activeRowValues
                      ? activeRowValues?.event.city?.charAt(0).toUpperCase() +
                        activeRowValues?.event.city?.slice(1).toLowerCase()
                      : null /* or some other value, like "City Not Available" */
                  }{" "}
                </Typography>
              </section>
            </div>
            <div className="flex flex-row w-full items-center">
              <section className="w-1/2 gap-4 mt-2">
                <Typography variant="h6">Booking date</Typography>
                <Typography variant="paragraph" className="mt-2">
                  {activeRowValues?.bookingDate}
                </Typography>
              </section>

              <section>
                <Typography variant="h6">Number of attendees</Typography>
                <Typography variant="paragraph" className="mt-2">
                  {activeRowValues?.numberOfAttendees}
                </Typography>
              </section>
            </div>
            <div className="flex flex-row w-full items-center">
              <section className="w-1/2 gap-4 mt-2">
                <Typography variant="h6">Quotation message</Typography>
                <Typography variant="paragraph" className="mt-2">
                  {activeRowValues?.message &&
                  activeRowValues.message.length > 0
                    ? activeRowValues.message
                    : "NA"}
                </Typography>
              </section>

              <section className="gap-4 mt-2">
                <Typography variant="h6">Pris</Typography>
                <Typography variant="paragraph" className="mt-2">
                  {activeRowValues?.price}
                </Typography>
              </section>
            </div>
            </div>
            <Typography variant="h5" className="mb-2">Kunduppgifter</Typography>
            <div className="border p-5">
            <div className="flex flex-row w-full items-center">
              <section className="w-1/2 mt-2 gap-4">
                <Typography variant="h6">Förnamn</Typography>
                <Typography variant="paragraph" className="mt-2">
                  {activeRowValues?.firstName!}
                </Typography>
              </section>
              <section className="w-1/2 gap-4 mt-2">
                <Typography variant="h6">Efternamn</Typography>
                <Typography variant="paragraph" className="mt-2">
                  {activeRowValues?.lastName}
                </Typography>
              </section>
            </div>
            </div>
          </section>
          {(activeRowValues?.quotationStatus === "ACCEPTED" || activeRowValues?.quotationStatus === "REJECTED") ? null :
          <div className="mt-10">
                <Typography variant="h6">Quotation Status</Typography>
                <div className="p-3 rounded-lg mt-1">
                  <div className="flex flex-row gap-4">
                    <button
                      onClick={handlePayment}
                      className={`px-4 py-2 rounded-md uppercase text-white font-semibold cursor-pointer ${activeRowValues?.price === 0 ? "bg-gray-500 text-white" : " bg-green-700"} `}
                      disabled={activeRowValues?.price === 0}
                    >
                      Acceptera order
                    </button>
                    <button
                      onClick={handleRejectOrder}
                      className="px-4 py-2 bg-golden text-white rounded-md uppercase text-white font-semibold cursor-pointer"
                    >
                      Neka order
                    </button>
                  </div>
                </div>
              </div> 
              }
        </DialogBody>
      </Dialog>
    </div>
  );
};

export default CustomerQuotationList;
