import { useLocation, useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Footer from "../../Components/Footer/Footer";
import Header from "../../Components/Header/Header";
import { failureNotify, successNotify } from '../../Components/Toast/Toast';
import { commonAxios } from '../../Axios/service';

const QuotationRefectedSuccessfully = () => {
  const navigate = useNavigate();
  const [rejected, setIsRejected] = useState(false);
  const location = useLocation();

  // Parse the query parameters
  const searchParams = new URLSearchParams(location.search);

  // Access the 'id' query parameter
  const id = searchParams.get('id')
  const goToLandingPage = () => {
    navigate("/");
  }
  //   const goToOderpage = () => {
  //     navigate("/ordershistory");
  //   }

  const handlerejectprice = async () => {
    const URL = `cemo/quotation/reject?quotationId=${id}&price=0&status=REJECTED`
    try {
      const response = await commonAxios.put(URL);
      if (response.data) {
        setIsRejected(true);
        successNotify('Quote rejected successfully');

      }
    } catch (error) {
      failureNotify('Price not updated, try again')
    }
  }

  useEffect(() => {
    if(id){
      handlerejectprice()
    }
  }, [id])

  return (
    <>
      <Header />
      <div className="flex relative rounded-lg flex-col items-center justify-center h-[50vh]">
        {rejected &&
          <section className='flex flex-col items-center justify-center absolute z-40 bg-white bg-opacity-30 backdrop-filter backdrop-blur-lg border border-gray-300 rounded-lg p-10'>
            <div className='flex'>
              <h1 className="text-4xl md:text-3xl font-bold text-secondary mt-8">
                Your Quotation Rejected successfully!
              </h1>
            </div>
            <p className="text-secondary text-2xl font-semibold my-8">
              Thank you for your response
            </p>
            <div className='flex'>
              <button
                onClick={goToLandingPage}
                className="bg-[#D6B27D] rounded-lg text-white uppercase p-2 px-6 md:px-[1.65rem] flex text-[14px] md:text-lg md:font-semibold"
              >
                Back to Home page
              </button>
              {/* <button
            onClick={goToOderpage}
            className="bg-[#D6B27D] rounded-lg ml-16 text-white uppercase p-2 px-6 md:px-[1.65rem] flex text-[14px] md:text-lg md:font-semibold"
          >
            View orders
          </button> */}
            </div>
          </section>
        }

        {!rejected &&
          <section className='flex flex-col items-center justify-center absolute z-40 bg-white bg-opacity-30 backdrop-filter backdrop-blur-lg border border-gray-300 rounded-lg p-10'>
            <div className="spinner-border" role="status">
              <span className ="visually-hidden">Loading...</span>
            </div>      </section>
        }
      </div>
      <Footer />
    </>
  );
};

export default QuotationRefectedSuccessfully;
