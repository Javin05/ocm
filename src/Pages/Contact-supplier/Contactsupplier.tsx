import React, { useState, useEffect } from 'react'
import { commonAxios } from '../../Axios/service'
import { setNotification } from '../../Redux/features/notificationSlice'
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'
import { RootState } from '../../Redux/store'
import { failureNotify } from '../../Components/Toast/Toast'
import SideNav from '../../Components/SideNav/SideNav'
import AdminHeader from '../../Components/AdminHeader/AdminHeader'

export interface Message {
    id: string
    booking: any
    customer: any
    supplier: Supplier
    admin: Admin
    message: string
    isCustomerRead: boolean
    isSupplierRead: boolean
    isAdminRead: boolean
    createdDate: string
    updatedDate: string
    isCustomerMessage: boolean
    isSupplierMessage?: boolean
}

export interface Supplier {
    id: string
    firstName: string
    lastName: string
    mobile: string
    telephone: string
    email: string
    businessName: string
    businessNumber: string
    businessAddress: string
    businessPinCode: string
    businessCity: string
    password: string
    accountStatus: string
    createdAt: string
    updatedAt: string
    categories: string
    profileImage: string
    resetToken: any
}

export interface Admin {
    id: string
    firstName: string
    lastName: string
    mobile: string
    telephone: string
    email: string
    businessName: string
    businessNumber: string
    businessAddress: string
    businessPinCode: string
    businessCity: string
    password: string
    accountStatus: string
    createdAt: string
    updatedAt: string
    categories: string
    profileImage: string
    resetToken: string
}


const Contactsupplier = () => {
    const userInfo = useSelector((state: RootState) => state.user.userDetails)
    const [messages, setMessages] = useState<Message[]>([]);
    const [messagedSuppliers, setMessagedSuppliers] = useState<any>([]);
    const [activeSupplier, setActiveSupplier] = useState<string>('');
    const [newMessage, setNewMessage] = useState("");
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const dispatch = useDispatch()
    const handlenewcomment = (e: any) => {
        setNewMessage(e.target.value);
    };

    const addComment = async () => {
        try {
            const payload = {
                "message": newMessage,
                "customerId": null,
                "supplierId": activeSupplier,
                "adminId": "admin",
                "isCustomerMessage": false,
                "isSupplierMessage": false
            };
            const response = await commonAxios.post("/cemo/messages", payload);
            if (response.data) {
                getMessages();
                setNewMessage("");
                setActiveSupplier(activeSupplier)
            }
        } catch (error) {
            failureNotify("oops! something went wrong");
        }
    };

    const sortAndSetMessages = (messagesArray: Message[], currSupplier: string) => {
        messagesArray.sort((a, b) => {
            const dateA = moment(a.updatedDate, 'YYYY-MM-DD HH:mm:ss');
            const dateB = moment(b.updatedDate, 'YYYY-MM-DD HH:mm:ss');
            return dateA.diff(dateB);
        });
        setMessages(messagesArray.filter((message: Message) => message.supplier.id === currSupplier));
        setIsLoading(false)
    }

    const setUniqueSuppliers = async (messageArray: Message[]) => {

        let unreadMessages = messageArray.filter((readMessage: any) => {
            return readMessage.isAdminRead === false
        })


        let unreadtIds = unreadMessages.map((val: any) => {
            return val.id
        })

        let payload = {
            id: unreadtIds,
            customerId: null,
            supplierId: null,
            adminId: userInfo.id
        }


        let updatedMessageResponse = await commonAxios.put(
            `/cemo/messages/update/status`,
            payload
        );

        if (updatedMessageResponse.data) {
            if (userInfo.role === "ROLE_ADMIN") {
                try {
                    const messageCount = await commonAxios.get(
                        `cemo/appuser/notification/count?adminId=${userInfo.id}`
                    );
                    if (messageCount.data) {
                        dispatch(setNotification(messageCount.data));
                    }
                } catch (e) { }
            }
        }
        if (activeSupplier === '') {
            const uniqueSuppliers = [...new Set(messageArray.map((message: Message) => message.supplier.id))];
            const uniqueSuppliersSet = new Set();
            const finalSuppliersNameList = messageArray?.filter((message: Message) => {
                if (!uniqueSuppliersSet.has(message.supplier.id)) {
                    uniqueSuppliersSet.add(message.supplier.id);
                    return true
                }
                return false
            })?.map((message: Message) => ({
                name: message.supplier.businessName,
                id: message.supplier.id
            }));

            setMessagedSuppliers(finalSuppliersNameList);
            setActiveSupplier(finalSuppliersNameList[0].id);
            sortAndSetMessages(messageArray, uniqueSuppliers[0])
        } else {
            sortAndSetMessages(messageArray, activeSupplier)
        }
    };

    const getMessages = async () => {
        try {
            setIsLoading(true)
            const response = await commonAxios.get(
                `/cemo/messages/admin`
            );
            if (response.data) {
                setUniqueSuppliers(response.data)
            }
        } catch (error) {

        } finally {

        }
    };

    useEffect(() => {
        setIsLoading(true)
        getMessages()
    }, [activeSupplier])

    return (
        <div className="flex">
            {/* {isLoading ? <Loader /> : null} */}
            <SideNav />
            <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
                <AdminHeader />


                <div className="mx-10 my-6">
                    {/* <div className="flex flex-row justify-start items-center">
                    <h1 className="text-secondary text-[20px] font-semibold">
                        Messages
                    </h1>
                </div> */}
                    <div className="bg-[#fff] p-4 m-6 h-[85vh] max-h-[90vh] flex flex-row">
                        <section className="w-1/3 py-4 px-10 h-full border-[#e3e3e3] border rounded-[10px] overflow-y-scroll">
                            <h2 className="font-bold font-[sofiapro] my-6 text-2xl text-secondary">
                                Suppliers
                            </h2>

                            {messagedSuppliers.length === 0 ? (
                                <div className="flex justify-center items-center">
                                    <p className="font-bold text-lg text-[#D6B27D]">
                                        Inga meddelanden
                                    </p>
                                </div>
                            ) : (
                                messagedSuppliers?.map((item: any) => (
                                    <p
                                        className={`cursor-pointer font-bold px-6 py-3 rounded-md transition-all my-2 text-secondary  ${activeSupplier === item.id
                                            ? "bg-[#D6B27D] text-white"
                                            : "bg-transparent text-[#171725]"
                                            }`}
                                        onClick={() => setActiveSupplier(item.id)}
                                    >
                                        {item.name}
                                    </p>
                                ))
                            )}
                        </section>
                        <section className="w-2/3 h-full">
                            <div className="w-4/5 h-[80%] overflow-y-scroll flex flex-col mx-auto border border-[#e3e3e3] rounded-[10px]">
                                {messages.length > 0 ? (
                                    messages.map((message: Message) =>
                                        message.isSupplierMessage ? (
                                            <div className="flex flex-row items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                                                <section className="w-3/4">
                                                    <div className="flex justify-start">
                                                        {message.supplier?.profileImage ? (
                                                            <img
                                                                className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                                                src={message.supplier?.profileImage}
                                                            ></img>
                                                        ) : (
                                                            <span className="h-9 w-9 p-2 rounded-full flex justify-center items-center font-semibold bg-[#D6B27D] text-white mr-2 mb-2">
                                                                {message.supplier?.firstName
                                                                    .split("")
                                                                    .at(0)
                                                                    ?.toUpperCase() +
                                                                    "" +
                                                                    message.supplier?.lastName
                                                                        .split("")
                                                                        .at(0)
                                                                        ?.toUpperCase()}
                                                            </span>
                                                        )}
                                                        <p className="text-[#D6B27D] font-medium mt-1.5">
                                                            {message?.supplier?.firstName}{" "}
                                                            {message?.supplier?.lastName}
                                                        </p>
                                                    </div>
                                                    <h4 className="text-sm text-left text-secondary">
                                                        {message?.message}
                                                    </h4>
                                                </section>
                                                <span className="w-1/4 text-sm text-secondary">
                                                    {moment(message?.createdDate).format(
                                                        "ddd, MMM DD YYYY"
                                                    )}
                                                </span>
                                            </div>
                                        ) : (
                                            <div className="flex flex-row justify-between items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                                                <span className="text-sm text-right text-secondary">
                                                    {moment(message?.createdDate).format(
                                                        "ddd, MMM DD YYYY"
                                                    )}
                                                </span>
                                                <section className="w-3/4">
                                                    <div className="flex flex-row justify-end">
                                                        {message.admin?.profileImage ? (
                                                            <img
                                                                className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                                                src={message.admin?.profileImage}
                                                            ></img>
                                                        ) : (
                                                            <span className="h-12 w-12 p-2 rounded-md font-semibold bg-[#D6B27D] text-secondary">
                                                                {message.admin?.firstName
                                                                    .split("")
                                                                    .at(0)
                                                                    ?.toUpperCase() +
                                                                    "" +
                                                                    message.admin?.lastName
                                                                        .split("")
                                                                        .at(0)
                                                                        ?.toUpperCase()}
                                                            </span>
                                                        )}
                                                        <p className="text-[#D6B27D] font-medium mt-1.5">
                                                            {message?.admin?.firstName}{" "}
                                                            {message?.admin?.lastName}
                                                        </p>
                                                    </div>
                                                    <h4 className="text-sm text-right text-secondary">
                                                        {message?.message}
                                                    </h4>
                                                </section>
                                            </div>
                                        )
                                    )
                                ) : (
                                    <div className="justify-center py-4 px-44 text-[#D6B37D]">
                                        Inga meddelanden
                                    </div>
                                )}
                            </div>
                            <div className="w-4/5 mx-auto mt-2">
                                <textarea
                                    placeholder="Dina kommentarer..."
                                    value={newMessage}
                                    onChange={handlenewcomment}
                                    className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md"
                                    name="comment"
                                    id="comment"
                                    rows={3}
                                />
                            </div>
                            <div className="w-4/5 mx-auto">
                                <div className='flex justify-center'>
                                    <button
                                        onClick={addComment}
                                        className="bg-[#D6B27D] outline-none w-auto cursor-pointer uppercase text-white mb-3 font-semibold rounded-md px-6 text-lg py-2"
                                    >
                                        SKICKA
                                    </button>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Contactsupplier