import React, { useContext, useEffect, useState } from "react";
import ClockImg from "../../assets/Clock.svg";
import clockWhiteImg from "../../assets/ClockWhite.svg";
import Customcalendar from "../../Components/CustomCalendar/Customcalendar";
import {
  createSearchParams,
  useLocation,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import { commonAxios } from "../../Axios/service";
import Loader from "../../Components/Loader/Loader";
import RelatedProductCard from "../../Components/RelatedProductCard/RelatedProductCard";
import { addProductToCart } from "../../Redux/features/productCartSlice";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import BreadCrumb from "../../Components/BreadCrumb/BreadCrumb";
import moment from "moment";
import { IoIosHeartEmpty, IoIosSend } from "react-icons/io";
import { AuthContext } from "../RootLayout/RootLayout";
import { HiHeart } from "react-icons/hi";
import { AiFillHeart } from "react-icons/ai"
import RatingBar from "../../Components/RatingBar/RatingBar";
import { Tooltip } from "@material-tailwind/react";
import MapView from "../../Components/Mapview/MapView";
import { MdOutlineIosShare } from "react-icons/md";
import notFavHeart from "../../assets/latestNotfav.png";
import share from "../../assets/share.png";
import { getWishlistCount } from "../../Redux/features/wishlistSlice";
import './BookingScreen.css'
import _ from "lodash";


export interface Event {
  id: string;
  name: string;
  description: string;
  city: string;
  image: string;
  appUser: AppUser;
  category: Category;
  pincode: string;
  createdAt: string;
  updatedAt: string;
  price: number;
  status: string;
  eventNumber: string;
  operatingHours: OperatingHour[];
  startDate: string;
  endDate: string;
  latitude: any;
  longitude: any;
}

export interface AppUser {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
}

export interface Category {
  id: string;
  categoryName: string;
  categoryPrice: number;
  image: any;
  createdAt: string;
  status: string;
}

export interface OperatingHour {
  id: string;
  weekDays: string;
  startTime: string;
  endTime: string;
  hourSlot: string;
  operatingDate: string;
  slot: Slot[];
}

export interface Slot {
  id: string;
  inTime: string;
  outTime: string;
  maxAvailableCount: number;
  bookedCount: number;
  slotTime: string;
}

const BookingScreen = (getaquote: any) => {
  const [time, setTime] = useState("");
  const [date, setDate] = useState<any>(new Date());
  const [selectedDay, setSelectedDay] = useState("");
  const [opsList, setOpsList] = useState<any>([]);
  const [defaultRatings, setDefaultRatings] = useState<any>();
  const [productsList, setProductsList] = useState<any>([]);
  const [newComment, setNewComment] = useState("");
  const [clickOpen, setClickOpen] = useState(false)
  const [datesBookingStatus, setDatesBookingStatus] = useState<any>([]);
  const [datesAvailableBookingStatus, setDatesAvailableBookingStatus] =
    useState<any>([]);
  const [ratingCount, setRatingCount] = useState()
  const [eventData, setEventData] = useState<any>({});
  const [isLoading, setIsLoading] = useState(false);
  const [bookingLoading, setBookingLoading] = useState(false);
  const [slotdetails, setSlotDetails] = useState<any>({});
  const [bookedCount, setBookedCount] = useState<number>(0);
  const [availableCount, setAvailableCount] = useState<number>();
  const [displayCount, setDisplayCount] = useState<number>();
  const [isValidCount, setIsValidCount] = useState<boolean>(true);
  const [showFullDescription, setShowFullDescription] = useState(false);
  const [message, setMessage] = useState<any>('')
  const [latLong, setLatLong] = useState({
    lat: 0.0,
    lng: 0.0,
  })
  const navigate = useNavigate();
  const locationData = useLocation();
  const [searchParams, setSearchParams] = useSearchParams();
  const { setIsDeleteEventModalOpen, setIsBookingWithOutLogIn } =
    useContext(AuthContext);
  const [isFavourite, setIsFavourite] = useState<boolean>(false);
  // const [event, setEvent] = useState();
  const dispatch = useDispatch();
  const detailsParams = {
    id: searchParams.get("id") as string,
    searchText: searchParams.get("searchText") as string,
  };

  const popularParams = {
    searchTitle: "",
    searchText: searchParams.get("searchText") as string,
  };

  const breadsList = [
    {
      name: "Startsida",
      naviagteTo: "/",
    },
    {
      name: "Populära aktiviteter",
      naviagteTo: popularParams.searchText
        ? `/popular?${createSearchParams(popularParams)}`
        : `/popular?searchTitle=&searchText=${eventData?.appUser?.businessCity}`,
    },
    {
      name: "Detaljer",
      naviagteTo: `/popular?searchTitle=&searchText=${eventData?.appUser?.businessCity}&category=${eventData?.category?.categoryName}`,
    },
    {
      name: "Boka",
    },
  ];

  const userInfo = useSelector((state: RootState) => state.user.userDetails);

  const toggleDescription = () => {
    setShowFullDescription(!showFullDescription);
  };

  const handleClick = (data: any) => {
    setIsValidCount(true);
    setBookedCount(0);
    setTime(data.slotTime);
    setSlotDetails(data);
    setAvailableCount(data?.maxAvailableCount - data?.bookedCount);
    setDisplayCount(data?.maxAvailableCount - data?.bookedCount);
  };

  const updateBookedDates = (eventDetailObj: Event) => {
    const fullyBookedDates = eventDetailObj.operatingHours
      .map((item: OperatingHour) => {
        if (
          item.slot.every(
            (value: Slot) => value.bookedCount == value.maxAvailableCount
          )
        ) {
          return {
            date: item.operatingDate,
            isBooked: true,
          };
        } else {
          return {
            date: item.operatingDate,
            isBooked: false,
          };
        }
      })
      ?.filter((item: any) => item.isBooked)
      ?.map((item: any) => moment(item.date).format("DD/MM/YYYY"));

    const availableBookedDates = eventDetailObj.operatingHours
      .map((item: OperatingHour) => {
        if (
          item.slot.every(
            (value: Slot) => value.bookedCount >= value.maxAvailableCount
          )
        ) {
          return {
            date: item.operatingDate,
            isBooked: true,
          };
        } else {
          return {
            date: item.operatingDate,
            isBooked: false,
          };
        }
      })
      ?.filter((item: any) => !item.isBooked)
      ?.map((item: any) => moment(item.date).format("DD/MM/YYYY"));
    setDatesAvailableBookingStatus(availableBookedDates);
    setDatesBookingStatus(fullyBookedDates);
    setIsLoading(false);
  };

  const getEventDetails = async () => {
    setIsLoading(true);
    setBookingLoading(true)
    const response = await commonAxios
      .get(`/cemo/events/${searchParams.get("id")}`)
      .then(
        (data) => {
          if (data.data) {
            if (!_.isEmpty(data && data.data && data.data.errorCode)) {
              setIsLoading(false);
              failureNotify(data && data.data && data.data.errorErrorMessage);
            } else {
              setEventData(data.data);
              updateBookedDates(data.data);
              getCoordinates(data.data.city)
            }
          }
          setIsLoading(false);
          setBookingLoading(false)
        },
        (error) => {
          setIsLoading(false);
        }
      );

  };

  function getCoordinates(cityName: string) {
    const apiKey = 'AIzaSyDYsIF0ap_NrOL3xyjf1yB-3IkhN0O4lSI';
    const apiUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(cityName)}&key=${apiKey}`;

    // Send an HTTP GET request
    fetch(apiUrl)
      .then(response => response.json())
      .then(data => {
        if (data.status === 'OK' && data.results.length > 0) {
          const location = data.results[0].geometry.location;
          const latitude = location.lat;
          const longitude = location.lng;
          setLatLong({
            lat: latitude,
            lng: longitude
          })
        } else {
        }
        if (data.status === 'REQUEST_DENIED') {
          setMessage(data.error_message)
        }
      })
      .catch(error => {

      });
  }

  const getProductList = async () => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get(
        `/cemo/products/get/${userInfo.id}`
      );
      if (response.data) {
        setProductsList(response.data);
      }
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  const getSlotsForDay = async (weekDay: string) => {
    setIsLoading(true);
    setBookedCount(0);
    setIsValidCount(true);
    try {
      const response = await commonAxios.get(
        `/cemo/events/${searchParams.get("id")}/date/${weekDay}`
      );
      if (response.data) {
        setOpsList(response.data);
        setTime(response.data?.at(0)?.slotTime);
        setSlotDetails(response.data?.at(0));
        if (
          response.data?.at(0)?.maxAvailableCount -
          response.data?.at(0)?.bookedCount <
          0
        ) {
          setAvailableCount(0);
          setDisplayCount(0);
        } else {
          setAvailableCount(
            response.data?.at(0)?.maxAvailableCount -
            response.data?.at(0)?.bookedCount
          );
          setDisplayCount(
            response.data?.at(0)?.maxAvailableCount -
            response.data?.at(0)?.bookedCount
          );
        }
      }
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  const setWeekDay = () => {
    const today = new Date();
    let monthFormatted =
      today.getMonth() + 1 < 10 ? `0${today.getMonth() + 1}` : today.getMonth() + 1;
    let dateFormatted =
      today.getDate() < 10 ? `0${today.getDate()}` : today.getDate();
    const weekDay = `${today.getFullYear()}-${monthFormatted}-${dateFormatted}`;
    setSelectedDay(weekDay);
    return weekDay;
  };

  const handleAddToCart = async (product: any) => {
    const updatedProduct = {
      appUserId: userInfo.id,
      eventId: searchParams.get("id"),
      bookingItemType: "PRODUCT",
      bookingDate: new Date()
        .toLocaleDateString()
        .split("/")
        .reverse()
        .join("-"),
      quantity: 1,
      productId: product.id,
    };
    try {
      const response = await commonAxios.post(
        "/cemo/bookingitems/new",
        updatedProduct
      );

      successNotify("Produkt lagd i varukorg");
    } catch (error) {
      failureNotify("oops! something went wrong, Try again");
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    const weekDay = setWeekDay();
    getEventDetails();
    getSlotsForDay(weekDay);
  }, []);

  const addComment = async () => {
    setIsLoading(true);
    const payload = {
      customerId: userInfo.id, //Not null
      supplierId: eventData?.appUser?.id, //Not null
      eventId: searchParams.get("id"), //It must be null if productId have a value
      productId: null, //It must be null if eventId have a value
      comments: newComment, //Not null
      rating: 2, //It can be null and value must be in 1 to 5
    };

    try {
      const response = await commonAxios.post("/cemo/comments/add", payload);
      if (response.data) {
        successNotify(`Comment added succesfully`);
        getEventDetails();
        setNewComment("");
      }
    } catch (error) {
      failureNotify("oops! something went wrong");
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    const handleEscPress = (event: KeyboardEvent) => {
      if (event.key === "Escape") {
        setIsDeleteEventModalOpen(false);
      }
    };

    document.addEventListener("keydown", handleEscPress);

    return () => {
      document.removeEventListener("keydown", handleEscPress);
    };
  });

  useEffect(() => {
    if (date) {
      const today = moment(date).format('YYYY-MM-DD');
      getSlotsForDay(today);

    }
  }, [date]);

  useEffect(() => {
    if (userInfo.id) {
      getFavouriteItems();

    }
  }, [isFavourite]);


  useEffect(() => {
    getReviews()
  }, [])

  const getReviews = async () => {
    let response;

    response = await commonAxios.get(
      `cemo/ratings/average/rating?productId=${searchParams.get(
        "id"
      )}`
    );

    setDefaultRatings(response.data);
    setRatingCount(response.data)

  }

  const getFavouriteItems = async () => {
    try {
      const response = await commonAxios.get(
        `cemo/favourite-items/favourite/${userInfo.id}/${searchParams.get(
          "id"
        )}`
      );
      setIsFavourite(response.data.isFavourite);
    } catch (e) {
    } finally {
    }
  };

  const orderBytime = (opsList: any) => {
    return opsList.slice().sort((a: any, b: any) => {
      return b.inTime.localeCompare(b.inTime);
    });
  };

  const togglefavourite = async (id: any, eventData: any) => {
    try {
      setIsLoading(true);
      const payload = {
        appUserId: userInfo.id,
        productId: null,
        eventId: id,
      };
      await commonAxios.post("/cemo/favourite-items", payload).then(
        (response) => {
          setIsFavourite(!response.data.deleted);
        },
        (error) => { }
      );
    } catch (error) {
    } finally {
      setIsLoading(false);
      dispatch(getWishlistCount(userInfo.id));
    }
  };

  const handlenewcomment = (e: any) => {
    setNewComment(e.target.value);
  };

  const redirectToChecking = () => {
    navigate("/checkout", {
      state: {
        memberCount: bookedCount,
        bookingSlotId: slotdetails?.id,
        bookingDate: date,
        eventId: eventData && eventData.id,
        eventName: eventData?.name,
        searchText: searchParams.get("searchText") as string,
      },
    });
  };

  const handleShareClick = async (id: any) => {
    try {
      let text = `${window.location.href}`
      await navigator.clipboard.writeText(text);
      successNotify('Link Copied')
    } catch (error) {
      failureNotify('the Link is Not Copied')
    }
  }
  const goToLandingPage = () => {
    navigate("/");
  }
  const goToOderpage = () => {
    navigate("/ordershistory");
  }


  const getSelectedTime = (item: any) => {
    const newDate = new Date(date);
    const year = newDate.getFullYear();
    const month = newDate.getMonth();
    const day = newDate.getDate();
    const [hours, minutes, seconds] = item.inTime.split(':').map(Number);
    return new Date(year, month, day, hours, minutes, seconds);
  }

  return (
    <>
      {
        bookingLoading ? <Loader /> :
          <>
            <section className="mt-10 -ml-6 text-secondary px-2">
              {eventData.name && eventData?.category?.categoryName && <BreadCrumb breadsList={breadsList} />}
            </section>
            {eventData.id ? (
              <div className="md:mx-14 mt-4 px-2 md:px-0">

                <section className="flex flex-row justify-start items-center gap-1 md:gap-2">
                  <div className="flex flex-col">
                    <h1 className="md:text-[35px] font-semibold px-1 font-[sofiapro] -ml-1 text-secondary">
                      {eventData?.name}
                    </h1>

                    <h1 className="text-[#D6B37E] md:text-[14px] font-semibold italic uppercase px-10 -ml-11">
                      -{eventData?.companyName}
                    </h1>

                    <div>
                      <div className="flex ">
                        <RatingBar ratings={defaultRatings} readonly={true} />
                        <p className="ml-3 mt-5  text-secondary text-sm">({ratingCount})</p>
                        <div
                          onClick={() => togglefavourite(searchParams.get("id"), eventData)}
                          className="cursor-pointer ml-3 flex justify-center "
                        >
                          {userInfo.id ? (
                            isFavourite ? (
                              <div className="rounded-full p-3 hover:bg-[#d7d1d1ef]">
                                <AiFillHeart className="h-6 w-6 text-[#D6B27D]" />
                              </div>
                            ) : (
                              <div className="rounded-full p-3 hover:bg-[#d7d1d1ef] ">
                                <img src={notFavHeart} className="h-7 w-7 z-40 text-white hover:text-[#D6B27D]" />
                              </div>
                            )
                          ) : null}
                        </div>
                        <div className="rounded-full p-3 hover:bg-[#d7d1d1ef] cursor-pointer" onClick={() => handleShareClick(searchParams.get("id"))}> <img src={share} className="h-6 w-6 text-[#D6B27D] z-40 " /></div>
                      </div>
                    </div>
                  </div>

                </section>
                <div className="lg:grid grid-cols-12 sm:gap-20 md:gap-10 lg:gap-5 sm:mx-1">
                  <div className="col-span-8 lg:col-span-6 mt-3">
                    <img
                      src={eventData.image}
                      className="w-auto sm:max-w-[30rem] md:max-w-[42rem] lg:max-w-[32rem] xl:w-[50rem] object-cover rounded-xl h-[580px]"
                      alt="Event Image"
                    />
                  </div>

                  <div
                    className="border mt-2 md:px-0 lg:px-0 rounded-2xl col-span-12 lg:col-span-3 lg:h-[100%]"
                    style={{ justifySelf: "baseline" }}
                  >
                    <p className="font-semibold text-[15px] mt-2 px-2 text-secondary">
                      Välj datum
                    </p>
                    <section className="flex justify-center items-center gap-8">
                      <div className="flex items-center gap-2">
                        <div style={{ backgroundColor: 'green', width: '12px', height: '12px', borderRadius: '50%', margin: '0px auto' }} />
                        <p>Tillgängliga tider</p>
                      </div>
                      <div className="flex items-center gap-2">
                        <div style={{ backgroundColor: 'red', width: '12px', height: '12px', borderRadius: '50%', margin: '0px auto' }} />
                        <p>Slots Booked</p>
                      </div>
                    </section>
                    <div className="my-2 mx-2 w-[97%]">
                      <Customcalendar
                        date={date}
                        datesBookingStatus={datesBookingStatus}
                        datesAvailableBookingStatus={datesAvailableBookingStatus}
                        setDate={setDate}
                        startDate={eventData.startDate}
                        endDate={eventData.endDate}
                      />
                    </div>
                    <div className="grid grid-cols-1 gap-2">
                      <div className="p-5">
                        {" "}
                        {opsList?.length > 0 && (
                          <h1 className="text-secondary mt-4 font-semibold text-[17px]">
                            Tillgängliga tider
                          </h1>
                        )}
                        {opsList?.length > 0 ? (
                          <>
                            <div className="flex flex-wrap mt-2 mb-1">
                              {orderBytime(opsList)
                                .slice()
                                .map((item: any) => {
                                  const combinedDateTime = getSelectedTime(item);
                                  return (
                                    <button
                                      className={`flex p-2 cursor-pointer border border-[#EAEBEF] mx-2 my-2 rounded-lg ${time === item.slotTime && "bg-[#D6B27D]"} ${combinedDateTime < new Date() && "bg-gray-400"}`}
                                      onClick={() => handleClick(item)}
                                      disabled={combinedDateTime < new Date()}
                                    >
                                      <img
                                        src={
                                          time === item.slotTime && combinedDateTime > new Date()
                                            ? clockWhiteImg
                                            : ClockImg
                                        }
                                        className="mr-2"
                                      />
                                      <p
                                        className={`${time === item.slotTime && combinedDateTime > new Date()
                                          ? "text-white"
                                          : "text-[#979797]"
                                          }`}
                                      >
                                        {item.slotTime}
                                      </p>
                                    </button>
                                  );
                                })}
                            </div>
                          </>
                        ) : (
                          <section className="text-center my-10 px-5">
                            <p className="font-bold text-xl mt-1 text-secondary">
                              Inga tider tillgängliga
                            </p>
                          </section>
                        )}
                      </div>
                    </div>
                    <div className="grid sm:grid-cols-1">
                      <div className="flex items-center justify-between">
                        <div className="mx-5 text-[#D6B37E] p-4">
                          Tillgängliga platser: {displayCount}
                        </div>
                        <div className="flex p-2">
                          {opsList?.length > 0 && (
                            <div className="ml-7 text-[#D6B37E] p-2">
                              Seats Count :
                            </div>
                          )}
                          <div className="pagination">
                            {opsList.length > 0 && (
                              <div>
                                <button
                                  disabled={bookedCount === 0}
                                  onClick={() => {
                                    setBookedCount((prev: any) => prev - 1);
                                    const availableSeats =
                                      Number(slotdetails.maxAvailableCount) -
                                      Number(slotdetails.bookedCount);
                                    if (
                                      Number(availableSeats) - Number(bookedCount - 1) < 0
                                    ) {
                                      setIsValidCount(false);
                                      setAvailableCount(0);
                                    } else if (
                                      Number(availableSeats) - Number(bookedCount - 1) === 0
                                    ) {
                                      setIsValidCount(true);
                                      setAvailableCount(0);
                                      setDisplayCount(0);
                                    } else {
                                      setAvailableCount(
                                        Number(availableSeats) - Number(bookedCount - 1)
                                      );
                                      setDisplayCount(
                                        Number(availableSeats) - Number(bookedCount - 1)
                                      );
                                      setIsValidCount(true);
                                    }
                                  }}
                                  className="pagination-button"
                                >
                                  -
                                </button>
                                <span className="vertical-left-line"></span>
                                <input
                                  className="w-12 mx-2 pl-[1.2rem] py-1 outline-none"
                                  type="number"
                                  name="count"
                                  id="count"
                                  min={0}
                                  max={displayCount}
                                  value={bookedCount}
                                  onChange={(event) => {
                                    setBookedCount(Number(event.target.value));
                                    const availableSeats =
                                      Number(slotdetails.maxAvailableCount) -
                                      Number(slotdetails.bookedCount);
                                    if (
                                      Number(availableSeats) - Number(event.target.value) <
                                      0
                                    ) {
                                      setIsValidCount(false);
                                      setAvailableCount(0);
                                    } else if (
                                      Number(availableSeats) - Number(event.target.value) ===
                                      0
                                    ) {
                                      setIsValidCount(true);
                                      setAvailableCount(0);
                                      setDisplayCount(0);
                                    } else {
                                      setAvailableCount(
                                        Number(availableSeats) - Number(event.target.value)
                                      );
                                      setDisplayCount(
                                        Number(availableSeats) - Number(event.target.value)
                                      );
                                      setIsValidCount(true);
                                    }
                                  }}
                                />
                                <span className="vertical-right-line"></span>
                                <button
                                  disabled={
                                    Number(slotdetails.maxAvailableCount) ===
                                    bookedCount
                                  }
                                  onClick={() => {
                                    setBookedCount((prev: any) => prev + 1);
                                    const availableSeats =
                                      Number(slotdetails.maxAvailableCount) -
                                      Number(slotdetails.bookedCount);
                                    if (
                                      Number(availableSeats) - Number(bookedCount + 1) <
                                      0
                                    ) {
                                      setIsValidCount(false);
                                      setAvailableCount(0);
                                    } else if (
                                      Number(availableSeats) -
                                      Number(bookedCount + 1) ===
                                      0
                                    ) {
                                      setIsValidCount(true);
                                      setAvailableCount(0);
                                      setDisplayCount(0);
                                    } else {
                                      setAvailableCount(
                                        Number(availableSeats) - Number(bookedCount + 1)
                                      );
                                      setDisplayCount(
                                        Number(availableSeats) - Number(bookedCount + 1)
                                      );
                                      setIsValidCount(true);
                                    }
                                  }}
                                  className="pagination-button"
                                >
                                  +
                                </button>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="flex justify-between">
                        {getaquote && (
                          <div className="mx-5 text-[#D6B37E] p-4">
                            Pris från: {eventData.fromPrice}  {eventData.units}
                          </div>
                        )}
                        {/* <div className="mx-5 text-[#D6B37E] p-4">
                          Välj unit: {eventData.units}
                        </div> */}
                      </div>

                    </div>
                    <div className="flex justify-center">
                      {(userInfo?.role !== "ROLE_SUPPLIER" &&
                        userInfo?.role !== "ROLE_ADMIN") &&
                        <button
                          onClick={redirectToChecking}
                          disabled={!bookedCount || !isValidCount}
                          className={`flex items-center justify-center ${!bookedCount || !isValidCount
                            ? "bg-[#D6B27D] opacity-50 rounded-md text-white capitalize md:text-[14px] px-6 m-5 p-2 shadow-lg  md:font-semibold mb-1 mt-2 w-auto"
                            : "bg-[#D6B27D] font-bold rounded-md text-white capitalize md:text-[14px] px-6 p-2 shadow-lg md:font-semibold mb-1 mt-2 w-auto m-5"
                            }`}
                        >
                          FORTSÄTT TILL UTCHECKNINGEN
                        </button>}
                    </div>

                    {!isValidCount && (
                      <p className="text-red-500 mb-3">
                        Your member count is higher than available count
                      </p>
                    )}
                  </div>
                </div>
                <div className="flex justify-between mt-14 ml-10">
                  <h1 className="md:text-[28px] font-semibold uppercase -ml-10 text-secondary">
                    {eventData?.name}
                  </h1>
                </div>
                <p className="p-4 my-1 text-lg font-medium -ml-4 text-secondary">
                  {eventData?.description.length > 200 ?
                    <div>
                      {showFullDescription ? (
                        <>
                          {eventData?.description}
                          <span style={{ color: 'grey', cursor: 'pointer' }} onClick={toggleDescription}>
                            {' '}
                            ... Read less
                          </span>
                        </>
                      ) : (
                        <>
                          {eventData?.description.slice(0, 200)} {/* Display the first 200 characters */}
                          <span style={{ color: 'grey', cursor: 'pointer' }} onClick={toggleDescription}>
                            {' '}
                            ... Read more
                          </span>
                        </>
                      )}
                    </div>
                    : <>
                      <p className="mb-0 mt-0 text-secondary text-lg font-normal">
                        {eventData?.description
                          ?.split(".")
                          ?.slice(0, 2) // Adjust the number of sentences to include as needed
                          ?.join(".")}
                      </p>
                    </>}

                </p>
                <div className={clickOpen ? "flex md:flex-row flex-col gap-1" : "hidden"}>
                  <div className="order-2 md:order-1 md:w-1/2 w-full h-full ">
                    {latLong.lat !== 0.0 && latLong.lng !== 0.0 ? (<MapView
                      latitude={latLong.lat}
                      longitude={latLong.lng}
                      address={eventData?.city}
                    />) : <p className="text-red-500 font-medium mt-2">{message}</p>}
                  </div>

                  <div className="order-1 md:order-2 md:w-1/2 w-full">
                    <section className="flex flex-col items-start bg-[#FFFF] rounded-[14px] md:h-[375px] p-4">
                      <p className="font-medium italic text-[#D6B27D]">
                        -Vår information
                      </p>
                      <h4 className=" md:text-[24px] font-medium mt-3 text-secondary">
                        Address: {`${eventData?.city}, ${eventData?.pincode}`}
                      </h4>
                    </section>
                  </div>
                </div>
                <button className="rounded-xl text-white uppercase items-start md:text-[16px] px-6 p-2 shadow-lg bg-[#D6B27D] md:text-lg  md:font-semibold mb-10 mt-1.5 ml-1" onClick={() => { setClickOpen(!clickOpen) }}>
                  {clickOpen ? "Läs mindre" : "Läs mer"}
                </button>
                {userInfo.id && (
                  <div className="relative px-7 md:px-1 flex flex-col items-center mt-8 my-8">

                    {eventData?.comments?.length > 0 &&
                      eventData?.comments?.map((comment: any) => {
                        if (comment.status === "ACCEPTED" && comment.rating) {
                          return (
                            <div className="flex flex-row justify-between items-baseline my-3 border-b-[1px] border-[#e3e3e3] rounded-md px-4">
                              <section>
                                <div className="flex flex-row">
                                  {comment.customer !== null ? (
                                    <img
                                      className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                      src={comment.customer?.profileImage}
                                    ></img>
                                  ) : (
                                    <img
                                      className="h-[32px] w-[32px] mx-4 mb-4 rounded-full"
                                      src={
                                        comment.supplier?.profileImage ??
                                        "https://ocm-images.fra1.cdn.digitaloceanspaces.com/assets/supplier.png"
                                      }
                                    ></img>
                                  )}
                                  {comment.customer !== null ? (
                                    <p className="text-[#D6B27D] font-medium mt-1.5">
                                      {comment.customer.firstName}
                                    </p>
                                  ) : (
                                    <p className="text-[#D6B27D] font-medium mt-1.5">
                                      {comment.supplier.firstName}
                                    </p>
                                  )}
                                </div>
                                <div className="ml-4 -mt-2">
                                  <RatingBar
                                    ratings={comment.rating?.ratings}
                                    readonly={true}
                                  />
                                </div>
                                <h4 className="text-lg mx-4 text-secondary">
                                  {comment.rating?.descriptions}
                                </h4>
                              </section>
                              <span className="text-xs text-secondary">
                                {moment(comment.createdDate).format(
                                  "ddd, MMM DD YYYY"
                                )}
                              </span>
                            </div>
                          );
                        }
                      })}
                  </div>
                )}
              </div>

            ) : <div className="flex relative rounded-lg flex-col items-center justify-center h-[40vh]">
              <section className='flex flex-col items-center justify-center absolute z-40 bg-white bg-opacity-30 backdrop-filter backdrop-blur-lg border border-gray-300 rounded-lg p-10'>
                <p className="text-secondary text-2xl font-semibold my-8">
                  Ditt favoritobjekt är för närvarande inte tillgängligt. Beklagar olägenheten.
                </p>
                <div className='flex'>
                  <button
                    onClick={goToLandingPage}
                    className="bg-[#D6B27D] rounded-lg text-white uppercase p-2 px-6 md:px-[1.65rem] flex text-[14px] md:text-lg md:font-semibold"
                  >
                    Fortsätt handla
                  </button>
                  <button
                    onClick={goToOderpage}
                    className="bg-[#D6B27D] rounded-lg ml-16 text-white uppercase p-2 px-6 md:px-[1.65rem] flex text-[14px] md:text-lg md:font-semibold"
                  >
                    Se beställningar
                  </button>
                </div>
              </section>
            </div>
            }
          </>
      }
    </>
  );
};

export default BookingScreen;