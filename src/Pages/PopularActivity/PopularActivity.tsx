import React, { useContext, useEffect, useState } from "react";
import deleteIcon from "../../assets/Delete.png";
import { HiHeart } from "react-icons/hi";
import { AiFillHeart } from "react-icons/ai";
import { IoIosHeartEmpty } from "react-icons/io";
import { IoMdArrowDropdown } from "react-icons/io";
import {
  createSearchParams,
  useNavigate,
  useSearchParams,
  useLocation,
} from "react-router-dom";
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import Button from "../../Components/Button/Button";
import { commonAxios } from "../../Axios/service";
import Loader from "../../Components/Loader/Loader";
import BreadCrumb from "../../Components/BreadCrumb/BreadCrumb";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { Drawer, IconButton, Tooltip } from "@material-tailwind/react";
import { XMarkIcon } from "@heroicons/react/24/outline";
import RatingBar from "../../Components/RatingBar/RatingBar";
import { addProductToCart } from "../../Redux/features/productCartSlice";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import { AuthContext } from "../RootLayout/RootLayout";
import { MdOutlineIosShare } from "react-icons/md";
import popfavicon from "../../assets/landinghearticon.png"
import notcopy from "../../assets/Notcopied.png";
import copied from "../../assets/copied.png";
import notFavHeart from "../../assets/newNotfav.png";
import share from "../../assets/share.png";
import { getWishlistCount } from "../../Redux/features/wishlistSlice";
import { getCartListCount } from "../../Redux/features/cartCountSlice";
import Spinner from "../../Components/Loader/Spinner";



const PopularActivity = ({ params }: any) => {
  const [eventsList, setEventsList] = useState<any>(null);
  const [isLoading, setIsLoading] = useState(false);
  const [searchParams] = useSearchParams();
  const [email, setEmail] = useState("");
  const [category, setCategory] = useState<any>([]);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [isValid, setIsValid] = useState(true);
  const [option, setOption] = useState({ label: "Filter", value: "Filter" });
  const [renders, setRenders] = useState(false);
  const [menuIsOpen, setMenuIsOpen] = useState(false);
  const [isFavourite, setIsFavourite] = useState(false);
  const [isMaxBooked, setIsMaxBooked] = useState(false);
  const [selectedCategories, setSelectedCategories] = useState<any>([]);
  const [ratings, setRatings] = useState<any>("");
  const dispatch = useDispatch()
  const [selectedCities, setSelectedCities] = useState<any>([]);
  const [title, setTitle] = useState("");
  const [amountOfPeople, setAmountofPeople] = useState<number>(0);
  const [activeMenu, setActiveMenu] = useState("event");
  const [dataFromLocation, setDataFromLocation] = useState(null);
  const [isFav, setIsFav] = useState<any>([]);

  const { setIsDeleteEventModalOpen, setIsBookingWithOutLogIn, setBookingLogin } =
    useContext(AuthContext);
  const [cities, setCities] = useState<any>([]);
  const [openRight, setOpenRight] = React.useState(false);

  const breadsList = [
    {
      name: "Startsida",
      naviagteTo: "/",
    },
    {
      name: "Populära aktiviteter",
    },
  ];

  const imgPerPage = 12;

  let loadImg = 0;

  const [loadImgCount, setLoadImgCount] = useState(12);
  const [quickViewDetail, setQuickViewDetail] = useState<any>({});
  const [isPopupDetailModalOpen, setIsPopupDetailModalOpen] = useState(false);
  const [gridImages, setGridImages] = useState<any>([]);
  const [isDropdownOpen, setIsDropdownOpen] = useState(true);
  const [isNewlyAdded, setIsNewlyAdded] = useState(false);
  const [defaultRatings, setDefaultRatings] = useState<any>();
  const [ratingCount, setRatingCount] = useState()
  const navigate = useNavigate();


  const location = useLocation();
  const { state }: any = location;

  useEffect(() => {
    console.log(state, "state")
    if (state) {
      setGridImages(state?.data?.data)
    }
    if (state?.data?.data.length === 0) {
      setIsModalOpen(true)
    }
  }, [])

  useEffect(() => {
    if (isModalOpen) {
      // Disable scrolling when the popup is opened
      document.body.style.overflow = "hidden";
    }

    return () => {
      // Enable scrolling when the component is unmounted
      document.body.style.overflow = "auto";
    };
  }, [isModalOpen]);

  const { isGlobalSearch } = state != null ? state : false;
  const { titleName } = state != null ? state : false;
  const handleChange = (event: any) => {
    setEmail(event.target.value);
  };

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    const searchText = searchParams.get("searchText");
    const searchTitle = searchParams.get("searchTitle");
    const searchLocation = searchParams.get("location")

    console.log(searchText, "searchText")
    console.log(searchTitle, "searchTitle")
    console.log(searchLocation, "searchLocation")



    if (validateEmail(email)) {
      setIsValid(true);
      setIsModalOpen(false);
      if (searchText === null && searchTitle === null && searchLocation === null) {
        const categoryList = state?.data.category.join(',');
        const cityList = state?.data.city.join(',');
        const notifyData = {
          email: email,
          searchKey: categoryList.length > 0 ? categoryList : cityList
        }
        try {
          const response = await commonAxios.post('/cemo/notify/add', notifyData);
          if (response && response.status === 200) {
            successNotify("avisering har skickats till din gmail");
            navigate('/');
          } else {
            failureNotify("error")
          }
        } catch (error) {
          console.log(error)
        }
      } else {
        const notifyData = {
          email: email,
          searchKey: searchTitle === "" ? searchText : searchTitle
        }
        try {
          const response = await commonAxios.post('/cemo/notify/add', notifyData);
          if (response && response.status === 200) {
            successNotify("avisering har skickats till din gmail");
            navigate('/');
          } else {
            failureNotify("error")
          }
          console.log("response", response)
        } catch (error) {
          console.log(error)
        }
      }
    } else {
      setIsValid(false);
    }

  };

  const validateEmail = (email: any) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const handleClear = (event: any) => {
    event.preventDefault();
    setIsModalOpen(false);
    setEmail("");
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const loadImages = () => {
    setLoadImgCount(loadImgCount + 12);
    setGridImages(eventsList?.slice(loadImg, imgPerPage + loadImgCount));
  };

  const selectedImg = (data: any) => {
    if (!data.isProduct) {
      const params = {
        id: data?.id,
        searchText: searchParams.get("searchText") as string,
      };
      navigate({
        pathname: "/booking",
        search: `?${createSearchParams(params)}`,
      });
    } else {
      const params = {
        id: data?.appUser?.id,
        productId: data?.id,
        searchText: searchParams.get("searchText") as string,
      };
      navigate({
        pathname: "/cart",
        search: `?${createSearchParams(params)}`,
      });
    }
  };

  useEffect(() => {
    if (searchParams.get('category')) {
      getEventsByCategories()
    } else {
      getEventsBySearch();
    }

  }, [location]);

  const getEventsByCategories = async () => {
    setIsLoading(true)
    let searchText;
    if (searchParams.get('category')) {
      searchText = searchParams.get("category") as string
    }
    const filterBody = {
      cityList: [],
      categoryList: [searchText],
      newlyAdded: false,
      maxBooked: false,
      amountOfPeople: amountOfPeople ? Number(amountOfPeople) : 0,
      searchKey: '',
    };

    try {
      const response = await commonAxios.post(
        `/cemo/bookingitems/filter`,
        filterBody
      );

      if (response.data) {
        const productArray = (response.data.product || []).filter(
          (item: any) => item.status === "ACTIVE"
        ).map((item: any) => {
          return {
            ...item,
            isProduct: true,
          };
        });
        const eventArray = (response.data.event || []).filter(
          (item: any) => item.status === "ACTIVE"
        ).map((item: any) => {
          return {
            ...item,
            isProduct: false,
          };
        });
        const gridData = [...eventArray, ...productArray];
        setEventsList(gridData);
        if (gridData.length === 0) {
          setIsModalOpen(true);
        }
        setGridImages(gridData?.slice(loadImg, imgPerPage));

      }
    } catch (e) {
    } finally {
      setIsLoading(false)
    }

  }
  const getEventsBySearch = async () => {
    setIsLoading(true)
    const globalSearchObj = JSON.parse(
      localStorage.getItem("globalSearchObj")!
    );
    if (globalSearchObj.filterList,
      searchParams.get("searchTitle") !== null && titleName) {
      const payload = {
        categoryList: [
          ...globalSearchObj.filterList,
          searchParams.get("searchTitle"),
        ],
      };
      const URL = "/cemo/bookingitems/filter";
      try {
        const response = await commonAxios.post(URL, payload);
        if (response.data) {
          const productArray = (response.data.product || []).filter(
            (item: any) => item.status === "ACTIVE"
          ).map((item: any) => {
            return {
              ...item,
              isProduct: true,
            };
          });
          const eventArray = (response.data.event || []).filter(
            (item: any) => item.status === "ACTIVE"
          ).map((item: any) => {
            return {
              ...item,
              isProduct: false,
            };
          });
          const gridData = [...eventArray, ...productArray];
          setEventsList(gridData);
          if (gridData.length === 0) {
            setIsModalOpen(true);
          }
          setGridImages(gridData?.slice(loadImg, imgPerPage));
          setIsLoading(false);
        }
      } catch (error) {
      } finally {
      }
    } else {
      const payload = {
        categoryList: [
          ...globalSearchObj.filterList,
          searchParams.get("searchTitle"),
        ],
        searchKey: searchParams.get("searchText"),
      };

      const URL = "/cemo/bookingitems/search";
      // searchParams.get("searchTitle") === ""
      //   ? `/cemo/bookingitems/search?search-key=${searchParams.get(
      //     "searchText"
      //   )}`
      //   : `/cemo/bookingitems/search?category-name=${searchParams.get(
      //     "searchTitle"
      //   )}&search-key=${searchParams.get("searchText")}`;
      try {
        const response = await commonAxios.post(URL, payload);
        const gridDatas = response.data?.product?.filter((item: any, index: any) => {
          if (item.status === "ACTIVE") {
            return true
          }
          return false
        });

        const gridDatas2 = gridDatas.map((item: any) => {
          return {
            ...item,
            isProduct: true
          }
        })

        const gridEventData = response.data?.event?.filter((item: any, index: any) => {
          if (item.status === "ACTIVE") {
            return true
          }
          return false
        });

        const gridEventDatavalue = gridEventData.map((item: any) => {
          return {
            ...item,
            isProduct: false
          }
        })

        const gridData = [...gridEventDatavalue, ...gridDatas2];
        setEventsList(gridData);
        if (gridData.length === 0) {
          setIsModalOpen(true);
        }
        setGridImages(gridData?.slice(loadImg, imgPerPage));
        setIsLoading(false);
      } catch (error) {
      } finally {
        setIsLoading(false);
      }
    }
    setIsLoading(false);
  };

  const userInfo = useSelector((state: RootState) => state.user.userDetails);


  const togglefavouriteforAllEvents = async (id: any) => {
    if (userInfo.id) {
      if (id.isProduct) {
        try {
          // setIsLoading(true);
          const payload = {
            appUserId: userInfo.id,
            eventId: null,
            productId: id.id,
          };
          await commonAxios.post("/cemo/favourite-items", payload).then(
            (response) => {
            },
            (error) => { }
          );
        } catch (error) {
        } finally {
          // setIsLoading(false);
          dispatch(getWishlistCount(userInfo.id));
        }
      }
      else {
        try {
          // setIsLoading(true);
          const payload = {
            appUserId: userInfo.id,
            eventId: id.id,
            productId: null
          };
          await commonAxios.post("/cemo/favourite-items", payload).then(
            (response) => {
            },
            (error) => { }
          );
        } catch (error) {
        } finally {
          dispatch(getWishlistCount(userInfo.id));
        }
      }
    }
  }


  const togglefavourite = async (id: any) => {

    if (userInfo.id) {
      if (id.isProduct) {
        try {
          const payload = {
            appUserId: userInfo.id,
            eventId: null,
            productId: id.id,
          };
          await commonAxios.post("/cemo/favourite-items", payload).then(
            (response) => {
              setIsFavourite(!response.data.deleted);
              getEventsBySearch()
            },
            (error) => { }
          );
        } catch (error) {
        } finally {
          dispatch(getWishlistCount(userInfo.id));
        }
      }
      else {
        try {
          const payload = {
            appUserId: userInfo.id,
            eventId: id.id,
            productId: null
          };
          await commonAxios.post("/cemo/favourite-items", payload).then(
            (response) => {
              setIsFavourite(!response.data.deleted);
              getEventsBySearch()
            },
            (error) => { }
          );
        } catch (error) {
        } finally {
          dispatch(getWishlistCount(userInfo.id));
        }
      }
    }
  };


  const handleNavigate = (value: any) => {

    if (value.isProduct) {
      navigate(`${`/cart?id=${value.appUser.id}&productId=${value.id}`}`)
    }
    else {
      navigate(`${`/booking?id=${value.id}`}`)
    }
  }

  const handleClearBuyProduct = async (value: any) => {
    let currentUrl = window.location.href
    // GuestUser Quick Buy functionality flow
    if (!localStorage.getItem("token")) {
      const cartSupplier = JSON.parse(
        localStorage.getItem("cartSupplier") as string
      );
      if (cartSupplier && cartSupplier !== value.appUser?.id) {
        failureNotify(
          "Products from multiple suppliers cannot be added to cart"
        );
      } else {
        localStorage.setItem(
          "cartSupplier",
          JSON.stringify(value.appUser?.id)
        );
        setIsLoading(true);

        const existingCartItems = localStorage.getItem("cartListDetails");
        let arr =
          existingCartItems && existingCartItems.length
            ? JSON.parse(existingCartItems)
            : [];
        const isExisProduct = arr.filter((item: any) => item.product.id === quickViewDetail.id);
        console.log(isExisProduct, "isExisProduct");
        arr = arr.filter((item: any) => item.product.id !== quickViewDetail.id);
        console.log(arr, "arr from");
        if (isExisProduct.length) {
          const finalQuantity = isExisProduct[0]?.quantity + 1;
          const cartItemList = {
            product: quickViewDetail,
            bookingItemType: "PRODUCT",
            totalPrice: quickViewDetail?.price * finalQuantity,
            price: quickViewDetail?.price,
            vatAmount: quickViewDetail?.Tax,
            quantity: finalQuantity,
          };
          arr.push(cartItemList);
        } else {
          const cartItemList = {
            product: quickViewDetail,
            bookingItemType: "PRODUCT",
            totalPrice: quickViewDetail?.price,
            price: quickViewDetail?.price,
            vatAmount: quickViewDetail?.Tax,
            quantity: 1,
          };
          arr.push(cartItemList);
        }
        localStorage.setItem("cartListDetails", JSON.stringify(arr));
        dispatch(addProductToCart());
        successNotify("Produkt lagd i varukorg");
        navigate(
          {
            pathname: "/shoppingCart",
          },
          {
            state: { currentUrl },
          }
        );
      }
    } else {
      // SignInUser Quick Buy functionality flow
      const cartSupplier = JSON.parse(
        localStorage.getItem("cartSupplier") as string
      );
      if (cartSupplier && cartSupplier !== value.appUser?.id) {
        failureNotify(
          "Products from multiple suppliers cannot be added to cart"
        );
      } else {
        localStorage.setItem(
          "cartSupplier",
          JSON.stringify(value.appUser?.id)
        );
        setIsLoading(false)
        const date = new Date();
        const year = date.getFullYear();
        const month = ("0" + (date.getMonth() + 1)).slice(-2);
        const day = ("0" + date.getDate()).slice(-2);
        const formattedDate = `${year}-${month}-${day}`;
        const updatedProduct = {
          appUserId: userInfo.id,
          productId: value?.id,
          bookingItemType: "PRODUCT",
          bookingDate: formattedDate,
          quantity: 1,
        };
        try {
          const response = await commonAxios.post(
            "/cemo/bookingitems/new",
            updatedProduct
          );
          const cartItemList = {
            product: quickViewDetail,
            bookingItemType: "PRODUCT",
            totalPrice: quickViewDetail?.price,
            price: quickViewDetail?.price,
            vatAmount: quickViewDetail?.Tax,
            quantity: 1,
          };
          localStorage.setItem("cartListDetails", JSON.stringify(cartItemList));
          successNotify("Produkt lagd i varukorg");
          dispatch(getCartListCount(userInfo.id))
          navigate(
            {
              pathname: "/shoppingCart",
            },
            {
              state: { currentUrl },
            }
          );
        } catch (error) {
          failureNotify("oops! something went wrong, Try again");
        } finally {
          // setIsLoading(false);
        }
      }
    }
  }

  console.log(isPopupDetailModalOpen, "isPopupDetailModalOpen")

  const PopupDetailModal = () => {

    const [showFullDescription, setShowFullDescription] = useState(false);

    const toggleDescription = () => {
      setShowFullDescription(!showFullDescription);
    };

    const handleclose = () => {
      // Close the popup
      setIsPopupDetailModalOpen(false);

      // Enable scrolling when the popup is closed
      document.body.style.overflow = "auto";
    };

    useEffect(() => {
      if (isPopupDetailModalOpen) {
        // Disable scrolling when the popup is opened
        document.body.style.overflow = "hidden";
      }

      return () => {
        // Enable scrolling when the component is unmounted
        document.body.style.overflow = "auto";
      };
    }, [isPopupDetailModalOpen]);



    return (
      <div className="w-screen h-screen bg-[#00000080] fixed top-0 z-40">
        <section className="w-full h-full flex sm:flex-row flex-col items-center justify-center">
          <div className="bg-white rounded-lg border-2 border-[#D7B480] p-3 flex flex-col sm:flex-row justify-start items-start w-full sm:w-[80%] lg:w-[80%] xl:w-1/2 h-auto relative">
            <div className="absolute top-2 right-3  w-4 h-2">
              {/* <Tooltip className='bg-[#D6B27D]' content="Close" placement="top-start"> */}
              {/* <IconButton
                  variant="text"
                  color="blue-gray"
                 
                > */}
              <XMarkIcon onClick={handleclose} className="h-5 w-5 cursor-pointer" />
              {/* </IconButton> */}
              {/* </Tooltip> */}
            </div>
            <div className={`${quickViewDetail?.description.length < 200 && "flex"}`}>
              <div className=" mt-1">
                <img
                  src={quickViewDetail.image.split(',')[0]}
                  className={` ${quickViewDetail?.description.length > 200 && "float-left"} w-[400px] mt-4 sm:mt-0 md:w-[24rem] lg:w-[24rem]  object-cover rounded-lg h-[20rem] mr-5`}
                  alt="Event Image"
                />
              </div>
              <section className="h-full sm:mt-0 mx-4">
                <div className="">
                  <div>
                    <h2 className="text-xl sm:text-2xl font-[sofiapro] font-semibold text-secondary -ml-1">
                      {quickViewDetail?.name}
                    </h2>
                    <h1 className="text-[#D6B37E] text-sm md:text-base font-semibold italic uppercase px-10 -ml-11">
                      - {quickViewDetail.companyName}
                    </h1>
                    <div className="flex items-center -ml-[5px]">
                      <RatingBar ratings={defaultRatings} readonly={true} />
                      <p className="ml-3 mt-1 text-secondary text-sm">({ratingCount})</p>
                      {/* <Tooltip className='bg-[#D6B27D]' content={isFavourite ? "Remove from wishlist" : 'Add to wishlist'} placement="right-start"> */}
                      <div
                        onClick={() => togglefavourite(quickViewDetail)}
                        className="cursor-pointer ml-2"
                      >
                        {userInfo.id ? (
                          isFavourite ? (
                            <div className="rounded-full p-2 hover:bg-[#e9e3e3ef]">
                              <AiFillHeart className="h-6 w-6 text-[#D6B27D]" />
                            </div>
                          ) : (
                            <div className="rounded-full p-2 hover:bg-[#e9e3e3ef]">
                              <img src={notFavHeart} className="h-5 w-5 z-40 hover:text-[#D6B27D]" />
                            </div>
                          )
                        ) : null}
                      </div>
                      {/* </Tooltip> */}

                      {/* <Tooltip className='bg-[#D6B27D]' content={"Share"} placement="right-start"> */}
                      <div className="rounded-full p-2 hover:bg-[#e9e3e3ef] cursor-pointer ml-2" onClick={() => handleShareClick(quickViewDetail)}> <img src={share} className="h-5 w-5 text-[#D6B27D] z-40 " /></div>
                      {/* </Tooltip> */}
                    </div>
                  </div>

                </div>
                {quickViewDetail?.description.length > 200 ?
                  <div>
                    {showFullDescription ? (
                      <>
                        <p className="text-secondary"> {quickViewDetail?.description} </p>
                        <span style={{ cursor: 'pointer' }} className="text-secondary" onClick={toggleDescription}>
                          {' '}
                          ... Read less
                        </span>
                      </>
                    ) : (
                      <>
                        <p className="text-secondary"> {quickViewDetail?.description.slice(0, 200)}</p> {/* Display the first 200 characters */}
                        <span style={{ cursor: 'pointer' }} className="text-secondary" onClick={toggleDescription} >
                          {' '}
                          ... Read more
                        </span>
                      </>
                    )}
                  </div>
                  : <>
                    <p className="mb-0 mt-0 text-secondary text-lg font-normal">
                      {quickViewDetail?.description
                        ?.split(".")
                        ?.slice(0, 2) // Adjust the number of sentences to include as needed
                        ?.join(".")}
                    </p>
                  </>}
                <div className="flex flex-row justify-left items-center space-x-4">
                  <div className="mr-2 mt-6" onClick={() => selectedImg(quickViewDetail)}>
                    {quickViewDetail.isProduct ? (
                      <Button btnName={"Köp"} />
                    ) : (
                      <Button btnName={"Besök"} />
                    )}
                  </div>
                  {(quickViewDetail.isProduct && userInfo?.role !== "ROLE_SUPPLIER" && userInfo?.role !== "ROLE_ADMIN") && (
                    <div className="mr-2 mt-6">
                      <div className="bg-[#D6B27D] p-2 rounded-xl shadow-2xl">
                        <button
                          className={`font-semibold text-xs md:text-base rounded-lg bg-[#D6B37D] text-white px-2 uppercase`}
                          onClick={() => handleClearBuyProduct(quickViewDetail)}
                        >
                          Quick Buy
                        </button>
                      </div>
                    </div>
                  )}
                </div>

              </section>
            </div>
          </div>
        </section>
      </div>


    );
  };

  const getFavouriteItems = async (id: any) => {
    if (userInfo.id) {
      try {
        const response = await commonAxios.get(
          `cemo/favourite-items/favourite/${userInfo.id}/${id}`
        );
        setIsFavourite(response.data.isFavourite);
      } catch (e) {
      } finally {
      }
    }
  };

  const handlequickviewclick = async (data: any) => {
    getFavouriteItems(data.id);
    setIsLoading(true);
    let response;
    if (data.isProduct) {
      response = await commonAxios.get(
        `cemo/ratings/average/rating?productId=${data.id}`
      );
      setDefaultRatings(response.data);
      setRatingCount(response.data)
    } else if (!data.isProduct) {
      response = await commonAxios.get(
        `cemo/ratings/average/rating?eventId=${data.id}`
      );
      setDefaultRatings(response.data);
      setRatingCount(response.data)
    }


    setQuickViewDetail(data);
    setIsPopupDetailModalOpen(true);
    setIsLoading(false);

  };

  useEffect(() => {
    const handleEscPress = (event: KeyboardEvent) => {
      if (event.key === "Escape") {
        setIsModalOpen(false);
        setIsPopupDetailModalOpen(false);
      }
    };

    document.addEventListener("keydown", handleEscPress);

    return () => {
      document.removeEventListener("keydown", handleEscPress);
    };
  }, []);

  const getAllCategories = async () => {
    try {
      const response = await commonAxios.get(`/cemo/categories`);
      const activeCategories = response.data.filter((item: any) => item.status === "ACTIVE");

      setCategory(activeCategories);
    } catch (e) { }
  };

  const getAllCities = async () => {
    try {
      const response = await commonAxios.get(`/cemo/events/cities`);

      let new_array: any = [];
      response.data.map((val: any) => {
        if (!new_array.includes(val)) {
          return new_array.push(val);
        }
      });

      setCities(new_array);
    } catch (e) { }
  };

  useEffect(() => {

    getAllCategories();
    getAllCities();
  }, []);

  useEffect(() => {
    if (isModalOpen && !isGlobalSearch && (eventsList !== null ? eventsList.length === 0 : false)) {
      document.body.classList.add("overflow-hidden");
    } else {
      document.body.classList.remove("overflow-hidden");
    }

    const favList = eventsList && eventsList.map((item: any) => item.favourite);
    setIsFav(favList || []);
  }, [isModalOpen, eventsList]);

  const handleChangeRange = (selectedOption: any) => {
    setOption({ label: selectedOption.label, value: selectedOption.value });
    const sortedArray = [...eventsList].sort((a, b) => {
      if (selectedOption.value === "Low to High") {
        return a.price - b.price;
      } else if (selectedOption.value === "High to Low") {
        return b.price - a.price;
      }

      return 0;
    });

    setEventsList(sortedArray);
    setGridImages(sortedArray.slice(loadImg, imgPerPage));
  };

  const openDrawerRight = () => {
    setOpenRight(true);
  };
  const closeDrawerRight = () => {
    setOpenRight(false);
  };

  const handleFilter = async (e: any) => {
    setIsLoading(true);
    e.preventDefault();
    const filterBody = {
      cityList: selectedCities,
      categoryList: selectedCategories,
      newlyAdded: isNewlyAdded,
      maxBooked: isMaxBooked,
      amountOfPeople: amountOfPeople ? Number(amountOfPeople) : 0,
      searchKey: '',
    };

    try {
      const response = await commonAxios.post(
        `/cemo/bookingitems/filter`,
        filterBody
      );
      if (response.data) {
        const productArray = (response.data.product || []).filter(
          (item: any) => item.status === "ACTIVE"
        ).map((item: any) => {
          return {
            ...item,
            isProduct: true,
          };
        });
        const eventArray = (response.data.event || []).filter(
          (item: any) => item.status === "ACTIVE"
        ).map((item: any) => {
          return {
            ...item,
            isProduct: false,
          };
        });
        const gridData = [...eventArray, ...productArray];
        setEventsList(gridData);
        if (gridData.length === 0) {
          setIsModalOpen(true);
        }
        setGridImages(gridData?.slice(loadImg, imgPerPage));
        setIsMaxBooked(false);
        setIsNewlyAdded(false);
        setSelectedCategories([]);
        setSelectedCities([]);
        setOpenRight(false);
        setIsLoading(false);
      }
    } catch (e) {
    } finally {
    }
  };

  // const handleChangeForMaxBooking = (e: any) => {
  //   setIsMaxBooked(e.target.checked);
  // };

  const handleChangeForNewlyAdded = (e: any) => {
    setIsNewlyAdded(e.target.checked);
  };

  const handleChangeForPeople = (e: any) => {
    setAmountofPeople(e.target.value);
  };

  const handleChangeforCategories = (e: any) => {
    const isChecked = e.target.checked;
    const values = e.target.name;
    if (isChecked) {
      setSelectedCategories((prevSelected: any) => [...prevSelected, values]);
    } else {
      setSelectedCategories((prevSelected: any) =>
        prevSelected.filter((value: any) => value !== values)
      );
    }
  };

  const handleChangeForCities = (e: any) => {
    const isChecked = e.target.checked;
    const values = e.target.name;
    if (isChecked) {
      setSelectedCities((prevSelected: any) => [...prevSelected, values]);
    } else {
      setSelectedCities((prevSelected: any) =>
        prevSelected.filter((value: any) => value !== values)
      );
    }
  };
  const activeChange = (title: any) => {
    setTitle((prev: any) => {
      if (prev === title) {
        return "";
      } else {
        return title;
      }
    });
  };

  const handleShareClick = async (orderDetails: any) => {

    if (orderDetails.isProduct) {
      try {
        let text = `${window.location.href.split('/')[2]}` + '/cart?id=' + orderDetails?.appUser?.id + '&productId=' + orderDetails?.id
        await navigator.clipboard.writeText(text);
        successNotify('Link Copied')
      } catch (error) {
        failureNotify('the Link is Not Copied')
      }
    } else {
      try {
        let text = `${window.location.href.split('/')[2]}` + '/booking?id=' + orderDetails?.id
        await navigator.clipboard.writeText(text);
        successNotify('Link Copied')
      } catch (error) {
        failureNotify('the Link is Not Copied')
      }
    }
    // return pendingOrders.map((val: any) => {
    //   if (val.orderNumber === orderId) {
    //     return val.showToolTip = true
    //   }
    // })
  }

  useEffect(() => {
    const overlayElements: any = document.querySelectorAll('.bg-black');
    overlayElements.forEach((element: any) => {
      element.style.display = 'none';
    });
  }, [openRight]); // Trigger this effect when the drawer is opened or closed



  return (
    <>
      {isLoading && <Loader />}
      {isPopupDetailModalOpen ? <PopupDetailModal /> : null}
      <div className="parent-container">
        <div className="drawer-container">
          <Drawer
            placement="right"
            size={499}
            open={openRight}
            onClose={closeDrawerRight}
            className="py-4 h-screen"
            style={{ backgroundColor: 'transparent', boxShadow: 'none' }}
          >

            <div className="px-5 h-[100vh] overflow-y-scroll">
              <div className="flex justify-between ">
                <p className="font-semibold mb-2 text-xl text-[#D6B27D]">Categories</p>
                <div className="flex items-center justify-between">
                  <IconButton
                    variant="text"
                    color="blue-gray"
                    onClick={closeDrawerRight}
                  >
                    <XMarkIcon className="h-4 w-4 border border-blue-gray" />
                  </IconButton>
                </div>
              </div>
              <div className="grid grid-cols-2">

                {category &&
                  category.map((val: any) => {
                    return (
                      <>
                        <div className="flex m-1">
                          <input
                            type="checkbox"
                            onChange={handleChangeforCategories}
                            name={val.categoryName}
                            className="cursor-pointer  accent-brown-300"
                            id={val.categoryName}
                          />
                          <label
                            className="ms-3 text-[10px] sm:text-[14px]  cursor-pointer text-secondary "
                            htmlFor={val.categoryName}
                          >
                            {val.categoryName.charAt(0).toUpperCase() + val.categoryName.slice(1).toLowerCase()}
                          </label>
                        </div>
                      </>
                    );
                  })}
              </div>
              <p className="mt-5 font-semibold text-xl text-[#D6B27D]">Cities</p>
              <div className="mt-2 grid grid-cols-3">
                {cities &&
                  cities.map((val: any) => {
                    return (
                      <>
                        <div className="flex m-1">
                          <input
                            type="checkbox"
                            name={val}
                            onChange={handleChangeForCities}
                            className="cursor-pointer accent-brown-300"
                            id={val}
                          // checked={cities.includes("trichy")}
                          />
                          <label
                            htmlFor={val}
                            className="text-[10px] sm:text-[14px] ms-3 cursor-pointer text-secondary"
                          >
                            {val.charAt(0).toUpperCase() + val.slice(1).toLowerCase()}
                          </label>
                        </div>
                      </>
                    );
                  })}
              </div>
              <div className="flex flex-row justify-start items-center gap-5">
                {/* <div className="mt-5  -ml-5 font-semibold text-xl text-[#D6B27D]">
                  <input
                    id="maxBooked"
                    className="ms-5 cursor-pointer accent-brown-300"
                    type="checkbox"
                    onChange={handleChangeForMaxBooking}
                  />
                  <label htmlFor="maxBooked" className="sm:text-md lg:text-lg text-[12px] ms-2 cursor-pointer">Max antal gäster</label>
                </div> */}
                <div className="mt-5 font-semibold text-xl text-[#D6B27D]">
                  <input
                    id="newlyAdded"
                    type="checkbox"
                    className="cursor-pointer accent-brown-300"
                    onChange={handleChangeForNewlyAdded}
                  />
                  <label htmlFor="newlyAdded" className="text-md sm:text-md lg:text-lg text-[12px] ms-2 cursor-pointer">Nyligen tillagda</label>
                </div>
              </div>
              <div className="mt-5 font-semibold flex-wrap md:text-xl text-md text-[#D6B27D] md:flex  items-center">
                <div className="mt-5 font-semibold md:text-xl text-md text-[#D6B27D] items-center">
                  <label className="text-start">Antal gäster</label>
                  <div className="flex items-center justify-center" style={{ marginLeft: '350px', marginTop: '-32px' }}>
                    <button
                      disabled={amountOfPeople === 0}
                      onClick={() => setAmountofPeople((prev: any) => prev - 1)}
                      className="rounded-lg w-8 h-8 accent-brown-300 cursor-pointer flex items-center justify-center mr-2 border border-gray-400 outline-none focus:border-gray-700"
                    >
                      <span className="text-[#D6B27D]">-</span>
                    </button>

                    <div className="flex items-center">
                      <input
                        type="number"
                        onChange={handleChangeForPeople}
                        className="rounded-lg w-2 sm:w-6 h-5 sm:h-8 accent-brown-300 cursor-pointer text-center mt-[2px] flex-grow border border-gray-400 outline-none focus:border-gray-700"
                        name="amountOfPeople"
                        value={amountOfPeople}
                      />
                    </div>
                    <button
                      onClick={() => setAmountofPeople((prev: any) => prev + 1)}
                      className="rounded-lg w-8 h-8 accent-brown-300 cursor-pointer flex items-center justify-center ml-2 border border-gray-400 outline-none focus:border-gray-700"
                    >
                      <span className="text-[#D6B27D]">+</span>
                    </button>
                  </div>
                </div>
              </div>
              {isLoading && <Loader />}
              <div className="md:ml-10 flex justify-center mt-4 mb-10">
                <button
                  type="submit"
                  onClick={handleFilter}
                  className="py-2 w-auto font-semibold rounded-lg bg-[#D6B37D] text-white uppercase px-6"
                >
                  Filtrera
                </button>
              </div>
            </div>
          </Drawer>
        </div>
        <div className="mt-5 my-10">
          <BreadCrumb breadsList={breadsList} />
          <div className="px-5 md:px-20 flex justify-between">
            <h1 className="md:text-[20px] lg:text-[30px] xl:text-[36px] font-semibold font-[sofiapro] text-secondary flex items-center">
              Populära aktiviteter
            </h1>
            <div>
              <p
                onClick={openDrawerRight}
                className="p-2 w-auto m-5 font-semibold rounded-lg bg-[#D6B37D] text-white px-6 cursor-pointer"
              >
                FILTRERA
              </p>
            </div>
          </div>
          <div className="md:px-20 px-5 mt-5">
            <ResponsiveMasonry
              columnsCountBreakPoints={{ 350: 1, 750: 3, 900: 4 }}
            >
              <Masonry gutter="24px">
                {gridImages?.map((event: any, i: any) => (
                  <div key={i} className="relative group" style={{ width: '100%', aspectRatio: '1' }}>
                    <div className="absolute inset-0 rounded-[2rem] group-hover:z-20 group-hover:bg-[#00000090] flex justify-center cursor-pointer" onClick={() => handlequickviewclick(event)}>
                      <div className="flex flex-col items-center justify-center">
                        <p className="font-extrabold text-white text-[20px] text-center">
                          {event?.name}
                        </p>
                        <p className="text-white font-bold text-center">
                          {event.companyName}
                        </p>
                      </div>
                    </div>
                    <img
                      src={event.image?.split(",")?.at(0)}
                      style={{ width: "100%", height: "100%", objectFit: "cover" }}
                      className="absolute inset-0 cursor-pointer rounded-[2rem] img"
                      alt=""
                      onError={(e) => console.log("Error loading image:", e)}
                    />
                    <div className="flex absolute group-hover:z-30 xl:bottom-1 xl:right-4 bottom-0 right-3">
                      {event.isProduct ? (
                        <button className={`p-2 mt-1 my-2 font-semibold rounded-lg bg-[#D6B37D] text-white px-6 w-auto uppercase`} onClick={() => handleNavigate(event)}>Köp</button>
                      ) : (
                        <button className={`p-2 mt-1 font-semibold rounded-lg bg-[#D6B37D] text-white px-6 my-2 w-auto uppercase`} onClick={() => handleNavigate(event)}>Besök</button>
                      )}
                      {userInfo.id && (
                        <div
                          onClick={() => {
                            const newFavorites = [...isFav];
                            newFavorites[i] = !newFavorites[i];
                            setIsFav(newFavorites);
                            togglefavouriteforAllEvents(event);
                          }}
                          className="cursor-pointer"
                        >
                          {isFav[i] ? (
                            <div className="rounded-full p-3 hover:bg-[#7c7979ef] ml-2">
                              <AiFillHeart className="h-6 w-6 text-[#D6B27D]" />
                            </div>
                          ) : (
                            <div className="rounded-full p-3 hover:bg-[#7c7979ef] ml-2">
                              <img src={notFavHeart} className="h-7 w-7  text-white hover:text-[#D6B27D]" />
                            </div>
                          )}
                        </div>
                      )}
                      {!userInfo.id && (
                        <div className="rounded-full p-3 hover:bg-[#7c7979ef]" onClick={() => handleShareClick(event)}>
                          <img src={share} className="h-6 w-6 text-[#D6B27D] z-40 " />
                        </div>
                      )}
                    </div>
                  </div>
                ))}
              </Masonry>

            </ResponsiveMasonry>
            {eventsList?.length >= 12 && eventsList?.length !== gridImages?.length ? (
              <div className="flex justify-center my-5">
                <button
                  className="p-2 w-auto m-5 font-semibold rounded-lg bg-[#D6B37D] text-white px-6 uppercase "
                  onClick={loadImages}
                >
                  Se mer
                </button>
              </div>
            ) : null}
            {isModalOpen && (
              <div className="z-10 modal-overlay fixed top-0 right-0 w-[100%] h-full bg-black bg-opacity-50 flex justify-center items-center">
                <div className="modal bg-white w-[600px] p-4 rounded-lg relative">
                  <div className="flex justify-between">
                    <h2 className="text-xl font-bold text-center md:text-left md:mt-3 mb-2 text-[#888888]">Hallå där, Glädjespridare!</h2>
                    <XMarkIcon onClick={handleClear} className="h-5 w-5 cursor-pointer" />
                  </div>
                  <form onSubmit={handleSubmit} className="flex flex-col">
                    <p className="font-normal mb-1 text-gray-600">
                      Det är verkligen något stort på gång, och vi vill att du ska vara med från början!
                    </p>
                    <p className="font-normal mb-6 text-gray-600">
                      Tyvärr har vi ännu inte lanserat i alla städer, men vi arbetar febrilt för att göra det möjligt.                      </p>
                    <p className="font-normal mb-1 text-gray-600">
                      Vi kommer att skicka spännande uppdateringar till dig så fort vi har en nyhet!                      </p>
                    <p className="font-normal mb-2 text-gray-600">
                      Fyll i dina uppgifter för att få sprakande nyheter!                      </p>
                    <input
                      type="email"
                      placeholder="Fyll i din e-post"
                      value={email}
                      onChange={handleChange}
                      className="border border-gray-600 p-2 rounded mb-5 lowercase"
                    />
                    {!isValid && (
                      <p className="text-red-500 -mt-4">
                        Fyll i din e-post
                      </p>
                    )}
                    <div className="flex justify-center">
                      <button
                        type="submit"
                        className="p-2 w-auto m-5 font-semibold rounded-lg bg-[#D6B37D] text-white px-6 uppercase cursor-pointer"
                      >
                        Spara
                      </button>
                      <button
                        type="button"
                        onClick={handleClear}
                        className="p-2 w-auto m-5 font-semibold rounded-lg bg-[#D6B37D] text-white px-6 uppercase cursor-pointer"
                      >
                        Avbryt
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            )}
            {!isGlobalSearch && eventsList && eventsList.length === 0 && (
              <div className="mb-14 mt-4 mx-5 flex justify-center text-gray-600 ">
                Inga mer aktiviteter tillgängliga
              </div>
            )}
          </div>
        </div>
      </div >
    </>
  );
};

export default PopularActivity;
