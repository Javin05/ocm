import React, { useEffect, useRef, useState } from "react";
import gpsIcon from "../../assets/gps.svg";
import searchIcon from "../../assets/search.svg";
import landingImg from "../../assets/heroImage.jpg";
import filterIcon from "../../assets/filter.svg";
import balloonsImg from "../../assets/balloons.png";
import baptismImg from "../../assets/baptism.png";
import bridalshowerImg from "../../assets/bridalshower.png";
import confettiImg from "../../assets/confetti.png";
import cheersImg from "../../assets/cheers.png";
import diamondImg from "../../assets/diamond.png";
import dinnerImg from "../../assets/dinner.png";
import SearchCard from "../../Components/SearchCard/SearchCard";
import danceImgActive from "../../assets/dance.png";
import locationImg from "../../assets/location.svg";
import Button from "../../Components/Button/Button";
import BookingCard from "../../Components/BookingCard/BookingCard";
import roseFlower from "../../assets/sista farväl_page-0001.jpg";
import ballonImg from "../../assets/teamwork.jpg";
import heroImage from "../../assets/14_page-0001.jpg";

import ballonTwo from "../../assets/ballonTwo.png";
import ballonOne from "../../assets/ballonOne.png";
import ballonThree from "../../assets/ballonThree.png";
import StaderCard from "../../Components/StaderCard/StaderCard";
import staderOne from "../../assets/staderOne.png";
import staderTwo from "../../assets/staderTwo.png";
import staderThree from "../../assets/staderThree.png";
import danceImg from "../../assets/danceImgActive.png";
import confettiImgActive from "../../assets/confettiImgActive.png";
import balloonsImgAcive from "../../assets/balloonsImgAcive.png";
import diamondImgActive from "../../assets/diamondImgActive.png";
import baptismImgActive from "../../assets/baptismImgActive.png";
import bridalshowerImgActive from "../../assets/bridalshowerImgActive.png";
import cheersImgActive from "../../assets/cheersImgActive.png";
import dinnerImgActive from "../../assets/dinnerImgActive.png";
import mainLogo from "../../assets/logo-gold.png";
import staderFour from "../../assets/staderFour.png";
import { Badge, Spinner } from "@material-tailwind/react";
// import { createSearchParams, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setUserDetails } from "../../Redux/features/userSlice";
import { RootState } from "../../Redux/store";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import { clearAll } from "../../Utils/auth";
import { isUserAuthenticated } from "../../Utils/auth";
import { commonAxios } from "../../Axios/service";
import adminProfile from "../../assets/adminProfile.svg";
import Loader from "../../Components/Loader/Loader";
import { Helmet } from "react-helmet";
import { RxCross1 } from "react-icons/rx";
import { Drawer, IconButton } from "@material-tailwind/react";
import LogoImg from "../../assets/blixa-black.png";
import cartIcon from "../../assets/cartIcon.png";
import profileIcon from "../../assets/Profile.svg";
import wishlistIcon from "../../assets/favourite.png";
import notificationIcon from "../../assets/notification.svg";
import { XMarkIcon } from "@heroicons/react/24/outline";
import Notifications from "../../Components/Notifications/Notifications"
import notFavHeart from "../../assets/newNotfav.png";
import share from "../../assets/share.png";
import {
  createSearchParams,
  useNavigate,
  useSearchParams,
  useLocation,
} from "react-router-dom";
import { setNotification } from "../../Redux/features/notificationSlice";
import axios from "axios";
import RatingBar from "../../Components/RatingBar/RatingBar";
import { AiFillHeart } from "react-icons/ai";
import CountLoader from "../../Components/CountLoader/CountLoader";
import { getWishlistCount } from "../../Redux/features/wishlistSlice";
import './LandingPage.css'

const LandingPage = () => {
  //const userDetails = useSelector((state: RootState) => state.user.userDetails);

  const [eventsList, setEventsList] = useState<any>([]);
  const dropdownRef = useRef<HTMLDivElement>(null);
  const [isFilterClicked, setIsFilterClicked] = useState(false);
  const [dropdownVisible, setDropdownVisible] = useState(false);
  const [filterList, setFilterList] = useState<any>([]);

  const [category, setCategory] = useState<any>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [title, setTitle] = useState("");
  const [globalSearch, setGlobalSearch] = useState("Örebro");
  const [currentLocation, setCurrentLocation] = useState("Örebro");
  const [position, setPosition] = useState({});
  const [isGlobalSearch, setIsGlobalSearch] = useState<boolean>(false);
  const [openRight, setOpenRight] = useState<boolean>(false);
  const [selectedCategories, setSelectedCategories] = useState<any>([]);
  const [selectedCities, setSelectedCities] = useState<any>([]);
  const [cities, setCities] = useState<any>([]);
  const [isMaxBooked, setIsMaxBooked] = useState(false);
  const [isNewlyAdded, setIsNewlyAdded] = useState(false);
  const [amountOfPeople, setAmountofPeople] = useState<number>(0);
  const [searchParams] = useSearchParams();
  const [gridImages, setGridImages] = useState<any>([]);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [location, setLocation] = useState<any>(null);
  const [city, setCity] = useState<any>(null);
  const [isFavourite, setIsFavourite] = useState<any>(false)
  const [wishList, setWishList] = useState<any>(null);
  const [defaultRatings, setDefaultRatings] = useState('')
  const [ratingCount, setRatingCount] = useState('')
  const [quickViewDetail, setQuickViewDetail] = useState<any>({})
  const userDetails = useSelector((state: RootState) => state.user.userDetails);
  const [listOfProductsSize, setListOfProducts] = useState<any>(null);
  const [isPopupDetailsModalOpen, setIsPopupDetailModalOpen] = useState(false)
  const cartInfo = useSelector(
    (state: RootState) => state.productsCart.productDetails.quantity
  );
  const userInfo = useSelector((state: RootState) => state.user.userDetails);

  // const messageCount = useSelector(
  //   (state: RootState) => state.notification.messageCount
  // );
  // const commentCount = useSelector(
  //   (state: RootState) => state.notification.commentCount
  // );
  const wishListInfo: any = useSelector((state: RootState) => state.wishlist.wishlistDetails)
  const getEventsByLocation = async (address: any) => {
    setIsLoading(true);
    const globalSearchObj = JSON.parse(
      localStorage.getItem("globalSearchObj")!
    );
    const payload = {
      categoryList: [],
      searchKey: city,
    };

    const URL = `/cemo/bookingitems/search`;
    // setIsLoading(true);
    try {
      const response = await commonAxios.post(URL, payload);
      const gridEventData = response.data?.event?.filter((item: any, index: any) => {
        if (item.status === "ACTIVE") {
          return true
        }
        return false
      });

      // const productArray = response.data.product?.map((item: any) => {
      //   return {
      //     ...item,
      //     isProduct: true,
      //   };
      // });
      const eventArray = response.data.event?.map((item: any) => {
        return {
          ...item,
          isProduct: false,
        };
      });
      const gridData = [...gridEventData];
      setEventsList(gridData?.slice(0, 7));
      setIsLoading(false);
    } catch (error) {
    } finally {
      // setIsLoading(false);
    }
  };

  const handleShareClick = async (orderDetails: any) => {

    if (orderDetails.isProduct) {
      try {
        let text = `${window.location.href.split('/')[2]}` + '/cart?id=' + orderDetails?.appUser?.id + '&productId=' + orderDetails?.id
        await navigator.clipboard.writeText(text);
        successNotify('Link Copied')
      } catch (error) {
        failureNotify('the Link is Not Copied')
      }
    } else {
      try {
        let text = `${window.location.href.split('/')[2]}` + '/booking?id=' + orderDetails?.id
        await navigator.clipboard.writeText(text);
        successNotify('Link Copied')
      } catch (error) {
        failureNotify('the Link is Not Copied')
      }
    }
    // return pendingOrders.map((val: any) => {
    //   if (val.orderNumber === orderId) {
    //     return val.showToolTip = true
    //   }
    // })
  }


  useEffect(() => {
    // Function to get the user's current location
    const getLocation = () => {
      if (navigator.geolocation) {
        const timeoutId = setTimeout(() => {
          setCity('Örebro');
        }, 5000);

        navigator.geolocation.getCurrentPosition(
          (position) => {
            clearTimeout(timeoutId);
            const { latitude, longitude } = position.coords;
            setLocation({ latitude, longitude });
            getCityName(latitude, longitude);
          },
          (error) => {
            clearTimeout(timeoutId);
            if (error.code === error.PERMISSION_DENIED) {
              setCity('Örebro');
            }
          }
        );
      } else {

      }
    };

    getLocation();
  }, []);

  const getCityName = async (latitude: any, longitude: any) => {
    try {
      const apiKey = '8d54880e7b596d8a3f292e5215b4fdcb';
      const response = await axios.get(
        `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${apiKey}`
      );
      const cityName = response.data.name;

      setCity(cityName);
    } catch (error) {

    }
  };


  const getEventsBySearch = async (text: string) => {
    const params = {
      searchText: text,
      searchTitle: title,
      location: currentLocation,
    };
    navigate(
      {
        pathname: "/popular",
        search: `?${createSearchParams(params)}`,
      },
      {
        state: { isGlobalSearch: isGlobalSearch },
      }
    );
  };
  const getEventsByCategorySearch = async (title: string) => {

    const params = {
      searchText: city,
      searchTitle: title.toUpperCase(),
      location: currentLocation,
    };
    navigate(
      {
        pathname: "/popular",
        search: `?${createSearchParams(params)}`,
      },
      {
        state: { isGlobalSearch: isGlobalSearch, titleName: title },
      }
    );
  };

  const handleKeyDown = (event: any) => {
    setIsGlobalSearch(true);
    if (event.key === "Enter" && city !== "") {
      getEventsBySearch(city);
    }
  };

  const handleEventsearch = () => {
    setIsLoading(true)
    setIsGlobalSearch(true);
    if (city !== "") {
      getEventsBySearch(city);
    } else {
      failureNotify("Enter a value to search");
    }
  };

  const activeChange = (title: any) => {
    setTitle((prev: any) => {
      if (prev === title) {
        return "";
      } else {
        return title;
      }
    });
  };

  const handleProfileSwitchToSupplier = () => {
    navigate("/supplierlogin");
  };

  const handleProfileSwitchToUser = () => {
    navigate("/login");
  };

  const handlefilterchange = (e: any) => {
    if (e.target.checked) {
      let newFilterList = [...filterList, e.target.value];
      setFilterList(newFilterList);
    } else {
      let newFilterList = filterList?.filter(
        (item: any) => item !== e.target.value
      );
      setFilterList(newFilterList);
    }
  };

  useEffect(() => {
    const params = {
      // searchText: globalSearch,
      searchText: city,
      searchTitle: title,
      location: currentLocation,
      filterList: filterList,
    };

    localStorage.setItem("globalSearchObj", JSON.stringify(params));
  }, [globalSearch, title, currentLocation, filterList]);

  useEffect(() => {
    setTimeout(() => {
      getEventsByLocation("");
    }, 2000)

  }, [city]);

  useEffect(() => {
    getUserLocation();
  }, [])

  const togglefavourite = async (id: any) => {
    if (userDetails.id) {
      if (id.isProduct) {
        try {
          // setIsLoading(true);
          const payload = {
            appUserId: userDetails.id,
            eventId: null,
            productId: id,
          };
          await commonAxios.post("/cemo/favourite-items", payload).then(
            (response) => {
              setIsFavourite(!response.data.deleted);
              // getEventsBySearch()
              dispatch(getWishlistCount(userInfo.id));
            },
            (error) => { }
          );
        } catch (error) {
        } finally {
          // setIsLoading(false);

        }
      }
      else {
        try {
          // setIsLoading(true);
          const payload = {
            appUserId: userDetails.id,
            eventId: id.id,
            productId: null
          };
          await commonAxios.post("/cemo/favourite-items", payload).then(
            (response) => {
              setIsFavourite(!response.data.deleted);
              // getEventsBySearch()
              dispatch(getWishlistCount(userInfo.id));
            },
            (error) => { }
          );
        } catch (error) {
        } finally {
          // setIsLoading(false);
          dispatch(getWishlistCount(userInfo.id));
        }
      }
    }
  };

  const navigatePage = () => {
    if (quickViewDetail.isProduct) {
      navigate(`${`/cart?id=${quickViewDetail.appUser.id}&productId=${quickViewDetail.id}`}`)
    }
    else {
      navigate(`${`/booking?id=${quickViewDetail.id}`}`)
    }
  }


  const PopupDetailModal = () => {

    const [showFullDescription, setShowFullDescription] = useState(false);

    const toggleDescription = () => {
      setShowFullDescription(!showFullDescription);
    };

    const handleclose = () => {

      setIsPopupDetailModalOpen(false);


      document.body.style.overflow = "auto";
    };

    useEffect(() => {
      if (isPopupDetailsModalOpen) {
        document.body.style.overflow = "hidden";
      }

      return () => {

        document.body.style.overflow = "auto";
      };
    }, [openRight]);

    return (
      <div className="w-screen h-screen bg-[#00000080] fixed top-0 z-40">
        <section className="w-full h-full flex sm:flex-row flex-col items-center justify-center">
          <div className="bg-white rounded-lg border-2 border-[#D7B480] p-3 flex flex-col sm:flex-row justify-start items-start w-full sm:w-[80%] lg:w-[80%] xl:w-1/2 h-auto relative">
            <div className="absolute top-2 right-3  w-4 h-2">
              <XMarkIcon onClick={handleclose} className="h-5 w-5 cursor-pointer" />
            </div>
            <div className={`${quickViewDetail?.description.length < 200 && "flex"}`}>
              <div className="mt-1">
                <img
                  src={quickViewDetail.image}
                  className={` ${quickViewDetail?.description.length > 200 && "float-left"} w-[400px] mt-4 sm:mt-0 md:w-[24rem] lg:w-[24rem]  object-cover rounded-lg h-[20rem] mr-5`}
                  alt="Event Image"
                />
              </div>
              <section className="h-full sm:mt-0 mx-4">
                <div className="">
                  <div>
                    <h2 className="text-xl sm:text-2xl font-[sofiapro] font-semibold text-secondary -ml-1">
                      {quickViewDetail?.name}
                    </h2>
                    <h1 className="text-[#D6B37E] text-sm md:text-base font-semibold italic uppercase px-10 -ml-11">
                      - {quickViewDetail?.companyName}
                    </h1>
                    <div className="flex items-center -ml-[5px]">
                      <RatingBar ratings={defaultRatings} readonly={true} />
                      <p className="ml-3 mt-1 text-secondary text-sm">({ratingCount})</p>
                      {/* <Tooltip className='bg-[#D6B27D]' content={isFavourite ? "Remove from wishlist" : 'Add to wishlist'} placement="right-start"> */}
                      <div
                        onClick={() => togglefavourite(quickViewDetail)}
                        className="cursor-pointer ml-2"
                      >
                        {userDetails.id ? (
                          isFavourite ? (
                            <div className="rounded-full p-2 hover:bg-[#e9e3e3ef]">
                              <AiFillHeart className="h-6 w-6 text-[#D6B27D]" />
                            </div>
                          ) : (
                            <div className="rounded-full p-2 hover:bg-[#e9e3e3ef]">
                              <img src={notFavHeart} className="h-5 w-5 z-40 hover:text-[#D6B27D]" />
                            </div>
                          )
                        ) : null}
                      </div>
                      <div className="rounded-full p-2 hover:bg-[#e9e3e3ef] cursor-pointer ml-2" onClick={() => handleShareClick(quickViewDetail)}> <img src={share} className="h-5 w-5 text-[#D6B27D] z-40 " /></div>
                    </div>
                  </div>
                </div>
                {quickViewDetail?.description.length > 200 ?
                  <div>
                    {showFullDescription ? (
                      <>
                        <h1 className="text-secondary">{quickViewDetail?.description}</h1>
                        <span style={{ cursor: 'pointer' }} onClick={toggleDescription} className="text-secondary">
                          {' '}
                          ... Read less
                        </span>
                      </>
                    ) : (
                      <>
                        <h1 className="text-secondary">{quickViewDetail?.description.slice(0, 200)}</h1> {/* Display the first 200 characters */}
                        <span style={{ cursor: 'pointer' }} onClick={toggleDescription} className="text-secondary">
                          {' '}
                          ... Read more
                        </span>
                      </>
                    )}
                  </div>
                  : <>
                    <p className="mb-0 mt-0 text-secondary text-lg font-normal">
                      {quickViewDetail?.description
                        ?.split(".")
                        ?.slice(0, 2) // Adjust the number of sentences to include as needed
                        ?.join(".")}
                    </p>
                  </>}
                <br />
                <div className="flex flex-row justify-left">
                  <div className=""
                    onClick={navigatePage}
                  >
                    {quickViewDetail.isProduct ? (
                      <Button btnName={"Köp"} />
                    ) : (
                      <Button btnName={"Besök"} />
                    )}
                  </div>
                  {(quickViewDetail.isProduct && userInfo?.role !== "ROLE_SUPPLIER" && userInfo?.role !== "ROLE_ADMIN") && (
                    <div className="bg-[#D6B27D] p-2 rounded-xl shadow-2xl mx-2">
                      <button
                        className={`font-semibold md:[text-16px] rounded-lg bg-[#D6B37D] text-white px-2 uppercase`}
                      >
                        Quick Buy
                      </button>
                    </div>
                  )}
                </div>
              </section>
            </div>
          </div>
        </section>
      </div>
    );
  };

  // useEffect(() => {
  //   const handleClickOutside = (event: MouseEvent) => {
  //     if (
  //       dropdownRef.current &&
  //       !dropdownRef.current.contains(event.target as Node)
  //     ) {
  //
  //       setIsFilterClicked(false);
  //     }
  //   };

  //   document.addEventListener("click", handleClickOutside);
  //   return () => {
  //     document.removeEventListener("click", handleClickOutside);
  //   };
  // }, []);

  function showPosition(position: any) {
    const latitude = 59.275871250388676;
    const longitude = 15.212776853330473;

    setPosition({
      latitude,
      longitude,

    });
    // fetch(`https://nominatim.openstreetmap.org/reverse?lat=${latitude}&lon=${longitude}&format=json`)
    // fetch(
    //   `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&result_type=administrative_area_level_2&key=AIzaSyDYsIF0ap_NrOL3xyjf1yB-3IkhN0O4lSI`
    // )
    //   .then((response) => response.json())
    //   .then((data) => {
    //     const address =
    //       data.results[0].address_components[0].long_name.split(" ")[0];
    //     const pincode = data.results[0].address_components[0].long_name;
    //
    //
    //     setGlobalSearch(address);
    //     setCurrentLocation(address);
    //     getEventsByLocation(address);
    //   })
    //   .catch((error) =>

    setGlobalSearch("Örebro");
    getEventsByLocation("Örebro");
    setCurrentLocation("Örebro");
  }

  const getUserLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else {
    }
  };

  // const [locations, setLocations] = useState([]);

  // const handleInputChange = () => {
  //     // Make API request to Google Places API
  //     fetch(
  //         `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${globalSearch}&key=AIzaSyDYsIF0ap_NrOL3xyjf1yB-3IkhN0O4lSI`
  //     )
  //         .then((response) => {
  //             return response.json()
  //         })
  //         .then(response => {
  //             setLocations(response.predictions)
  //         })
  //         .catch((error) => {
  //
  //         });
  // };
  // useEffect(() => {
  //     if (globalSearch !== "") {
  //         handleInputChange();
  //     }
  //     if (globalSearch === "") {
  //         setLocations([])
  //     }
  // }, [globalSearch])

  const imgPerPage = 10;

  let loadImg = 0;
  const filterLabels = [
    "Mat",
    "Aktiviteter",
    "Dryck",
    "Upplevelser",
    "Utbildning",
    "Musik",
    "Äventyr",
  ];
  const searchData = [
    {
      img: danceImg,
      imgActive: danceImgActive,
      title: "Barnkalas",
    },
    {
      img: confettiImg,
      imgActive: confettiImgActive,
      title: "fodelsedag",
    },
    {
      img: balloonsImg,
      imgActive: balloonsImgAcive,
      title: "fest",
    },
    {
      img: diamondImg,
      imgActive: diamondImgActive,
      title: "brollop",
    },
    {
      img: baptismImg,
      imgActive: baptismImgActive,
      title: "dop",
    },
    {
      img: bridalshowerImg,
      imgActive: bridalshowerImgActive,
      title: "mohippa",
    },
    {
      img: cheersImg,
      imgActive: cheersImgActive,
      title: "afterwork",
    },
    {
      img: dinnerImg,
      imgActive: dinnerImgActive,
      title: "middag",
    },
    {
      img: cheersImg,
      imgActive: cheersImgActive,
      title: "mat",
    },
    {
      img: dinnerImg,
      imgActive: dinnerImgActive,
      title: "musik",
    },
  ];

  const getCartDetails = async () => {

    if (userDetails.id) {

      if (userDetails.role === "ROLE_CUSTOMER") {
        try {
          setIsLoading(true);
          const response = await commonAxios.get(
            `/cemo/bookingitems/get/cart/${userDetails.id}`
          );
          const listOfProducts = response.data.bookingItemList.filter(
            (item: any) => item.bookingItemType.toLowerCase() === "product"
          );

          if (listOfProducts) {
            setListOfProducts(listOfProducts.length);
          } else {
            setListOfProducts(0)
          }
        } catch (error) {
        } finally {
          setIsLoading(false);
        }
      }
    } else {
      const listOfProducts = JSON.parse(localStorage.getItem("cartListDetails") as string);
      if (listOfProducts) {
        setListOfProducts(listOfProducts.quantity || 0);
      }
    }
  };

  const getWishlist = async () => {
    try {
      // setIsLoading(true);
      const response = await commonAxios.get(
        `/cemo/favourite-items/${userInfo.id}`
      );

      setWishList(response.data);
      // setInitialPageData(response.data.slice(0, perPage))
    } catch (error) {
    } finally {
      // setIsLoading(false);
    }
  };

  useEffect(() => {
    if (userDetails?.id !== "") {
      getCartDetails();
      getWishlist();
    }
  }, [cartInfo]);

  const getMessageCount = async () => {
    try {
      const messageCount = await commonAxios.get(
        `cemo/appuser/notification/count?supplierId=&customerId=${userDetails.id}`
      );

      if (messageCount.data) {
        dispatch(setNotification(messageCount.data));
      }
    } catch (e) { }
  };

  const handleLogout = () => {
    dispatch(setUserDetails({}));
    successNotify("Logout successful");
    clearAll();
    navigate("/login");
  };
  const getAllCategories = async () => {
    try {
      const response = await commonAxios.get(`/cemo/categories`);

      const activeCategories = response.data.filter((item: any) => item.status === "ACTIVE");

      setCategory(activeCategories);
    } catch (e) { }
  };

  const getAllCities = async () => {
    try {
      const response = await commonAxios.get(`/cemo/events/cities`);

      let new_array: any = [];
      response.data.map((val: any) => {
        if (!new_array.includes(val)) {
          return new_array.push(val);
        }
      });
      setCities(new_array);
    } catch (e) { }
  };
  useEffect(() => {
    // getEventsBySearch();
    getAllCategories();
    getAllCities();
    if (userDetails.id) {
      getMessageCount();
    }
  }, []);

  // const updateProfile = () => {
  //
  //   setEditedUserDetails({
  //     firstName: userDetails.firstName,
  //     lastName: userDetails.lastName,
  //     telephone: userDetails.telephone,
  //     id: userDetails.id,
  //     mobile: userDetails.mobile,
  //     profileImage: userDetails.profileImage,
  //   });
  //   toggleDropdown();
  //   toggleEditModal();
  //
  //   setBindedImage([{ id: 1, image: userDetails.profileImage }]);
  // };

  const toggleDropdown = () => {
    setDropdownVisible(!dropdownVisible);
  };

  const openDrawerRight = () => {
    setOpenRight(true);
  };
  const closeDrawerRight = () => {
    setOpenRight(false);
  };
  // const handleChangeForMaxBooking = (e: any) => {
  //   setIsMaxBooked(e.target.checked);
  // };

  const handleChangeForNewlyAdded = (e: any) => {
    setIsNewlyAdded(e.target.checked);
  };

  const handleChangeForPeople = (e: any) => {
    setAmountofPeople(e.target.value);
  };

  const handleChangeforCategories = (e: any) => {
    const isChecked = e.target.checked;
    const values = e.target.name;
    if (isChecked) {
      setSelectedCategories((prevSelected: any) => [...prevSelected, values]);
    } else {
      setSelectedCategories((prevSelected: any) =>
        prevSelected.filter((value: any) => value !== values)
      );
    }
  };

  const handleFilter = async (e: any) => {
    setIsLoading(true);
    e.preventDefault();
    const filterBody = {
      cityList: selectedCities,
      categoryList: selectedCategories,
      newlyAdded: isNewlyAdded,
      maxBooked: isMaxBooked,
      amountOfPeople: amountOfPeople ? Number(amountOfPeople) : 0,
      searchKey: searchParams.get("searchText"),
    };
    try {
      const response = await commonAxios.post(
        `/cemo/bookingitems/filter`,
        filterBody
      );

      if (response.data) {
        const productArray = (response.data.product || []).filter(
          (item: any) => item.status === "ACTIVE"
        ).map((item: any) => {
          return {
            ...item,
            isProduct: true,
          };
        });
        const eventArray = (response.data.event || []).filter(
          (item: any) => item.status === "ACTIVE"
        ).map((item: any) => {
          return {
            ...item,
            isProduct: false,
          };
        });
        const gridData: any = [...eventArray, ...productArray];
        const values = {
          data: gridData,
          category: selectedCategories,
          city: selectedCities
        }
        setGridImages(gridData?.slice(loadImg, imgPerPage));
        navigate('/popular', { state: { data: values } });
        setIsMaxBooked(false);
        setIsNewlyAdded(false);
        setSelectedCategories([]);
        setSelectedCities([]);
        setOpenRight(false);
        setIsLoading(false);
      }
    } catch (e) {
    } finally {
    }
  };

  const handleChangeForCities = (e: any) => {
    const isChecked = e.target.checked;
    const values = e.target.name;
    if (isChecked) {
      setSelectedCities((prevSelected: any) => [...prevSelected, values]);
    } else {
      setSelectedCities((prevSelected: any) =>
        prevSelected.filter((value: any) => value !== values)
      );
    }
  };
  const handleUpdateFavourite = async (id: any) => {
    if (userDetails.id) {
      if (id.isProduct) {
        try {
          setIsLoading(true);
          const payload = {
            appUserId: userDetails.id,
            eventId: null,
            productId: id,
          };
          await commonAxios.post("/cemo/favourite-items", payload).then(
            (response) => {
              getEventsByLocation(response)
              //setIsFavourite(!response.data.deleted);
            },
            (error) => { }
          );
        } catch (error) {
        } finally {
          setIsLoading(false);
        }
      }
      else {
        try {
          setIsLoading(true);
          const payload = {
            appUserId: userDetails.id,
            eventId: id,
            productId: null
          };
          await commonAxios.post("/cemo/favourite-items", payload).then(
            (response) => {
              getEventsByLocation(response)
              //setIsFavourite(!response.data.deleted);
            },
            (error) => { }
          );
        } catch (error) {
        } finally {
          setIsLoading(false);
        }
      }
    }
  }

  let currentUrl = window.location.href;

  const pathname = window.location.pathname;

  return (
    <div className="">
      <Helmet>
        ‍<title>Blixa</title>‍
        <meta name="description" content="Mange your events with us" />
        <meta property="og:title" content="Blixa" />
        <meta
          property="og:description"
          content="Best Event management platform"
        />
        <meta
          property="og:image"
          content="https://ocm-images.fra1.cdn.digitaloceanspaces.com/assets/ocm_logo.png"
        />
        <meta property="og:url" content="app.ourcreativemoments.se" />
        <meta property="og:site_name" content="creative moments" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="events" />
        <meta property="fb:app_id" content="100087915611006" />
      </Helmet>
      {/* {isLoading ? <Loader /> : null} */}
      <Drawer
        placement="right"
        size={499}
        open={openRight}
        onClose={closeDrawerRight}
        className="py-4"
      >
        <div className="px-5 h-[100vh] overflow-y-scroll">
          <div className="flex justify-between ">
            <p className="font-semibold mb-2 text-xl text-[#D6B27D]">Kategorier</p>
            <div className="flex items-center justify-between">
              <IconButton
                variant="text"
                color="blue-gray"
                onClick={closeDrawerRight}
              >
                <XMarkIcon className="h-4 w-4 border border-blue-gray" />
              </IconButton>
            </div>
          </div>
          <div className="grid grid-cols-2">

            {category &&
              category.map((val: any) => {
                return (
                  <>
                    <div className="flex m-1">
                      <input
                        type="checkbox"
                        onChange={handleChangeforCategories}
                        name={val.categoryName}
                        className="cursor-pointer accent-brown-300"
                        id={val.categoryName}
                      />
                      <label
                        className="ms-3 text-[10px] sm:text-[14px]  cursor-pointer text-secondary "
                        htmlFor={val.categoryName}
                      >
                        {val.categoryName.charAt(0).toUpperCase() + val.categoryName.slice(1).toLowerCase()}
                      </label>
                    </div>
                  </>
                );
              })}
          </div>
          <p className="mt-5 font-semibold text-xl text-[#D6B27D]">Stad</p>
          <div className="mt-2 grid grid-cols-3">
            {cities &&
              cities.map((val: any) => {
                return (
                  <>
                    <div className="flex m-1">
                      <input
                        type="checkbox"
                        name={val}
                        onChange={handleChangeForCities}
                        className="cursor-pointer accent-brown-300"
                        id={val}
                      // checked={cities.includes("trichy")}
                      />
                      <label
                        htmlFor={val}
                        className="text-[10px] sm:text-[14px] ms-3 cursor-pointer text-secondary"
                      >
                        {val.charAt(0).toUpperCase() + val.slice(1).toLowerCase()}
                      </label>
                    </div>
                  </>
                );
              })}
          </div>
          <div className="flex flex-row justify-start items-center gap-5">
            {/* <div className="mt-5 -ml-5 font-semibold text-xl text-[#D6B27D]">
              <input
                id="maxBooked"
                className="ms-5 cursor-pointer accent-brown-300"
                type="checkbox"
                onChange={handleChangeForMaxBooking}
              />
              <label htmlFor="maxBooked" className="sm:text-md lg:text-lg text-[12px] ms-2 cursor-pointer">Max antal gäster</label>
            </div> */}
            <div className="mt-5 font-semibold text-xl text-[#D6B27D]">
              <input
                id="newlyAdded"
                type="checkbox"
                className="cursor-pointer accent-brown-300"
                onChange={handleChangeForNewlyAdded}
              />
              <label htmlFor="newlyAdded" className="text-md sm:text-md lg:text-lg text-[12px] ms-2 cursor-pointer">Nyligen tillagda</label>
            </div>
          </div>
          <div className="mt-5 font-semibold md:text-xl text-md text-[#D6B27D] items-center">
            <label className="text-start" style={{ marginLeft: '2px' }}>Antal gäster</label>
            <div className="flex items-center justify-center pagination-type">
              <button
                disabled={amountOfPeople === 0}
                onClick={() => setAmountofPeople((prev: any) => prev - 1)}
                className="rounded-lg w-8 h-8 accent-brown-300 cursor-pointer flex items-center justify-center mr-2 border-gray-400 outline-none focus:border-gray-700"
              >
                <span className="text-secondary relative">-
                  <span className="vertical-left"></span>
                </span>

              </button>

              <div className="flex items-center">
                <input
                  type="number"
                  onChange={handleChangeForPeople}
                  className="rounded-lg w-2 sm:w-6 h-5 sm:h-8 accent-brown-300 cursor-pointer text-center mt-[2px] flex-grow border-gray-400 outline-none focus:border-gray-700"
                  name="amountOfPeople"
                  value={amountOfPeople}
                />
              </div>
              <button
                onClick={() => setAmountofPeople((prev: any) => prev + 1)}
                className="rounded-lg w-8 h-8 accent-brown-300 cursor-pointer flex items-center justify-center ml-2 border-gray-400 outline-none focus:border-gray-700"
              >
                <span className="text-secondary relative">+
                  <span className="vertical-right"></span>
                </span>
              </button>
            </div>

          </div>
          {isLoading && <Loader />}
          <div className="md:ml-10 flex justify-center mt-8 mb-10">
            <button
              type="submit"
              onClick={handleFilter}
              className="py-2 w-auto font-semibold rounded-lg bg-[#D6B37D] text-white uppercase px-6"
            >
              Filtrera
            </button>
          </div>
        </div>
      </Drawer>
      {
        isPopupDetailsModalOpen && <PopupDetailModal />
      }
      <div
        className="h-[130vh] sm:h-screen bg-cover landing-page-image"
        // landing-page-image"
        style={{ backgroundImage: `url(${heroImage})` }}
      >
        <div className="flex justify-between">
          {!isUserAuthenticated() ? (
            <>
              <div>
                <img
                  src={mainLogo}
                  onClick={() => navigate("/")}
                  className="sm:w-[200px] w-[100px] sm:px-4 m-5 cursor-pointer"
                  style={{ marginTop: '-8px' }}
                />
              </div>
              {!userDetails.id && (
                <div>
                  <button
                    className="p-2 w-auto m-5 font-semibold rounded-lg bg-[#D6B37D] text-white px-6 "
                    onClick={handleProfileSwitchToUser}
                  >
                    LOGGA IN
                  </button>
                </div>
              )}
            </>
          ) : (
            <>
              <div>
                <img
                  src={mainLogo}
                  className="sm:w-[200px] w-[100px] sm:px-4 m-5 cursor-pointer"
                />
              </div>
              {/* <button className='text-[#D6B27D] text-[24px] p-3 sm:px-8 rounded-lg font-medium mt-4' onClick={handleLogout}>Logout</button> */}
              {userDetails.role !== "ROLE_CUSTOMER" ? (
                <div>
                  <button
                    className="text-white bg-[#D6B37D] text-[16px] p-2 sm:px-6 rounded-lg font-bold mt-7 mr-7 uppercase flex items-center justify-center"
                    onClick={() => navigate("/orderList")}
                  >
                    Min profil
                  </button>
                </div>
              ) : null}
            </>
          )}
          {userDetails.role === "ROLE_CUSTOMER" ? (
            <div
              ref={dropdownRef}
              className={`flex flex-col cursor-pointer sm:mr-10 w-[30%] sm:w-[22%] md:w-[20%] lg:w-[14%] xl:w-[12%] ${dropdownVisible ? "rounded-md border-[#D6B27D] border-2" : null
                } mt-7`}
            >
              <div className="flex flex-row justify-between items-center">
                {userDetails.role === "ROLE_CUSTOMER" ? (
                  <div
                    className="items-center"
                  >
                    <Notifications />
                  </div>
                ) : null}

                {userDetails.role === "ROLE_CUSTOMER" ? (
                  <div
                    className="cart-item grid items-center"
                    onClick={() =>
                      navigate({
                        pathname: "/ordershistory",
                        search: `?${createSearchParams({
                          activeMenu: "Favourites",
                        })}`,
                      })
                    }
                  >
                    <Badge
                      content={wishListInfo ? <span>{wishListInfo.wishlist?.length}</span> : <CountLoader />}
                      className="bg-[#D6B27D] border border-white min-w-[12px] min-h-[12px] w-[20px] h-[20px] "
                    >
                      <img
                        src={wishlistIcon}
                        className="cursor-pointer w-[30px] h-[30px] filter brightness-0 invert"
                        style={{ objectPosition: '0', objectFit: 'cover' }}
                      />
                    </Badge>
                  </div>
                ) : null}
                {userDetails.role === "ROLE_CUSTOMER" ? (
                  <div
                    className="cart-item grid items-center"
                    onClick={() => {
                      navigate(
                        {
                          pathname: "/shoppingCart",
                        },
                        {
                          state: { currentUrl },
                        }
                      );
                    }}
                  >
                    <Badge
                      content={listOfProductsSize != null ? <span>{listOfProductsSize}</span> : <CountLoader />}
                      className="bg-[#D6B27D] border border-white min-w-[12px] min-h-[12px] w-[20px] h-[20px]"
                    >
                      <img src={cartIcon} className="cursor-pointer w-[30px] h-[30px] filter brightness-0 invert" style={{ objectPosition: '0', objectFit: 'cover' }} />
                    </Badge>
                  </div>
                ) : null}

                <div className="grid items-center" onClick={() =>
                  navigate("/ordershistory?activeMenu=Orders")
                }>
                  {userDetails.profileImage ? (
                    <img
                      className="w-[30px] h-[30px] rounded-full"
                      src={userDetails.profileImage}
                      style={{ objectPosition: '0', objectFit: 'cover' }}
                    />
                  ) : (
                    <span className="h-10 w-10 mb-2 ml-2 flex justify-center items-center text-center rounded-full font-semibold bg-[#D6B27D] text-white">
                      {" "}
                      {userDetails.firstName.split("").at(0)?.toUpperCase() +
                        "" +
                        userDetails.lastName.split("").at(0)?.toUpperCase()}
                    </span>
                  )}
                </div>
              </div >
            </div >
          ) : null}
        </div >
        <div className="flex flex-col justify-center items-center h-[70vh] mt-24 mb-32">
          <div className="grid grid-cols-3 md:grid-cols-5 gap-4 justify-center md:justify-between mt-7 md:w-[440px]">
            {searchData.map((item) => {
              return (
                <SearchCard
                  img={title === item.title ? item.imgActive : item.img}
                  title={item.title}
                  key={item.title}
                  activeChange={activeChange}
                  onClick={getEventsByCategorySearch}
                  isActive={title === item.title}
                />
              );
            })}
          </div>
          <div className="relative mt-10 md:w-[450px] mb-20">
            <input
              type="text"
              placeholder="Search event, place etc..."
              className="border-none py-5 pl-16 pr-8 rounded-lg border-2 border-gray-300 w-full sm:w-1/2 md:w-[450px] text-secondary placeholder:text-secondary placeholder:text-base focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent"
              value={city}
              onChange={(event: any) => setCity(event.target.value)}
              onKeyDown={handleKeyDown}
            />
            <div
              onClick={getUserLocation}
              className="absolute left-5 top-5 cursor-pointer"
            >
              <img src={gpsIcon} />
            </div>
            <div
              onClick={handleEventsearch}
              className="absolute cursor-pointer right-16 top-5 opacity-75 sm:opacity-100"
            >
              <img src={searchIcon} />
            </div>
            <div className="border-t-2 cursor-pointer border-[#D6B27D] w-6 h-1 transform rotate-90 absolute right-10 top-[30px]"></div>
            <div
              onClick={() => setOpenRight(!openRight)}
              className="absolute cursor-pointer right-4 top-5"
            >
              <img src={filterIcon} />
            </div>
            <div
              className={`absolute flex flex-col items-center justify-between gap-5 transition-all ${isFilterClicked ? "block" : "hidden"
                } -right-60 top-0 z-40 py-5 pr-20 pl-7 bg-[#FFF5F4] rounded-md`}
            >
              <RxCross1
                className="absolute top-2 right-2 cursor-pointer"
                onClick={() => setIsFilterClicked(false)}
                style={{ color: '#A09792' }}
              />
              {filterLabels?.map((item: any) => {
                return (
                  <section className="flex items-center w-full gap-3">
                    <input
                      checked={filterList.includes(item)}
                      onChange={handlefilterchange}
                      type="checkbox"
                      name={item}
                      id={item}
                      key={item}
                      value={item}
                    />
                    <label className="font-semibold" htmlFor={item}>
                      {item}
                    </label>
                  </section>
                );
              })}
            </div>
            {/* <div className={`w-full absolute z-50 bg-white rounded-bl-lg rounded-br-lg transition-all duration-300 ${locations.length > 0 ? 'h-fit p-6' : 'h-0 p-0'}`}>
                            <ul className='w-full h-full'>
                                {locations.map((location: any) => (
                                    <li className='py-2 font-medium cursor-pointer hover:text-[#D6B27D] transition' key={location.place_id}>{location.description}</li>
                                ))}
                            </ul>
                        </div> */}
          </div>
        </div>
      </div >
      <div className="">
        <div className="py-5 flex justify-between items-center md:mx-16 px-3 md:px-0">
          <h1 className="text-sm sm:text-2xl font-semibold font-[sofiapro] text-secondary">
            Några av våra rekommenderade aktiviteter{" "}
          </h1>
          {currentLocation !== "" && (
            <button className="bg-[#D6B27D] w-auto p-2 px-4 sm:px-6 rounded-lg text-white font-bold text-lg flex justify-center items-center">
              <img src={locationImg} className="h-[25px] px-2 -ml-[10px]" />{" "}
              <span className="mt-1">{city}</span>
            </button>
          )}
        </div>
        <>
          {isLoading ? (
            <div className="">{isLoading && <div className="w-full h-96 my-4 flex items-center justify-center bg-[#FFE5D8] opacity-90 z-50">
              <div className="border-t-8 border-[#F7CB73] rounded-full animate-spin h-12 w-12"></div>
            </div>}</div>
          ) : !isLoading && eventsList.length > 0 ? (
            <>
              <div className="flex flex-row justify-between items-center flex-wrap md:mx-16 relative z-20 gap-4">
                {
                  eventsList.map((event: any, i: any) => {
                    if (i < 3) {
                      return (
                        <>
                          <div
                            className="w-full sm:w-[48%] md:w-[32%] h-96 flex justify-center mt-4"
                          >
                            <BookingCard
                              cardImg={event.image}
                              businessName={event.companyName}
                              name={event.name}
                              isFavourite={event.favourite}
                              id={event.id}
                              value={togglefavourite}
                              isFirstRow={true}
                              actualEvent={event}
                              setIsPopupDetailModalOpen={setIsPopupDetailModalOpen}
                              setDefaultRatings={setDefaultRatings}
                              setIsLoading={setIsLoading}
                              setQuickViewDetail={setQuickViewDetail}
                              setRatingCount={setRatingCount}
                              setIsFavourite={setIsFavourite}
                            />
                          </div>
                        </>
                      )
                    }
                  })
                }
              </div>
              <div className="sm:gap-0 py-8 md:mx-16 flex justify-center">
                <div className="md:grid grid-cols-4 gap-6 flex flex-col justify-center ">
                  {
                    eventsList.map((event: any, i: any) => {
                      if (i > 2) {
                        return (
                          <>
                            <div

                              className="w-full h-96 flex justify-center mt-2"
                            >
                              <BookingCard
                                cardImg={event.image}
                                businessName={event.companyName}
                                name={event.name}
                                isFavourite={event.favourite}
                                id={event.id}
                                value={togglefavourite}
                                isFirstRow={false}
                                actualEvent={event}
                                setDefaultRatings={setDefaultRatings}
                                setIsLoading={setIsLoading}
                                setQuickViewDetail={setQuickViewDetail}
                                setRatingCount={setRatingCount}
                                setIsFavourite={setIsFavourite}
                                setIsPopupDetailModalOpen={setIsPopupDetailModalOpen}
                              />
                            </div>
                          </>
                        )
                      }
                    })
                  }
                </div>
              </div>
            </>
          ) : (
            <p className="text-center font-bold text-3xl py-28 my-36 text-secondary">
              No nearby events in your location
            </p>
          )}
        </>
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:gap-0 px-5 sm:px-0">
          <div className="lg:px-16 lg:py-12 sm:p-5">
            <h2 className="text-[16px] font-semibold tracking-[7px]  text-[#D6B27D] uppercase lg:mt-1 text-center md:text-left">
              Inspireras här
            </h2>
            {/* <h2 className="uppercase mt-5 text-center md:text-left lg:text-[38px] font-[sofiapro] font-bold text-secondary">
              Event Places
            </h2> */}
            <h1 className="text-3xl font-bold text-center md:text-left md:mt-5 text-secondary">
              Från första skrattet till sista farväl
            </h1>
            <p className="text-lg font-normal text-center md:text-left md:mt-5 text-secondary">
              Livet är en resa fylld med många speciella stunder –
              från de första skratten till de sista farvälen.
              Hos oss på Blixa förstår vi vikten
              av att omfamna varje ögonblick. Vi är så stolta över
              att erbjuda en plattform som täcker hela spektrat av
              livsupplevelser. Från glädjefyllda babyshower till
              minnesvärda stunder av samhörighet och till och med
              mer formella tillställningar som begravningar.
              Vi är här för att skapa minnen som vara en livstid.
              Välkommen till en plattform som omfamnar livets alla delar.
            </p>
            {/* <div className="mt-10 w-28 mx-auto flex md:w-full">
              <Button btnName={"Besök"} />
            </div> */}
          </div>
          <div className="mt-4 md:mt-0">
            <img src={roseFlower} className="w-full" />
          </div>
        </div>
        <div className="grid grid-cols-1 sm:grid-cols-2 mt-6 md:mt-0 lg:gap-0 px-5 sm:px-0">
          <div className="">
            <img src={ballonImg} className="w-full" />
          </div>
          <div className="lg:px-16 lg:py-12 sm:p-5 mt-2 md:mt-0">
            <h2 className="text-[16px] tracking-[7px] mt-5 text-[#D6B27D] font-semibold uppercase lg:mt-1 text-center md:text-left">
              Inspireras här
            </h2>
            {/* <h2 className="uppercase mb-1 mt-5 text-center md:text-left lg:text-[38px] font-[sofiapro] text-secondary font-bold">
              Födelsedagskalas
            </h2> */}
            <h1 className="text-3xl font-bold text-center md:text-left md:mt-5 text-secondary">
              Stärk ditt team genom oss
            </h1>
            <p className="text-lg font-normal text-center md:text-left md:mt-5 text-secondary">
              För att bygga ett starkt företag krävs ett ännu starkare team.
              Upptäck hur smidigt det är att planera och genomföra företagsevent,
              kunddagar eller teambildning hos oss. Blixa frigör
              tid och energi för det som verkligen betyder något för er.
              Det är inte bara tidsbesparande att boka genom oss,
              vi inspirerar också till att utmana era komfortzoner och
              skapa oförglömliga stunder tillsammans.
            </p>
            {/* <div className="mt-10 w-28 mx-auto flex md:w-full">
              <Button btnName={"Besök"} />
            </div> */}
          </div>
        </div>
        <div className="grid grid-cols-6 justify-start items-start xl:grid-cols-12 mt-10 gap-6 md:px-0 md:mx-16">
          {/* <div className="col-span-2">
            <img
              src={ballonTwo}
              alt="Image 1"
              className="mt-3 object-cover lg:h-[150px] lg:w-[200px] rounded-lg"
            />
            <img
              src={ballonOne}
              alt="Image 2"
              className="mt-3 object-cover lg:h-[150px] lg:w-[200px] rounded-lg"
            />
            <img
              src={ballonThree}
              alt="Image 3"
              className="mt-3 object-cover lg:h-[150px] lg:w-[200px] rounded-lg"
            />
          </div> */}
          {/* <div className="col-span-4 relative">
            <img
              src={ballonTwo}
              alt="Image 1"
              className="mt-3 object-cover h-full lg:h-[470px] rounded-lg"
            /> */}
          {/* <div className="absolute bottom-6 lg:bottom-24 right-4 lg:right-0">
              <Button btnName={"Besök"} />
            </div> */}
          {/* </div> */}
          {/* <div className="lg:px-8 col-span-6 px-4">
            <h1 className="text-[24px] sm:mt-1.5 font-medium md:ml-14 text-[#D6B27D]">
              Our Application Features
            </h1>
            <h1 className="sm:text-[36px] font-semibold mt-5 text-secondary lg:text-[38px] font-[sofiapro] md:ml-14">
              Our Event Places
            </h1>
            <p className="text-secondary mt-6 text-lg md:ml-14">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, Lorem Ipsum has been the industry's standard
              dummy text ever since the 1500s, Lorem Ipsum is simply dummy text
              of the printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s, Lorem Ipsum
              has been the industry's standard dummy text ever since the 1500s.
            </p>
          </div> */}
        </div>
        <div className="mt-1 mx-6 md:mx-16">
          <h1 className="font-semibold text-[12px] sm:text-[1rem] leading-7 text-[#D6B27D]">
            Städer
          </h1>
          <h1 className="sm:text-[32px] font-semibold font-[sofiapro] text-secondary">
            Hitta oss dessa städer och manga fler
          </h1>
          <div className="grid grid-cols-1 sm:grid-cols-2 mb-6 md:grid-cols-2 lg:grid-cols-4 mt-4 gap-4 md:gap-6">
            <div className="grid-item">
              <StaderCard
                onCardClick={getEventsBySearch}
                staderImg={staderOne}
                staderTitle={"Örebro"}
              />
            </div>
            <div className="grid-item">
              <StaderCard
                onCardClick={getEventsBySearch}
                staderImg={staderTwo}
                staderTitle={"Stockholm"}
              />
            </div>
            <div className="grid-item">
              <StaderCard
                onCardClick={getEventsBySearch}
                staderImg={staderThree}
                staderTitle={"Uppsala"}
              />
            </div>
            <div className="grid-item">
              <StaderCard
                onCardClick={getEventsBySearch}
                staderImg={staderFour}
                staderTitle={"Västerås"}
              />
            </div>
          </div>
        </div>
      </div>
    </div >
  );
};

export default LandingPage;