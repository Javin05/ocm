import ShoppingCart from "../../Components/ShoppingCart/ShoppingCart";
import NewsLetter from "../../Components/NewsLetter/NewsLetter";
import { useEffect, useState, useRef, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { commonAxios } from "../../Axios/service";
import { RootState } from "../../Redux/store";
// import Loader from "../../Components/Loader/Loader";
import { HiChevronRight, HiChevronLeft } from "react-icons/hi";
import { AiFillHeart } from "react-icons/ai";
import { Tooltip } from "@material-tailwind/react";
import {
  createSearchParams,
  useLocation,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import { addProductToCart } from "../../Redux/features/productCartSlice";
import "./Shopping.css";
import BreadCrumb from "../../Components/BreadCrumb/BreadCrumb";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import { AuthContext } from "../RootLayout/RootLayout";
import { getCartListCount } from "../../Redux/features/cartCountSlice";
const Shopping = () => {
  const location: any = useLocation();
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const dispatch = useDispatch();
  const [cartList, setCartList] = useState<any>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [subTotal, setSubTotal] = useState(0);
  const [total, setTotal] = useState(0);
  const [tax, setTax] = useState(0);
  const navigate = useNavigate();
  const [count, setCount] = useState<any>(0);
  const [searchParams] = useSearchParams();
  const [url, setUrl] = useState("/shoppingCart");
  const { setBookingLogin } = useContext(AuthContext);
  const locationData = useLocation();
  const previousUrlRef = useRef("");

  const pageRoutes = [
    {
      pathname: "/popular",
      name: "Popular Activities",
    },
  ];

  const state = location.state;

  const breadsList2 = [
    {
      name: "Home",
      naviagteTo: "/",
    },
    {
      name: state?.currentUrls,
      naviagteTo: "dynamicRoute",
    },
    {
      name: "Kundvagn",
    },
  ];

  const breadsList = [
    {
      name: "Home",
      naviagteTo: "/",
    },
    {
      name:
        state?.currentUrl?.split("/")[3][0]?.toUpperCase() +
        state?.currentUrl?.split("/")[3]?.split("?")[0]?.slice(1) &&
        state?.currentUrl?.split("/")[3][0]?.toUpperCase() +
        state?.currentUrl?.split("/")[3]?.split("?")[0]?.slice(1),
      naviagteTo: "dynamicRoute",
    },
    {
      name: "Kundvagn",
    },
  ];

  // Get the cart items while customer login

  const getCartDetails = async () => {

    setIsLoading(true);
    try {
      const response = await commonAxios.get(
        `/cemo/bookingitems/get/cart/${userInfo.id}`
      );
      console.log('response', response)
      localStorage.setItem("cartListDetails", JSON.stringify(response && response.data));
      const listOfProducts = response.data.bookingItemList.filter(
        (item: any) => item.bookingItemType.toLowerCase() === "product"
      );
      if (response.data?.bookingItemList?.length === 0) {
        localStorage.removeItem("cartSupplier");
      }
      setCartList(listOfProducts);
      const quantity = listOfProducts.map((item: any) => item.quantity);
      setCount(quantity);
      dispatch(addProductToCart());
      // calculating subtotal of fetched products list
      // let totalPrice = 0;
      let bookingItems: any[] = [];
      listOfProducts.forEach((item: any) => {
        //   totalPrice += (item.quantity * item.price)
        bookingItems.push(item.id);
      });
      setSubTotal(response.data.totalVATDetailsDTO.overAllWithoutVATAmount);
      setTax(response.data.totalVATDetailsDTO.overAllVATAmount);
      setTotal(response.data.totalVATDetailsDTO.overAllTotalAmount);
      //setPaymentDetails(response.data);
      localStorage.setItem(
        "totalPrice",
        JSON.stringify(response.data.totalVATDetailsDTO.overAllTotalAmount)
      );
      localStorage.setItem("itemIds", JSON.stringify(bookingItems));

      setIsLoading(false);
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  // Get the cart items while guest login

  const getCartWithoutLogin = () => {
    let cartItems = localStorage.getItem("cartListDetails");
    if (cartItems !== null) {
      const parsedCartItems = JSON.parse(cartItems);
      setCartList(parsedCartItems);
      const totalPricesSum = parsedCartItems.reduce(
        (accumulator: any, item: any) => {
          return accumulator + item.totalPrice;
        },
        0
      );
      const totalPercent = parsedCartItems.reduce(
        (accumulator: any, item: any) => {
          return accumulator + item.product?.taxType?.taxPercentage;
        },
        0
      );
      setTotal(totalPricesSum);
      CalculateTax(totalPricesSum, totalPercent);
      const quantity = parsedCartItems.map((item: any) => item.quantity);
      setCount(quantity);
    }
  };

  // Calculate the tax amount from total price

  const CalculateTax = (totalPrice: any, taxaPercentage: any) => {
    const includeVATPercentage = (taxaPercentage + 100) / 100;
    const totalAmountWithoutIncludeVAT = totalPrice / includeVATPercentage;
    const roundedTotalAmountWithoutIncludeVAT =
      Math.round(totalAmountWithoutIncludeVAT * 100) / 100;
    const includeVATAmount =
      Math.round((totalPrice - totalAmountWithoutIncludeVAT) * 100) / 100;
    setSubTotal(roundedTotalAmountWithoutIncludeVAT);
    setTax(includeVATAmount);
  };

  useEffect(() => {
    // Get the cart details based on the customer signIn or guest user signIn
    if (userInfo.id) {
      getCartDetails();
    } else {
      getCartWithoutLogin();
    }
  }, []);

  // remove the All products from cart

  const removeAll = () => {
    localStorage.removeItem("cartListDetails");
    localStorage.removeItem("cartSupplier");
    setCartList([]);
    dispatch(addProductToCart());
  };

  // Remove the product from the cart list

  const removeProduct = (cart: any) => {
    if (userInfo.id) {
      localStorage.removeItem("cartListDetails");
      localStorage.removeItem("cartSupplier");
      // const removeItem = cartList.filter((cartValue:any) => cartValue.product.id != cart.product.id)
      deletecartproduct(cart);
      setCartList(
        cartList.filter(
          (cartValue: any) => cartValue.product.id != cart.product.id
        )
      );
    } else {
      const cartList: any = localStorage.getItem("cartListDetails");
      const cartItems = JSON.parse(cartList);
      const removeItem = cartItems.filter(
        (cartValue: any) => cartValue.product.id != cart.product.id
      );
      localStorage.setItem("cartListDetails", JSON.stringify(removeItem));
      getCartWithoutLogin();
      setCartList(
        cartItems.filter(
          (cartValue: any) => cartValue.product.id != cart.product.id
        )
      );
    }
  };

  // Increament and Decrement the cart product quantity

  const increment = (cart: any, indexValue: any) => {
    if (!userInfo.id) {
      let cartItems = localStorage.getItem("cartListDetails");
      if (cartItems !== null) {
        const parsedCartItems = JSON.parse(cartItems);
        parsedCartItems.map((parsedCartItems: any, index: any) => {
          if (parsedCartItems.quantity < parsedCartItems.product.quantity) {
            if (indexValue === index) {
              parsedCartItems.quantity += 1;
              parsedCartItems.totalPrice =
                parsedCartItems.quantity * parsedCartItems.price;
              const updatedCount = [...count];
              updatedCount[index] = parsedCartItems.quantity;
              setCount(updatedCount);
            }
          }
        });
        setCartList(parsedCartItems);
        localStorage.setItem(
          "cartListDetails",
          JSON.stringify(parsedCartItems)
        );
        getCartWithoutLogin();
        dispatch(addProductToCart());
      }
    } else {
      handlecartupdate("inc", cart);
    }
  };

  const decrement = (cart: any, indexValue: any) => {
    if (!userInfo.id) {
      let cartItems = localStorage.getItem("cartListDetails");
      if (cartItems !== null) {
        const parsedCartItems = JSON.parse(cartItems);
        parsedCartItems.map((parsedCartItems: any, index: any) => {
          if (parsedCartItems.quantity > 1) {
            if (indexValue === index) {
              parsedCartItems.quantity -= 1;
              parsedCartItems.totalPrice =
                parsedCartItems.quantity * parsedCartItems.price;
              const updatedCount = [...count];
              updatedCount[index] = parsedCartItems.quantity;
              setCount(updatedCount);
            }
          }
        });
        localStorage.setItem(
          "cartListDetails",
          JSON.stringify(parsedCartItems)
        );
        getCartWithoutLogin();
        dispatch(addProductToCart());
      }
    } else {
      handlecartupdate("dec", cart);
    }
  };

  // Update cart items

  const handlecartupdate = async (update: string, cart: any) => {
    let orderCount;
    const reqBody = {
      appUserId: userInfo.id,
      orderCount: update === "inc" ? +1 : -1,
      productId: cart.product.id,
    };

    if (update === "dec") {
      orderCount = cart.quantity - 1;
    } else {
      orderCount = cart.quantity + 1;
    }
    if (orderCount >= 1) {
      try {
        const response = await commonAxios.put(
          "/cemo/bookingitems/update/product/cart",
          reqBody
        );
        getCartDetails();
        dispatch(getCartListCount(userInfo.id));
      } catch (error) {
      } finally {
      }
    }
  };

  // Delete the particular cart items

  const deletecartproduct = async (cart: any) => {
    try {
      setIsLoading(true);
      const response = await commonAxios.delete(
        `/cemo/bookingitems/delete/${userInfo.id}/cart/${cart.product.id}`
      );
      dispatch(getCartListCount(userInfo.id));
      getCartDetails();
    } catch (error) {
    } finally {
      setIsLoading(false);
      dispatch(getCartListCount(userInfo.id));
    }
  };

  // Clear the all items in the cart

  const handleClearAllCart = async () => {
    console.log("hello");
    try {
      if (userInfo.id) {
        setIsLoading(true);
        const response = await commonAxios.delete(
          `/cemo/bookingitems/delete/${userInfo.id}/clearcart`
        );
        console.log(response, "response");
        getCartDetails();
        dispatch(getCartListCount(userInfo.id));
      } else {
        removeAll();
        getCartWithoutLogin();
      }
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  // Checkout the product from cart

  const handleCheckout = () => {
    if (userInfo.id) {
      const invalidItems = [];
      for (const cartItem of cartList) {
        if (cartItem.quantity > cartItem.product.quantity) {
          invalidItems.push(cartItem);
        }
      }
      if (invalidItems.length === 0) {
        navigate(`/payment`, {
          state: cartList,
        });
      } else {
        let errorMessage = "Sorry!\n";
        for (const item of invalidItems) {
          errorMessage += `One or more products in your cart is "Out of stock". Please remove them and proceed with checkout.\n`;
        }
        alert(errorMessage);
      }
    } else {
      setBookingLogin(true);
      window.scrollTo(0, 0);
      let StateDate = locationData.state;
      localStorage.setItem(
        "redirectBackTo",
        JSON.stringify({
          pathname: window.location.pathname,
          search: window.location.search,
          state: StateDate,
        })
      );
    }
  };

  console.log("cartList", cartList)

  return (
    <>
      <div className="">
        {/* {isLoading ? <MinLoader /> : null} */}

        <section className="mt-10 lg:ml-10">
          {url ? (
            <BreadCrumb breadsList={breadsList2} />
          ) : (
            <BreadCrumb breadsList={breadsList} />
          )}
        </section>
        {cartList?.length > 0 ? (
          <div className="h-full mb-5 lg:px-10">
            <h1 className="lg:ml-20 sm:ml-20 ml-14 text-[30px] font-bold font-[sofiapro] text-secondary">
              Varukorg
            </h1>
            <div className="md:flex my-10">
              <div className="rounded-lg md:w-2/4 px-5 sm:px-32 md:px-20">
                {cartList?.map((cart: any, index: any) => {
                  return (
                    <div className="justify-between mb-6 rounded-lg bg-white p-6 sm:flex">
                      <img
                        src={cart.product.image?.split(",")?.at(0)}
                        alt="product-image"
                        className="w-full rounded-lg lg:[w-152px] sm:w-24 lg:[h-152px] sm:h-24"
                      />

                      <div className="sm:ml-10 sm:flex sm:w-full sm:justify-between">
                        <div className="mt-5 sm:mt-0">
                          <h2 className="text-lg font-bold text-secondary text-[40px]">
                            {cart.product.name}
                          </h2>
                          <p className="mt-1 text-xs text-secondary">
                            Moms : {cart.product?.taxType?.taxPercentage} %
                          </p>
                          <p className="text-sm mt-3 font-bold text-secondary">
                            {cart.product.price} SEK
                          </p>
                          <p
                            className={`text-sm ${cart.product.quantity >= cart.quantity
                              ? "text-secondary"
                              : "text-red-500"
                              }`}
                          >
                            {cart.product.quantity >= cart.quantity
                              ? "I lager"
                              : "Slut i lager"}
                          </p>
                          <div className="mt-4 flex">
                            <div className="flex items-center border border-gray rounded-full">
                              <span
                                className={`${count[index] > 1
                                  ? "cursor-pointer rounded-l-full py-1 px-3.5 duration-100 text-secondary"
                                  : "cursor-not-allowed rounded-l-full py-1 px-3.5 duration-100 text-secondary opacity-50"
                                  }`}
                                onClick={() => decrement(cart, index)}
                              >
                                <HiChevronLeft />
                              </span>
                              <input
                                className="h-8 w-8 bg-white text-center text-md outline-none"
                                type="number"
                                value={count[index]}
                                min="1"
                                max={cart.product.quantity} // Set the maximum quantity based on product's available quantity
                              />
                              <span
                                className={`${count[index] < cart.product.quantity
                                  ? "cursor-pointer rounded-r-full py-1 px-3 duration-100 text-secondary"
                                  : "cursor-not-allowed rounded-r-full py-1 px-3 duration-100 text-secondary opacity-50"
                                  }`}
                                onClick={() => {
                                  if (cart.quantity < cart.product.quantity) {
                                    increment(cart, index);
                                  }
                                }}
                              >
                                <HiChevronRight />
                              </span>
                            </div>

                            <Tooltip
                              className="bg-[#D6B27D]"
                              content="Ta bort produkten"
                              placement="right"
                            >
                              <div className="mt-2 ml-4">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  onClick={() => removeProduct(cart)}
                                  stroke-width="1.5"
                                  stroke="currentColor"
                                  className="h-5 w-5 cursor-pointer border border-gray-100 rounded-full"
                                >
                                  <path
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    d="M6 18L18 6M6 6l12 12"
                                  />
                                </svg>
                              </div>
                            </Tooltip>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>

              <div className="mt-6 h-full rounded-lg bg-white p-[12px] border border-gray-400 md:mt-0 md:w-2/4 mx-5 md:mr-20">
                <p className="font-bold text-secondary">Kort total</p>
                <div className="mb-2 flex justify-between mt-2">
                  <p className="text-gray-700">Delsumma</p>
                  <p className="text-gray-700">{subTotal} SEK</p>
                </div>
                <div className="flex justify-between">
                  <p className="text-gray-700">Moms</p>
                  <p className="text-gray-700">{tax} SEK</p>
                </div>
                <hr className="my-4" />
                <div className="flex justify-between">
                  <p className="text-lg font-bold text-secondary">Total</p>
                  <div className="">
                    <p className="mb-1 text-lg font-bold text-secondary">
                      {total} SEK
                    </p>
                  </div>
                </div>
                <div className="flex justify-center">
                  <button
                    className="mt-6 p-2 rounded-md bg-[#D6B27D] py-1.5 font-bold text-lg text-white px-6 w-auto uppercase"
                    onClick={handleCheckout}
                  >
                    Checka ut
                  </button>

                  <button
                    onClick={handleClearAllCart}
                    className="mt-6 p-2 rounded-md ml-4 py-1.5 font-bold text-lg text-golden px-6 w-auto uppercase"
                  >
                    Rensa varukorg
                  </button>
                </div>
              </div>
            </div>
            <div className="my-10 text-center">

            </div>
          </div>
        ) : (
          <>
            {!isLoading ? (
              <div className="flex justify-center items-center py-32 -mt-1 w-full h-full">
                <p className="text-secondary">Varukorgen är tom</p>
              </div>
            ) : (
              <div className="h-[40vh]">
                {/* <MinLoader /> */}
              </div>
            )}
          </>
        )}
      </div>
    </>
  );
};

export default Shopping;
