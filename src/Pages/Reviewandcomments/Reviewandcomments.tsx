import React, { useEffect, useState } from "react";
import SideNav from "../../Components/SideNav/SideNav";
import AdminHeader from "../../Components/AdminHeader/AdminHeader";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { commonAxios } from "../../Axios/service";
import Loader from "../../Components/Loader/Loader";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import deleteIcon from "../../assets/Delete.png";
import moment from "moment";
import {
  HiCheckCircle,
  HiOutlineCheckCircle,
  HiOutlineXCircle,
  HiXCircle,
} from "react-icons/hi";
import RatingBar from "../../Components/RatingBar/RatingBar";

export interface Message {
  id: string;
  customer: Customer;
  supplier: Supplier;
  event: Event | any;
  product: Product | any;
  comments: string;
  rating: number;
  isRead: boolean;
  status: string;
  createdDate: string;
  updatedDate: string;
}

export interface Customer {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
  profileImage: string;
}

export interface Supplier {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  businessCity: string;
  password: string;
  accountStatus: string;
  createdAt: string;
  updatedAt: string;
  categories: string;
  profileImage: any;
}

export interface Event {
  id: string;
  name: string;
  description: string;
  city: string;
  image: string;
  appUser: AppUser;
}

export interface Product {
  id: string;
  name: string;
  description: string;
  image: string;
  quantity: number;
  price: number;
  status: string;
  productNumber: string;
  appUser: AppUser;
  category: Category;
  taxType: TaxType;
}

export interface AppUser {
  id: string;
  firstName: string;
  lastName: string;
  mobile: string;
  telephone: string;
  email: string;
  businessName?: string;
  businessNumber?: string;
  businessAddress?: string;
  businessPinCode?: string;
  businessCity?: string;
  password: string;
  accountStatus: string;
  createdAt?: string;
  updatedAt: string;
  categories?: string;
  profileImage: string;
}

export interface Category {
  id: string;
  categoryName: string;
  categoryPrice: number;
  image?: string;
  createdAt: string;
  status: string;
}

export interface TaxType {
  id: string;
  taxPercentage: number;
}

export interface ProdOrEventList {
  id: string;
  supplierId: string;
  name: string;
  type: string;
}

const Reviewandcomments = () => {
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [comments, setcomments] = useState<Message[]>([]);
  const [productsAndEventsList, setProductsAndEventsList] = useState<
    ProdOrEventList[]
  >([]);
  const [isLoading, setIsLoading] = useState(false);
  const [activeMenu, setActiveMenu] = useState("");
  const [itemToReply, setItemToReply] = useState<Message>();
  const [commentModalOpen, setAddCommentModalOpen] = useState(false);

  const handleclick = async (item: ProdOrEventList) => {
    setActiveMenu(item.id);
    try {
      setIsLoading(true);
      const URL =
        item.type === "product"
          ? `/cemo/comments/get?supplierId=${item.supplierId}&productId=${item.id}`
          : `/cemo/comments/get?supplierId=${item.supplierId}&eventId=${item.id}`;
      const response = await commonAxios.get(URL);

      // if (response.data) {
      //   let updatedComments = response.data.filter((val: any) => {
      //     return val.isRead === false;
      //   });

      //   let updatedMessagesId = updatedComments.map((val: any) => {
      //     return val.id;
      //   });
      //   let payload = {
      //     id: updatedMessagesId,
      //     customerId: null,
      //     supplierId: userInfo.id,
      //   };
      //   let updatedMessageResponse = await commonAxios.put(
      //     `/cemo/comments/update/status`,
      //     payload
      //   );
      // }
      setcomments(response.data);
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  const getMessages = async () => {
    try {
      setIsLoading(true);
      const response = await commonAxios.get(
        `/cemo/comments/get?supplierId=${userInfo.id}`
      );

      const prodOrEventList: ProdOrEventList[] = [];
      const prodOrEventListId: string[] = [];
      response.data?.forEach((item: Message) => {
        if (item.event !== null) {
          if (!prodOrEventListId?.includes(item.event.id)) {
            prodOrEventListId.push(item.event.id);
            prodOrEventList.push({
              id: item.event.id,
              supplierId: item.supplier.id,
              name: item.event.name,
              type: "event",
            });
          }
        } else {
          if (!prodOrEventListId?.includes(item.product.id)) {
            prodOrEventListId.push(item.product.id);
            prodOrEventList.push({
              id: item.product.id,
              supplierId: item.supplier.id,
              name: item.product.name,
              type: "product",
            });
          }
        }
      });
      setProductsAndEventsList(prodOrEventList);
      setActiveMenu(prodOrEventList?.at(0)?.id as string);
      if (prodOrEventList?.at(0)) {
        handleclick(prodOrEventList?.at(0)!);
      }
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  const handlereply = () => {
    const activeItem = comments
      .filter((item: Message) => item.product.id === activeMenu)
      ?.at(0);
    setItemToReply(activeItem);
    setAddCommentModalOpen(true);
  };

  const AddCommentModal = () => {
    const [newComment, setNewComment] = useState("");
    const handleclose = () => {
      setAddCommentModalOpen(false);
    };

    const handlenewcomment = (e: any) => {
      setNewComment(e.target.value);
    };

    const addComment = async () => {
      setIsLoading(true);
      try {
        const payload = {
          customerId: null,
          supplierId: userInfo.id, //Not null
          eventId: null, //It must be null if productId have a value
          productId: itemToReply?.product.id, //It must be null if eventId have a value
          comments: newComment, //Not null
          rating: 2, //It can be null and value must be in 1 to 5
        };
        const response = await commonAxios.post("/cemo/comments/add", payload);
        if (response.data) {
          const activeItem = productsAndEventsList.filter(
            (item: ProdOrEventList) => item.id === itemToReply?.product.id
          );
          handleclick(activeItem.at(0)!);
          handleclose();
        }
      } catch (error) {
        failureNotify("oops! something went wrong");
      } finally {
        setIsLoading(false);
      }
    };

    return (
      <div className="w-[85%] h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <div className="bg-white rounded-lg p-6 flex flex-col justify-center items-center w-1/2 h-72 relative">
            <button onClick={handleclose}>
              <img
                src={deleteIcon}
                className="absolute top-3 right-5 h-6 w-6"
              />
            </button>
            <div className="w-full">
              <textarea
                placeholder="Dina kommentarer..."
                value={newComment}
                onChange={handlenewcomment}
                className="w-full border border-[#e3e3e3] outline-none p-4 rounded-md"
                name="comment"
                id="comment"
                rows={4}
              />
            </div>
            <button
              onClick={addComment}
              className="px-8 py-2 mt-4 rounded-lg bg-[#D6B27D] text-white font-semibold text-base cursor-pointer uppercase"
            >
              Post
            </button>
          </div>
        </section>
      </div>
    );
  };

  useEffect(() => {
    getMessages();
  }, []);

  console.log(comments,"comments?.rating?.ratings")
  const toggleMesageStatus = async (comment: any, status: boolean) => {
    try {
      setIsLoading(true);
      if (status) {
        comment.status = "ACCEPTED";
      } else {
        comment.status = "REJECTED";
      }
      await commonAxios
        .put(`/cemo/comments/update/${comment.id}`, comment)
        .then(
          (response) => {
            successNotify("Comment updated successfully");
          },
          (error) => {
            failureNotify("Comment Update failed.");
          }
        );
    } catch (error) {
    } finally {
      setIsLoading(false);
      getMessages();
    }
  };

  return (
    <div className="flex">
      <SideNav />
      <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
        {isLoading ? <Loader /> : null}
        {commentModalOpen ? <AddCommentModal /> : null}
        <AdminHeader />
        <div className="mx-10 my-6">
          <div className="flex flex-row justify-start items-center">
            <h1 className="text-secondary text-[20px] font-semibold">
              Omdömen och kommentarer
            </h1>
            {/* <button
                onClick={() => handlereply()}
                className="px-4 py-2 mb-2 ml-auto md:mr-4 bg-[#D6B27D] rounded-xl text-white text-lg font-semibold cursor-pointer outline-none "
              >
                ADD COMMENTS
              </button> */}
          </div>
          <div className="bg-[#fff] p-4 m-6 h-[80vh] max-h-[80vh] flex flex-row">
            <section className="w-1/3 py-4 px-10 h-full border-[#e3e3e3] border rounded-[10px] overflow-y-scroll">
              <h2 className="font-bold font-[sofiapro] my-6 text-2xl text-secondary">
                  Produkter and Event
              </h2>
              {productsAndEventsList.length === 0 ? (
                <div className="flex justify-center items-center">
                  <p className="font-bold text-lg text-[#D6B27D]">
                  Inga produkter eller event att visa
                  </p>
                </div>
              ) : (
                productsAndEventsList?.map((item: ProdOrEventList) => (
                  <p
                    className={`cursor-pointer font-bold px-6 py-3 rounded-md transition-all my-2 text-secondary ${activeMenu === item.id
                      ? "bg-[#D6B27D] text-white"
                      : "bg-transparent text-[#171725]"
                      }`}
                    onClick={() => handleclick(item)}
                  >
                    {item.name}
                  </p>
                ))
              )}
            </section>
            <section className="w-2/3 h-full">
              <div className="w-4/5 h-full overflow-y-scroll flex flex-col mx-auto border border-[#e3e3e3] rounded-[10px]">
                {comments.length === 0 ? (
                  <div className="flex justify-center items-center h-full">
                    <p className="font-bold text-3xl text-[#d69a40]">
                    Inga kommentarer att visa
                    </p>
                  </div>
                ) : (
                  comments.map((comment: any) => (
                    <section className="w-full px-10 my-3 border-b-[1px]">
                      <div className="mb-4 flex flex-row items-center justify-between">
                        <div className="flex flex-row items-center text-secondary">
                          {comment.customer != null ? (
                            <>
                             { comment.customer.profileImage != null ? 
                            
                            <img
                              className="w-10 rounded-full"
                              src={comment.customer.profileImage}
                              alt={comment.customer.firstName}
                            /> : <span className="h-10 w-10 mb-2 ml-2 flex justify-center items-center text-center rounded-full font-semibold bg-[#D6B27D] text-white">
                            {" "}
                            {comment.customer.firstName.split("").at(0)?.toUpperCase() +
                              "" +
                            comment.customer.lastName.split("").at(0)?.toUpperCase()}
                          </span> }
                            </>
                          ) : (
                            <img
                              className="w-10 rounded-full"
                              src={
                                comment.supplier.profileImage ??
                                "https://ocm-images.fra1.cdn.digitaloceanspaces.com/assets/supplier.png"
                              }
                              alt={comment.supplier.firstName ?? "alt"}
                            />
                          )}
                          {comment.customer != null ? (
                            <div className="flex flex-col">
                            <span className="mx-4 text-lg font-medium">
                              {comment.customer.firstName}
                            </span>
                            <div className="mx-4">
                            <RatingBar
                                ratings={comment.rating?.ratings}
                                readonly={true}
                              />
                              </div>
                            </div>
                          ) : (
                            <span className="mx-4 text-lg font-medium">
                              {comment.supplier.firstName}
                            </span>
                          )}
                        </div>
                        <div className="text-[#d69a40]">
                          <p className="text-xs">
                            {moment(comment.createdDate).format(
                              "ddd, MMM DD YYYY"
                            )}
                          </p>
                        </div>
                      </div>
                      <div className="flex flex-row">
                        <p className="ms-4 text-secondary mt-2">{comment.rating?.descriptions}</p>
                        <div className="flex flex-row  mx-4 mb-2 justify-center items-center">
                          {comment.status === "ACCEPTED" ||
                            (comment.status !== "PENDING" &&
                              comment.status !== "REJECTED") ? (
                            <HiCheckCircle className="h-[36px] w-[36px] text-green-200" />
                          ) : (
                            <HiOutlineCheckCircle
                              className="h-[36px] w-[36px] text-green-400 cursor-pointer"
                              onClick={() => toggleMesageStatus(comment, true)}
                            />
                          )}
                          {comment.status === "REJECTED" ||
                            (comment.status !== "PENDING" &&
                              comment.status !== "ACCEPTED") ? (
                            <HiXCircle className="mx-4 h-[36px] w-[36px] text-red-200" />
                          ) : (
                            <HiOutlineXCircle
                              className="mx-4 h-[36px] w-[36px] text-red-400 cursor-pointer"
                              onClick={() => toggleMesageStatus(comment, false)}
                            />
                          )}
                        </div>
                      </div>

                    </section>
                  ))
                )}
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Reviewandcomments;
