/* global google */
import React, { useEffect, useState } from "react";
import {
  createSearchParams,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import { commonAxios } from "../../Axios/service";
import Loader from "../../Components/Loader/Loader";
import BreadCrumb from "../../Components/BreadCrumb/BreadCrumb";
import MapView from "../../Components/Mapview/MapView";
import RatingBar from "../../Components/RatingBar/RatingBar";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { AiFillHeart } from "react-icons/ai";
import { IoIosHeartEmpty } from "react-icons/io";
import { Tooltip } from "@material-tailwind/react";

const DetailsPage = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [isFavourite, setIsFavourite] = useState<boolean>(false);
  const [eventData, setEventData] = useState<any>({});
  const [isLoading, setIsLoading] = useState(false);
  const [defaultRatings, setDefaultRatings] = useState<any>()
  const [ratingCount, setRatingCount] = useState('')
  const [latLong, setLatLong] = useState({
    lat: 0.0,
    lng: 0.0,
  })

  const popularParams = {
    searchTitle: "",
    searchText: searchParams.get("searchText") as string,
  };

  const breadsList = [
    {
      name: "Home",
      naviagteTo: "/",
    },
    {
      name: "Popular Activities",
      naviagteTo: popularParams.searchText
        ? `/popular?${createSearchParams(popularParams)}`
        : `/popular?searchTitle=&searchText=${eventData?.appUser?.businessCity}`,
    },
    {
      name: "Details",
    },
  ];

  // function geocodeCity(cityName: string) {
  //   console.log('cityName',cityName)
  //   const geocoder = new window.google.maps.Geocoder()

  //   geocoder.geocode({ address: cityName }, (results, status) => {
  //     if (status === window.google.maps.GeocoderStatus.OK) {
  //       const { lat, lng } = results![0].geometry.location;
  // setLatLong({
  //   lat: lat(),
  //   lng: lng()
  // })
  //       console.log(`Latitude: ${lat()}`);
  //       console.log(`Longitude: ${lng()}`);
  //     } else {
  //       console.error('Geocode was not successful for the following reason:', status);
  //     }
  //   });
  // }

  // Make a request to the Geocoding API
  function getCoordinates(cityName: string) {
    const apiKey = 'AIzaSyDYsIF0ap_NrOL3xyjf1yB-3IkhN0O4lSI';
    const apiUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(cityName)}&key=${apiKey}`;

    // Send an HTTP GET request
    fetch(apiUrl)
      .then(response => response.json())
      .then(data => {
        if (data.status === 'OK' && data.results.length > 0) {
          const location = data.results[0].geometry.location;
          const latitude = location.lat;
          const longitude = location.lng;
          setLatLong({
            lat: latitude,
            lng: longitude
          })
          console.log(`Latitude: ${latitude}, Longitude: ${longitude}`);
        } else {
          console.log('Unable to geocode the city.');
        }
      })
      .catch(error => {
        console.log('An error occurred while geocoding:', error);
      });
  }

  const getEventDetails = async () => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get(
        `/cemo/events/${searchParams.get("id")}`
      );
      if (response.data) {
        setEventData(response.data);
        getCoordinates(response.data.city)
      }
    } catch (error) {
      console.log("getEventDetails", error);
    } finally {
      setIsLoading(false);
    }
  };

  const togglefavourite = async (id: any, eventData: any) => {
    try {
      setIsLoading(true);
      const payload = {
        appUserId: userInfo.id,
        productId: null,
        eventId: id,
      };
      await commonAxios.post("/cemo/favourite-items", payload).then(
        (response) => {
          setIsFavourite(!response.data.deleted);
        },
        (error) => { }
      );
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    getEventDetails();
  }, []);


  useEffect(() => {
    getReviews()
  }, [])

  const getReviews = async () => {
    let response;

    response = await commonAxios.get(
      `cemo/ratings/average/rating?productId=${searchParams.get(
        "id"
      )}`
    );
    console.log(response, 'whatresponseget')
    setDefaultRatings(response.data);
    setRatingCount(response.data)

  }

  useEffect(() => {
    if (userInfo.id) {
      getFavouriteItems();

    }
  }, [isFavourite]);

  const getFavouriteItems = async () => {
    try {
      const response = await commonAxios.get(
        `cemo/favourite-items/favourite/${userInfo.id}/${searchParams.get(
          "id"
        )}`
      );
      setIsFavourite(response.data.isFavourite);
    } catch (e) {
    } finally {
    }
  };

  return (
    <>
      {isLoading && <Loader />}
      {eventData && (
        <div className="mt-10 text-secondary">
          <BreadCrumb breadsList={breadsList} />
          <div className="px-4 md:px-14 md:ml-6">
            <h1 className="uppercase italic font-bold md:text-[32px] font-[sofiapro] ">
              {eventData?.name}
            </h1>
            <h1 className="text-[#D6B37E] md:text-[14px] font-semibold italic uppercase px-10 -ml-11">
              -{eventData?.category?.categoryName}
            </h1>
            <div>
              <div className="flex">
                <RatingBar ratings={defaultRatings} readonly={true} />
                <p className="ml-3 mt-1 text-secondary text-sm">({ratingCount})</p>
                {/* <Tooltip className='bg-[#D6B27D]' content={isFavourite ? "Remove from wishlist" : 'Add to wishlist'} placement="right-start"> */}
                <div
                  onClick={() => togglefavourite(searchParams.get("id"), eventData)}
                  className="cursor-pointer ml-3"
                >
                  {userInfo.id ? (
                    isFavourite ? (
                      <div className="bg-white rounded-full p-2 hover:bg-gray-300 shadow-md">
                        <AiFillHeart className="h-6 w-6 text-[#D6B27D] z-40 " />
                      </div>
                    ) : (
                      <div className="bg-[#D6B27D] rounded-full p-2 hover:bg-orange-200 shadow-md">
                        <IoIosHeartEmpty className="h-6 w-6 text-white z-40" />
                      </div>
                    )
                  ) : null}
                </div>
                {/* </Tooltip> */}
              </div>

            </div>
          </div>

          <div
            className="rounded-3xl mx-4 bg-cover bg-no-repeat bg-center md:mx-20 h-[50vh]"
            style={{ backgroundImage: `url(${eventData.image})` }}
          />
          <div className="grid grid-cols-1 lg:grid-cols-2 px-5 md:px-14 md:py-1 mt-1">
            <div className="sm:mx-5">
              <h1 className="text-[#D6B27D] italic text-[16px] font-semibold mt-10 mx-1">
                - How it has Started
              </h1>
              <h1 className="md:text-[28px] font-bold uppercase font-[sofiapro]">
                {eventData?.name}
              </h1>
              <img src={eventData.image} className="hidden md:flex mt-10 rounded-2xl " />
            </div>
            <div className="sm:mx-1">
              {/* <h1 className="uppercase text-[24px] text-[#D6B27D] md:mt-20 font-bold">
                  {eventData?.name}
                </h1> */}
              <p className=" md:text-[24px] text-[18px] md:mt-36 mt-4 text-secondary">
                {eventData?.description}
              </p>
              <div
                onClick={() => {
                  const params = {
                    id: searchParams.get("id") as string,
                    searchText: searchParams.get("searchText") as string,
                  };
                  navigate({
                    pathname: "/booking",
                    search: `?${createSearchParams(params)}`,
                  });
                }}
                className="flex justify-start mt-2"
              >
                <button className="rounded-xl text-white md:text-[14px] shadow-lg bg-[#D6B27D] md:text-lg md:font-semibold mb-10 h-[50px] mt-8 px-10 py-1 uppercase">
                  Besök
                </button>
              </div>
            </div>
          </div>
          {/* <div className="grid grid-cols-1 mt-3 lg:grid-cols-2 px-10 md:px-14">
                    <div className="lg:mx-44 md:mx-28 sm:mx-16">
                        <h1 className='uppercase text-[24px] text-[#D6B27D] md:mt-20 font-bold'>vad gor man en kall vinterag i orebro</h1>
                        <p className='text-[#1A202C] text-[24px] mt-3'>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Minus amet nihil, fuga explicabo distinctio officia non facilis dolorem incidunt inventore aliquid, suscipit quod commodi doloremque iure, omnis eaque tempore libero?</p>
                    </div>
                    <div className="lg:mx-44 md:mx-28 sm:mx-16">
                        <img src={eventData.image} className='rounded-2xl' />
                    </div>
                </div>
                <div className="grid grid-cols-1 mt-3 lg:grid-cols-2 px-10 md:px-14">
                    <div className="lg:mx-44 md:mx-28 sm:mx-16">
                        <img src={eventData.image} className='rounded-2xl' />
                    </div>
                    <div className="lg:mx-44 md:mx-28 sm:mx-16">
                        <h1 className='uppercase text-[24px] text-[#D6B27D] md:mt-20 font-bold'>vad gor man en kall vinterag i orebro</h1>
                        <p className='text-[#1A202C] text-[24px] mt-3'>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Minus amet nihil, fuga explicabo distinctio officia non facilis dolorem incidunt inventore aliquid, suscipit quod commodi doloremque iure, omnis eaque tempore libero?</p>
                    </div>
                </div> */}
          <div className=" flex md:flex-row flex-col md:justify-center md:items-center md:px-10 px-4 gap-1">
            <div className="order-2 md:order-1 md:w-1/2 w-full h-full md:mx-10">
              {latLong.lat !== 0.0 && latLong.lng !== 0.0 ? (<MapView
                latitude={latLong.lat}
                longitude={latLong.lng}
                address={eventData?.city}
              />) : null}
            </div>

            <div className="order-1 md:order-2 md:w-1/2 w-full mx-auto">
              <section className="flex flex-col items-start bg-[#FFFF] rounded-[14px] md:h-[375px] p-10">
                <p className="font-medium italic text-[#D6B27D]">
                  -Vår information
                </p>
                <h4 className=" md:text-[24px] font-medium mt-3 text-secondary">
                  Address: {`${eventData?.city}, ${eventData?.pincode}`}
                </h4>
                {/* <h4 className='text-black md:text-[24px] font-medium my-5'>{`Öpening hours: ${eventData.operatingHours?.at(0)?.startTime} - ${eventData.operatingHours?.at(0)?.endTime}`}</h4> */}
                {/* <h4 className='text-black md:text-[24px] font-medium mt-3'>Telefon: {eventData?.appUser?.mobile}</h4>
                        <h4 className='text-black md:text-[24px] font-medium mt-3'>Email: {eventData?.appUser?.email}</h4> */}
              </section>
            </div>
          </div>

          {/* <div
              onClick={() => {
                const params = { id: searchParams.get("id") as string };
                navigate({
                  pathname: "/booking",
                  search: `?${createSearchParams(params)}`,
                });
              }}
              className="flex justify-center mt-8"
            >
              <button className="rounded-xl text-white capitalize flex items-center md:text-[14px] px-10 py-3 shadow-lg bg-[#D6B27D] md:text-lg  md:font-semibold mb-10">
                Besök
              </button>
            </div> */}
        </div>
      )}
    </>
  );
};

export default DetailsPage;
