import React, { useState, useEffect, ChangeEvent, FormEvent } from 'react'
import { useSearchParams } from 'react-router-dom';
import mainLogo from "../../assets/blixa-black.png";
import Loader from '../../Components/Loader/Loader';
import { useNavigate } from "react-router-dom";
import { commonAxios } from '../../Axios/service';
import { failureNotify, successNotify } from '../../Components/Toast/Toast';
import eyeSlashIcon from "../../assets/icons8-eye-24.png";
import eyeIcon from "../../assets/icons8-eye-24.png";
import { BsEye, BsEyeSlash } from "react-icons/bs";

const ResetPassword = () => {

  const [searchParams] = useSearchParams();
  const [token, setToken] = useState<string>('');
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [newPassword, setNewPassword] = useState<string>('');
  const [confirmPassword, setConfirmPassword] = useState<string>('');
  const [isValid, setIsValid] = useState<boolean>(true);
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    if (searchParams.get('token')) {
      setToken((searchParams.get('token') as string).replaceAll(" ", "+"));
    }
  }, [])

  const handleNewPasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
    setIsValid(true);
    setNewPassword(event.target.value);
  };

  const handleConfirmPasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
    setIsValid(true);
    setConfirmPassword(event.target.value);

  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    if (newPassword.length > 5 && confirmPassword.length > 5 && newPassword === confirmPassword) {
      if (newPassword.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/)) {
        setIsValid(true);
        resetPassword();
      } else {
        setIsValid(false);
      }
    } else {
      setIsValid(false);
    }
  };

  const resetPassword = async () => {
    setIsLoading(true);
    try {
      const eventBody: any = await {
        token: token,
        newPassword: newPassword,
        confirmPassword: confirmPassword
      };
      const response = await commonAxios.put("/cemo/user/security", eventBody);
      setIsLoading(false)
      if (response.data != null && response.data.errorErrorMessage == null) {
        successNotify(response.data);
        navigate("/login")
      } else {
        failureNotify(response.data.errorErrorMessage);
      }
    } catch (error) {
      setIsLoading(false);
      failureNotify("Error occurred...");
    }
  }
  const toggleNewPasswordVisibility = () => {
    setShowNewPassword(!showNewPassword);
  };

  const toggleConfirmPasswordVisibility = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  return (
    <div className='h-[100vh] flex flex-col justify-center items-center bg -mt-14'>
      {isLoading ? <Loader /> : null}
      <img
        src={mainLogo}
        className="h-24"
        onClick={() => navigate("/")}
      />
      <div className='block h-auto w-auto bg-[#FFE5D8] px-10 py-10 m-5 rounded-3xl'>
        <p className='font-semibold text-[24px] text-secondary w-full flex flex-row justify-center mb-10'>Återställ lösenord</p>
        <form onSubmit={handleSubmit} className="max-w-[300px] mx-auto">
          <h1 className="font-bold text-secondary">Nytt lösenord:</h1>
          <div className="relative">
            <div className="flex">
              <input
                type={showNewPassword ? "text" : "password"}
                placeholder="Nytt lösenord"
                value={newPassword}
                onChange={handleNewPasswordChange}
                autoComplete="off"
                className="appearance-none border w-full mt-1 mb-5 text-gray-700 leading-tight focus:outline-none focus:shadow-outline px-4
            py-2
           rounded-[10px]"
              />
              <div
                className="absolute inset-y-0 right-0 flex items-center pr-3 -mt-4 cursor-pointer"
                onClick={toggleNewPasswordVisibility}
              >
                {showNewPassword ? <BsEye /> : <BsEyeSlash />}
              </div>
            </div>
          </div>
          <h1 className="font-bold text-secondary">Bekräfta lösenord:</h1>
          <div className="relative">
            <div className="flex">
              <input
                type={showConfirmPassword ? "text" : "password"}
                placeholder="Bekräfta lösenord"
                value={confirmPassword}
                onChange={handleConfirmPasswordChange}
                autoComplete="off"
                className="appearance-none border w-full mt-1 mb-5 text-gray-700 leading-tight focus:outline-none focus:shadow-outline px-4
            py-2
           rounded-[10px]"
              />

              <div
                className="absolute inset-y-0 right-0 flex items-center pr-3 -mt-4 cursor-pointer"
                onClick={toggleConfirmPasswordVisibility}
              >
                {showConfirmPassword ? <BsEye /> : <BsEyeSlash />}
              </div>
            </div>
          </div>
          {!isValid && (
            <p className="text-red-500 -mt-4 mb-4">
              {
                newPassword.length > 5 || confirmPassword.length > 5
                  ? !newPassword.match(/^(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9!@#$%^&*()-_+=]+$/)
                    ? "Password must contain at least one uppercase, one lowercase, and one numeric character" : "Password doesn't match"
                  : "Need minimum six characters"
              }
            </p>
          )}
          <div className='flex flex-row justify-center mt-1'>
            <button
              type="submit"
              className="bg-[#D6B37D] text-[15px] sm:text-[18px] text-white font-bold py-2 px-8 sm:py-2 w-auto sm:px-4 rounded-xl uppercase"
            >
              Lägg till
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default ResetPassword