import React, { ChangeEvent, FormEvent, useState } from "react";
import Loader from "../../Components/Loader/Loader";
import SideNav from "../../Components/SideNav/SideNav";
import AdminHeader from "../../Components/AdminHeader/AdminHeader";
import { commonAxios } from "../../Axios/service";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { BsEye, BsEyeSlash } from "react-icons/bs";

const ProfilePasswordReset = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [currentPassword, setCurrentPassword] = useState<string>("");
  const [newPassword, setNewPassword] = useState<string>("");
  const [confirmPassword, setConfirmPassword] = useState<string>("");
  const [isValid, setIsValid] = useState<boolean>(true);
  const [errorMessage, setErrorMessage] = useState<string>("");
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const userInfo = useSelector((state: RootState) => state.user.userDetails);

  const handleCurrentPasswordChange = (
    event: ChangeEvent<HTMLInputElement>
  ) => {
    setIsValid(true);
    setCurrentPassword(event.target.value);
  };

  const handleNewPasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
    setIsValid(true);
    setNewPassword(event.target.value);
  };

  const handleConfirmPasswordChange = (
    event: ChangeEvent<HTMLInputElement>
  ) => {
    setIsValid(true);
    setConfirmPassword(event.target.value);
  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    getErrorMessage();
    if (
      newPassword.length > 5 &&
      confirmPassword.length > 5 &&
      newPassword === confirmPassword &&
      currentPassword &&
      currentPassword !== newPassword
    ) {
      if (
        newPassword.match(
          /^(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9!@#$%^&*()-_+=]+$/,
        )
      ) {
        setIsValid(true);
        resetPassword();
      } else {
        setIsValid(false);
      }
    } else {
      setIsValid(false);
    }
  };

  const getErrorMessage = () => {
    if (
      currentPassword.length === 0 ||
      newPassword.length === 0 ||
      confirmPassword.length === 0
    ) {
      setErrorMessage("All fields are Obligatorisk");
    } else if (newPassword.length < 6 && confirmPassword.length < 6) {
      setErrorMessage("Minimum length of the password is six characters");
    } else if (
      !newPassword.match(
        /^(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9!@#$%^&*()-_+=]+$/,
      )
    ) {
      setErrorMessage(
        "Password must contain at least one uppercase, one lowercase, and one numeric character"
      );
    } else if (currentPassword === newPassword) {
      setErrorMessage("New password must not to be same as current password");
    } else if (newPassword !== confirmPassword) {
      setErrorMessage("New password and confirm password must be same");
    } else {
      setErrorMessage("");
    }
  };

  const resetPassword = async () => {
    setIsLoading(true);
    try {
      const eventBody: any = await {
        appUserId: userInfo.id,
        currentPassword: currentPassword,
        newPassword: newPassword,
        confirmPassword: confirmPassword,
      };
      const response = await commonAxios.post("/cemo/user/security", eventBody);
      if (response.data != null && response.data.errorErrorMessage == null) {
        successNotify("Lösenordet byttes utan problem");
      } else {
        failureNotify(response.data.errorErrorMessage);
      }
    } catch (error) {
      failureNotify("Error occurred...");
    } finally {
      setIsLoading(false);
    }
  };

  const toggleNewPasswordVisibility = () => {
    setShowNewPassword(!showNewPassword);
  };

  const toggleConfirmPasswordVisibility = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  return (
    <>
      <div className="flex">
        {isLoading ? <Loader /> : null}
        <SideNav />
        <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
          <AdminHeader />
          <div className="block border-[2px] px-10 py-20 mx-32 rounded-3xl mt-8 mb-20">
            <p className="font-semibold text-[24px] flex flex-row justify-center text-secondary mb-10">
              Återställ lösenord
            </p>
            <form onSubmit={handleSubmit} className="max-w-[300px] mx-auto">
              <h1 className="font-semibold text-secondary">Nuvarande lösenord:</h1>
              <input
                type="text"
                placeholder="Nuvarande lösenord"
                value={currentPassword}
                onChange={handleCurrentPasswordChange}
                autoComplete="off"
                className="appearance-none border w-full  mt-1 mb-5 text-gray-600 leading-tight focus:outline-none focus:shadow-outline  px-4
                py-2
               rounded-[10px]"
              />

              <h1 className="font-semibold text-secondary">Nytt lösenord:</h1>
              <div className="relative">
                <div className="flex">
                  <input
                    type={showNewPassword ? "text" : "password"}
                    placeholder="Nytt lösenord"
                    value={newPassword}
                    onChange={handleNewPasswordChange}
                    autoComplete="off"
                    className="appearance-none border w-full mt-1 text-gray-600 leading-tight focus:outline-none focus:shadow-outline px-4
                    py-2
                   rounded-[10px]"
                  />
                  <div
                    className="absolute inset-y-0 right-0 flex items-center pr-3 mt-1 cursor-pointer"
                    onClick={toggleNewPasswordVisibility}
                  >
                    {showNewPassword ? <BsEye /> : <BsEyeSlash />}
                  </div>
                </div>
              </div>

              <h1 className="font-semibold text-secondary mt-5">Bekräfta lösenord:</h1>
              <div className="relative">
                <div className="flex">
                  <input
                    type={showConfirmPassword ? "text" : "password"}
                    placeholder="Bekräfta lösenord"
                    value={confirmPassword}
                    onChange={handleConfirmPasswordChange}
                    autoComplete="off"
                    className="appearance-none border w-full mt-1 text-gray-600 leading-tight focus:outline-none focus:shadow-outline px-4
                    py-2
                   rounded-[10px]"
                  />
                  <div
                    className="absolute inset-y-0 right-0 flex items-center pr-3 mt-1 cursor-pointer"
                    onClick={toggleConfirmPasswordVisibility}
                  >
                    {showConfirmPassword ? <BsEye /> : <BsEyeSlash />}
                  </div>
                </div>
              </div>

              {!isValid && (
                <p className="text-red-500 ml-3 mb-8">{errorMessage}</p>
              )}
              <div className="flex flex-row justify-center mt-5">
                <button
                  type="submit"
                  className="bg-[#D6B37D] text-[15px] sm:text-[18px] w-auto text-white font-bold py-1 px-6 sm:py-2 sm:px-6 rounded-md mt-5 uppercase"
                >
                  Lägg till
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProfilePasswordReset;
