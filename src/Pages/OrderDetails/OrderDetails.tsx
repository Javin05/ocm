import React from "react";
import { useLocation } from "react-router-dom";

const OrderDetails = () => {
  const location = useLocation();
  const state = location.state;
   // Split the image URLs into an array
  const imageUrls = state.image.split(',');
  const firstImageUrl = imageUrls.length > 0 ? imageUrls[0].trim() : null;

  return (
    <>
      <div className="grid grid-cols-2 p-10 ">
        <div className="p-5">
          <img
            src={firstImageUrl}
            className="rounded-lg object-cover max-h-[400px] w-full"
          />
        </div>
        <div className="p-5">
          <div>
            <h1 className="font-bold text-gray-600 text-[30px]">{state.companyName}</h1>
            <p className="font-bold text-gray-600 ">
            Ordernummer  #{" "}
              <span className="font-bold">{state.orderNumber}</span>
            </p>
          </div>
          <div>
            <p className="text-gray-600">{state.orderDetails.length} Items</p>
          </div>
          <section className="pb-6 mb-8 border-dotted border-b-2 border-[#e3e3e3] text-gray-600">
            {state?.orderDetails.map((item: any) => (
              <div className="flex flex-row justify-between items-center w-full mb-2">
                <span className="font-semibold">
                  {item.itemName} {item.quantity}
                </span>
                <span className="text-gray-600 text-lg font-medium">
                  moms/item -{" "}
                  {item.itemCategory === "EVENT"
                    ? 25
                    : item.momsPercentagePerItem}{" "}
                  %
                </span>
                <span className="text-gray-600 text-lg font-medium">
                  {item.price} SEK
                </span>
              </div>
            ))}
          </section>
          <section className="pb-8 mb-6 border-b-[1px] border-black">
            <div className="flex flex-row justify-between items-center w-full mb-2">
              <span className="font-bold text-gray-600">Item Total</span>
              <span className="text-gray-600 text-lg font-bold">
              {state?.totalAmount} SEK
                
              </span>
            </div>
            <div className="flex flex-row justify-between items-center w-full mb-2">
              <span className="text-gray-600 font-bold">MOMS</span>
              <span className="text-gray-600 text-lg font-bold">
                {state?.taxes} SEK
              </span>
            </div>
          </section>
          <div className="flex flex-row justify-between items-center w-full mb-2">
            <span className="text-gray-600 font-bold">BILL TOTAL</span>
            <span className="text-gray-600 font-bold">
              {state?.totalItemPrice} SEK
            </span>
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderDetails;
