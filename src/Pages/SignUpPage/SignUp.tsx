import Footer from "../../Components/Footer/Footer";
import Heading from "../../assets/blixa-black.png";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { commonAxios, formAxios } from "../../Axios/service";
import { useCallback, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { setUserDetails } from "../../Redux/features/userSlice";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import { setRefreshToken, setToken } from "../../Utils/auth";
import { RxCross1 } from "react-icons/rx";
import Select from "react-select";
import "../../Components/AddProduct/addproduct.css";

// import "react-phone-number-input/style.css";
// import PhoneInput from "react-phone-number-input";
// import PhoneInput from "react-phone-input-2";
// import "react-phone-input-2/lib/style.css";
import Loader from "../../Components/Loader/Loader";

// import {
//   isValidPhoneNumber,
//   isPossiblePhoneNumber,
// } from "react-phone-number-input";

type initialSignupValues = {
  firstName: string;
  lastName: string;
  mobile: string;
  email: string;
  // telephone: string;
  password: string;
};

type initialLoginValues = {
  email: string;
  password: string;
};

function Signup() {
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();
  const naviagte = useNavigate();
  const [profilePic, setProfilePic] = useState("");
  const [profilePicforpost, setprofilePicforpost] = useState("");
  const [isValidNumber, setIsValidNumber] = useState(true);
  const [value, setValue] = useState("");
  const [choose, setChoose] = useState<any>([{}]);
  const firstNameInput = useCallback((inputElement: any) => {
    if (inputElement) {
      inputElement.focus();
    }
  }, []);

  const handleLogin = async (values: initialLoginValues) => {
    setIsLoading(true);
    await commonAxios
      .post("/cemo/auth/login", values)
      .then(
        (result) => {
          console.log("Login", result);
          if (result.data.role === "ROLE_CUSTOMER") {
            setIsLoading(false);
            dispatch(setUserDetails(result.data));
            setToken(result.data?.accessToken);
            setRefreshToken(result.data?.refreshToken);
            const redirectBactTo = JSON.parse(localStorage.getItem('redirectBactTo') as string);
            if (redirectBactTo) {
              localStorage.removeItem('redirectBactTo')
              naviagte(redirectBactTo, { replace: true })
            } else {
              naviagte("/", { replace: true });
            }
          } else {
            failureNotify("Login with user credentials.");
          }
        },
        (error) => {
          console.log(error, "error in else parts");
          setIsLoading(false);
          failureNotify("oops!something went wrong");
        }
      )
      .finally(() => {
        setIsLoading(false);
      });
  };

  const handleSignup = async (values: initialSignupValues) => {
    setIsLoading(true);

    let body = {
      ...values,
      profileImage: null,
      businessName: null,
      businessNumber: null,

      businessPinCode: null,

    };

    let value: any = {};
    if (profilePicforpost) {
      const formData = new FormData();
      formData.append(`file`, profilePicforpost);
      formData.append("folder", "user_profile");
      value = await formAxios.post("/cemo/upload/files", formData);
      if (value.data) {
        body = {
          ...values,
          profileImage: value.data.join(","),
          businessName: null,
          businessNumber: null,

          businessPinCode: null,

        };
      }
    }

    try {
      const response = await commonAxios.post("/cemo/appuser/customer", body);

      if (response.status === 201) {
        setIsLoading(false);
        successNotify("Signup successful");
        handleLogin({ email: values.email, password: values.password })
        // naviagte("/login", { replace: true });
      } else {
        setIsLoading(false);
        failureNotify("oops!something went wrong");
      }
    } catch (ex) {
      console.log("ex", ex);
    } finally {
      setIsLoading(false);
    }
  };

  const validateEmail = async (values: initialSignupValues) => {
    const body = {
      emailId: values.email,
    };

    try {
      const response = await commonAxios.post(
        "/cemo/appuser/validate/email",
        body
      );
      if (response.data.isAccountAlreadyExists) {
        failureNotify("Account already exists with same Email id, try new one");
      } else {
        handleSignup(values);
      }
    } catch (error) {
      console.log("validateEmail", error);
    }
  };

  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      mobile: "",
      email: "",
      // telephone: "",
      businessAddress: "",
      businessCity: "",
      password: "",
    },
    validationSchema: Yup.object({
      firstName: Yup.string()
        // .max(15, "Must be 15 characters or less")
        .required("Förnamn is required"),
      lastName: Yup.string()
        // .max(20, "Must be 20 characters or less")
        .required("Efternamn is required"),
      email: Yup.string().email("Invalid email address").required("Email is required"),
      businessAddress: Yup.string().required("Address is required"),
      businessCity: Yup.string().required("City is required"),
      mobile: Yup.string().required("mobile number is required"),
      // mobile: Yup.string()
      //   .transform((value, originalValue) => {
      //     // Remove spaces from the phone number
      //     return originalValue.replace(/\s/g, "");
      //   })
      //   .matches(/^\+46\d{9}-$/, "Invalid phone number, alike (+46 xx xxx xxxx)").length(12)
      //   .required("Required"),
      // telephone: Yup.string()
      // .transform((value, originalValue) => {
      //   // Remove spaces from the phone number
      //   return originalValue.replace(/\s/g, "");
      // })
      // .matches(/^\+46\d{9}$/, "Invalid phone number, alike (+46 xx xxx xxxx)").length(12)
      // .matches(/^\d$/, "Invalid mobile number")
      // .required("Telefonummer  is required"),
      password: Yup.string()
        .required("Password is required")
        .min(6, "Password must be at least 6 characters long")
        // .max(8, "Password must be at most 8 characters long")
        .matches(
          /^(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9!@#$%^&*()-_+=]+$/,
          "Password must contain at least one uppercase, one lowercase, and one numeric character"
        ),
    }),
    onSubmit: (values) => validateEmail(values),
    validateOnBlur: true,         // Trigger validation on blur
    validateOnChange: false,
  });

  const getEventCities = async () => {
    const response = await commonAxios.get("/cemo/cities");
    if (response.status === 200) {
      const cityList = response.data.map((city: any) => {
        return {
          value: city.city,
          label: city.city,
          id: city.id,
        };
      });
      console.log("response", response);
      console.log(cityList, "cityList")
      setChoose(cityList);
    }

  }

  useEffect(() => {
    getEventCities();
  }, [])

  const handleChange = (e: any) => {
    e.preventDefault();
    const file = e.target.files[0];

    // Check if the selected file's type is an image
    if (file && file.type.startsWith('image/')) {
      setprofilePicforpost(file);
      const reader = new FileReader();
      reader.onloadend = () => {
        const base64String: any = reader.result;
        setProfilePic(base64String);
      };
      reader.readAsDataURL(file);
    } else {
      failureNotify('Please upload a valid image file (jpg, jpeg, png, gif).');
    }
  };

  const handlePhoneNumberChange = (value: any) => {
    if (!undefined) {
      formik.setFieldValue("telephone", value);
    }
    console.log(value, "value");
    setValue(value);
    // console.log(isValidPhoneNumber(value), "isValidPhoneNumber");
    // console.log(isPossiblePhoneNumber(value), "isPossiblePhoneNumber");
    setIsValidNumber(value);
  };

  const handleEmailChange = (e: any) => {
    const email = e.target.value;
    const lowercaseEmail = email.toLowerCase();
    formik.handleChange(e);
    formik.setFieldValue('email', lowercaseEmail);
  };

  return (
    <div className="Signup">
      {isLoading && <Loader />}
      <div className="bg-[#FFE5D8]">
        <div className="flex justify-end">
          <button
            className="p-2 w-auto m-5 font-semibold rounded-lg bg-[#D6B37D] text-white px-6 text-sm uppercase"
            onClick={() => naviagte("/login")}
          >
            Logga in
          </button>
          {/* <button className=' p-3 m-5  font-semibold rounded-lg bg-[#D6B37D] text-white px-8' onClick={() => naviagte('/supplierlogin')}>Login as Supplier</button> */}
        </div>
        <div className="grid md:grid-cols-1 min-h-[20vh]">
          <div className="px-4 flex justify-center items-center">
            <img
              className="mb-5 ml-4 pl-0 h-24 -mt-14 cursor-pointer"
              src={Heading}
              onClick={() => naviagte("/")}
              loading="eager"
            ></img>
          </div>
        </div>
      </div>
      {/*  */}
      <div className='ms-8'>
        <h2 className="mx-n3 text-2xl md:text-3xl px-3 md:px-6 mt-5 ml-1 font-semibold text-secondary">
          Yay, du är här!
        </h2>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 min-h-[40vh] m-10 -mt-1">
        <div>
          <div className="flex justify-center items-center mt-5">
            <h4 className=" px-3 md:px-6 text-left text-secondary first-letter:text-1xl text-[18px]">
              Du är bara ett steg ifrån att upptäcka en värld full av spännande, utmanande och glädjefyllda upplevelser tillgängliga för dig.
            </h4>
          </div>
          <div className="p-3 md:p-6  items-center -mt-0.5">
            <p className="text-left text-secondary first-letter:text-1xl text-[18px]">
              Hos oss kan du utforska ett utbud av oförutsägbara och glädjefyllda och för dig kanske helt nya saker att göra och uppleva. Allt från kulinariska upplevelser, underhållare, musikanter, äventyr, lustfyllda kunskaper eller helt enkelt ett otroligt brett sortiment. Vi har allt för alla minnesvärda stunder. Planera hela tillställningen på ett ställe!
            </p>
            <p className="text-left text-secondary first-letter:text-1xl text-[18px]">
              Fyll i dina uppgifter och låt oss börja skapa minnen tillsammans!
            </p>
          </div>
          {/* <div className="lg:p-10 p-5 flex justify-center items-center -mt-6"> */}
          {/* <div className="p-3 md:p-6 flex justify-center items-center -mt-5">
            <h4 className="text-left text-secondary text-[18px]">
              Gör dig redo att utforska, planera och skapa minnen som kommer vara omtalade länge efter att festen är över. Bli medlem nu och låt oss tillsammans skapa magiska stunder och inspirera andra att tänka utanför boxen. Välkommen till vårt Community av evenemangsinnovatörer.
            </h4>
          </div> */}
        </div>
        <div>


          <div className="flex justify-center items-center mt-3 mb-12">
            <form
              onSubmit={formik.handleSubmit}
              className="lg:w-[75%] w-[95%] max-w-xl mx-auto h-auto shadow-2xl first-letter:border bg-[#FFFFFF] rounded-2xl p-6"
            >
              <div className="mb-3">
                <label className="block text-secondary mb-2 font-semibold">Förnamn</label>
                <input
                  className="appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="firstName"
                  name="firstName"
                  type="text"
                  onChange={formik.handleChange}
                  // ref={firstNameInput}
                  onBlur={formik.handleBlur}
                  value={formik.values.firstName}
                  placeholder="Förnamn"
                  autoComplete="off"
                />
                {formik.touched.firstName && formik.errors.firstName ? (
                  <div className="text-red-600 my-2">
                    {formik.errors.firstName}
                  </div>
                ) : null}
              </div>
              <div className="mb-3">
                <label className="block text-secondary font-semibold  mb-2">Efternamn</label>
                <input
                  className="appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="lastName"
                  name="lastName"
                  type="text"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.lastName}
                  placeholder="Efternamn"
                  autoComplete="off"
                />
                {formik.touched.lastName && formik.errors.lastName ? (
                  <div className="text-red-600 my-2">
                    {formik.errors.lastName}
                  </div>
                ) : null}
              </div>
              {/* <div className="mb-3">
              <label className="block text-[#1A202C]   mb-2">Personummer</label>
              <input
                className="appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="mobile"
                name="mobile"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.mobile}
                placeholder="Personummer"
                autoComplete="off"
              />
              {formik.touched.mobile && formik.errors.mobile ? (
                <div className="text-red-600 my-2">{formik.errors.mobile}</div>
              ) : null}
            </div> */}
              {/* <div className="mb-3">
              <label className="block text-secondary  font-semibold mb-2">
                Telefonummer
              </label>
              <input
                className="appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="telephone"
                name="telephone"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.telephone}
                placeholder="Telefonummer"
                autoComplete="off"
              />
              {formik.touched.telephone && formik.errors.telephone ? (
                <div className="text-red-600 my-2">
                  {formik.errors.telephone}
                </div>
              ) : null}
            </div> */}
              <div className="mb-3">
                <label className="block text-secondary  font-semibold mb-2">
                  Telefonummer
                </label>
                <input
                  className="appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="mobile"
                  name="mobile"
                  type="text"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.mobile}
                  placeholder="Telefonummer"
                  autoComplete="off"
                />
                {formik.touched.mobile && formik.errors.mobile ? (
                  <div className="text-red-600 my-2">
                    {formik.errors.mobile}
                  </div>
                ) : null}
              </div>
              <div className="mb-3">
                <label className="block text-secondary  font-semibold mb-2">
                  Adress
                </label>
                <input
                  className="appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="businessAddress"
                  name="businessAddress"
                  type="text"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.businessAddress}
                  placeholder="Address"
                  autoComplete="off"
                />
                {formik.touched.businessAddress &&
                  formik.errors.businessAddress ? (
                  <div className="text-red-600 my-2">
                    {formik.errors.businessAddress}
                  </div>
                ) : null}
              </div>
              <div className="mb-3">
                {/* <label className="block text-secondary  font-semibold mb-2">
              Stad
              </label>
              <input
                className="appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="businessCity"
                name="businessCity"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.businessCity}
                placeholder="Stad"
                autoComplete="off"
              />
              {formik.touched.businessCity &&
                formik.errors.businessCity ? (
                <div className="text-red-600 my-2">
                  {formik.errors.businessCity}
                </div>
              ) : null} */}

                <label className="block text-secondary  font-semibold mb-2">Stad</label>
                <Select
                  className="py-3 md:w-full  outline-none leading-tight mb-2"
                  placeholder="Choose Stad"
                  name="city"
                  onBlur={formik.handleBlur}
                  onChange={(option: any) =>
                    formik.setFieldValue("businessCity", option?.value?.toUpperCase())
                  }
                  value={choose.filter((option: any) =>
                    formik.values.businessCity?.includes(option?.value?.toUpperCase())
                  )}
                  options={choose}
                // styles={{
                //   option: (provided, state) => ({
                //     ...provided,
                //     color: 'gray', // Set the text color to gray
                //   }),
                // }}

                />
              </div>

              <div className="mb-3 flex">
                <div>
                  <label
                    className="border-[1px] block w-full h-15 rounded-lg text-center my-5 p-3 text-secondary"
                    htmlFor="fileInput"
                    id="customLabel"
                  >
                    Ladda Upp bild
                  </label>
                  <input
                    className="appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    id="fileInput"
                    name="profilepic"
                    type="file"
                    accept=".jpg, .jpeg, .png, .gif"
                    onChange={(e) => handleChange(e)}
                    autoComplete="off"
                  />
                </div>
                {profilePic && (
                  <div className="relative">
                    <img src={profilePic} width={"200px"} height={"50px"} />
                    <RxCross1
                      className="absolute top-0 right-0 bg-[#cc392e] text-white cursor-pointer"
                      onClick={() => setProfilePic("")}
                      style={{ color: '#A09792' }}
                    />
                  </div>
                )}
              </div>


              <div>
                <label className="block text-secondary font-extrabold text-[16px] mb-2">
                  E-post
                </label>
                <input
                  className="appearance-none border rounded-[10px] w-full py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="email"
                  type="email"
                  name="email"
                  onChange={handleEmailChange}
                  // ref={emailInput}
                  onBlur={formik.handleBlur}
                  value={formik.values.email}
                  placeholder="E-post"
                  autoComplete="off"
                />
                {formik.errors.email ? (
                  <div className="text-red-600 my-2">{formik.errors.email}</div>
                ) : null}
              </div>
              <div className="mb-3">
                <label className="block text-secondary font-semibold  mb-2">Lösenord</label>
                <input
                  className="appearance-none border rounded-md  w-full py-2 px-4 text-secondary leading-tight focus:outline-none focus:shadow-outline"
                  id="password"
                  name="password"
                  type="password"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.password}
                  placeholder="Lösenord"
                  autoComplete="off"
                />
                {formik.touched.password && formik.errors.password ? (
                  <div className="text-red-600 my-2">
                    {formik.errors.password}
                  </div>
                ) : null}
              </div>
              <div className="flex items-center mb-6">
                <input
                  className="mr-2 leading-tight"
                  type="checkbox"
                  id="remember"
                />
                <label className="text-sm text-secondary">Kom ihåg mig</label>
              </div>
              <div className="flex items-center justify-center ">
                <button
                  className="p-2 w-auto m-1 font-semibold rounded-lg bg-[#D6B37D] text-white px-6 uppercase"
                  type="submit"
                >
                  Skapa konto
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}
export default Signup;
