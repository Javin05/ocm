import React, { useState, ChangeEvent, FormEvent } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { commonAxios } from "../../Axios/service";
import mainLogo from "../../assets/blixa-black.png";
import Loader from "../../Components/Loader/Loader";
import { successNotify } from "../../Components/Toast/Toast";
import { failureNotify } from "../../Components/Toast/Toast";

const ForgotPassword = () => {
  const [email, setEmail] = useState<string>("");
  const [isValid, setIsValid] = useState<boolean>(true);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const navigate = useNavigate();
  const location = useLocation();
  const { state } = location;
  const { pathURL } = state;

  const handleEmailChange = (event: ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
    setIsValid(true);
  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    if (validateEmail(email)) {
      setIsValid(true);
      getResetLink(email);
    } else {
      setIsValid(false);
    }
  };

  const validateEmail = (email: any) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const getResetLink = async (email: any) => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get(
        `/cemo/user/security?emailId=${email}`
      );
      setIsLoading(false);
      if (response.data != null && response.data.errorErrorMessage == null) {
        successNotify(response.data);
        navigate(pathURL)
      } else {
        failureNotify(response.data.errorErrorMessage);
      }
    } catch (error) {
      setIsLoading(false);
      failureNotify("Error occurred...");
    }
  };

  return (
    <div className="h-[100vh] flex flex-col justify-center items-center bg -mt-2">
      {isLoading ? <Loader /> : null}
      <img src={mainLogo} className="h-28" onClick={() => navigate("/")} />
      <div className="block h-auto w-auto bg-[#FFE5D8] px-10 py-10 m-5 rounded-3xl">
        <p className="font-semibold text-[24px] w-full flex flex-row justify-center text-secondary">
          GLÖMT LÖSENORD?
        </p>
        <form onSubmit={handleSubmit} className="max-w-md mx-auto">
          <label className="block mt-6 mb-2 text-secondary font-extrabold">
            E-post:
          </label>
          <input
            type="text"
            placeholder="Enter E-post"
            value={email}
            onChange={handleEmailChange}
            className="appearance-none border rounded-xl w-full py-3 px-3 mt-1 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          />

          {!isValid && (
            <p className="text-red-500 mt-1">
              Enter a valid email address.
            </p>
          )}
          <div className="flex flex-row justify-between">
            <button
              type="submit"
              className="bg-[#D6B37D] text-[14px] sm:text-[16px] text-white font-bold py-1 px-2 sm:py-2 sm:px-4 rounded-xl mt-5 uppercase"
            >
              RESET LINK
            </button>
            <p
              className="  mt-8 text-[12px] sm:text-[16px] font-semibold underline text-[#D6B37D] cursor-pointer "
              onClick={() => navigate(pathURL)}
            >
              Back to SignIn
            </p>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ForgotPassword;
