import { createContext, useEffect, useState } from "react";
import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";
import { Outlet } from "react-router";
import { isUserAuthenticated } from "../../Utils/auth";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

import { RootState } from "../../Redux/store";
import { failureNotify } from "../../Components/Toast/Toast";
import { commonAxios } from "../../Axios/service";

const headerLessRoute = [
  "/",
  "/products",
  "/productscreen",
  "/orderList",
  "/supplierlogin",
  "/merchant",
  "/schedule",
  "/category",
  "/eventslist",
  "/eventscreen",
  "/customerlist",
  "/activateSupplier",
  "/messages",
  "/Reviewandcomments",
  "/profile-password-reset",
  "/backpanel-support",
  "/customer-messages",
  "/quotations-list",
  "/contact-supplier",
  "/contactFeedback",
  "/notifyme"
];

const footerLessRoute = [
  "/orderList",
  "/ordershistory",
  "/products",
  "/productscreen",
  "/merchant",
  "/schedule",
  "/category",
  "/eventslist",
  "/eventscreen",
  "/customerlist",
  "/messages",
  "/Reviewandcomments",
  "/activateSupplier",
  "/profile-password-reset",
  "/backpanel-support",
  "/customer-messages",
  "/quotations-list",
  "/contact-supplier",
  "/contactFeedback",
  "/notifyme"
];
const supplierRoutes = [
  "/orderList",
  "/merchant",
  "/schedule",
  "/category",
  "/eventslist",
];
const publicRoutes = [
  "/",
  "/popular",
  "/details",
  "/booking",
  "/checkout",
  "/cart",
  "/shoppingCart",
];
const AuthContext = createContext<any>(undefined);

const RootLayout = () => {
  const isLandingPage = headerLessRoute.includes(window.location.pathname);
  const isFooterLessComponent = footerLessRoute.includes(
    window.location.pathname
  );
  const isPublicRoute = publicRoutes.includes(window.location.pathname);

  const navigate = useNavigate();
  const userInfo = useSelector((state: RootState) => state.user.userDetails);

  useEffect(() => {
    if (!isPublicRoute && !isUserAuthenticated()) {
      failureNotify("Session expired! Login to continue");
      navigate("/login");
    }

    if (
      userInfo.role === "ROLE_CUSTOMER" &&
      supplierRoutes.includes(window.location.pathname)
    ) {
      failureNotify(
        "Page access restricted! Login as Supplier/Admin to continue"
      );
      navigate("/supplierlogin");
    }
    getCartDetails();
  }, [window.location.pathname]);
  const userDetails = useSelector((state: RootState) => state.user.userDetails);
  const [listOfProductsSize, setListOfProducts] = useState(0);
  const getCartDetails = async () => {
    if (userDetails.id) {
      if (userDetails.role === "ROLE_CUSTOMER") {
        try {
          // setIsLoading(true);
          const response = await commonAxios.get(
            `/cemo/bookingitems/get/cart/${userDetails.id}`
          );
          const listOfProducts = response.data.bookingItemList.filter(
            (item: any) => item.bookingItemType.toLowerCase() === "product"
          );

          if (listOfProducts) {
            setListOfProducts(listOfProducts.length);
          } else {
            setListOfProducts(0)
          }
        } catch (error) {
        } finally {
          // setIsLoading(false);
        }
      }
    } else {
      const listOfProducts = JSON.parse(localStorage.getItem("cartListDetails") as string);
      if (listOfProducts) {
        setListOfProducts(listOfProducts.quantity || 0);
      }
    }
  };


  return (
    <>
      {isLandingPage ? null : <Header productCount={listOfProductsSize} />}
      <Outlet />
      {isFooterLessComponent ? null : <Footer />}
    </>
  );
};

export { RootLayout as default, AuthContext as AuthContext };
