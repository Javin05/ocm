import React, { useEffect, useState } from "react";
import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";

function FAQ() {
  const [activeIndex, setActiveIndex] = useState(null);

  const handleAccordionClick = (index: any) => {
    setActiveIndex(activeIndex === index ? null : index);
  };
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <>
      <Header />
      <div className="h-[100vh] sm:h-screen my-32">
        <div className="flex flex-wrap md:p-10 lg:p-10 xl:p-10 p-2">
          <div className="w-full md:w-full lg:w-1/2 xl:w-1/2 justify-center items-center">
            {" "}
            <h1 className="font-bold text-[30px] text-secondary">FAQ</h1>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti,
              et quis? Delectus repudiandae recusandae saepe voluptate
              accusantium a, tenetur iusto quasi, quis amet rem obcaecati
              provident officia eos. Nulla, cupiditate?
            </p>
          </div>
          <div className="w-full md:full lg:w-1/2 xl:w-1/2">
            <div>
              <AccordionItem
                title="Accordion Item 1"
                content="Content for Accordion Item 1"
                isActive={activeIndex === 0}
                onClick={() => handleAccordionClick(0)}
              />
              <AccordionItem
                title="Accordion Item 2"
                content="Content for Accordion Item 2"
                isActive={activeIndex === 1}
                onClick={() => handleAccordionClick(1)}
              />
              <AccordionItem
                title="Accordion Item 3"
                content="Content for Accordion Item 3"
                isActive={activeIndex === 2}
                onClick={() => handleAccordionClick(2)}
              />
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}

const AccordionItem = ({ title, content, isActive, onClick }: any) => {
  const [isRead, setIsRead] = useState(false);
  const handleHeadingClick = () => {
    setIsRead(!isRead);
    onClick();
  };
  return (
    <div>
      <div
        className={`accordion-heading accordion-heading cursor-pointer rounded-lg text-black p-4 mt-5  flex justify-between ${
          isRead ? "bg-[#D6B27D] text-black font-bold" : " bg-[#FFE5D8] "
        }`}
        onClick={handleHeadingClick}
      >
        <h3>{title}</h3>
        {!isActive && (
          <span className={`accordion-icon  ${isActive ? "active" : ""}`}>
            +
          </span>
        )}

        {isActive && (
          <span className={`accordion-icon  ${isActive ? "active" : ""}`}>
            -
          </span>
        )}
      </div>
      {isActive && <div className="accordion-content mt-5">{content}</div>}
    </div>
  );
};

export default FAQ;
