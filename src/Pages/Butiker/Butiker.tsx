import React, { useEffect, useRef, useState } from "react";
import landingImg from "../../assets/hero_image.jpeg";
import Footer from "../../Components/Footer/Footer";
import Header from "../../Components/Header/Header";
import shopImage from "../../assets/shop3.jpg";
import shopsImage from "../../assets/shop1.jpg";
import StaderCard from "../../Components/StaderCard/StaderCard";
import staderOne from "../../assets/staderOne.png";
import staderTwo from "../../assets/staderTwo.png";
import staderThree from "../../assets/staderThree.png";
import staderFour from "../../assets/staderFour.png";
import { createSearchParams, useNavigate } from "react-router-dom";

const Butiker = () => {

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);
    const naviagte = useNavigate();
    const navigate = useNavigate();
    const [isGlobalSearch, setIsGlobalSearch] = useState<boolean>(false);
    const [title, setTitle] = useState("");
    const [globalSearch, setGlobalSearch] = useState("Örebro");
    const [currentLocation, setCurrentLocation] = useState("Örebro");

    const getEventsBySearch = async (text: string) => {
        const params = {
            searchText: text,
            searchTitle: title,
            location: currentLocation,
        };
        navigate(
            {
                pathname: "/popular",
                search: `?${createSearchParams(params)}`,
            },
            {
                state: { isGlobalSearch },
            }
        );
    };

    return (
        <>
            <Header />
            <div className="flex md:flex-row flex-col">
                {/* <div className="w-full p-7 mt-8">
                    <img src={shopImage} className=" object-fill rounded-lg" />
                </div> */}
                <div className="w-full p-5 flex">
                    <p className="text-secondary ">
                        <h1 className="order-first font-[sofiapro] font-bold text-[24px] text-secondary sm:p-4 py-4">Samarbetspartners</h1>
                        <img src={shopImage} className="rounded-lg sm:w-[50%] w-[100%] sm:ml-3 sm:mb-4 float-left mr-6 mb-5" />
                        <br />
                        {/* <h1 className="font-[sofiapro] font-bold text-[28px] ml-3 text-secondary ">Samarbetspartners</h1> */}
                        <p className="mr-4 text-secondary sm:ml-4 mt-4 sm:mt-0">
                            {/* <img src={shopsImage} className="object-fill rounded-lg sm:w-[50%] w-[100%] sm:ml-2 mb-4 float-right " /> */}
                            <p className="mr-4 text-secondary -mt-7 font-bold text-xl">
                                Fantastiskt att du överväger att bli en del av Blixa!
                            </p>
                            <br />
                            <p className="-mt-1 mr-4 text-secondary">
                                Plattformen som förändrar hur vi planerar och genomför fest, event och sociala tillställningar.
                            </p>
                            <br />
                            <p className="-mt-1 mr-4 text-secondary">
                                Som vår blivande samarbetspartner kommer du att upptäcka ett gäng fördelar för er som företag.</p>
                            <p className="-mt-1 mr-4 text-secondary">
                                Ni kommer bli exponerade för en helt ny kundgrupp. Det finns ett helt gäng människor där ute som helt har missat er på marknaden och som inte har tid och engagemang att leta reda på er heller. Och det är där vi kommer slå två flugor i en smäll! </p>
                            <p className="-mt-1 mr-4 text-secondary">
                                Dom orkar inte leta och kommer in på BLIXA´s sida och då finns ni där och väcker nyfikenhet. </p>
                            <p className="-mt-1 mr-4 text-secondary">
                                Ni kommer alltså ha en synlig plats på hemsidan inom ert område. Vi kommer hjälpa er att nå större synlighet och på så vis marknadsföra er för våra användare. Vi ser till att er verksamhet syns på rätt plats vid rätt tillfälle. </p>
                            <p className="-mt-1 mr-4 text-secondary">
                                Detta innebär att du kan fokusera på det du är bra på och låt oss sprida vetskapen om er verksamhet! </p>
                            <br />
                            <p className="-mt-1 mr-4 text-secondary">
                                Vi förstår att din tid är värdefull, så vi har förenklat hanteringen av bokningsförfrågan och bokningen på vår plattform. Detta är en smidig process som spar dig tid och resurser.                             </p>
                            <br />

                            <p className="-mt-1 mr-4 text-secondary">
                                Det bästa av allt är ett det är helt kostnadsfritt att ansluta dig till oss! Vi tar endast n procentandel på det som bokas och säljs via vår plattform.                                 </p>
                            <p className="-mt-1 mr-4 text-secondary">
                                Detta innebär att du kan testa vårt samarbete utan någon ekonomisk risk!
                            </p>


                            <p className="-mt-1 mr-4 text-secondary">
                                Tillsammans kan vi förändra och förenkla planeringen för fest, event och sociala tillställningar.
                            </p>
                            <p className="-mt-1 mr-4 text-secondary">
                                Vi tror på gemenskap, kreativt samarbete, nytänkande och glädje!
                            </p>
                            <br />
                            <p className="-mt-1 mr-4 text-secondary">
                                Välkommen till Blixa!
                            </p>

                        </p>
                        <div className="flex justify-center ">
                            <button
                                className="p-2 m-5 font-semibold rounded-lg bg-[#D6B37D] text-white px-6 uppercase"
                                onClick={() => naviagte('/suppliersignup')}
                            >
                                Skapa konto
                            </button>
                        </div>
                    </p>
                </div>
            </div>


            <div className="flex md:flex-row flex-col">
                <div className="w-full p-5 -mt-2">
                    {/* <h1 className="font-[sofiapro] font-bold text-[28px] text-secondary">Samarbetspartners</h1> */}
                    {/* <p className="mr-4 text-secondary sm:ml-4 mt-4 sm:mt-0">
                    {/* <p className="mr-4 text-secondary sm:ml-4 mt-4 sm:mt-0">
                        <img src={shopsImage} className="object-fill rounded-lg sm:w-[50%] w-[100%] sm:ml-2 mb-4 float-right " />
                        <p className="mr-4 text-secondary text-xl font-bold">
                            ELLER… om den tidigare är för lång för bilden
                        </p>
                        <p className="mr-4 text-secondary mt-3">
                            Är du redo att ta ditt företag till nästa nivå?
                        </p>
                        <p>
                            Plattformen som förändrar hur vi planerar och genomför fest, event och sociala tillställningar.
                        </p>
                        <br />
                        <p className="-mt-1 mr-4 text-secondary">
                            Som vår blivande samarbetspartner kommer du att upptäcka ett gäng fördelar för er verksamhet.
                        </p>
                        <p className="-mt-1 mr-4 text-secondary">
                        Ni kommer bli exponerade för en helt ny kundgrupp. Det finns ett helt gäng människor där ute som helt har missat er på marknaden och som inte har tid och engagemang att leta efter nya erfarenheter.                            
                        </p>
                        <br />
                        <p className="-mt-1 mr-4 text-secondary">
                        Vi kommer hjälpa er att nå större synlighet och på så vis marknadsföra er för våra användare. Vi ser till att er verksamhet syns på rätt plats vid rätt tillfälle.                            
                        </p>
                        <p className="-mt-1 mr-4 text-secondary">
                        Detta innebär att du kan fokusera på det du är bra på och låt oss sprida vetskapen om er verksamhet!                         
                        </p>
                        <br />
                        <p className="-mt-1 mr-4 text-secondary">
                        Vi förstår att din tid är värdefull, så vi har förenklat hanteringen av bokningsförfrågan och bokningen på vår plattform. Detta är en smidig process som spar dig tid och resurser.                         
                        </p>
                        <br/>
                        <p className="-mt-1 mr-4 text-secondary">
                        Det bästa av allt är ett det är helt kostnadsfritt att ansluta dig till oss! Vi tar endast n procentandel på det som bokas och säljs via vår plattform.                        
                        </p>
                        <p className="-mt-1 mr-4 text-secondary">
                        Detta innebär att du kan testa vårt samarbete utan någon ekonomisk risk!
                        </p>
                        <br/>
                        <p className="-mt-1 mr-4 text-secondary">
                        Välkommen!                        
                        </p>
                    </p> */}
                </div>
                {/* <div className="w-full flex justify-center p-6">
                </div> */}
            </div>

            <div className="-mt-0.5 mx-6 md:mx-16">
                <h1 className="font-semibold text-[28px] sm:text-[1rem] leading-7 text-[#D6B27D]">
                    Städer
                </h1>
                <h1 className="sm:text-[32px] font-semibold font-[sofiapro] text-secondary">
                    Hitta oss dessa städer och manga fler
                </h1>
                <div className="grid grid-cols-1 sm:grid-cols-2 mb-6  md:grid-cols-2 lg:grid-cols-4 mt-4 gap-4 md:gap-6">
                    <div className="grid-item">
                        <StaderCard
                            onCardClick={getEventsBySearch}
                            staderImg={staderOne}
                            staderTitle={"Örebro"}
                        />
                    </div>
                    <div className="grid-item">
                        <StaderCard
                            onCardClick={getEventsBySearch}
                            staderImg={staderTwo}
                            staderTitle={"Stockholm"}
                        />
                    </div>
                    <div className="grid-item">
                        <StaderCard
                            onCardClick={getEventsBySearch}
                            staderImg={staderThree}
                            staderTitle={"Uppsala"}
                        />
                    </div>
                    <div className="grid-item">
                        <StaderCard
                            onCardClick={getEventsBySearch}
                            staderImg={staderFour}
                            staderTitle={"Västerås"}
                        />
                    </div>
                </div>
            </div>
            <Footer />
        </>
    );
};

export default Butiker;
