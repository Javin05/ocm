import React, { useState, ChangeEvent, FormEvent, useEffect } from "react";
import mainLogo from "../../assets/blixa-black.png";
import { useLocation, useNavigate } from "react-router-dom";
import { commonAxios } from "../../Axios/service";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import Loader from "../../Components/Loader/Loader";
import Footer from "../../Components/Footer/Footer";
import Header from "../../Components/Header/Header";
import locationIcon from "../../assets/location.svg";
import location from "../../assets/gps.svg"
import epostIcon from "../../assets/epost.png";
import callIcon from "../../assets/call.svg";
import clockIcon from "../../assets/Clock.svg";
import { MdOutlineMail } from "react-icons/md";

const ContactUs = () => {
  const [name, setName] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [message, setMessage] = useState<string>("");
  const [isValidEmail, setIsValidEmail] = useState<boolean>(true);
  const [isValidName, setIsValidName] = useState<boolean>(true);
  const [isValidMessage, setIsValidMessage] = useState<boolean>(true);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const navigate = useNavigate();
  const location = window.location.pathname;
  useEffect(() => {
    if (location === "/contactus/false") {
      window.scrollTo(0, 850);
    } else {
      window.scrollTo(0, 0);
    }
  }, []);
  const handleNameChange = (event: ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
    setIsValidName(true);
  };
  const handleEmailChange = (event: ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
    setIsValidEmail(true);
  };
  const handleMessageChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
    setMessage(event.target.value);
    setIsValidMessage(true);
  };
  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    validateName(name);
    validateEmailID(email);
    validateMessage(message);
    if (
      validateName(name) &&
      validateEmailID(email) &&
      validateEmail(email)
    ) {
      console.log("send Email!")
      sendEmail();
    }
  };
  const sendEmail = async () => {
    console.log("entered in the api call!")
    setIsLoading(true);
    try {
      const body: any = await {
        name: name,
        emailID: email,
        message: message,
      };
      const response = await commonAxios.post("/cemo/contact", body);
      setIsLoading(false);
      if (response.data != null && response.data.errorErrorMessage == null) {
        navigate("/contactUsSuccess")
        // successNotify(response.data);
      } else {
        failureNotify(response.data.errorErrorMessage);
      }
    } catch (error) {
      setIsLoading(false);
      failureNotify("Error occurred...");
    }
  };
  const validateName = (name: string) => {
    if (name.trim().length > 0) {
      setIsValidName(true);
      return true;
    } else {
      setIsValidName(false);
      return false;
    }
  };
  const validateMessage = (message: string) => {
    if (message.trim().length >= 12) {
      setIsValidMessage(true);
      return true;
    } else {
      setIsValidMessage(false);
      return false;
    }
  };
  const validateEmailID = (email: string) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (emailRegex.test(email)) {
      setIsValidEmail(true);
      return true;
    } else {
      setIsValidEmail(false);
      return false;
    }
  };
  const [activeIndex, setActiveIndex] = useState(null);
  const handleAccordionClick = (index: any) => {
    setActiveIndex(activeIndex === index ? null : index);
  };
  const AccordionItem = ({ title, content, isActive, onClick }: any) => {
    const [isRead, setIsRead] = useState(false);
    const handleHeadingClick = () => {
      setIsRead(!isRead);
      onClick();
    };
    return (
      <div className="sm:p-4 p-1 sm:px-16 px-8 ">
        <div className="border-2 border-[#D6B27D] rounded-lg">
          <div
            className={`accordion-heading cursor-pointer text-secondary h-18 p-4  flex justify-between ${isRead ? "flex text-black font-bold" : ""
              }`}
            onClick={handleHeadingClick}
          >
            <h3>{title}</h3>
            {!isActive && (
              <span className={`accordion-icon  ${isActive ? "active" : ""}`}>
                +
              </span>
            )}
            {isActive && (
              <span className={`accordion-icon  ${isActive ? "active" : ""}`}>
                -
              </span>
            )}
          </div>
          {isActive && <div className="accordion-content mt-5 ml-4 text-secondary">{content}</div>}
        </div>

      </div>
    );
  };
  const validateEmail = (email: string) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };
  return (
    <>
      <Header />

      <div className="grid grid-cols-1 md:grid-cols-2  sm:p-6 -mt-1 ">
        {/* <div className="lg:h-[70vh] xl:h-auto flex flex-col items-center sm:mt-14 sm:mb-14 md:mt-14 md:mb-28 lg:mt-20 lg:mb-[14rem] xl:mt-20 xl:mb-20 mt-10 mb-10"> */}
        {isLoading ? <Loader /> : null}
        {/* <div className=" bg-[#FFE5D8] grid grid-cols-1 lg:grid-cols-2 p-5 sm:p-10 rounded-3xl w%] sm:w-[60%]"> */}
        <div className="w-full md:w-full md:p-10 p-5" style={{
          background: 'radial-gradient(circle, rgba(255, 229, 216, 1) 0%, rgba(250, 250, 250, 1) 70%)',
        }}>
          {/* <h3 className="font-semibold text-[13px] sm:text-[15px] text-secondary mb-2">
            Kontakta oss
          </h3> */}
          {/* <div className="text-center mt-12">
            <img
              src={mainLogo}
              className="mx-auto"
              onClick={() => navigate("/")}
            />
          </div> <br /> */}
          <div className="grid grid-cols-1 min-h-[20v]">
            <div className="min-h-[20vh] flex justify-center items-center">
              <img
                className="ml-4 pl-0 w-52 cursor-pointer"
                src={mainLogo}
                onClick={() => navigate("/")}
              ></img>
            </div>

          </div>
          <h3 className="font-semibold text-[13px] sm:text-[30px] text-secondary mb-2 text-center">
            Hej där, du fantastiska människa!
          </h3>
          <p className="text-[12px] sm:text-[15px] text-secondary text-center">
            Vi är här för att lyssna på dig. Har du frågor, förslag eller bara vill dela med dig av dina mest knasiga idéer?</p>
          <p className="text-[12px] sm:text-[15px] text-secondary text-center">
            Släng i väg ett meddelande och låt oss fördjupa oss i spännande samtal!
          </p>
          <div className="flex-1">
            <ul className="pt-7 pb-4 space-y-1 text-sm">
              {/* <li className="rounded-sm p-1 py-2 flex">
                <img
                  src={location}
                  className="text-[#D6B27D] h-[24px] w-[24px]"
                />
                <span
                  className={`mx-2 text-[14px] text-secondary font-medium `}
                >
                  Kulvagen 8, 703 69 Örebro
                </span>
              </li> */} <br />
              <br />
              <div className="flex justify-between">
                <li className="rounded-sm sm:p-1 py-2 flex">
                  <div style={{ height: '20px', width: '20px', color: 'gray' }}>
                    <MdOutlineMail size={22} />
                  </div>
                  <span className={`mx-2 text-[15px] text-secondary font-medium`}>
                    Info@ourcreativemoments.se
                  </span>
                </li>
                <li className="rounded-sm sm:p-1 py-2 flex">
                  <img src={callIcon} />
                  <span className={`mx-2 text-[15px] text-secondary font-medium`}>
                    +1234567890
                  </span>
                </li>
              </div>
              {/* <li className="rounded-sm p-1 py-4 flex">
                <img
                  src={clockIcon}
                  className="text-[#D6B27D] h-[24px] w-[24px]"
                />
                <span
                  className={`mx-2 text-[14px] text-secondary font-medium `}
                >
                  05.30 - 23.00
                </span>
              </li> */}
            </ul>
          </div>
        </div>
        <form className="lg:w-full w-full max-w-xl shadow-md mx-auto h-auto first-letter:border bg-[#FFFFFF] rounded-2xl px-5 sm:mt-10 mb-5 sm:mb-0" style={{ height: '570px' }}>
          <div className="">
            <p className="text-golden text-center font-bold pt-7 pb-5">
              Kontakta oss idag! Vi kommer att kontakta dig snart!
            </p>
          </div>
          <label className="font-semibold text-golden">Namn:</label>
          <input
            type="text"
            placeholder="Ange ditt namn"
            value={name}
            onChange={handleNameChange}
            className="appearance-none border rounded-md w-full py-3 px-3 mt-1 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          />
          {!isValidName && (
            <p className="text-red-500 mt-2 mb-5">Please enter your name.</p>
          )}
          <div className="mt-6">
            <label className="font-semibold text-golden ">E-post:</label>
            <input
              type="text"
              placeholder="Ange E-post"
              value={email}
              onChange={handleEmailChange}
              className="appearance-none border rounded-md w-full py-3 px-3 mt-1 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
            {!isValidEmail && (
              <p className="text-red-500 mt-2 mb-5">
                Please enter a valid email address.
              </p>
            )}
          </div>
          <div className="mt-6">
            <label className="font-semibold mt-4 text-golden">Meddelande:</label>
            <textarea
              rows={5}
              placeholder="Skriv här..."
              value={message}
              onChange={handleMessageChange}
              className="appearance-none border rounded-md w-full py-3 px-3 mt-1 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          {/* {!isValidMessage && (
                <p className="text-red-500 mt-2">
                  Message must be more than 12 letters.
                </p>
              )} */}
          {/* <div className="lg:flex lg:flex-row sm:flex sm:flex-col">
              <button className="rounded-md mt-6 uppercase text-white md:text-[14px] px-6 shadow-lg bg-[#D6B27D] md:text-md md:font-semibold mb-10 p-2 w-auto items-center" onClick={handleSubmit} style={{ flexGrow: 1 }}>
                Lägg till
              </button>
              <p
                className="mt-2 sm:mt-7 text-[12px] sm:text-[16px] font-semibold underline text-[#D6B37D] cursor-pointer"
                onClick={() => navigate("/")}
              style={{ flexGrow: 1 }}
              >
                Back to Home
              </p>
            </div> */}
          <div className="lg:flex lg:flex-row sm:flex sm:flex-col">
            <button className="rounded-md mt-6 uppercase text-white md:text-[14px] px-6 shadow-lg bg-[#D6B27D] md:text-md md:font-semibold mb-4 p-2 w-auto items-center" onClick={handleSubmit} style={{ flexGrow: 1 }}>
              Lägg till
            </button>
          </div>
          <div className="text-center">
            <p
              className="text-[12px] sm:text-[16px] font-semibold underline text-[#D6B37D] cursor-pointer pt-5"
              onClick={() => navigate("/")}
            >
              Tillbaka till hemmet
            </p>
          </div>
        </form>

      </div>
      <div className=" w-full">
        <div className="w-full place-items-center md:mb-1 md:mt-10 text-center " >
          <h3 className="font-bold text-[35px] sm:text-[35px] text-[#D6B27D] py-8">
            Frågor och svar
          </h3>
        </div>

        <div className="grid grid-cols-1 md:grid-cols-2 min-h-[70vh] justify-center">
          <div className="">

            <div className="">
              <AccordionItem
                title=" Vilka typer av evenemang kan jag planera och boks via er plattform?"
                content="Du kan planera och boka allt från barnkalas, möhippor/svensexor, födelsedagsfester, företagsevenemang, kickoffer, teambildning och sociala möten. Vi strävar efter att erbjuda en helhetslösning för alla typer av tillställningar."
                isActive={activeIndex === 0}
                onClick={() => handleAccordionClick(0)}
              />
              <AccordionItem
                title="Hur fungerar bokningsprocessen?"
                content="Bokningsprocessen är enkel och smidig. Du kan bläddra igenom vårt utbud av lokaler, aktiviteter, mat, drycker och underhållning. När du har hittat det du söker kan du göra en bokningsförfrågan och boka direkt på plattformen."
                isActive={activeIndex === 1}
                onClick={() => handleAccordionClick(1)}
              />
              <AccordionItem
                title="Kan jag anpassa mitt evenemang efter mina specifika önskemål och behov?"
                content="Absolut! Vi tror på att skapa skräddarsydda evenemang som passar dina unika önskemål. Du kan anpassa allt från meny till aktiviteterna och dekorationerna för att skapa det perfekt evenemanget."
                isActive={activeIndex === 2}
                onClick={() => handleAccordionClick(2)}
              />
              <AccordionItem
                title=" Hur vet jag att leverantörerna på er plattform är pålitliga?"
                content="Vi utvärderingar av alla leverantörer som finns på vår plattform för att säkerställa deras pålitlighet och kvalitet. Vi tar kundfeedback på allvar och strävar efter att erbjuda endast de bästa leverantörerna."
                isActive={activeIndex === 3}
                onClick={() => handleAccordionClick(3)}
              />
              <AccordionItem
                title=" Kan jag få hjälp och support under planeringsprocessen?"
                content="Absolut! Vi finns här för att hjälpa dig under hela planeringsprocessen. Du kan kontakta vår support via vår chattfunktion på plattformen, mejl eller telefon, och vi svarar gärna på dina frågor och ger dig den hjälp du behöver."
                isActive={activeIndex === 4}
                onClick={() => handleAccordionClick(4)}
              />
            </div>
          </div>
          <div className="">

            <div className="">
              <AccordionItem
                title="Finns det möjlighet att göra ändringar eller avboka min bokning?"
                content="Vi förstår att det ibland kan uppstå ändringar i planerna. Du kan kontakta oss för att göra ändringar i din bokning eller avboka den enligt våra avbokningsvillkor. Var noga med att läsa igenom dessa villkor för att få information om eventuella avgifter eller tidsgränser för ändringar och avbokningar."
                isActive={activeIndex === 5}
                onClick={() => handleAccordionClick(5)}
              />
              <AccordionItem
                title="Kan jag få en offert eller prisförslag innan jag bokar?"
                content="Självklart! Vi kan ge dig prisuppgifter och offertförlag baserat på specifika behov och önskemål."
                isActive={activeIndex === 6}
                onClick={() => handleAccordionClick(6)}
              />
              <AccordionItem
                title="Hur kan jag betala för min bokning?"
                content="Vi erbjuder olika betalningsalternativ för att passa dina behov. Du kan betala via kortbetalning på plattformen eller på faktura vid belopp över 5000kr, beroende på vad som är bekvämast för dig."
                isActive={activeIndex === 7}
                onClick={() => handleAccordionClick(7)}
              />
              <AccordionItem
                title="Vilka områden täcker ni med era tjänster?"
                content="Vi strävar efter att erbjuda våra tjänster över hela landet. Men för tillfället arbetar vi enbart med leverantörer i Örebro med omnejd."
                isActive={activeIndex === 8}
                onClick={() => handleAccordionClick(8)}
              />
              <AccordionItem
                title="Kan jag lämna feedback eller recensioner efter mitt evenemang?"
                content="Ja, vi uppskattar din feedback och recensioner! Vi välkomnar din åsikt och erfarenhet av att använda vår plattform och våra leverantörer. Du kan lämna feedback och recensioner på vår webbplats eller kontakta oss direkt."
                isActive={activeIndex === 9}
                onClick={() => handleAccordionClick(9)}
              />
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};
export default ContactUs;