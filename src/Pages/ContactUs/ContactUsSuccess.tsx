import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import Footer from "../../Components/Footer/Footer";
import Header from "../../Components/Header/Header";

const ContactUsSuccess = () => {
  const navigate = useNavigate();
  const goToLandingPage = () => {
    navigate("/");
  }
  const goToOderpage = () => {
    navigate("/ordershistory");
  }

  useEffect(() => {
    localStorage.removeItem('cartListDetails')
  }, [])

  return (
    <>
   <Header />
    <div className="flex relative rounded-lg flex-col items-center justify-center h-full my-5 mx-10">
      
      <section className='flex flex-col items-center justify-center bg-white bg-opacity-30 backdrop-filter backdrop-blur-lg border border-gray-300 rounded-lg p-5'>
        <div className='flex'>
          <h1 className="text-3xl md:text-xl font-bold text-secondary">
          “Wow! Tack för att du kontaktar oss!
          </h1>
        </div>
        <p className="text-secondary text-xl font-semibold my-3">
        Vi är så peppade på att vara en del av din resa mot minnesvärda stunder. Vi ser fram emot att utforska dina funderingar och idéer!
        </p>
        <p className="text-secondary text-xl font-semibold my-2">
        Vi kommer att besvara ditt meddelande snarast möjligt. Till dess, fortsätt sprida glädje och värme runt dig och framför allt fortsätt att vara DU!
        </p>
        <div className='flex justify-center'>
          <button
            onClick={goToLandingPage}
            className="bg-[#D6B27D] rounded-lg text-white uppercase p-2 px-6 md:px-[1.65rem] flex text-[14px] md:text-lg md:font-semibold my-4"
          >
            Back to Home page
          </button>
          {/* <button
            onClick={goToOderpage}
            className="bg-[#D6B27D] rounded-lg ml-16 text-white uppercase p-2 px-6 md:px-[1.65rem] flex text-[14px] md:text-lg md:font-semibold"
          >
            View orders
          </button> */}
        </div>
      </section>
    </div>
    <Footer />
    </>
  );
};

export default ContactUsSuccess;
