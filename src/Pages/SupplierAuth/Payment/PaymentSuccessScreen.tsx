import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import backgroundVideo from '../../../assets/confetti.mp4'
import { commonAxios } from '../../../Axios/service';
import { useSelector } from 'react-redux';
import { RootState } from '../../../Redux/store';

const PaymentSuccess = () => {
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const navigate = useNavigate();
  const goToLandingPage = () => {
    navigate("/");
  }
  const goToOderpage = () => {
    navigate("/ordershistory");
  }

  const handleClearAllCart = async () => {
    try {
      if (userInfo.id) {
        const response = await commonAxios.delete(
          `/cemo/bookingitems/delete/${userInfo.id}/clearcart`
        );
      }
    } catch (error) {
    } 
  };

  useEffect(() => {
    localStorage.removeItem('cartListDetails');
    handleClearAllCart();
  }, [])

  return (
    <div className="flex relative rounded-lg flex-col items-center justify-center h-[70vh]">
      <video
        autoPlay
        muted
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          objectFit: 'cover',
        }}
      >
        <source src={backgroundVideo} type="video/mp4" />
      </video>
      <section className='flex flex-col items-center justify-center absolute z-40 bg-white bg-opacity-30 backdrop-filter backdrop-blur-lg border border-gray-300 rounded-lg p-10'>
        <div className='flex'>
          <h1 className="text-4xl md:text-3xl font-bold text-secondary mt-8">
          Betalning genomförd!
          </h1>
        </div>
        <p className="text-secondary text-2xl font-semibold my-8">
          Tack för ditt köp! Din betalning har gått igenom.
        </p>
        <div className='flex'>
          <button
            onClick={goToLandingPage}
            className="bg-[#D6B27D] rounded-lg text-white uppercase p-2 px-6 md:px-[1.65rem] flex text-[14px] md:text-lg md:font-semibold"
          >
            Continue Shopping
          </button>
          <button
            onClick={goToOderpage}
            className="bg-[#D6B27D] rounded-lg ml-16 text-white uppercase p-2 px-6 md:px-[1.65rem] flex text-[14px] md:text-lg md:font-semibold"
          >
            View orders
          </button>
        </div>
      </section>
    </div>
  );
};

export default PaymentSuccess;
