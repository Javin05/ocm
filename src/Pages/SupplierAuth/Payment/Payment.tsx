import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import CheckoutForm from "./CheckoutForm";
import { commonAxios } from "../../../Axios/service";
import { failureNotify } from "../../../Components/Toast/Toast";
import { useEffect, useState } from "react";
import { json, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import { RootState } from "../../../Redux/store";
// Need to move this pl_test key to .env file
const stripePromise = loadStripe(
  "pk_test_51N3ztzJikOheBMh4MrGhAHVbHlsAamR9YLjcJnOD6LiGOaoH4sHak4X4YPoFizmflQhkY7RPq6JLlds5RKKEov6m009FqueMJd"
);

const Payment = () => {
  const [options, setOptions] = useState({ clientSecret: "" });
  const [momsDetails, setMomsDetails] = useState<any>(null);
  const location = useLocation();
  const productData = location.state;
  const eventData = location.state?.bookingData?.event;
  const totalAmount: any = (localStorage.getItem('cartListDetails'))
  const fixedTotalAmount = JSON.parse(totalAmount)
  const totalPrice = location.state?.bookingData?.totalPrice
    ? location.state?.bookingData?.totalPrice
    : JSON.parse(localStorage.getItem("totalPrice")!);
  const memberCount = location.state?.memberCount;
  const pricePerSeat = location.state?.bookingData?.price;
  const userInfo = useSelector((state: RootState) => state.user.userDetails);

  const { functionIdentifier } = location.state || {};

  const getToken = async () => {
    try {
      const data = {
        amount: Number(totalPrice),
        description: "Payment for cart checkout",
        currency: "SEK",
        stripeEmail: userInfo.email,
        token: {
          id: "dlosaldoas",
        },
      };
      const response = await commonAxios.post(
        "/cemo/create-payment-intent",
        data
      );

      setOptions((prev) => {
        return {
          ...prev,
          clientSecret: response?.data?.clientSecret,
        };
      });
    } catch (error) {
      failureNotify("oops!something went wrong");
    }
  };



  const getMomsDetails = async () => {
    try {
      const response = await commonAxios.get(
        `/cemo/bookingitems/get/${location.state?.bookingData?.id}`
      );
      setMomsDetails(response?.data);
    } catch (error) { }
  };

  const acceptStatusCall = async () => {
    try {
      const response = await commonAxios.put(`/cemo/quotation/update?quotationId=${location.state?.paymentData?.id}&status=ACCEPTED`);
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getToken();
    if (location.state?.bookingData?.id) {
      getMomsDetails();
    } else {
      setMomsDetails({});
    }
  }, []);


  return (
    <div className="grid md:grid-cols-2 grid-cols-1 md:p-10 p-2">
      <div className="px-5">
        <div className="flex justify-end">
          {options.clientSecret === "" || momsDetails === null ? null : (
            <Elements stripe={stripePromise} options={options}>
              <CheckoutForm momsDetails={momsDetails} eventData={eventData} bookingData={location.state?.bookingData} totalPrice={totalPrice} acceptStatusCall={acceptStatusCall} functionIdentifier={functionIdentifier} />
            </Elements>
          )}
        </div>
      </div>
      {location.state?.paymentData?.price ? (
        <div className="flex flex-col mt-8 md:mt-0">
          <div className="border rounded-md p-5">
            <h1 className="text-secondary font-semibold text-[20px] text-center">
              Bokningsdetaljer
            </h1>
            <div className="flex sm:flex-row flex-col sm:items-start sm:justify-start justify-center items-center mt-10">
              <div className="px-4"><img src={eventData.image} className="sm:w-[8rem] md:w-[8rem] lg:w-[8rem] object-cover rounded-xl h-[8rem]" /></div>
              <div className="">
                <h3 className="text-secondary text-[20px] mt-5">
                  {eventData?.name}
                </h3>
                {/* <h3 className="text-secondary text-[20px] mt-5">
                  Start date: {eventData?.startDate}
                </h3>
                <h3 className="text-secondary text-[20px] mt-5">
                  End date: {eventData?.endDate}
                </h3> */}
                <h3 className="text-secondary text-[20px] mt-5">
                  {totalPrice} SEK
                </h3>
                {/* <h3 className="text-secondary text-[20px] font-bold mt-5">
                  Category: {eventData?.category?.categoryName}
                </h3> */}
                {/* <h3 className="text-secondary text-[20px] mt-5">
                  Total : {totalPrice} SEK
                </h3> */}
              </div>

            </div>

            <div className="p-5 mt-5 flex flex-col sm:flex-row sm:justify-end justify-center items-center">
              <span className="text-[20px]">Total price :
                {pricePerSeat} * {memberCount} = {totalPrice} SEK
              </span>
            </div>

            <div className="p-5 mt-5 flex flex-col sm:flex-row sm:justify-end justify-center items-center">

              <span className="text-[20px] font-bold">Total :</span>  <span className="text-[20px] font-bold">{totalPrice} SEK </span>
            </div>
          </div>

        </div>

      ) : <div className="">
        <div className="border rounded-md p-5 mx-5 my-5 sm:my-0">
          <h1 className="text-secondary font-semibold text-[20px] text-center">
            Produktdetaljer
          </h1>
          <div className="mt-10">
            {
              productData?.map((product: any) => {
                return (
                  <>
                    <div className="flex sm:flex-row mt-10 items-center">
                      <div className="px-4"><img src={product.product?.image.split(',')[0]} className="sm:w-[8rem] md:w-[8rem] lg:w-[8rem] object-cover rounded-xl h-[8rem]" /></div>
                      <div className="">
                        <h3 className="text-secondary text-[20px] ">
                          {product.product?.name}
                        </h3>
                        {/* <h3 className="text-secondary text-[20px] mt-5">
                          Sub Total: {product.totalPrice - product.vatAmount} SEK
                        </h3>
                        <h3 className="text-secondary text-[20px] mt-5">
                          Tax: {product.vatAmount} SEK
                        </h3> */}
                        <h3 className="text-secondary text-[20px] mt-5">
                          {product.totalPrice} SEK
                        </h3>
                        {/* <h3 className="text-[#1A202C] text-[20px] font-bold mt-5">
                          Category: {product?.product?.category?.categoryName}
                        </h3> */}
                      </div>
                    </div>
                  </>
                )
              })
            }
            <div className="mt-5 p-5">
              <div className="flex justify-end">
                <span className="text-[16px] font-bold text-secondary" >Beskatta: </span> <span className="text-[16px] font-bold ml-4 text-secondary">{fixedTotalAmount?.totalVATDetailsDTO?.overAllVATAmount} SEK </span>
              </div>
              <div className="flex justify-end">
                <span className="text-[16px] font-bold text-secondary">Total : </span>   <span className="text-[16px] font-bold ml-4 text-secondary">{fixedTotalAmount?.totalVATDetailsDTO?.overAllTotalAmount} SEK </span>
              </div>
            </div>
          </div>
        </div>
      </div>}
    </div>
  );
};

export default Payment;
