import React, { useEffect, useState } from "react";
import {
  PaymentElement,
  LinkAuthenticationElement,
  useStripe,
  useElements,
} from "@stripe/react-stripe-js";
import { failureNotify, successNotify } from "../../../Components/Toast/Toast";
import { useNavigate } from "react-router-dom";
import { PaymentIntent } from "@stripe/stripe-js/types/api";
import { commonAxios } from "../../../Axios/service";
import { useDispatch, useSelector } from "react-redux";
import { getCartListCount } from "../../../Redux/features/cartCountSlice";
import { RootState } from "../../../Redux/store";

export default function CheckoutForm(props: any) {
  const stripe = useStripe();
  const elements = useElements();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [clientSecret, setClientSecret] = useState(
    new URLSearchParams(window.location.search).get(
      "payment_intent_client_secret"
    ) ?? ""
  );

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!stripe) {
      return;
    }

    const clientSecret =
      new URLSearchParams(window.location.search).get(
        "payment_intent_client_secret"
      ) ?? "";

    if (!clientSecret) {
      return;
    }
    // maintain proper state and set clientSecret value into retrievePaymentIntent .
    stripe
      .retrievePaymentIntent(
        "pi_3N6IBPJikOheBMh40tYkINOb_secret_DPgDKlOBja0uUmVm4OZJButga"
      )
      .then(({ paymentIntent }) => {
        localStorage.setItem("paymentIntent", JSON.stringify(paymentIntent));
        switch (paymentIntent?.status) {
          case "succeeded":
            successNotify("Payment succeeded!");
            break;
          case "processing":
            break;
          case "requires_payment_method":
            failureNotify("Your payment was not successful, please try again.");
            break;
          default:
            failureNotify("Something went wrong.");
            break;
        }
      });
  }, [stripe]);

  const postPaymentDetails = (paymentIntent: PaymentIntent) => {
    let itemIds = JSON.parse(localStorage.getItem("itemIds")!);
    const cartListDetails = JSON.parse(
      localStorage.getItem("cartListDetails")!
    );

    if(cartListDetails){
      const body = {
        paymentId: paymentIntent.id,
        paymentType: "Card",
        amount: paymentIntent.amount,
        paymentMethod: paymentIntent.payment_method,
        paymentStatus: "succeeded",
        bookingItemId: itemIds,
        curreny: "sek",
        ...props.momsDetails,
        ...cartListDetails,
      };
      commonAxios
      .post("/cemo/payments", body)
      .then((data) => {
        localStorage.removeItem("itemIds");
        localStorage.removeItem("paymentIntent");
        localStorage.removeItem("totalPrice");
        localStorage.removeItem("cartListDetails");
        localStorage.removeItem("cartSupplier");
        dispatch(getCartListCount(userInfo.id))
        if(props.functionIdentifier === "myFunction"){
          props.acceptStatusCall();
        }
        navigate("/PaymentSuccess", { state: { ...props.eventData } });
      })
      .catch((error: any) => {
        failureNotify("oops! Something went wrong, payment not successful");
      });
    }else {
    const body = {
      paymentId: paymentIntent.id,
      paymentType: "Card",
      amount: paymentIntent.amount,
      paymentMethod: paymentIntent.payment_method,
      paymentStatus: "succeeded",
      bookingId: props.bookingData?.booking?.id,
      curreny: "sek",
      bookingItemId: itemIds,
     
        ...props.momsDetails,
        // ...cartListDetails,
      };

    commonAxios
      .post("/cemo/payments", body)
      .then((data) => {
        if (data.status === 201) {
        localStorage.removeItem("itemIds");
        localStorage.removeItem("paymentIntent");
        localStorage.removeItem("totalPrice");
        localStorage.removeItem("cartListDetails");
        localStorage.removeItem("cartSupplier");
        console.log(props.functionIdentifier,"props.functionIdentifier")
        if(props.functionIdentifier === "myFunction"){
          console.log(props.acceptStatusCall,"props.acceptStatusCall")
          props.acceptStatusCall();
        }
        navigate("/PaymentSuccess", { state: { ...props.eventData } });
      } else {
        // Request was not successful, show an error message
        failureNotify("Invalid payment");
      }
    })
    .catch((error) => {
      // Handle other errors, such as network issues or server errors
      failureNotify("Oops! Something went wrong, Invalid payment");
    });
}
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (!stripe || !elements) {
      // Stripe.js hasn't yet loaded.
      // Make sure to disable form submission until Stripe.js has loaded.
      return;
    }

    setIsLoading(true);

    const { error, paymentIntent } = await stripe.confirmPayment({
      elements,
      redirect: "if_required",
      confirmParams: {
        return_url: "",
      },
    });

    // This point will only be reached if there is an immediate error when
    // confirming the payment. Otherwise, your customer will be redirected to
    // your `return_url`. For some payment methods like iDEAL, your customer will
    // be redirected to an intermediate site first to authorize the payment, then
    // redirected to the `return_url`.
    if (error?.type === "card_error" || error?.type === "validation_error") {
      failureNotify(
        error?.message?.toLocaleLowerCase() ?? "please fill all required values"
      );
    }

    if (!error) {
      localStorage.setItem("paymentIntent", JSON.stringify(paymentIntent));
      // successNotify("Payment successful");
      postPaymentDetails(paymentIntent);
    }

    setIsLoading(false);
  };

  const paymentElementOptions = {
    layout: undefined,
  };

  return (
    <form
      className="md:w-96 border rounded-md p-12"
      id="payment-form"
      onSubmit={handleSubmit}
    >
      {/* <LinkAuthenticationElement
        id="link-authentication-element"
        onChange={(e: any) => setEmail(e.target.value)}
      /> */}
      <PaymentElement id="payment-element" options={paymentElementOptions} />
      <div className="flex justify-center">
        <button
          className="w-auto mt-8 bg-[#D6B27D] p-2 px-6 font-semibold rounded-[10px] text-[#F7FAFC]"
          disabled={isLoading || !stripe || !elements}
          id="submit"
        >
          <span id="button-text">
            {`Pay Now ( ${props.totalPrice
              ? props.totalPrice
              : localStorage.getItem("totalPrice")
              } SEK )`}
          </span>
        </button>
      </div>
    </form>
  );
}
