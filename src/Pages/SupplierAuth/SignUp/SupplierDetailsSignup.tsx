import React from "react";
import Heading from "../../../assets/blixa-black.png";
import username from "../../../assets/username.png";
import personal from "../../../assets/personal.svg";
import lastname from "../../../assets/lastname.png";
import postnummer from "../../../assets/telefonnumer.png";
import city from "../../../assets/epost.png";
import chooseCategory from "../../../assets/Vector.png";
import addressline from "../../../assets/address.png";
import line from "../../../assets/Line 1.png";
import SupplierNav from "../../../Components/Navbar/SupplierNav";

const SupplierDetailsSignup = () => {
  return (
    <div className="BusinessDetailsSignup">
      {/* <SupplierNav role={"Supplier"} /> */}
      <div className="bg-[#FFE5D8] h-[50vh]">
        <div className="p-4 flex justify-center items-center pt-10 w-80">
          <img
            className="mb-5 ml-4 pl-0 w-full"
            src={Heading}
            loading="eager"
          ></img>
        </div>
        <div className="flex justify-center items-center relative mt-24">
          <img className="ml-8  pl-0 " src={personal} loading="eager"></img>
          <h1 className="text-2xl ml-2 font-bold text-[#D6B37D]">
            Personlig information
          </h1>
          <img className=" ml-14 pl-0" src={line} loading="eager"></img>

          <img className=" ml-1 pl-0" src={personal} loading="eager"></img>
          <h1 className="text-2xl ml-2 font-bold  text-[#92929D] mx-10">
            Företagsuppgifter
          </h1>
        </div>
      </div>
      <div className="flex justify-center items-center  py-2 -mt-20">
        <form className="md:px-10 lg:px-20 w-[350px] sm:w-[80%] md:w-[30%] h-[800px] shadow-2xl lg:-mt-[100px]  border bg-[#FFFFFF] rounded-[30px]  p-6">
          <div className="text-[#14px] text-[#B5B5BE] mt-5 text-center uppercase">
            Kom igång
          </div>
          <div className="font-semibold text-[24px] text-secondary mt-5 text-center">
            Företagsinformation
          </div>
          <div className="mt-2 flex items-center">
            <img className=" mb-3 " src={username} loading="eager" />
            <input
              type="text"
              className="border-b-2 px-4 py-2 border-[#D6B37D] placeholder-[#D8B37  outline-none leading-tight focus:shadow-outline mb-2 text-2xl "
              placeholder="Företagsnamn"
              autoComplete="off"
            />
            <img className=" mb-3 " src={lastname} loading="eager" />
            <input
              type="text"
              className="border-b-2 py-3 px-2 border-[#B5B5BE]  placeholder-[#B5B5BE] sm:w-1/2 md:w-full outline-none leading-tight mb-4 mt-4 text-2xl "
              placeholder="Organisationsnummer"
            />
          </div>
          <div className="mt-2 flex items-center">
            <img className=" mb-3 " src={addressline} loading="eager" />
            <input
              type="text"
              className="border-b-2 py-2 px-1 border-[#B5B5BE]  placeholder-[#B5B5BE] sm:w-1/2 md:w-full outline-none leading-tight mb-4 mt-4 text-2xl "
              placeholder="Adress line 1"
              autoComplete="off"
            />
          </div>
          <div className="mt-2 flex items-center">
            <img className=" mb-3 " src={postnummer} loading="eager" />
            <input
              className="border-b-2 border-[#B5B5BE] py-3 px-2 text-[#B5B5BE]  text-[16px]placeholder-[#B5B5BE] md:w-full  outline-none leading-tight  mb-2 text-2xl "
              placeholder="Postnummer "
              autoComplete="off"
            />
          </div>
          <div className="mt-2 flex items-center">
            <img className=" mb-3 " src={city} loading="eager" />
            <input
              className="border-b-2 border-[#B5B5BE] py-3 px-2  text-[#B5B5BE]  text-[16px]placeholder-[#B5B5BE] md:w-full   outline-none leading-tight  mb-2 text-2xl  "
              placeholder="City"
              autoComplete="off"
            />
          </div>
          <div className="mt-2 flex items-center">
            <img className=" mb-3 " src={chooseCategory} loading="eager" />
            <input
              className="border-b-2 border-[#B5B5BE] py-3 px-2  text-[#B5B5BE]  text-[16px]placeholder-[#B5B5BE]  md:w-full  outline-none leading-tight  mb-2 text-2xl  "
              placeholder="Välj kategori"
              autoComplete="off"
            />
          </div>
          <div className=" flex items-center justify-center ">
            <button
              className="bg-[#D6B37D] text-white mb-10 mt-24 font-bold border rounded-lg py-4 px-5  w-[500px] leading-tight outline-none focus:shadow-outline text-2xl uppercase"
              type="button"
            >
              Lägg till
            </button>
          </div>
          {/* <div className="flex items-center justify-center ">
            <div className="ppearance-none text-[#12] text-[#92929D] ">
              By clicking "Get Started - Free!" I agree to Creative Terms of
              Service
            </div>
          </div> */}
          <div className="font-[#regular] text-[#12] text-[#D6B37D] mt-3 text-center">
            Har du redan ett leverantörskonto? Logga in nu
          </div>
        </form>
      </div>
    </div>
  );
};

export default SupplierDetailsSignup;
