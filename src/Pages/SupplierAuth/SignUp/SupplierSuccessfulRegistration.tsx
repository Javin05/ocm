import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import Footer from "../../../Components/Footer/Footer";
import Header from "../../../Components/Header/Header";

const SupplierSuccessfullRegistration = () => {
  const navigate = useNavigate();
  const goToLandingPage = () => {
    navigate("/");
  }
  const goToOderpage = () => {
    navigate("/ordershistory");
  }

  useEffect(() => {
    localStorage.removeItem('cartListDetails')
  }, [])

  return (
    <>
      <Header />
      <div className="flex relative rounded-lg flex-col items-center justify-center h-[50vh]">

        <section className='flex flex-col items-center justify-center absolute z-40 bg-white bg-opacity-30 backdrop-filter backdrop-blur-lg border border-gray-300 rounded-lg p-10'>
          <div className='flex'>
            <h1 className="text-4xl md:text-3xl font-bold text-secondary mt-8">
              Registrering lyckad!
            </h1>
          </div>
          <p className="text-secondary text-2xl font-semibold my-8">
            Tack för att du registrerade dig som vår leverantör. Vi återkommer till dig inom 24 timmar.
          </p>
          <div className='flex'>
            <button
              onClick={goToLandingPage}
              className="bg-[#D6B27D] rounded-lg text-white uppercase p-2 px-6 md:px-[1.65rem] flex text-[14px] md:text-lg md:font-semibold"
            >
              Tillbaka till startsidan
            </button>
            {/* <button
            onClick={goToOderpage}
            className="bg-[#D6B27D] rounded-lg ml-16 text-white uppercase p-2 px-6 md:px-[1.65rem] flex text-[14px] md:text-lg md:font-semibold"
          >
            View orders
          </button> */}
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
};

export default SupplierSuccessfullRegistration;
