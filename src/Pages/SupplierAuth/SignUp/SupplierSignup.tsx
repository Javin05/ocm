import { useCallback, useEffect, useState } from "react";
import Heading from "../../../assets/blixa-black.png";
import username from "../../../assets/username.png";
import personal from "../../../assets/personal.svg";
import business from "../../../assets/icons8-round-24.png";
import lastname from "../../../assets/lastname.png";
import bankname from "../../../assets/city.png"
import telefonnumer from "../../../assets/mobile.png";
import epost from "../../../assets/epost.png";
import postnummer from "../../../assets/telefonnumer.png";
import city from "../../../assets/epost.png";
import lockpassword from "../../../assets/lockpassword.png";
import chooseCategory from "../../../assets/Vector.png";
import chooseCity from "../../../assets/city.png";
import addressline from "../../../assets/address.png";
import line from "../../../assets/Line 1.png";
import SupplierNav from "../../../Components/Navbar/SupplierNav";
import { useFormik } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import { commonAxios, formAxios } from "../../../Axios/service";
import { failureNotify, successNotify } from "../../../Components/Toast/Toast";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { RxCross1 } from "react-icons/rx";
import { setToken } from "../../../Utils/auth";
import { setUserDetails } from "../../../Redux/features/userSlice";

type initialPersonalInfoValues = {
  firstName: string;
  lastName: string;
  mobile: string;
  email: string;
  password: string;
};

type initialBusinessInfoValues = {
  businessName: string;
  businessNumber: string;
  businessAddress: string;
  businessPinCode: string;
  accountNumber: string;
  bankName: string;
  businessCity: string[];
  categories: string[];
};

type initialLoginValues = {
  email: string;
  password: string;
};

const SupplierSignup = () => {
  const [activeStep, setActiveStep] = useState(0);
  const [profilePic, setProfilePic] = useState("");
  const [profilePicforpost, setprofilePicforpost] = useState("");
  const [body, setBody] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [options, setOptions] = useState([{}]);
  const [cityChoose, setCityChoose] = useState([{}]);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const userNameInput = useCallback((inputElement: any) => {
    if (inputElement) {
      inputElement.focus();
    }
  }, []);
  const getEventCategories = async () => {
    const response = await commonAxios.get("/cemo/categories");

    if (response.status === 200) {
      const categoriesList = response.data?.map((category: any) => {
        return {
          value: category.categoryName,
          label: category.categoryName,
        };
      });

      setOptions(categoriesList);
    }
  };

  const getEventCities = async () => {
    const response = await commonAxios.get("/cemo/cities");

    if (response.status === 200) {
      const citiesList = response.data?.map((city: any) => {
        return {
          value: city.city,
          label: city.city,
          id: city.id,
        };
      });

      setCityChoose(citiesList);
    }
  };

  useEffect(() => {
    getEventCategories();
    getEventCities();
  }, [activeStep]);

  const handlePersonalInfoSignup = async (
    fieldValues: initialPersonalInfoValues
  ) => {
    const body = {
      emailId: fieldValues.email,
    };
    try {
      const response = await commonAxios.post(
        "/cemo/appuser/validate/email",
        body
      );
      if (response.data.isAccountAlreadyExists) {
        failureNotify("Account already exists with same Email id, try new one");
      } else {
        setActiveStep(1);
        setBody(fieldValues);
        console.log("handlePersonalInfoSignup", fieldValues);
      }
    } catch (error) {
      console.log("validateEmail", error);
    }
  };

  const handleLogin = async (values: initialLoginValues) => {
    setIsLoading(true);
    try {
      const response = await commonAxios.post("/cemo/auth/login", values);
      console.log("Login Supplier", response);
      if (response.data.role === "ROLE_CUSTOMER") {
        failureNotify(
          "Page access restricted! Login as Supplier/Admin to continue"
        );
      } else if (response.data.role === "ROLE_ADMIN") {
        failureNotify("Logga in som leverantör.");
      } else {
        successNotify("Login successful");
        dispatch(setUserDetails(response.data));
        setToken(response.data?.accessToken);
        navigate("/orderList", { replace: true });
      }
    } catch (error: any) {
      failureNotify(error.errorErrorMessage);
      navigate("/supplierSuccessfulRegistration")
    } finally {
      setIsLoading(false);
    }
  };

  const handleBusinessInfoSignup = async (fieldValues: any) => {
    console.log("handleBusinessInfoSignup", fieldValues);

    const formData = new FormData();
    formData.append(`file`, profilePicforpost);
    formData.append("folder", "user_profile");

    if (profilePicforpost) {
      const value: any = await formAxios
        .post("/cemo/upload/files", formData)
        .then(
          async (fileData) => {
            if (fileData) {
              // const categoriesString = fieldValues.categories.join(",");
              const finalBody = {
                ...body,
                ...fieldValues,
                // categories: categoriesString,
                profileImage: fileData.data.join(","),
              };
              const response = await commonAxios.post(
                "/cemo/appuser/supplier",
                finalBody
              );
              console.log("Signup Supplier", response);
              if (response.status === 201) {
                setIsLoading(false);
                // dispatch(setUserDetails(response.data));
                successNotify("Signup successful");
                handleLogin({ email: finalBody.email, password: finalBody.password })
                // navigate("/supplierlogin", { replace: true });
              } else {
                setIsLoading(false);
                failureNotify("oops!something went wrong");
              }
            }
          },
          async (error) => {
            console.log("Error occured while uploading file");
            const categoriesString = fieldValues.categories.join(",");
            const finalBody = {
              ...body,
              ...fieldValues,
              categories: categoriesString,
              profileImage: "",
            };

            const response = await commonAxios.post(
              "/cemo/appuser/supplier",
              finalBody
            );
            console.log("Signup Supplier", response);
            if (response.status === 201) {
              setIsLoading(false);
              // dispatch(setUserDetails(response.data));
              successNotify("Signup successful");
              handleLogin({ email: finalBody.email, password: finalBody.password })
              // navigate("/supplierlogin", { replace: true });
            } else {
              setIsLoading(false);
              failureNotify("oops!something went wrong");
            }
          }
        );
    } else {
      // console.log(
      //   "Creating user without Profile ic",
      //   fieldValues.categories.join(",")
      // );
      // const categoriesString = fieldValues.categories.join(",");
      const finalBody = {
        ...body,
        ...fieldValues,
        // categories: categoriesString,
        profileImage: "",
      };
      //  const categoriesValue =  finalBody.categories.join(",");
      const response = await commonAxios.post(
        "/cemo/appuser/supplier",
        finalBody
      );
      console.log("Signup Supplier", response);
      if (response.status === 201) {
        setIsLoading(false);
        // dispatch(setUserDetails(response.data));
        successNotify("Signup successful");
        handleLogin({ email: finalBody.email, password: finalBody.password })
        // navigate("/supplierlogin", { replace: true });
      } else {
        setIsLoading(false);
        failureNotify("oops!something went wrong");
      }
    }
  };

  const formikPersonalInfo = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      mobile: "",
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      firstName: Yup.string().required("Förnamn is required"),
      lastName: Yup.string().required("Efternamn is required"),
      email: Yup.string().email("Invalid email address").required("Email is required"),
      mobile: Yup.string()
        // .transform((value, originalValue) => {
        //   // Remove spaces from the phone number
        //   return originalValue.replace(/\s/g, "");
        // })
        // .matches(/^\+46\d{9}$/, "Invalid phone number, alike (+46 xx xxx xxxx)")
        .required("Telefonummer is required"),
      password: Yup.string()
        .required("Password is required")
        .min(6, "Password must be at least 6 characters long")
        .matches(
          /^(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9!@#$%^&*()-_+=]+$/,
          "Password must contain at least one uppercase, one lowercase, and one numeric character"
        ),
    }),

    onSubmit: (values) => handlePersonalInfoSignup(values),
    validateOnBlur: true,         // Trigger validation on blur
    validateOnChange: false,
  });

  const formikBusinessInfo = useFormik({
    initialValues: {
      businessName: "",
      businessNumber: "",
      businessAddress: "",
      businessPinCode: "",
      accountNumber: "",
      bankName: "",
      businessCity: "",
      // categories: [""],
    },
    validationSchema: Yup.object({
      businessName: Yup.string().required("Företagsnamn is required"),
      businessNumber: Yup.string().required("Organization number is required"),
      businessAddress: Yup.string().required("Address is required"),
      businessPinCode: Yup.string()
        // .matches(/^\d{5}$/, 'Invalid Post Number')
        .required("Postnumber is required"),
      businessCity: Yup.string().required("Företagets stad is required"),
      // categories: Yup.array()
      //   .min(1, "Please select at least one option")
      //   .required("Atleast One catgory is required"),
    }),
    onSubmit: (values) => handleBusinessInfoSignup(values),
    // onSubmit: (values) => console.log(values, "values"),
    validateOnBlur: true,         // Trigger validation on blur
    validateOnChange: false,
  });

  console.log(formikBusinessInfo.errors, "formikPersonalInfo.errors")

  const handleSelectChange = (selectedOptions: any) => {
    const selectedValues = selectedOptions.map((option: any) => option.value);
    const FieldValues = formikBusinessInfo.setFieldValue("categories", selectedValues);

  };

  const handleSelectCityChange = (selectedCityOptions: any) => {
    const selectedCities = selectedCityOptions.map((option: any) => option.value);
    const FieldValue = formikBusinessInfo.setFieldValue("businessCity", selectedCities.join(", "));

  };
  const handleChange = (e: any) => {
    e.preventDefault();
    const file = e.target.files[0];

    if (file) {
      // Check the file type
      if (file.type === 'image/jpeg' || file.type === 'image/jpg' || file.type === 'image/png' || file.type === 'image/gif') {
        setprofilePicforpost(file);
        const reader = new FileReader();
        reader.onloadend = () => {
          const base64String: any = reader.result;
          setProfilePic(base64String);
        };
        reader.readAsDataURL(file);
      } else {
        // Notify the user that the file format is not supported
        failureNotify('Please upload a valid image file (jpg, jpeg, png, gif).');
      }
    }
  };


  const renderPersonalInfo = () => {
    return (
      <>
        <div className="bg-[#FFE5D8] h-1/2 pb-10">
          <div className="p-4 flex justify-center items-center pt-5">
            <img
              className="mb-5 ml-4 pl-0 mt-2 w-[180px] cursor-pointer"
              src={Heading}
              onClick={() => navigate("/")}
              loading="eager"
            ></img>
          </div>
          <div className="flex-col sm:flex sm:flex-row justify-center items-center relative">
            <div className="flex justify-center items-center">
              <img className="sm:ml-8 pl-0 w-6 sm:w-8" src={personal} loading="eager"></img>
              <h1
                className="sm:text-2xl text-md ml-2 font-bold text-[#D6B37D] "
                onClick={() => setActiveStep(0)}
              >
                Personlig information
              </h1>
            </div>
            <div className="flex justify-center items-center mt-2 sm:mt-0">
              <img className="mx-4 pl-0 " src={line} loading="eager"></img></div>
            <div className="flex justify-center items-center mt-4 sm:mt-0">

              <img className=" sm:ml-1 pl-0 w-6 sm:w-10" src={business} loading="eager"></img>
              <h1
                className="sm:text-2xl text-md sm:ml-2 font-bold sm:mx-10 mx-2 text-[#B5B5BE]"
                onClick={() => setActiveStep(1)}
              >
                Företagsuppgifter
              </h1>
            </div>
          </div>
        </div>
        <div className="flex justify-center items-center pt-2 ">
          <form
            onSubmit={formikPersonalInfo.handleSubmit}
            className="md:px-10 w-[350px] sm:w-[80%] md:w-[60%] xl:w-[35%] h-1/2 shadow-2xl border bg-[#FFFFFF] rounded-[30px] p-6 mt-10"
          >
            <div className="text-[#14px] text-[#B5B5BE] mt-5 text-center uppercase">
              Kom igång
            </div>
            <div className="font-semibold text-[24px] text-secondary mt-5 text-center">
              Personlig information
            </div>
            <div className="mt-3 flex flex-col">
              <section className="flex flex-row items-center">
                <img className=" -ml-1" src={lastname} loading="eager" />
                <input
                  type="text"
                  className="border-b-2 py-3 px-2 border-[#B5B5BE] text-[16px]  text-[#000000] placeholder-[#B5B5BE] sm:w-1/2 md:w-full focus:outline-none leading-tight mb-2 mt-4 text-2xl"
                  placeholder="Förnamn"
                  name="firstName"
                  onChange={formikPersonalInfo.handleChange}
                  onBlur={formikPersonalInfo.handleBlur}
                  // ref={userNameInput}
                  value={formikPersonalInfo.values.firstName}
                  autoComplete="off"
                />
              </section>
              {formikPersonalInfo.touched.firstName &&
                formikPersonalInfo.errors.firstName ? (
                <div className="text-red-500 font-medium">
                  {formikPersonalInfo.errors.firstName}
                </div>
              ) : null}
            </div>
            <div className="mt-3 flex flex-col">
              <section className="flex flex-row items-center">
                <img className="-ml-1" src={lastname} loading="eager" />
                <input
                  type="text"
                  className="border-b-2 py-3 px-2 border-[#B5B5BE] text-[16px]  text-[#000000] placeholder-[#B5B5BE] md:w-full  focus:outline-none leading-tight  mb-2 text-2xl "
                  placeholder="Efternamn"
                  name="lastName"
                  onChange={formikPersonalInfo.handleChange}
                  onBlur={formikPersonalInfo.handleBlur}
                  value={formikPersonalInfo.values.lastName}
                  autoComplete="off"
                />
              </section>
              {formikPersonalInfo.touched.lastName &&
                formikPersonalInfo.errors.lastName ? (
                <div className="text-red-500 font-medium">
                  {formikPersonalInfo.errors.lastName}
                </div>
              ) : null}
            </div>
            <div className="mb-3 flex flex-col">
              <section className="flex flex-row items-center">
                <img className=" mb-2 -ml-1.5 " src={telefonnumer} loading="eager" />
                <input
                  type="text"
                  className="border-b-2 border-[#B5B5BE] py-3 px-2 text-[#000000] text-[16px] placeholder-[#B5B5BE]  md:w-full  focus:outline-none leading-tight mb-2 text-2xl "
                  placeholder="Telefonummer"
                  name="mobile"
                  onChange={formikPersonalInfo.handleChange}
                  onBlur={formikPersonalInfo.handleBlur}
                  value={formikPersonalInfo.values.mobile}
                  autoComplete="off"
                />
              </section>

              {formikPersonalInfo.touched.mobile &&
                formikPersonalInfo.errors.mobile ? (
                <div className="text-red-500 font-medium">
                  {formikPersonalInfo.errors.mobile}
                </div>
              ) : null}
            </div>
            <div className=" flex">
              <div>
                <label
                  className="border-[1px] block w-full h-15 rounded-lg text-center text-[16px] text-[#B5B5BE] border-[#B5B5BE] my-5 p-3"
                  htmlFor="fileInput"
                  id="customLabel"
                >
                  Ladda Upp bild
                </label>
                <input
                  className="appearance-none border rounded-full w-full py-5 px-3 text-gray-900 leading-tight focus:outline-none focus:shadow-outline"
                  id="fileInput"
                  name="profilepic"
                  type="file"
                  accept=".jpg, .jpeg, .png, .gif"
                  onChange={(e) => handleChange(e)}
                  autoComplete="off"
                />
              </div>
              {profilePic && (
                <div className="relative">
                  <img src={profilePic} width={"200px"} height={"50px"} />
                  <RxCross1
                    className="absolute top-2 right-2 bg-[white] cursor-pointer"
                    onClick={() => setProfilePic("")}
                    style={{ color: '#A09792' }}

                  />
                </div>
              )}
            </div>
            <div className=" flex flex-col">
              <section className="flex flex-row items-center">
                <img className="" src={epost} loading="eager" />
                <input
                  type="email"
                  className="border-b-2 border-[#B5B5BE] py-3 px-2 text-[#000000]  text-[16px] placeholder-[#B5B5BE] md:w-full focus:outline-none leading-tight mb-2 text-2xl lowercase"
                  placeholder="E-Post"
                  name="email"
                  onChange={formikPersonalInfo.handleChange}
                  onBlur={formikPersonalInfo.handleBlur}
                  value={formikPersonalInfo.values.email}
                  autoComplete="off"
                />
              </section>
              {formikPersonalInfo.touched.email &&
                formikPersonalInfo.errors.email ? (
                <div className="text-red-500 font-medium">
                  {formikPersonalInfo.errors.email}
                </div>
              ) : null}
            </div>
            <div className="mb-3 flex flex-col">
              <section className="flex flex-row items-center">
                <img className=" mb-2 " src={lockpassword} loading="eager" />
                <input
                  type="password"
                  className="border-b-2 border-[#B5B5BE] py-3 px-2 text-[#000000] text-[16px] placeholder-[#B5B5BE]  md:w-full  focus:outline-none leading-tight mb-2 text-2xl "
                  placeholder="Lösenord"
                  name="password"
                  onChange={formikPersonalInfo.handleChange}
                  onBlur={formikPersonalInfo.handleBlur}
                  value={formikPersonalInfo.values.password}
                  autoComplete="off"
                />
              </section>

              {formikPersonalInfo.touched.password &&
                formikPersonalInfo.errors.password ? (
                <div className="text-red-500 font-medium">
                  {formikPersonalInfo.errors.password}
                </div>
              ) : null}
            </div>
            <div className="flex items-center justify-center">
              <button
                className="bg-[#D6B37D] text-white mb-10 mt-6 font-bold border rounded-lg p-2 px-6 w-auto leading-tight outline-none focus:shadow-outline text-2xl uppercase"
                type="submit"

              >
                Next
              </button>
            </div>
            {/* <div className="flex items-center justify-center">
              <div className="appearance-none text-[#12] text-[#92929D] ">
                By clicking "Get Started - Free!" I agree to Creative Terms of
                Service
              </div>
            </div> */}
            <div
              onClick={() => navigate("/supplierlogin")}
              className="font-[#regular] text-[#12] text-[#D6B37D] mt-3 text-center cursor-pointer"
            >
              Har du redan ett leverantörskonto? Logga in nu
            </div>
          </form>

        </div>
        <div className="text-center py-10">
          <span className="text-[14px] text-[#92929D] ">&copy; 2024 Blixa</span>
        </div>
      </>
    );
  };

  const renderBusinessInfo = () => {
    return (
      <>
        <div className="bg-[#FFE5D8] h-1/2 pb-10">
          <div className="p-4 flex justify-center items-center">
            <img
              className="mb-5 ml-4 pl-0 mt-2 w-[180px] cursor-pointer"
              src={Heading}
              onClick={() => navigate("/")}
              loading="eager"
            ></img>
          </div>
          <div className="flex-col sm:flex sm:flex-row justify-center items-center relative">
            <div className="flex justify-center items-center">
              <img className="sm:ml-8 pl-0 w-6 mx-2 sm:w-8" src={personal} loading="eager"></img>
              <h1
                className="sm:text-2xl text-md sm:ml-2 font-bold text-[#D6B37D]"
                onClick={() => setActiveStep(0)}
              >
                Personlig information
              </h1>
            </div>
            <div className="flex justify-center items-center mt-2 sm:mt-0">
              <img className="mx-4 pl-0" src={line} loading="eager"></img>
            </div>
            <div className="flex justify-center items-center mt-4 sm:mt-0">


              <img className="sm:ml-1 pl-0 w-6 sm:w-8" src={personal} loading="eager"></img>
              <h1
                className="sm:text-2xl text-md font-bold text-[#D6B37D] sm:mx-4"
                onClick={() => setActiveStep(1)}
              >
                Företagsuppgifter
              </h1>
            </div>
          </div>
        </div>
        <div className="flex justify-center items-center py-1">
          <form
            onSubmit={formikBusinessInfo.handleSubmit}
            className="md:px-10 w-[350px] sm:w-[80%] md:w-[60%] xl:w-[35%] h-1/2 shadow-2xl border bg-[#FFFFFF] rounded-[30px] px-5 mt-10"
          >
            <div className="text-[14px] text-[#B5B5BE] mt-5 text-center uppercase">
              Kom igång
            </div>
            <div className="font-semibold text-[24px] text-secondary mt-5 text-center">
              Företagsinformation
            </div>
            <div>
              <section className="flex items-center mt-4 sm:mt-0">
                <img className="p-1" src={lastname} loading="eager" />
                <input
                  type="text"
                  className="border-b-2 py-2 px-1 border-[#B5B5BE]  placeholder-[#B5B5BE] text-[#000000] text-[16px] sm:w-1/2 md:w-full outline-none leading-tight mb-4 mt-4 text-2xl "
                  placeholder="Företagsnamn"
                  name="businessName"
                  onChange={formikBusinessInfo.handleChange}
                  onBlur={formikBusinessInfo.handleBlur}
                  value={formikBusinessInfo.values.businessName}
                />
              </section>
            </div>
            <div>
              {formikBusinessInfo.touched.businessName &&
                formikBusinessInfo.errors.businessName ? (
                <div className="text-red-500 px-6 font-medium">
                  {formikBusinessInfo.errors.businessName}
                </div>
              ) : null}
            </div>

            <div>
              <section className="flex items-center mt-4 sm:mt-0">
                <img className="p-1" src={lastname} loading="eager" />
                <input
                  type="text"
                  className="border-b-2 py-2 px-1 border-[#B5B5BE]  placeholder-[#B5B5BE] text-[#000000] text-[16px] sm:w-1/2 md:w-full outline-none leading-tight mb-4 mt-4 text-2xl "
                  placeholder="Organisationsnummer"
                  name="businessNumber"
                  onChange={formikBusinessInfo.handleChange}
                  onBlur={formikBusinessInfo.handleBlur}
                  value={formikBusinessInfo.values.businessNumber}
                />
              </section>
            </div>
            <div className="flex">
              {formikBusinessInfo.touched.businessNumber &&
                formikBusinessInfo.errors.businessNumber ? (
                <div
                  className={
                    !formikBusinessInfo.errors.businessName
                      ? "text-red-500 px-6 font-medium"
                      : "text-red-500 px-6 font-medium"
                  }
                >
                  {formikBusinessInfo.errors.businessNumber}
                </div>
              ) : null}
            </div>

            <div className="mt-2 flex items-center">
              <img className="p-1" src={addressline} loading="eager" />
              <input
                type="text"
                className="border-b-2 py-2 px-1 border-[#B5B5BE]  placeholder-[#B5B5BE] text-[#000000] text-[16px] sm:w-1/2 md:w-full outline-none leading-tight mb-4 mt-3 text-2xl "
                placeholder="Adress"
                name="businessAddress"
                onChange={formikBusinessInfo.handleChange}
                onBlur={formikBusinessInfo.handleBlur}
                value={formikBusinessInfo.values.businessAddress}
              />
            </div>
            {formikBusinessInfo.touched.businessAddress &&
              formikBusinessInfo.errors.businessAddress ? (
              <div className="text-red-500 px-6 font-medium">
                {formikBusinessInfo.errors.businessAddress}
              </div>
            ) : null}

            <div className="mt-2 flex items-center">
              <img className="mb-1 h-7 w-7 p-1" src={chooseCity} loading="eager" />
              <Select
                options={cityChoose}
                className="py-3 -px-1 text-[#000000] custom-placeholder text-[16px] md:w-full outline-none leading-tight text-2xl"
                placeholder={"Välj städer"}
                value={cityChoose.filter((option: any) =>
                  formikBusinessInfo.values.businessCity.includes(option.value)
                )}
                onChange={handleSelectCityChange}
                isMulti={true}
              />
            </div>

            <div className="mt-2 flex items-center">
              <img className="mb-1 p-1" src={postnummer} loading="eager" />
              <input
                type="number"
                className="border-b-2 border-[#B5B5BE] py-3 px-2 text-[#000000]  placeholder-[#B5B5BE] text-[16px] md:w-full  outline-none leading-tight  mb-2 text-2xl "
                placeholder="Postnummer"
                name="businessPinCode"
                onChange={formikBusinessInfo.handleChange}
                onBlur={formikBusinessInfo.handleBlur}
                value={formikBusinessInfo.values.businessPinCode}
              />
            </div>
            {formikBusinessInfo.touched.businessPinCode &&
              formikBusinessInfo.errors.businessPinCode ? (
              <div className="text-red-500 px-6 font-medium">
                {formikBusinessInfo.errors.businessPinCode}
              </div>
            ) : null}

            <div className="mt-2 flex items-center">
              <img className="mb-1 p-1" src={lastname} loading="eager" />
              <input
                type="number"
                className="border-b-2 border-[#B5B5BE] py-3 px-2 text-[#000000]  placeholder-[#B5B5BE] text-[16px] md:w-full  outline-none leading-tight  mb-2 text-2xl "
                placeholder="Kontonummer"
                name="accountNumber"
                onChange={formikBusinessInfo.handleChange}
                // onBlur={formikBusinessInfo.handleBlur}
                value={formikBusinessInfo.values.accountNumber}
              />
            </div>

            <div className="mt-2 flex items-center">
              <img className="mb-1 p-1" src={bankname} loading="eager" />
              <input
                type="text"
                className="border-b-2 border-[#B5B5BE] py-3 px-2 text-[#000000]  placeholder-[#B5B5BE] text-[16px] md:w-full  outline-none leading-tight  mb-2 text-2xl "
                placeholder="Bank namn"
                name="bankName"
                onChange={formikBusinessInfo.handleChange}
                // onBlur={formikBusinessInfo.handleBlur}
                value={formikBusinessInfo.values.bankName}
              />
            </div>
            {/* <div className="mt-2 flex items-center">
              <img className=" mb-0.5 " src={chooseCategory} loading="eager" />
              // Comment the below code
              <Select className="py-3 px-2 text-[#000000]  placeholder-[#B5B5BE]  md:w-full  outline-none leading-tight  mb-2 text-2xl  " placeholder="Välj kategori" name='categoryList'
                                onBlur={formikBusinessInfo.handleBlur}
                                isMulti
                                onChange={(option) => formikBusinessInfo.setFieldValue('categoryList', (option as any)?.map(({ value }: { value: string }) => value))}
                                value={options.find((option: any) => formikBusinessInfo.values.categoryList.includes(option.value))}
                                options={options} />
              //
              <Select
                options={options}
                className="py-3 px-1 text-[#000000] custom-placeholder text-[16px] md:w-full outline-none leading-tight text-2xl"
                placeholder={"Choose Categories"}
                value={options.filter((option: any) =>
                  formikBusinessInfo.values.categories.includes(option.value)
                )}
                onChange={handleSelectChange}
                isMulti={true}
              />
            </div> */}
            <div className=" flex items-center justify-center ">
              <button
                className="bg-[#D6B37D] uppercase text-white mb-10 mt-6 font-bold border rounded-lg p-2 px-6 w-auto leading-tight outline-none focus:shadow-outline text-2xl"
                type="submit"
              >

                {isLoading && (
                  <svg
                    aria-hidden="true"
                    className="inline w-5 h-5 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-[#D6B37D]"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                      fill="currentColor"
                    />
                    <path
                      d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                      fill="currentFill"
                    />
                  </svg>
                )}
                {isLoading ? '' : "Skapa konto"}
              </button>
            </div>
            {/* <div className="flex items-center justify-center ">
              <div className="ppearance-none text-[#12] text-[#92929D] ">
                By clicking "Get Started - Free!" I agree to Creative Terms of
                Service
              </div>
            </div> */}
            <div
              className="font-[#regular] text-[#12] text-[#D6B37D] pb-4 text-center cursor-pointer"
              onClick={() => navigate("/supplierlogin")}
            >
              Har du redan ett leverantörskonto? Logga in nu
            </div>
          </form>

        </div>
        <div className="text-center py-10">
          <span className="text-[14px] text-[#92929D] ">&copy; 2023 Blixa</span>
        </div>
      </>
    );
  };
  return (
    <div className="BusinessSignup">
      {/* <SupplierNav role={"Supplier"} /> */}
      {activeStep === 0 ? renderPersonalInfo() : renderBusinessInfo()}
    </div>
  );
};

export default SupplierSignup;
