import React, { useCallback, useEffect, useState } from "react";
import Heading from "../../../assets/blixa-black.png";
import SupplierNav from "../../../Components/Navbar/SupplierNav";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { commonAxios } from "../../../Axios/service";
import { successNotify, failureNotify } from "../../../Components/Toast/Toast";
import { setUserDetails } from "../../../Redux/features/userSlice";
import { setToken } from "../../../Utils/auth";
import { BsEye, BsEyeSlash } from "react-icons/bs";

type initialLoginValues = {
  email: string;
  password: string;
};

const AdminLogin = () => {
  const [pathURL, setPathURL] = useState<string>("/adminlogin");
  const [isLoading, setIsLoading] = useState(false);
  const naviagte = useNavigate();
  const dispatch = useDispatch();
  const [showPassword, setShowPassword] = useState(false);
  const emailInput = useCallback((inputElement: any) => {
    if (inputElement) {
      inputElement.focus();
    }
  }, []);

  // const handleLogin = async (values: initialLoginValues) => {
  //   setIsLoading(true);
  //   try {
  //     const response = await commonAxios.post("/cemo/auth/login", values);
  //     setIsLoading(false);
  //     if (response.data.role === "ROLE_CUSTOMER") {
  //       failureNotify(
  //         "Page access restricted! Login as Supplier/Admin to continue"
  //       );
  //     } else {
  //       successNotify("Login successful");
  //       dispatch(setUserDetails(response.data));
  //       setToken(response.data?.accessToken);
  //       naviagte("/orderList", { replace: true });
  //     }
  //   } catch (error: any) {
  //     setIsLoading(false);
  //   }
  // };

  const handleLogin = async (values: initialLoginValues) => {
    setIsLoading(true);

    console.log("api called")
    try {
      console.log("api called in try=======>")

      const response = await commonAxios.post("/cemo/auth/login", values);
      console.log("response", response)
      setIsLoading(false);
      if (response.data.role === "ROLE_CUSTOMER") {
        failureNotify(
          "Page access restricted! Login as Supplier/Admin to continue"
        );
      } else {
        successNotify("Login successful");
        dispatch(setUserDetails(response.data));
        setToken(response.data?.accessToken);
        naviagte("/orderList", { replace: true });
      }
    } catch (error: any) {
      setIsLoading(false);
      console.log("api called in catchhhh=======>")
      if (error.response && error.response.status === 401) {
        // Handle 401 Unauthorized error, for example:
        failureNotify("Unauthorized access! Please check your credentials.");
      } else {
        // Handle other types of errors
        console.error("Error:", error);
        // You can also display a generic error message here if needed
      }

    }
  };

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string().email("Invalid email address").required("Email is required"),
      password: Yup.string()
        .required("Password is required")
        .min(6, "Password should be minimum 6 characters"),
    }),
    onSubmit: (values) => handleLogin(values),
  });

  // const formik = useFormik({
  //   initialValues: {
  //     email: "",
  //     password: "",
  //   },
  //   validationSchema: Yup.object({
  //     email: Yup.string().email("Invalid email address").required("Email is required"),
  //     password: Yup.string()
  //       .required("Password is required")
  //       .min(6, "Password should be minimum 6 characters"),
  //   }),
  //   onSubmit: (values) => handleLogin(values),
  //   validateOnBlur: false,         // Trigger validation on blur
  //   validateOnChange: false,
  // });

  const handleEmailChange = (e: any) => {
    const email = e.target.value;
    const lowercaseEmail = email.toLowerCase();
    formik.handleChange(e);
    formik.setFieldValue('email', lowercaseEmail);
  };

  return (
    <div className="SupplierLogin">
      {/* <SupplierNav role={"Admin"} /> */}
      <div className="bg-[#FFE5D8] min-h-[20vh] flex justify-center items-center">
        <img
          className="ml-4 pl-0 w-52 cursor-pointer"
          src={Heading}
          onClick={() => naviagte("/")}
          loading="eager"
        ></img>
      </div>
      <div className="flex justify-center items-center pt-2 lg:mt-[50px]">
        <div className="flex justify-center items-center">
          <form
            onSubmit={formik.handleSubmit}
            className="md:px-10 lg:px-20 w-[300px] sm:w-[550px] shadow-2xl border bg-[#FFFFFF] rounded-[30px] p-6"
          >
            <div className="text-[#14px] text-[#B5B5BE] mt-5 text-center">
              LOGGA IN FÖRST
            </div>
            <div className="font-semibold text-[24px] text-secondary mt-5 mb-11 text-center">
              Kom igång
            </div>
            <div className="mb-10">
              <label className="block text-secondary font-extrabold text-[16px] mb-2">
                E-post
              </label>
              <input
                className="appearance-none border rounded-[10px] w-full py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="email"
                type="email"
                name="email"
                onChange={handleEmailChange}
                placeholder=" E-post"
                // ref={emailInput}
                onBlur={formik.handleBlur}
                value={formik.values.email}
              />
              {formik.errors.email ? (
                <span className="text-red-500 font-medium mt-2">
                  {formik.errors.email}
                </span>
              ) : null}
            </div>
            <div className="mb-10">
              <label className="block text-secondary font-extrabold text-[16px] mb-2">
                Lösenord
              </label>
              <div className="relative">
                <input
                  className="appearance-none border rounded-[10px] w-full py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="password"
                  type={showPassword ? 'text' : 'password'} // Toggle between 'text' and 'password'
                  name="password"
                  onChange={formik.handleChange}
                  placeholder="Lösenord"
                  onBlur={formik.handleBlur}
                  value={formik.values.password}
                />
                <span
                  className="absolute top-0 right-2 mt-4 mr-2 cursor-pointer"
                  onClick={togglePasswordVisibility}
                >
                  {showPassword ? <BsEye /> : <BsEyeSlash />}
                </span>
              </div>
              {formik.errors.password ? (
                <span className="text-red-500 font-medium mt-2">
                  {formik.errors.password}
                </span>
              ) : null}
            </div>
            <div className="flex items-center justify-between mb-6">
              <section>
                <input
                  className="mr-2 leading-tight text-2xl text-[#696974]"
                  type="checkbox"
                  id="remember"
                />
                <label className="text-sm w-36 text-secondary">Kom ihåg mig</label>
              </section>
              <button
                className="appearance-none py-3 text-[#D6B37D] focus:shadow-outline text-1xl uppercase"
                type="button"
                onClick={() => {
                  naviagte(
                    {
                      pathname: "/forgotpassword",
                    },
                    {
                      state: { pathURL },
                    }
                  );
                }}
              >
                Glömt lösenord?
              </button>
            </div>
            <div className="flex items-center justify-center">
              <button
                className="bg-[#D6B37D] text-lg text-white font-bold border rounded-lg p-2 px-6 w-auto leading-tight focus:outline-none focus:shadow-outline uppercase"
                type="submit"
              >
                {isLoading && (
                  <svg
                    aria-hidden="true"
                    className="inline w-5 h-5 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-[#D6B37D]"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                      fill="currentColor"
                    />
                    <path
                      d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                      fill="currentFill"
                    />
                  </svg>
                )}
                {isLoading ? '' : "Logga in"}
              </button>

            </div>

          </form>

        </div>
      </div>
      <div className="text-center py-10">
        <span className="text-[14px] text-[#92929D] ">&copy; 2024 Blixa</span>
      </div>
    </div>
  );
};

export default AdminLogin;
