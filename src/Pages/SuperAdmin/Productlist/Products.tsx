import React, { useEffect, useState } from "react";
import AdminHeader from "../../../Components/AdminHeader/AdminHeader";
import GridTable from "../../../Components/GridTable/GridTable";
import SideNav from "../../../Components/SideNav/SideNav";
import searchIcon from "../../../assets/adminSearch.svg";
import eyeIcon from "../../../assets/viewIcon.svg";
import deleteIcon from "../../../assets/Delete.png";
import eventdeleteicon from "../../../assets/deleteIcon.svg";
import editIcon from "../../../assets/editIcon.svg";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { commonAxios, formAxios } from "../../../Axios/service";
import { successNotify, failureNotify } from "../../../Components/Toast/Toast";
import { setUserDetails } from "../../../Redux/features/userSlice";
import { RootState } from "../../../Redux/store";
import { setToken } from "../../../Utils/auth";
import { FormikContext, useFormik } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import Loader from "../../../Components/Loader/Loader";
import addProductsModal from "../../../Components/AddProductsModal/AddProductsModal";
import Imagepicker from "../../../Components/Imagepicker/Imagepicker";
import ImageShown from "../../../Components/Imagepicker/ImageShown";
import AddProducts from "../../../Components/AddProduct/Addproducts";

type initialProductValues = {
  id: string;
  name: string;
  description: string;
  price: string;
  category: string;
  files: string[];
};

const Products = () => {
  const [rowData, setRowData] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isAddProductModalOpen, setIsAddProductModalOpen] = useState(false);
  const [isActionModalOpen, setIsActionModalOpen] = useState(false);
  const [isDeleteEventModalOpen, setIsDeleteEventModalOpen] = useState(false);
  const [customerData, setCustomerData] = useState<any>([]);
  const [productsData, setproductsData] = useState<any>([]);
  const [productsList, setproductsList] = useState([]);
  const [filterData, setFilterData] = useState("");
  const [addEventModalHeader, setAddEventModalHeader] =
    useState("Add New Event");
  const [imagelist, setImagelist] = useState<any>([]);
  const [imagevalue, setimagevalue] = useState<any>([]);
  const [base64Image, setBase64Image] = useState<any>([]);
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [rowHeight, setRowHeight] = useState(50);

  const [columnDefs]: any = useState([
    { headerName: "SNI?", field: "Sno", width: 80 },
    { headerName: "Produktnamn", field: "name", width: 180 },
    { headerName: "Pris  (SEK)", field: "price", width: 130 },
    { headerName: "Företagsnamn", field: "companyName", width: 150 },
    { headerName: "Antal", field: "quantity", width: 100 },
    {
      headerName: "Kategori", field: "categories",
      cellRenderer: (params: any) => {
        const categoryNames = params.data.categories.map((category: any) => category.categoryName);
        return categoryNames.join(', ');
      },
    },
    { headerName: "moms %", field: "taxType.taxPercentage", width: 90 },
    {
      headerName: "Åtgärder",
      cellRendererFramework: (params: any) => {
        const handleButtonClick = (data: any) => {
          setCustomerData(data.data);
          setIsActionModalOpen(true);
        };
        const handleAddProduct = () => {
          setIsLoading(true);
          setproductsData(params.data);
          setIsModalOpen(true);
          setAddEventModalHeader("Save Product");
          const updatedimage = () => {
            let newObj: any = {};
            let value = params.data.image.split(",").map((val: any, i: any) => {
              return {
                id: i + 1,
                image: val,
              };
            });
            setimagevalue(value);
          };
          updatedimage();
          setIsLoading(false);
        };
        const handleEventDelete = () => {
          setproductsData(params.data);
          setIsDeleteEventModalOpen(true);
        };
        return (
          <>
            <div className="flex flex-row justify-between items-center">
              <button
                onClick={() => {
                  handleButtonClick(params);
                }}
                className="mr-2"
              >
                <img
                  src={eyeIcon}
                  className="mx-1 border-[#E2E2EA] p-1 "
                />
              </button>
              <button onClick={handleAddProduct}>
                <img
                  src={editIcon}
                  className=" mx-1 border-[#E2E2EA]  p-1 "
                />
              </button>
              <button onClick={handleEventDelete}>
                <img
                  src={eventdeleteicon}
                  className=" mx-2 border-[#E2E2EA]  p-1"
                  width={30}
                  height={30}
                />
              </button>
            </div>
          </>
        );
      },
      width: 240,
    },
  ]);

  const getEventCategories = async () => {
    const response = await commonAxios.get("/cemo/categories");

    if (response.status === 200) {
      const categoriesList = response.data.map((category: any) => {
        return {
          value: category.categoryName,
          label: category.categoryName,
          id: category.id,
        };
      });

      setOptions(categoriesList);
    }
  };

  const [isLoading, setIsLoading] = useState(false);
  const [options, setOptions] = useState<any>([{}]);
  let base64ProductImage: string | undefined;

  const gridOptions = {
    getRowHeight: function (params: any) {
      return 80;
    },
    defaultColDef: {
      cellStyle: { fontFamily: "Montserrat" },
    },
  };
  const getProductsCategories = async () => {
    const response = await commonAxios.get("/cemo/categories");

    if (response.status === 200) {
      const categoriesList = response.data.map((category: any) => {
        return {
          value: category.categoryName,
          label: category.categoryName,
          id: category.id,
        };
      });

      setOptions(categoriesList);
    }
  };

  const getFormattedDate = (timeStamp: string) => {
    const date = new Date(timeStamp);

    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString();
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");

    const formattedDate = `${day}/${month}/${year} ${hours}:${minutes}`;

    return formattedDate;
  };

  // const getMerchantDetails = async () => {
  //   setIsLoading(true);
  //   try {
  //     const response = await commonAxios.get(`/cemo/appuser/all/suppliers`);
  //
  //     if (response.data) {
  //       const gridData = response.data.filter((item: any, index: number) => {
  //         if (item.accountStatus === "ACTIVE") {
  //           return {
  //             ...item,
  //             Sno: index + 1,
  //           };
  //         }
  //       });
  //
  //       setData(gridData);
  //       let filteredCities: any = [];
  //       const cities = response.data.map((item: any) => {
  //         if (!filteredCities.includes(item.businessCity.toLowerCase())) {
  //           return filteredCities.push(item.businessCity.toLowerCase());
  //         }
  //       });

  //       const filterCities = filteredCities.map((item: any) => {
  //         return {
  //           label: item,
  //           value: item,
  //         };
  //       });

  //       setCityOptions(filterCities);
  //     }
  //   } catch (error) {
  //   } finally {
  //     setIsLoading(false);
  //   }
  // };

  const getProductsList = async () => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get("/cemo/products/all");

      setproductsList(response.data);
      setIsLoading(false);
      let count = 0;
      const gridData = response.data?.filter((item: any, index: any) => {
        if (item.status === "ACTIVE") {
          return true
        }
        return false
      }).map((item: any, index: any) => {
        count = count + 1;
        return {
          Sno: count,
          merchantName:
            item.appUser?.firstName + " " + item.appUser?.lastName,
          customerMobile: item.appUser?.mobile,
          dateTime: getFormattedDate(item.createdAt),
          ...item,
        };
      })
      setRowData(gridData);
    } catch (error) {
      setIsLoading(false);
      failureNotify("oops!something went wrong");
    }
  };

  const getProductsListBySupplier = async () => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get(
        `/cemo/products/get/${userInfo.id}`
      );
      setproductsList(response.data);

      const getFormattedDate = (timeStamp: string) => {
        const date = new Date(timeStamp);

        const day = date.getDate().toString().padStart(2, "0");
        const month = (date.getMonth() + 1).toString().padStart(2, "0");
        const year = date.getFullYear().toString();
        const hours = date.getHours().toString().padStart(2, "0");
        const minutes = date.getMinutes().toString().padStart(2, "0");

        const formattedDate = `${day}/${month}/${year} ${hours}:${minutes}`;

        return formattedDate;
      };
      setIsLoading(false);
      let count = 0;
      const gridData = response.data?.map((item: any, index: any) => {
        if (item.status === "ACTIVE") {
          count = count + 1;
          return {
            Sno: count,
            merchantName:
              item.appUser?.firstName + " " + item.appUser?.lastName,
            customerMobile: item.appUser?.mobile,
            dateTime: getFormattedDate(item.createdAt),
            ...item,
            name: item.name,

          };
        }
      });
      setRowData(gridData);
    } catch (error) {
      setIsLoading(false);
      failureNotify("oops!something went wrong");
    }
  };

  useEffect(() => {
    const handleEscPress = (event: KeyboardEvent) => {
      if (event.key === "Escape") {
        setIsModalOpen(false);
        setIsActionModalOpen(false);
        setIsAddProductModalOpen(false);
        setIsDeleteEventModalOpen(false);
        setCustomerData([]);
        setproductsData([]);
      }
    };

    document.addEventListener("keydown", handleEscPress);

    return () => {
      document.removeEventListener("keydown", handleEscPress);
    };
  }, []);

  useEffect(() => {
    if (userInfo.role === "ROLE_ADMIN") {
      getProductsList();
    } else {
      getProductsListBySupplier();
    }
    getEventCategories();
    getProductsCategories();
  }, []);

  const categoryNames = customerData?.categories?.map((item: any) => item?.categoryName);
  const formattedCategoryNames = categoryNames?.join(', ');

  const CustomerDetails = () => {
    return (
      // <section className="flex flex-col h-[45rem] items-center justify-around ">
      <div className="w-full h-full fixed top-0 left-0 flex items-center justify-center bg-[#00000080] z-40">
        <div className="bg-white w-[80%] md:w-[80%] lg:w-[60%] xl:w-[60%] max-w-[900px] max-h-[90%] overflow-y-auto p-10 relative">

          <button onClick={() => setIsActionModalOpen(false)}>
            <img
              src={deleteIcon}
              className="absolute top-5 right-5 h-6 w-6"
            />
          </button>
          <div className="grid grid-cols-1 gap-4">

            <div className="p-4">
              <h1 className="-mt-3 font-extrabold text-secondary text-xl">Produktdetaljer</h1>
              <div className="border py-1 px-1 relative rounded-lg  mt-1">
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Produktnamn</h1>
                      <h1 className="text-secondary">{customerData?.name}</h1>
                    </div>
                  </div>
                  <div className="grid grid-cols-2 gap-4">
                    <div className="p-4">
                      <div className="">
                        <h1 className="font-semibold text-secondary">Pris</h1>
                        <h1 className="text-secondary">{customerData?.price}</h1>
                      </div>
                    </div>
                  </div>
                </div>
                {/* <div className="grid grid-cols-2 gap-4">
                    <div className="p-4">
                      <div className="">
                        <h1 className="font-semibold">Start Date</h1>
                        <h1>{customerData?.startDate}</h1>
                      </div>
                    </div>
                    <div className="p-4">
                      <div className="">
                        <h1 className="font-semibold">End Date</h1>
                        <h1>{customerData?.endDate}</h1>
                      </div>
                    </div>
                  </div> */}
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Företagsnamn</h1>
                      <h1 className="text-secondary">{customerData?.companyName}</h1>
                    </div>
                  </div>
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary ">Kategori</h1>

                      {/* <h1 className="text-secondary">
                        {customerData?.category.categoryName ? (
                          customerData.category.categoryName.charAt(0).toUpperCase() +
                          customerData.category.categoryName.slice(1).toLowerCase()
                        ) : (
                          "No Category"
                        )}
                      </h1> */}
                      <h1 className="text-secondary">{formattedCategoryNames}</h1>
                    </div>
                  </div>
                </div>
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="h-20 overflow-y-auto whitespace-normal">
                      <h1 className="font-semibold text-secondary">Beskrivning</h1>
                      <h1 className="text-secondary">{customerData?.description}</h1>
                    </div>
                  </div>
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Moms Percentage</h1>
                      <h1 className="text-secondary">{customerData?.taxType.taxPercentage}</h1>
                    </div>
                  </div>
                </div>
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Product Quantity</h1>
                      <h1 className="text-secondary">{customerData?.quantity}</h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="p-4">
              <h1 className=" font-extrabold text-secondary text-xl">Leverantörsuppgifter</h1>
              <div className="border p-3 rounded-lg mt-1">
                <div className="grid grid-cols-2 gap-1">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Förnamn</h1>
                      <h1 className="text-secondary">{customerData?.appUser.firstName}</h1>
                    </div>
                  </div>
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Efternamn</h1>
                      <h1 className="text-secondary">{customerData?.appUser.lastName}</h1>
                    </div>
                  </div>
                </div>
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Telefon</h1>
                      <h1 className="text-secondary">{customerData?.appUser.telephone}</h1>
                    </div>
                  </div>
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">E-post</h1>
                      <h1 className="text-secondary">{customerData?.appUser.email}</h1>
                    </div>
                  </div>
                </div>
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Telefonnummer</h1>
                      <h1 className="text-secondary">{customerData?.appUser?.mobile}</h1>
                    </div>
                  </div>
                  <div className="p-4">
                    <h1 className="font-semibold text-secondary">Skapad den</h1>
                    <h1 className="text-secondary">
                      {getFormattedDate(customerData?.appUser.createdAt)}
                    </h1>
                  </div>
                </div>
              </div>
            </div>

            {/* {
                customerData?.booking?.bookingStatus !== 'REJECTED' ? (
                  <div className="p-4">
                    <h1 className='-mt-3 font-bold'>Booking Action</h1>
                    <div className='p-3 rounded-lg mt-1'>
                      <div className="grid grid-cols-1 gap-4">
                        <section>
                          <button onClick={() => updateOrderStatus(true)} className='px-4 py-2 bg-green-700 rounded-md text-white font-semibold cursor-pointer'>
                            Accept the order
                          </button>
                        </section>
                        <section>
                          <button onClick={() => updateOrderStatus(false)} className='px-4 py-2 bg-red-700 rounded-md text-white font-semibold cursor-pointer'>
                            Reject the order
                          </button>
                        </section>
                      </div>
                    </div>
                  </div>
                ) : null
              } */}
          </div>
        </div>

      </div>
    );
  };

  const addNewProduct = async (productsData: initialProductValues) => {
    setIsLoading(true);

    // const productBody = {
    //   id: productsData.id,
    //   name: productsData.name,
    //   description: productsData.description,
    //   quantity: productsData.quantity,
    //   price: productsData.price,
    //   categories: productsData.category,
    // };
  };

  const filterDatas = (filterData: any) => {
    const value = rowData.filter(
      (val: any) =>
        (val?.name && val.name.toLowerCase().includes(filterData.toLowerCase())) ||
        (val?.price && val.price.toString().toLowerCase().includes(filterData.toLowerCase()))
    );
    return value;
  };


  const AddProductsModal = () => {
    const addNewProduct = async (productValue: initialProductValues) => {
      setIsLoading(true);
      const response = await commonAxios.get("/cemo/categories");
      let categoryId;
      if (response.data) {
        categoryId = response.data.filter((name: any) => {
          return (
            name.categoryName.toLowerCase() ===
            productValue.category.toLowerCase()
          );
        });
      }
      let fileArray: string[] = [];
      for (let i = 0; i < imagevalue.length; i++) {
        fileArray.push(imagevalue[i]);
      }
      const formData = new FormData();
      fileArray.forEach((file: any, i: any) => {
        formData.append(`file`, file);
      });
      formData.append("folder", "products");

      const postUpdatedImageurl: any = await formAxios.post(
        "/cemo/upload/files",
        formData
      );

      if (postUpdatedImageurl.data) {
        const productBody = {
          price: Number(productValue.price),
          image: postUpdatedImageurl.data.join(","),
          appUserId: userInfo.id,
          categoryId: categoryId.map((val: any) => val.id).join(" "),
          name: productValue.name,
          description: productValue.description,

        };
        // const productBodys = {
        //   price: Number(productValue.price),
        //   image: "base64",
        //   id: productsData.id,
        //   name: productValue.name,
        //   description: productValue.description,
        //   categories: productValue.categories.join(","),
        //   quantity: Number(productValue.quantity),
        // };

        try {
          const response = await commonAxios.put(
            `/cemo/products/${productsData.id}`,
            productBody
          );
          successNotify("Product edited successfully");
          setIsAddProductModalOpen(false);
          if (userInfo.role === "ROLE_ADMIN") {
            getProductsList();
          } else {
            getProductsListBySupplier();
          }
        } catch (error) {
        } finally {
          setIsModalOpen(false);
          setIsLoading(false);
        }
      }
    };

    const handleclose = () => {
      setIsDeleteEventModalOpen(false);
      setproductsData([]);
    };

    const formik = useFormik<initialProductValues>({
      initialValues:
        addEventModalHeader === "Save Product"
          ? {
            id: productsData.id,
            name: productsData.name,
            description: productsData.description,
            price: productsData.price,
            category: productsData.category?.categoryName,
            files: [],
          }
          : {
            id: "",
            name: "",
            description: "",
            price: "",
            category: "",
            files: [],
          },
      // validationSchema: Yup.object({
      //   name: Yup.string().required("Required"),
      //   description: Yup.string().required("Required"),
      //   quantity: Yup.string().required("Required"),
      //   price: Yup.string().required("Required"),
      // }),
      onSubmit: (values) => addNewProduct(values),
    });

    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      if (event.target.files && event.target.files.length > 0) {
        const file = event.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          event.preventDefault();
          const imageUrl = reader.result?.toString();
          base64ProductImage = imageUrl;
        };
      }
    };

    const updatedValue = (value: any) => {
      setimagevalue(value);
    };

    const updatedValues = (value: any) => {
      setBase64Image(value);
    };

    const handleImageUpload = (image: any) => {
      setimagevalue(image);
    };


    return (
      <div className="w-[85%] h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <form
            onSubmit={formik.handleSubmit}
            className="bg-[#ffffff] w-[40rem] h-auto px-8 py-6 rounded-lg relative"
          >
            <button
              onClick={() => {
                setIsAddProductModalOpen(false);
                setIsModalOpen(false);
                setCustomerData([]);
                setproductsData([]);
              }}
            >
              <img
                src={deleteIcon}
                className="absolute top-5 right-5 h-6 w-6"
              />
            </button>
            <h2 className="mb-4 text-lg font-semibold text-center">
              Edit Product
            </h2>
            <input
              type="text"
              name="name"
              placeholder="Produktnamn"
              className="outline-none border-[1px] rounded-lg border-gray-300 my-2 px-2 w-full h-10 bg-transparent"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.name}
            />
            <textarea
              rows={6}
              name="description"
              id="eventDescription"
              placeholder="Produktbeskrivning"
              className="outline-none border-[1px] rounded-lg border-gray-300 my-1 px-2 pt-2 w-full bg-transparent"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.description}
            />
            <Select
              className="py-3 text-[#000000] placeholder-[#B5B5BE]  md:w-full  outline-none leading-tight mb-2 text-lg"
              placeholder="Välj kategori"
              name="category"
              onBlur={formik.handleBlur}
              onChange={(option: any) =>
                formik.setFieldValue("category", option.value.toUpperCase())
              }
              value={options.find((option: any) =>
                formik.values.category?.includes(option.value.toUpperCase())
              )}
              options={options}
            // isMulti={true}
            />
            <>
              <Imagepicker
                product={productsData}
                imagevalue={imagevalue}
                updatedValues={updatedValues}
                max={3}
                value={
                  addEventModalHeader === "Save Product" ? "updateproducts" : ""
                }
                onImageUpload={handleImageUpload}
                base64Image={base64Image}
              // updatedValue={conversionImage}
              // base64={base64Image}
              />
              <ImageShown
                base64Image={base64Image}
                imagevalue={imagevalue}
                product={productsData}
                updatedValues={updatedValues}
                updatedValue={updatedValue}
              // base64Image={base64Image}
              // updatedValue={conversionImage}
              />
            </>

            {/* <div className="flex mt-4">
              <label className="w-1/2 mt-1 font-semibold" htmlFor="eventImage">
                Upload Image:
              </label>
              <input
                type="file"
                name="image"
                id="eventImage"
                placeholder="Product Image"
                className="outline-none px-2 w-full h-10 bg-transparent"
                onChange={handleFileChange}
              />
            </div> */}
            {/* <input
              type="text"
              name="quantity"
              placeholder="Quantity"
              className="outline-none border-[1px] rounded-lg border-gray-300 my-2 px-2 w-full h-10 bg-transparent"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.quantity}
            /> */}
            <input
              type="text"
              name="price"
              placeholder="Produktpris"
              className="outline-none border-[1px] rounded-lg border-gray-300 my-2 px-2 w-full h-10 bg-transparent"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.price}
            />
            <div className="flex justify-center">
              <button
                type="submit"
                onClick={AddProductsModal}
                disabled={isLoading}
                className="px-6 p-2 w-auto py-2 md:mr-4 bg-[#D6B27D] rounded-lg mt-6 text-md font-semibold cursor-pointer text-white"
              >
                {addEventModalHeader == "Save Product"
                  ? "Save Product"
                  : "Lägg till produkt"}
              </button>
            </div>
          </form>
        </section>
      </div>
    );
  };

  const DeleteEventModal = () => {
    const handleclose = () => {
      setIsDeleteEventModalOpen(false);
      setCustomerData([]);
      setproductsData([]);
    };
    const deleteEvent = async () => {
      setIsLoading(true);
      try {
        const response = await commonAxios.delete(
          `/cemo/products/${productsData.id}`
        );

        if (response.status === 204) {
          if (userInfo.role === "ROLE_ADMIN") {
            getProductsList();
          } else {
            getProductsListBySupplier();
          }
          successNotify(`${productsData.name} product deleted successfully`);
          handleclose();
          setIsLoading(false);
        }
      } catch (error) {
        failureNotify("oops! something went wrong");
      } finally {
        setIsLoading(false);
      }
    };

    return (
      <div className="w-[85%] h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <div className="bg-white rounded-lg p-6 flex flex-col justify-center items-center w-1/3 h-72 relative">
            <button onClick={handleclose}>
              <img
                src={deleteIcon}
                className="absolute top-5 right-5 h-6 w-6"
              />
            </button>
            <p className="font-semibold text-lg text-secondary">
              Är du säker att du vill ta bort {productsData.name} produkten?
            </p>
            <section className="flex flex-row justify-between items-center gap-5 mt-10">
              <button
                onClick={deleteEvent}
                className="px-6 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer uppercase"
              >
                Ta bort
              </button>
              <button
                onClick={handleclose}
                className="px-6 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer uppercase"
              >
                Avbryt
              </button>
            </section>
          </div>
        </section>
      </div>
    );
  };

  console.log("filterData", filterData)
  return (
    <div className="flex">
      <SideNav />
      {isModalOpen && <AddProducts addEventModalHeader={addEventModalHeader}
        productsData={productsData}
        setIsAddProductModalOpen={setIsAddProductModalOpen}
        setIsLoading={setIsLoading}
        getProductsList={getProductsList}
        getProductsListBySupplier={getProductsListBySupplier}
        setIsModalOpen={setIsModalOpen}
        // seteventData={seteventData}
        setCustomerData={setCustomerData}
        isLoading={isLoading} />}
      {/* {isAddProductModalOpen ? <AddProductsModal /> : null} */}
      {isActionModalOpen ? <CustomerDetails /> : null}
      {isDeleteEventModalOpen ? <DeleteEventModal /> : null}
      {isAddProductModalOpen ? (
        <AddProducts
          addEventModalHeader={addEventModalHeader}
          productsData={productsData}
          setIsAddProductModalOpen={setIsAddProductModalOpen}
          setIsLoading={setIsLoading}
          getProductsList={getProductsList}
          getProductsListBySupplier={getProductsListBySupplier}
          setIsModalOpen={setIsModalOpen}
          // seteventData={seteventData}
          setCustomerData={setCustomerData}
          isLoading={isLoading}
        />
      ) : null}
      {isLoading && <Loader />}
      <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
        <AdminHeader />
        <div className="mx-0 my-6">
          <div className="flex flex-row items-center justify-between">
            <h1 className="text-secondary  text-[24px] font-semibold p-4">
              Produkter
            </h1>
            <button
              onClick={() => {
                setIsAddProductModalOpen(true);
                setAddEventModalHeader("");
                setproductsData([]);
              }}
              className="px-6 p-2 md:mr-4 uppercase bg-[#D6B27D] text-white rounded-xl text-lg font-semibold cursor-pointer outline-none"
            >
              Lägg till produkt
            </button>
            {/* <section>
              <button
                onClick={() => {
                  setIsAddProductModalOpen(true);
                }}
                className="px-4 py-2 md:mr-4 bg-[#FFE5D8] rounded-xl text-lg font-semibold cursor-pointer outline-none"
              >
                Lägg till produkt
              </button>
              <button
                onClick={() => {
                  setIsModalOpen(true);
                  setAddEventModalHeader('Add New Event')
                }}
                className="px-4 py-2 md:mr-4 bg-[#FFE5D8] rounded-xl text-lg font-semibold cursor-pointer outline-none"
              >
                Add Event
              </button>
            </section> */}
          </div>
          <div className=" border border-[#FFFFFF] bg-[#ffff] rounded-[20px] p-4 mt-2">
            <div className="relative w-full">
              <input
                type="text"
                className="w-full h-[38px] pl-10 pr-5 border rounded-md border-[#E2E2EA] focus:outline outline-[#E2E2EA] focus:border-transparent"
                placeholder="Sök efter namn, leverantör, mobil etc..."
                onChange={(e) => {
                  setFilterData(e.target.value);
                }}
                value={filterData}
              />
              <div className="absolute inset-y-0 left-0 flex items-center pl-3">
                <img src={searchIcon} />
              </div>
            </div>
            <div className="mt-6">
              <GridTable
                columnDefs={columnDefs}
                gridOptions={{
                  ...gridOptions,
                  getRowHeight: () => rowHeight,
                }}
                rowData={filterDatas(filterData)}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Products;
