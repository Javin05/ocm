import deleteIcon from "../../../assets/Delete.png";

const RejectSupplier = ( {setIsRejectModalOpen , rejectUser, eventData}:any) => {

  const rejectUserApi = () =>  {
    rejectUser();
  }
    return (
      <div className="w-[85%] h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <div className="bg-white rounded-lg p-6 flex flex-col justify-center items-center w-1/2 h-72 relative">
            <button onClick={() => setIsRejectModalOpen(false)}>
              <img
                src={deleteIcon}
                className="absolute top-5 right-5 h-6 w-6"
              />
            </button>
            <p className="font-semibold text-lg text-secondary" >
              Are you sure to Reject {eventData.firstName + " "+ eventData.lastName}'s Request?
            </p>
            {/* <section className="flex flex-row justify-between items-center w-[40%] mt-10">
              <button
                onClick={rejectUserApi}
                className="px-2 py-2 rounded-lg bg-red-700 text-white font-semibold text-base cursor-pointer"
              >
                REJECT
              </button>
              <button
                onClick={() => setIsRejectModalOpen(false)}
                className="px-2 py-2 rounded-lg bg-[#FFE5D8] text-secondary font-semibold text-base cursor-pointer"
              >
                DISCARD
              </button>
            </section> */}
              <section className="flex flex-row justify-between items-center w-[40%] gap-8 mt-10 mr-10">
              <button
                onClick={rejectUserApi}
                className="px-6 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer uppercase"
              >
                Nekad
              </button>
              <button
               onClick={() => setIsRejectModalOpen(false)}
                className="px-6 py-2 rounded-lg bg-[#FFE5D8] text-secondary font-semibold text-base cursor-pointer uppercase"
              >
                Avbryt
              </button>
            </section>
          </div>
        </section>
      </div>
    );
  };

  export default RejectSupplier;