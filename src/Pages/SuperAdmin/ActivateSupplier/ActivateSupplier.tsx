import { useEffect } from "react";
import AdminHeader from "../../../Components/AdminHeader/AdminHeader";
import SideNav from "../../../Components/SideNav/SideNav";
import searchIcon from "../../../assets/adminSearch.svg";
import { useState } from "react";
import GridTable from "../../../Components/GridTable/GridTable";
import Select from "react-select";
import { commonAxios } from "../../../Axios/service";
import { SiMicrosoftexcel } from "react-icons/si";
import { AsyncDownloadCSV } from "../../../Components/DownloadCsv/DownloadCSV";
import moment from "moment";
import Loader from "../../../Components/Loader/Loader";
import { IoMdCheckboxOutline } from "react-icons/io";
import { MdCancelPresentation } from "react-icons/md";
import RejectSupplier from "./RejectSupplier";
import { failureNotify, successNotify } from "../../../Components/Toast/Toast";
import { Drawer, IconButton, Tooltip } from "@material-tailwind/react";
import { RxCross1 } from "react-icons/rx";



const ActivateSupplier = () => {
  const [selectedOption, setSelectedOption]: any = useState(null);
  const [isloading, setIsLoading]: any = useState(false);
  const [filterData, setFilterData] = useState("");
  const [isRejectModalOpen, setIsRejectModalOpen] = useState<boolean>(false);
  const [accountData, setAccountData] = useState<any>(null);
  const [rowData, setData]: any = useState([]);
  const [newFilterData, setNewFilterData] = useState<any>([]);
  const [values, setValues] = useState<any>(null);
  const gridOptions = {
    getRowHeight: function (params: any) {
      return 80;
    },
  };

  const getCustomerDetails = async () => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get(`/cemo/appuser/all/suppliers`);
      if (response.data.length) {
        const filteredSupplier = response.data.filter((supplier: any) => {
          const createdAt = moment(supplier.createdAt);
          const elapsedTimeHours = moment().diff(createdAt, "hours");
          const isWithin24Hours = elapsedTimeHours < 24;

          if (!isWithin24Hours) {
            // If it's beyond 24 hours, display only the date without time
            supplier.createdAt = createdAt.format("YYYY-MM-DD h:mm a");
          } else {
            // If it's within 24 hours, display the full dateand time
            supplier.createdAt = createdAt.format("YYYY-MM-DD");
          }

          return supplier.accountStatus === "REQUESTED";
        });

        setData(filteredSupplier);
        setNewFilterData(filteredSupplier);
      }
    } catch (error) {
      console.log("Error fetching suppliers:", error);
    } finally {
      setIsLoading(false);
    }
  };


  const [columnDefs]: any = useState([
    { headerName: "Företagsnamn", field: "businessName", width: 200 },
    {
      headerName: "Kontaktperson",
      cellRendererFramework: (props: any) => {
        return (
          <>
            <div className={`${props.data?.profileImage && props.data?.profileImage != null ? `bg-white` : `bg-[#D6B27D]`}  w-10 h-10 rounded-lg flex items-center justify-center font-bold`}>
              {
                props.data?.profileImage && props.data?.profileImage != null ?
                  <img src={props.data?.profileImage} className="h-[full] w-[full] rounded-sm " /> : <span className="text-white">
                    {props.data?.firstName[0].toUpperCase() +
                      props.data?.lastName[0].toUpperCase()}
                  </span>
              }
            </div>
            <p className="ml-3">{props.data?.firstName + " " + props.data?.lastName}</p>
          </>
        );
      },
      width: 250,
    },
    { headerName: "E-Post", field: "email", width: 250 },
    { headerName: "Telefonnummer", field: "mobile", width: 140 },
    { headerName: "Företagets stad", field: "businessCity", width: 150 },
    {
      headerName: "Skapad den",
      cellRendererFramework: (props: any) => {
        const [counter, setCounter] = useState(0);
        useEffect(() => {
          const createdAtTime = moment(props?.data?.createdAt);
          const currentTime = moment();
          const differenceInHours = currentTime.diff(createdAtTime, 'hours');
          if (differenceInHours >= 0 && differenceInHours <= 24) {
            const differenceInSeconds = moment.duration(currentTime.diff(createdAtTime)).asSeconds();
            const remainingTime = 24 * 3600 - differenceInSeconds;
            console.log("remainingTime", remainingTime)
            if (remainingTime > 0) {
              setCounter(remainingTime);
              
              const timer = setTimeout(() => {
                setCounter(prevCounter => prevCounter - 1);
              }, 1000);
        
              return () => clearTimeout(timer);
            }
          }
        }, [props.data?.createdAt, setCounter]);

        const formatTime = (timeInSeconds: number): JSX.Element => {
          const duration = moment.duration(timeInSeconds, 'seconds');
          const hours = String(duration.hours()).padStart(2, '0');
          const seconds = String(duration.seconds()).padStart(2, '0');
          const style: React.CSSProperties = { color: 'red' };
        
          return (
            <span style={style}>
              {hours}:{seconds}
            </span>
          );
        };
        

        return (
          <div>
            {counter > 0 ? formatTime(counter) : props?.data?.createdAt}

            {/* {counter} */}
          </div>
        );
      },
      width: 160,
    },
    {
      headerName: "Åtgärder",
      cellRendererFramework: (params: any) => {
        const acceptSupplier = async (eventData: any) => {
          setIsLoading(true);
          await commonAxios
            .post(`/cemo/appuser/activate/supplier/` + eventData.data.id + "/ACTIVE")
            .then(
              (res) => {
                if (res.data) {
                  successNotify("Leverantörsgodkännandet lyckades!!!");
                  setIsLoading(false);
                }
              },
              (err) => {
                failureNotify("Supplier Approve failed !!!");
                setIsLoading(false);
              }
            );
          getCustomerDetails();
        }

        const rejectSupplier = () => {
          setAccountData(params.data);
          setIsRejectModalOpen(true);
        }



        return (
          <>
            <div className="flex justify-between">
              <Tooltip className='bg-[#D6B27D]' content={"Godkänn"} placement="right-start">
                <div>
                  <IoMdCheckboxOutline onClick={() => acceptSupplier(params)} className="h-[24px] w-[24px] rounded-xl text-[#42d49c] hover:text-[#277c5b] cursor-pointer" />
                </div>
              </Tooltip>
              <Tooltip className='bg-[#D6B27D]' content={"Cancel"} placement="right-start">
                <div>
                  <MdCancelPresentation onClick={() => rejectSupplier()} className="ml-3 h-[24px] w-[24px] rounded-xl text-[#FC5A5A] hover:text-[#ce4949] cursor-pointer" />
                </div>
              </Tooltip>
            </div>
          </>
        );
      },
      width: 90,
    },
  ]);

  const rejectUser = async () => {
    setIsLoading(true);
    await commonAxios
      .post(`/cemo/appuser/activate/supplier/` + accountData.id + "/INACTIVE")
      .then(
        (res) => {
          if (res.data) {
            successNotify("Supplier Rejected successfully!!!");
            // toggleEditModal();
            setIsLoading(false);
            setIsRejectModalOpen(false);
          }
        },
        (err) => {
          failureNotify("Supplier Rejection failed !!!");
          setIsLoading(false);
        }
      );
    getCustomerDetails();
  }

  useEffect(() => {
    getCustomerDetails();
  }, []);
  let customerCity: any = [];
  rowData.forEach((val: any) => {
    if (
      val.businessCity !== null &&
      !customerCity.includes(val.businessCity?.toLowerCase())
    ) {
      customerCity.push(val.businessCity.toLowerCase());
    }
  });

  const options = customerCity.map((val: any) => {
    return {
      label: val.charAt(0).toUpperCase() + val.slice(1).toLowerCase(),
      value: val,
    };
  });

  const CustomDropdownIndicator = () => (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        style={{ width: "24px", height: "24px", fill: '#A09792' }}
      >
        <path fill="none" d="M0 0h24v24H0z" />
        <path d="M7 10l5 5 5-5z" />
      </svg>
    </div>
  );

  const customComponents: any = {
    DropdownIndicator: CustomDropdownIndicator,
  };

  console.log(newFilterData, "newFilterData")

  const getCustomerList = () => {
    return newFilterData.map((data: any) => {
      return getEventDownloadInfo(data)
    });
  };

  function getEventDownloadInfo(data: any) {

    return {
      "FirstName": data.firstName,
      "LastName": data.lastName,
      "Email": data.email,
      "Telephone": data.mobile,
      // "Mobile": data.mobile,
      "Address": data.businessAddress,
      "City": data.businessCity,

    };
  }

  const handleClick = () => {
    console.log("cleared!")
    setSelectedOption(null)
    getCustomerDetails();
  }

  const filteredDatas = (filterData: any) => {
    let value = rowData.filter(
      (val: any) =>
        val?.firstName?.toLowerCase().includes(filterData.toLowerCase()) ||
        val?.mobile?.toLowerCase().includes(filterData.toLowerCase()) ||
        val?.email?.toLowerCase().includes(filterData.toLowerCase()) ||
        val?.businessCity?.toLowerCase().includes(filterData.toLowerCase())
    );
    setNewFilterData(value);
  };

  const customStyles = {
    control: (provided: any, state: any) => ({
      ...provided,
      border: "none",
      outline: "none",
      boxShadow: "none",
      borderRadius: "4px",
      fontSize: "13px",
      width: "150px",
    }),
    option: (provided: any, state: any) => ({
      ...provided,
      backgroundColor: "transparent",
      fontSize: "13px",
      width: "150px",
      color: state.isSelected ? "#D6B27D" : "gray",
    }),
    singleValue: (provided: any) => ({
      ...provided,
      color: "gray", // Set the initial text color to gray
    }),
    valueContainer: (provided: any) => ({
      ...provided,
      color: "gray", // Set the initial text color to gray
    }),
  };

  const handlecitychange = (event: any) => {
    setSelectedOption(event);
    filteredDatas(event.value);
  };
  const [rowHeight, setRowHeight] = useState(60);

  return (
    <div className="flex">
      <SideNav />
      <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
        <AdminHeader />
        {isloading && <Loader />}
        {isRejectModalOpen && <RejectSupplier setIsRejectModalOpen={setIsRejectModalOpen} rejectUser={rejectUser} eventData={accountData} />}
        <div className="mx-10 my-6">


          <div className="flex justify-between py-4">
            <h1 className="text-secondary text-[24px] font-semibold">
              Väntande konto
            </h1>
            <AsyncDownloadCSV
              data={getCustomerList}
              asyncOnClick={true}
              filename={
                "Pending-Accounts" + moment(new Date()).format("DD/MM/YYYY")
              }
              onClick={() => { }}
            >
              <SiMicrosoftexcel className="w-6 h-6 text-[#D6B27D] hover:text-[#F7CB73] hover:cursor-pointer mt-2" />
            </AsyncDownloadCSV>
          </div>
          <div className=" border border-[#FFFFFF] bg-[#ffff] rounded-[20px] p-4 mt-1">
            <div className="flex">
              <div className="relative w-[93%]">
                <input
                  type="text"
                  className=" w-[98%] h-[38px] pl-10 pr-5 border rounded-md border-[#E2E2EA] focus:outline outline-[#E2E2EA]  focus:border-transparent"
                  placeholder="Sök efter namn, leverantör, mobil etc..."
                  onChange={(e) => {
                    setFilterData(e.target.value);
                    filteredDatas(e.target.value)
                  }}
                  value={filterData}
                />

                <div className="absolute inset-y-0 left-0 flex items-center pl-3">
                  <img src={searchIcon} />
                </div>
              </div>

              {/* <div className=""> */}
              <div className="flex items-center mr-5 border border-1 border-slate-300 h-10 bg-[white] rounded-lg">

                <Select
                  options={options}
                  placeholder={"Stad"}
                  styles={customStyles}
                  // defaultValue={selectedOption}
                  value={selectedOption}
                  onChange={handlecitychange}
                  components={customComponents}

                // isClearable
                />
                <RxCross1
                  className="mx-2 cursor-pointer"
                  onClick={handleClick}
                  style={{ color: '#A09792' }}
                />
              </div>
            </div>

            <div className="mt-6">
              <GridTable
                columnDefs={columnDefs}
                gridOptions={{
                  ...gridOptions,
                  getRowHeight: () => rowHeight,
                }}
                rowData={newFilterData}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ActivateSupplier;
