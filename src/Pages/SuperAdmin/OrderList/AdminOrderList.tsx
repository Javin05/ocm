import { useEffect, useState } from "react";
import AdminHeader from "../../../Components/AdminHeader/AdminHeader";
import SideNav from "../../../Components/SideNav/SideNav";
import searchIcon from "../../../assets/adminSearch.svg";
import timerIcon from "../../../assets/timer.svg";
import eyeIcon from "../../../assets/viewIcon.svg";
import GridTable from "../../../Components/GridTable/GridTable";
import deniedIcon from "../../../assets/deniedIcon.svg";
import deleteIcon from "../../../assets/Delete.png";
import AcceptIcon from "../../../assets/tick.png";
import { commonAxios } from "../../../Axios/service";
import { RootState } from "../../../Redux/store";
import { failureNotify, successNotify } from "../../../Components/Toast/Toast";
import Loader from "../../../Components/Loader/Loader";
import { useSelector } from "react-redux";
import { SiMicrosoftexcel } from "react-icons/si";
import { AsyncDownloadCSV } from "../../../Components/DownloadCsv/DownloadCSV";
import moment from "moment";
import Select from "react-select";
import Flatpickr from "react-flatpickr";
import "flatpickr/dist/themes/light.css";
import { RxCross1 } from "react-icons/rx";
import { input } from "@material-tailwind/react";


const AdminOrderList = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [tabMenu, setTabMenu] = useState("order");
  const [customerData, setCustomerData]: any = useState(null);
  const [rowData, setRowData]: any = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [filterData, setFilterData] = useState("");
  const [fileName, setFileName] = useState("Order-list");
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [selectedOption, setSelectedOption]: any = useState(null);
  const [appUser, setAppUser] = useState<any>([]);

  const [option, setOption] = useState<any>({
    label: "Välj leverantörer",
    value: "Välj leverantörer",
  });

  // const [filterData, setFilterDatas] = useState<any>();
  // const options = [
  //   rowData.map((data: any) => {
  //     return {
  //       value: data.appUser.mobile,
  //       label: data.appUser.firstName + " " + data.appUser.lastName,
  //     };
  //   }),
  // ];

  const [inputDate, setInputDate] = useState({
    start: "",
    end: "",
  });

  const customDate = (value: any) => {
    const gmtOffset = "+05:30";
    if (value.length === 2) {
      const newValue = value.map((e: any) => {
        let date = moment(e);
        let response = `${date.format("YYYY")}-${date.format(
          "MM"
        )}-${date.format("DD")}`;
        return response;
      });

      const data = {
        start: moment(newValue[0])
          .utcOffset(gmtOffset)
          .startOf("date")
          .utc()
          .format(),
        end: moment
          .utc(newValue[1])
          .utcOffset(gmtOffset)
          .endOf("date")
          .utc()
          .format(),
      };
      setInputDate(data);
    }
  };

  const CustomDropdownIndicator = () => (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        style={{ width: "24px", height: "24px", fill: '#A09792' }}
      >
        <path fill="none" d="M0 0h24v24H0z" />
        <path d="M7 10l5 5 5-5z" />
      </svg>
    </div>
  );

  const customComponents: any = {
    DropdownIndicator: CustomDropdownIndicator,
  };

  const gridColData = [
    {
      headerName: "Order ID",
      cellRendererFramework: (props: any) => {
        return <p>{props.data?.orderNumber}</p>;
      },
      width: 130,
    },
    {
      headerName: tabMenu === "product" ? "Produktnamn" : "Eventnamn",
      cellRendererFramework: (props: any) => {
        return (
          <p>
            {props.data.bookingItemType === "PRODUCT"
              ? (props.data.product?.name || "").toUpperCase()
              : (props.data.event?.name || "").toUpperCase()}
          </p>
        );
      },
      width: 190,
    },
    {
      headerName: "Kundens namn",
      hide: userInfo.role === 'ROLE_ADMIN' ? false : true,
      cellRendererFramework: (props: any) => {
        console.log("props", props);
        return (
          <p>
            {props.data.bookingItemType === "PRODUCT"
              ? props.data.product.appUser?.firstName[0].toUpperCase() + props.data.product.appUser?.firstName.slice(1) +
              " " +
              props.data.product.appUser?.lastName[0].toUpperCase() + props.data.product.appUser?.lastName.slice(1)
              : props.data.event.appUser?.firstName +
              " " +
              props.data.event.appUser?.lastName}
          </p>
        );
      },
      width: 150,
    },
    {
      headerName: "Telefonnummer till kund", field: "appUser.mobile", width: 150, cellClassRules: {
        'custom-text-color': () => true,
      }
    },
    {
      headerName: "Leverantörsnamn", field: "appUser.firstName", width: 150,

      cellRendererFramework: (props: any) => {
        console.log("props", props);
        const merchantName = props.data.appUser?.firstName;
        const capitalizedMerchantName = merchantName ? merchantName[0].toUpperCase() + merchantName.slice(1) : "";
        return (<p>{capitalizedMerchantName}</p>);
      },
    },
    {
      headerName: "Datum & tid",
      cellRendererFramework: (props: any) => {
        return (
          <>
            <div>
              <span>{getFormattedDate(props.data?.booking?.bookingDate)}</span>
            </div>
          </>
        );
      },
      width: 140,
    },
    {
      headerName: "Pris (SEK)",
      cellRendererFramework: (props: any) => {
        return (
          <p>
            {props.data.bookingItemType === "PRODUCT"
              ? props.data.product.price
              : props.data.price}
          </p>
        );
      },
      width: 100,
    },
    {
      headerName: "Status",
      cellRendererFramework: (props: any) => {
        const orderStatus = props.data?.status;

        return (
          <>
            <div className="flex">
              <button
                className={`${orderStatus === "PENDING"
                  ? "bg-[#D6B27D]"
                  : orderStatus === "ACCEPTED" || orderStatus === "SUCCESS"
                    ? "bg-[#048060]"
                    : "bg-[#D9512C]"
                  } w-[100px] rounded-lg text-[#FFFFFF] p-1`}
              >
                {/* <img
                  src={
                    orderStatus === "PENDING"
                      ? timerIcon
                      : orderStatus === "ACCEPTED" || orderStatus === "SUCCESS"
                        ? AcceptIcon
                        : deniedIcon
                  }
                  className="w-5"
                /> */}
                <span className="ml-0">
                  {orderStatus === "PENDING"
                    ? "Inväntar"
                    : orderStatus === "ACCEPTED" || orderStatus === "SUCCESS"
                      ? orderStatus[0] + orderStatus.slice(1).toLowerCase()
                      : "Nekad"}
                </span>
              </button>
            </div>
          </>
        );
      },
      width: 180,
    },
    {
      headerName: "Åtgärder",
      cellRendererFramework: (params: any) => {
        const handleButtonClick = (data: any) => {
          setCustomerData(data.data);
        };
        return (
          <>
            <div className="flex">
              <button
                className=""
                onClick={() => {
                  setIsModalOpen(true);
                  handleButtonClick(params);
                }}
              >
                <img
                  src={eyeIcon}
                  className="border border-[#E2E2EA] p-2 rounded-[10px]"
                />
              </button>
            </div>
          </>
        );
      },
      width: 100,
    },
  ];
  const getOrderData = async () => {
    const URL =
      userInfo.role === "ROLE_ADMIN"
        ? "/cemo/bookingitems/admin/event/orderlist"
        : `/cemo/bookingitems/supplier/event/orderlist/${userInfo.id}`;
    setIsLoading(true);
    try {
      const response = await commonAxios.get(URL);

      if (response.data) {
        await setRowData(response.data);
        let supplier: any = [];
        const appUserData = response.data.map((data: any) => {
          if (
            !supplier.includes(
              data.event.appUser.firstName +
              " " +
              data.event.appUser.lastName +
              "," +
              data.event.appUser.id
            )
          ) {
            return supplier.push(
              data.event.appUser.firstName +
              " " +
              data.event.appUser.lastName +
              "," +
              data.event.appUser.id
            );
          }
        });

        const values = supplier.map((val: any) => {
          return {
            value: val.split(",")[1],
            label: val.split(",")[0],
          };
        });

        await setAppUser(values);
      }
    } catch (error) {
      failureNotify("oops! Something went wrong");
    } finally {
      setIsLoading(false);
    }
  };

  const getProductDatas = async () => {
    setRowData([]);
    setIsLoading(true);
    const URL =
      userInfo.role === "ROLE_ADMIN"
        ? "/cemo/bookingitems/product/orderList"
        : `/cemo/bookingitems/supplier/product/orderlist/${userInfo.id}`;
    try {
      const response = await commonAxios.get(URL);

      if (Array.isArray(response.data)) {
        setRowData(response.data);
        let supplier: any = [];
        const appUserData = response.data.map((data: any) => {
          if (
            !supplier.includes(
              data.product.appUser.firstName +
              " " +
              data.product.appUser.lastName +
              "," +
              data.product.appUser.id
            )
          ) {
            return supplier.push(
              data.product.appUser.firstName +
              " " +
              data.product.appUser.lastName +
              "," +
              data.product.appUser.id
            );
          }
        });
        const values = supplier.map((val: any) => {
          return {
            value: val.split(",")[1],
            label: val.split(",")[0],
          };
        });
        await setAppUser(values);
      }
    } catch (error) {
      failureNotify("oops! Something went wrong");
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    setIsLoading(true);
    if (tabMenu !== "product") {
      getOrderData();
    } else {
      getProductDatas();
    }
  }, [tabMenu]);

  const updateOrderStatus = async (status: boolean) => {
    setIsLoading(true);
    try {
      const response = await commonAxios.put(
        `/cemo/bookingitems/${customerData.id}/status/${status}`
      );
      if (response.status === 200) {
        successNotify("Order status updated successfully");
        getOrderData();
        setIsModalOpen(false);
      } else {
        failureNotify("oops! Something went wrong, Try again");
        setIsModalOpen(false);
      }
    } catch (error) {
      failureNotify("oops! Something went wrong, Try again");
    } finally {
      setIsLoading(false);
    }
  };
  const [isConfirmationOpen, setIsConfirmationOpen] = useState(false);
  const [action, setAction] = useState(''); // To track the action (accept/reject)

  const openConfirmation = (action: any) => {
    setAction(action);
    setIsConfirmationOpen(true);
  };

  const closeConfirmation = () => {
    setIsConfirmationOpen(false);
  };

  const handleConfirm = (confirmed: any) => {
    if (confirmed) {
      if (action === 'accept') {
        // Perform the accept action
        updateOrderStatus(true);
      } else if (action === 'reject') {
        // Perform the reject action
        updateOrderStatus(false);
      }
    }

    // Close the confirmation dialog
    closeConfirmation();
  };

  const CustomerDetails = () => {
    return (
      <div className="w-full h-full fixed top-0 left-0 flex justify-center items-center bg-[#00000080] z-40">
        <section className="flex flex-col items-center justify-around h-[35rem]">
          <div className="bg-white py-10 px-10 relative w-[1000px] h-[40rem] overflow-y-scroll">
            <button onClick={() => setIsModalOpen(false)}>
              <img
                src={deleteIcon}
                className="absolute top-5 right-5 h-6 w-6"
              />
            </button>
            <div className="flex " >
              <h1 className="p-4 -mt-10 mr-60 font-bold text-secondary text-xl">Order Details</h1>
              <h1 className=" p-4 -mt-10 ml-[75px] font-bold text-secondary">Order ID :</h1>
              <h1 className=" -mt-6 -ml-3 text-secondary">{customerData?.orderNumber}</h1>
            </div>
            <div className="grid grid-cols-2 gap-4">
              <div className="p-4">
                <h1 className="-mt-3 font-bold text-secondary">Kunduppgifter</h1>
                <div className="border p-3 rounded-lg mt-1">
                  <div className="grid grid-cols-2 gap-2">
                    <div className="p-4">
                      <div>
                        <h1 className="font-semibold text-secondary">Förnamn</h1>
                        <h1 className="text-secondary">
                          {customerData.booking?.firstName ?? customerData.product?.appUser?.firstName ?? 'N/A'}
                        </h1>
                      </div>
                    </div>
                    <div className="p-4">
                      <div>
                        <h1 className="font-semibold text-secondary">Efternamn</h1>
                        <h1 className="text-secondary">
                          {customerData.booking?.lastName ?? customerData.product?.appUser?.lastName ?? 'N/A'}
                        </h1>
                      </div>
                    </div>
                  </div>
                  <div className="grid grid-cols-2 gap-2">
                    <div className="p-4">
                      <div>
                        <h1 className="font-semibold text-secondary">Telefonnummer</h1>
                        <h1 className="text-secondary">
                          {customerData.appUser?.mobile ?? customerData.product?.appUser?.mobile ?? 'N/A'}
                        </h1>
                      </div>
                    </div>
                    <div className="p-4">
                      <div>
                        <h1 className="font-semibold text-secondary">E-post</h1>
                        <div className="h-12 overflow-y-auto whitespace-normal">
                          <h1 className="text-secondary">
                            {customerData.appUser?.email ?? customerData.product?.appUser?.email ?? 'N/A'}
                          </h1>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="grid">
                    <div className="p-4">
                      <div>
                        <h1 className="font-semibold text-secondary">Booked Date</h1>
                        <h1 className="text-secondary">
                          {customerData.booking?.createdAt?.split("T")[0] ?? customerData.product?.appUser?.updatedAt?.split("T")[0] ?? 'N/A'}{' '}
                          {customerData.booking?.createdAt?.split("T")[1]?.substring(0, 5) ?? customerData.product?.appUser?.updatedAt?.split("T")[1]?.substring(0, 5) ?? 'N/A'}
                        </h1>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="p-4">
                <h1 className="-mt-3 font-bold  text-secondary">Order Details</h1>
                <div className="border p-3 rounded-lg mt-1">
                  <div className="grid grid-cols-2 gap-4">
                    <div className="p-4">
                      <div>
                        <h1 className="font-semibold text-secondary">Order Type</h1>
                        <h1 className="text-secondary">
                          {customerData.bookingItemType
                            ? customerData.bookingItemType.charAt(0).toUpperCase() +
                            customerData.bookingItemType.slice(1).toLowerCase()
                            : 'No Order Type'}
                        </h1>
                      </div>
                    </div>

                    <div className="p-4">
                      <div className="">
                        <h1 className="font-semibold text-secondary">Order Date</h1>
                        <h1 className="text-secondary">{customerData.booking.bookingDate}</h1>
                      </div>
                    </div>
                  </div>
                  <div className="grid grid-cols-2 gap-4">
                    <div className="p-4">
                      <div>
                        <h1 className="font-semibold text-secondary">Order Status</h1>
                        <h1 className="text-secondary">
                          {customerData.status
                            ? customerData.status.charAt(0).toUpperCase() +
                            customerData.status.slice(1).toLowerCase()
                            : 'No Order Status'}
                        </h1>
                      </div>
                    </div>
                    <div className="p-4">
                      <div className="">
                        <h1 className="font-semibold text-secondary">Payment Type</h1>
                        <h1 className={`w-32  rounded-md text-secondary `} >
                          {customerData.booking?.paymentType
                            ? customerData.booking.paymentType.charAt(0).toUpperCase() +
                            customerData.booking.paymentType.slice(1).toLowerCase()
                            : 'No Payment Type'}
                        </h1>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="p-4">
              <h1 className="-mt-3 font-bold text-secondary">
                {tabMenu === "order" ? "Eventuppgifter" : "Produktdetaljer"}
              </h1>
              <div className="border p-3 rounded-lg mt-1">
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">
                        {tabMenu === "order" ? "Eventnamn" : "Produktnamn"}
                      </h1>
                      <h1 className="text-secondary">
                        {tabMenu === "order"
                          ? customerData.event?.name
                          : customerData.product?.name}
                      </h1>
                    </div>
                  </div>
                  <div className="grid grid-cols-2 gap-4">
                    <div className="p-4">
                      <div className="text-secondary">
                        {tabMenu === "order" && (
                          <>
                            <h1 className="font-semibold text-secondary">Stad</h1>
                            <h1 className="text-secondary">
                              {customerData.event.city
                                ? customerData.event.city.charAt(0).toUpperCase() +
                                customerData.event.city.slice(1).toLowerCase()
                                : 'No City'}
                            </h1>{" "}
                          </>
                        )}
                      </div>
                    </div>

                  </div>
                </div>
                <div className="p-4">
                  <div className="">
                    <h1 className="font-semibold text-secondary">Beskrivning</h1>
                    <h1 className="text-secondary">
                      {tabMenu === "order"
                        ? customerData.event.description.charAt(0).toUpperCase() +
                        customerData.event.description.slice(1).toLowerCase()
                        : customerData.product.description.charAt(0).toUpperCase() +
                        customerData.product.description.slice(1).toLowerCase()}
                    </h1>
                  </div>
                </div>
              </div>
            </div>
            {customerData.status === "REJECTED" ||
              customerData.status === "ACCEPTED" ||
              customerData.status === "SUCCESS" ||
              customerData?.bookingItemType !== "EVENT" ? null : (
              <div className="p-4">
                <h1 className="-mt-3 font-bold text-secondary">Bokningsåtgärder</h1>
                <div className="p-3 rounded-lg mt-1">
                  <div className="flex flex-row gap-4">
                    <button
                      onClick={() => openConfirmation('accept')}
                      className="px-4 py-2 bg-golden rounded-md uppercase text-white font-semibold cursor-pointer"
                    >
                      Acceptera order
                    </button>
                    <button
                      onClick={() => openConfirmation('reject')}
                      className="px-4 py-2 bg-golden text-white rounded-md uppercase font-semibold cursor-pointer"
                    >
                      Neka order
                    </button>

                    {isConfirmationOpen && (
                      <div className="fixed inset-0 flex items-center justify-center z-50 bg-[#00000080]">
                        <div className="bg-white rounded-lg md:p-6 w-full p-4 relative max-w-md">
                          <button onClick={closeConfirmation}>
                            <img
                              src={deleteIcon}
                              className="absolute top-1 right-3 mt-3 h-4 w-4"
                              alt="Close"
                            />
                          </button>
                          <p className="font-semibold text-lg text-center text-secondary">
                            {action === 'accept'
                              ? 'Är du säker att du vill godkänna ordern?'
                              : 'Är du säker att du vill neka ordern??'}
                          </p>
                          <section className="flex flex-row justify-center mt-6">
                            <button
                              onClick={() => handleConfirm(true)}
                              className="px-8 py-2 rounded-lg uppercase bg-[#D6B37D] text-white font-semibold text-base cursor-pointer mx-2"
                            >
                              Ja
                            </button>
                            <button
                              onClick={() => handleConfirm(false)}
                              className="px-8 py-2 rounded-lg uppercase bg-golden text-white font-semibold text-base cursor-pointer mx-2"
                            >
                              Nej
                            </button>
                          </section>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            )}
          </div>
        </section>
      </div>
    );
  };

  const getFormattedDate = (timeStamp: string) => {
    const date = new Date(timeStamp);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString();
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");

    const formattedDate = `${year}-${month}-${day} ${hours}:${minutes}`;

    return formattedDate;
  };

  const [columnDefs, setColumnDefs]: any = useState([]);

  const gridOptions = {
    getRowHeight: function (params: any) {
      return 50;
    },
    defaultColDef: {
      cellStyle: { fontFamily: "Monsterrat" },
    },
  };

  useEffect(() => {
    setColumnDefs(gridColData);
  }, [tabMenu]);

  const getProductDataForDownload = async () => {
    setFileName("Supplier-product-orderlist");
    const downloadData = rowData.map((data: any) => {
      const tableDate = new Date(data?.booking?.bookingDate);
      const startDate = inputDate.start ? new Date(inputDate.start) : null;
      const endDate = inputDate.end ? new Date(inputDate.end) : null;

      if (
        selectedOption !== null &&
        data.product.appUser.id === selectedOption.value &&
        startDate !== null &&
        endDate !== null
      ) {
        if (tableDate >= startDate && tableDate <= endDate) {
          return getProductDownloadInfo(data);
        }
      } else if (
        selectedOption === null &&
        startDate !== null &&
        endDate !== null
      ) {
        if (tableDate >= startDate && tableDate <= endDate) {
          return getProductDownloadInfo(data);
        } else {
          return [];
        }
      } else if (
        selectedOption !== null &&
        data.product.appUser.id === selectedOption.value &&
        (startDate === null || endDate === null)
      ) {
        return getProductDownloadInfo(data);
      } else if (
        selectedOption === null &&
        startDate === null &&
        endDate === null
      ) {
        return getProductDownloadInfo(data);
      } else {
        return [];
      }
    });

    return downloadData.filter((item: any) => item.length !== 0);
  };

  function getProductDownloadInfo(data: any) {
    return {
      "Order ID": data.orderNumber,
      "Produktnamn": data.product.name,
      "Supplier Name": data.product.appUser.firstName,
      "Telefonnummer till kund": data.appUser.mobile,
      "Kundens namn": data.appUser.firstName[0].toUpperCase() + data.appUser.firstName.slice(1) + " " + data.appUser.lastName[0].toUpperCase() + data.appUser.lastName.slice(1),
      "Order Date": data?.booking?.bookingDate,
      "Price": data.product.price + " SEK",
      "MOMS(SEK)": data.vatAmount ?? "-",
    };
  }

  const getEventDataForDownload = async () => {
    const downloadData = rowData.map((data: any) => {
      const tableDate = new Date(data?.booking?.bookingDate);
      const startDate = inputDate.start ? new Date(inputDate.start) : null;
      const endDate = inputDate.end ? new Date(inputDate.end) : null;

      if (
        selectedOption !== null &&
        data.event.appUser.id === selectedOption.value &&
        startDate !== null &&
        endDate !== null
      ) {
        if (tableDate >= startDate && tableDate <= endDate) {
          return getEventDownloadInfo(data);
        }
      } else if (
        selectedOption === null &&
        startDate !== null &&
        endDate !== null
      ) {
        if (tableDate >= startDate && tableDate <= endDate) {
          return getEventDownloadInfo(data);
        } else {
          return [];
        }
      } else if (
        selectedOption !== null &&
        data.event.appUser.id === selectedOption.value &&
        (startDate === null || endDate === null)
      ) {
        return getEventDownloadInfo(data);
      } else if (
        selectedOption === null &&
        startDate === null &&
        endDate === null
      ) {
        return getEventDownloadInfo(data);
      } else {
        return [];
      }
    });

    return downloadData.filter((item: any) => item.length !== 0);
  };

  function getEventDownloadInfo(data: any) {
    return {
      "Order ID": data.orderNumber,
      "Eventnamn": data.event?.name,
      "Location": data.event.city,
      "Supplier Name": data.event.appUser.firstName,
      "Telefonnummer till kund": data.appUser.mobile,
      "Kundens namn": data.appUser.firstName[0].toUpperCase() + data.appUser.firstName.slice(1) + " " + data.appUser.lastName[0].toUpperCase() + data.appUser.lastName.slice(1),
      "Order Date": data?.booking?.bookingDate,
      "Price": data.event.price + " SEK",
      "MOMS(SEK)": data.vatAmount ?? "-",
    };
  }

  const filteredData = rowData?.filter((val: any) => {
    let value;
    if (tabMenu === "product") {
      value =
        val & val.product && val.product.name ? val.product.name.toLowerCase().includes(filterData.toLowerCase()) : '' ||
          val.id.toLowerCase().includes(filterData.toLowerCase()) ||
          val.appUser.mobile.toLowerCase().includes(filterData.toLowerCase()) ||
          val.appUser.firstName
            .toLowerCase()
            .includes(filterData.toLowerCase()) ||
          val.product.price
            .toString()
            .toLowerCase()
            .includes(filterData.toLowerCase()) ||
          val.status.toLowerCase().includes(filterData.toLowerCase()) ||
          val.booking.bookingDate
            .toLowerCase()
            .includes(filterData.toLowerCase());
    } else {
      value =
        val.event?.name.toLowerCase().includes(filterData.toLowerCase()) ||
        val.id?.toLowerCase().includes(filterData.toLowerCase()) ||
        val.appUser?.mobile.toLowerCase().includes(filterData.toLowerCase()) ||
        val.appUser?.firstName
          .toLowerCase()
          .includes(filterData.toLowerCase()) ||
        val.event?.price
          .toString()
          .toLowerCase()
          .includes(filterData.toLowerCase()) ||
        val.status?.toLowerCase().includes(filterData.toLowerCase());
    }
    let matchesDropdown;
    if (tabMenu === "product") {
      matchesDropdown = selectedOption
        ? val.product.appUser.id === selectedOption.value
        : true;
    } else {
      matchesDropdown = selectedOption
        ? val.event.appUser.id === selectedOption.value
        : true;
    }

    const tableDate: any = new Date(val.booking?.bookingDate);
    const startDate: any = inputDate.start ? new Date(inputDate.start) : null;
    const endDate: any = inputDate.end ? new Date(inputDate.end) : null;

    let filteredDate;
    filteredDate =
      inputDate.start !== "" || inputDate.end !== ""
        ? tableDate >= startDate && tableDate <= endDate
        : true;

    return value && matchesDropdown && filteredDate;
  });

  const handleMerchantChange = (selectedOption: any) => {
    setOption({ label: selectedOption.label, value: selectedOption.value });
    setSelectedOption(selectedOption);
  };

  const customStyles = {
    control: (provided: any, state: any) => ({
      ...provided,
      border: "none",
      outline: "none",
      fontSize: "17px",
      width: "200px",
      boxShadow: "none",
    }),
    option: (provided: any, state: any) => ({
      ...provided,
      backgroundColor: "transparent",
      fontSize: "17px",
      width: "300px",
      boxShadow: "none",
      color: state.isSelected ? "#D6B27D" : "gray",
    }),
    singleValue: (provided: any) => ({
      ...provided,
      color: "gray", // Set the initial text color to gray
    }),
    valueContainer: (provided: any) => ({
      ...provided,
      color: "gray", // Set the initial text color to gray
    }),

  };

  const handleClick = () => {
    setSelectedOption("");
    setOption({ label: "Välj leverantörer", value: "Välj leverantörer" });
  };

  return (
    <>
      {isModalOpen && <CustomerDetails />}
      {isLoading && <Loader />}
      <div className="flex">
        <SideNav />
        <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
          <AdminHeader />
          {tabMenu === "order" ? (
            <div className="mx-0 ">
              <h1 className="text-secondary text-[24px] font-semibold p-4">
                Orders
              </h1>
              <div className="mx-5">
                <button
                  className={`text-xl px-5 rounded-tl-lg text-secondary rounded-tr-lg  ${tabMenu === "order" &&
                    "border-t-2 border-l-2 border-r-2 text-[#D6B27D] border-[#FFE5D8]"
                    }`}
                  onClick={() => {
                    setTabMenu("order")
                    handleClick()
                  }}
                >
                  Event
                </button>
                <button
                  className={`text-xl mx-4 text-secondary ${tabMenu !== "order" && " text-[#D6B27D] "
                    }`}
                  onClick={() => {
                    setTabMenu("product");
                    handleClick()
                  }}
                >
                  Produkter
                </button>
                {(userInfo.role === "ROLE_SUPPLIER" ||
                  userInfo.role === "ROLE_ADMIN") && (
                    <div className="float-right flex flex-row ">
                      {userInfo.role === "ROLE_ADMIN" ? (
                        <div className="flex items-center -mt-[7px] mr-5 border border-1 border-slate-300 h-10 bg-[white] rounded-lg">
                          <Select
                            components={customComponents}
                            // defaultValue={selectedOption}
                            value={selectedOption}
                            onChange={handleMerchantChange}
                            options={appUser}
                            // value={option}
                            placeholder={"Välj leverantörer"}
                            className={""}
                            styles={customStyles}
                          />
                          <RxCross1
                            className="mx-2 cursor-pointer"
                            onClick={handleClick}
                            style={{ color: '#A09792' }}
                          />
                        </div>
                      ) : null}
                      <Flatpickr
                        options={{
                          mode: "range",
                          dateFormat: "d-m-Y",
                        }}
                        onChange={(range: any) => customDate(range)}
                        className="h-[37px] -mt-[5px] w-40 border focus:outline-none focus:border-none focus:ring-2 focus:ring-blue-600 border-gray-300 mr-5 p-2 rounded-md"
                        placeholder="Välj datumintervall"
                      />

                      {
                        <AsyncDownloadCSV
                          data={getEventDataForDownload}
                          asyncOnClick={true}
                          filename={
                            "Supplier-event-orders-" +
                            moment(new Date()).format("DD/MM/YYYY")
                          }
                          onClick={() => { }}
                        >
                          <SiMicrosoftexcel
                            className={
                              filteredData.length > 0
                                ? "w-6 h-6 text-[#D6B27D] hover:text-[#F7CB73] hover:cursor-pointer"
                                : "w-6 h-6 text-[#D6B27D] hover:text-[#F7CB73] hover:cursor-not-allowed"
                            }
                          />
                        </AsyncDownloadCSV>
                      }
                    </div>
                  )}
              </div>
              <div className=" border border-[#ffffff]  bg-[#ffff] rounded-[20px] p-4 mt-2">
                <div className="relative w-full">
                  <input
                    type="text"
                    className="w-full h-[38px] pl-10 pr-5 border rounded-md border-[#E2E2EA] focus:outline outline-[#E2E2EA]  focus:border-transparent"
                    placeholder="Sök efter namn, leverantör, mobil etc..."
                    onChange={(e) => setFilterData(e.target.value)}
                    value={filterData}
                  />
                  <div className="absolute inset-y-0 left-0 flex items-center pl-3">
                    <img src={searchIcon} />
                  </div>
                </div>
                <div className="mt-6">
                  <GridTable
                    columnDefs={columnDefs}
                    gridOptions={gridOptions}
                    rowData={filteredData.sort((a: any, b: any) => moment(b.booking.bookingDate).diff(moment(a.booking.bookingDate)))}
                  />
                </div>
              </div>
            </div>
          ) : (
            <div className="mx-0">
              <h1 className="text-secondary text-[24px] font-semibold p-4">
                Orders
              </h1>
              <div className="mx-5">
                <button
                  className="text-xl px-3 text-secondary"
                  onClick={() => {
                    setTabMenu("order");
                    setSelectedOption(null);
                  }}
                >
                  Event
                </button>
                <button
                  className={`text-xl px-3 rounded-tl-lg text-secondary rounded-tr-lg ${tabMenu === "product" &&
                    "border-t-2 border-l-2 border-r-2 text-[#D6B27D] border-[#FFE5D8]"
                    }`}
                  onClick={() => {
                    setSelectedOption(null);
                    setTabMenu("product");
                  }}
                >
                  Produkter
                </button>
                {(userInfo.role === "ROLE_ADMIN" ||
                  userInfo.role === "ROLE_SUPPLIER") && (
                    <div className="float-right flex flex-row">
                      {userInfo.role === "ROLE_ADMIN" ? (
                        <div className="flex items-center -mt-[7px] mr-5 border border-1 border-slate-300 h-10 bg-[white] rounded-lg">
                          <Select
                            components={customComponents}
                            onChange={handleMerchantChange}
                            options={appUser}
                            placeholder={"Välj leverantörer"}
                            styles={customStyles}
                            value={option}
                          />
                          <RxCross1
                            className="mx-2 cursor-pointer"
                            onClick={handleClick}
                            style={{ color: '#A09792' }}
                          />
                        </div>
                      ) : null}
                      <Flatpickr
                        options={{
                          mode: "range",
                          dateFormat: "d-m-Y",
                        }}
                        onChange={(range: any) => customDate(range)}
                        className="h-[37px] -mt-[5px] w-40 border focus:outline-none focus:border-none focus:ring-2 focus:ring-blue-600 border-gray-300 mr-5 p-2 rounded-md"

                        placeholder="Välj datumintervall"
                      />
                      {
                        <AsyncDownloadCSV
                          data={getProductDataForDownload}
                          asyncOnClick={true}
                          filename={
                            "Supplier-product-orders-" +
                            moment(new Date()).format("DD/MM/YYYY")
                          }
                          onClick={() => { }}
                        >
                          <SiMicrosoftexcel
                            className={
                              filteredData.length > 0
                                ? "w-6 h-6 text-[#D6B27D] hover:text-[#F7CB73] hover:cursor-pointer"
                                : "w-6 h-6 text-[#D6B27D] hover:text-[#F7CB73] hover:cursor-not-allowed"
                            }
                          />
                        </AsyncDownloadCSV>
                      }
                    </div>
                  )}
              </div>

              <div className=" border border-[#FFFFFF]  bg-[#ffff] rounded-[20px] p-4 mt-2">
                <div className="relative w-full">
                  <input
                    type="text"
                    className="w-full h-[38px] pl-10 pr-5 border rounded-md border-[#E2E2EA] focus:outline outline-[#E2E2EA]  focus:border-transparent"
                    placeholder="Sök efter namn, leverantör, mobil etc..."
                    onChange={(e) => setFilterData(e.target.value)}
                    value={filterData}
                  />
                  <div className="absolute inset-y-0 left-0 flex items-center pl-3">
                    <img src={searchIcon} />
                  </div>
                </div>
                <div className="mt-6">
                  <GridTable
                    columnDefs={columnDefs}
                    gridOptions={gridOptions}
                    rowData={filteredData.sort((a: any, b: any) => moment(b.booking.bookingDate).diff(moment(a.booking.bookingDate)))}
                  />
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default AdminOrderList;
