import React, { useEffect, useState } from "react";
import AdminHeader from "../../../Components/AdminHeader/AdminHeader";
import SideNav from "../../../Components/SideNav/SideNav";
import addIcon from "../../../assets/addIcon.svg";
import searchIcon from "../../../assets/adminSearch.svg";
import GridTable from "../../../Components/GridTable/GridTable";
import hotelImg from "../../../assets/hotel.png";
import settingIcon from "../../../assets/settingIcon.svg";
import { commonAxios } from "../../../Axios/service";
import Select from "react-select";
import moment from "moment";
import AddCategoryModal from "../../../Components/AddCategoryModal/AddCategoryModal";
import eyeIcon from "../../../assets/viewIcon.svg";
import editIcon from "../../../assets/editIcon.svg";
import deleteIcon from "../../../assets/Delete.png";
import { failureNotify, successNotify } from "../../../Components/Toast/Toast";
import eventdeleteicon from "../../../assets/deleteIcon.svg";
import modaldeleteIcon from "../../../assets/Delete.png";
import { RootState } from "../../../Redux/store";
import { useSelector } from "react-redux";

const CategoryScreen = () => {
  const [isloading, setIsLoading] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [filterData, setFilterData] = useState("");
  const [viewCategory, setViewCategory] = useState(false);
  const [categoryData, setCategoryData] = useState<any>([]);

  const [categorydeletePopup, setCategoryDeletePopup] = useState(false);
  const [editCategory, setEditCategory] = useState(false);
  const loggedInUser = useSelector(
    (state: RootState) => state.user.userDetails
  );
  const [content, setContent] = useState("");

  const getFormattedDate = (timeStamp: string) => {
    const date = new Date(timeStamp);

    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString();
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");


    const formattedDate = `${day}/${month}/${year} ${hours}:${minutes}`;

    return formattedDate;
  };

  


  function capitalizeFirstLetter(inputString: any) {
    if (typeof inputString !== 'string' || inputString.length === 0) {
      return inputString; // Return the input as is if it's not a non-empty string
    }

    // Split the input string into words, capitalize the first letter of each word, and join them back together
    return inputString
      .toLowerCase()
      .split(' ')
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');

  }


  const [columnDefs]: any = useState([
    {
      headerName: "Kategorinamn",
      cellRendererFramework: (props: any) => {
        return (
          <>
            <button className="bg-[#D6B27D] p-2 font-semibold text-[16px] text-[#FFFFFF] rounded-[10px]">
              {props.data.categoryName[0] +
                props.data.categoryName[1]}
            </button>
            <span className="mx-2">{props.data.categoryName.toUpperCase()}</span>
          </>
        );
      },
      width: 350,
    },
    
    {
      headerName: "Skapad den",
      cellRendererFramework: (props: any) => {
        return (
          <>
            <span className={`${props.data.createdAt ? "" : "ml-10"}`}>
              {" "}
              {props.data.createdAt
                ? moment(props.data.createdAt).format("YYYY-MM-DD")
                : "-"}
            </span>
          </>
        );
      },
      width: 350,
    },

    {
      headerName: "Åtgärder",
      cellRendererFramework: (params: any) => {
        const handleButtonClick = (data: any) => {
          setCategoryData(data.data);
          setViewCategory(true);
        };
        const handleAddProduct = () => {
          setCategoryData(params.data);
          setIsModalOpen(true);
          setContent("Edit category");
        };
        const handleEventDelete = () => {
          setCategoryData(params.data);
          setCategoryDeletePopup(true);
        };

        return (
          <>
            <div className="flex flex-row">
              <button onClick={handleAddProduct}>
                <img
                  src={editIcon}
                  className=" border-[#E2E2EA] rounded-[10px] p-1 "
                />
              </button>
              <button onClick={handleEventDelete} >
                <label className="relative inline-flex items-center cursor-pointer ml-2 mt-2">
                  <input type="checkbox"
                    value=""
                    className="sr-only peer"
                    checked={params.data.status === 'ACTIVE'}
                  />
                  <div className="w-11 h-6 bg-gray-200 rounded-full peer peer-checked:after:translate-x-full  after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-[#D6B27D]"></div>
                </label>
              </button>
            </div>
          </>
        );
      },
      width: 200,
    },
    
  ]);

  const [rowData, setRowData]: any = useState([]);
  const [statusData, setStatusData]: any = useState([]);
  const [SelectedStatus, setSelectedStatus]: any = useState({value:"ACTIVE",label:"ACTIVE"});


  const gridOptions = {
    getRowHeight: function (params: any) {
      return 60;
    },
  };
  const customStyles = {
    control: (provided: any, state: any) => ({
      ...provided,
      border: "1px solid #ccc",
      borderRadius: "4px",
      fontSize: "13px",
      width: "150px",
    }),
    option: (provided: any, state: any) => ({
      ...provided,
      backgroundColor:"transparent",
      fontSize: "13px",
      width: "150px",
      color: state.isSelected ? "#D6B27D" : "gray",
    }),
    singleValue: (provided: any) => ({
        ...provided,
        color: "gray", // Set the initial text color to gray
      }),
      valueContainer: (provided: any) => ({
        ...provided,
        color: "gray", // Set the initial text color to gray
      }),
  };

  const getAllCategory = async () => {
    setIsLoading(true);
    console.log(loggedInUser,"loggedInUser")
    try {
      if(loggedInUser.role === "ROLE_ADMIN"){
        const response = await commonAxios.get(
          `/cemo/categories`
        );
        if (response.data) {
          const filteredData = response.data
  
  
          setRowData(filteredData);
        }

      } else {
        const response = await commonAxios.get(
            `/cemo/categories/supplier/` + loggedInUser.id
          );
          if (response.data) {
            const filteredData = response.data
    
    
            setRowData(filteredData);
          }
      }
     
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  let categoryStatus: any = [];
  rowData.forEach((val: any) => {
    if (!categoryStatus.includes(val.status?.toUpperCase())) {
      categoryStatus.push(val.status?.toUpperCase());
    }
  });

  const statusOptions = categoryStatus.map((val: any) => {
    return {
      label: val,
      value: val
    };
  });
  
  const handleStatusChange = (selectedOption:any) => {
    setSelectedStatus(selectedOption);
  };


  const filteredData = (filterData: any) => {
    let value = rowData.filter(
      (val: any) =>
        val.categoryName.toLowerCase().includes(filterData.toLowerCase()) ||
        moment(val.createdAt)
          .format("MMMM Do YYYY")
          .toLowerCase()
          .includes(filterData.toLowerCase())
    );
    if (SelectedStatus) {
      value = value.filter((val: any) => val.status === SelectedStatus.value);
    }
    return value;
  };



  const CategoryDelete = () => {
    const handleclose = () => {
      setCategoryDeletePopup(false);
      setCategoryData([]);
    };

    const deleteEvent = async () => {
      setIsLoading(true);
      console.log(categoryData.status, "data for categories");
      try {
        const status = categoryData.status === "ACTIVE" ? "INACTIVE" : "ACTIVE"
        console.log(status,"status")
        const response = await commonAxios.delete(
          `/cemo/categories/${categoryData.categoryName}/${status}`
          
        );
        
        if (response.status == 200 || response.status == 204) {
          getAllCategory();
          successNotify(`${categoryData.categoryName} ${response.data.status.toLowerCase()} succesfully`);
          handleclose();
        }
      } catch (error) {
        failureNotify("oops! something went wrong");
      } finally {
        setIsLoading(false);
      }
    };
    


    return (
      <div className="w-[85%] h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <div className="bg-white rounded-lg flex flex-col justify-center items-center md:w-[450px] w-full p-4 md:h-56 h-46 relative">
            <button onClick={handleclose}>
              <img
                src={modaldeleteIcon}
                className="absolute top-5 right-5 h-6 w-6"
              />
            </button>
            <p className="font-semibold text-lg text-secondary">
              Are you sure you want to change the category status "{categoryData.categoryName}" ?
            </p>
            <section className="flex flex-row justify-between items-center w-[40%] gap-8 mt-10 mr-28">
              <button
                onClick={deleteEvent}
                className="px-6 p-2 w-auto rounded-lg bg-golden text-white font-semibold text-base cursor-pointer"
              >
                {categoryData.status=="ACTIVE" ? "INAKTIV": "AKTIV" }
              </button>
              <button
                onClick={handleclose}
                className="px-6 p-2 w-auto rounded-lg bg-[#FFE5D8] text-secondary font-semibold text-base cursor-pointer"
              >
                Avbryt
              </button>
            </section>
          </div>
        </section>
      </div>
    );
  };

  useEffect(() => {
    getAllCategory();
  }, []);

  const newCategory = () => {
    setIsModalOpen(true);
    setContent("");
  };

  return (
    <div className="flex">
      <SideNav />
      {categorydeletePopup && <CategoryDelete />}
      {isModalOpen ? (
        <AddCategoryModal
          setIsModalOpen={setIsModalOpen}
          getCategoryList={getAllCategory}
          categoryData={categoryData}
          content={content}
        />
      ) : null}
      

      <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
        <AdminHeader />
        <div className="mx-2 my-6">
          <h1 className="text-secondary text-[24px] font-semibold px-4">Kategori</h1>
          <div className=" border border-[#FFFFFF] bg-[#ffff] rounded-[20px] p-4 mt-1">
            <div className="flex">
              <div className="relative w-[93%]">
                <input
                  type="text"
                  className=" w-[98%] h-[38px] pl-10 pr-5 border rounded-md border-[#E2E2EA] focus:outline outline-[#E2E2EA]  focus:border-transparent"
                  placeholder="Sök efter namn, leverantör, mobil etc..."
                  onChange={(e: any) => {
                    setFilterData(e.target.value);
                  }}
                  value={filterData}
                />
                <div className="absolute inset-y-0 left-0 flex items-center pl-3">
                  <img src={searchIcon} />
                </div>
              </div>
              <button
                className="bg-[#D6B27D] text-[#FFFFFF] flex px-6 p-2 w-auto rounded-[10px] font-bold mr-4"
                onClick={newCategory}
              >
                Ny <img src={addIcon} className="ml-2" />
              </button>
            </div>

            <div className="mt-6">
              <div className='w-full flex justify-between mb-6'>
              <h1 className="mb-4 text-gray-600 text-[16px] font-semibold">
                Listor
              </h1>
              <div>
                <Select
                  options={statusOptions}
                  styles={customStyles}
                  value={SelectedStatus} 
                  onChange={handleStatusChange}
                  isClearable
                />
              </div>
              </div>
              <GridTable
                columnDefs={columnDefs}
                gridOptions={gridOptions}
                rowData={filteredData(filterData)}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CategoryScreen;
