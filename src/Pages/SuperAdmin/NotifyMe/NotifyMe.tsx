import React, { useEffect, useState } from 'react'
import { commonAxios } from '../../../Axios/service';
import AdminHeader from '../../../Components/AdminHeader/AdminHeader';
import GridTable from '../../../Components/GridTable/GridTable';
import SideNav from '../../../Components/SideNav/SideNav';
import searchIcon from '../../../assets/adminSearch.svg';
import moment from 'moment';
import 'moment/locale/sv';

const NotifyMe = () => {

  const [rowHeight, setRowHeight] = useState(50);
  const [rowData, setRowData] = useState<any>(null);
  const [filterData, setFilterData] = useState("");


  moment.locale('sv');
  const formatDate = (params: any) => {
    return moment(params.value).format('YYYY-MM-DD - HH:mm:ss A');
  };

  const columnDefs = [
    {
      headerName: 'S.No',
      field: 'sNo',
      width: 150,
    },
    {
      headerName: 'E-post',
      field: 'email',
      width: 400,
    },
    {
      headerName: 'Sök nyckel',
      field: 'searchKey',
      width: 400,
    },
    {
      headerName: 'Datum och tid',
      field: 'createdAt',
      width: 300,
      cellRenderer: formatDate,
    },


  ];

  const gridOptions = {
    getRowHeight: function (params: any) {
      return 80;
    },
    defaultColDef: {
      cellStyle: { fontFamily: "Montserrat" },
    },
  };


  const getNotifyUsers = async () => {
    try {
      const response = await commonAxios.get('/cemo/notify');
      let notifyUserList: any = [];
      response.data.map((item: any, index: any) => {
        notifyUserList.push({
          ...item,
          sNo: index + 1
        })
      })

      setRowData(notifyUserList)
    } catch (error) {
      console.log(error);
    }
  }

  const filterDatas = (filterData: any) => {
    const value = rowData?.filter(
      (val: any) =>
        val?.email?.toLowerCase().includes(filterData?.toLowerCase()) ||
        val?.searchKey?.toString().toLowerCase().includes(filterData?.toLowerCase())
    );
    return value;
  };

  useEffect(() => {
    getNotifyUsers();
  }, [])

  return (
    <div className="flex">
      <SideNav />
      <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
        <AdminHeader />
        <div className="mx-0 my-6">
          <div className="flex flex-row items-center justify-between">
            <h1 className="text-secondary  text-[24px] font-semibold p-4">
              Meddela kunder
            </h1>
          </div>
          <div className=" border border-[#FFFFFF] bg-[#ffff] rounded-[20px] p-4 mt-2">
            <div className="relative w-full">
              <input
                type="text"
                className="w-full h-[38px] pl-10 pr-5 border rounded-md border-[#E2E2EA] focus:outline outline-[#E2E2EA] focus:border-transparent"
                placeholder="Sök efter namn, leverantör, mobil etc..."
                onChange={(e) => {
                  setFilterData(e.target.value);
                }}
                value={filterData}
              />
              <div className="absolute inset-y-0 left-0 flex items-center pl-3">
                <img src={searchIcon} />
              </div>
            </div>
            <div className="mt-6">
              <GridTable
                columnDefs={columnDefs}
                gridOptions={{
                  ...gridOptions,
                  getRowHeight: () => rowHeight,
                }}
                rowData={filterDatas(filterData)}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default NotifyMe