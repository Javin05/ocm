import React, { useEffect, useState } from "react";
import { commonAxios } from "../../../Axios/service";
import AdminHeader from "../../../Components/AdminHeader/AdminHeader";
import GridTable from "../../../Components/GridTable/GridTable";
import SideNav from "../../../Components/SideNav/SideNav";
import searchIcon from "../../../assets/adminSearch.svg";
import eventdeleteicon from "../../../assets/deleteIcon.svg";
import eyeIcon from "../../../assets/viewIcon.svg";
import deleteIcon from "../../../assets/Delete.png";
import { failureNotify, successNotify } from "../../../Components/Toast/Toast";
import moment from "moment";

const ContactUsFeedBack = () => {
  const [rowData, setRowData] = useState<any>([]);
  const [filterData, setFilterData] = useState("");
  const [rowHeight, setRowHeight] = useState(50);
  const [isDeleteEventModalOpen, setIsDeleteEventModalOpen] = useState(false);
  const [eventData, seteventData] = useState<any>([]);
  const [isActionModalOpen, setIsActionModalOpen] = useState(false);
  const [customerData, setCustomerData] = useState<any>([]);
  const [columnDefs]: any = useState([
    { headerName: "SNo", field: "sNo", width: 60 },
    { headerName: "Namn", field: "name", width: 140 },
    { headerName: "E-Post", field: "emailId", width: 200 },
    { headerName: "Message", field: "message", width: 500 },
    { headerName: "Received At", field: "dateTime", width: 140,cellRendererFramework: (params:any) => moment(params.value).format("YYYY-MM-DD HH:mm") },
    {
      headerName: "Åtgärder",
      cellRendererFramework: (params: any) => {
        const handleEventDelete = () => {
          seteventData(params.data);
          setIsDeleteEventModalOpen(true);
        };

        const handleButtonClick = (data: any) => {
          setCustomerData(data.data);
          setIsActionModalOpen(true);
        };
        return (
          <>
            <div className="flex flex-row justify-between items-center">
              <button
                onClick={() => {
                  handleButtonClick(params);
                }}
                className="mr-2"
              >
                <img src={eyeIcon} className="mx-1 border-[#E2E2EA] p-1 " />
              </button>
              <button onClick={handleEventDelete}>
                <img
                  src={eventdeleteicon}
                  className=" mx-2 border-[#E2E2EA]  p-1"
                  width={30}
                  height={30}
                />
              </button>
            </div>
          </>
        );
      },
      width: 200,
    },
  ]);

  const getFormattedDate = (timeStamp: string) => {
    const date = new Date(timeStamp);

    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString();
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");

    const formattedDate = `${day}/${month}/${year} ${hours}:${minutes}`;

    return formattedDate;
  };

  const getContactMessage = async () => {
    try {
      const response = await commonAxios.get("/cemo/contact/all");
      const rowData = response?.data.map((data:any,index:any) => {
        return {
          ...data,
          sNo:index + 1,
          dateTime: getFormattedDate(data.createdAt),
        }
      })
      setRowData(rowData);
    } catch (error) {
      console.log(error);
    }
  };

  console.log(rowData,"rowDatarowData")

  const DeleteEventModal = () => {
    const handleclose = () => {
      setIsDeleteEventModalOpen(false);
      setCustomerData([]);
      seteventData([]);
    };

    const deleteEvent = async () => {
      try {
        const response = await commonAxios.delete(
          `/cemo/contact/${eventData.id}`
        );
        if (response.data) {
          successNotify(`Submission Information deleted succesfully`);
          getContactMessage();
          handleclose();
        }
      } catch (error) {
        failureNotify("oops! something went wrong");
      }
    };

    

    return (
      <div className="w-[87%] h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <div className="bg-white rounded-lg flex flex-col justify-center items-center md:w-[450px] w-full p-4 md:h-56 h-46 relative">
            <button onClick={handleclose}>
              <img
                src={deleteIcon}
                className="absolute top-5 right-5 h-5 w-5"
              />
            </button>
            <p className="font-semibold text-lg text-secondary">
              Are you sure you want to delete the Submission Information
            </p>
            <section className="flex flex-row justify-between items-center w-[50%] mr-25 pt-10">
              <button
                onClick={deleteEvent}
                className="px-6 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer uppercase"
              >
               Ta bort 
              </button>
              <button
                onClick={handleclose}
                className="px-6 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer uppercase"
              >
                Avbryt
              </button>
            </section>
          </div>
        </section>
      </div>
    );
  };

  const CustomerDetails = () => {
    return (
      <div className="w-full h-full fixed top-0 left-0 flex items-center justify-center bg-[#00000080] z-40">
        <div className="bg-white w-[80%] md:w-[80%] lg:w-[60%] xl:w-[60%] max-w-[900px] max-h-[90%] overflow-y-auto p-10 relative">
          <button onClick={() => setIsActionModalOpen(false)}>
            <img src={deleteIcon} className="absolute top-5 right-5 h-6 w-6" />
          </button>
          <h2 className="text-xl font-bold font-[sofiapro] text-center text-secondary">
            Contact us
          </h2>
          <div className="grid grid-cols-1 gap-4">
            <div className="p-4">
              <h1 className="-mt-3 font-bold text-secondary">
                Submission Information
              </h1>
              <div className="border p-3 rounded-lg pt-1">
                <div className="grid grid-cols-2 gap-4">
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">Förnamn</h1>
                      <h1 className="text-secondary">{customerData?.name}</h1>
                    </div>
                  </div>
                  <div className="p-4">
                    <div className="">
                      <h1 className="font-semibold text-secondary">E-Post</h1>
                      <h1 className="text-secondary">{customerData.emailId}</h1>
                    </div>
                  </div>
                </div>
              </div>
              <div className="border p-3 rounded-lg pt-1">
                <div className="p-4">
                  <div className="">
                    <h1 className="font-semibold text-secondary">Message</h1>
                    <h1 className="text-secondary">{customerData.message}</h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const filterDatas = (filterData: any) => {
    const value = rowData?.filter(
      (val: any) =>
        val?.name?.toLowerCase()?.includes(filterData?.toLowerCase()) ||
        val?.price
          ?.toString()
          ?.toLowerCase()
          ?.includes(filterData?.toLowerCase()) ||
        val?.updatedAt?.toLowerCase()?.includes(filterData?.toLowerCase())
    );
    return value;
  };

  const gridOptions = {
    getRowHeight: function (params: any) {
      return 50;
    },
    defaultColDef: {
      cellStyle: { fontFamily: "Montserrat" },
    },
  };

  useEffect(() => {
    getContactMessage();
  }, []);

  return (
    <>
      <div className="flex">
        <SideNav />
        <div className="mx-2 w-full h-screen overflow-y-scroll">
          <AdminHeader />
          {isDeleteEventModalOpen ? <DeleteEventModal /> : null}
          {isActionModalOpen ? <CustomerDetails /> : null}
          <div className="">
            <div className="flex flex-row items-center justify-between">
              <h1 className=" text-secondary text-[24px] font-semibold p-4">
                Kontakta oss
              </h1>
            </div>
            <div className=" border border-[#FFFFFF] bg-[#ffff] rounded-[20px] p-4 pt-2">
              <div className="relative w-full">
                <input
                  type="text"
                  className="w-full h-[38px] pl-10 pr-5 border rounded-md border-[#E2E2EA] focus:outline outline-[#E2E2EA] focus:border-transparent"
                  placeholder="Sök efter namn, leverantör, mobil etc..."
                  onChange={(e) => setFilterData(e.target.value)}
                  value={filterData}
                />
                <div className="absolute inset-y-0 left-0 flex items-center pl-3">
                  <img src={searchIcon} />
                </div>
              </div>
              <div className="pt-6">
                {rowData.length >= 1 ? (
                  <GridTable
                    columnDefs={columnDefs}
                    gridOptions={{
                      ...gridOptions,
                      getRowHeight: () => rowHeight,
                    }}
                    rowData={filterDatas(filterData)}
                  />
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ContactUsFeedBack;
