import React, { useEffect } from "react";
import AdminHeader from "../../../Components/AdminHeader/AdminHeader";
import SideNav from "../../../Components/SideNav/SideNav";
import searchIcon from "../../../assets/adminSearch.svg";
import { useState } from "react";
import deleteIcon from "../../../assets/Delete.png";
import editIcon from "../../../assets/editIcon.svg";
import eventdeleteicon from "../../../assets/deleteIcon.svg";
import inActive from "../../../assets/inactive-icon-1.jpg";
import Inactive from "../../../assets/2d65659a9d.png"
import loginIcon from "../../../assets/loginIcon.svg";
import eyeIcon from "../../../assets/viewIcon.svg";
import GridTable from "../../../Components/GridTable/GridTable";
import addIcon from "../../../assets/addIcon.svg";
import littleAdventureIcon from "../../../assets/littleAdventure.svg";
import Select from "react-select";
import downIcon from "../../../assets/downIcon.svg";
import { commonAxios } from "../../../Axios/service";
import city from "../../../assets/epost.png";
import { SiMicrosoftexcel } from "react-icons/si";
import { AsyncDownloadCSV } from "../../../Components/DownloadCsv/DownloadCSV";
import moment from "moment";
import Loader from "../../../Components/Loader/Loader";
import * as Yup from "yup";
import { failureNotify, successNotify } from "../../../Components/Toast/Toast";
import { useFormik } from "formik";
import modaldeleteIcon from "../../../assets/Delete.png";
import { RxCross2 } from "react-icons/rx";

const Customerlist = () => {
  const [selectedOption, setSelectedOption]: any = useState(null);
  const [selectStatus, setSelectStatus]: any = useState(null)
  const [isloading, setIsLoading]: any = useState(false);
  const [filterDatum, setFilterDatum] = useState("");
  const [customerData, setCustomerData] = useState<any>("");
  const [rowData, setData]: any = useState([]);
  const [newFilterData, setNewFilterData] = useState<any>([]);
  const [isActionModalOpen, setIsActionModalOpen] = useState<any>(false);
  const [customerDatas, setCustomerDatas] = useState<any>("");
  const [isDeleteCustomerModalOpen, setIsDeleteCustomerModalOpen] =
    useState<boolean>(false);
  const [modalPopup, setModalPopup] = useState(false)
  const [checked, setChecked] = useState('')
  const [active, setActive] = useState(false);
  const [modalCustomer, setModalCustomer] = useState(false);
  const [customerDelete, setCustomerDelete] = useState<any>([]);
  const [isDeleteCustomer, setIsDeleteCustomer] = useState(false)
  const [values, setValues] = useState<any>(null);
  const gridOptions = {
    getRowHeight: function (params: any) {
      return 80;
    },
  };
  const [rowHeight, setRowHeight] = useState(60);

  const getCustomerDetails = async () => {

    setIsLoading(true);
    try {
      const response = await commonAxios.get(`/cemo/appuser/all/customers`);

      if (response.data) {
        let filteredUsers = response.data.filter((user: any) => {
          let users = user.email !== 'anonymous@cemo.in'
          return users

        })

        setData(filteredUsers);
        setNewFilterData(filteredUsers);

      }
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  const [columnDefs]: any = useState([
    {
      headerName: "namn",
      cellRendererFramework: (props: any) => {
        return (
          <>
            <div className="bg-[#D6B27D] w-10 h-10 rounded-lg flex items-center justify-center font-bold">
              <span className="text-white">
                {props.data?.firstName[0].toUpperCase() +
                  props.data?.lastName[0].toUpperCase()}
              </span>
            </div>
            <p className="ml-3">{props.data?.firstName}</p>
            {
              props.data.accountStatus === 'ACTIVE' ? <div className="w-2 h-2 bg-green-800 rounded-md mx-2"></div> : <div className="w-2 h-2 bg-red-800 rounded-md mx-2"></div>
            }
          </>
        );
      },
      width: 250,
    },
    { headerName: "E-post", field: "email", width: 250 },
    { headerName: "Telefonnummer", field: "telephone", width: 150 },
    { headerName: "Stad", field: "businessCity", width: 150 },
    { headerName: "Status", field: "accountStatus", width: 150 },
    {
      headerName: "Åtgärder",
      cellRendererFramework: (params: any) => {
        const handleButtonClick = (data: any) => {
          setCustomerData(data.data);
          setIsActionModalOpen(true);
        };
        const handleAddProduct = () => {
          setIsLoading(true);
          setCustomerData(params.data);
          setModalCustomer(true);
          setActive(params.data.accountStatus);
          setChecked(params.data.accountStatus)
        };

        const handleEventDelete = () => {
          setCustomerDelete(params.data);
          setIsDeleteCustomer(true)

        }
        const handleEventInactive = () => {

          setCustomerDatas(params.data);
          setIsDeleteCustomerModalOpen(true);
        };
        return (
          <>
            <div className="flex flex-row justify-between items-center">
              <button
                onClick={() => {
                  handleButtonClick(params);
                }}
                className="mr-4"
              >
                <img
                  src={eyeIcon}
                  className=" border-[#E2E2EA] p-1"
                />
              </button>
              <button onClick={handleAddProduct}>
                <img
                  src={editIcon}
                  className=" mx-2 border-[#E2E2EA]  p-1 "
                />
              </button>
              {
                params.data.accountStatus === 'ACTIVE' && <button onClick={handleEventInactive}>
                  <img src={Inactive} className=" mx-2 border-[#E2E2EA]  p-1"
                    width={32}
                    height={32} />
                </button>
              }
              {
                params.data.accountStatus === 'INACTIVE' && <button onClick={handleEventDelete}>
                  <img
                    src={eventdeleteicon}
                    className="border mx-4 border-[#E2E2EA] rounded-[10px] p-1"
                    width={32}
                    height={32}
                  />
                </button>
              }

            </div>
          </>
        );
      },
      width: 300,
    },
  ]);

  useEffect(() => {
    getCustomerDetails();
  }, []);
  let customerCity: any = [];
  rowData.forEach((val: any) => {
    if (
      val.businessCity !== null &&
      !customerCity.includes(val.businessCity?.toLowerCase())
    ) {
      customerCity.push(val.businessCity.toLowerCase());
    }
  });

  const options = customerCity.map((val: any) => {
    return {
      label: val.charAt(0).toUpperCase() + val.slice(1).toLowerCase(),
      value: val,
    };
  });

  let customerStatus: any = []
  rowData.forEach((val: any) => {
    if (!customerStatus.includes(val.accountStatus.toUpperCase())) {
      customerStatus.push(val.accountStatus.toUpperCase())
    }
  })

  const statusOptions = customerStatus.map((val: any) => {
    return {
      label: val,
      value: val
    }
  })

  const CustomDropdownIndicator = () => (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        style={{ width: "24px", height: "24px",fill: '#A09792' }}
      >
        <path fill="none" d="M0 0h24v24H0z" />
        <path d="M7 10l5 5 5-5z" />
      </svg>
    </div>
  );

  const customComponents: any = {
    DropdownIndicator: CustomDropdownIndicator,
  };

  const getCustomerList = () => {
    return newFilterData.map((data: any) => {
      return getEventDownloadInfo(data)
    });
  };

  function getEventDownloadInfo(data: any) {

    return {
      "FirstName": data.firstName,
      "LastName": data.lastName,
      // "Mobile": data.mobile,
      "Telephone": data.telephone,
      "Email": data.email,
      "Address": data.businessAddress,
      "City": data.businessCity,
    };
  }


  let filterData = rowData.filter((searchTerm: any) => {
    let filteredValue = searchTerm.firstName.toLowerCase().includes(filterDatum.toLowerCase()) || searchTerm.email.toLowerCase().includes(filterDatum.toLowerCase()) || searchTerm?.telephone?.toLowerCase().includes(filterDatum.toLowerCase()) || searchTerm.businessCity?.toLowerCase().includes(filterDatum.toLowerCase()) || searchTerm.accountStatus.toLowerCase().includes(filterDatum.toLowerCase());
    let status = !selectStatus || searchTerm.accountStatus?.toLowerCase() === selectStatus?.value?.toLowerCase();
    let citySelected = !values || searchTerm.businessCity?.toLowerCase() === values?.value?.toLowerCase();
    return filteredValue && status && citySelected;
  })

  const customStyles = {
    control: (provided: any, state: any) => ({
      ...provided,
      border: "1px solid #ccc",
      borderRadius: "4px",
      fontSize: "13px",
      width: "150px",
    }),
    option: (provided: any, state: any) => ({
      ...provided,
      backgroundColor:"transparent",
      fontSize: "13px",
      width: "150px",
      color: state.isSelected ? "#D6B27D" : "gray",
    }),
    singleValue: (provided: any) => ({
        ...provided,
        color: "gray", // Set the initial text color to gray
      }),
      valueContainer: (provided: any) => ({
        ...provided,
        color: "gray", // Set the initial text color to gray
      }),
  };

  const handlecitychange = (selectedOption: any) => {
    setValues(selectedOption);
  };

  const handleStatuschange = (event: any) => {
    setSelectStatus(event)
  }

  const EditCustomerPopup = () => {
    setIsLoading(false);
    const handleEditSubmit = async (values: any) => {

      let customerBody = {
        ...values,
        // status: active === true ? "ACTIVE" : "INACTIVE",
        accountStatus: customerData.accountStatus,
        businessAddress: customerData.businessAddress,
        businessCity: customerData.businessCity,
        businessName: customerData.businessName,
        businessNumber: customerData.businessNumber,
        businessPinCode: customerData.businessPinCode,
        categories: customerData.categories,
        createdAt: customerData.createdAt,
        email: customerData.email,
        id: customerData.id,
        resetToken: customerData.resetToken,
        updatedAt: customerData.updatedAt,
        profileImage: customerData.profileImage,
      };

      try {
        const response = await commonAxios.put(
          `/cemo/appuser/update/${customerData.id}`,
          customerBody
        );

        if (response.data) {
          successNotify("Customer updated successfully");
          setCustomerData([]);
          setModalCustomer(false);
          getCustomerDetails();
        }
      } catch (error) {
        failureNotify("oops! something went wrong");
      } finally {
        setIsLoading(false);
      }
    };





    const ModalPopup = () => {

      const handleActivatetheCustomer = async () => {
        setIsLoading(true)
        if (customerData.accountStatus === "INACTIVE") {
          try {
            const response = await commonAxios.post(
              `/cemo/appuser/activate/supplier/${customerData.id}/ACTIVE`,
            )
            if (response.data) {
              if (response.data.accountStatus === 'ACTIVE') {
                successNotify(`Customer account Activated`)
              }
              else {
                successNotify(`Customer account Deactivated`)
              }
              setModalPopup(false)
              setIsLoading(false)
              setCustomerData(response.data)
              getCustomerDetails()
              setChecked('ACTIVE')
            }
          }
          catch (e) {

          }
        } else {
          try {
            const response = await commonAxios.post(
              `/cemo/appuser/activate/supplier/${customerData.id}/INACTIVE`,
            )
            if (response.data) {
              if (response.data.accountStatus === 'ACTIVE') {
                successNotify(`Customer account Activated`)
              }
              else {
                successNotify(`Customer account Deactivated`)
              }
              setModalPopup(false)
              setIsLoading(false)
              setCustomerData(response.data)
              getCustomerDetails()
              setChecked('INACTIVE')
            }
          }
          catch (e) {

          }
        }
      }


      const handleClosePopup = () => {
        setModalPopup(false)
        // setChecked(!checked)
      }
      return (
        <>
          <div className="w-[100%] h-full bg-[#00000080] absolute z-[100] right-0 flex justify-center items-center">
            <section className="bg-white h-[30%] w-[50%] flex flex-col items-center justify-center " >
              <div >
                {
                  customerData.accountStatus === "ACTIVE" ? <p className="text-center">Do you want to deactivate the Customer account?</p> : <p className="text-center">Do you want to activate the Customer account?
                  </p>
                }
              </div>
              <div className="flex">
                <button
                  type="submit"
                  // disabled={loading}
                  className="px-4 py-2 md:mr-4 bg-[#D6B27D] rounded-lg mt-6 text-md font-semibold cursor-pointer text-white"
                  onClick={handleActivatetheCustomer}
                >
                  Ja
                </button>
                <button
                  type="submit"
                  // disabled={loading}
                  className="px-4 py-2 md:mr-4 bg-[#D6B27D] rounded-lg mt-6 text-md font-semibold cursor-pointer text-white"
                  onClick={handleClosePopup}
                >
                  Nej
                </button>
              </div>

            </section >
          </div>
        </>
      )
    }


    const formik = useFormik<any>({
      initialValues: {
        firstName: customerData.firstName,
        lastName: customerData.lastName,
        mobile: customerData.mobile,
        email: customerData.email,
        telephone: customerData.telephone,
        address: customerData.businessAddress,
        city: customerData.businessCity
      },
      validationSchema: Yup.object({
        // firstName: Yup.string().required("Required"),
        // lastName: Yup.string().required("Required"),
        // mobile: Yup.string()
        // .transform((value, originalValue) => {
        //   // Remove spaces from the phone number
        //   return originalValue.replace(/\s/g, '');
        // }).matches(/^\+46\d{9}$/, 'Invalid phone number, alike (+46 xx xxx xxxx)')
        // .required("Required"),
        email: Yup.string().email("Enter a valid Email").required("Obligatorisk"),
        // telephone: Yup.string()
        // .transform((value, originalValue) => {
        //   // Remove spaces from the phone number
        //   return originalValue.replace(/\s/g, '');
        // }).matches(/^\+46\d{9}$/, 'Invalid phone number, alike (+46 xx xxx xxxx)')
        // .required("Required"),
        // businessName: Yup.string().required("Required"),
        // businessAddress: Yup.string().required("Required"),
        // businessNumber: Yup.string().required("Required"),
        // businessPinCode: Yup.string()
        // .matches(/^\d{5}$/, 'Invalid Post Number')
        // .required('Post Number is required'),
        // businessCity: Yup.string().required("Required"),
      }),
      onSubmit: (values) => handleEditSubmit(values),
    });

    const handleChangeStatus = (e: any) => {
      setActive(e.target.checked);
    };

    const handleToggle = (e: any) => {
      setModalPopup(true)

      // setChecked(e.target.checked ? 'ACTIVE' : 'INACTIVE');
    };

    return (

      <div className="w-full h-full bg-[#00000080] absolute z-40 right-0">
        {modalPopup ? <ModalPopup /> : null}
        <section className="w-full h-full flex flex-col items-center justify-center">
          <form
            onSubmit={formik.handleSubmit}
            className="bg-[#ffffff] w-[60rem] h-[30rem] overflow-y-scroll px-8 py-6 relative"
          >
            <div className="flex flex-row justify-between items-center my-6">
              <p className="text-secondary font-bold text-2xl">
                {" "}
                Edit Customer
              </p>
              <label htmlFor="toggle" className="flex items-center cursor-pointer">
                <div className="relative">
                  <input
                    type="checkbox"
                    id="toggle"
                    className="sr-only"
                    checked={checked === 'ACTIVE' ? true : false}
                    onChange={handleToggle}
                  />
                  {
                    checked === "ACTIVE" ? <div className="block bg-[#D6B27D] w-10 h-6 rounded-full"></div> : <div className="block bg-gray-300 w-10 h-6 rounded-full"></div>
                  }

                  <div
                    className={`dot absolute left-1 top-1 bg-white w-4 h-4 rounded-full transition ${checked === "ACTIVE" ? 'translate-x-full' : ''
                      }`}
                  ></div>
                </div>

                <div className="ml-3 text-gray-700 font-medium">Active</div>


              </label>
              <button
                onClick={() => {
                  setCustomerData([]);
                  setModalCustomer(false);
                }}
              >
                <img
                  src={modaldeleteIcon}
                  className="absolute top-5 right-5 h-6 w-6"
                />
              </button>
            </div>
            <section className="flex flex-row justify-between items-center">
              <div className="w-full mr-10">
                <label
                  className="text-secondary text-sm font-bold"
                  htmlFor="firstName"
                >
                  Förnamn
                </label>
                <input
                  type="text"
                  name="firstName"
                  id="firstName"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="outline-none border my-2 w-full mb-5 px-4 py-2 text-gray-600
                  rounded-[10px]"
                  value={formik.values.firstName}
                />
                {formik.errors.firstName ? (
                  <span className="text-red-500 font-medium">
                    {/* {formik.errors.firstName} */}
                  </span>
                ) : null}
              </div>
              <div className="w-full">
                <label
                  className="text-secondary text-sm  font-bold"
                  htmlFor="lastName"
                >
                 Efternamn
                </label>
                <input
                  type="text"
                  name="lastName"
                  id="lastName"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="outline-none border my-2 w-full mb-5 px-4 py-2 text-gray-600
                  rounded-[10px]"
                  value={formik.values.lastName}
                />
                {formik.errors.lastName ? (
                  <span className="text-red-500 font-medium">
                    {/* {formik.errors.lastName} */}
                  </span>
                ) : null}
              </div>
            </section>
            <section className="flex flex-row justify-between items-center">
              <div className="w-full mr-10">
                <label
                  className="text-secondary text-sm font-bold"
                  htmlFor="mobile"
                >
                 Telefonnummer  
                </label>
                <input
                  type="text"
                  name="mobile"
                  id="mobile"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="outlinEdit Customere-none border my-2 w-full mb-5 px-4 py-2 text-gray-600
                  rounded-[10px]"
                  value={formik.values.mobile}
                />
                {formik.errors.mobile ? (
                  <span className="text-red-500 font-medium">
                    {/* {formik.errors.mobile} */}
                  </span>
                ) : null}
              </div>
              <div className="w-full">
                <label
                  className="text-secondary text-sm font-bold"
                  htmlFor="email"
                >
                E-Post
                </label>
                <input
                  type="email"
                  name="email"
                  id="email"
                  disabled={true}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="outline-none border  my-1 w-full mb-5 lowercase px-4 py-2 text-gray-600
                  rounded-[10px]"
                  value={formik.values.email}
                />
                {formik.errors.email ? (
                  <span className="text-red-500 font-normal">
                    {/* {formik.errors.email} */}
                  </span>
                ) : null}
              </div>
            </section>
            <section className="flex flex-row justify-between items-center">
              <div className="w-full mr-10">
                <label
                  className="text-secondary text-sm font-bold"
                  htmlFor="telephone"
                >
                 Telefon
                </label>
                <input
                  type="text"
                  name="telephone"
                  id="telephone"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="outline-none border my-2 w-full mb-5 px-4 py-2 text-gray-600
                  rounded-[10px]"
                  value={formik.values.telephone}
                />
                {formik.errors.telephone ? (
                  <span className="text-red-500 font-medium">
                    {/* {formik.errors.mobile} */}
                  </span>
                ) : null}
              </div>
              <div className="w-full">
                <label
                  className="text-secondary text-sm font-bold"
                  htmlFor="address"
                >
                  Adress
                </label>
                <input
                  type="text"
                  name="address"
                  id="address"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="outline-none border my-2 w-full mb-5 px-4 py-2 text-gray-600
                  rounded-[10px]"
                  value={formik.values.address}
                />
                {formik.errors.address ? (
                  <span className="text-red-500 font-medium">
                    {/* {formik.errors.lastName} */}
                  </span>
                ) : null}
              </div>
              {/* <div className="w-1/2">
                <label
                  className="text-[#97A0C3] text-xs font-bold"
                  htmlFor="telephone"
                >
                  Status
                </label>
                <div>
                  <label
                    className="text-[#97A0C3] text-xs font-bold"
                    htmlFor="telephone"
                  >
                    Active
                  </label>
                  <input
                    type="checkbox"
                    name="status"
                    id="status"
                    onChange={handleChangeStatus}
                    onBlur={formik.handleBlur}
                    className="outline-none border rounded-lg ms-10 mt-5"
                    value={formik.values.status}
                    checked={active}
                  />
                  {formik.errors.telephone ? (
                    <span className="text-red-500 font-medium">
                      {/* {formik.errors.telephone} */}
              {/* </span>
                  ) : null}
                </div>
              </div> */}

            </section>
            <section>
              <div className="w-[48%] mr-10">
                <label
                  className="text-[rgb(140,126,121)] text-sm font-bold"
                  htmlFor="city"
                >
                  Stad
                </label>
                <input
                  type="text"
                  name="city"
                  id="city"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="outline-none border my-2 w-full mb-5 px-4 py-2 text-gray-600
                  rounded-[10px]"
                  value={formik.values.city}
                />
                {formik.errors.city ? (
                  <span className="text-red-500 font-medium">
                    {/* {formik.errors.mobile} */}
                  </span>
                ) : null}
              </div>
            </section>
            <div className="flex justify-center items-center">
              <button
                type="submit"
                // disabled={loading}
                className="px-6 py-2 bg-[#D6B27D] rounded-lg mt-6 text-md font-semibold cursor-pointer text-white uppercase"
              >
                Spara
              </button>
            </div>
          </form>
        </section>
      </div>
    );
  };

  const DeleteEventModal = () => {
    const handleclose = () => {
      setIsDeleteCustomerModalOpen(false);
      setCustomerData([]);
      setCustomerDatas("");
    };

    const deleteEvent = async () => {
      setIsLoading(true);
      try {
        const response = await commonAxios.delete(
          `/cemo/appuser/delete?userId=${customerDatas.id}&status=INACTIVE`
        );
        if (response.status === 200) {

          successNotify(`${customerDatas.firstName} Deactivated succesfully`);
          handleclose();
          getCustomerDetails();
        }
      } catch (error) {
        failureNotify("oops! something went wrong");
      } finally {
        setIsLoading(false);
      }
    };

    return (
      <div className="w-[85%] h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <div className="bg-white rounded-lg p-6 flex flex-col justify-center items-center w-1/2 h-72 relative">
            <button onClick={handleclose}>
              <img
                src={deleteIcon}
                className="absolute top-5 right-5 h-6 w-6"
              />
            </button>
            <p className="font-semibold text-lg text-secondary text-center">
              Are you sure you want to Inactivate the customer account "{customerDatas.firstName}"?
            </p>
            <section className="flex flex-row justify-between items-center w-[40%] gap-8 mt-10 mr-10">
              <button
                onClick={deleteEvent}
                className="px-6 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer uppercase"
              >
                Ja
              </button>
              <button
                onClick={handleclose}
                className="px-6 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer"
              >
                NO
              </button>
            </section>
          </div>
        </section>
      </div>
    );
  };

  const DeleteCustomer = () => {

    const handleclose = () => {
      setIsDeleteCustomer(false);
      setCustomerDelete([])
    };

    const deleteEvent = async () => {
      setIsLoading(true);
      try {
        const response = await commonAxios.delete(
          `/cemo/appuser/delete?userId=${customerDelete.id}&status=DELETE`
        );
        if (response.status === 200) {



          successNotify(`${customerDelete.firstName} deleted succesfully`);
          handleclose();
          getCustomerDetails();
        }
      } catch (error) {
        failureNotify("oops! something went wrong");
      } finally {
        setIsLoading(false);
      }
    };

    return (
      <div className="w-[85%] h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <div className="bg-white rounded-lg p-6 flex flex-col justify-center items-center w-1/2 h-72 relative">
            <button onClick={handleclose}>
              <img
                src={deleteIcon}
                className="absolute top-5 right-5 h-6 w-6"
              />
            </button>
            <p className="font-semibold text-lg text-secondary text-center">
              Are you sure you want to Delete the customer account "{customerDelete.firstName}"?
            </p>
            <section className="flex flex-row justify-between items-center w-[40%] gap-8 mt-10 mr-10">
              <button
                onClick={deleteEvent}
                className="px-6 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer"
              >
                Ja
              </button>
              <button
                onClick={handleclose}
                className="px-6 py-2 rounded-lg bg-[#FFE5D8] text-secondary font-semibold text-base cursor-pointer"
              >
                NO
              </button>
            </section>
          </div>
        </section>
      </div>
    );
  };

  const CustomerDetails = () => {
    return (
      // <section className="flex flex-col h-[35rem] items-center justify-around">
      <div className="w-full h-full bg-[#00000080] absolute z-40 right-0 pt-16">
        <section className="flex flex-col h-[35rem] items-center justify-around">
          <div className="bg-white py-10 px-10 relative w-[800px] h-[40rem] mt-40">
            <button onClick={() => setIsActionModalOpen(false)}>
              <img
                src={deleteIcon}
                className="absolute top-5 right-5 h-6 w-6"
              />
            </button>
            <h2 className=" text-xl font-bold font-[sofiapro] text-center text-secondary">
              Kunduppgifter
            </h2>
            <div className="grid grid-cols-1 gap-4">
              <div className="p-4">
                <div className="border p-3 rounded-lg mt-1">
                  <div className="grid grid-cols-2 gap-4">
                    <div className="p-4">
                      <div className="">
                        <h1 className="font-semibold text-secondary">Förnamn</h1>
                        <h1 className="text-secondary">{customerData?.firstName}</h1>
                      </div>
                    </div>
                    <div className="p-4">
                      <div className="">
                        <h1 className="font-semibold text-secondary">Efternamn</h1>
                        <h1 className="text-secondary">{customerData?.lastName}</h1>
                      </div>
                    </div>
                  </div>
                  <div className="grid grid-cols-2 gap-4">
                    <div className="p-4">
                      <div className="">
                        <h1 className="font-semibold text-secondary">Telefonnummer</h1>
                        <h1 className="text-secondary">{customerData?.mobile}</h1>
                      </div>
                    </div>
                    <div className="p-4">
                      <div className="">
                        <h1 className="font-semibold text-secondary">E-Post</h1>
                        <h1 className="text-secondary">{customerData?.email}</h1>
                      </div>
                    </div>
                  </div>
                  <div className="grid grid-cols-2 gap-4">
                    <div className="p-4">
                      <div className="">
                        <h1 className="font-semibold text-secondary">Telefon</h1>
                        <h1 className="text-secondary">{customerData?.telephone || "-"}</h1>
                      </div>
                    </div>
                    <div className="p-4">
                      <div className="">
                        <h1 className="font-semibold text-secondary">Status</h1>
                        <h1 className="text-secondary">{customerData?.accountStatus}</h1>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* {
                customerData?.booking?.bookingStatus !== 'REJECTED' ? (
                  <div className="p-4">
                    <h1 className='-mt-3 font-bold'>Booking Action</h1>
                    <div className='p-3 rounded-lg mt-1'>
                      <div className="grid grid-cols-1 gap-4">
                        <section>
                          <button onClick={() => updateOrderStatus(true)} className='px-4 py-2 bg-green-700 rounded-md text-white font-semibold cursor-pointer'>
                            Accept the order
                          </button>
                        </section>
                        <section>
                          <button onClick={() => updateOrderStatus(false)} className='px-4 py-2 bg-red-700 rounded-md text-white font-semibold cursor-pointer'>
                            Reject the order
                          </button>
                        </section>
                      </div>
                    </div>
                  </div>
                ) : null
              } */}
            </div>
          </div>
        </section>
      </div>
    );
  };

  return (
    <div className="flex">
      {isActionModalOpen ? <CustomerDetails /> : null}
      {isDeleteCustomerModalOpen ? <DeleteEventModal /> : null}
      {isDeleteCustomer ? <DeleteCustomer /> : null}
      {modalCustomer ? <EditCustomerPopup /> : null}
      <SideNav />
      <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
        <AdminHeader />
        <div className="mx-10 my-6">
          {isloading && <Loader />}
          <div className="flex justify-between py-4">
            <h1 className="text-secondary text-[24px] font-semibold">
              Kundlista
            </h1>
            <AsyncDownloadCSV
              data={getCustomerList}
              asyncOnClick={true}
              filename={
                "customer-list" + moment(new Date()).format("DD/MM/YYYY")
              }
              onClick={() => { }}
            >
              <SiMicrosoftexcel className="w-6 h-6 text-[#D6B27D] hover:text-[#F7CB73] hover:cursor-pointer mt-2" />
            </AsyncDownloadCSV>
          </div>
          <div className="border border-[#FFFFFF] bg-[#ffff] rounded-[20px] p-4 mt-1">
            <div className="flex">
              <div className="relative w-[93%]">
                <input
                  type="text"
                  className=" w-[98%] h-[38px] pl-10 pr-5 border rounded-md border-[#E2E2EA] focus:outline outline-[#E2E2EA]  focus:border-transparent "
                  placeholder="Sök efter namn, leverantör, mobil etc..."
                  onChange={(e) => {
                    setFilterDatum(e.target.value);

                  }}
                  value={filterDatum}
                />
                <div className="absolute inset-y-0 left-0 flex items-center pl-3">
                  <img src={searchIcon} />
                </div>
              </div>
              <div className="mx-4">
                <Select
                  options={statusOptions}
                  placeholder={"Status"}
                  styles={customStyles}
                  defaultValue={selectStatus}
                  value={selectStatus}
                  onChange={handleStatuschange}
                  components={customComponents}
                  isClearable
                />
              </div>
              <div className="">
                <Select
                  options={options}
                  placeholder={"Stad"}
                  styles={customStyles}
                  value={values}
                  defaultValue={selectedOption}
                  onChange={handlecitychange}
                  components={customComponents}
                  isClearable
                />
              </div>

            </div>
            <div className="mt-6">
              <GridTable
                columnDefs={columnDefs}
                gridOptions={{
                  ...gridOptions,
                  getRowHeight: () => rowHeight,
                }}
                rowData={filterData}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Customerlist;
