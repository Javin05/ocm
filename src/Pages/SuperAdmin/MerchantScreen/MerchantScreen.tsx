import React, { ChangeEvent, useEffect } from "react";
import AdminHeader from "../../../Components/AdminHeader/AdminHeader";
import SideNav from "../../../Components/SideNav/SideNav";
import searchIcon from "../../../assets/adminSearch.svg";
import { useState } from "react";
import fileupload from "../../../assets/file-upload.svg";
import deleteIcon from "../../../assets/deleteIcon.svg";
import closeIcon from "../../../assets/Delete.png";
import modaldeleteIcon from "../../../assets/Delete.png";
import editIcon from "../../../assets/editIcon.svg";
import loginIcon from "../../../assets/loginIcon.svg";
import GridTable from "../../../Components/GridTable/GridTable";
import Inactive from "../../../assets/2d65659a9d.png"
import addIcon from "../../../assets/addIcon.svg";
import Select from "react-select";
import { commonAxios } from "../../../Axios/service";
import { useFormik } from "formik";
import * as Yup from "yup";
import { AsyncDownloadCSV } from "../../../Components/DownloadCsv/DownloadCSV";
import Loader from "../../../Components/Loader/Loader";
import { failureNotify, successNotify } from "../../../Components/Toast/Toast";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../Redux/store";
import { SiMicrosoftexcel } from "react-icons/si";
import moment from "moment";
import { RxCross2 } from "react-icons/rx";
import { setUserDetails } from "../../../Redux/features/userSlice";
import { setRefreshToken, setToken } from "../../../Utils/auth";
import { useNavigate } from "react-router-dom";
import chooseCity from "../../../assets/city.png";

type initialAddMerchantModal = {
  firstName: string;
  lastName: string;
  mobile: string;
  accountNumber: string;
  bankName: string;
  commission: string;
  email: string;
  telephone: string;
  businessName: string;
  businessAddress: string;
  businessNumber: string;
  businessPinCode: string;
  businessCity: string;
  categories: string[];
};

const MerchantScreen = () => {
  let base64Image: string | undefined;
  const [selectedOption, setSelectedOption]: any = useState(null);
  const [modalPopup, setModalPopup] = useState(false)
  const [checked, setChecked] = useState<any>('');
  const [isloading, setIsLoading]: any = useState(false);
  const [options, setOptions] = useState([{}]);
  const [formStep, setFormStep] = useState(1);
  const [cityOptions, setCityOptions] = useState([{}]);
  const [filterData, setFilterData] = useState<any>("");
  const [isAddMerchantModalOpen, setIsAddMerchantModalOpen] = useState(false);
  const [file, setFile] = useState<File | null>(null);
  const [merchantData, setMerchantData] = useState<any>([]);
  const [content, setContent] = useState("");
  const dispatch = useDispatch();
  const naviagte = useNavigate();
  const [deletePopup, setDeletePopup] = useState(false);
  const [stepOneFormData, setStepOneFormData] = useState<any>(null);
  const [cityChoose, setCityChoose] = useState<any>(null);
  const [selectedCity, setSelectedCity] = useState(null);
  const [customerDatas, setCustomerDatas] = useState<any>("");
  const [isDeleteCustomerModalOpen, setIsDeleteCustomerModalOpen] =
    useState<boolean>(false);
  const gridOptions = {
    getRowHeight: function (params: any) {
      return 80;
    },
  };
  const [rowHeight, setRowHeight] = useState(60);

  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [base64MerchantImage, setBase64MerchantImage] = useState("");
  const [rowData, setData]: any = useState([]);
  const [filteredRowData, setFilteredRowData] = useState<any>([]);
  const [selectStatus, setSelectStatus]: any = useState(null);
  const [values, setValues] = useState<any>(null);
  const [newFilterData, setNewFilterData] = useState<any>([]);


  const getMerchantDetails = async () => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get(`/cemo/appuser/all/suppliers`);

      if (response.data) {

        const sanitiseBusinessCity = (city: any) => {
          if (!city) return 'Not specified'
          if (city === "") 'Empty'

          return city.trim();
        }
        var serialNo = 1;
        const gridData = response.data.map((item: any, index: number) => {
          if (item.accountStatus !== "REQUESTED") {
            return {
              ...item,
              businessCity: sanitiseBusinessCity(item.businessCity),
              Sno: serialNo++,
            };
          }
        }).filter((row: any) => row)
        setData(gridData)
        setNewFilterData(gridData);
      }
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  const getEventCategories = async () => {
    setIsLoading(true);
    try {
      const response = await commonAxios.get("/cemo/categories");
      if (response.status === 200) {
        // Filter categories with status 'ACTIVE'
        const activeCategories = response.data.filter((category: any) => category.status === "ACTIVE");

        const categoriesList = activeCategories.map((category: any) => ({
          value: category.categoryName,
          label: category.categoryName,
          id: category.id,
        }));

        setOptions(categoriesList);
      }
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  const getEventCities = async () => {
    const response = await commonAxios.get("/cemo/cities");

    if (response.status === 200) {
      const citiesList = response.data?.map((city: any) => {
        return {
          value: city.city,
          label: city.city,
          id: city.id,
        };
      });
      setCityChoose(citiesList);
    }
  };

  const [columnDefs]: any = useState([
    { headerName: "SNI?", field: "Sno", width: 70 },
    { headerName: "Leverantörsnamn", field: "businessName", width: 150 },
    {
      headerName: "Kontaktperson",
      cellRendererFramework: (props: any) => {
        return (
          <>
            <div className={`${props.data?.profileImage && props.data?.profileImage != null ? `bg-white` : `bg-[#D6B27D]`}  w-6 h-6 rounded-lg flex items-center justify-center font-bold`}>
              {
                props.data?.profileImage && props.data?.profileImage != null ?
                  <img src={props.data?.profileImage} className="h-[full] w-[full] rounded-sm " /> : <span className="text-white">
                    {props.data?.firstName[0].toUpperCase() +
                      props.data?.lastName[0].toUpperCase()}
                  </span>
              }
            </div>
            <p className="ml-3">{props.data?.firstName[0].toUpperCase() + props.data?.firstName.slice(1) + " " + props.data?.lastName[0].toUpperCase() + props.data?.lastName.slice(1)}</p>
          </>
        );
      },
      width: 260,
    },
    { headerName: "Adress", field: "businessAddress", width: 220 },
    { headerName: "Stad", field: "businessCity", width: 140 },
    { headerName: "Telefonnummer", field: "mobile", width: 120 },
    { headerName: "Status", field: "accountStatus", width: 120 },
    { headerName: "Account Number", field: "accountNumber", width: 120 },
    { headerName: "Bank Name", field: "bankName", width: 120 },
    { headerName: "Commission", field: "commission", width: 120 },

    {
      headerName: "Åtgärd",
      cellRendererFramework: (props: any) => {
        const handleEdit = () => {

          setIsAddMerchantModalOpen(true);
          setFormStep(1);
          setMerchantData(props.data);
          setContent("editmerchant");
          setChecked(props.data.accountStatus)
        };

        const handleEventInactive = () => {
          setCustomerDatas(props.data);
          setIsDeleteCustomerModalOpen(true);
        };

        const loginAsMerchant = async (values: any) => {

          setIsLoading(true);
          try {
            values.shadowLogin = true;
            const loginRequest = {
              shadowLogin: true,
              password: values.password,
              email: values.email
            }
            const response = await commonAxios.post("/cemo/auth/shadow/login", loginRequest);

            if (response.data.role === "ROLE_SUPPLIER") {
              setIsLoading(false);
              // successNotify("Login successful");
              dispatch(setUserDetails(response.data));
              setToken(response.data?.accessToken);
              setRefreshToken(response.data?.refreshToken);
              localStorage.setItem("shadowToken", response.data?.accessToken);
              naviagte("/orderList", { replace: true });
              successNotify("Nu kan du fortsätta som leverantör !", 'bottom');
            } else {
              failureNotify("Login as merchant failed.!");
            }
          } catch (error) {
            setIsLoading(false);
            failureNotify("Account is Inactive please Activate to Login");
          } finally {
            setIsLoading(false);
          };



        }
        return (
          <>
            <button>
              <img
                src={loginIcon}
                onClick={() => loginAsMerchant(props.data)}
                className="bg-[#D6B27D] p-1 rounded-[10px]"
              />
            </button>
            <button>
              <img
                onClick={handleEdit}
                src={editIcon}
                className=" mx-2  border-[#E2E2EA]  p-1 "
              />
            </button>
            {props.data.accountStatus === 'ACTIVE' && <button onClick={handleEventInactive}>
              <img src={Inactive} className=" mx-2 border-[#E2E2EA]  p-1"
                width={32}
                height={32} />
            </button>
            }
            {
              props.data.accountStatus === 'INACTIVE' && <button onClick={() => {
                setDeletePopup(true);
                setMerchantData(props.data);
              }}
              >
                <img
                  src={deleteIcon}
                  className="border border-[#E2E2EA] rounded-[10px] p-1 "
                />
              </button>
            }
            {/* <button
              onClick={() => {
                setDeletePopup(true);
                setMerchantData(props.data);
              }}
            >
              <img
                src={deleteIcon}
                className="border border-[#E2E2EA] rounded-[10px] p-1 "
              />
            </button> */}
          </>
        );
      },
      width: 200,
    },
  ]);

  useEffect(() => {
    if (rowData?.length > 0) {
      let filteredCities: any = [];

      rowData.forEach((row: any) => {
        if (row && !filteredCities.includes(row?.businessCity?.trim().toLowerCase())) {
          filteredCities.push(row?.businessCity?.trim().toLowerCase());
        }
      })

      const filterCities = filteredCities.map((item: any) => {
        return {
          label: item.charAt(0).toUpperCase() + item.slice(1).toLowerCase(),
          value: item,
        };
      });

      setCityOptions(filterCities);
    }
    getEventCities();
  }, [rowData])

  useEffect(() => {

    if (rowData?.length > 0 && filterData?.length > 0) {
      const data = rowData.filter((item: any) => {
        const matchesInput =
          item?.firstName?.toLowerCase().includes(filterData?.toLowerCase()) ||
          item?.businessAddress?.toLowerCase().includes(filterData?.toLowerCase()) ||
          item?.businessCity?.toLowerCase().includes(filterData?.toLowerCase()) ||
          item?.mobile?.toLowerCase().includes(filterData?.toLowerCase()) ||
          item?.accountNumber?.toLowerCase().includes(filterData?.toLowerCase()) ||
          item?.bankName?.toLowerCase().includes(filterData?.toLowerCase()) ||
          item?.commission?.toLowerCase().includes(filterData?.toLowerCase()) ||
          item?.accountStatus?.toLowerCase().includes(filterData?.toLowerCase())
        const matchesDropdown = values
          ? item?.businessCity?.toLowerCase() === values?.value?.toLowerCase()
          : true;
        return matchesInput && matchesDropdown;
      });

      setNewFilterData(data)
    } else {
      setNewFilterData(rowData)
    }
  }, [rowData, filterData, values]);

  useEffect(() => {
    if (rowData?.length > 0) {
      const data = rowData.filter((item: any) => {
        const matchesDropdown = values
          ? item?.businessCity?.toLowerCase() === values?.value?.toLowerCase()
          : true;
        return matchesDropdown;
      });
      setFilteredRowData(data)
    } else {
      setFilteredRowData(rowData)
    }
  }, [values])

  const HandleStatusChange = () => {
    const handleclose = () => {
      setIsDeleteCustomerModalOpen(false);
      setCustomerDatas("");
    };

    const deleteEvent = async () => {
      setIsLoading(true);
      try {
        const response = await commonAxios.delete(
          `/cemo/appuser/delete?userId=${customerDatas.id}&status=INACTIVE`
        );
        if (response.status === 200) {
          successNotify(`${customerDatas.firstName} Deactivated succesfully`);
          handleclose();
          getMerchantDetails();
        }
      } catch (error) {
        failureNotify("oops! something went wrong");
      } finally {
        setIsLoading(false);
      }
    };

    return (
      <div className="w-[85%] h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <div className="bg-white rounded-lg p-6 flex flex-col justify-center items-center w-1/2 h-72 relative">
            <button onClick={handleclose}>
              <img
                src={closeIcon}
                className="absolute top-5 right-5 h-6 w-6"
              />
            </button>
            <p className="font-semibold text-lg text-secondary text-center">
              Are you sure you want to Inactivate the merchant account "{customerDatas.firstName}"?
            </p>
            <section className="flex flex-row justify-between items-center w-[40%] gap-8 mt-10 mr-10">
              <button
                onClick={deleteEvent}
                className="px-6 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer uppercase"
              >
                Ja
              </button>
              <button
                onClick={handleclose}
                className="px-6 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer uppercase"
              >
                Nej
              </button>
            </section>
          </div>
        </section>
      </div>
    );
  };





  useEffect(() => {
    const adminDetails = userInfo;
    localStorage.setItem("admin-details", JSON.stringify(adminDetails));
    getMerchantDetails();
    getEventCategories();
  }, []);

  const handleToggle = (e: any) => {
    setModalPopup(true)

    // setChecked(e.target.checked ? 'ACTIVE' : 'INACTIVE');
  };
  const editMerchant = () => {
    const categoryArr = merchantData.categories
      ?.split(",")
      .map((cat: string) => cat.toUpperCase());
    const initialEditMerchantValues = {
      firstName: merchantData?.firstName,
      lastName: merchantData.lastName,
      mobile: merchantData.mobile,
      accountNumber: merchantData.accountNumber,
      bankName: merchantData.bankName,
      commission: merchantData.commission,
      email: merchantData.email,
      telephone: merchantData.telephone,
      businessName: merchantData.businessName,
      businessAddress: merchantData.businessAddress,
      businessNumber: merchantData.businessNumber,
      businessPinCode: merchantData.businessPinCode,
      businessCity: merchantData.businessCity,
      categories: categoryArr,
    };

    return initialEditMerchantValues;
  };

  const CustomDropdownIndicator = () => (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        style={{ width: "24px", height: "24px", fill: '#A09792' }}
      >
        <path fill="none" d="M0 0h24v24H0z" />
        <path d="M7 10l5 5 5-5z" />
      </svg>
    </div>
  );

  const customComponents: any = {
    DropdownIndicator: CustomDropdownIndicator,
  };

  const AddMerchantModal = () => {

    const handleFileChange = (event: ChangeEvent<HTMLInputElement>) => {
      const selectedFile = event.target.files?.[0];
      if (selectedFile) {
        setFile(selectedFile);
        if (event.target.files && event.target.files.length > 0) {
          const file = event.target.files[0];

          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () => {
            event.preventDefault();
            const imageUrl = reader.result?.toString();
            base64Image = imageUrl;
            setBase64MerchantImage(imageUrl!)
          };
        }
      }
    };

    const handleSelectChange = (selectedOptions: any) => {
      const selectedValues = selectedOptions.map((option: any) =>
        option.value?.toUpperCase()
      );
      formik.setFieldValue("categories", selectedValues);
    };

    const handleDragOver = (event: React.DragEvent<HTMLDivElement>) => {
      event.preventDefault();
    };

    const handleDrop = (event: React.DragEvent<HTMLDivElement>) => {
      event.preventDefault();
      const selectedFile = event.dataTransfer.files?.[0];
      if (selectedFile) {
        setFile(selectedFile);
      }
    };
    const addNewMerchant = async (values: initialAddMerchantModal) => {

      setIsLoading(true);

      let merchantBody;
      if (content !== "editmerchant") {
        merchantBody = {
          ...stepOneFormData,
          categories: stepOneFormData.categories?.join(","),
          profileImage: base64MerchantImage,
          password: "Merchant@123",
        };
      } else {

        merchantBody = {
          ...values,
          id: merchantData.id,
          createdAt: stepOneFormData.createdAt,
          updatedAt: stepOneFormData.updatedAt,
          accountStatus: checked,
          password: stepOneFormData.password,
          profileImage: base64MerchantImage,
          categories: values.categories?.join(","),
        };
      }

      if (content === "editmerchant") {

        try {
          const response = await commonAxios.put(
            `/cemo/appuser/update/${merchantData.id}`,
            merchantBody
          );

          if (response.data) {
            successNotify("Merchant updated successfully");
            getMerchantDetails();
            setIsAddMerchantModalOpen(false);
            setFormStep(1);
          }
        } catch (error) {
          failureNotify("oops! something went wrong");
        } finally {
          setIsLoading(false);
        }
      } else {
        try {
          const response = await commonAxios.post(
            "/cemo/appuser/supplier",
            merchantBody
          );
          if (response.data.errorErrorMessage) {
            failureNotify("Account already exists with same Email id, try new one");
          } else {
            successNotify("Merchant added successfully");
            getMerchantDetails();
            setIsAddMerchantModalOpen(false);
            setFormStep(1);
          }
        } catch (error) {
          failureNotify("oops! something went wrong");
        } finally {
          setIsLoading(false);
        }
      }
    };

    const validateEmail = async (values: any) => {
      // const body = {
      //   emailId: values.email,
      // };

      // try {
      //   const response = await commonAxios.post(
      //     "/cemo/appuser/validate/email",
      //     body
      //   );
      //   if (response.data.isAccountAlreadyExists) {
      //     failureNotify("Account already exists with same Email id, try new one");
      //   } else {
      setStepOneFormData(values)
      setFormStep(2);
      // addNewMerchant(values);
      //   }
      // } catch (error) {

      // }
    };

    const formik = useFormik<initialAddMerchantModal>({
      initialValues:
        content === "editmerchant"
          ? editMerchant()
          : {
            firstName: "",
            lastName: "",
            mobile: "",
            accountNumber: "",
            bankName: "",
            commission: "",
            email: "",
            telephone: "",
            businessName: "",
            businessAddress: "",
            businessNumber: "",
            businessPinCode: "",
            businessCity: "",
            categories: [],
          },
      validationSchema: Yup.object({
        // firstName: Yup.string().required("Required"),
        // lastName: Yup.string().required("Required"),
        // mobile: Yup.string()
        // .transform((value, originalValue) => {
        //   // Remove spaces from the phone number
        //   return originalValue.replace(/\s/g, '');
        // }).matches(/^\+46\d{9}$/, 'Invalid phone number, alike (+46 xx xxx xxxx)')
        // .required("Required"),
        email: Yup.string().email("Enter a valid Email").required("Required"),
        // telephone: Yup.string()
        // .transform((value, originalValue) => {
        //   // Remove spaces from the phone number
        //   return originalValue.replace(/\s/g, '');
        // }).matches(/^\+46\d{9}$/, 'Invalid phone number, alike (+46 xx xxx xxxx)')
        // .required("Required"),
        // businessName: Yup.string().required("Required"),
        // businessAddress: Yup.string().required("Required"),
        // businessNumber: Yup.string().required("Required"),
        // businessPinCode: Yup.string()
        // .matches(/^\d{5}$/, 'Invalid Post Number')
        // .required('Post Number is required'),
        // businessCity: Yup.string().required("Required"),
      }),
      onSubmit: (values) => setFormDataAndNavigateToFileUpload(values),
    });

    const setFormDataAndNavigateToFileUpload = (
      eventValue: initialAddMerchantModal
    ) => {
      // setStepOneFormData(eventValue);
      validateEmail(eventValue);
    };


    const ModalPopup = () => {


      const handleActivatetheMerchant = async () => {
        setIsLoading(true)
        if (merchantData.accountStatus === 'INACTIVE') {
          try {
            const response = await commonAxios.post(
              `/cemo/appuser/activate/supplier/${merchantData.id}/ACTIVE`,
            )
            if (response.data) {
              if (response.data.accountStatus === 'ACTIVE') {
                successNotify(`Merchant account activated`)
              }
              else {
                successNotify(`Merchant account deactivated`)
              }
              getMerchantDetails()
              setModalPopup(false)
              setChecked('ACTIVE')
              setIsLoading(false)
              setMerchantData(response.data)
            }
          }
          catch (e) {
          }
        } else {
          try {
            const response = await commonAxios.post(
              `/cemo/appuser/activate/supplier/${merchantData.id}/INACTIVE`,
            )
            if (response.data) {
              if (response.data.accountStatus === 'ACTIVE') {
                successNotify(`Merchant account activated`)
              }
              else {
                successNotify(`Merchant account deactivated`)
              }
              getMerchantDetails()
              setModalPopup(false)
              setChecked('INACTIVE')
              setIsLoading(false)
              setMerchantData(response.data)
            }
          }
          catch (e) {

          }
        }
      }

      const handleClosePopup = () => {
        setModalPopup(false)
      }
      return (
        <>
          <div className="w-[100%] h-full bg-[#00000080] absolute z-[100] right-0 flex justify-center items-center">
            <section className="bg-white h-[30%] w-[50%] flex flex-col items-center justify-center">
              <div >
                {
                  merchantData.accountStatus === "ACTIVE" ? <p>Do you want to deactivate the merchant account?</p> : <p>Do you want to activate the merchant account?
                  </p>
                }
              </div>
              <div className="flex">
                <button
                  type="submit"
                  // disabled={loading}
                  className="px-4 py-2 md:mr-4 bg-[#D6B27D] rounded-lg mt-6 text-md font-semibold cursor-pointer text-white"
                  onClick={handleActivatetheMerchant}
                >
                  Ja
                </button>
                <button
                  type="submit"
                  // disabled={loading}
                  className="px-4 py-2 md:mr-4 bg-[#D6B27D] rounded-lg mt-6 text-md font-semibold cursor-pointer text-white"
                  onClick={handleClosePopup}
                >
                  Nej
                </button>
              </div>
            </section >
          </div>
        </>
      )
    }

    const handleSelectCityChange = (selectedCityOptions: any) => {
      // const selectedCities = [selectedCityOptions].map((option: any) => option.value);
      formik.setFieldValue("businessCity", selectedCityOptions.value);
    };

    return (
      <div className="w-full h-full bg-[#00000080] absolute z-40 right-0">
        {modalPopup ? <ModalPopup /> : null}
        {formStep === 1 ? (
          <section className="w-full h-full flex flex-col items-center justify-center text-secondary">
            <form
              onSubmit={formik.handleSubmit}
              className="bg-[#ffffff] w-[55rem] h-[35rem] overflow-y-scroll px-8 py-6 relative"
            >
              <div className="flex flex-row justify-between items-center my-6">
                <p className=" font-bold text-2xl">
                  {" "}
                  {content.length > 0 ? "EDIT MERCHANT" : "Lägg till leverantör"}
                </p>
                {
                  content.length > 0 && <label htmlFor="toggle" className="flex items-center cursor-pointer">
                    <div className="relative">
                      <input
                        type="checkbox"
                        id="toggle"
                        className="sr-only"
                        checked={checked === 'ACTIVE' ? true : false}
                        onChange={handleToggle}
                      />
                      {
                        checked === "ACTIVE" ? <div className="block bg-[#D6B27D] w-10 h-6 rounded-full"></div> : <div className="block bg-gray-300 w-10 h-6 rounded-full"></div>
                      }

                      <div
                        className={`dot absolute left-1 top-1 bg-white w-4 h-4 rounded-full transition ${checked === "ACTIVE" ? 'translate-x-full' : ''
                          }`}
                      ></div>
                    </div>

                    <div className="ml-3 text-gray-700 font-medium">Active</div>


                  </label>
                }

                <button
                  onClick={() => {
                    setMerchantData([]);
                    setIsAddMerchantModalOpen(false);
                    setFile(null);
                    setContent("");
                    setBase64MerchantImage("");
                    setFormStep(1);
                    setStepOneFormData(null);
                  }}
                >
                  <img
                    src={modaldeleteIcon}
                    className="absolute top-5 right-5 h-6 w-6"
                  />
                </button>
              </div>
              <section className="flex flex-row justify-between items-center">
                <div className="w-full mr-10">
                  <label
                    className="text-sm  font-bold"
                    htmlFor="firstName"
                  >
                    Förnamn
                  </label>
                  <input
                    type="text"
                    name="firstName"
                    id="firstName"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className="outline-none border my-2 w-full mb-5 px-4 py-2
                    rounded-[10px]"
                    value={formik.values.firstName}
                  />
                  {formik.errors.firstName ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.firstName}
                    </span>
                  ) : null}
                </div>
                <div className="w-full">
                  <label
                    className=" text-sm font-bold"
                    htmlFor="lastName"
                  >
                    Efternamn
                  </label>
                  <input
                    type="text"
                    name="lastName"
                    id="lastName"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className="outline-none border my-2 w-full mb-5 px-4 py-2
                    rounded-[10px]"
                    value={formik.values.lastName}
                  />
                  {formik.errors.lastName ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.lastName}
                    </span>
                  ) : null}
                </div>
              </section>
              <section className="flex flex-row justify-between items-center">
                <div className="w-full mr-10">
                  <label
                    className="text-sm font-bold"
                    htmlFor="mobile"
                  >
                    Telefonnummer
                  </label>
                  <input
                    type="text"
                    name="mobile"
                    id="mobile"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className="outline-none border my-2 w-full mb-5 px-4 py-2
                    rounded-[10px]"
                    value={formik.values.mobile}
                  />
                  {formik.errors.mobile ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.mobile}
                    </span>
                  ) : null}
                </div>
                <div className="w-full">
                  <label
                    className=" text-sm font-bold"
                    htmlFor="email"
                  >
                    E-Post
                  </label>
                  <input
                    type="email"
                    name="email"
                    id="email"
                    disabled={content.length > 0}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className="outline-none border my-1 w-full mb-5 lowercase px-4 py-2
                    rounded-[10px]"
                    value={formik.values.email}
                  />
                  {formik.errors.email ? (
                    <span className="text-red-500 font-normal">
                      {formik.errors.email}
                    </span>
                  ) : null}
                </div>
              </section>
              <section className="flex flex-row justify-between items-center">

                <div className="w-full mr-10">
                  <label
                    className="text-sm font-bold"
                    htmlFor="accountNumber"
                  >
                    Kontonummer
                  </label>
                  <input
                    type="number"
                    name="accountNumber"
                    id="accountNumber"
                    disabled={content.length > 0}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className="outline-none border my-2 w-full mb-5 px-4 py-2
                    rounded-[10px]"
                    value={formik.values.accountNumber}
                  />
                  {formik.errors.accountNumber ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.accountNumber}
                    </span>
                  ) : null}
                </div>
                <div className="w-full">
                  <label
                    className="text-sm font-bold"
                    htmlFor="bankName"
                  >
                    Bank namn
                  </label>
                  <input
                    type="text"
                    name="bankName"
                    id="bankName"
                    disabled={content.length > 0}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className="outline-none border my-2 w-full mb-5 px-4 py-2
                    rounded-[10px]"
                    value={formik.values.bankName}
                  />
                  {formik.errors.bankName ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.bankName}
                    </span>
                  ) : null}
                </div>
              </section>
              <section className="flex flex-row justify-between items-center">
                <div className="w-full mr-10">
                  <label
                    className=" text-sm font-bold"
                    htmlFor="businessName"
                  >
                    Företagsnamn
                  </label>
                  <input
                    type="text"
                    name="businessName"
                    id="businessName"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className="outline-none border my-1 w-full mb-5 px-4 py-2
                    rounded-[10px]"
                    value={formik.values.businessName}
                  />
                  {formik.errors.businessName ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.businessName}
                    </span>
                  ) : null}
                </div>
                <div className="w-full">
                  <label
                    className=" text-sm font-bold"
                    htmlFor="commission"
                  >
                    Provision
                  </label>
                  <input
                    type="number"
                    name="commission"
                    id="commission"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className="outline-none border my-1 w-full mb-5 px-4 py-2
                    rounded-[10px]"
                    value={formik.values.commission}
                  />
                  {formik.errors.commission ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.commission}
                    </span>
                  ) : null}
                </div>
              </section>
              <section className="flex flex-row justify-between items-center">
                <div className="w-full mr-10">
                  <label
                    className=" text-sm font-bold"
                    htmlFor="businessAddress"
                  >
                    Företagsadress
                  </label>
                  <input
                    type="text"
                    name="businessAddress"
                    id="businessAddress"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className="outline-none border my-1 w-full mb-5 px-4 py-2
                    rounded-[10px]"
                    value={formik.values.businessAddress}
                  />
                  {formik.errors.businessAddress ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.businessAddress}
                    </span>
                  ) : null}
                </div>
                <div className="w-full">
                  <label
                    className=" text-sm font-bold"
                    htmlFor="businessNumber"
                  >
                    Organisationsnummer
                  </label>
                  <input
                    type="text"
                    name="businessNumber"
                    id="businessNumber"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className="outline-none border my-1 w-full mb-5 px-4 py-2
                    rounded-[10px]"
                    value={formik.values.businessNumber}
                  />
                  {formik.errors.businessNumber ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.businessNumber}
                    </span>
                  ) : null}
                </div>
              </section>
              <section className="flex flex-row justify-between items-center">
                <div className="w-full mr-10">
                  <label
                    className=" text-sm font-bold"
                    htmlFor="businessPinCode"
                  >
                    Företagets postkod
                  </label>
                  <input
                    type="text"
                    name="businessPinCode"
                    id="businessPinCode"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className="outline-none border my-1 w-full mb-5 px-4 py-2
                    rounded-[10px]"
                    value={formik.values.businessPinCode}
                  />
                  {formik.errors.businessPinCode ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.businessPinCode}
                    </span>
                  ) : null}
                </div>
                {/* <div className="w-full">
                  <label
                    className=" text-sm font-bold"
                    htmlFor="businessCity"
                  >
                  Företagets stad
                  </label>
                  <input
                    type="text"
                    name="businessCity"
                    id="businessCity"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className="outline-none border my-1 w-full mb-5 px-4 py-2
                    rounded-[10px]"
                    value={formik.values.businessCity}
                  />
                  {formik.errors.businessCity ? (
                    <span className="text-red-500 font-medium">
                      {formik.errors.businessCity}
                    </span>
                  ) : null}
                </div> */}
                <div className="-mt-2 w-full">
                  <label
                    className=" text-sm font-bold"
                    htmlFor="businessCity"
                  >
                    Företagets stad
                  </label>
                  {/* <img className="mb-1 h-7 p-1" src={chooseCity} loading="eager" /> */}
                  <Select
                    options={cityChoose}
                    className="py-3 -px-1 text-[#000000] custom-placeholder text-[16px] md:w-full outline-none leading-tight text-2xl"
                    placeholder={"Välj städer"}
                    value={cityChoose.filter((option: any) =>
                      formik.values.businessCity.includes(option.value)
                    )}
                    // value={selectedCity}
                    onChange={handleSelectCityChange}
                  // isMulti={true}
                  />
                </div>
              </section>

              <div className="flex justify-center">
                <button
                  type="submit"
                  // disabled={loading}
                  className="px-4 py-2 md:mr-4 bg-[#D6B27D] rounded-lg mt-6 text-md font-semibold cursor-pointer text-white uppercase"
                >
                  Fortsätt
                </button>
              </div>
            </form>

          </section>
        ) : (
          <section className="w-full h-full flex flex-col items-center justify-center">
            <form
              onSubmit={() => addNewMerchant(stepOneFormData)}
              className="bg-[#ffffff] w-[50rem] h-[50rem] overflow-y-scroll px-8 py-6 rounded-lg relative"
            >
              <button
                onClick={() => {
                  setMerchantData([]);
                  setIsAddMerchantModalOpen(false);
                  setFile(null);
                  setContent("");
                  setBase64MerchantImage("");
                  setFormStep(1);
                  setStepOneFormData(null);
                }}
              >
                <img
                  src={modaldeleteIcon}
                  className="absolute top-5 right-5 h-6 w-6"
                />
              </button>

              <section
                className="w-full py-10 px-10 border-[1px] rounded-lg flex flex-col items-center justify-center my-5"
                onDragOver={handleDragOver}
                onDrop={handleDrop}
              >
                <input
                  id="file-upload"
                  type="file"
                  accept=".jpg,.jpeg,.png,.gif"
                  className="hidden"
                  onChange={handleFileChange}
                />
                {base64MerchantImage ? (
                  <>
                    <div className="relative">
                      <img
                        className="w-full object-cover"
                        src={base64MerchantImage}
                        alt={file?.name}
                      />
                      <RxCross2
                        className="cursor-pointer m-1 top-0 right-0 absolute bg-white"
                        onClick={() => {
                          setFile(null);
                          setBase64MerchantImage("");
                        }}
                      />
                    </div>
                  </>
                ) : (
                  <label htmlFor="file-upload" className="cursor-pointer">
                    <img
                      className="ml-11 mb-2"
                      src={fileupload}
                      alt="fileupload"
                    />
                    <span className="text-[#97A0C3] text-xs ml-4 font-bold">
                      Ladda upp bild
                    </span>
                  </label>
                )}
              </section>
              <section className="flex flex-row justify-center items-center">
                <button
                  type="submit"
                  className="outline-none border-none uppercase rounded-lg bg-[#D6B27D] py-3 px-14 cursor-pointer mt-4 font-bold text-white text-base"
                >
                  Lägg till
                </button>
              </section>
            </form>
          </section>
        )}
      </div>
    );
  };

  useEffect(() => {
    const handleEscPress = (event: KeyboardEvent) => {
      if (event.key === "Escape") {
        setIsAddMerchantModalOpen(false);
        setFormStep(1);
        setFile(null);
        base64Image = undefined;
      }
    };

    document.addEventListener("keydown", handleEscPress);

    return () => {
      document.removeEventListener("keydown", handleEscPress);
    };
  }, []);

  const addNewMerchantPopup = () => {
    setIsAddMerchantModalOpen(true);
    setFormStep(1);
    setContent("");
  };


  let merchantStatus: any = []
  rowData.forEach((val: any) => {
    if (!merchantStatus.includes(val.accountStatus.toUpperCase())) {
      merchantStatus.push(val.accountStatus.toUpperCase())
    }
  })
  const statusOptions = merchantStatus.map((val: any) => {
    return {
      label: val,
      value: val
    }
  })

  const setCityChange = (selectedOption: any) => {
    setValues(selectedOption);
  };

  const keysToDeleteInDownloadFile = ['id', 'password', 'createdAt', 'updatedAt', 'resetToken'];

  const getMerchantList = () => {
    const value = rowData.map((obj: any, index: any) => {
      const newObj = { ...obj };
      keysToDeleteInDownloadFile.forEach(key => delete newObj[key]);
      return getEventDownloadInfo(newObj, index);
    });
    return value;
  };

  function getEventDownloadInfo(data: any, index: any) {

    return {
      // "S.no":index + 1,
      "FirstName": data.firstName,
      "LastName": data.lastName,
      "Mobile": data.mobile,
      "AccountNumber": data.accountNumber,
      "BankName": data.bankName,
      "Commission": data.commission,
      "Email": data.email,
      "Telephone": data.telephone,
      "businessName": data.businessName,
      "businessNumber": data.businessNumber,
      "businessAddress": data.businessAddress,
      "businessPinCode": data.businessPinCode,
      "businessCity": data.businessCity,
      "accountStatus": data.accountStatus,

    };
  }


  const MerchantDelete = () => {

    const handleclose = () => {
      setDeletePopup(false);
      setMerchantData([]);
    };

    const deleteEvent = async () => {
      setIsLoading(true);
      try {
        const response = await commonAxios.delete(
          `/cemo/appuser/delete?userId=${merchantData.id}&status=INACTIVE`
        );

        if (response.status == 200) {
          getMerchantDetails();
          successNotify(
            `${merchantData.firstName + "" + merchantData.lastName
            } event deleted succesfully`
          );
          handleclose();
        }
      } catch (error) {

        failureNotify("oops! something went wrong");
      } finally {
        setIsLoading(false);
      }
    };


    const deleteSupplier = async () => {
      setIsLoading(true);
      try {
        const response = await commonAxios.delete(
          `/cemo/appuser/delete?userId=${merchantData.id}&status=DELETE`
        );

        if (response.status == 200) {
          getMerchantDetails();
          successNotify(
            `${merchantData.firstName + "" + merchantData.lastName
            } deleted succesfully`
          );
          handleclose();
        }
      } catch (error) {

        failureNotify("oops! something went wrong");
      } finally {
        setIsLoading(false);
      }
    }

    return (
      <div className="w-[85%] h-full bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center">
          <div className="bg-white rounded-lg p-6 flex flex-col justify-center items-center w-1/2 h-64 relative">
            <button onClick={handleclose}>
              <img
                src={modaldeleteIcon}
                className="absolute top-5 right-5 h-6 w-6"
              />
            </button>
            <p className="font-semibold text-lg text-secondary">
              { }
              Are you sure you want to delete the merchant "{merchantData.firstName}{" "}
              {merchantData.lastName}"?
            </p>
            <section className="flex flex-row justify-between items-center w-[40%] mt-10">
              <button
                onClick={deleteSupplier}
                className="px-6 py-2 rounded-lg bg-golden text-white text-white font-semibold text-base cursor-pointer"
              >
                Ta bort
              </button>
              <button
                onClick={handleclose}
                className="px-6 py-2 rounded-lg bg-[#FFE5D8] text-secondary font-semibold text-base cursor-pointer"
              >
                Avbryt
              </button>
            </section>
          </div>
        </section>
      </div>
    );
  };

  const customStyles = {
    control: (provided: any, state: any) => ({
      ...provided,
      border: "1px solid #ccc",
      borderRadius: "4px",
      fontSize: "13px",
      width: "150px",
    }),
    option: (provided: any, state: any) => ({
      ...provided,
      backgroundColor: "transparent",
      fontSize: "13px",
      width: "150px",
      color: state.isSelected ? "#D6B27D" : "gray",
    }),
    singleValue: (provided: any) => ({
      ...provided,
      color: "gray", // Set the initial text color to gray
    }),
    valueContainer: (provided: any) => ({
      ...provided,
      color: "gray", // Set the initial text color to gray
    }),

  };

  let filterDatas = rowData.filter((searchTerm: any) => {
    let filteredValue = searchTerm.firstName.toLowerCase().includes(filterData.toLowerCase()) || searchTerm.email.toLowerCase().includes(filterData.toLowerCase()) || searchTerm?.telephone?.toLowerCase().includes(filterData.toLowerCase()) || searchTerm.businessCity?.toLowerCase().includes(filterData.toLowerCase()) || searchTerm.accountStatus.toLowerCase().includes(filterData.toLowerCase());
    let status = !selectStatus || searchTerm.accountStatus?.toLowerCase() === selectStatus?.value?.toLowerCase();
    let citySelected = !values || searchTerm.businessCity?.toLowerCase() === values?.value?.toLowerCase();
    return filteredValue && status && citySelected;
  })

  const handleStatuschange = (event: any) => {
    setSelectStatus(event)
  }

  useEffect(() => {
    // filteredDatas()
  }, [values, selectStatus])

  return (
    <div className="flex">
      <SideNav />
      {deletePopup && <MerchantDelete />}
      <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
        {isloading ? <Loader /> : null}
        {isAddMerchantModalOpen ? <AddMerchantModal /> : null}
        {isDeleteCustomerModalOpen ? <HandleStatusChange /> : null}
        <AdminHeader />
        <div className="mx-10 my-6">
          <div className="flex justify-between py-4">
            <h1 className="text-secondary text-[24px] font-semibold">
              Leverantörer
            </h1>
            <div className="flex">
              {" "}
              <button
                onClick={addNewMerchantPopup}
                className="bg-[#D6B27D] text-[#FFFFFF] flex px-6 p-2 rounded-[10px] font-semibold mr-4 w-auto uppercase"
              >
                Ny <img src={addIcon} className="ml-2" />
              </button>
              <AsyncDownloadCSV
                data={getMerchantList}
                asyncOnClick={true}
                filename={
                  "Merchants-" +
                  moment(new Date()).format("DD/MM/YYYY")
                }
                onClick={() => { }}
              >
                <SiMicrosoftexcel className="w-6 h-6 text-[#D6B27D] hover:text-[#F7CB73] hover:cursor-pointer mt-2" />
              </AsyncDownloadCSV>
            </div>
          </div>

          <div className=" border border-[#FFFFFF] bg-[#ffff] rounded-[20px] p-4 mt-1">
            <div className="flex">
              <div className="relative w-[93%]">
                <input
                  type="text"
                  className=" w-[98%] h-[38px] pl-10 pr-5 border rounded-md border-[#E2E2EA] focus:outline outline-[#E2E2EA]  focus:border-transparent "
                  placeholder="Sök efter namn, leverantör, mobil etc..."
                  onChange={(e: any) => setFilterData(e.target.value)}
                  value={filterData}
                />
                <div className="absolute inset-y-0 left-0 flex items-center pl-3">
                  <img src={searchIcon} />
                </div>
              </div>
              <div className="mx-4">
                <Select
                  options={statusOptions}
                  placeholder={"Status"}
                  styles={customStyles}
                  value={selectStatus}
                  defaultValue={selectStatus}
                  onChange={handleStatuschange}
                  components={customComponents}
                  isClearable
                />
              </div>

              <div className="">
                <Select
                  options={cityOptions}
                  placeholder={"Stad"}
                  styles={customStyles}
                  value={values}
                  defaultValue={selectedOption}
                  onChange={setCityChange}
                  components={customComponents}
                  isClearable
                />
              </div>
            </div>

            <div className="mt-6">
              <GridTable
                columnDefs={columnDefs}
                gridOptions={{
                  ...gridOptions,
                  getRowHeight: () => rowHeight,
                }}
                rowData={filterDatas}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MerchantScreen;
