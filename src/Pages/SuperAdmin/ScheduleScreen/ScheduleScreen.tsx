import React, { useState, useEffect } from "react";
import AdminHeader from "../../../Components/AdminHeader/AdminHeader";
import SideNav from "../../../Components/SideNav/SideNav";
import addIcon from "../../../assets/addIcon.svg";
import "react-calendar/dist/Calendar.css";
import ScheduleCalender from "../../../Components/ScheduleCalendar/ScheduleCalendar";
import BookedCard from "../../../Components/BookedCard/BookedCard";
import Select from "react-select";
import { commonAxios } from "../../../Axios/service";
import moment from "moment";
import Loader from "../../../Components/Loader/Loader";
import OperatingHoursCard from "../../../Components/OperatingHoursCard/OperatingHoursCard";
import AvailableSlots from "../../../Components/AvailableSlots/AvailableSlots";
import { RootState } from "../../../Redux/store";
import { useSelector } from "react-redux";
import { failureNotify } from "../../../Components/Toast/Toast";

const ScheduleScreen = () => {
  const [loader, setLoader] = useState(false);
  const [listOfEvents, setListOfEvents] = useState([]);
  const [operatinghours, setOperatinghours] = useState([]);
  const [slots, setSlots] = useState([]);
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [selectedDates, setSelectedDates]: any = useState([]);

  const getEventsByDate = async (startDate: any, endDate: any) => {
    try {
      setLoader(true);
      console.log("in api call method--->", startDate, endDate);
      const url = userInfo.role === "ROLE_ADMIN" ? `/cemo/events?startDate=${moment(startDate).format(
        "YYYY-MM-DD"
      )}&endDate=${moment(endDate).format("YYYY-MM-DD")}` : `/cemo/events/supplier/${userInfo.id}?startDate=${moment(startDate).format(
        "YYYY-MM-DD"
      )}&endDate=${moment(endDate).format("YYYY-MM-DD")}`;

      console.log("url--->", url);
      const response = await commonAxios.get(url);
      if (response.data) {
        console.log("resposne--> ", response.data);
        setListOfEvents(response.data);
      }
      setLoader(false);
    } catch (error) {
      console.error("error occured");
      failureNotify("Something went wrong, while fetching Events!")
    } finally {
      setLoader(false);
    }
  };

  const getEventDetails = async (e: any) => {
    setLoader(true);
    try {
      const response = await commonAxios.get(`/cemo/events/${e.id}`);
      if (response.data) {
        console.log(
          "response.data.operatingHours------------->",
          response.data.operatingHours
        );
        await setOperatinghours(response.data.operatingHours);
      }
    } catch (error) {
      console.log("operatingHours", error);
      failureNotify("Something went wrong, while fetching Events!")
    } finally {
      setLoader(false);
    }
  };


  const getSupplierEvents = async (id: string) => {
    try {
      setLoader(true);
      const response = await commonAxios.get(
        `/cemo/events/supplier/${id}`
      );
      if (response.data) {
        console.log("supplier resposne--> ", response.data);
        setListOfEvents(response.data);
      }
      setLoader(false);
    } catch (error) {
      console.error("error occured");
      failureNotify("Something went wrong, while fetching Events!")
    } finally {
      setLoader(false);
    }
  }


  const setSlotsForActiveOperatingHrs = async (ophrs: any) => {
    await setSlots(ophrs.slot);
  };

  const resetOperatingHoursValues = () => {
    setSlots([]);
    setOperatinghours([]);
  };

  const resetSlotsValues = () => {
    setSlots([]);
  };

  const onSubmit = () => {
    getEventsByDate(selectedDates[0], selectedDates[1]);
    resetOperatingHoursValues();
  }


  useEffect(() => {
    if (selectedDates.length > 0) {
      const startDate = moment(selectedDates[0]).format("YYYY-MM-DD");
      const endDate = moment(selectedDates[1]).format("YYYY-MM-DD");

      getEventsByDate(startDate, endDate);
    }
    else if (userInfo.role === "ROLE_ADMIN" || "ROLE_SUPPLIER") {
      getEventsByDate(
        new Date(),
        new Date(moment().add(15, "days").format("YYYY-MM-DD"))
      );
    }

  }, [selectedDates]);

  return (
    <div className="flex">
      <SideNav />
      <div className="w-[85%] h-screen overflow-y-scroll bg-[#fafafb]">
        <AdminHeader />
        <div className="mx-4">
          {loader && <Loader />}
          <div className="py-6 flex">
            <div className="">
              <h1 className="text-2xl font-[sofiapro] font-bold text-secondary">
                Schema
              </h1>
            </div>
          </div>
          <div className="">
            <div>
              <p className="text-md text-secondary">Välj start- och slutdatum för att se alla event och dess uppgifter</p>
            </div>
            <div className="flex flex-row items-center ">
              <div className=" ">
                <div className="px-2 border rounded-lg mb-4">
                  <ScheduleCalender
                    selectedDates={selectedDates}
                    setSelectedDates={setSelectedDates}
                  />
                </div>
                {/* <p className="text-md text-secondary">Select the start and end date to view the <br />list of events and their details</p> */}
                {/* <div className="flex">
                  <button className="bg-[#D6B37D] mt-12 text-sm  text-white mb-10 font-bold border rounded-lg p-2 px-6  w-auto leading-tight focus:outline-none focus:shadow-outline" onClick={() => onSubmit()}>VIEW EVENTS DETAILS</button>
                </div> */}
              </div>
              <div className="px-4 w-full">
                {
                  listOfEvents?.length > 0 ? (
                    <div className="h-[75vh] overflow-hidden flex flex-row shadow-xl rounded-md gap-2 my-6">
                      <div className="h-[70vh]">
                        <p className=" flex justify-center items-center text-[16px] font-bold font-[sofiapro] text-secondary">
                          Event
                        </p>
                        <div className="max-h-full h-full overflow-y-scroll text-secondary">
                          <BookedCard
                            events={listOfEvents}
                            getEventDetails={getEventDetails}
                            resetSlotsValues={resetSlotsValues}
                          />
                        </div>
                      </div>
                      {operatinghours?.length > 0 && (
                        <div className="h-[70vh]">
                          <p className=" flex justify-center items-center text-[16px] font-bold font-[sofiapro] text-secondary">
                            Operating hours
                          </p>
                          <div className="max-h-full h-full overflow-y-scroll">
                            <div className="border-l-2 text-secondary">
                              <OperatingHoursCard
                                operatingHours={operatinghours}
                                setSlotsForActiveOperatingHrs={
                                  setSlotsForActiveOperatingHrs
                                }
                              />
                            </div>
                          </div>
                        </div>
                      )}
                      {slots?.length > 0 && (
                        <div className="h-[70vh] ">
                          <p className=" flex justify-center items-center text-[16px] font-bold font-[sofiapro] text-secondary">
                            Booking status
                          </p>
                          <div className="max-h-full h-full overflow-y-scroll">
                            <div className="border-l-2">
                              <AvailableSlots slots={slots} />
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                  ) : null
                }

              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  );
};

export default ScheduleScreen;
