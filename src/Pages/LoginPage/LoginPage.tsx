import React, { LegacyRef, useCallback, useState } from "react";
import Heading from "../../assets/blixa-black.png";
import Footer from "../../Components/Footer/Footer";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { commonAxios } from "../../Axios/service";
import { failureNotify, successNotify } from "../../Components/Toast/Toast";
import { RootState } from "../../Redux/store";
import { useDispatch, useSelector } from "react-redux";
import { setUserDetails } from "../../Redux/features/userSlice";
import { setRefreshToken, setToken } from "../../Utils/auth";
import { addProductToCart } from "../../Redux/features/productCartSlice";
import { BsEye, BsEyeSlash } from "react-icons/bs";

type initialLoginValues = {
  email: string;
  password: string;
};

const Login = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [pathURL, setPathURL] = useState<string>("/login");
  const naviagte = useNavigate();
  const dispatch = useDispatch();
  const [showPassword, setShowPassword] = useState(false);
  const emailInput = useCallback((inputElement: any) => {
    if (inputElement) {
      inputElement.focus();
    }
  }, []);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };
  const userInfo = useSelector((state: RootState) => state.user.userDetails);

  const handleAddToCart = async (cartArr: any) => {
    setIsLoading(true);
    try {
      const response = await commonAxios.post(
        "/cemo/bookingitems/new",
        cartArr
      );
      dispatch(addProductToCart());
      // successNotify("Produkt lagd i varukorg");
    } catch (error) {
      failureNotify("oops! something went wrong, Try again");
    } finally {
      setIsLoading(false);
    }
  }

  const handleLogin = async (values: initialLoginValues) => {
    setIsLoading(true);
    const body = {
      ...values,
      customerLogin: true
    }
    await commonAxios.post("/cemo/auth/login", body)
      .then(
        (result) => {
          if (result.data.role === "ROLE_CUSTOMER") {
            const cartItems: any = localStorage.getItem("cartListDetails");
            const parsedCartItems = JSON.parse(cartItems);


            if (cartItems) {
              const cartValues = parsedCartItems.map((item: any) => {
                return {
                  // ...item,
                  appUserId: result.data.id,
                  productId: item.product.id,
                  bookingItemType: item.bookingItemType,
                  quantity: item.quantity
                }
              })
              cartValues.map((item: any) => {
                handleAddToCart(item);
              })
            }
            setIsLoading(false);
            successNotify("Login successful");
            dispatch(setUserDetails(result.data));
            setToken(result.data?.accessToken);
            setRefreshToken(result.data?.refreshToken);

            const redirectBackTo = JSON.parse(localStorage.getItem('redirectBackTo') as string);
            if (redirectBackTo) {
              localStorage.removeItem('redirectBackTo');
              if (redirectBackTo.state) {
                naviagte(redirectBackTo.pathname, { state: redirectBackTo.state });
              }
              else {
                naviagte(redirectBackTo, { replace: true });
              }
            } else {
              naviagte("/", { replace: true });
            }
          } else if (result.data.role === "ROLE_SUPPLIER") {
            // Block supplier login
            setIsLoading(false);
            failureNotify("Supplier login is not allowed.");
          } else {
            // Handle other roles or conditions as needed
            setIsLoading(false);
            failureNotify("This is not a proper user credentials. Please Login with user credentials.");
          }
        },
        (error) => {
          setIsLoading(false);
          failureNotify("Oops! Something went wrong");
        }
      )
      .finally(() => {
        setIsLoading(false);
      });
  };
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email("Invalid email address")
        .required("Email is Obligatorisk"),
      password: Yup.string()
        .required("Password is Obligatorisk")
        .min(6, "Password should be minimum 6 characters"),
    }),
    onSubmit: (values) => handleLogin(values),
    validateOnChange: false, // Disable onChange validation
    validateOnBlur: false,
  });

  const handleEmailChange = (e: any) => {
    const email = e.target.value;
    const lowercaseEmail = email.toLowerCase();
    formik.handleChange(e);
    formik.setFieldValue('email', lowercaseEmail);
  };

  return (
    <div className="Login">
      <div className="bg-[#FFE5D8]">
        <div className="flex justify-end">
          <button
            className="p-2 w-auto m-5 lg:mt-1 mr-3 font-semibold rounded-lg bg-[#D6B37D] uppercase text-white px-6 text-sm "
            onClick={() => naviagte("/signup")}
          >
            Skapa konto
          </button>
          {/* <button className='p-3 m-5 font-semibold rounded-lg bg-[#D6B37D] text-white px-8' onClick={() => naviagte('/suppliersignup')}>Signup as Supplier</button> */}
        </div>
        <div className="grid grid-cols-1 min-h-[20v]">
          <div className="bg-[#FFE5D8] min-h-[20vh] flex justify-center items-center">
            <img
              className="ml-4 pl-0 w-52 cursor-pointer"
              src={Heading}
              onClick={() => naviagte("/")}
              loading="eager"
            ></img>
          </div>

        </div>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 min-h-[40vh] m-10 -mt-1">
        <div>
          <h2 className="font-semibold px-3 md:px-6 mt-5 text-[20px] lg:text-[30px] text-secondary">
            Logga in på ditt konto
          </h2>
          <div className="flex justify-center items-center mt-5">
            <h2 className="px-3 md:px-6 text-left  first-letter:text-1xl text-[18px] text-secondary">
              Så härligt att se dig igen! Vi hoppas du är redo för ännu mer skoj och äventyr. Släng in dina inloggningsuppgifter och låt oss fortsätta skapa minnen som kommer finnas med i resten av ditt liv!
            </h2>
          </div>
          {/* <div className="p-3 md:p-6 flex -mt-0.5">
            <h4 className="text-left text-secondary first-letter:text-1xl text-[18px]">
              Logga in och låt din fantasi flöda! Tillsammans skapar vi magiska minnen för livet.
            </h4>
          </div> */}
        </div>
        <div className="flex justify-center items-center mt-12 mb-12">
          <form
            onSubmit={formik.handleSubmit}
            autoComplete="off"
            className="lg:w-[75%] w-[95%] max-w-xl mx-auto h-auto shadow-2xl first-letter:border bg-[#FFFFFF] rounded-2xl p-6"
          >
            <div>
              <label className="block text-secondary font-extrabold text-[16px] mb-2">
                E-post
              </label>
              <input
                className="appearance-none border rounded-[10px] w-full py-3 px-4 text-gray-600 leading-tight focus:outline-none focus:shadow-outline"
                id="email"
                type="email"
                name="email"
                onChange={handleEmailChange}
                // ref={emailInput}
                onBlur={formik.handleBlur}
                value={formik.values.email}
                placeholder="E-post"
                autoComplete="off"
              />
              {formik.errors.email ? (
                <div className="text-red-600 my-2">{formik.errors.email}</div>
              ) : null}
            </div>
            <div className="mt-10">
              <label className="block text-gray-700 text-[16px] font-extrabold  mb-2">
                Lösenord
              </label>
              <div className="relative">
                <input
                  className="appearance-none border rounded-[10px] w-full py-3 px-4 text-gray-600 leading-tight focus:outline-none focus:shadow-outline"
                  id="password"
                  type={showPassword ? 'text' : 'password'} // Toggle between 'text' and 'password'
                  name="password"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.password}
                  placeholder="Lösenord"
                  autoComplete="off"
                />
                <span
                  className="absolute top-0 right-2 mt-4 mr-4 cursor-pointer"
                  onClick={togglePasswordVisibility}
                >
                  {showPassword ? <BsEye /> : <BsEyeSlash />}
                </span>
              </div>
              {formik.errors.password ? (
                <div className="text-red-600 my-2">{formik.errors.password}</div>
              ) : null}
            </div>
            <div className="flex items-center mt-10">
              <input
                className="mr-2 leading-tight text-[20px]"
                type="checkbox"
                id="remember"
              />
              <label className="text-sm text-secondary">Kom ihåg mig</label>
            </div>
            <div className="flex items-center justify-center mt-10">
              <button
                className="p-2 w-auto m-5 font-semibold rounded-lg bg-[#D6B37D] text-white px-6 uppercase"
                type="submit"
              >
                {isLoading && (
                  <svg
                    aria-hidden="true"
                    className="inline w-5 h-5 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-[#D6B37D]"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                      fill="currentColor"
                    />
                    <path
                      d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                      fill="currentFill"
                    />
                  </svg>
                )}
                {isLoading ? '' : "Logga in"}
              </button>
            </div>
            <div className="flex items-center justify-between xl:flex-row flex-col">
              <div>
                <button
                  className="appearance-none border font-semibold rounded-[10px] w-auto py-3 px-3 text-[15px] leading-tight focus:shadow-outline text-secondary uppercase"
                  type="button"
                  onClick={() => naviagte("/signup")}
                >
                  Skapa konto
                </button>
              </div>
              <div>
                <p
                  className=" underline leading-tight focus:shadow-outline text-[14px] xl:mt-0 mt-3 text-secondary cursor-pointer uppercase"
                  onClick={() => {
                    naviagte(
                      {
                        pathname: "/forgotpassword",
                      },
                      {
                        state: { pathURL },
                      }
                    );
                  }}
                >
                  Glömt lösenord?
                </p>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
};
export default Login;
