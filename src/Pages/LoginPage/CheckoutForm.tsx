
import {PaymentElement} from '@stripe/react-stripe-js';

const CheckoutForm = () => {
    return (
        <div className="w-full top-20">
      <form>
        <PaymentElement  />
        <button className="w-full mt-8 bg-[#D6B27D] py-3 font-semibold rounded-[10px] text-[#F7FAFC] uppercase"> SUBMIT</button>
      </form>
      </div>
    );
  };

  export default CheckoutForm;
