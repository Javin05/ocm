import React, { useEffect, useRef, useState } from "react";
import landingImg from "../../assets/hero_image.jpeg";
import Footer from "../../Components/Footer/Footer";
import Header from "../../Components/Header/Header";
import omImage from "../../assets/sweden-marriage.jpg";
import StaderCard from "../../Components/StaderCard/StaderCard";
import staderOne from "../../assets/staderOne.png";
import staderTwo from "../../assets/staderTwo.png";
import staderThree from "../../assets/staderThree.png";
import staderFour from "../../assets/staderFour.png";
import { createSearchParams, useNavigate } from "react-router-dom";

const Aboutus = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const navigate = useNavigate();
  const [isGlobalSearch, setIsGlobalSearch] = useState<boolean>(false);
  const [title, setTitle] = useState("");
  const [globalSearch, setGlobalSearch] = useState("Örebro");
  const [currentLocation, setCurrentLocation] = useState("Örebro");
  const getEventsBySearch = async (text: string) => {
    const params = {
      searchText: text,
      searchTitle: title,
      location: currentLocation,
    };
    navigate(
      {
        pathname: "/popular",
        search: `?${createSearchParams(params)}`,
      },
      {
        state: { isGlobalSearch },
      }
    );
  };
  return (
    <>
      <Header />
      {/* <div className="md:grid md:grid-cols-2 grid-cols-1 p-2"> */}
      <div className="flex md:flex-row flex-col">
        <div className="w-full px-5 flex">
          <p className="text-secondary">
            {/* <div className="w-full flex justify-center items-start p-6"> */}
            <h1 className="order-first font-[sofiapro] font-bold text-[24px] text-secondary sm:p-4 py-4">Om oss</h1>
            <img src={omImage} className="rounded-lg sm:w-[50%] w-[100%] sm:ml-3 sm:mb-4 float-left mr-6 mb-6" />
            {/* </div> */}
            Välkommen till Blixa – plattformen som
            revolutionerar sättet vi planerar och genomför minnesvärda stunder och
            fartfyllda event!
            <br />
            <p className="mt-2 text-secondary sm:ml-4">
              Vi är ett företag som brinner för att göra det möjligt för
              dig att skapa minnesvärda stunder och samtidigt inspirera till
              kreativitet och nyfikenhet.
              <br />
              <p className="mt-2 mr-4 text-secondary">
                Vår vision är att erbjuda en heltäckande lösning för
                livets alla stunder. Oavsett om det är en babyshower,
                begravning eller alla stunder där emellan så finns vi där.
                Till både privatpersoner och företag. Vi tror på att tänka
                utanför boxen och skapa unika tillfällen som får människor
                att le och minnas.
                <br />
                <p className="mt-2 mr-4 text-secondary">
                  På vår plattform hittar du allt du behöver för att skapa
                  den perfekta tillställningen. Vi har samlat en omfattande
                  samling lokaler, mat, dryck, aktiviteter, underhållning och
                  tillbehör av alla dess slag. Vårt mål är att erbjuda ett
                  brett spektrum av alternativ som spänner över traditionella, n
                  aturnära, kreativa och härligt lärande upplevelser.
                  <br />
                  <p className="mt-2 mr-4 text-secondary">
                    Vi är stolta över att vara en plattform där kreativiteten
                    möter bekvämligheten och där varje detalj spelar roll
                    för att skapa magiska stunder. Vi är också angelägna om
                    att stödja företagare inom eventbranschen. Genom att samla
                    mång unika leverantörer och ge dom en möjlighet att nå ut
                    till helt nya potentiella kunder, strävar vi efter att främja
                    tillväxt och framgång.
                    <br />
                    <p className="mt-2 mr-4 text-secondary">
                      Vi är glada över att vara en del av din festliga
                      tillställning och se hur dina drömmar blir till verklighet.
                      Tillsammans kan vi skapa minnen som varar förevigt och inspirera
                      till minnesvärda ögonblick.                    </p>
                  </p>
                </p>
              </p>
            </p>
          </p>
        </div>
      </div>
      <div className="md:px-8 px-5">
        <p className="sm:mt-12 sm:text-center text-left text-secondary">
          ”Samarbete är nyckeln till framgång och skapandet av magiska stunder.
          Vi tror på att förena kreativitet, passion och enkelhet för att göra
          varje tillställning till något alldeles extra. Genom att samla alla
          möjligheter och idéer på ett ställe har vi skapat en plattform där
          drömmar förvandlas till verklighet. Här skapar vi ögonblick som fr
          hjärtat att le och minnen att leva vidare.”
        </p>
      </div>
      <br />
      <div className="flex md:flex-row flex-col">

        <div className="w-full p-5">
          <h1 className="font-[sofiapro] font-bold text-[24px] sm:ml-4 text-secondary">Vår grundare </h1>
          <p className="mr-4 text-secondary sm:ml-4 mt-4 sm:mt-0">
            <img src={landingImg} className="object-fill rounded-lg sm:w-[50%] w-[100%] sm:ml-2 mb-4 float-right " />

            Sara är en nyfiken och kreativ person som älskar att tänka utanför boxen.
            Med tre små barn var hon på jakt efter nya och spännande sätt att skapa
            minnesvärda stunder för och tillsammans med dom. Men hon stod inför
            utmaningen om att tiden inte räckte till för att genomföra fantastiska
            kalas på det sätt hon önskade.
            <br />
            <p className="mt-2 mr-4 text-secondary">
              Sara drömde om att skapa unika och upplevelsebaserade tillställningar
              som verkligen skulle sticka ut och skapa minnen för livet. Hon upplevde
              att det var tidskrävande och svårt att tänka tillräckligt kreativt och
              långt utanför ramen för ”normala” barnkalas på egen hand. Det var då
              som hon kom på sig själv att önska att det fanns en annan lösning,
              EN plats alla möjligheter och idéer låg samlade för att inspirera
              och utmana till nya upplevelsebaserade kalas.
              <br />
              <p className="mt-2 mr-4 text-secondary">
                Det var under denna tid av önskningar och behov som Saras idé om
                en plattform föddes. Hon insåg att inte var ensam i sin strävan –
                det måste finnas fler föräldrar och festfixare som önskade samma sak.
                Hon bestämde sig för att ta saken i egna händer, att skapa den tjänst
                hon själv saknade. Sara ville inte bara skapa en plattform för att
                inspirera och hjälpa föräldrar att planera och genomföra fantastiska
                tillställningar, utan också att inkludera alla typer av event och
                tillställningar. Hon vill skapa en helhetslösning där människor
                kunde hitta allt det behövde för att skapa unika och minnesvärda stunder.
              </p>

              <p className="mt-2 mr-4 text-secondary">
                Och nu är BLIXA en verklighet. Det är en plats där kreativitet och
                glädje möts, där unika tillställningar tar form och där stunderna
                förblir oförglömliga.
              </p>
            </p>

          </p>
        </div>

        {/* <div className="w-full flex justify-start items-start p-6"> */}
        {/* </div> */}
      </div>
      <br />
      <div className="sm:-mt-6 mx-6 md:mx-16 mb-6">
        <h1 className="font-semibold text-[28px] sm:text-[1rem] leading-7 text-[#D6B27D]">
          Städer
        </h1>
        <h1 className="sm:text-[32px] font-semibold font-[sofiapro] text-secondary">
          Hitta oss dessa städer och manga fler
        </h1>
        <div className="grid grid-cols-1 sm:grid-cols-2 mb-2  md:grid-cols-2 lg:grid-cols-4 mt-4 gap-4 md:gap-6">
          <div className="grid-item">
            <StaderCard
              onCardClick={getEventsBySearch}
              staderImg={staderOne}
              staderTitle={"Örebro"}
            />
          </div>
          <div className="grid-item">
            <StaderCard
              onCardClick={getEventsBySearch}
              staderImg={staderTwo}
              staderTitle={"Stockholm"}
            />
          </div>
          <div className="grid-item">
            <StaderCard
              onCardClick={getEventsBySearch}
              staderImg={staderThree}
              staderTitle={"Uppsala"}
            />
          </div>
          <div className="grid-item">
            <StaderCard
              onCardClick={getEventsBySearch}
              staderImg={staderFour}
              staderTitle={"Västerås"}
            />
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};
export default Aboutus;
