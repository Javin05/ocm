import "./App.css";
import { RouterProvider } from "react-router-dom";
import router from "./Routes/Routes";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Helmet } from "react-helmet";
import { useState } from "react";
import { clearAll } from "./Utils/auth";
import { RootState } from "./Redux/store";
import deleteIcon from "./assets/Delete.png";
import { useSelector, useDispatch } from "react-redux";
import { failureNotify, successNotify } from "./Components/Toast/Toast";
import { AuthContext } from "./Pages/RootLayout/RootLayout";
import { setUserDetails } from "./Redux/features/userSlice";
import React, { ChangeEvent, useEffect } from "react";


function App() {
  const [isDeleteEventModalOpen, setIsDeleteEventModalOpen] = useState(false);
  const [isBookingWithOutLogIn, setIsBookingWithOutLogIn] = useState(false);
  const [bookingLogin, setBookingLogin] = useState(false)
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [menuClicked, setMenuClicked] = useState<string>("");
  const userInfo = useSelector((state: RootState) => state.user.userDetails);
  const [notifyRender, setNotifyReRender] = useState(false);

  const dispatch = useDispatch()

  const BookingWithOutLogIn = () => {
    const handleclose = () => {
      setIsBookingWithOutLogIn(false);
      console.log("isBookingWithOutLogIn set to true");
      closePopup();
      setMenuClicked("");
      document.body.classList.remove("overflow-hidden");
    }

    const handleredirect = () => {
      window.location.href = window.location.origin + "/login";
    };

    useEffect(() => {
      if (isBookingWithOutLogIn || isDeleteEventModalOpen || bookingLogin) {
        document.body.classList.add("overflow-hidden");
      } else {
        document.body.classList.remove("overflow-hidden");
      }
    }, [isBookingWithOutLogIn, isDeleteEventModalOpen, bookingLogin]);

    const closePopup = () => {
      setIsDeleteEventModalOpen(false);
      setIsBookingWithOutLogIn(false);
      setBookingLogin(false);
    };

    return (
      <div className="w-full h-screen bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center px-4">
          <div className="bg-white rounded-lg md:p-6 flex flex-col justify-center items-center md:w-[450px] w-full p-4 md:h-56 h-46 relative">
            <button onClick={handleclose}>
              <img
                src={deleteIcon}
                className="absolute top-1 md:right-3 right-1 md:mt-3 mt-1 h-4 w-4"
              />
            </button>
            <p className="font-semibold text-lg text-secondary">
              You should login to make a booking
            </p>
            <section className="flex flex-row items-center md:mt-10 mt-3">
              <button
                onClick={handleredirect}
                className="px-8 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer mx-4 uppercase"
              >
                Login
              </button>
              <button
                onClick={handleclose}
                className="px-5 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer uppercase"
              >
                Discard
              </button>
            </section>
          </div>
        </section>
      </div>
    );
  };

  const LogOutWhileMoveFromFooter = () => {
    const handleclose = () => {
      setIsDeleteEventModalOpen(false);
      setMenuClicked("");
      closePopup();

      // Remove the overflow-hidden class when the popup is closed
      document.body.classList.remove("overflow-hidden");
    };

    const logutPage = async (pageToRedirect: string) => {
      setIsLoading(true);
      try {
        if (userInfo.id && userInfo.role === "ROLE_ADMIN") {
          clearAll();
          window.location.href = window.location.origin;
        } else if (userInfo.id && userInfo.role === "ROLE_SUPPLIER") {
          clearAll();
          window.location.href = window.location.origin
        }
        else {

          dispatch(setUserDetails({}));
          successNotify("Logout successful");
          clearAll();
          window.location.href = window.location.origin


        }
      } catch (error) {
        failureNotify("oops! something went wrong");
      } finally {
        setIsLoading(false);
      }
    };

    useEffect(() => {
      if (isDeleteEventModalOpen || isBookingWithOutLogIn || bookingLogin) {
        document.body.classList.add("overflow-hidden");
      } else {
        document.body.classList.remove("overflow-hidden");
      }
    }, [isDeleteEventModalOpen, isBookingWithOutLogIn, bookingLogin]);

    const closePopup = () => {
      setIsDeleteEventModalOpen(false);
      setIsBookingWithOutLogIn(false);
      setBookingLogin(false);
    };

    let user_role;
    let opposite_user;
    if (userInfo.role === "ROLE_CUSTOMER") {
      user_role = 'customer'
      opposite_user = 'admin/supplier'
    } else if (userInfo.role === "ROLE_ADMIN") {
      user_role = 'admin'
      opposite_user = 'customer/supplier'
    }
    else {
      user_role = 'supplier'
      opposite_user = 'admin/customer'
    }

    return (
      <div className="w-full h-screen bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center px-4">
          <div className="bg-white rounded-lg md:p-6 flex flex-col justify-center items-center md:w-[450px] w-full p-4 md:h-56 h-46 relative">
            <button onClick={handleclose}>
              <img
                src={deleteIcon}
                className="absolute top-1 right-3 mt-3 h-4 w-4"
              />
            </button>

            <p className="font-semibold text-lg text-secondary mr-3 text-center">
              {user_role === 'customer' ?
                menuClicked === 'supplierlogin' ? `Currently logged in as a customer. Are you sure you want to logout and login as Supplier ?` : menuClicked === "adminlogin" ? `Currently logged in as a customer. Are you sure you want to logout and login as Admin ?` : `Är du säker på att du vill logga ut ?` : user_role === 'supplier' ? menuClicked === 'supplierlogin' ? `Currently logged in as a Supplier. Are you sure you want to logout and login as supplier ?` : menuClicked === "adminlogin" ? `Currently logged in as a supplier. Are you sure you want to logout and login as admin ?` : `Är du säker på att du vill logga ut ?` :
                  menuClicked === 'supplierlogin' ? `Currently logged in as a admin. Are you sure you want to logout and login as supplier ?` : menuClicked === "adminlogin" ? `Currently logged in as a Admin. Are you sure you want to logout and login as Admin ?` : `Är du säker på att du vill logga ut ?`
              }

            </p>
            <section className="flex flex-row items-center md:mt-10 mt-3">
              <button
                onClick={() => logutPage(menuClicked)}
                className="px-5 py-2 uppercase rounded-lg bg-golden text-white font-semibold text-base cursor-pointer"
              >
                Logga ut
              </button>
              <button
                onClick={handleclose}
                className="px-8 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer mx-4 uppercase"
              >
                Avbryt
              </button>
            </section>
          </div>
        </section>
      </div>
    );
  };


  const LogoutWithPopular = () => {
    const handleclose = () => {
      setBookingLogin(false);
      closePopup();
      document.body.classList.remove("overflow-hidden");
    }

    const handleredirect = () => {
      window.location.href = window.location.origin + "/login";
    };

    useEffect(() => {
      if (isDeleteEventModalOpen || isBookingWithOutLogIn || bookingLogin) {
        document.body.classList.add("overflow-hidden");
      } else {
        document.body.classList.remove("overflow-hidden");
      }
    }, [isDeleteEventModalOpen, isBookingWithOutLogIn, bookingLogin]);

    const closePopup = () => {
      setIsDeleteEventModalOpen(false);
      setIsBookingWithOutLogIn(false);
      setBookingLogin(false);
    };

    return (
      <div className="w-full h-screen bg-[#00000080] absolute z-40 right-0">
        <section className="w-full h-full flex flex-col items-center justify-center px-4">
          <div className="bg-white rounded-lg md:p-6 flex flex-col justify-center items-center md:w-[450px] w-full p-4 md:h-56 h-46 relative">
            <button onClick={handleclose}>
              <img
                src={deleteIcon}
                className="absolute top-1 md:right-3 right-1 md:mt-3 mt-1 h-4 w-4"
              />
            </button>
            <p className="font-semibold text-lg text-secondary">
              You should login to make a Purchase
            </p>
            <section className="flex flex-row items-center md:mt-10 mt-3">
              <button
                onClick={handleredirect}
                className="px-8 py-2 rounded-lg bg-golden text-white  font-semibold text-base cursor-pointer mx-4 uppercase"
              >
                Login
              </button>
              <button
                onClick={handleclose}
                className="px-5 py-2 rounded-lg bg-golden text-white font-semibold text-base cursor-pointer uppercase"
              >
                Discard
              </button>
            </section>
          </div>
        </section>
      </div>
    );
  };




  return (
    <div className="max-w-[1920px] mx-auto">
      <Helmet>
        <title>Blixa</title>‍
        <meta name="description" content="Mange your events with us" />
        <meta property="og:title" content="Blixa" />
        <meta
          property="og:description"
          content="Best Event management platform"
        />
        <meta
          property="og:image"
          content="https://ocm-images.fra1.cdn.digitaloceanspaces.com/assets/ocm_logo.png"
        />
        <meta property="og:url" content="app.ourcreativemoments.se" />
        <meta property="og:site_name" content="creative moments" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="events" />
        <meta property="fb:app_id" content="100087915611006" />
      </Helmet>
      <AuthContext.Provider
        value={{
          setIsDeleteEventModalOpen,
          setMenuClicked,
          setIsBookingWithOutLogIn,
          setBookingLogin,
          setNotifyReRender,
          menuClicked,
          notifyRender
        }}
      >
        {bookingLogin ? <LogoutWithPopular /> : null}
        {isDeleteEventModalOpen ? <LogOutWhileMoveFromFooter /> : null}
        {isBookingWithOutLogIn ? <BookingWithOutLogIn /> : null}
        <RouterProvider router={router} />
        <ToastContainer />
      </AuthContext.Provider>
    </div>
  );
}

export default App;
