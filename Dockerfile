### STAGE 1: Build ###
FROM node:16.19.0 AS build
WORKDIR /usr/src/app
COPY package*.json ./
COPY . .
ARG NPM_COMMAND=build  # Default npm command if not provided as a build argument
RUN npm install -f
RUN npm run build
### STAGE 2: Run ###
FROM nginx:latest
RUN rm -rf /usr/share/nginx/html/*
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/app/dist/ /usr/share/nginx/html
EXPOSE 4200
