import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

export default defineConfig({
  plugins: [react()],
  server: {
    proxy: {
      '/cemo': 'http://104.248.241.79:8022/'
      //  '/cemo': 'http://localhost:8080'
    },
  }
})


