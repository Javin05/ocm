const withMT = require("@material-tailwind/react/utils/withMT");
/** @type {import('tailwindcss').Config} */
module.exports = withMT({
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    colors: {
      primary: '#5c6ac4',
      secondary: '#A09792',
      golden: '#D6B37D',
      // ...
    }
  },
  plugins: [],
});

